FROM openjdk:alpine
COPY build/libs/A2ZBill-be-0.0.1-SNAPSHOT.jar A2ZBill-be-0.0.1-SNAPSHOT.jar
COPY  keystore.p12 keystore.p12
EXPOSE 8075

ENTRYPOINT exec java -Xmx500m -Xss50m -jar -Dspring.profiles.active=qa A2ZBill-be-0.0.1-SNAPSHOT.jar

##ENTRYPOINT exec java -Xmx500m -Xss50m -jar -Dspring.profiles.active=pord A2ZBill-be-0.0.1-SNAPSHOT.jar




