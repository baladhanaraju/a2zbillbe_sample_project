package com.a2zbill.services;

import com.a2zbill.domain.OrganisationPayments;
import java.util.List;

public interface OrganisationPaymentsService {

  void save(OrganisationPayments organisationPayments);

  List<OrganisationPayments> getAllOrganisationPayments();

  void update(OrganisationPayments organisationPayments);

  List<OrganisationPayments> getOrganisationPaymentsByOrgId(final long orgId);

  OrganisationPayments getOrganisationPaymentsByOrgIdAndPaymentId(
      final long orgId, final long paymentId);
}
