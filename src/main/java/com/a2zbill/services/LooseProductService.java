package com.a2zbill.services;

import com.a2zbill.domain.LooseProduct;
import java.util.List;

public interface LooseProductService {
  void save(LooseProduct looseProduct);

  boolean update(LooseProduct looseProduct);

  List<LooseProduct> getAllLooseProductDetails();

  LooseProduct getLooseProductDetailsByCode(final String looseProductCode);

  LooseProduct getLooseProductDetailsByHsnCode(String looseProductHsnCode);

  LooseProduct getLooseProductDetailsById(long looseProductId);
}
