package com.a2zbill.services;

import com.a2zbill.domain.CancelHistory;
import java.util.List;

public interface CancelHistoryService {
  void save(CancelHistory cancelHistory);

  boolean delete(long id);

  boolean update(CancelHistory cancelHistory);

  List<CancelHistory> getAllCancelHistory();

  CancelHistory getCancelHistoryById(Long id);
}
