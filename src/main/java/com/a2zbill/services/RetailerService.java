package com.a2zbill.services;

import com.a2zbill.domain.Retailer;
import java.util.List;

public interface RetailerService {
  void save(Retailer retailer);

  List<Retailer> getRetailerDetails();

  Retailer getRetailerDetailsByGSTNumber(String retailerGstNumber);
}
