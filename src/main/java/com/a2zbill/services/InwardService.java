package com.a2zbill.services;

import com.a2zbill.domain.Inward;
import java.util.List;

public interface InwardService {
  void save(final Inward inwards);

  boolean delete(final long id);

  boolean update(final Inward inwards);

  List<Inward> getAllInwards();

  List<Inward> getInwardDetailsByMonthAndYear(final String month, final String year);
}
