package com.a2zbill.services;

import com.a2zbill.domain.SuggestedContent;
import java.util.List;

public interface SuggestedContentService {

  void save(SuggestedContent suggestedContent);

  List<SuggestedContent> getAllSuggestedContent();
}
