package com.a2zbill.services;

import com.a2zbill.domain.FormType;
import java.util.List;
import java.util.Map;

public interface FormTypeService {
  void save(final FormType formType);

  boolean delete(final long id);

  boolean update(final FormType formType);

  List<FormType> getAllFormTypes();

  FormType getFormTypeById(final long formid);

  Map<String, Object> getFormTypedetailsById(final long formid);
}
