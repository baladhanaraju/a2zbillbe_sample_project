package com.a2zbill.services;

import com.a2zbill.domain.RecipeProduct;
import java.util.List;

public interface RecipeProductService {

  void save(RecipeProduct recipeProduct);

  boolean delete(Long id);

  boolean update(RecipeProduct recipeProduct);

  List<RecipeProduct> getAllRecipeProductDetails();

  List<RecipeProduct> getAllRecipeProductByOrgAndBranch(long orgId, long branchId);

  List<RecipeProduct> getAllRecipeProductByOrg(long orgId);

  public RecipeProduct getRecipeProductById(long recipeProductId);

  RecipeProduct getRecipeProductByRecipeIdAndProductId(long recipeId, long productId);

  List<RecipeProduct> getAllRecipeProductsProductIdAndOrgAndBranch(
      long orgId, long branchId, long productId);

  List<RecipeProduct> getAllRecipeProductByRecipeId(long recipeId);
}
