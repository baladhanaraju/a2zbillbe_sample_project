package com.a2zbill.services;

import com.a2zbill.domain.Item;
import java.util.List;

public interface ItemService {
  void save(Item items);

  boolean delete(Long id);

  boolean update(Item items);

  Item getItemsById(Long id);

  List<Item> getAllItemsName();

  Item getItemsByHsnNo(String hsnNo);

  List<Item> getAllItemsByHsnNo();

  Item getItemsByName(String commodity);
}
