package com.a2zbill.services;

import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingPayments;
import java.util.List;

public interface BillingPaymentsService {
  void save(BillingPayments billing);

  boolean update(BillingPayments billing);

  Billing getBillingDetailsByBillId(long billId);

  Billing getBillingPaymentsAndOrgIdAndBranchId(long billId, long orgId, long branchId);

  List<BillingPayments> getBillingPaymentsDetailsByBillId(long billId);
}
