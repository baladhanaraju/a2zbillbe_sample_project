package com.a2zbill.services;

import com.a2zbill.domain.ReverseOrder;
import java.util.List;

public interface ReverseOrderService {
  void save(ReverseOrder reverseOrder);

  boolean delete(long id);

  boolean update(ReverseOrder reverseOrder);

  List<ReverseOrder> getAllReverseOrderDetails();

  ReverseOrder getReverseOrderDetailsByReverseOrderById(final long reverseOrderId);
}
