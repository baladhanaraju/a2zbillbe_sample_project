package com.a2zbill.services;

import com.a2zbill.domain.Rating;
import java.util.List;

public interface RatingService {
  void save(Rating rating);

  boolean delete(Long id);

  boolean update(Rating rating);

  Rating getRatingByRatingId(Long ratingId);

  Rating getRatingByProductId(Long productId);

  List<Rating> getAllRatingDetails();

  Rating getRatingByEmpIdAndProductId(Long productId, long empId);
}
