package com.a2zbill.services;

import com.a2zbill.domain.Category;
import java.util.List;

public interface CategoryService {
  void save(Category category);

  boolean delete(Long id);

  boolean update(Category category);

  Category getCategoryByName(String categoryName);

  List<Category> getAllCategories();
}
