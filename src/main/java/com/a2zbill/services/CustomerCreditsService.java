package com.a2zbill.services;

import com.a2zbill.domain.CustomerCredits;
import com.tsss.basic.paytm.domain.Credits;
import java.util.List;

public interface CustomerCreditsService {

  void save(CustomerCredits customerCredits);

  boolean delete(final long id);

  boolean update(CustomerCredits customerCredits);

  List<CustomerCredits> getAllCustomerCredits();

  CustomerCredits getCustomerCreditsById(final Long id);

  CustomerCredits getCustomerCreditsByCustomerId(final Long customerId);

  Credits getAllCreditsByOrgidCustomer(final long orgid);
}
