package com.a2zbill.services;

import com.a2zbill.domain.Rules;
import java.util.List;

public interface RulesService {
  void save(Rules rules);

  boolean delete(Rules rules);

  boolean update(Rules rules);

  List<Rules> getAllRulesDetails();

  Rules getRuleDetailsByRuleId(long ruleId);

  List<Rules> getRulesDetailsByOrgAndBranch(long orgId, long branchId);

  List<Rules> getRulesDetailsByOrg(long orgId);
}
