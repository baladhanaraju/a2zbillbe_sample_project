package com.a2zbill.services;

import com.a2zbill.domain.CancelItem;
import java.util.List;

public interface CancelItemService {

  void save(final CancelItem cancelItem);

  boolean delete(final long id);

  boolean update(final CancelItem cancelItem);

  List<CancelItem> getAllCancelItemDetails();

  List<CancelItem> getAllCancelItemDetailsByorgBranchId(final long orgId, final long branchId);

  CancelItem getCancelItemDetailsByorgBranchIdAndGuid(
      final long orgId, final long branchId, final String guid);

  List<CancelItem> getAllCancelItemDetailsByorg(final long orgId);
}
