package com.a2zbill.services;

import com.a2zbill.domain.Payment;
import java.util.List;

public interface PaymentService {
  void save(final Payment payment);

  void update(final Payment payment);

  List<Payment> getAllPayments();
  // List<Payment> getPaymentDetailsByOrgIdAndBranchId(long orgId, long branchId);
  Payment getPaymentType(final String paymentType);

  Payment getPaymentById(final long paymentId);

  List<Payment> getAllPaymentsDefaultTrue();

  void savePaymentModes(Payment payment);
}
