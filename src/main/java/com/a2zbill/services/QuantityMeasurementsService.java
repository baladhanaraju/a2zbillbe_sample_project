package com.a2zbill.services;

import com.a2zbill.domain.QuantityMeasurements;
import java.util.List;

public interface QuantityMeasurementsService {
  void save(QuantityMeasurements quantityMeasurements);

  boolean delete(long id);

  boolean update(QuantityMeasurements quantityMeasurements);

  List<QuantityMeasurements> getAllQuantityMeasurements();

  QuantityMeasurements getQuantityMeasurementsById(Long id);

  QuantityMeasurements getQuantityMeasurementsByName(String name);
}
