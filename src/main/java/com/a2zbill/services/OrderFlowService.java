package com.a2zbill.services;

import com.a2zbill.domain.OrderFlow;
import java.util.List;

public interface OrderFlowService {
  void save(OrderFlow orderFlow);

  boolean delete(Long id);

  boolean update(OrderFlow orderFlow);

  OrderFlow getOrderFlowById(Long id);

  OrderFlow getOrderFlowByCartId(Long cartId);

  OrderFlow getOrderFlowByCartIdAndProductIdAndFlowId(Long cartId, Long productId);

  OrderFlow getOrderFlowByCartIdAndProductIdAndCartIdByServiceSection(Long cartId, Long productId);

  List<OrderFlow> getOrderFlowByFlowId();

  List<OrderFlow> getOrderFlowByCounterIdAndFlowId();

  List<OrderFlow> getAllOrderFlow();
}
