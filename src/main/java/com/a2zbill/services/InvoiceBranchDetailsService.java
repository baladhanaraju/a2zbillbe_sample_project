package com.a2zbill.services;

import com.a2zbill.domain.InvoiceBranchDetails;

public interface InvoiceBranchDetailsService {
  void save(InvoiceBranchDetails invoiceBranchDetails);
}
