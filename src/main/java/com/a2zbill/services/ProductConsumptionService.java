package com.a2zbill.services;

import com.a2zbill.domain.ProductConsumption;
import java.util.Date;
import java.util.List;

public interface ProductConsumptionService {

  void save(ProductConsumption productConsumption);

  boolean delete(final long id);

  boolean update(ProductConsumption productComposition);

  List<ProductConsumption> getAllProductConsumption();

  public ProductConsumption getProductConsumptionById(long id);

  ProductConsumption getProductConsumptionByproductAndcurrentDate(long productId, Date date);

  ProductConsumption getProductConsumptionByproductId(long productId);

  ProductConsumption
      getProductConsumptionByproductAndmenuProductAndwastageFlagAnddateAndOrgAndBranch(
          long productId, long menuProductId, boolean status, Date date, long orgId, long branchId);

  ProductConsumption getProductConsumptionBymenuProductAndwastageFlagAnddateAndOrgAndBranch(
      long menuProductId, boolean status, Date date, long orgId, long branchId);
}
