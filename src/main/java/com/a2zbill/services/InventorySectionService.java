package com.a2zbill.services;

import com.a2zbill.domain.InventorySection;
import java.util.List;

public interface InventorySectionService {
  void save(InventorySection inventorySection);

  boolean delete(long id);

  boolean update(InventorySection inventorySection);

  InventorySection getInventorySectionById(Long id);

  List<InventorySection> getAllInventorySection();

  List<InventorySection> getAllInventorySectionByOrgId(long orgId);

  List<InventorySection> getAllInventorySectionByOrgIdBranchId(long orgId, long branchId);

  List<InventorySection> getAllInventorySectionByOrgIdBranchIdAndMasterFlag(
      long orgId, long branchId, String masterCountFlag);

  InventorySection getInventorySectionByOrgIdBranchIdAndMasterFlag(
      long orgId, long branchId, String masterCountFlag);

  InventorySection getInventorySectionByOrgIdBranchIdAndId(long orgId, long branchId, long id);

  InventorySection getInventorySectionBySectionNameOrgIdBranchId(
      String sectionName, long orgId, long branchId);

  InventorySection getInventorySectionByOrgIdAndId(long orgId, long id);

  List<InventorySection> getAllInventorySectionByOrgIdBranchIdByStaticKotFlag(
      long orgId, long branchId);

  List<Object[]> getInventorySectionNameByOrgId(long orgId);

  List<Object[]> getInventorySectionNamesByOrgIdAndBranchId(long orgId, long branchId);
}
