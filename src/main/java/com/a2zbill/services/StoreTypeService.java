package com.a2zbill.services;

import com.a2zbill.domain.StoreType;
import java.util.List;

public interface StoreTypeService {

  public void save(StoreType storeType);

  public boolean delete(long id);

  public boolean update(StoreType storeType);

  List<StoreType> getAllStoreTypeDetails();

  StoreType storeTypegetDetalisById(final long id);
}
