package com.a2zbill.services;

import com.a2zbill.domain.BillingNumData;
import java.util.Date;
import java.util.List;

public interface BillingNumDataService {

  void save(BillingNumData billingNumData);

  boolean delete(BillingNumData billingNumData);

  boolean update(BillingNumData billingNumData);

  List<BillingNumData> getAllBillingNumData();

  BillingNumData getAllBillingNumDataByDateAndOrgBranchId(
      Date date, long billingNumberTypeId, long orgId, long branchId);

  BillingNumData getAllBillingNumDataByDateExtractAndOrgBranchId(
      long billingNumberTypeId, int day, int month, int year, long orgId, long branchId);
}
