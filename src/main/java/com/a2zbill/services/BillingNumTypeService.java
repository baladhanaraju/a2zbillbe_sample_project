package com.a2zbill.services;

import com.a2zbill.domain.BillingNumType;
import java.util.List;

public interface BillingNumTypeService {
  void save(BillingNumType billingNumType);

  boolean delete(BillingNumType billingNumType);

  boolean update(BillingNumType billingNumType);

  List<BillingNumType> getAllBillingNumType();

  BillingNumType getBillingNumTypeDetailsByBillingTypeName(String billingTypeName);
}
