package com.a2zbill.services;

import com.a2zbill.domain.ProductPackaging;
import java.util.List;

public interface ProductPackagingService {

  void save(ProductPackaging productPackaging);

  boolean delete(ProductPackaging productPackaging);

  boolean update(ProductPackaging productPackaging);

  List<ProductPackaging> getAllProductPackagingDetails();

  ProductPackaging getProductPackagingDetailsByProductId(final long productId);

  List<ProductPackaging> getProductPackgingDetailsByParentId(final long parentId);
}
