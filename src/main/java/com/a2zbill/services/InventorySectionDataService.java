package com.a2zbill.services;

import com.a2zbill.domain.InventorySectionData;
import java.util.Date;
import java.util.List;

public interface InventorySectionDataService {
  void save(InventorySectionData inventorySectionData);

  boolean delete(long id);

  boolean update(InventorySectionData inventorySectionData);

  List<InventorySectionData> getAllInventorySectionData();

  InventorySectionData getInventorySectionDataByInventorySectionId(long inventorySectionId);

  InventorySectionData getInventorySectionDataByproductIdAndOrgIdBranchId(
      String inventorysectionName, long productId, long orgId, long branchId);

  InventorySectionData getInventorySectionDataByproductIdAndOrgId(
      String inventorysectionName, long productId, long orgId);

  List<InventorySectionData> getAllInventorySectionDataByOrgIdBranchId(long orgId, long branchId);

  List<InventorySectionData> getAllInventorySectionDataByOrgId(long orgId);

  List<InventorySectionData> getInventorySectionDataByFromDateToDateOrgIdAndBranchId(
      Date fromDate, Date toDate, long orgId, long branchId);

  List<InventorySectionData> getInventorySectionDataByFromDateToDateOrgId(
      Date fromDate, Date toDate, long orgId);

  List<InventorySectionData> getInventorySectionDataByOrgIdAndBranchIdAndSectionName(
      long orgId, long branchId, String sectionName);
}
