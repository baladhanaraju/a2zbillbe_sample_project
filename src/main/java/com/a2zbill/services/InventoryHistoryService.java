package com.a2zbill.services;

import com.a2zbill.domain.InventoryHistory;
import java.util.Date;
import java.util.List;

public interface InventoryHistoryService {
  void save(InventoryHistory inventoryHistory);

  boolean delete(long id);

  boolean update(InventoryHistory inventoryHistory);

  List<InventoryHistory> getAllInventoryHistorys();

  List<InventoryHistory> getAllInventoryHistorys(long branchId, long orgId);

  List<InventoryHistory> getAllInventoryHistorysByOrg(long orgId);

  List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndVendorBetweenTwoDates(
      Date fromDate, Date toDate, long orgId, long vendorId);

  List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBranchAndVendorBetweenTwoDates(
      Date fromDate, Date toDate, long orgId, long branchId, long vendorId);

  List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBranchAndBetweenTwoDates(
      Date fromDate, Date toDate, long orgId, long branchId);

  List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBetweenTwoDates(
      Date fromDate, Date toDate, long orgId);

  List<InventoryHistory> getAllInventoryHistorysByInventoryId(
      long branchId, long orgId, long inventoryId);

  List<InventoryHistory> getAllInventoryHistorysByInventoryId(long branchId, long orgId);

  List<InventoryHistory> getAllInventoryHistoryDetailsByInvoiceBillId(
      long branchId, long orgId, long invoiceId);

  List<InventoryHistory> getAllInventoryHistoryDetailsByInvoiceBillId(long orgId, long invoiceId);

  List<InventoryHistory>
      getAllInventoryHistoryDetailsByInventoryIdAndOrgAndBranchAndBetweenTwoDates(
          long inventoryId, long orgId, long branchId, Date fromDate, Date toDate);

  String getMaxCountByBatchNumberAndInventoryIdAndOrgAndBranch(
      String batchNumber, long inventoryId, long orgId, long branchId);

  InventoryHistory getInventoryHisotryDetailsByBatchNumberAndInventoryIdAndOrgAndBranch(
      String batchNumber, long inventoryId, long orgId, long branchId);

  List<InventoryHistory>
      getInventoryHsitoryDetailsByExpiryDateAndProductIdAndOrgIdAndBranchIdOrderByAvailableQuantity(
          Date expiryDate, long productId, long orgId, long branchId);

  List<InventoryHistory> getInventoryHistoryDetailsByProductIdAndOrgIdAndBranchIdOrderByExpiryDate(
      long productId, long orgId, long branchId);

  List<Object[]> getAvailableQuantityWithExpiryDateByProductIdAndOrgIdAndBrnachId(
      long productId, long orgId, long branchId);

  List<InventoryHistory> getInventoryHisotryDetailsByDatesAndProductIdAndOrgIdAndBranchId(
      final Date fromDate,
      final Date toDate,
      final long productId,
      final long orgId,
      final long branchId);

  List<InventoryHistory> getInventoryHistoryDetailsByDateAndVendorIdAndProductIdAndOrgIdAndBranchId(
      final Date fromDate,
      final Date toDate,
      final long vendorId,
      final long productId,
      final long orgId,
      final long branchId);
}
