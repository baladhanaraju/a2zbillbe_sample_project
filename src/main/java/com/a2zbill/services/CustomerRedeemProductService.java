package com.a2zbill.services;

import com.a2zbill.domain.CustomerRedeemProduct;
import java.util.List;

public interface CustomerRedeemProductService {

  void save(final CustomerRedeemProduct customerRedeemProduct);

  boolean delete(final long id);

  boolean update(final CustomerRedeemProduct customerRedeemProduct);

  List<CustomerRedeemProduct> getAllCustomerRedeemProducts();

  List<Object[]> getCustomerRedeemProductById(final long productid);

  CustomerRedeemProduct getCustomerRedeemProductByproductIdOrgIdAndBranchId(
      final long productid, final long orgId, final long branchId);

  CustomerRedeemProduct getCustomerRedeemProductByproductIdOrgId(
      final long productid, final long orgId);

  List<CustomerRedeemProduct> getCustomerRedeemProductBycustomerId(final long customerid);

  List<CustomerRedeemProduct> getCustomerRedeemProductBycustomerIdByOrgIdAndBranchId(
      final long customerid, final long orgId, final long branchId);

  CustomerRedeemProduct getCustomerRedeemProductByproductIdAndCustId(
      final long productId, final long customerId);
}
