package com.a2zbill.services;

import com.a2zbill.domain.CustomerReward;
import java.util.List;

public interface CustomerRewardService {
  void save(CustomerReward customerReward);

  boolean delete(Long id);

  boolean update(CustomerReward customerReward);

  CustomerReward getCustomerRewardsById(Long id);

  List<CustomerReward> getAllCustomerRewards();
}
