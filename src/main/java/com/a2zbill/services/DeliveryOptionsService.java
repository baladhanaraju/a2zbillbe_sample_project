package com.a2zbill.services;

import com.a2zbill.domain.DeliveryOptions;
import java.util.List;

public interface DeliveryOptionsService {

  void save(final DeliveryOptions deliveryOptions);

  boolean delete(final DeliveryOptions deliveryOptions);

  boolean update(final DeliveryOptions deliveryOptions);

  DeliveryOptions getDeliveryOptionsByid(final long id);

  List<DeliveryOptions> getAllDeliveryOptions();
}
