package com.a2zbill.services;

import com.a2zbill.domain.ExpensesManagement;
import java.util.Date;
import java.util.List;

public interface ExpensesManagementService {

  void save(final ExpensesManagement expensesManagement);

  boolean delete(final ExpensesManagement expensesManagement);

  boolean update(final ExpensesManagement expensesManagement);

  ExpensesManagement getExpensesManagementById(final long id);

  List<ExpensesManagement> getAllExpensesManagements();

  List<ExpensesManagement> getExpensesByOrgIdAndBranchId(final long orgId, final long branchId);

  List<ExpensesManagement> getExpensesByOrgId(final long orgId);

  List<ExpensesManagement> getExpensesByOrgIdAndBranchIdAndDate(
      final long orgId, final long branchId, final Date date);

  List<ExpensesManagement> getExpensesByOrgIdAndDate(final long orgId, final Date date);

  List<ExpensesManagement> getExpensesDetailsByOrgAndBranch(
      final Date fromDate, final Date toDate, final long orgId, final long branchId);

  List<ExpensesManagement> getExpensesDetailsByOrgAndBranchisNUll(
      final Date fromDate, final Date toDate, final long orgId);
}
