package com.a2zbill.services;

import com.a2zbill.domain.InvoiceBill;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface InvoiceBillService {

  void save(InvoiceBill invoiceBill);

  boolean update(InvoiceBill invoiceBill);

  boolean delete(InvoiceBill invoiceBill);

  InvoiceBill getInvoiceBillByIdAndOrgId(long invoiceBillId, long orgId);

  InvoiceBill getInvoiceBillByIdAndOrgIdAndBranchId(long invoiceBillId, long orgId, long branchId);

  List<InvoiceBill> getAllInvoiceBillsByOrgId(long orgId);

  List<InvoiceBill> getAllInvoiceBillsByOrgIdAndBranchId(long orgId, long branchId);

  InvoiceBill getInvoiceBillByInvoiceNumber(String invoiceNumber);

  List<InvoiceBill> getAllInvoiceDetailsByDates(Date fromDate, Date toDate, long branchId);

  List<InvoiceBill> getAllInvoiceDetailsByDatesVendor(
      Date fromDate, Date toDate, long branchId, long vendorId);

  List<InvoiceBill> getAllInvoiceDetailsVendorNameTotalAmount(
      Date fromDate, Date toDate, long branchId);

  List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchVendorAndOrganisation(
      Date fromDate, Date toDate, long branchVendorId, long branchId);

  List<Object[]> getInvoiceVendorDetails(
      String invoiceNumber, Date fromDate, Date toDate, long branchId);

  List<Object[]> getAllInvoiceVendorDetails(Date fromDate, Date toDate, long branchId);

  List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchAndBranchVendor(
      Date fromDate, Date toDate, long orgId, long branchId, long branchVendorId);

  InvoiceBill getAllInvoiceDetailsByDatesByRefernceNumber(
      Date fromDate,
      Date toDate,
      long orgId,
      String refernceNumber,
      long branchId,
      long branchvendorId);

  List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDates(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long frombranchId);

  List<InvoiceBill> getInvoiceBillsTotalAmt(
      Date formDate, Date endDate, long branchId, long orgId, long frombranchId);

  List<InvoiceBill> getAllInvoiceDetailsVendorNameTotalAmount(
      long orgId, Date fromDate, Date toDate, long branchId);

  List<InvoiceBill> getAllInvoiceDetailsByDatesVendor(
      long orgId, Date fromDate, Date toDate, long branchId, long vendorId);

  List<InvoiceBill> getAllInvoiceDetailsByDateBydate(
      Date fromDate, Date toDate, long orgId, long branchId);

  List<Object[]> getAllInvoiceDetailsBySection(
      long orgId, long branchId, Date fromDate, Date toDate, long fromSectionId);

  List<Object[]> getAllInvoiceDetailsByFromBranch(
      long orgId, long branchId, Date fromDate, Date toDate, long fromBranchId);

  List<InvoiceBill> getAllInvoiceDetailsByBranchAndVendorIsNull(
      long orgId, Date fromdate, Date todate, long branchId);

  List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchAndFromSectionId(
      Date fromDate, Date toDate, long orgId, long branchId, long fromSectionId);

  InvoiceBill getInvoiceDetailsByDatesAndRefernceNumberAndBranchAndFromSectionId(
      Date fromDate,
      Date toDate,
      long orgId,
      String referenceNumber,
      long branchId,
      long fromSectionId);

  List<InvoiceBill> getInvoiceBillByOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long fromSectionId, long orgId, long branchId);

  List<InvoiceBill> getInvoiceBillByTransferOrgIdFromBranchId(
      Date fromdate, Date todate, long fromBranchId, long orgId, long branchId);

  List<InvoiceBill> getInvoiceBillByTransferOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long fromSectionId, long orgId, long branchId);

  List<InvoiceBill> getInvoiceBillByOrgIdFromBranchId(
      Date fromdate, Date todate, long fromBranchId, long orgId, long branchId);

  List<Object[]> getAllInvoiceBillProductQuantityByFromToBranch(
      long orgId, long branchId, Date fromdate, Date todate, long frombranchId);

  List<Object[]> getAllInvoiceBillProductQuantityByFromSectionToBranch(
      long orgId, long branchId, Date fromdate, Date todate, long fromSectionId);

  List<InvoiceBill> getAllInvoiceDetailsByBranchAndVendorIdIsNull(
      long orgId, Date fromdate, Date todate, long fromBranchId);

  List<InvoiceBill> getAllInvoiceDetailsByBranchAndSectionIdIsNull(
      long orgId, Date fromdate, Date todate, long fromSectionId);

  List<InvoiceBill> getAllInvoiceBillBySection(
      Date fromDate, Date toDate, long orgId, long branchId, long fromsectionId);

  List<InvoiceBill> getAllInvoiceBillBySectionreference(
      Date fromDate, Date toDate, long orgId, long branchId, long fromsectionId, String reference);

  List<Object[]> getAllInvoiceBillByTotalAmountSection(
      Date fromDate, Date toDate, long orgId, long branchId, long fromsectionId);

  List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDatesToBranchReference(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long fromSectionId);

  List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDatesToBranch(
      Date formDate, Date endDate, long branchId, long orgId, long fromSectionId);

  List<Map<String, Object>> getInvoiceBillDetailsByLoginVendor(
      final long vendorId, final long orgId);

  List<Object[]> getInvoiceBillDetailsByReport(
      Date fromDate, Date toDate, long orgId, long branchId);
}
