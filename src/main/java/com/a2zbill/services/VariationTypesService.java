package com.a2zbill.services;

import com.a2zbill.domain.VariationTypes;
import java.util.List;

public interface VariationTypesService {

  List<VariationTypes> getAllVariationTypesDetails();

  VariationTypes getVariationTypesById(long id);

  VariationTypes getVariationTypesByName(String name);

  List<VariationTypes> getOptionsByVariationType(long variationTypeId);
}
