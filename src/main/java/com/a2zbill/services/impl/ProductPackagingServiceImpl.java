package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductPackagingDao;
import com.a2zbill.domain.ProductPackaging;
import com.a2zbill.services.ProductPackagingService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductPackagingServiceImpl implements ProductPackagingService {

  @Autowired ProductPackagingDao productPackagingDao;

  @Override
  public void save(final ProductPackaging productPackaging) {

    this.productPackagingDao.save(productPackaging);
  }

  @Override
  public boolean delete(final ProductPackaging productPackaging) {

    return this.productPackagingDao.delete(productPackaging);
  }

  @Override
  public boolean update(final ProductPackaging productPackaging) {

    return this.productPackagingDao.update(productPackaging);
  }

  @Override
  public List<ProductPackaging> getAllProductPackagingDetails() {

    return this.productPackagingDao.getAllProductPackagingDetails();
  }

  @Override
  public ProductPackaging getProductPackagingDetailsByProductId(final long productId) {
    return this.productPackagingDao.getProductPackagingDetailsByProductId(productId);
  }

  @Override
  public List<ProductPackaging> getProductPackgingDetailsByParentId(final long parentId) {
    return productPackagingDao.getProductPackgingDetailsByParentId(parentId);
  }
}
