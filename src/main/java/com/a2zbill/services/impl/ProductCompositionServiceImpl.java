package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductCompositionDao;
import com.a2zbill.domain.ProductComposition;
import com.a2zbill.services.ProductCompositionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductCompositionServiceImpl implements ProductCompositionService {
  @Autowired ProductCompositionDao productCompositionDao;

  @Override
  public void save(final ProductComposition productComposition) {
    this.productCompositionDao.save(productComposition);
  }

  @Override
  public boolean delete(final long id) {
    return this.productCompositionDao.delete(id);
  }

  @Override
  public boolean update(final ProductComposition productComposition) {
    return this.productCompositionDao.update(productComposition);
  }

  @Override
  public List<ProductComposition> getAllProductComposition() {
    return this.productCompositionDao.getAllProductComposition();
  }

  @Override
  public List<ProductComposition> getProductCompositionbyPackageFlag(final Boolean packageFlag) {
    return this.productCompositionDao.getProductCompositionbyPackageFlag(packageFlag);
  }

  @Override
  public List<ProductComposition> getAllProductCompositionByProductId(final long productId) {
    return this.productCompositionDao.getAllProductCompositionByProductId(productId);
  }
}
