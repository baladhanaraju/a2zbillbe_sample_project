package com.a2zbill.services.impl;

import com.a2zbill.dao.RecipeProductDao;
import com.a2zbill.domain.RecipeProduct;
import com.a2zbill.services.RecipeProductService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RecipeProductServiceImpl implements RecipeProductService {

  @Autowired private RecipeProductDao recipeProductDao;

  @Override
  public void save(final RecipeProduct recipeProduct) {
    recipeProductDao.save(recipeProduct);
  }

  @Override
  public boolean delete(final Long id) {
    return false;
  }

  @Override
  public boolean update(final RecipeProduct recipeProduct) {
    this.recipeProductDao.update(recipeProduct);
    return true;
  }

  @Override
  public List<RecipeProduct> getAllRecipeProductDetails() {
    return this.recipeProductDao.getAllRecipeProductDetails();
  }

  @Override
  public List<RecipeProduct> getAllRecipeProductByOrgAndBranch(
      final long orgId, final long branchId) {
    return this.recipeProductDao.getAllRecipeProductByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<RecipeProduct> getAllRecipeProductByOrg(final long orgId) {
    return this.recipeProductDao.getAllRecipeProductByOrg(orgId);
  }

  @Override
  public RecipeProduct getRecipeProductById(final long recipeProductId) {
    return this.recipeProductDao.getRecipeProductById(recipeProductId);
  }

  @Override
  public RecipeProduct getRecipeProductByRecipeIdAndProductId(
      final long recipeId, final long productId) {
    return recipeProductDao.getRecipeProductByRecipeIdAndProductId(recipeId, productId);
  }

  @Override
  public List<RecipeProduct> getAllRecipeProductsProductIdAndOrgAndBranch(
      final long orgId, final long branchId, final long productId) {
    return this.recipeProductDao.getAllRecipeProductsProductIdAndOrgAndBranch(
        orgId, branchId, productId);
  }

  @Override
  public List<RecipeProduct> getAllRecipeProductByRecipeId(final long recipeId) {
    return this.recipeProductDao.getAllRecipeProductByRecipeId(recipeId);
  }
}
