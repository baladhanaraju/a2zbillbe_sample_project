package com.a2zbill.services.impl;

import com.a2zbill.dao.SecretKeyDao;
import com.a2zbill.domain.SecretKey;
import com.a2zbill.services.SecretKeyService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SecretKeyServiceImpl implements SecretKeyService {
  @Autowired SecretKeyDao secretDao;

  @Override
  public void save(final SecretKey secret) {
    this.secretDao.save(secret);
  }

  @Override
  public void update(final SecretKey secret) {
    this.secretDao.update(secret);
  }

  @Override
  public boolean delete(final Long id) {
    this.secretDao.delete(id);
    return true;
  }

  @Override
  public List<SecretKey> getAllSecret() {
    return this.secretDao.getAllSecret();
  }

  @Override
  public SecretKey getSecretKeyById(final long id) {
    return this.secretDao.getSecretKeyById(id);
  }

  @Override
  public SecretKey getDetailsBySecretKey(final String secretKey) {
    return this.secretDao.getDetailsBySecretKey(secretKey);
  }

  @Override
  public List<SecretKey> getAllSecretByOrgIdBranchId(final long orgId, final long branchId) {
    return this.secretDao.getAllSecretByOrgIdBranchId(orgId, branchId);
  }

  @Override
  public List<SecretKey> getAllSecretByOrgId(final long orgId) {
    return this.secretDao.getAllSecretByOrgId(orgId);
  }
}
