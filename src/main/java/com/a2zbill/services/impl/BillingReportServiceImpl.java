package com.a2zbill.services.impl;

import com.a2zbill.dao.BillingReportDao;
import com.a2zbill.domain.BillingReport;
import com.a2zbill.services.BillingReportService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BillingReportServiceImpl implements BillingReportService {
  @Autowired BillingReportDao billingReportDao;

  @Override
  public void save(final BillingReport billingreport) {
    this.billingReportDao.save(billingreport);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final BillingReport billingreport) {
    return this.billingReportDao.update(billingreport);
  }

  @Override
  public List<BillingReport> getAllBillingReport() {
    return this.billingReportDao.getAllBillingReport();
  }

  @Override
  public BillingReport getBillingReportByEmpid(final long empid, final Date date) {
    return this.billingReportDao.getBillingReportByEmpid(empid, date);
  }
}
