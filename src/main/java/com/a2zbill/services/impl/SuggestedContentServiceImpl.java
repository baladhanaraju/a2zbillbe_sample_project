package com.a2zbill.services.impl;

import com.a2zbill.dao.SuggestedContentDao;
import com.a2zbill.domain.SuggestedContent;
import com.a2zbill.services.SuggestedContentService;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SuggestedContentServiceImpl implements SuggestedContentService {

  @Autowired private SuggestedContentDao suggestedContentDao;

  @Value("${key}")
  private String key;

  @Value("${SALT}")
  private String SALT;

  /*
   * @Value("${action1}") private String action1;
   */
  @Override
  public void save(final SuggestedContent suggestedContent) {
    suggestedContentDao.save(suggestedContent);
  }

  @Override
  public List<SuggestedContent> getAllSuggestedContent() {
    return suggestedContentDao.findAll();
  }

  public boolean empty(final String s) {
    if (s == null || s.trim().equals("")) {
      return true;
    } else {
      return false;
    }
  }

  public String hashCal(final String type, final String str) {
    final byte[] hashseq = str.getBytes();
    final StringBuffer hexString = new StringBuffer();
    try {
      final MessageDigest algorithm = MessageDigest.getInstance(type);
      algorithm.reset();
      algorithm.update(hashseq);
      final byte messageDigest[] = algorithm.digest();
      for (int i = 0; i < messageDigest.length; i++) {
        final String hex = Integer.toHexString(0xFF & messageDigest[i]);
        if (hex.length() == 1) {
          hexString.append("0");
        }
        hexString.append(hex);
      }

    } catch (final NoSuchAlgorithmException nsae) {
    }
    return hexString.toString();
  }

  public Map<String, String> hashCalMethod(
      final String txnId,
      final String amount,
      final String productinfo,
      final String firstName,
      final String email,
      final String mobileNumber,
      final String sucessUrl,
      final String failureUrl)
      throws ServletException, IOException {

    final Map<String, String> map = new HashMap<String, String>();
    String hashString = "";
    String action = "";
    final String base_url = "https://test.payu.in";
    String hash = "";

    action = base_url.concat("/_payment");
    hashString =
        key
            + "|"
            + txnId
            + "|"
            + amount
            + "|"
            + productinfo
            + "|"
            + firstName
            + "|"
            + email
            + "|||||||||||"
            + SALT;
    hash = hashCal("SHA-512", hashString);

    map.put("hash", hash);
    map.put("action", action);
    map.put("txnid", txnId);
    map.put("phone", mobileNumber);
    map.put("surl", sucessUrl);
    map.put("furl", failureUrl);
    map.put("hashString", hashString);
    map.put("key", key);
    map.put("amount", amount);
    map.put("productinfo", productinfo);
    map.put("firstname", firstName);
    map.put("email", email);
    map.put("SALT", SALT);

    return map;
  }
}
