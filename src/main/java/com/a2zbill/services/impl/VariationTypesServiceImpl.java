package com.a2zbill.services.impl;

import com.a2zbill.dao.VariationTypesDao;
import com.a2zbill.domain.VariationTypes;
import com.a2zbill.services.VariationTypesService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class VariationTypesServiceImpl implements VariationTypesService {

  @Autowired VariationTypesDao variationTypesDao;

  @Override
  public List<VariationTypes> getAllVariationTypesDetails() {
    return this.variationTypesDao.getAllVariationTypes();
  }

  @Override
  public VariationTypes getVariationTypesById(final long id) {
    return this.variationTypesDao.getVariationTypesById(id);
  }

  @Override
  public VariationTypes getVariationTypesByName(final String name) {
    return this.variationTypesDao.getVariationTypesByName(name);
  }

  @Override
  public List<VariationTypes> getOptionsByVariationType(final long variationTypeId) {
    return this.variationTypesDao.getOptionsByVariationType(variationTypeId);
  }
}
