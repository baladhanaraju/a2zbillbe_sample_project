package com.a2zbill.services.impl;

import com.a2zbill.dao.CounterDao;
import com.a2zbill.domain.Counter;
import com.a2zbill.services.CounterService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CounterServiceImpl implements CounterService {
  @Autowired CounterDao counterDao;

  @Override
  public void save(final Counter counter) {
    this.counterDao.save(counter);
  }

  @Override
  public boolean delete(final long id) {
    this.counterDao.delete(id);
    return true;
  }

  @Override
  public boolean update(final Counter counter) {
    this.counterDao.update(counter);
    return true;
  }

  @Override
  public List<Counter> getAllCounterDetails() {
    return this.counterDao.getAllCounterDetails();
  }

  @Override
  public List<Counter> getCounterDetailsOnStatus(final String counterStatus) {
    return this.counterDao.getCounterDetailsOnStatus(counterStatus);
  }

  @Override
  public Counter getCounterDetailsById(final long id) {
    return this.counterDao.getCounterDetailsById(id);
  }

  @Override
  public Counter getCounterDetailOnStatus(final String counterStatus) {
    return this.counterDao.getCounterDetailOnStatus(counterStatus);
  }

  @Override
  public List<Long> getAllCounterDetail(final long orgId, final long branchId) {
    return this.counterDao.getAllCounterDetail(orgId, branchId);
  }

  @Override
  public List<Counter> getCounterDetailOnStatusByOrgIdandBranchId(
      final long orgId, final long branchId, final String counterStatus) {
    return this.counterDao.getCounterDetailOnStatusByOrgIdandBranchId(
        orgId, branchId, counterStatus);
  }

  @Override
  public List<Counter> getAllCounterDetailsByOrgBranchId(final long orgId, final long branchId) {
    return this.counterDao.getAllCounterDetailsByOrgBranchId(orgId, branchId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgBranchIdBySectionId(
      final long orgId, final long branchId, final long sectionId) {
    return this.counterDao.getCounterDetailsByOrgBranchIdBySectionId(orgId, branchId, sectionId);
  }

  @Override
  public Counter getCounterDetailsBycounterNumber(
      final long orgId, final long branchId, final long counterNumber) {
    return this.counterDao.getCounterDetailsBycounterNumber(orgId, branchId, counterNumber);
  }

  @Override
  public List<Counter> getAllCounterDetailsByOrgId(final long orgId) {
    return this.counterDao.getAllCounterDetailsByOrgId(orgId);
  }

  @Override
  public List<Counter> getCounterDetailOnStatusByOrgId(
      final long orgId, final String counterStatus) {
    return this.counterDao.getCounterDetailOnStatusByOrgId(orgId, counterStatus);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdBySectionId(final long orgId, final long sectionId) {
    return this.counterDao.getCounterDetailsByOrgIdBySectionId(orgId, sectionId);
  }

  @Override
  public List<Counter> getCounterDetilsByOrgIdAndBranchId(final long orgId) {
    return counterDao.getCounterDetilsByOrgIdAndBranchId(orgId);
  }

  @Override
  public List<Counter> getCounterDetailsBasedOnOrgIdAndBranchId(
      final long orgId, final long branchId) {
    return counterDao.getCounterDetailsBasedOnOrgIdAndBranchId(orgId, branchId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdandStatusActive(final long orgId) {
    return this.counterDao.getCounterDetailsByOrgIdandStatusActive(orgId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdandbranchIdStatusActive(
      final long orgId, final long branchId) {
    return this.counterDao.getCounterDetailsByOrgIdandbranchIdStatusActive(orgId, branchId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdandStatusInActive(
      final long orgId, final long branchId) {
    return this.counterDao.getCounterDetailsByOrgIdBranchIdandStatusInActive(orgId, branchId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdandStatusInActive(final long orgId) {
    return this.counterDao.getCounterDetailsByOrgIdandStatusInActive(orgId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdandStatusActiveAndInActive(
      final long orgId, final long branchId) {
    return this.counterDao.getCounterDetailsByOrgIdBranchIdandStatusActiveAndInActive(
        orgId, branchId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdandStatusActiveAndInActive(final long orgId) {
    return this.counterDao.getCounterDetailsByOrgIdandStatusActiveAndInActive(orgId);
  }

  @Override
  public List<Counter> getAllCounterDetailByStatus(final long orgId, final long branchId) {
    return this.counterDao.getAllCounterDetailByStatus(orgId, branchId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgBranchIdBySectionAndStatus(
      final long orgId, final long branchId, final long sectionId) {
    return this.counterDao.getCounterDetailsByOrgBranchIdBySectionAndStatus(
        orgId, branchId, sectionId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdSectionIdandOnline(
      final long orgId, final long branchId, final long sectionId) {
    return this.counterDao.getCounterDetailsByOrgIdBranchIdSectionIdandOnline(
        orgId, branchId, sectionId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdSectionIdandOnline(
      final long orgId, final long sectionId) {

    return this.counterDao.getCounterDetailsByOrgIdSectionIdandOnline(orgId, sectionId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdSectionIdandtrue(final long orgId) {
    return this.counterDao.getCounterDetailsByOrgIdSectionIdandtrue(orgId);
  }

  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdSectionIdandtrue(
      final long orgId, final long branchId) {
    return this.counterDao.getCounterDetailsByOrgIdBranchIdSectionIdandtrue(orgId, branchId);
  }

  @Override
  public Counter getCounterDetailOnlineStatus(final boolean counterStatus) {
    return counterDao.getCounterDetailOnlineStatus(counterStatus);
  }
}
