package com.a2zbill.services.impl;

import com.a2zbill.dao.PaymentDao;
import com.a2zbill.domain.Payment;
import com.a2zbill.services.PaymentService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {
  @Autowired PaymentDao paymentDao;

  @Override
  public void save(final Payment payment) {
    this.paymentDao.save(payment);
  }

  @Override
  public List<Payment> getAllPayments() {
    return this.paymentDao.getAllPayments();
  }

  @Override
  public void savePaymentModes(final Payment payment) {
    this.paymentDao.savePaymentMode(payment);
  }

  @Override
  public Payment getPaymentType(final String paymentType) {
    return this.paymentDao.getPaymentType(paymentType);
  }

  @Override
  public List<Payment> getAllPaymentsDefaultTrue() {
    return this.paymentDao.getAllPaymentsDefaultTrue();
  }

  @Override
  public Payment getPaymentById(final long paymentId) {
    return this.paymentDao.getPaymentById(paymentId);
  }

  @Override
  public void update(final Payment payment) {
    this.paymentDao.update(payment);
  }
}
