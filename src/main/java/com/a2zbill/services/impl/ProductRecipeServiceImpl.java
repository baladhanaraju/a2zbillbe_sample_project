package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductRecipeDao;
import com.a2zbill.domain.ProductRecipe;
import com.a2zbill.services.ProductRecipeService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductRecipeServiceImpl implements ProductRecipeService {
  @Autowired ProductRecipeDao productRecipeDao;

  @Override
  public void save(final ProductRecipe productRecipe) {
    this.productRecipeDao.save(productRecipe);
  }

  @Override
  public boolean delete(final Long id) {
    return false;
  }

  @Override
  public boolean update(final ProductRecipe productRecipe) {
    productRecipeDao.update(productRecipe);
    return true;
  }

  @Override
  public List<ProductRecipe> getAllProductRecipeDetails() {
    return this.productRecipeDao.getAllProductRecipeDetails();
  }

  @Override
  public List<ProductRecipe> getAllProductRecipesByOrgAndBranch(
      final long orgId, final long branchId) {
    return this.productRecipeDao.getAllProductRecipesByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<ProductRecipe> getAllProductRecipesByOrg(final long orgId) {
    return this.productRecipeDao.getAllProductRecipesByOrg(orgId);
  }

  @Override
  public ProductRecipe getProductRecipesById(final long recipeId) {
    return this.productRecipeDao.getProductRecipesById(recipeId);
  }

  @Override
  public List<ProductRecipe> getAllProductRecipesByIngredientsId(final long ingredientsId) {
    return this.productRecipeDao.getAllProductRecipesByIngredientsId(ingredientsId);
  }

  @Override
  public List<ProductRecipe> getAllProductRecipesById(final long id) {
    return this.productRecipeDao.getAllProductRecipesById(id);
  }

  @Override
  public ProductRecipe getProductRecipesByRecipeName(final String recipeName) {
    return this.productRecipeDao.getProductRecipesByRecipeName(recipeName);
  }

  @Override
  public ProductRecipe getProductRecipesByRecipebyId(final long id) {
    return this.productRecipeDao.getProductRecipesById(id);
  }
}
