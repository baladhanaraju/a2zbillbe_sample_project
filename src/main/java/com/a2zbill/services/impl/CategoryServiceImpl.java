package com.a2zbill.services.impl;

import com.a2zbill.dao.CategoryDao;
import com.a2zbill.domain.Category;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryDao {
  @Autowired private CategoryDao categoryDao;

  @Override
  public void save(Category productCategory) {}

  @Override
  public boolean delete(Long id) {
    return false;
  }

  @Override
  public boolean update(Category productCategory) {
    return false;
  }

  @Override
  public List<Category> getAllCategories() {
    return this.categoryDao.getAllCategories();
  }

  @Override
  public Category getCategoryByName(final String categoryName) {
    return this.categoryDao.getCategoryByName(categoryName);
  }
}
