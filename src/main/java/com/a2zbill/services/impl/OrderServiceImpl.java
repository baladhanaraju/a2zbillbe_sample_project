package com.a2zbill.services.impl;

import com.a2zbill.dao.OrderDao;
import com.a2zbill.domain.Order;
import com.a2zbill.domain.OrderStatus;
import com.a2zbill.services.OrderService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

  @Autowired private OrderDao orderDao;

  @Override
  public void save(final Order order) {
    this.orderDao.save(order);
  }

  @Override
  public boolean delete(final long id) {
    this.orderDao.delete(id);
    return false;
  }

  @Override
  public boolean update(final Order order) {
    this.orderDao.update(order);
    return true;
  }

  @Override
  public List<Order> getAllOrderDetails() {
    return this.orderDao.getAllOrderDetails();
  }

  @Override
  public Order getOrderById(final long id) {
    return this.orderDao.getOrderById(id);
  }

  @Override
  public Order getOrderByGuid(final String guid) {
    return this.orderDao.getOrderByGuid(guid);
  }

  @Override
  public Order getOrdersDetailsByStatus(final String orderStatus, final long counterId) {
    return this.orderDao.getOrdersDetailsByStatus(orderStatus, counterId);
  }

  @Override
  public OrderStatus getOrderStatusById(long id) {
    return orderDao.getOrderStatusById(id);
  }
}
