package com.a2zbill.services.impl;

import com.a2zbill.dao.OrganisationFlowDao;
import com.a2zbill.domain.OrganisationFlow;
import com.a2zbill.services.OrganisationFlowService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrganisationFlowServiceImpl implements OrganisationFlowService {

  @Autowired private OrganisationFlowDao organisationFlowDao;

  @Override
  public void save(final OrganisationFlow organisationFlow) {
    this.organisationFlowDao.save(organisationFlow);
  }

  @Override
  public OrganisationFlow getOrganisationFlowById(final Long id) {
    return this.organisationFlowDao.getOrganisationFlowById(id);
  }

  @Override
  public List<OrganisationFlow> getOrganisationFlowByOrgId(final Long orgId) {
    return this.organisationFlowDao.getOrganisationFlowByOrgId(orgId);
  }
}
