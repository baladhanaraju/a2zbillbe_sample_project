package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductConsumptionDao;
import com.a2zbill.domain.ProductConsumption;
import com.a2zbill.services.ProductConsumptionService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductConsumptionServiceImpl implements ProductConsumptionService {

  @Autowired ProductConsumptionDao productConsumptionDao;

  @Override
  public void save(final ProductConsumption productConsumption) {
    productConsumptionDao.save(productConsumption);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final ProductConsumption productComposition) {
    productConsumptionDao.update(productComposition);
    return true;
  }

  @Override
  public List<ProductConsumption> getAllProductConsumption() {
    return productConsumptionDao.getAllProductConsumption();
  }

  @Override
  public ProductConsumption getProductConsumptionById(final long id) {
    return productConsumptionDao.getProductConsumptionById(id);
  }

  @Override
  public ProductConsumption getProductConsumptionByproductAndcurrentDate(
      final long productId, final Date date) {
    return productConsumptionDao.getProductConsumptionByproductAndcurrentDate(productId, date);
  }

  @Override
  public ProductConsumption getProductConsumptionByproductId(final long productId) {
    return productConsumptionDao.getProductConsumptionByproductId(productId);
  }

  @Override
  public ProductConsumption
      getProductConsumptionByproductAndmenuProductAndwastageFlagAnddateAndOrgAndBranch(
          final long productId,
          final long menuProductId,
          final boolean status,
          final Date date,
          final long orgId,
          final long branchId) {
    return productConsumptionDao
        .getProductConsumptionByproductAndmenuProductAndwastageFlagAnddateAndOrgAndBranch(
            productId, menuProductId, status, date, orgId, branchId);
  }

  @Override
  public ProductConsumption getProductConsumptionBymenuProductAndwastageFlagAnddateAndOrgAndBranch(
      final long menuProductId,
      final boolean status,
      final Date date,
      final long orgId,
      final long branchId) {
    return productConsumptionDao
        .getProductConsumptionBymenuProductAndwastageFlagAnddateAndOrgAndBranch(
            menuProductId, status, date, orgId, branchId);
  }
}
