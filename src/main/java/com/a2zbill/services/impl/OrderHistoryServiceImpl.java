package com.a2zbill.services.impl;

import com.a2zbill.dao.OrderHistoryDao;
import com.a2zbill.domain.OrderHistory;
import com.a2zbill.services.OrderHistoryService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OrderHistoryServiceImpl implements OrderHistoryService {

  @Autowired private OrderHistoryDao orderHistoryDao;

  @Override
  public void save(OrderHistory orderHistory) {
    orderHistoryDao.save(orderHistory);
  }

  @Override
  public boolean delete(long id) {
    this.orderHistoryDao.delete(id);
    return false;
  }

  @Override
  public boolean update(OrderHistory orderHistory) {
    this.orderHistoryDao.update(orderHistory);
    return true;
  }

  @Override
  public List<OrderHistory> getAllOrderHistoryDetails() {

    return orderHistoryDao.getAllOrderHistoryDetails();
  }

  @Override
  public OrderHistory getOrderHistoryById(long id) {

    return orderHistoryDao.getOrderHistoryById(id);
  }
}
