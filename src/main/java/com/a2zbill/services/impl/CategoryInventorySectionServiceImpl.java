package com.a2zbill.services.impl;

import com.a2zbill.dao.CategoryInventorySectionDao;
import com.a2zbill.domain.CategoryInventorySection;
import com.a2zbill.services.CategoryInventorySectionService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CategoryInventorySectionServiceImpl implements CategoryInventorySectionService {
  @Autowired private CategoryInventorySectionDao categoryInventorySectionDao;

  @Override
  public void save(final CategoryInventorySection categoryInventorySection) {
    this.categoryInventorySectionDao.save(categoryInventorySection);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final CategoryInventorySection categoryInventorySection) {
    return false;
  }

  @Override
  public List<CategoryInventorySection> getAllCategoryInventorySection() {
    return this.categoryInventorySectionDao.getAllCategoryInventorySection();
  }

  @Override
  public CategoryInventorySection getCategoryInventorySectionByCategoryIdAndInventorySectionId(
      final long categoryId, final long inventorySectionId) {
    return this.categoryInventorySectionDao
        .getCategoryInventorySectionByCategoryIdAndInventorySectionId(
            categoryId, inventorySectionId);
  }
}
