package com.a2zbill.services.impl;

import com.a2zbill.dao.InvoiceDetailsDao;
import com.a2zbill.domain.InvoiceDetails;
import com.a2zbill.services.InvoiceDetailsService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InvoiceDetailsServiceImpl implements InvoiceDetailsService {
  @Autowired InvoiceDetailsDao invoiceDetailsDao;

  @Override
  public void save(final InvoiceDetails invoiceDetails) {
    this.invoiceDetailsDao.save(invoiceDetails);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final InvoiceDetails invoiceDetails) {
    return false;
  }

  @Override
  public List<InvoiceDetails> getAllInvoiceDetails() {
    return this.invoiceDetailsDao.getAllInvoiceDetails();
  }

  @Override
  public InvoiceDetails getInvoiceDetailsByVendorId(final long vendorId) {
    return this.invoiceDetailsDao.getInvoiceDetailsByVendorId(vendorId);
  }

  @Override
  public List<InvoiceDetails> getAllInvoiceDetailsByOrgAndBranch(
      final long orgId, final long branchId) {
    return this.invoiceDetailsDao.getAllInvoiceDetailsByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<InvoiceDetails> getAllInvoiceDetailsByOrg(final long orgId) {
    return this.invoiceDetailsDao.getAllInvoiceDetailsByOrg(orgId);
  }

  @Override
  public InvoiceDetails getInvoiceDetailsByInvoiceId(final long invoiceId) {
    return this.invoiceDetailsDao.getInvoiceDetailsByInvoiceId(invoiceId);
  }

  @Override
  public List<Object[]> getInvoiceDetailsByVendor(final long vendorId, final long orgId) {
    return this.invoiceDetailsDao.getInvoiceDetailsByVendor(vendorId, orgId);
  }
}
