package com.a2zbill.services.impl;

import com.a2zbill.dao.CartDao;
import com.a2zbill.domain.Cart;
import com.a2zbill.services.CartService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CartServiceImpl implements CartService {
  @Autowired private CartDao cartDao;

  @Override
  public void save(final Cart cart) {
    this.cartDao.save(cart);
  }

  @Override
  public boolean delete(final Long id) {
    return false;
  }

  @Override
  public boolean update(final Cart cart) {
    return false;
  }

  @Override
  public List<Cart> getAllCartDetails() {
    return this.cartDao.getAllCartDetails();
  }

  @Override
  public Cart getCartDetailsOnStatus(final String cartStatus, final long counterId) {
    return this.cartDao.getCartDetailsOnStatus(cartStatus, counterId);
  }

  @Override
  public List<Cart> getCartDetailsByCounterId(final long counterId) {
    return this.cartDao.getCartDetailsByCounterId(counterId);
  }

  @Override
  public Cart getCartDetailsByCartId(final long cartId) {
    return this.cartDao.getCartDetailsByCartId(cartId);
  }

  @Override
  public List<Cart> getCartDetailsByStatus() {
    return this.cartDao.getCartDetailsByStatus();
  }

  @Override
  public Cart getCartByGuid(final String guid) {
    return this.cartDao.getCartByGuid(guid);
  }

  @Override
  public Cart getCartDetailsByStatus(String cartStatus, long customerId, long orgId) {
    return this.cartDao.getCartDetailsByStatus(cartStatus, customerId, orgId);
  }
}
