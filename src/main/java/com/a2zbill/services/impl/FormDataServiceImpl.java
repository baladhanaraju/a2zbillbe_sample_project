package com.a2zbill.services.impl;

import com.a2zbill.dao.FormDataDao;
import com.a2zbill.domain.FormData;
import com.a2zbill.services.FormDataService;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FormDataServiceImpl implements FormDataService {
  @SuppressWarnings("unused")
  private static final Logger LOGGER = LoggerFactory.getLogger(FormDataServiceImpl.class);

  @Autowired private FormDataDao formDataDao;

  @Override
  public void save(final FormData formData) {
    this.formDataDao.save(formData);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final FormData formData) {
    return false;
  }

  @Override
  public List<FormData> getAllFormData() {
    return this.formDataDao.getAllFormData();
  }

  @Override
  public List<FormData> getFormDataByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {
    return this.formDataDao.getFormDataByBranchAndOrg(formDate, endDate, orgId, branchId);
  }

  @Override
  public List<FormData> getFormDataByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {
    return this.formDataDao.getFormDataByOrgAndBranchisNull(formDate, endDate, orgId);
  }

  @Override
  public Long getFormDataByOrgandBranch(final long orgId, final long branchId) {
    return this.formDataDao.getFormDataByOrgandBranch(orgId, branchId);
  }
}
