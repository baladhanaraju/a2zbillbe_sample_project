package com.a2zbill.services.impl;

import com.a2zbill.dao.RoleserviceDao;
import com.a2zbill.services.RoleserviceService;
import com.tsss.basic.domain.Roles;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleserviceServiceImpl implements RoleserviceService {

  @Autowired RoleserviceDao roleserviceDao;

  @Override
  public void save(final Roles role) {}

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final Roles role) {
    return false;
  }

  @Override
  public List<Roles> getAllRolesServiceDetails() {
    return this.roleserviceDao.getAllRolesServiceDetails();
  }
}
