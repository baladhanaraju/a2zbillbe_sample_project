package com.a2zbill.services.impl;

import com.a2zbill.dao.ItemDao;
import com.a2zbill.domain.Item;
import com.a2zbill.services.ItemService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {
  @Autowired ItemDao itemsDao;

  @Override
  public void save(Item items) {}

  @Override
  public boolean delete(Long id) {
    return false;
  }

  @Override
  public boolean update(Item items) {
    return false;
  }

  @Override
  public Item getItemsById(final Long id) {
    return null;
  }

  @Override
  public List<Item> getAllItemsName() {
    return this.itemsDao.getAllItemsName();
  }

  @Override
  public Item getItemsByHsnNo(final String hsnNo) {
    return this.itemsDao.getItemsByHsnNo(hsnNo);
  }

  @Override
  public Item getItemsByName(final String commodity) {
    return this.itemsDao.getItemsByName(commodity);
  }

  @Override
  public List<Item> getAllItemsByHsnNo() {
    return this.itemsDao.getAllItemsByHsnNo();
  }
}
