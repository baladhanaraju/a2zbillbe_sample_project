package com.a2zbill.services.impl;

import com.a2zbill.dao.OrderFlowDao;
import com.a2zbill.domain.OrderFlow;
import com.a2zbill.services.OrderFlowService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrderFlowServiceImpl implements OrderFlowService {
  @Autowired OrderFlowDao orderFlowDao;

  @Override
  public void save(final OrderFlow orderFlow) {
    this.orderFlowDao.save(orderFlow);
  }

  @Override
  public boolean delete(final Long id) {
    return this.orderFlowDao.delete(id);
  }

  @Override
  public boolean update(final OrderFlow orderFlow) {
    this.orderFlowDao.update(orderFlow);
    return true;
  }

  @Override
  public OrderFlow getOrderFlowById(final Long id) {
    return this.orderFlowDao.getOrderFlowById(id);
  }

  @Override
  public OrderFlow getOrderFlowByCartId(final Long cartId) {
    return null;
  }

  @Override
  public List<OrderFlow> getAllOrderFlow() {
    return this.orderFlowDao.getAllOrderFlow();
  }

  @Override
  public OrderFlow getOrderFlowByCartIdAndProductIdAndFlowId(
      final Long cartId, final Long productId) {
    return this.orderFlowDao.getOrderFlowByCartIdAndProductIdAndFlowId(cartId, productId);
  }

  @Override
  public OrderFlow getOrderFlowByCartIdAndProductIdAndCartIdByServiceSection(
      final Long cartId, final Long productId) {
    return this.orderFlowDao.getOrderFlowByCartIdAndProductIdAndCartIdByServiceSection(
        cartId, productId);
  }

  @Override
  public List<OrderFlow> getOrderFlowByFlowId() {
    return this.orderFlowDao.getOrderFlowByFlowId();
  }

  @Override
  public List<OrderFlow> getOrderFlowByCounterIdAndFlowId() {
    return this.orderFlowDao.getOrderFlowByCounterIdAndFlowId();
  }
}
