package com.a2zbill.services.impl;

import com.a2zbill.dao.ReOrderBranchDao;
import com.a2zbill.domain.ReOrderBranch;
import com.a2zbill.services.ReOrderBranchService;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ReOrderBranchServiceImpl implements ReOrderBranchService {

  @Autowired ReOrderBranchDao reOrderBranchDao;

  @Override
  public void save(ReOrderBranch reOrderBranch) {
    reOrderBranchDao.save(reOrderBranch);
  }
}
