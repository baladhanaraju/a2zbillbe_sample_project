package com.a2zbill.services.impl;

import com.a2zbill.controller.KotStatusController;
import com.a2zbill.dao.KotStatusDao;
import com.a2zbill.domain.KotHistory;
import com.a2zbill.domain.KotStatus;
import com.a2zbill.services.KotHistoryService;
import com.a2zbill.services.KotStatusService;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Notification;
import com.tsss.basic.service.NotificationService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.transaction.Transactional;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class KotStatusServiceImpl implements KotStatusService {

  private static final Logger LOGGER = LoggerFactory.getLogger(KotStatusController.class);

  @Autowired private KotStatusDao kotStatusDao;

  @Autowired private NotificationService notificationService;

  @Autowired private KotHistoryService kotHistoryService;

  @Override
  public void save(final KotStatus kotStatus) {
    this.kotStatusDao.saveKotStatus(kotStatus);
  }

  @Override
  public void update(final KotStatus kotStatus) {
    this.kotStatusDao.update(kotStatus);
  }

  @Override
  public List<KotStatus> getAllKotStatus() {
    return this.kotStatusDao.getAllKotStatus();
  }

  @Override
  public KotStatus getKotStatusById(final long id) {
    return this.kotStatusDao.getKotStatusById(id);
  }

  @Override
  public KotStatus getKotStatusByBranchId(
      final long branchId, final String productName, final int quantity, final long counterNumber) {
    return this.kotStatusDao.getKotStatusByBranchId(branchId, productName, quantity, counterNumber);
  }

  @Override
  public void delete(final Long id) {
    this.kotStatusDao.delete(id);
  }

  public Long savingKotDetails(final String message) {

    final String[] data = message.split("\\|");
    LOGGER.error("branchId:" + data[0]);
    LOGGER.error("quantity:" + data[2]);
    final KotHistory kotHistory = new KotHistory();
    final KotStatus kotStatus = new KotStatus();
    kotStatus.setProductName(data[1]);
    kotStatus.setQuantity(Integer.parseInt(data[2]));
    kotStatus.setCounterNumber(Long.parseLong(data[3]));
    kotStatus.setBranchId(Long.parseLong(data[0]));
    kotStatus.setEmpId(Long.parseLong(data[4]));
    kotStatus.setProductImage(data[5]);
    save(kotStatus);
    kotHistory.setProductName(data[1]);
    kotHistory.setQuantity(Integer.parseInt(data[2]));
    kotHistory.setCounterNumber(Long.parseLong(data[3]));
    kotHistory.setBranchId(Long.parseLong(data[0]));
    kotHistory.setSection("kitchen");
    kotHistory.setKotStatusId(kotStatus.getId());
    kotHistory.setEmpId(Long.parseLong(data[4]));
    kotHistory.setProductImage(data[5]);

    this.kotHistoryService.save(kotHistory);

    return kotStatus.getId();
  }

  public void gettingDeatilsWithServiceSection(final String payload, final Employee employee)
      throws JSONException {

    final KotHistory kotHistory = new KotHistory();
    final JSONArray jsonArray = new JSONArray(payload);
    try {
      for (int i = 0; i < jsonArray.length(); i++) {
        final String message = ((JSONObject) jsonArray.get(i)).getString("message");
        LOGGER.info(message);
        String[] data = message.split("\\|");
        kotHistory.setProductName(data[1]);
        kotHistory.setQuantity(Integer.parseInt(data[2]));
        kotHistory.setCounterNumber(Long.parseLong(data[3]));
        kotHistory.setBranchId(Long.parseLong(data[0]));
        kotHistory.setSection("service");
        kotHistory.setKotStatusId(Long.parseLong(data[6]));
        kotHistory.setEmpId(Long.parseLong(data[4]));
        kotHistory.setProductImage(data[5]);
        this.kotHistoryService.save(kotHistory);
        delete(Long.parseLong(data[6]));
        final List<Notification> notification =
            this.notificationService.getNotifivcationByEmpId(Long.parseLong(data[4]));
        sendPushMessage(message, notification);
      }
    } catch (final Exception ex) {
      LOGGER.error("gettingDeatilsWithServiceSection:" + ex, ex);
    }
  }

  void sendPushMessage(final String message, final List<Notification> token)
      throws InterruptedException, ExecutionException, FirebaseMessagingException {

    final Map<String, String> data = new HashMap<>();
    data.put("data", message);
    System.out.println("Sending chuck joke...");
  }

  @Override
  public List<KotStatus> getKotStatusByCounterNumber(final long counterNumber) {
    return this.kotStatusDao.getKotStatusByCounterNumber(counterNumber);
  }

  @Override
  public List<KotStatus> getKotStatusByCounterNumber(
      final long counterNumber, final long branchId) {
    return this.kotStatusDao.getKotStatusByCounterNumber(counterNumber, branchId);
  }
}
