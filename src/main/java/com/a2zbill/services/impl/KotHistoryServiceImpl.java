package com.a2zbill.services.impl;

import com.a2zbill.dao.KotHistoryDao;
import com.a2zbill.domain.KotHistory;
import com.a2zbill.services.KotHistoryService;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class KotHistoryServiceImpl implements KotHistoryService {
  @Autowired KotHistoryDao kotHistoryDao;

  @Override
  public void save(KotHistory kotHistory) {
    this.kotHistoryDao.save(kotHistory);
  }

  @Override
  public void update(KotHistory kotHistory) {
    this.kotHistoryDao.update(kotHistory);
  }
}
