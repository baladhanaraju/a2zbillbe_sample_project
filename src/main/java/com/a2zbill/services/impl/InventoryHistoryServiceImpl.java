package com.a2zbill.services.impl;

import com.a2zbill.dao.InventoryHistoryDao;
import com.a2zbill.domain.InventoryHistory;
import com.a2zbill.services.InventoryHistoryService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventoryHistoryServiceImpl implements InventoryHistoryService {
  @Autowired InventoryHistoryDao inventoryHistoryDao;

  @Override
  public void save(final InventoryHistory inventoryHistory) {
    this.inventoryHistoryDao.save(inventoryHistory);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final InventoryHistory inventoryHistory) {
    return this.inventoryHistoryDao.update(inventoryHistory);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistorys() {
    return this.inventoryHistoryDao.getAllInventoryHistorys();
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistorys(final long branchId, final long orgId) {
    return this.inventoryHistoryDao.getAllInventoryHistorys(branchId, orgId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistorysByOrg(final long orgId) {
    return this.inventoryHistoryDao.getAllInventoryHistorysByOrg(orgId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndVendorBetweenTwoDates(
      final Date fromDate, final Date toDate, final long orgId, final long vendorId) {
    return this.inventoryHistoryDao.getAllInventoryHistoryDetailsByOrgAndVendorBetweenTwoDates(
        fromDate, toDate, orgId, vendorId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBranchAndVendorBetweenTwoDates(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long vendorId) {
    return this.inventoryHistoryDao
        .getAllInventoryHistoryDetailsByOrgAndBranchAndVendorBetweenTwoDates(
            fromDate, toDate, orgId, branchId, vendorId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBranchAndBetweenTwoDates(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {
    return this.inventoryHistoryDao.getAllInventoryHistoryDetailsByOrgAndBranchAndBetweenTwoDates(
        fromDate, toDate, orgId, branchId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBetweenTwoDates(
      final Date fromDate, final Date toDate, final long orgId) {
    return this.inventoryHistoryDao.getAllInventoryHistoryDetailsByOrgAndBetweenTwoDates(
        fromDate, toDate, orgId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistorysByInventoryId(
      final long branchId, final long orgId, final long inventoryId) {

    return this.inventoryHistoryDao.getAllInventoryHistorysByInventoryId(
        branchId, orgId, inventoryId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistorysByInventoryId(
      final long branchId, final long orgId) {
    return this.inventoryHistoryDao.getAllInventoryHistorysByInventoryId(branchId, orgId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByInvoiceBillId(
      final long branchId, final long orgId, final long invoiceId) {
    return inventoryHistoryDao.getAllInventoryHistoryDetailsByInvoiceBillId(
        branchId, orgId, invoiceId);
  }

  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByInvoiceBillId(
      final long orgId, final long invoiceId) {
    return inventoryHistoryDao.getAllInventoryHistoryDetailsByInvoiceBillId(orgId, invoiceId);
  }

  @Override
  public List<InventoryHistory>
      getAllInventoryHistoryDetailsByInventoryIdAndOrgAndBranchAndBetweenTwoDates(
          final long inventoryId,
          final long orgId,
          final long branchId,
          final Date fromDate,
          final Date toDate) {
    return inventoryHistoryDao
        .getAllInventoryHistoryDetailsByInventoryIdAndOrgAndBranchAndBetweenTwoDates(
            inventoryId, orgId, branchId, fromDate, toDate);
  }

  @Override
  public String getMaxCountByBatchNumberAndInventoryIdAndOrgAndBranch(
      final String batchNumber, final long inventoryId, final long orgId, final long branchId) {
    return inventoryHistoryDao.getMaxCountByBatchNumberAndInventoryIdAndOrgAndBranch(
        batchNumber, inventoryId, orgId, branchId);
  }

  @Override
  public InventoryHistory getInventoryHisotryDetailsByBatchNumberAndInventoryIdAndOrgAndBranch(
      final String batchNumber, final long inventoryId, final long orgId, final long branchId) {
    return inventoryHistoryDao.getInventoryHisotryDetailsByBatchNumberAndInventoryIdAndOrgAndBranch(
        batchNumber, inventoryId, orgId, branchId);
  }

  @Override
  public List<InventoryHistory>
      getInventoryHsitoryDetailsByExpiryDateAndProductIdAndOrgIdAndBranchIdOrderByAvailableQuantity(
          final Date expiryDate, final long productId, final long orgId, final long branchId) {
    return inventoryHistoryDao
        .getInventoryHsitoryDetailsByExpiryDateAndProductIdAndOrgIdAndBranchIdOrderByAvailableQuantity(
            expiryDate, productId, orgId, branchId);
  }

  @Override
  public List<InventoryHistory>
      getInventoryHistoryDetailsByProductIdAndOrgIdAndBranchIdOrderByExpiryDate(
          final long productId, final long orgId, final long branchId) {
    return inventoryHistoryDao
        .getInventoryHistoryDetailsByProductIdAndOrgIdAndBranchIdOrderByExpiryDate(
            productId, orgId, branchId);
  }

  @Override
  public List<Object[]> getAvailableQuantityWithExpiryDateByProductIdAndOrgIdAndBrnachId(
      final long productId, final long orgId, final long branchId) {
    return inventoryHistoryDao.getAvailableQuantityWithExpiryDateByProductIdAndOrgIdAndBrnachId(
        productId, orgId, branchId);
  }

  @Override
  public List<InventoryHistory> getInventoryHisotryDetailsByDatesAndProductIdAndOrgIdAndBranchId(
      final Date fromDate,
      final Date toDate,
      final long productId,
      final long orgId,
      final long branchId) {
    return inventoryHistoryDao.getInventoryHisotryDetailsByDatesAndProductIdAndOrgIdAndBranchId(
        fromDate, toDate, productId, orgId, branchId);
  }

  @Override
  public List<InventoryHistory>
      getInventoryHistoryDetailsByDateAndVendorIdAndProductIdAndOrgIdAndBranchId(
          final Date fromDate,
          final Date toDate,
          final long vendorId,
          final long productId,
          final long orgId,
          final long branchId) {
    return inventoryHistoryDao
        .getInventoryHistoryDetailsByDateAndVendorIdAndProductIdAndOrgIdAndBranchId(
            fromDate, toDate, vendorId, productId, orgId, branchId);
  }
}
