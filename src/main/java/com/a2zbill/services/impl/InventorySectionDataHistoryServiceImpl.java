package com.a2zbill.services.impl;

import com.a2zbill.dao.InventorySectionDataHistoryDao;
import com.a2zbill.domain.InventorySectionDataHistory;
import com.a2zbill.services.InventorySectionDataHistoryService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventorySectionDataHistoryServiceImpl implements InventorySectionDataHistoryService {
  @Autowired InventorySectionDataHistoryDao inventorySectionDataHistoryDao;

  @Override
  public void save(final InventorySectionDataHistory inventorySectionDataHistory) {
    this.inventorySectionDataHistoryDao.save(inventorySectionDataHistory);
  }

  @Override
  public boolean delete(final long id) {
    return this.inventorySectionDataHistoryDao.delete(id);
  }

  @Override
  public boolean update(final InventorySectionDataHistory inventorySectionDataHistory) {
    return this.inventorySectionDataHistoryDao.update(inventorySectionDataHistory);
  }

  @Override
  public List<InventorySectionDataHistory> getAllInventorySectionDataHistory() {
    return this.inventorySectionDataHistoryDao.getAllInventorySectionDataHistory();
  }

  @Override
  public InventorySectionDataHistory getInventorySectionDataHistoryByInventorySectionId(
      final long inventorySectionId) {
    return null;
  }

  @Override
  public InventorySectionDataHistory getInventorySectionDataHistoryByproductIdAndOrgIdBranchId(
      final long productId, final long orgId, final long branchId) {
    return null;
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByOrgIdBranchId(
      final long orgId, final long branchId) {
    return null;
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByItemWiseOrgIdBranchId(
      final long orgId, final long branchId, final Date date) {
    return this.inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByItemWiseOrgIdBranchId(orgId, branchId, date);
  }

  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseOrgIdBranchIdByDateWise(
          final long orgId, final long branchId, final Date fromdate, final Date todate) {
    return this.inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByItemWiseOrgIdBranchIdByDateWise(
            orgId, branchId, fromdate, todate);
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByItemWiseOrgIdByDateWise(
      final long orgId, final Date fromDate, final Date toDate) {
    return this.inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByItemWiseOrgIdByDateWise(orgId, fromDate, toDate);
  }

  @Override
  public List<Object[]> getSumOfInventoryHistoryDetailsByOrgBranch(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {
    return inventorySectionDataHistoryDao.getSumOfInventoryHistoryDetailsByOrgBranch(
        formDate, endDate, orgId, branchId);
  }

  @Override
  public List<Object[]> getSumOfInventoryHistoryDetailsByOrgBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {
    return inventorySectionDataHistoryDao.getSumOfInventoryHistoryDetailsByOrgBranchisNull(
        formDate, endDate, orgId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSectionId(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long sectionId,
      final long fromSectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSectionId(
            orgId, branchId, fromdate, todate, sectionId, fromSectionId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByBillIdAndDates(
      final Date formDate,
      final Date endDate,
      final String referenceNumber,
      final long branchId,
      final long orgId,
      final long sectionId,
      final long toSectionId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByBillIdAndDates(
        formDate, endDate, referenceNumber, branchId, orgId, sectionId, toSectionId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryTotalAmt(
      final Date formDate,
      final Date endDate,
      final long branchId,
      final long orgId,
      final long sectionId,
      final long toSectionId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryTotalAmt(
        formDate, endDate, branchId, orgId, sectionId, toSectionId);
  }

  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSumOfQuantity(
          final long orgId, final long branchId, final Date fromdate, final Date todate) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSumOfQuantity(
            orgId, branchId, fromdate, todate);
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryBySection(
      final long orgId, final long branchId, final long sectionId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryBySection(
        orgId, branchId, sectionId);
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByVendorIdBranchId(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long sectionId,
      final long vendorId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByVendorIdBranchId(
        orgId, branchId, fromdate, todate, sectionId, vendorId);
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByBranchId(
      final long orgId,
      final long branchId,
      final Date fromDate,
      final Date toDate,
      final long sectionId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByBranchId(
        orgId, branchId, fromDate, toDate, sectionId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdFromBranchId(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long sectionId,
      final long fromBranchId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByDepartmentWiseOrgIdFromBranchId(
            orgId, branchId, fromdate, todate, sectionId, fromBranchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByTotalAmt(
      final Date formDate,
      final Date endDate,
      final long fromSectionId,
      final long orgId,
      final long branchId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByTotalAmt(
        formDate, endDate, fromSectionId, orgId, branchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchId(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long fromBranchId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchId(
            orgId, branchId, fromdate, todate, fromBranchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdAndSectionId(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long sectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdAndSectionId(
            orgId, branchId, fromdate, todate, sectionId);
  }

  @Override
  public List<Object[]>
      getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromBranchId(
          final Date fromDate,
          final Date endDate,
          final long orgId,
          final long sectionId,
          final long fromBranchId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromBranchId(
            fromDate, endDate, orgId, sectionId, fromBranchId);
  }

  @Override
  public List<Object[]>
      getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromSectionId(
          final Date fromDate,
          final Date endDate,
          final long orgId,
          final long sectionId,
          final long fromSectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromSectionId(
            fromDate, endDate, orgId, sectionId, fromSectionId);
  }

  @Override
  public Object[]
      getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromBranchIdAndReferenceNumber(
          final Date fromDate,
          final Date endDate,
          final long orgId,
          final long sectionId,
          final long fromBranchId,
          final String referenceNumber) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromBranchIdAndReferenceNumber(
            fromDate, endDate, orgId, sectionId, fromBranchId, referenceNumber);
  }

  @Override
  public Object[]
      getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromSectionIdAndReferenceNumber(
          final Date fromDate,
          final Date endDate,
          final long orgId,
          final long sectionId,
          final long fromSectionId,
          final String referenceNumber) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromSectionIdAndReferenceNumber(
            fromDate, endDate, orgId, sectionId, fromSectionId, referenceNumber);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByOrgIdFromBranchId(
      final Date fromdate, final Date todate, final long orgId, final long branchId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByOrgIdFromBranchId(
        fromdate, todate, orgId, branchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByOrgIdBranchIdSectionId(
      final Date fromdate,
      final Date todate,
      final long sectionId,
      final long fromSectionId,
      final long orgId,
      final long branchId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByOrgIdBranchIdSectionId(
        fromdate, todate, sectionId, fromSectionId, orgId, branchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByOrgIdBranchIdSection(
      final Date fromdate,
      final Date todate,
      final long sectionId,
      final long frombranchId,
      final long orgId,
      final long branchId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByOrgIdBranchIdSection(
        fromdate, todate, sectionId, frombranchId, orgId, branchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByTransferOrgIdFromBranchId(
      final Date fromdate, final Date todate, final long orgId, final long branchId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByTransferOrgIdFromBranchId(
        fromdate, todate, orgId, branchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByTransferOrgIdBranchIdSection(
      final Date fromdate,
      final Date todate,
      final long sectionId,
      final long frombranchId,
      final long orgId,
      final long branchId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByTransferOrgIdBranchIdSection(
            fromdate, todate, sectionId, frombranchId, orgId, branchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByTransferOrgIdBranchIdSectionId(
      final Date fromdate,
      final Date todate,
      final long sectionId,
      final long fromSectionId,
      final long orgId,
      final long branchId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByTransferOrgIdBranchIdSectionId(
            fromdate, todate, sectionId, fromSectionId, orgId, branchId);
  }

  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseConsolidatedOrgIdBranchIdAndSectionId(
          final long orgId,
          final long branchId,
          final Date fromdate,
          final Date todate,
          final long fromSectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByItemWiseConsolidatedOrgIdBranchIdAndSectionId(
            orgId, branchId, fromdate, todate, fromSectionId);
  }

  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseOrgIdBranchIdAndFromSectionId(
          final long orgId, final Date fromdate, final Date todate, final long fromBranchId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByItemWiseOrgIdBranchIdAndFromSectionId(
            orgId, fromdate, todate, fromBranchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryQuantityByProductAndFromToSection(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long fromsectionId,
      final long toSectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryQuantityByProductAndFromToSection(
            orgId, branchId, fromdate, todate, fromsectionId, toSectionId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryQuantityByProductAndFromBranchToSection(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long fromBranchId,
      final long toSectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryQuantityByProductAndFromBranchToSection(
            orgId, branchId, fromdate, todate, fromBranchId, toSectionId);
  }

  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromBranchId(
          final Date fromDate,
          final Date endDate,
          final long orgId,
          final long sectionId,
          final long fromBranchId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromBranchId(
            fromDate, endDate, orgId, sectionId, fromBranchId);
  }

  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromSectionId(
          final Date fromDate,
          final Date endDate,
          final long orgId,
          final long sectionId,
          final long fromSectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromSectionId(
            fromDate, endDate, orgId, sectionId, fromSectionId);
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByOrgIdAndReferenceNumber(
      final long orgId, final String referenceNumber) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByOrgIdAndReferenceNumber(
        orgId, referenceNumber);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByBranch(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long fromBranchId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByBranch(
        orgId, branchId, fromdate, todate, fromBranchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByBranchReference(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long fromBranchId,
      final String reference) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByBranchReference(
        orgId, branchId, fromdate, todate, fromBranchId, reference);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByTotalAmountBranch(
      final long orgId, final long branchId, final Date fromdate, final Date todate) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryByTotalAmountBranch(
        orgId, branchId, fromdate, todate);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryTotalAmtToSection(
      final Date formDate,
      final Date endDate,
      final long branchId,
      final long orgId,
      final long sectionId,
      final long fromBranchId) {
    return inventorySectionDataHistoryDao.getInventorySectionDataHistoryTotalAmtToSection(
        formDate, endDate, branchId, orgId, sectionId, fromBranchId);
  }

  @Override
  public List<Object[]> getInventorySectionDataHistoryByBillIdAndDatesToSectionReference(
      final Date formDate,
      final Date endDate,
      final String referenceNumber,
      final long branchId,
      final long orgId,
      final long fromBranchId,
      final long toSectionId) {
    return inventorySectionDataHistoryDao
        .getInventorySectionDataHistoryByBillIdAndDatesToSectionReference(
            formDate, endDate, referenceNumber, branchId, orgId, fromBranchId, toSectionId);
  }
}
