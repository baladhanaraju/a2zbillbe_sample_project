package com.a2zbill.services.impl;

import com.a2zbill.dao.RatingDao;
import com.a2zbill.domain.Rating;
import com.a2zbill.services.RatingService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RatingServiceImpl implements RatingService {
  @Autowired RatingDao ratingDao;

  @Override
  public void save(Rating rating) {
    this.ratingDao.save(rating);
  }

  @Override
  public boolean delete(Long id) {
    return this.ratingDao.delete(id);
  }

  @Override
  public boolean update(Rating rating) {
    return this.ratingDao.update(rating);
  }

  @Override
  public List<Rating> getAllRatingDetails() {
    return this.ratingDao.getAllRatingDetails();
  }

  @Override
  public Rating getRatingByRatingId(Long ratingId) {
    return this.ratingDao.getRatingByRatingId(ratingId);
  }

  @Override
  public Rating getRatingByProductId(Long productId) {
    return this.ratingDao.getRatingByProductId(productId);
  }

  @Override
  public Rating getRatingByEmpIdAndProductId(Long productId, long empId) {
    return this.ratingDao.getRatingByEmpIdAndProductId(productId, empId);
  }
}
