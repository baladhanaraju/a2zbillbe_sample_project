package com.a2zbill.services.impl;

import com.a2zbill.dao.CustomerHistoryDao;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.CustomerHistory;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CustomerHistoryService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerHistoryServiceImpl implements CustomerHistoryService {
  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerHistoryServiceImpl.class);
  @Autowired CustomerHistoryDao customerHistoryDao;
  @Autowired BranchService branchService;
  @Autowired CustomerService customerService;
  @Autowired OrganisationService organisationService;
  @Autowired EmployeeService employeeService;
  @Autowired BillingService billingService;

  @Override
  public void save(CustomerHistory customerHistory) {
    this.customerHistoryDao.save(customerHistory);
  }

  @Override
  public boolean delete(long id) {
    return false;
  }

  @Override
  public boolean update(CustomerHistory customerHistory) {
    return this.customerHistoryDao.update(customerHistory);
  }

  public void savingCustomerHistoryDetails(long employeeNumber, String message, Billing billing)
      throws JSONException {
    /*String authToken = request.getHeader(this.tokenHeader);
    final String token = authToken.substring(7);
    String emailAddress = this.jwtTokenUtil.getUsernameFromToken(token);
    JwtUser user = (JwtUser) this.userDetailsService.loadUserByUsername(emailAddress);
    Employee employeeDetails = this.employeeService.getEmployeeDetailsByEmail(user.getEmailAddress());*/
    Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    JSONObject customerHistoryDetails = new JSONObject(message);
    CustomerHistory customerHistory = new CustomerHistory();
    try {
      Branch branch = this.branchService.getBranchById(employeeDetails.getBranch().getId());
      customerHistory.setBranch(branch);
      Organisation organisation =
          this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
      customerHistory.setOrganisation(organisation);
      customerHistory.setCustomer(billing.getCustomer());
      customerHistory.setCreditAmount(new BigDecimal(customerHistoryDetails.getInt("debitAmount")));
      customerHistory.setDebitAmount(new BigDecimal(customerHistoryDetails.getInt("debitAmount")));
      customerHistory.setDate(new Date(System.currentTimeMillis()));
      save(customerHistory);
    } catch (Exception ex) {
      LOGGER.error("savingCustomerHistoryDetails" + ex, ex);
    }
  }
}
