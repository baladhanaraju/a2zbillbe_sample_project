package com.a2zbill.services.impl;

import com.a2zbill.dao.FlowDao;
import com.a2zbill.domain.Flow;
import com.a2zbill.services.FlowService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FlowServicesImpl implements FlowService {
  @Autowired private FlowDao flowDao;

  @Override
  public void save(final Flow flow) {
    this.flowDao.save(flow);
  }

  @Override
  public boolean delete(final Long id) {
    return this.flowDao.delete(id);
  }

  @Override
  public boolean update(final Flow flow) {
    return false;
  }

  @Override
  public Flow getFlowById(final Long id) {
    return this.flowDao.getFlowById(id);
  }

  @Override
  public List<Flow> getAllFlowDetails() {
    return null;
  }

  @Override
  public List<Flow> getFlowByOrgId(final Long orgId) {
    return this.flowDao.getFlowByOrgId(orgId);
  }

  @Override
  public Flow getFlowByFlowTypeId(final Long flowTypeId, final Long flowRanking) {
    return this.flowDao.getFlowByFlowTypeId(flowTypeId, flowRanking);
  }

  @Override
  public Flow getFlowByRaking(final Long flowRanking) {
    return this.flowDao.getFlowByRaking(flowRanking);
  }
}
