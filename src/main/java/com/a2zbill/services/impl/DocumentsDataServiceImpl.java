package com.a2zbill.services.impl;

import com.a2zbill.dao.DocumentsDataDao;
import com.a2zbill.domain.DocumentsData;
import com.a2zbill.domain.ProductCategory;
import com.a2zbill.services.DocumentsDataService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DocumentsDataServiceImpl implements DocumentsDataService {

  @Autowired private DocumentsDataDao documentsDataDao;

  @Override
  public void saveDocumentData(final DocumentsData documentsData) {
    documentsDataDao.saveDocumentData(documentsData);
  }

  @Override
  public List<ProductCategory> getCategoryDetailsByCategoryType(
      final long orgId, final long branchId) {
    return documentsDataDao.getCategoryDetailsByCategoryType(orgId, branchId);
  }

  @Override
  public List<ProductCategory> getCategoryDetailsByCategoryTypeByOrg(final long orgId) {

    return documentsDataDao.getCategoryDetailsByCategoryTypeByOrg(orgId);
  }
}
