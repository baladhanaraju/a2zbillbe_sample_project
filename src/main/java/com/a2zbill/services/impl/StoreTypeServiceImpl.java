package com.a2zbill.services.impl;

import com.a2zbill.dao.StoreTypeDao;
import com.a2zbill.domain.StoreType;
import com.a2zbill.services.StoreTypeService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class StoreTypeServiceImpl implements StoreTypeService {

  @Autowired StoreTypeDao storeTypeDao;

  @Override
  public void save(StoreType storeType) {
    this.storeTypeDao.save(storeType);
  }

  @Override
  public boolean delete(long id) {
    this.storeTypeDao.delete(id);
    return true;
  }

  @Override
  public boolean update(StoreType storeType) {

    return this.storeTypeDao.update(storeType);
  }

  @Override
  public List<StoreType> getAllStoreTypeDetails() {

    return this.storeTypeDao.getAllStoreTypeDetails();
  }

  @Override
  public StoreType storeTypegetDetalisById(long id) {

    return this.storeTypeDao.storeTypegetDetalisById(id);
  }
}
