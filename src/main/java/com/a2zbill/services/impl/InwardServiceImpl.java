package com.a2zbill.services.impl;

import com.a2zbill.dao.InwardDao;
import com.a2zbill.domain.Inward;
import com.a2zbill.services.InwardService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InwardServiceImpl implements InwardService {
  @Autowired private InwardDao inputwordsDao;

  @Override
  public void save(final Inward inwards) {
    this.inputwordsDao.save(inwards);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final Inward inwards) {
    return this.inputwordsDao.update(inwards);
  }

  @Override
  public List<Inward> getAllInwards() {
    return this.inputwordsDao.getAllInwards();
  }

  @Override
  public List<Inward> getInwardDetailsByMonthAndYear(final String month, final String year) {
    return this.inputwordsDao.getInwardDetailsByMonthAndYear(month, year);
  }
}
