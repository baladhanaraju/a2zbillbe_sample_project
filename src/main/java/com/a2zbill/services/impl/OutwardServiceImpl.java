package com.a2zbill.services.impl;

import com.a2zbill.dao.OutwardDao;
import com.a2zbill.domain.Outward;
import com.a2zbill.services.OutwardService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OutwardServiceImpl implements OutwardService {
  @Autowired private OutwardDao outwardDao;

  @Override
  public void save(final Outward outward) {
    this.outwardDao.save(outward);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final Outward inward) {
    return false;
  }

  @Override
  public List<Outward> getAllOutwardDetails() {
    return this.outwardDao.getAllOutwardDetails();
  }

  @Override
  public List<Outward> getOutwardDetailsByMonthAndYear(final String month, final String year) {
    return this.outwardDao.getOutwardDetailsByMonthAndYear(month, year);
  }

  @Override
  public List<Outward> getOutwardDetailsByGstNumber(final String gstNumber) {
    return this.outwardDao.getOutwardDetailsByGstNumber(gstNumber);
  }

  @Override
  public List<Outward> getOutwardDetailsByCustomerId(final long customerId) {
    return this.outwardDao.getOutwardDetailsByCustomerId(customerId);
  }

  @Override
  public Outward getOutwardDetailByGstNumber(final String gstNumber) {
    return this.outwardDao.getOutwardDetailByGstNumber(gstNumber);
  }

  @Override
  public List<Outward> getOutwardDetailsByMonthAndYearAndGSTNumber(
      final String month, final String year) {
    return this.outwardDao.getOutwardDetailsByMonthAndYearAndGSTNumber(month, year);
  }
}
