package com.a2zbill.services.impl;

import com.a2zbill.dao.VariationOptionsDao;
import com.a2zbill.domain.VariationOptions;
import com.a2zbill.services.VariationOptionsService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class VariationOptionsServiceImpl implements VariationOptionsService {

  @Autowired VariationOptionsDao variationOptionsDao;

  @Override
  public List<VariationOptions> getAllVariationOptionsDetails() {
    return this.variationOptionsDao.getAllVariationOptions();
  }

  @Override
  public VariationOptions getVariationOptionsById(final long id) {
    return this.variationOptionsDao.getVariationOptionsById(id);
  }

  @Override
  public VariationOptions getVariationOptionsByName(final String name) {
    return this.variationOptionsDao.getVariationOptionsByName(name);
  }

  @Override
  public List<VariationOptions> getVariationOptionsByType(final long typeId) {

    return this.variationOptionsDao.getVariationOptionsByType(typeId);
  }
}
