package com.a2zbill.services.impl;

import com.a2zbill.dao.SectionDao;
import com.a2zbill.domain.Section;
import com.a2zbill.services.SectionService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SectionServiceImpl implements SectionService {
  @Autowired SectionDao sectionDao;

  @Override
  public void save(final Section section) {
    this.sectionDao.save(section);
  }

  @Override
  public boolean delete(final long id) {
    this.sectionDao.delete(id);
    return true;
  }

  @Override
  public boolean update(final Section section) {
    return this.sectionDao.update(section);
  }

  @Override
  public List<Section> getAllSectionDetails() {
    return this.sectionDao.getAllSectionDetails();
  }

  public List<Section> getSectionDetailsByOrgIdAndBranchId(final long orgId, final long branchId) {
    return this.sectionDao.getSectionDetailsByOrgIdAndBranchId(orgId, branchId);
  }

  public Section getSectionDetailsBySectionId(final long sectionId) {
    return this.sectionDao.getSectionDetailsBySectionId(sectionId);
  }

  @Override
  public List<Section> getSectionDetailsByOrgId(final long orgId) {
    return this.sectionDao.getSectionDetailsByOrgId(orgId);
  }

  @Override
  public List<Section> getSectionDetailsByOrgAndStatus(final long orgId) {
    return this.sectionDao.getSectionDetailsByOrgAndStatus(orgId);
  }

  @Override
  public List<Section> getSectionDetailsByOrgIdAndBranchAndStatus(
      final long orgId, final long branchId) {
    return this.sectionDao.getSectionDetailsByOrgIdAndBranchAndStatus(orgId, branchId);
  }

  @Override
  public Section getSectiondetails(final String sectionName) {
    return this.sectionDao.getSectiondetails(sectionName);
  }

  @Override
  public Section getSectiondetailsdisplayname(final String displayName) {

    return this.sectionDao.getSectiondetailsdisplayname(displayName);
  }
}
