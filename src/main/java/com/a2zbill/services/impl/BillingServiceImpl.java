package com.a2zbill.services.impl;

import com.a2zbill.dao.BillingDao;
import com.a2zbill.dao.BillingNumDataDao;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingNumData;
import com.a2zbill.domain.BillingNumType;
import com.a2zbill.domain.DiscountCoupons;
import com.a2zbill.services.BillingNumTypeService;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.DiscountCouponsService;
import com.offers.domain.Offers;
import com.offers.services.OffersService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BillingServiceImpl implements BillingService {
  @Autowired private BillingNumDataDao billingNumDataDao;
  @Autowired private EmployeeService employeeService;
  @Autowired private BillingNumTypeService billingNumTypeService;
  @Autowired private OffersService offersService;
  @Autowired private DiscountCouponsService discountCouponsService;
  @Autowired private BillingDao billingdao;

  @Override
  public void save(final Billing billing) {
    this.billingdao.save(billing);
  }

  public Billing getBillingDetailsByCartId(final long cartId) {
    return this.billingdao.getBillingDetailsByCartId(cartId);
  }

  public Billing getBillingDetailsByBillId(final long billId) {
    return this.billingdao.getBillingDetailsByBillId(billId);
  }

  @Override
  public List<Billing> getBillingDetailsByDateAndCounter(
      final Date billingdate, final long counter) {
    return this.billingdao.getBillingDetailsByDateAndCounter(billingdate, counter);
  }

  @Override
  public BigDecimal getBillingCurrentDaySaleAmount(final Date billingdate) {
    return this.billingdao.getBillingCurrentDaySaleAmount(billingdate);
  }

  @Override
  public BigDecimal getBillingCurrentMonthSaleAmount(final int month, final int year) {
    return this.billingdao.getBillingCurrentMonthSaleAmount(month, year);
  }

  @Override
  public BigDecimal getBillingLastSixMonthSaleAmount(final int month, final int year) {
    return this.billingdao.getBillingLastSixMonthSaleAmount(month, year);
  }

  @Override
  public List<Billing> getBillingsByCurrentDate(final Date billingdate) {
    return this.billingdao.getBillingsByCurrentDate(billingdate);
  }

  @Override
  public List<Billing> getBillingsByCurrentDateAndOrgBranch(
      final long orgId, final long branchId, final Date billingdate) {
    return this.billingdao.getBillingsByCurrentDateAndOrgBranch(orgId, branchId, billingdate);
  }

  @Override
  public List<Billing> getBillingDetailsByDateAndCounterAndOrgBranch(
      final long orgId, final long branchId, final Date billingdate, final long counter) {
    return this.billingdao.getBillingDetailsByDateAndCounterAndOrgBranch(
        orgId, branchId, billingdate, counter);
  }

  @Override
  public List<Billing> getBillingsByCurrentDateAndOrgId(
      final Date billingdate, final Date todate, final long orgId) {
    return this.billingdao.getBillingsByCurrentDateAndOrgId(billingdate, todate, orgId);
  }

  @Override
  public List<Billing> getBillingDetailsByDateAndCounterAndOrgId(
      final long orgId, final Date billingdate, final long counter) {
    return this.billingdao.getBillingDetailsByDateAndCounterAndOrgId(orgId, billingdate, counter);
  }

  @Override
  public Billing getBillingDetailsByBillIdAndOrgIdAndBranchId(
      final long billId, final long orgId, final long branchId) {
    return this.billingdao.getBillingDetailsByBillIdAndOrgIdAndBranchId(billId, orgId, branchId);
  }

  @Override
  public boolean update(final Billing billing) {
    this.billingdao.update(billing);
    return true;
  }

  @Override
  public List<Billing> getAllBillingDetailsByDate(final Date date) {
    return this.billingdao.getAllBillingDetailsByDate(date);
  }

  @Override
  public BigDecimal getBillingCurrentDaySaleAmountByOrgBranch(
      final Date billingDate, final long orgId, final long branchId) {
    return this.billingdao.getBillingCurrentDaySaleAmountByOrgBranch(billingDate, orgId, branchId);
  }

  @Override
  public BigDecimal getBillingCurrentMonthSaleAmountorgBranch(
      final int month, final int year, final long orgId, final long branchId) {
    return this.billingdao.getBillingCurrentMonthSaleAmountByorgBranch(
        month, year, orgId, branchId);
  }

  @Override
  public List<Billing> getAllBillingDetailsByDateAndBrachIdAndOrgId(
      final Date date, final long branchId, final long orgId) {
    return this.billingdao.getAllBillingDetailsByDateAndBrachIdAndOrgId(date, branchId, orgId);
  }

  @Override
  public List<Billing> getAllBillingDetailsByCurrentDate() {
    return this.billingdao.getAllBillingDetailsByCurrentDate();
  }

  @Override
  public List<BigDecimal> getSumOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {

    return this.billingdao.getSumOfBillingDetailsByBranchAndOrg(formDate, endDate, orgId, branchId);
  }

  @Override
  public List<BigDecimal> getSumOfBillingDetailsByOrgBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {

    return this.billingdao.getSumOfBillingDetailsByOrgBranchisNull(formDate, endDate, orgId);
  }

  public long saveBillingNumberTypeData(final String billingTypeName, final String emailAddress)
      throws Exception {

    try {
      final Employee employeeDetails = this.employeeService.getEmployeeDetailsByEmail(emailAddress);
      final long orgId = employeeDetails.getOrganisation().getId();
      final long branchId = employeeDetails.getBranch().getId();
      final Date currentDate = new Date();
      final Calendar now = Calendar.getInstance();
      now.setTime(currentDate);
      final int month = now.get(Calendar.MONTH) + 1;
      final int year = now.get(Calendar.YEAR);
      final String s = year + "-04" + "-01"; // 2019-05-04
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      final Date d = sdf.parse(s);
      final BillingNumData savebillingNumData = new BillingNumData();
      final BillingNumType billingNumberType =
          this.billingNumTypeService.getBillingNumTypeDetailsByBillingTypeName(billingTypeName);
      if (billingNumberType.getTypeValue().equals("y")) {
        final long billingNumberTypeId = billingNumberType.getId();
        if (month > 3) {

          final BillingNumData billingNumData =
              this.billingNumDataDao.getAllBillingNumDataByDateAndOrgBranchId(
                  d, billingNumberTypeId, orgId, branchId);
          if (billingNumData != null) {
            final long currentCount = billingNumData.getCurrentCount() + 1;
            billingNumData.setCurrentCount(currentCount);
            billingNumData.setBilldate(d);
            billingNumData.setCreatedDate(currentDate);
            billingNumData.setModifiedDate(currentDate);
            this.billingNumDataDao.update(billingNumData);
            return billingNumData.getCurrentCount();
          } else {
            savebillingNumData.setBillingNumType(billingNumberType);
            savebillingNumData.setCurrentCount(1);
            savebillingNumData.setOrganisation(employeeDetails.getOrganisation());
            savebillingNumData.setBranch(employeeDetails.getBranch());
            savebillingNumData.setBilldate(currentDate);
            savebillingNumData.setCreatedDate(currentDate);
            savebillingNumData.setModifiedDate(currentDate);
            this.billingNumDataDao.save(savebillingNumData);
            return savebillingNumData.getCurrentCount();
          }
        } else {
          final BillingNumData billingNumData =
              this.billingNumDataDao.getAllBillingNumDataByDateExtractAndOrgBranchId(
                  billingNumberTypeId, 01, 04, year - 1, orgId, branchId);
          billingNumData.setBilldate(currentDate);
          billingNumData.setCreatedDate(currentDate);
          billingNumData.setModifiedDate(currentDate);
          this.billingNumDataDao.update(billingNumData);
          return billingNumData.getCurrentCount();
        }
      }
      if (billingNumberType.getTypeValue().equals("d")) {
        final long billingNumTypeId = billingNumberType.getId();
        final BillingNumData billingNumData =
            this.billingNumDataDao.getAllBillingNumDataByDateAndOrgBranchId(
                currentDate, billingNumTypeId, orgId, branchId);
        if (billingNumData != null) {
          final long currentCount = billingNumData.getCurrentCount() + 1;
          billingNumData.setCurrentCount(currentCount);
          billingNumData.setBilldate(currentDate);
          billingNumData.setCreatedDate(currentDate);
          billingNumData.setModifiedDate(currentDate);
          this.billingNumDataDao.update(billingNumData);
          return billingNumData.getCurrentCount();
        } else {
          savebillingNumData.setBillingNumType(billingNumberType);
          savebillingNumData.setCurrentCount(1);
          savebillingNumData.setOrganisation(employeeDetails.getOrganisation());
          savebillingNumData.setBranch(employeeDetails.getBranch());
          savebillingNumData.setBilldate(currentDate);
          savebillingNumData.setCreatedDate(currentDate);
          savebillingNumData.setModifiedDate(currentDate);
          this.billingNumDataDao.save(savebillingNumData);
          return savebillingNumData.getCurrentCount();
        }
      }
    } catch (final Exception e) {

    }

    return 0L;
  }

  @Override
  public List<Billing> getBillingsByStartAndEndDateAndOrgAndBranch(
      final Date startDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao.getBillingsByStartAndEndDateAndOrgAndBranch(
        startDate, endDate, orgId, branchId);
  }

  @Override
  public List<Object[]> getnetsaleOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao.getnetsaleOfBillingDetailsByBranchAndOrg(
        formDate, endDate, orgId, branchId);
  }

  @Override
  public List<Object[]> getAllTxesOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao.getAllTxesOfBillingDetailsByBranchAndOrg(
        formDate, endDate, orgId, branchId);
  }

  @Override
  public List<Object[]> getAllDiscountOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao.getAllDiscountOfBillingDetailsByBranchAndOrg(
        formDate, endDate, orgId, branchId);
  }

  @Override
  public List<Object[]> getAllGrossSalesOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao.getAllGrossSalesOfBillingDetailsByBranchAndOrg(
        formDate, endDate, orgId, branchId);
  }

  @Override
  public List<Billing> getBillingsByOrgIdByFromdate(final Date billingdate, final long orgId) {
    return this.billingdao.getBillingsByOrgIdByFromdate(billingdate, orgId);
  }

  @Override
  public List<Billing> getBillingsByCategoryAndStartAndEndDateAndOrgAndBranch(
      final Date startDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao.getBillingsByCategoryAndStartAndEndDateAndOrgAndBranch(
        startDate, endDate, orgId, branchId);
  }

  @Override
  public Billing getBillingDetailsByBillIdAndOrgId(final long billId, final long orgId) {
    return this.billingdao.getBillingDetailsByBillIdAndOrgId(billId, orgId);
  }

  @Override
  public BigDecimal getBillingCurrentDaySaleAmountByOrg(final Date billingDate, final long orgId) {
    return this.billingdao.getBillingCurrentDaySaleAmountByOrg(billingDate, orgId);
  }

  @Override
  public BigDecimal getBillingCurrentMonthSaleAmountorg(
      final int Month, final int year, final long orgId) {
    return this.billingdao.getBillingCurrentMonthSaleAmountorg(Month, year, orgId);
  }

  @Override
  public List<Billing> getBillingDetailsByDatesAndOrgIdAndCounterNumber(
      final Date fromDate, final Date toDate, final long orgId, final long counterNumber) {
    return this.billingdao.getBillingDetailsByDatesAndOrgIdAndCounterNumber(
        fromDate, toDate, orgId, counterNumber);
  }

  @Override
  public List<Billing> getBillingDetailsByDatesAndOrgIdAndBranchIdAndCounterNumber(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long counterNumber) {
    return this.billingdao.getBillingDetailsByDatesAndOrgIdAndBranchIdAndCounterNumber(
        fromDate, toDate, orgId, branchId, counterNumber);
  }

  @Override
  public List<Object[]> getnetsaleOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {
    return this.billingdao.getnetsaleOfBillingDetailsByOrgAndBranchisNull(formDate, endDate, orgId);
  }

  @Override
  public List<Object[]> getAllTxesOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {
    return this.billingdao.getAllTxesOfBillingDetailsByOrgAndBranchisNull(formDate, endDate, orgId);
  }

  @Override
  public List<Object[]> getAllDiscountOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {
    return this.billingdao.getAllDiscountOfBillingDetailsByOrgAndBranchisNull(
        formDate, endDate, orgId);
  }

  @Override
  public List<Object[]> getAllGrossSalesOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {
    return this.billingdao.getAllGrossSalesOfBillingDetailsByOrgAndBranchisNull(
        formDate, endDate, orgId);
  }

  @Override
  public List<Billing> getBillingDetailsByCustomerId(final long custId) {
    return this.billingdao.getBillingDetailsByCustomerId(custId);
  }

  @Override
  public List<Billing> getBillingByCustomerId(final long custId) {
    return this.billingdao.getBillingByCustomerId(custId);
  }

  @Override
  public List<Object[]>
      getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndBranchIdGroupedByPaymentMode(
          final Date fromDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao
        .getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndBranchIdGroupedByPaymentMode(
            fromDate, endDate, orgId, branchId);
  }

  @Override
  public List<Object[]> getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndGroupedByPaymentMode(
      final Date fromDate, final Date endDate, final long orgId) {
    return this.billingdao.getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndGroupedByPaymentMode(
        fromDate, endDate, orgId);
  }

  @Override
  public List<Object[]> getAllServiceChargesOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {
    return this.billingdao.getAllServiceChargesOfBillingDetailsByBranchAndOrg(
        formDate, endDate, orgId, branchId);
  }

  @Override
  public List<Object[]> getAllServiceChargesOfBillingDetailsByOrg(
      final Date formDate, final Date endDate, final long orgId) {
    return this.billingdao.getAllServiceChargesOfBillingDetailsByOrg(formDate, endDate, orgId);
  }

  @Override
  public List<Object[]> getSaleReportBetweenTwoDatesByOrgIdAndBranchIdAndDates(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {
    return this.billingdao.getSaleReportBetweenTwoDatesByOrgIdAndBranchIdAndDates(
        fromDate, toDate, orgId, branchId);
  }

  public final Integer getOffersByOfferCodeAndCustId(
      final String offerCode, final String customerMobileNumber, BigDecimal bill) {
    final Billing billing = new Billing();
    final Calendar call = Calendar.getInstance();
    final Date currentDate = call.getTime();
    if (offerCode != null) {
      if (offerCode.startsWith("F")) {
        final Offers offer = offersService.getOffersByOfferCode(offerCode);
        if ((currentDate).equals(offer.getStartTime())
            || (currentDate.equals(offer.getEndTime()))
            || (currentDate.after(offer.getStartTime())
                && (currentDate).before(offer.getEndTime()))) {
          final BigDecimal hundered = new BigDecimal(100);
          if (offer.getDiscountType().getId() == 1) {
            final BigDecimal discountBill =
                bill.multiply((offer.getDiscountValue()).divide(hundered));
            bill = bill.subtract(discountBill);
            billing.setTotalAmount(bill);
            return Integer.valueOf(bill.intValue());
          } else if (offer.getDiscountType().getId() == 2) {
            bill = bill.subtract(offer.getDiscountValue());
            billing.setTotalAmount(bill);
            return Integer.valueOf(bill.intValue());
          }
          return 0;
        }
        return 0;
      } else if (offerCode.startsWith("p")) {
        final DiscountCoupons discountCoupons =
            this.discountCouponsService.getDiscountCouponsByOfferCode(offerCode);
        if ((currentDate).equals(discountCoupons.getStartTime())
            || (currentDate.equals(discountCoupons.getEndTime()))
            || (currentDate.after(discountCoupons.getStartTime())
                && (currentDate).before(discountCoupons.getEndTime()))) {
          final BigDecimal hundered = new BigDecimal(100);
          if ((discountCoupons.getCustomer().getMobileNumber()).equals(customerMobileNumber)
              || (discountCoupons.getCustomer().getMobileNumber()).isEmpty()) {
            if (discountCoupons.getDiscountType().getId() == 1) {
              final BigDecimal discountBill =
                  bill.multiply((discountCoupons.getDiscountAmount()).divide(hundered));
              bill = bill.subtract(discountBill);
              billing.setTotalAmount(bill);
              return Integer.valueOf(bill.intValue());
            } else if (discountCoupons.getDiscountType().getId() == 2) {
              bill = bill.subtract(discountCoupons.getDiscountAmount());
              billing.setTotalAmount(bill);
              return Integer.valueOf(bill.intValue());
            }
          }
          return 0;
        }
        return 0;
      }
    }
    return 0;
  }

  @Override
  public Billing getBillingDetailsByOrdersId(final long ordersId) {
    return billingdao.getBillingDetailsByOrdersId(ordersId);
  }

  @Override
  public Billing getBillingDetailsByBillNumberAndOrg(String billNumber, long orgId, Date date) {
    // TODO Auto-generated method stub
    return billingdao.getBillingDetailsByBillNumberAndOrg(billNumber, orgId, date);
  }

  @Override
  public Billing getBillingDetailsByBillNumberAndOrgAndBranch(
      String billNumber, long orgId, long branchId, Date date) {
    // TODO Auto-generated method stub
    return billingdao.getBillingDetailsByBillNumberAndOrgAndBranch(
        billNumber, orgId, branchId, date);
  }

  @Override
  public Billing getBillingDetailsByBillNumberCountAndOrg(
      String billNumber, long orgId, Date date) {
    // TODO Auto-generated method stub
    return billingdao.getBillingDetailsByBillNumberCountAndOrg(billNumber, orgId, date);
  }

  @Override
  public Billing getBillingDetailsByBillNumberCountAndOrgAndBranch(
      String billNumber, long orgId, long branchId, Date date) {
    // TODO Auto-generated method stub
    return billingdao.getBillingDetailsByBillNumberCountAndOrgAndBranch(
        billNumber, orgId, branchId, date);
  }
}
