package com.a2zbill.services.impl;

import com.a2zbill.dao.ReservationDao;
import com.a2zbill.domain.Reservation;
import com.a2zbill.services.ReservationService;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {
  @Autowired private ReservationDao reservationDao;

  @Override
  public void save(final Reservation reservation) {
    this.reservationDao.save(reservation);
  }

  @Override
  public boolean delete(final Reservation reservation) {
    return this.reservationDao.delete(reservation);
  }

  @Override
  public void update(final Reservation reservation) {
    this.reservationDao.update(reservation);
  }

  @Override
  public List<Reservation> getAllReservationDetails() {
    return this.reservationDao.getAllReservationDetails();
  }

  @Override
  public Reservation getReservationDetailsByReservationid(final long id) {
    return this.reservationDao.getReservationDetailsByid(id);
  }

  @Override
  public List<Reservation> getReservationDetailsByStatus(final String status) {
    return this.reservationDao.getReservationDetailsByStatus(status);
  }

  @Override
  public List<Reservation> getReservationDetailsByDateAndTime(
      final Date date,
      final long reservedtable,
      final String startTime,
      final long orgId,
      final long branchId) {
    return this.reservationDao.getReservationDetailsByDateAndTime(
        date, reservedtable, startTime, orgId, branchId);
  }

  @Override
  public List<Reservation> getReservationDetailsByDateAndTimeAndOrg(
      final Date date, final long reservedtable, final String startTime, final long orgId) {
    return this.reservationDao.getReservationDetailsByDateAndTimeAndOrg(
        date, reservedtable, startTime, orgId);
  }

  @Override
  public List<Reservation> getReservationDetailsByOrgAndBranch(
      final long orgId, final long branchId) {
    return this.reservationDao.getReservationDetailsByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<Reservation> getReservationDetailsByOrg(final long orgId) {
    return this.reservationDao.getReservationDetailsByOrg(orgId);
  }

  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrgAndBranch(
      final Date currentdate,
      final long reservetable,
      final String time,
      final long orgId,
      final long branchId) {
    return this.reservationDao.getReservationDetailsByStarttimeOrgAndBranch(
        currentdate, reservetable, time, orgId, branchId);
  }

  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrg(
      final Date currentdate, final long reservetable, final String time, final long orgId) {
    return this.reservationDao.getReservationDetailsByStarttimeOrg(
        currentdate, reservetable, time, orgId);
  }

  @Override
  public List<Map<String, Object>> getAllReservationsByDate(
      final Date date, final long orgId, final long branchId) throws ParseException {
    final List<Reservation> reservationDetails =
        this.reservationDao.getAllReservationsByDate(date, orgId, branchId);
    final DateFormat sdf = new SimpleDateFormat("HH:mm");
    final String currentTime = sdf.format(date.getTime());
    final Calendar cl = Calendar.getInstance();
    final Calendar cstart = Calendar.getInstance();
    final Calendar cend = Calendar.getInstance();
    cl.setTime(sdf.parse(currentTime));
    cl.add(Calendar.MINUTE, 15);
    final Date timeAfter15Minutes = cl.getTime();
    // String timeAfter15Minutes = sdf.format(cl.getTime());
    final List<Map<String, Object>> reservationList = new ArrayList<Map<String, Object>>();
    for (final Reservation reservation : reservationDetails) {
      final Map<String, Object> tableMap = new HashMap<String, Object>();
      final Date startTime = sdf.parse(reservation.getStartTime());
      cstart.setTime(startTime);
      // Date endTime = sdf.parse(reservation.getEndTime());
      // cend.setTime(endTime);
      System.out.println(
          "timeAfter15Minutes"
              + timeAfter15Minutes
              + "   cstart.getTime()"
              + cstart.getTime()
              + "  cend.getTime()"
              + cend.getTime());
      if (timeAfter15Minutes.before(cstart.getTime()) || cend.getTime().before(cl.getTime())) {
        tableMap.put("table", reservation.getReserveTable());
        System.out.println("reservation.getReserveTable()" + reservation.getReserveTable());
        System.out.println("tableMap.size()" + tableMap.size());
        System.out.println("reservationList.size()" + reservationList.size());
        // reservationList.add(tableMap);
        reservationList.add(tableMap);
      }
    }
    return reservationList;
  }

  @Override
  public List<Reservation> getReservationDeatilsByTable(
      final Date date, final long reservetable, final long orgId, final long branchId) {
    return this.reservationDao.getReservationDeatilsByTable(date, reservetable, orgId, branchId);
  }

  @Override
  public List<Reservation> getReservationDeatilsByTableAndOrg(
      final Date date, final long reservetable, final long orgId) {
    return this.reservationDao.getReservationDeatilsByTableAndOrg(date, reservetable, orgId);
  }

  @Override
  public Reservation getReservationByTable(
      final Date date, final long reservetable, final long orgId, final long branchId) {
    return this.reservationDao.getReservationByTable(date, reservetable, orgId, branchId);
  }

  @Override
  public Reservation getReservationByTableAndOrg(
      final Date date, final long reservetable, final long orgId) {
    return this.reservationDao.getReservationByTableAndOrg(date, reservetable, orgId);
  }

  @Override
  public Reservation getReservationdeatilsByTableAndcustomerId(
      final long customer,
      final Date date,
      final long reservetable,
      final long orgId,
      final long branchId) {
    return this.reservationDao.getReservationdeatilsByTableAndcustomerId(
        customer, date, reservetable, orgId, branchId);
  }

  @Override
  public Reservation getReservationdeatilsByTableAndOrgAndcustomerId(
      final long customer, final Date date, final long reservetable, final long orgId) {
    return this.reservationDao.getReservationdeatilsByTableAndOrgAndcustomerId(
        customer, date, reservetable, orgId);
  }

  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrgAndBranch(
      final String startTime, final long orgId, final long branchId) {

    return this.reservationDao.getReservationDetailsByStarttimeOrgAndBranch(
        startTime, orgId, branchId);
  }

  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrg(
      final String startTime, final long orgId) {

    return this.reservationDao.getReservationDetailsByStarttimeOrg(startTime, orgId);
  }

  @Override
  public Reservation getReservationDetailsByreserveTableOrgAndBranch(
      final Date date,
      final long reservetable,
      final String startTime,
      final long orgId,
      final long branchId) {
    return this.reservationDao.getReservationDetailsByreserveTableOrgAndBranch(
        date, reservetable, startTime, orgId, branchId);
  }

  @Override
  public Reservation getReservationDetailsByreserveTableOrg(
      final long reservetable, final String startTime, final long orgId) {
    return this.reservationDao.getReservationDetailsByreserveTableOrg(
        reservetable, startTime, orgId);
  }

  @Override
  public List<Reservation>
      getReservationDetailsByReservedTableAndOrgAndBranchAndBetweenTwoTimesAndDate(
          final long reservedTable,
          final Date reserveDate,
          final long orgId,
          final long branchId,
          final String lowestTime,
          final String highestTime) {
    return this.reservationDao
        .getReservationDetailsByReservedTableAndOrgAndBranchAndBetweenTwoTimesAndDate(
            reservedTable, reserveDate, orgId, branchId, lowestTime, highestTime);
  }

  @Override
  public BigInteger getCountOfReservationByOrganisationIdAndCurrentMonth(final long orgId) {
    return this.reservationDao.getCountOfReservationByOrganisationIdAndCurrentMonth(orgId);
  }

  @Override
  public BigInteger getCountOfReservationByOrganisationIdAndCurrentMonthBranchId(
      final long orgId, final long branchId) {
    return this.reservationDao.getCountOfReservationByOrganisationIdAndCurrentMonthBranchId(
        orgId, branchId);
  }

  @Override
  public List<Reservation> getAllReservationsFromCurrentDate(
      final long orgId, final long branchId, final Date date, final String startTime) {
    return this.reservationDao.getAllReservationsFromCurrentDate(orgId, branchId, date, startTime);
  }

  @Override
  public Reservation getReservationDetailsBySlotTimeBranch(
      final Date date,
      final String startTime,
      final long reservetable,
      final long orgId,
      final long branchId) {
    return reservationDao.getReservationDetailsBySlotTimeBranch(
        date, startTime, reservetable, orgId, branchId);
  }

  @Override
  public List<Reservation> getReservationDetailsBySlotTimeorg(
      final String startTime, final long reservetable, final long orgId) {
    return reservationDao.getReservationDetailsBySlotTimeorg(startTime, reservetable, orgId);
  }
}
