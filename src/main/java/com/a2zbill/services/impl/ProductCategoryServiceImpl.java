package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductCategoryDao;
import com.a2zbill.domain.ProductCategory;
import com.a2zbill.services.ProductCategoryService;
import com.tsss.basic.domain.MyJson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductCategoryServiceImpl implements ProductCategoryService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductCategoryServiceImpl.class);

  @Autowired private ProductCategoryDao productcategorydao;

  @Override
  public void save(final ProductCategory productCategory) {
    this.productcategorydao.save(productCategory);
  }

  @Override
  public List<ProductCategory> getAllProductCategory() {
    return this.productcategorydao.getAllProductCategory();
  }

  @Override
  public ProductCategory getProductCategoryByProductCategoryName(final String productCategoryName) {
    return this.productcategorydao.getProductCategoryByProductCategoryName(productCategoryName);
  }

  @Override
  public MyJson getFeaturesById(final Long id) {
    return this.productcategorydao.getFeaturesById(id);
  }

  @Override
  public ProductCategory gettFeaturesById(final Long id) {
    return this.productcategorydao.gettFeaturesById(id);
  }

  @Override
  public ProductCategory getProductCategoryById(final Long id) {
    return this.productcategorydao.getProductCategoryById(id);
  }

  @Override
  public List<ProductCategory> getProductCategorys(final long orgId) {
    return this.productcategorydao.getProductCategorys(orgId);
  }

  @Override
  public List<ProductCategory> getProductCategorysByOrgAndBranchId(
      final long branchId, final long orgId) {
    return this.productcategorydao.getProductCategorysByOrgAndBranchId(branchId, orgId);
  }

  @Override
  public ProductCategory getProductCategoryByProductCategoryNameorgIdAndbrnachId(
      final String productCategoryName, final long orgId, final long brnachId) {
    return this.productcategorydao.getProductCategoryByProductCategoryNameorgIdAndbrnachId(
        productCategoryName, orgId, brnachId);
  }

  @Override
  public List<ProductCategory> getProductCategorysBasedBranchIdAndOrgId(
      final long branchId, final long orgId) {
    return this.productcategorydao.getProductCategorysBasedBranchIdAndOrgId(branchId, orgId);
  }

  @Override
  public List<ProductCategory> getProductCategorysBasedBranchIdAndOrgIdAndParentId(
      final long branchId, final long orgId, final long categoryTypeId, final long parentId) {
    return this.productcategorydao.getProductCategorysBasedBranchIdAndOrgIdAndParentId(
        branchId, orgId, categoryTypeId, parentId);
  }

  @Override
  public List<ProductCategory> getProductCategoriesIfRedeemFlagIsTrue(
      final long branchId, final long orgId) {
    return this.productcategorydao.getProductCategoriesIfRedeemFlagIsTrue(branchId, orgId);
  }

  @Override
  public List<ProductCategory> getProductCategoriesIfRedeemFlagIsFalseAndOrgIdAndBranchId(
      final long branchId, final long orgId) {
    return this.productcategorydao.getProductCategoriesIfRedeemFlagIsFalseAndOrgIdAndBranchId(
        branchId, orgId);
  }

  @Override
  public List<ProductCategory> getProductCategorysBasedBranchIdAndOrgId(
      final long branchId, final long orgId, final long categoryTypeId) {
    return this.productcategorydao.getProductCategorysBasedBranchIdAndOrgId(
        branchId, orgId, categoryTypeId);
  }

  @Override
  public List<ProductCategory> getProductCategoryByBranchIdAndOrgIdAndParentId(
      final long branchId, final long orgId, final long parentId) {
    return this.productcategorydao.getProductCategoryByBranchIdAndOrgIdAndParentId(
        branchId, orgId, parentId);
  }

  @Override
  public List<ProductCategory> getProductCategoriesRedeemFlagIsTrue(final long orgId) {
    return this.productcategorydao.getProductCategoriesRedeemFlagIsTrue(orgId);
  }

  @Override
  public List<ProductCategory> getProductCategoriesIfRedeemFlagIsFalseAndOrgId(final long orgId) {
    return this.productcategorydao.getProductCategoriesIfRedeemFlagIsFalseAndOrgId(orgId);
  }

  @Override
  public List<ProductCategory> getProductCategorysBasedOnOrgIdAndParentId(
      final long orgId, final long categoryTypeId, final long parentId) {
    return this.productcategorydao.getProductCategorysBasedOnOrgIdAndParentId(
        orgId, categoryTypeId, parentId);
  }

  @Override
  public List<ProductCategory> getProductCategoryByOrgIdAndParentId(
      final long orgId, final long parentId) {
    return this.productcategorydao.getProductCategoryByOrgIdAndParentId(orgId, parentId);
  }

  @Override
  public ProductCategory getProductCategoryByProductCategoryNameorgId(
      final String productCategoryName, final long orgId) {
    return this.productcategorydao.getProductCategoryByProductCategoryNameorgId(
        productCategoryName, orgId);
  }

  @Override
  public List<ProductCategory> getProductCategoriesByCategoryTypeId(final long categoryTypeId) {
    return this.productcategorydao.getProductCategoriesByCategoryTypeId(categoryTypeId);
  }

  @Override
  public boolean update(final ProductCategory productCategory) {
    this.productcategorydao.update(productCategory);
    return true;
  }

  @Override
  public List<ProductCategory> getProductCategoriesBasedOnOrgIdAndCategoryType(
      final long orgId, final long categoryTypeId) {
    return this.productcategorydao.getProductCategoriesBasedOnOrgIdAndCategoryType(
        orgId, categoryTypeId);
  }

  @Override
  public ProductCategory getProductCategoryByProductTypeName(final String productTypeName) {
    return this.productcategorydao.getProductCategoryByProductTypeName(productTypeName);
  }

  @Override
  public List<String> getProductCategoryNames(
      final long branchId, final long orgId, final long categoryTypeId) {
    return productcategorydao.getProductCategoryNames(branchId, orgId, categoryTypeId);
  }

  @Override
  public List<Map<String, Object>> getProductCategoryNameAndIdMap(long branchId, long orgId) {
    final List<Map<String, Object>> lMaps = new ArrayList<>();
    try {
      final List<Object[]> producObjects =
          productcategorydao.getProductCategoryNameAndIdMap(branchId, orgId);
      for (Object[] product : producObjects) {
        final Map<String, Object> results =
            new HashMap<String, Object>() {
              private static final long serialVersionUID = 1L;

              {
                put("id", product[0]);
                put("productTypeName", product[1]);
              }
            };
        lMaps.add(results);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return lMaps;
  }

  @Override
  public List<Long> getProductCategoryIds(long branchId, long orgId, long categoryTypeId) {

    return productcategorydao.getProductCategoryIds(branchId, orgId, categoryTypeId);
  }

  @Override
  public List<Object[]> getCategoriesBycategoryType(long categoryId, String pathUrl) {
    return productcategorydao.getCategoriesBycategoryType(categoryId, pathUrl);
  }

  @Override
  public Integer[] getCategoryTypesByStoreTemplateId(long storeTemplateId) {
    return this.productcategorydao.getCategoryTypesByStoreTemplateId(storeTemplateId);
  }

  @Override
  public List<Object[]> getProductCategorysByOrg(long orgId) {
    // TODO Auto-generated method stub
    return productcategorydao.getProductCategorysByOrg(orgId);
  }

  @Override
  public List<Object[]> getProductCategorysByOrgAndBranch(long orgId, long branchId) {
    // TODO Auto-generated method stub
    return productcategorydao.getProductCategorysByOrgAndBranch(orgId, branchId);
  }
}
