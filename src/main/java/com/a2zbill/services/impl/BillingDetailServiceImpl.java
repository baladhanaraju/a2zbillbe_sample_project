package com.a2zbill.services.impl;

import com.a2zbill.dao.BillingDetailDao;
import com.a2zbill.domain.BillingDetail;
import com.a2zbill.services.BillingDetailService;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BillingDetailServiceImpl implements BillingDetailService {
  @Autowired BillingDetailDao billingDetailsDao;

  @Override
  public void save(BillingDetail billingDetails) {
    this.billingDetailsDao.save(billingDetails);
  }

  @Override
  public boolean delete(long id) {
    return false;
  }

  @Override
  public boolean update(BillingDetail billingDetails) {
    return this.billingDetailsDao.update(billingDetails);
  }

  @Override
  public List<BillingDetail> getAllBillingDetails() {
    return new ArrayList<>();
  }
}
