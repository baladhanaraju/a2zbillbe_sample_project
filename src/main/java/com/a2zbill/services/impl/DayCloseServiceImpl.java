package com.a2zbill.services.impl;

import com.a2zbill.dao.DayCloseDao;
import com.a2zbill.domain.DayClose;
import com.a2zbill.services.DayCloseService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class DayCloseServiceImpl implements DayCloseService {

  @Autowired DayCloseDao dayCloseDao;

  @Override
  public void save(final DayClose dayClose) {
    dayCloseDao.save(dayClose);
  }

  @Override
  public boolean update(final DayClose dayClose) {
    return false;
  }

  @Override
  public List<DayClose> getAllDayCloseDetails() {
    return null;
  }

  @Override
  public List<DayClose> getDayCloseDatails(
      final Date fromdate, final Date toDate, final long branchId) {
    return dayCloseDao.getDayCloseDatails(fromdate, toDate, branchId);
  }
}
