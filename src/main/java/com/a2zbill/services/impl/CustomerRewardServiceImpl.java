package com.a2zbill.services.impl;

import com.a2zbill.dao.CustomerRewardDao;
import com.a2zbill.domain.CustomerReward;
import com.a2zbill.services.CustomerRewardService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerRewardServiceImpl implements CustomerRewardService {
  @Autowired CustomerRewardDao customerRewardDao;

  @Override
  public void save(final CustomerReward customerReward) {
    this.customerRewardDao.save(customerReward);
  }

  @Override
  public boolean delete(final Long id) {
    return false;
  }

  @Override
  public boolean update(final CustomerReward customerReward) {
    return false;
  }

  @Override
  public CustomerReward getCustomerRewardsById(final Long id) {
    return null;
  }

  @Override
  public List<CustomerReward> getAllCustomerRewards() {
    return this.customerRewardDao.getAllCustomerRewards();
  }
}
