package com.a2zbill.services.impl;

import com.a2zbill.dao.BillingNumTypeDao;
import com.a2zbill.domain.BillingNumType;
import com.a2zbill.services.BillingNumTypeService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BillingNumTypeServiceImpl implements BillingNumTypeService {
  @Autowired BillingNumTypeDao billingNumTypeDao;

  @Override
  public void save(BillingNumType billingNumType) {
    this.billingNumTypeDao.save(billingNumType);
  }

  @Override
  public boolean delete(BillingNumType billingNumType) {
    return this.billingNumTypeDao.delete(billingNumType);
  }

  @Override
  public boolean update(BillingNumType billingNumType) {
    return this.billingNumTypeDao.update(billingNumType);
  }

  @Override
  public List<BillingNumType> getAllBillingNumType() {
    return this.billingNumTypeDao.getAllBillingNumType();
  }

  @Override
  public BillingNumType getBillingNumTypeDetailsByBillingTypeName(final String billingTypeName) {
    return this.billingNumTypeDao.getBillingNumTypeDetailsByBillingTypeName(billingTypeName);
  }
}
