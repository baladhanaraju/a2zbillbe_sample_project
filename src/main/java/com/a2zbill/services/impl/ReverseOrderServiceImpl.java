package com.a2zbill.services.impl;

import com.a2zbill.dao.ReverseOrderDao;
import com.a2zbill.domain.ReverseOrder;
import com.a2zbill.services.ReverseOrderService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ReverseOrderServiceImpl implements ReverseOrderService {
  @Autowired ReverseOrderDao reverseOrderDao;

  @Override
  public void save(ReverseOrder reverseOrder) {
    this.reverseOrderDao.save(reverseOrder);
  }

  @Override
  public boolean delete(long id) {
    return false;
  }

  @Override
  public boolean update(ReverseOrder reverseOrder) {
    return this.reverseOrderDao.update(reverseOrder);
  }

  @Override
  public List<ReverseOrder> getAllReverseOrderDetails() {
    return this.reverseOrderDao.getAllReverseOrderDetails();
  }

  @Override
  public ReverseOrder getReverseOrderDetailsByReverseOrderById(long reverseOrderId) {
    return this.reverseOrderDao.getReverseOrderDetailsById(reverseOrderId);
  }
}
