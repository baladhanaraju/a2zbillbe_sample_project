package com.a2zbill.services.impl;

import com.a2zbill.dao.BillingPaymentsDao;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingPayments;
import com.a2zbill.services.BillingPaymentsService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BillingPaymentsServiceImpl implements BillingPaymentsService {
  @Autowired BillingPaymentsDao billingPaymentsDao;

  @Override
  public void save(BillingPayments billing) {
    this.billingPaymentsDao.save(billing);
  }

  @Override
  public boolean update(BillingPayments billing) {
    return this.billingPaymentsDao.update(billing);
  }

  @Override
  public Billing getBillingDetailsByBillId(long billId) {
    return this.billingPaymentsDao.getBillingDetailsByBillId(billId);
  }

  @Override
  public Billing getBillingPaymentsAndOrgIdAndBranchId(long billId, long orgId, long branchId) {
    return this.billingPaymentsDao.getBillingPaymentsAndOrgIdAndBranchId(billId, orgId, branchId);
  }

  @Override
  public List<BillingPayments> getBillingPaymentsDetailsByBillId(long billId) {
    //
    return this.billingPaymentsDao.getBillingPaymentsDetailsByBillId(billId);
  }
}
