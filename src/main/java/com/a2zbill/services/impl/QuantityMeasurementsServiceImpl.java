package com.a2zbill.services.impl;

import com.a2zbill.dao.QuantityMeasurementsDao;
import com.a2zbill.domain.QuantityMeasurements;
import com.a2zbill.services.QuantityMeasurementsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class QuantityMeasurementsServiceImpl implements QuantityMeasurementsService {
  @Autowired QuantityMeasurementsDao quantityMeasurementsDao;

  @Override
  public void save(final QuantityMeasurements quantityMeasurements) {
    this.quantityMeasurementsDao.save(quantityMeasurements);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final QuantityMeasurements quantityMeasurements) {
    return false;
  }

  @Override
  public List<QuantityMeasurements> getAllQuantityMeasurements() {
    return this.quantityMeasurementsDao.getAllQuantityMeasurements();
  }

  @Override
  public QuantityMeasurements getQuantityMeasurementsById(final Long id) {
    return this.quantityMeasurementsDao.getQuantityMeasurementsById(id);
  }

  @Override
  public QuantityMeasurements getQuantityMeasurementsByName(final String name) {
    return quantityMeasurementsDao.getQuantityMeasurementsByName(name);
  }
}
