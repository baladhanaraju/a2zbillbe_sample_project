package com.a2zbill.services.impl;

import com.a2zbill.dao.BillingNumDataDao;
import com.a2zbill.domain.BillingNumData;
import com.a2zbill.domain.BillingNumType;
import com.a2zbill.services.BillingNumDataService;
import com.a2zbill.services.BillingNumTypeService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.service.EmployeeService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BillingNumDataServiceImpl implements BillingNumDataService {
  @Autowired BillingNumDataDao billingNumDataDao;
  @Autowired EmployeeService employeeService;
  @Autowired BillingNumTypeService billingNumTypeService;

  @Override
  public void save(final BillingNumData billingNumData) {
    this.billingNumDataDao.save(billingNumData);
  }

  @Override
  public boolean delete(final BillingNumData billingNumData) {
    return this.billingNumDataDao.delete(billingNumData);
  }

  @Override
  public boolean update(final BillingNumData billingNumData) {
    return this.billingNumDataDao.update(billingNumData);
  }

  @Override
  public List<BillingNumData> getAllBillingNumData() {
    return this.billingNumDataDao.getAllBillingNumData();
  }

  @Override
  public BillingNumData getAllBillingNumDataByDateAndOrgBranchId(
      final Date date, final long billingNumberTypeId, final long orgId, final long branchId) {
    return this.billingNumDataDao.getAllBillingNumDataByDateAndOrgBranchId(
        date, billingNumberTypeId, orgId, branchId);
  }

  @Override
  public BillingNumData getAllBillingNumDataByDateExtractAndOrgBranchId(
      final long billingNumberTypeId,
      final int day,
      final int month,
      final int year,
      final long orgId,
      final long branchId) {
    return this.billingNumDataDao.getAllBillingNumDataByDateExtractAndOrgBranchId(
        billingNumberTypeId, day, month, year, orgId, branchId);
  }

  public long saveBillingNumberTypeData(final String billingTypeName, final String emailAddress) {

    try {
      final Employee employeeDetails = this.employeeService.getEmployeeDetailsByEmail(emailAddress);
      final long orgId = employeeDetails.getOrganisation().getId();
      final long branchId = employeeDetails.getBranch().getId();
      final Date currentDate = new Date();
      final Calendar now = Calendar.getInstance();
      now.setTime(currentDate);
      final int month = now.get(Calendar.MONTH) + 1;
      final int year = now.get(Calendar.YEAR);
      final String s = year + "-04" + "-01"; // 2019-05-04
      final String s1 = year - 1 + "-04" + "-01";
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      final Date d = sdf.parse(s);
      final Date d1 = sdf.parse(s1);
      final BillingNumData savebillingNumData = new BillingNumData();
      final BillingNumType billingNumberType =
          this.billingNumTypeService.getBillingNumTypeDetailsByBillingTypeName(billingTypeName);
      if (billingNumberType.getTypeValue().equals("y")) {
        final long billingNumberTypeId = billingNumberType.getId();
        if (month > 3) {
          final BillingNumData billingNumData =
              this.billingNumDataDao.getAllBillingNumDataByDateAndOrgBranchId(
                  d, billingNumberTypeId, orgId, branchId);
          if (billingNumData != null) {
            long currentCount = billingNumData.getCurrentCount() + 1;
            billingNumData.setCurrentCount(currentCount);
            billingNumData.setCreatedDate(currentDate);
            billingNumData.setModifiedDate(currentDate);
            this.billingNumDataDao.update(billingNumData);
            return billingNumData.getCurrentCount();
          } else {
            savebillingNumData.setBillingNumType(billingNumberType);
            savebillingNumData.setCurrentCount(1);
            savebillingNumData.setOrganisation(employeeDetails.getOrganisation());
            savebillingNumData.setBranch(employeeDetails.getBranch());
            savebillingNumData.setBilldate(currentDate);
            savebillingNumData.setCreatedDate(currentDate);
            savebillingNumData.setModifiedDate(currentDate);
            this.billingNumDataDao.save(savebillingNumData);
            return savebillingNumData.getCurrentCount();
          }
        } else {
          final BillingNumData billingNumData =
              this.billingNumDataDao.getAllBillingNumDataByDateExtractAndOrgBranchId(
                  billingNumberTypeId, 01, 04, year - 1, orgId, branchId);
          if (billingNumData != null) {
            billingNumData.setCurrentCount(billingNumData.getCurrentCount() + 1);
            billingNumData.setModifiedDate(currentDate);
            this.billingNumDataDao.update(billingNumData);
            return billingNumData.getCurrentCount();
          } else {
            savebillingNumData.setBillingNumType(billingNumberType);
            savebillingNumData.setCurrentCount(1);
            savebillingNumData.setOrganisation(employeeDetails.getOrganisation());
            savebillingNumData.setBranch(employeeDetails.getBranch());
            savebillingNumData.setBilldate(d1);
            savebillingNumData.setCreatedDate(currentDate);
            savebillingNumData.setModifiedDate(currentDate);
            this.billingNumDataDao.save(savebillingNumData);
            return savebillingNumData.getCurrentCount();
          }
        }
      }
      if (billingNumberType.getTypeValue().equals("d")) {
        final long billingNumTypeId = billingNumberType.getId();
        final BillingNumData billingNumData =
            this.billingNumDataDao.getAllBillingNumDataByDateAndOrgBranchId(
                currentDate, billingNumTypeId, orgId, branchId);
        if (billingNumData != null) {
          final long currentCount = billingNumData.getCurrentCount() + 1;
          billingNumData.setCurrentCount(currentCount);
          billingNumData.setBilldate(currentDate);
          billingNumData.setCreatedDate(currentDate);
          billingNumData.setModifiedDate(currentDate);
          this.billingNumDataDao.update(billingNumData);
          return billingNumData.getCurrentCount();
        } else {
          savebillingNumData.setBillingNumType(billingNumberType);
          savebillingNumData.setCurrentCount(1);
          savebillingNumData.setOrganisation(employeeDetails.getOrganisation());
          savebillingNumData.setBranch(employeeDetails.getBranch());
          savebillingNumData.setBilldate(currentDate);
          savebillingNumData.setCreatedDate(currentDate);
          savebillingNumData.setModifiedDate(currentDate);
          this.billingNumDataDao.save(savebillingNumData);
          return savebillingNumData.getCurrentCount();
        }
      }
    } catch (final Exception e) {

    }
    return 0L;
  }
}
