package com.a2zbill.services.impl;

import com.a2zbill.dao.CancelHistoryDao;
import com.a2zbill.domain.CancelHistory;
import com.a2zbill.services.CancelHistoryService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.service.EmployeeService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CancelHistoryServiceImpl implements CancelHistoryService {

  @Autowired private CancelHistoryDao cancelHistoryDao;

  @Autowired private EmployeeService employeeService;

  @Override
  public void save(final CancelHistory cancelHistory) {
    this.cancelHistoryDao.save(cancelHistory);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final CancelHistory cancelHistory) {
    return this.cancelHistoryDao.update(cancelHistory);
  }

  @Override
  public List<CancelHistory> getAllCancelHistory() {
    return this.cancelHistoryDao.getAllCancelHistory();
  }

  @Override
  public CancelHistory getCancelHistoryById(final Long id) {
    return this.cancelHistoryDao.getCancelHistoryById(id);
  }

  public CancelHistory saveCancelHistory(final String emailAddress) {
    final Employee employeeDetails = this.employeeService.getEmployeeDetailsByEmail(emailAddress);
    final CancelHistory cancelHistory = new CancelHistory();
    cancelHistory.setDate(new Date());
    cancelHistory.setEmployee(employeeDetails);
    cancelHistory.setOrganisation(employeeDetails.getOrganisation());
    cancelHistory.setBranch(employeeDetails.getBranch());
    return null;
  }
}
