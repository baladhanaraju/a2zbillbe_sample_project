package com.a2zbill.services.impl;

import com.a2zbill.dao.InventorySectionDataDao;
import com.a2zbill.domain.InventorySectionData;
import com.a2zbill.services.InventorySectionDataService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventorySectionDataServiceImpl implements InventorySectionDataService {
  @Autowired InventorySectionDataDao inventorySectionDataDao;

  @Override
  public void save(final InventorySectionData inventorySectionData) {
    this.inventorySectionDataDao.save(inventorySectionData);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final InventorySectionData inventorySectionData) {
    return this.inventorySectionDataDao.update(inventorySectionData);
  }

  @Override
  public List<InventorySectionData> getAllInventorySectionData() {
    return this.inventorySectionDataDao.getAllInventorySectionData();
  }

  @Override
  public InventorySectionData getInventorySectionDataByInventorySectionId(
      final long inventorySectionId) {
    return this.inventorySectionDataDao.getInventorySectionDataByInventorySectionId(
        inventorySectionId);
  }

  @Override
  public InventorySectionData getInventorySectionDataByproductIdAndOrgIdBranchId(
      final String inventorysectionName,
      final long productId,
      final long orgId,
      final long branchId) {
    return this.inventorySectionDataDao.getInventorySectionDataByproductIdAndOrgIdBranchId(
        inventorysectionName, productId, orgId, branchId);
  }

  @Override
  public List<InventorySectionData> getAllInventorySectionDataByOrgIdBranchId(
      final long orgId, final long branchId) {
    return this.inventorySectionDataDao.getAllInventorySectionDataByOrgIdBranchId(orgId, branchId);
  }

  @Override
  public List<InventorySectionData> getAllInventorySectionDataByOrgId(final long orgId) {
    return this.inventorySectionDataDao.getAllInventorySectionDataByOrgId(orgId);
  }

  @Override
  public InventorySectionData getInventorySectionDataByproductIdAndOrgId(
      final String inventorysectionName, final long productId, final long orgId) {
    return this.inventorySectionDataDao.getInventorySectionDataByproductIdAndOrgId(
        inventorysectionName, productId, orgId);
  }

  @Override
  public List<InventorySectionData> getInventorySectionDataByFromDateToDateOrgIdAndBranchId(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {
    return this.inventorySectionDataDao.getInventorySectionDataByFromDateToDateOrgIdAndBranchId(
        fromDate, toDate, orgId, branchId);
  }

  @Override
  public List<InventorySectionData> getInventorySectionDataByFromDateToDateOrgId(
      final Date fromDate, final Date toDate, final long orgId) {
    return this.inventorySectionDataDao.getInventorySectionDataByFromDateToDateOrgId(
        fromDate, toDate, orgId);
  }

  @Override
  public List<InventorySectionData> getInventorySectionDataByOrgIdAndBranchIdAndSectionName(
      final long orgId, final long branchId, final String sectionName) {
    return this.inventorySectionDataDao.getInventorySectionDataByOrgIdAndBranchIdAndSectionName(
        orgId, branchId, sectionName);
  }
}
