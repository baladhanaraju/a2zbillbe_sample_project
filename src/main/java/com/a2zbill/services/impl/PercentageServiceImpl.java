package com.a2zbill.services.impl;

import com.a2zbill.dao.PercentageDao;
import com.a2zbill.domain.Percentage;
import com.a2zbill.services.PercentageService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PercentageServiceImpl implements PercentageService {
  @Autowired PercentageDao percentageDao;

  @Override
  public List<Percentage> getAllPercentages() {
    return this.percentageDao.getAllPercentages();
  }
}
