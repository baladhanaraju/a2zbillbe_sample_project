package com.a2zbill.services.impl;

import com.a2zbill.dao.CustomerVendorTokenDao;
import com.a2zbill.domain.CustomerVendorToken;
import com.a2zbill.services.CustomerVendorTokenService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CustomerVendorTokenSeviceImpl implements CustomerVendorTokenService {
  @Autowired CustomerVendorTokenDao customerVendorTokenDao;

  @Override
  public void save(CustomerVendorToken customerVendorToken) {
    this.customerVendorTokenDao.save(customerVendorToken);
  }

  @Override
  public List<CustomerVendorToken> getAllCustomerVendorToken() {
    return this.customerVendorTokenDao.getAllCustomerVendorToken();
  }

  @Override
  public CustomerVendorToken getTokenAndRootNameBymobileNumber(
      final String mobileNumber, final long orgId) {
    return this.customerVendorTokenDao.getTokenAndRootNameBymobileNumber(mobileNumber, orgId);
  }
}
