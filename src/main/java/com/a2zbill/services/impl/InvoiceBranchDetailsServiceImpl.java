package com.a2zbill.services.impl;

import com.a2zbill.dao.InvoiceBranchDetailsDao;
import com.a2zbill.domain.InvoiceBranchDetails;
import com.a2zbill.services.InvoiceBranchDetailsService;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InvoiceBranchDetailsServiceImpl implements InvoiceBranchDetailsService {

  @Autowired InvoiceBranchDetailsDao invoiceBranchDetailsDao;

  @Override
  public void save(InvoiceBranchDetails invoiceBranchDetails) {
    invoiceBranchDetailsDao.save(invoiceBranchDetails);
  }
}
