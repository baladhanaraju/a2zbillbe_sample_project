package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductRewardPointsDao;
import com.a2zbill.domain.ProductRewardPoint;
import com.a2zbill.services.ProductRewardPointsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductRewardPointsServiceImpl implements ProductRewardPointsService {
  @Autowired ProductRewardPointsDao productRewardPointsDao;

  @Override
  public void save(final ProductRewardPoint productRewardPoint) {
    this.productRewardPointsDao.save(productRewardPoint);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final ProductRewardPoint productRewardPoint) {
    return false;
  }

  @Override
  public List<ProductRewardPoint> getAllProductRewardPoints() {
    return this.productRewardPointsDao.getAllProductRewardPoints();
  }

  @Override
  public ProductRewardPoint getProductRewardPointsByproductId(final long productId) {
    return this.productRewardPointsDao.getProductRewardPointsByproductId(productId);
  }

  @Override
  public ProductRewardPoint getAllProductRewardPointsByproductId(
      final long productId, final long orgId, final long branchId) {
    return this.productRewardPointsDao.getAllProductRewardPointsByproductId(
        productId, orgId, branchId);
  }

  @Override
  public ProductRewardPoint getAllProductRewardPointsByproductIdOrgId(
      final long productId, final long orgId) {
    return this.productRewardPointsDao.getAllProductRewardPointsByproductIdOrgId(productId, orgId);
  }
}
