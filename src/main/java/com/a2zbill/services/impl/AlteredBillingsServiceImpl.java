package com.a2zbill.services.impl;

import com.a2zbill.dao.AlteredBillingsDao;
import com.a2zbill.domain.AlteredBillings;
import com.a2zbill.services.AlteredBillingsService;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AlteredBillingsServiceImpl implements AlteredBillingsService {

  @Autowired private AlteredBillingsDao alteredbillingsdao;

  @Override
  public void save(final AlteredBillings alteredbillings) {
    this.alteredbillingsdao.save(alteredbillings);
  }

  @Override
  public Boolean update(final AlteredBillings alteredbillings) {
    return this.alteredbillingsdao.update(alteredbillings);
  }

  @Override
  public Boolean delete(final long id) {
    return false;
  }

  @Override
  public List<AlteredBillings> getAllAlteredBillings() {
    return new ArrayList<>();
  }

  @Override
  public AlteredBillings getAlteredBillingsbyid(final long id) {
    return this.getAlteredBillingsbyid(id);
  }
}
