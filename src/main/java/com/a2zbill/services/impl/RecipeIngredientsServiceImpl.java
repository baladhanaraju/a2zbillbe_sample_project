package com.a2zbill.services.impl;

import com.a2zbill.dao.RecipeIngredientsDao;
import com.a2zbill.domain.RecipeIngredients;
import com.a2zbill.services.RecipeIngredientsService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RecipeIngredientsServiceImpl implements RecipeIngredientsService {

  @Autowired RecipeIngredientsDao recipeIngredientsDao;

  @Override
  public void save(final RecipeIngredients recipeIngredients) {
    recipeIngredientsDao.save(recipeIngredients);
  }

  @Override
  public boolean delete(final Long id) {
    return false;
  }

  @Override
  public boolean update(final RecipeIngredients recipeIngredients) {
    recipeIngredientsDao.update(recipeIngredients);
    return true;
  }

  @Override
  public List<RecipeIngredients> getAllRecipeIngredients() {
    return this.recipeIngredientsDao.getAllRecipeIngredients();
  }

  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByOrgAndBranch(
      final long orgId, final long branchId) {
    return this.recipeIngredientsDao.getAllRecipeIngredientsByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByOrg(final long orgId) {
    return this.recipeIngredientsDao.getAllRecipeIngredientsByOrg(orgId);
  }

  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByRecipeId(final long recipeId) {
    return this.recipeIngredientsDao.getAllRecipeIngredientsByRecipeId(recipeId);
  }

  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByProductId(final long productId) {
    return this.recipeIngredientsDao.getAllRecipeIngredientsByProductId(productId);
  }

  @Override
  public RecipeIngredients getRecipeIngredientsByRecipeIdAndProductId(
      final long recipeId, final long productId) {
    return this.recipeIngredientsDao.getRecipeIngredientsByRecipeIdAndProductId(
        recipeId, productId);
  }
}
