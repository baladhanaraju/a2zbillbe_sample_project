package com.a2zbill.services.impl;

import com.a2zbill.dao.SyncDao;
import com.a2zbill.domain.Sync;
import com.a2zbill.services.SyncService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SyncServiceImpl implements SyncService {
  @Autowired SyncDao syncDao;

  @Override
  public void save(Sync sync) {
    this.syncDao.save(sync);
  }

  @Override
  public boolean delete(Long id) {
    this.syncDao.delete(id);
    return true;
  }

  @Override
  public boolean update(Sync sync) {
    this.syncDao.update(sync);
    return true;
  }

  @Override
  public List<Sync> getAllSyncDetails() {
    return this.syncDao.getAllSyncDetails();
  }

  @Override
  public List<Sync> getAllSyncDetailsByOrgBranchId(long orgId, long branchId) {
    return this.syncDao.getAllSyncDetailsByOrgBranchId(orgId, branchId);
  }

  @Override
  public Sync getSyncDetailsByOrgBranchIdAndSyncUrl(long orgId, long branchId, String syncUrl) {
    return this.syncDao.getSyncDetailsByOrgBranchIdAndSyncUrl(orgId, branchId, syncUrl);
  }

  @Override
  public List<Sync> getAllSyncDetailsByOrg(long orgId) {
    return this.syncDao.getAllSyncDetailsByOrg(orgId);
  }
}
