package com.a2zbill.services.impl;

import com.a2zbill.services.ItemService;
import com.a2zbill.services.ProductService;
import com.tsss.basic.service.EmployeeService;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@SuppressWarnings("serial")
@Service
@Transactional
public class TokenServiceImpl implements Serializable {
  @SuppressWarnings("unused")
  private static final Logger LOGGER = LoggerFactory.getLogger(TokenServiceImpl.class);

  @Autowired EmployeeService employeeService;
  @Autowired ProductService productService;
  @Autowired ItemService itemService;

  public List<Date> getReportDates(JSONObject jsonObject) throws JSONException, ParseException {
    List<Date> dates = new ArrayList<Date>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date(); // from currentDate
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    int month = cal.get(Calendar.MONTH) + 1; // 6
    int year = cal.get(Calendar.YEAR); // 2019
    // if Dates came from frontend
    String fromDate = jsonObject.getString("fromDate");
    String toDate = jsonObject.getString("toDate");
    if (!fromDate.equals("null") || !toDate.equals("null")) {
      if (!toDate.equals("null")) {
        dates.add(sdf.parse(fromDate));
        dates.add(sdf.parse(toDate));
        return dates;
      } else {
        dates.add(sdf.parse(fromDate));
        dates.add(date);
        return dates;
      }
    }
    // if reports came from frontend
    String reports = jsonObject.getString("reports");
    if (reports.equalsIgnoreCase("day")) {
      dates.add(date);
      dates.add(date);
      return dates;
    }
    if (reports.equalsIgnoreCase("week")) {
      cal.set(Calendar.DAY_OF_WEEK, cal.getActualMinimum(Calendar.DAY_OF_WEEK));
      Date firstDayOfTheWeek = cal.getTime();
      dates.add(firstDayOfTheWeek);
      dates.add(date);
      return dates;
    }
    if (reports.equalsIgnoreCase("month")) {
      String monthDate = year + "-" + month + "-" + "01";
      Date fromDate1 = sdf.parse(monthDate);
      dates.add(fromDate1);
      dates.add(date);
      return dates;
    }
    String rangeMin = jsonObject.getString("rangemin");
    String rangeMax = jsonObject.getString("rangemax");
    // if range came from frontend
    if (rangeMin != null && rangeMax != null) {
      cal.add(Calendar.DATE, Integer.parseInt(rangeMin));
      Date rangeStartDate = cal.getTime();
      Calendar call = Calendar.getInstance();
      call.setTime(date);
      call.add(Calendar.DATE, Integer.parseInt(rangeMax));
      Date rangeToDate = call.getTime();
      dates.add(rangeStartDate);
      dates.add(rangeToDate);
      return dates;
    }
    return dates;
  }
}
