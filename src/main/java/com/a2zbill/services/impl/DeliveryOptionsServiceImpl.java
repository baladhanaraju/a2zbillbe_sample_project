package com.a2zbill.services.impl;

import com.a2zbill.dao.DeliveryOptionsDao;
import com.a2zbill.domain.DeliveryOptions;
import com.a2zbill.services.DeliveryOptionsService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class DeliveryOptionsServiceImpl implements DeliveryOptionsService {

  @Autowired private DeliveryOptionsDao deliveryOptionsDao;

  @Override
  public void save(final DeliveryOptions deliveryOptions) {
    this.deliveryOptionsDao.save(deliveryOptions);
  }

  @Override
  public boolean delete(final DeliveryOptions deliveryOptions) {

    return this.deliveryOptionsDao.delete(deliveryOptions);
  }

  @Override
  public boolean update(final DeliveryOptions deliveryOptions) {

    return this.deliveryOptionsDao.update(deliveryOptions);
  }

  @Override
  public DeliveryOptions getDeliveryOptionsByid(final long id) {

    return this.deliveryOptionsDao.getDeliveryOptionsByid(id);
  }

  @Override
  public List<DeliveryOptions> getAllDeliveryOptions() {
    return this.deliveryOptionsDao.getAllDeliveryOptions();
  }
}
