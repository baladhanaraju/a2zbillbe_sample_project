package com.a2zbill.services.impl;

import com.a2zbill.dao.CustomerCreditsDao;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.CustomerCredits;
import com.a2zbill.services.CustomerCreditsService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.paytm.domain.Credits;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerCreditsServiceImpl implements CustomerCreditsService {
  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerCreditsServiceImpl.class);
  @Autowired CustomerCreditsDao customerCreditsDao;
  @Autowired BranchService branchService;
  @Autowired CustomerService customerService;
  @Autowired OrganisationService organisationService;
  @Autowired EmployeeService employeeService;

  @Override
  public void save(final CustomerCredits customerCredits) {
    this.customerCreditsDao.save(customerCredits);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final CustomerCredits customerCredits) {
    return this.customerCreditsDao.update(customerCredits);
  }

  @Override
  public List<CustomerCredits> getAllCustomerCredits() {
    return this.customerCreditsDao.getAllCustomerCredits();
  }

  @Override
  public CustomerCredits getCustomerCreditsById(final Long id) {
    return this.customerCreditsDao.getCustomerCreditsById(id);
  }

  @Override
  public CustomerCredits getCustomerCreditsByCustomerId(final Long customerId) {
    return this.customerCreditsDao.getCustomerCreditsByCustomerId(customerId);
  }

  public void savingCustomerCreditDetails(
      final long employeeNumber, final String message, final Billing billing) throws JSONException {
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final JSONObject customerHistoryDetails = new JSONObject(message);
    final CustomerCredits customerCredits = new CustomerCredits();
    try {
      final Branch branch = this.branchService.getBranchById(employeeDetails.getBranch().getId());
      customerCredits.setBranch(branch);
      final Organisation organisation =
          this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
      customerCredits.setOrganisation(organisation);
      customerCredits.setCustomer(billing.getCustomer());
      customerCredits.setCreditAmount(new BigDecimal(customerHistoryDetails.getInt("debitAmount")));
      customerCredits.setDebitAmount(new BigDecimal(customerHistoryDetails.getInt("debitAmount")));
      customerCredits.setCreateDate(new Date(System.currentTimeMillis()));
      customerCredits.setModifiedDate(new Date(System.currentTimeMillis()));
      save(customerCredits);
    } catch (final Exception ex) {
      LOGGER.error("savingCustomerCreditDetails" + ex, ex);
    }
  }

  @Override
  public Credits getAllCreditsByOrgidCustomer(final long orgid) {

    return customerCreditsDao.getAllCreditsByOrgidCustomer(orgid);
  }
}
