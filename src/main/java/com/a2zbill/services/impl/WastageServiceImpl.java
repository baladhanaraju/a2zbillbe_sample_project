package com.a2zbill.services.impl;

import com.a2zbill.dao.WastageDao;
import com.a2zbill.domain.Wastage;
import com.a2zbill.services.WastageService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class WastageServiceImpl implements WastageService {

  @Autowired WastageDao wastageDao;

  @Override
  public void save(Wastage wastage) {
    this.wastageDao.save(wastage);
  }

  @Override
  public Boolean update(Wastage wastage) {
    this.wastageDao.update(wastage);
    return true;
  }

  @Override
  public Boolean delete(long id) {
    this.wastageDao.delete(id);
    return true;
  }

  @Override
  public List<Wastage> getAllWastages() {

    return this.wastageDao.getAllWastages();
  }
}
