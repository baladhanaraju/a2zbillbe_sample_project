package com.a2zbill.services.impl;

import com.a2zbill.dao.RetailerDao;
import com.a2zbill.domain.Retailer;
import com.a2zbill.services.RetailerService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RetailerServiceImpl implements RetailerService {
  @Autowired RetailerDao retailerDao;

  @Override
  public void save(final Retailer retailer) {
    this.retailerDao.save(retailer);
  }

  @Override
  public List<Retailer> getRetailerDetails() {
    return this.retailerDao.getRetailerDetails();
  }

  @Override
  public Retailer getRetailerDetailsByGSTNumber(final String retailerGstNumber) {
    return this.retailerDao.getRetailerDetailsByGSTNumber(retailerGstNumber);
  }
}
