package com.a2zbill.services.impl;

import com.a2zbill.dao.OrderLineItemsDao;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.DiscountCoupons;
import com.a2zbill.domain.LooseProduct;
import com.a2zbill.domain.Order;
import com.a2zbill.domain.OrderLineItems;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.Review;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.DiscountCouponsService;
import com.a2zbill.services.LooseProductService;
import com.a2zbill.services.OrderLineItemsService;
import com.a2zbill.services.OrderService;
import com.a2zbill.services.ReviewService;
import com.offers.domain.CustomerTransactionSummary;
import com.offers.domain.Offers;
import com.offers.domain.Reward;
import com.offers.domain.RewardHistory;
import com.offers.services.CustomerTransactionSummaryService;
import com.offers.services.OffersService;
import com.offers.services.RewardHistoryService;
import com.offers.services.RewardService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OrderLineItemsServiceImpl implements OrderLineItemsService {

  private static final Logger LOGGER = LoggerFactory.getLogger(OrderLineItemsServiceImpl.class);

  private final String LooseProductCode = "#w";

  private static final String ORDER_STATUS = "Open";
  private final BigDecimal HUNDRED = new BigDecimal(100);

  @SuppressWarnings("unused")
  private final BigDecimal TEN = new BigDecimal(10);

  private final boolean STATUS_TRUE = true;

  @SuppressWarnings("unused")
  private final boolean STATUS_FALSE = false;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private LooseProductService looseProductService;

  @Autowired private EmployeeService employeeService;

  @Autowired private BranchService branchService;

  @Autowired private DiscountCouponsService discountCouponsService;

  @Autowired private RewardService rewardService;

  @Autowired private OrganisationService organisationService;

  @Autowired private BillingService billingService;

  @Autowired private CounterService counterService;

  @Autowired private ProductServiceImpl ProductsService;

  @Autowired private OffersService offersService;

  @Autowired private ReviewService reviewService;

  @Autowired private CustomerTransactionSummaryService customerrewardsService;

  @Autowired private RewardHistoryService rewardHistoryService;

  @Autowired private OrderService orderService;

  @Autowired private OrderLineItemsService orderlineitemsService;
  @Autowired private CustomerService customerService;

  @Autowired private OrderLineItemsDao orderLineItemsDao;

  @Override
  public void save(final OrderLineItems orderLineItems) {
    this.orderLineItemsDao.save(orderLineItems);
  }

  @Override
  public boolean delete(final long id) {
    this.orderLineItemsDao.delete(id);
    return true;
  }

  @Override
  public boolean update(final OrderLineItems orderLineItems) {
    this.orderLineItemsDao.update(orderLineItems);
    return true;
  }

  @Override
  public List<OrderLineItems> getAllOrderLineItems() {
    return this.orderLineItemsDao.getAllOrderLineItems();
  }

  @Override
  public List<OrderLineItems> getOrderLineItemsByorderId(final long orderId) {
    return this.orderLineItemsDao.getOrderLineItemsByorderId(orderId);
  }

  @Override
  public List<OrderLineItems> getOrderLineItemsByCounterIdAndFlowId() {
    return orderLineItemsDao.getOrderLineItemsByCounterIdAndFlowId();
  }

  @Override
  public OrderLineItems getOrderLineItemsByProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode) {
    return orderLineItemsDao.getOrderLineItemsByProductNameAndHsnCode(
        counterId, productName, hsnCode);
  }

  @Override
  public OrderLineItems getOrderDetailsByCounterIdProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode) {
    return orderLineItemsDao.getOrderDetailsByCounterIdProductNameAndHsnCode(
        counterId, productName, hsnCode);
  }

  @Override
  public List<OrderLineItems> getOrderLineItemsGroupByProductNameifBrnachNull(
      final Date fromDate, final Date toDate, final long orgId) {
    return orderLineItemsDao.getOrderLineItemsGroupByProductNameifBrnachNull(
        fromDate, toDate, orgId);
  }

  @Override
  public List<OrderLineItems> getOrderLineItemsGroupByProductName(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {
    return orderLineItemsDao.getOrderLineItemsGroupByProductName(fromDate, toDate, orgId, branchId);
  }

  @Override
  public List<OrderLineItems> getproductQuantityByBranchAndOrgIdAndDate(
      final long orgId, final long branchId, final Date fromDate, final Date toDate) {
    return orderLineItemsDao.getproductQuantityByBranchAndOrgIdAndDate(
        orgId, branchId, fromDate, toDate);
  }

  @Override
  public List<OrderLineItems> getproductQuantityByOrgIdAndDate(
      final long orgId, final Date fromDate, final Date toDate) {
    return orderLineItemsDao.getproductQuantityByOrgIdAndDate(orgId, fromDate, toDate);
  }

  public Map<String, Object> addingProductToCart(
      final long orgId, final long branchId, final String code) throws JSONException {
    final Long QTY = 1L;
    Product productDetails = null;
    final Map<String, Object> productDetails1 = new HashMap<String, Object>();
    try {
      if (code.contains(this.LooseProductCode)) {
        final String[] codes = code.split(this.LooseProductCode);
        final String loosecode = codes[1];
        @SuppressWarnings("unused")
        final LooseProduct LooseProduct =
            this.looseProductService.getLooseProductDetailsByCode(loosecode);
      } else {
        if (branchId != 0l) {
          productDetails =
              this.ProductsService.getProductsByProductCodeAndOrgIdAndBranchId(
                  code, orgId, branchId);
          if (productDetails == null) {
            productDetails1.put(
                "product", "Products Not Available For this Organisation and Branch");
            return productDetails1;
          }
        } else {
          productDetails = this.ProductsService.getProductsByProductCodeAndOrgId(code, orgId);
        }
      }
      if (productDetails != null) {
        productDetails1.put("ProductCode", productDetails.getCode());
        productDetails1.put("ProductName", productDetails.getProductName());
        productDetails1.put("HsnNumber", productDetails.getHsnNumber());
        productDetails1.put("cartId", 0);
        if (productDetails.getIncludingTaxFlag().equals("true")) {
          final BigDecimal price =
              productDetails
                  .getProductPrice()
                  .subtract(
                      (productDetails.getProductPrice())
                          .multiply(productDetails.getSGST().add(productDetails.getCGST()))
                          .divide(this.HUNDRED));
          productDetails1.put("ProductPrice", price);
          productDetails1.put("SGST", productDetails.getSGST());
          productDetails1.put("CGST", productDetails.getCGST());
        } else {
          productDetails1.put("ProductPrice", productDetails.getProductPrice());
          productDetails1.put("SGST", productDetails.getSGST());
          productDetails1.put("CGST", productDetails.getCGST());
        }
        productDetails1.put("Quantity", QTY);
        productDetails1.put("IGST", productDetails.getIGST());
        return productDetails1;
      } else {
        productDetails1.put("product", "Products Not Available");
        return productDetails1;
      }
    } catch (final Exception ex) {
      LOGGER.error("gettingRestaurentCartProductDetails1" + ex, ex);
    }
    return productDetails1;
  }

  public Map<String, Long> generateBill(
      final HttpServletRequest request, final HttpServletResponse responce, final String payload)
      throws JSONException {
    final Map<String, Long> map = new HashMap<String, Long>();
    final Billing billing = new Billing();
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Review review = new Review();
    final long orgId = employeeDetails.getOrganisation().getId();
    String offerCode = request.getParameter("offerCode");
    if (offerCode == null) {
      LOGGER.error("offerCode does not exist");
    }
    final long counterNumber = Long.parseLong(request.getParameter("counterNumber"));
    final long flowsId = Long.parseLong(request.getParameter("flowsId"));
    final String splitbillFlag = request.getParameter("splitbillFlag");
    final Counter counterDetails = this.counterService.getCounterDetailsById(counterNumber);
    Order ordersDetails = new Order();
    if (flowsId == 3) {
      Order order = new Order();
      if (splitbillFlag.equals("true")) {
        long orderId = Long.parseLong(request.getParameter("orderId"));
        order = this.orderService.getOrderById(orderId);
        if (order.getOrderStatus().equals("Open")) {
          // order.setOrderStatus("Closed");
          this.orderService.update(order);
        }
        if (orgId == order.getOrganisation().getId()) {
          if (order.getGuid() != null) {
            order = this.orderService.getOrderByGuid("guid");
          } else {
            order = new Order();
            // order.setOrderStatus(ORDER_STATUS);
            final Date date = new Date();
            order.setCreatedTs(date);
            order.setModifiedTs(date);
            order.setCounterDetails(counterDetails);
            this.orderService.save(order);
            orderId = order.getId();
            final JSONArray ja = new JSONArray(payload);
            for (int i = 0; i < ja.length(); ++i) {
              final JSONObject jsonObject = ja.getJSONObject(i);
              final JSONObject products = jsonObject.getJSONObject("code");
              final OrderLineItems orderlineitemsDetails = new OrderLineItems();
              orderlineitemsDetails.setOrderId(order);
              orderlineitemsDetails.setCounterDetails(order.getCounterDetails());
              orderlineitemsDetails.setDiscountAmount(null);
              orderlineitemsDetails.setHsnCode(products.getString("HsnNumber"));
              orderlineitemsDetails.setCgst(new BigDecimal(products.getInt("CGST")));
              orderlineitemsDetails.setSgst(new BigDecimal(products.getInt("SGST")));
              orderlineitemsDetails.setProductName(products.getString("ProductName"));
              orderlineitemsDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
              orderlineitemsDetails.setProductPrice(
                  new BigDecimal(products.getInt("ProductPrice")));
              final BigDecimal totalAmount =
                  new BigDecimal(products.getInt("ProductPrice"))
                      .multiply(new BigDecimal(products.getString("Quantity")));
              final BigDecimal taxableAmount =
                  new BigDecimal(products.getInt("CGST"))
                      .add(new BigDecimal(products.getInt("SGST")))
                      .multiply(new BigDecimal(products.getInt("ProductPrice")))
                      .divide(this.HUNDRED);
              orderlineitemsDetails.setTotalAmount(totalAmount);
              orderlineitemsDetails.setTaxableAmount(
                  taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
              final Date CartTime = new Date();
              orderlineitemsDetails.setTime(CartTime);
              final Branch branch =
                  this.branchService.getBranchById(employeeDetails.getBranch().getId());
              orderlineitemsDetails.setBranchId(branch);
              final Organisation organisation =
                  this.organisationService.getOrganisationById(
                      employeeDetails.getOrganisation().getId());
              orderlineitemsDetails.setOrganisation(organisation);
              this.orderlineitemsService.save(orderlineitemsDetails);
            }
          }
        } else {
          final JSONArray ja = new JSONArray(payload);
          for (int i = 0; i < ja.length(); ++i) {
            final JSONObject jsonObject = ja.getJSONObject(i);
            final JSONObject products = jsonObject.getJSONObject("code");
            review.setGuid(products.getString("guid"));
            review.setParentGuid(products.getString("parentGuid"));
            review.setBillDate(new Date());
            final Branch branch =
                this.branchService.getBranchById(employeeDetails.getBranch().getId());
            review.setBranch(branch);
            final Organisation organisation =
                this.organisationService.getOrganisationById(
                    employeeDetails.getOrganisation().getId());
            review.setOrganisation(organisation);
            review.setBillAmount(new BigDecimal(products.getInt("billingAmount")));
            review.setTaxableAmount(new BigDecimal(products.getInt("totalTaxableAmount")));
            this.reviewService.update(review);
          }
        }
      } else {
        order = this.orderService.getOrdersDetailsByStatus(ORDER_STATUS, counterNumber);
        if (order != null) {
          // order.setOrderStatus(ORDER_STATUS);
          order.setCounterDetails(counterDetails);
          this.orderService.save(order);
        }
      }
      final String customerMobileNumber = request.getParameter("customerMobileNumber");
      if (splitbillFlag.equals("true")) {
        billing.setOrder(order);
      }
      final String discountPercentage = request.getParameter("discountPercentage");
      final String discountAmount = request.getParameter("discountAmount");
      final BigDecimal sumOfTotalAmount = new BigDecimal(request.getParameter("netAmount"));
      final String sumOfTaxableAmount = request.getParameter("taxableAmount");
      final String sumOfcgstAmount = request.getParameter("cgstAmount");
      final String sumOfsgstAmount = request.getParameter("sgstAmount");
      billing.setTotalAmount(sumOfTotalAmount);
      billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));
      billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
      billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
      billing.setBillDate(new Date());
      billing.setCounterDetails(order.getCounterDetails());
      if (customerMobileNumber.equals("")) {
        billing.setCustomer(null);
      } else {
        billing.setCustomer(this.customerService.getCustomerByMobileNumber(customerMobileNumber));
        final Integer amount =
            this.billingService.getOffersByOfferCodeAndCustId(
                offerCode, customerMobileNumber, sumOfTotalAmount);
        final Long disAmount =
            Long.valueOf((sumOfTotalAmount.subtract(new BigDecimal(amount))).longValue());
        billing.setTotalAmount(sumOfTotalAmount);
        final List<Reward> rewards = this.rewardService.getrewardsBystatus(this.STATUS_TRUE);
        for (final Reward reward : rewards) {
          if (reward.getActive() == STATUS_TRUE) {
            final CustomerTransactionSummary customerReward = new CustomerTransactionSummary();
            final BigDecimal TotalAmount = sumOfTotalAmount;
            final long rewardpoints = TotalAmount.divide(this.HUNDRED).longValue();
            customerReward.setCustomer(billing.getCustomer().getCustomerId());
            customerReward.setRewardPoints(rewardpoints);
            customerReward.setCreateDate(new Date());
            customerReward.setModifiedDate(new Date());
            this.customerrewardsService.save(customerReward);
            final RewardHistory rewardHistory = new RewardHistory();
            final Date date = new Date();
            rewardHistory.setCustomer(billing.getCustomer());
            rewardHistory.setRewardPoints(rewardpoints);
            rewardHistory.setRedeemAmount(rewardpoints);
            rewardHistory.setDate(new Date());
            rewardHistory.setStartTime(date);
            this.rewardHistoryService.save(rewardHistory);
          }
        }
        map.put("discountAmount", disAmount);
      }
      billing.setEmployeeDetails(employeeDetails);
      billing.setDiscountPercentage(new BigDecimal(discountPercentage));
      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setOrganisation(employeeDetails.getOrganisation());
      billing.setBranch(employeeDetails.getBranch());
      billing.setOrder(order);
      this.billingService.save(billing);
      // order.setOrderStatus("Closed");
      this.orderService.update(order);
      counterDetails.setCounterStatus("Closed");
      this.counterService.update(counterDetails);
      map.put("billingId", this.billingService.getBillingDetailsByOrdersId(order.getId()).getId());
      return map;
    } else if (flowsId == 2) {
      final Order ordersDetails1 = new Order();
      // ordersDetails1.setOrderStatus(ORDER_STATUS);
      Date date = new Date();
      ordersDetails1.setCounterDetails(counterDetails);
      this.orderService.save(ordersDetails1);
      final JSONArray ja = new JSONArray(payload);
      for (int i = 0; i < ja.length(); ++i) {
        final JSONObject jsonObject = ja.getJSONObject(i);
        final JSONObject products = jsonObject.getJSONObject("code");
        final Product product =
            this.ProductsService.getProductsByHsnCodeAndProductNameByBranchIdAndOrgId(
                products.getString("HsnNumber"), products.getString("ProductName"),
                employeeDetails.getBranch().getId(), employeeDetails.getOrganisation().getId());
        final long id = ordersDetails1.getId();
        final Order order = this.orderService.getOrderById(id);
        final OrderLineItems orderlineitemsDetails = new OrderLineItems();
        orderlineitemsDetails.setOrderId(order);
        orderlineitemsDetails.setCounterDetails(order.getCounterDetails());
        orderlineitemsDetails.setDiscountAmount(null);
        orderlineitemsDetails.setHsnCode(products.getString("HsnNumber"));
        orderlineitemsDetails.setCgst(new BigDecimal(products.getInt("CGST")));
        orderlineitemsDetails.setSgst(new BigDecimal(products.getInt("SGST")));
        orderlineitemsDetails.setProductName(products.getString("ProductName"));
        orderlineitemsDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
        orderlineitemsDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));
        final BigDecimal totalAmount =
            new BigDecimal(products.getInt("ProductPrice"))
                .multiply(new BigDecimal(products.getString("Quantity")));
        final BigDecimal taxableAmount =
            new BigDecimal(products.getInt("CGST"))
                .add(new BigDecimal(products.getInt("SGST")))
                .multiply(new BigDecimal(products.getInt("ProductPrice")))
                .divide(this.HUNDRED);
        orderlineitemsDetails.setTotalAmount(totalAmount);
        orderlineitemsDetails.setTaxableAmount(
            taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
        final Branch branch = this.branchService.getBranchById(employeeDetails.getBranch().getId());
        orderlineitemsDetails.setBranchId(branch);
        final Organisation organisation =
            this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
        orderlineitemsDetails.setOrganisation(organisation);
        this.orderlineitemsService.save(orderlineitemsDetails);
        final String kafkadata =
            employeeDetails.getBranch().getId()
                + "|"
                + products.getString("ProductName")
                + "|"
                + products.getString("Quantity")
                + "|"
                + counterNumber
                + "|"
                + employeeDetails.getId()
                + "|"
                + product.getImage();
        LOGGER.info("parcel flow details sending to kafka:" + kafkadata);
        try {
          LOGGER.info("parcel flow details sent to kafka:" + kafkadata);
        } catch (final Exception ex) {
          LOGGER.error("parcel flow details send to kafka:" + ex, ex);
        }
      }
      long ordersId = ordersDetails.getId();
      final Order order = this.orderService.getOrderById(ordersId);
      final String customerMobileNumber = request.getParameter("customerMobileNumber");
      billing.setOrder(order);
      final String discountPercentage = request.getParameter("discountPercentage");
      final String discountAmount = request.getParameter("discountAmount");
      final BigDecimal sumOfTotalAmount = new BigDecimal(request.getParameter("netAmount"));
      final String sumOfTaxableAmount = request.getParameter("taxableAmount");
      final String sumOfcgstAmount = request.getParameter("cgstAmount");
      final String sumOfsgstAmount = request.getParameter("sgstAmount");
      billing.setTotalAmount(sumOfTotalAmount);
      billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));
      billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
      billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
      billing.setBillDate(new Date());
      billing.setCounterDetails(order.getCounterDetails());
      if (customerMobileNumber.equals("")) {
        billing.setCustomer(null);
      } else {
        billing.setCustomer(this.customerService.getCustomerByMobileNumber(customerMobileNumber));
        final Integer amount =
            this.billingService.getOffersByOfferCodeAndCustId(
                offerCode, customerMobileNumber, sumOfTotalAmount);
        final Long disAmount =
            Long.valueOf((sumOfTotalAmount.subtract(new BigDecimal(amount))).longValue());
        billing.setTotalAmount(sumOfTotalAmount);
        ordersId = ordersDetails.getId();

        if (offerCode != null) {
          if (offerCode.startsWith("F")) {
            final Offers offer = this.offersService.getOffersByOfferCode(offerCode);
            if (offer.getDiscountType().getId() == 1) {
              final BigDecimal bill =
                  sumOfTotalAmount.multiply((offer.getDiscountValue()).divide(this.HUNDRED));
              final BigDecimal billWithOffer = sumOfTotalAmount.subtract(bill);
              billing.setTotalAmount(billWithOffer);
            } else if (offer.getDiscountType().getId() == 2) {
              final BigDecimal bill = sumOfTotalAmount.subtract(offer.getDiscountValue());
              billing.setTotalAmount(bill);
            }
          } else if (offerCode.startsWith("p")) {
            @SuppressWarnings("unused")
            final DiscountCoupons discountCoupons =
                this.discountCouponsService.getDiscountCouponsByOfferCode(offerCode);
          }
        }
        final List<Reward> rewards = this.rewardService.getrewardsBystatus(this.STATUS_TRUE);
        for (final Reward reward : rewards) {
          if (reward.getActive() == STATUS_TRUE) {
            final CustomerTransactionSummary customerReward = new CustomerTransactionSummary();
            final BigDecimal TotalAmount = sumOfTotalAmount;
            final long rewardpoints = TotalAmount.divide(this.HUNDRED).longValue();
            customerReward.setCustomer(billing.getCustomer().getCustomerId());
            customerReward.setRewardPoints(rewardpoints);
            customerReward.setCreateDate(new Date());
            customerReward.setModifiedDate(new Date());
            this.customerrewardsService.save(customerReward);
            final RewardHistory rewardHistory = new RewardHistory();
            date = new Date();
            rewardHistory.setCustomer(billing.getCustomer());
            rewardHistory.setRewardPoints(rewardpoints);
            rewardHistory.setRedeemAmount(rewardpoints);
            rewardHistory.setDate(new Date());
            rewardHistory.setStartTime(date);
            this.rewardHistoryService.save(rewardHistory);
          }
        }
        map.put("discountAmount", disAmount);
      }
      billing.setEmployeeDetails(employeeDetails);
      billing.setDiscountPercentage(new BigDecimal(discountPercentage));
      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setOrganisation(employeeDetails.getOrganisation());
      billing.setBranch(employeeDetails.getBranch());
      this.billingService.save(billing);
      // order.setOrderStatus("Closed");
      this.orderService.update(order);
      map.put("billingId", this.billingService.getBillingDetailsByOrdersId(ordersId).getId());
      return map;
    } else {
      ordersDetails = new Order();
      //  ordersDetails.setOrderStatus(ORDER_STATUS);
      Date date = new Date();
      ordersDetails.setCreatedTs(date);
      ordersDetails.setModifiedTs(date);
      ordersDetails.setCounterDetails(counterDetails);
      this.orderService.save(ordersDetails);
      final JSONArray ja = new JSONArray(payload);
      for (int i = 0; i < ja.length(); ++i) {
        final JSONObject jsonObject = ja.getJSONObject(i);
        final JSONObject products = jsonObject.getJSONObject("code");
        final long id = ordersDetails.getId();
        final Order order = this.orderService.getOrderById(id);
        final OrderLineItems orderlineitemsDetails = new OrderLineItems();
        orderlineitemsDetails.setOrderId(order);
        orderlineitemsDetails.setCounterDetails(order.getCounterDetails());
        orderlineitemsDetails.setDiscountAmount(null);
        orderlineitemsDetails.setHsnCode(products.getString("HsnNumber"));
        orderlineitemsDetails.setCgst(new BigDecimal(products.getInt("CGST")));
        orderlineitemsDetails.setSgst(new BigDecimal(products.getInt("SGST")));
        orderlineitemsDetails.setProductName(products.getString("ProductName"));
        orderlineitemsDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
        orderlineitemsDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));
        final BigDecimal totalAmount =
            new BigDecimal(products.getInt("ProductPrice"))
                .multiply(new BigDecimal(products.getString("Quantity")));
        final BigDecimal taxableAmount =
            new BigDecimal(products.getInt("CGST"))
                .add(new BigDecimal(products.getInt("SGST")))
                .multiply(new BigDecimal(products.getInt("ProductPrice")))
                .divide(this.HUNDRED);
        orderlineitemsDetails.setTotalAmount(totalAmount);
        orderlineitemsDetails.setTaxableAmount(
            taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
        final Date OrdersTime = new Date();
        orderlineitemsDetails.setTime(OrdersTime);
        final Branch branch = this.branchService.getBranchById(employeeDetails.getBranch().getId());
        orderlineitemsDetails.setBranchId(branch);
        final Organisation organisation =
            this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
        orderlineitemsDetails.setOrganisation(organisation);
        this.orderlineitemsService.save(orderlineitemsDetails);
      }
      long ordersId = ordersDetails.getId();
      final Order order = this.orderService.getOrderById(ordersId);
      final String customerMobileNumber = request.getParameter("customerMobileNumber");
      billing.setOrder(order);
      final String discountPercentage = request.getParameter("discountPercentage");
      final String discountAmount = request.getParameter("discountAmount");
      final BigDecimal sumOfTotalAmount = new BigDecimal(request.getParameter("netAmount"));
      final String sumOfTaxableAmount = request.getParameter("taxableAmount");
      final String sumOfcgstAmount = request.getParameter("cgstAmount");
      final String sumOfsgstAmount = request.getParameter("sgstAmount");
      billing.setTotalAmount(sumOfTotalAmount);
      billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));
      billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
      billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
      billing.setBillDate(new Date());
      billing.setCounterDetails(order.getCounterDetails());
      if (customerMobileNumber.equals("")) {
        billing.setCustomer(null);
      } else {
        billing.setCustomer(this.customerService.getCustomerByMobileNumber(customerMobileNumber));
        final Integer amount =
            this.billingService.getOffersByOfferCodeAndCustId(
                offerCode, customerMobileNumber, sumOfTotalAmount);

        final Long disAmount =
            Long.valueOf((sumOfTotalAmount.subtract(new BigDecimal(amount))).longValue());
        billing.setTotalAmount(sumOfTotalAmount);
        ordersId = ordersDetails.getId();
        if (offerCode != null) {
          if (offerCode.startsWith("F")) {
            final Offers offer = this.offersService.getOffersByOfferCode(offerCode);
            if (offer.getDiscountType().getId() == 1) {
              final BigDecimal bill =
                  sumOfTotalAmount.multiply((offer.getDiscountValue()).divide(this.HUNDRED));
              final BigDecimal billWithOffer = sumOfTotalAmount.subtract(bill);
              billing.setTotalAmount(billWithOffer);
            } else if (offer.getDiscountType().getId() == 2) {
              final BigDecimal bill = sumOfTotalAmount.subtract(offer.getDiscountValue());
              billing.setTotalAmount(bill);
            }
          } else if (offerCode.startsWith("p")) {
            @SuppressWarnings("unused")
            final DiscountCoupons discountCoupons =
                this.discountCouponsService.getDiscountCouponsByOfferCode(offerCode);
          }
        }
        final List<Reward> rewards = this.rewardService.getrewardsBystatus(this.STATUS_TRUE);
        for (final Reward reward : rewards) {
          if (reward.getActive() == STATUS_TRUE) {
            final CustomerTransactionSummary customerReward = new CustomerTransactionSummary();
            final BigDecimal TotalAmount = sumOfTotalAmount;
            final long rewardpoints = TotalAmount.divide(this.HUNDRED).longValue();
            customerReward.setCustomer(billing.getCustomer().getCustomerId());
            customerReward.setRewardPoints(rewardpoints);
            customerReward.setCreateDate(new Date());
            customerReward.setModifiedDate(new Date());
            this.customerrewardsService.save(customerReward);
            final RewardHistory rewardHistory = new RewardHistory();
            date = new Date();
            rewardHistory.setCustomer(billing.getCustomer());
            rewardHistory.setRewardPoints(rewardpoints);
            rewardHistory.setRedeemAmount(rewardpoints);
            rewardHistory.setDate(new Date());
            rewardHistory.setStartTime(date);
            this.rewardHistoryService.save(rewardHistory);
          }
        }
        map.put("discountAmount", disAmount);
      }
      billing.setEmployeeDetails(employeeDetails);
      billing.setDiscountPercentage(new BigDecimal(discountPercentage));
      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setOrganisation(employeeDetails.getOrganisation());
      billing.setBranch(employeeDetails.getBranch());
      this.billingService.save(billing);
      // order.setOrderStatus("Closed");
      this.orderService.update(order);
      map.put("billingId", this.billingService.getBillingDetailsByOrdersId(ordersId).getId());
      return map;
    }
  }

  @Override
  public OrderLineItems getOrderLineItemsById(final long orderLineItemId) {
    return orderLineItemsDao.getOrderLineItemsById(orderLineItemId);
  }

  @Override
  public OrderLineItems getOrderLineItemsByIdAndProductName(
      final long orderId, final String productName) {
    return orderLineItemsDao.getOrderLineItemsByIdAndProductName(orderId, productName);
  }

  @Override
  public List<OrderLineItems> getOrderLineItemsByProductName(
      String productName, long orgId, long branchId) {
    return orderLineItemsDao.getOrderLineItemsByProductName(productName, orgId, branchId);
  }
}
