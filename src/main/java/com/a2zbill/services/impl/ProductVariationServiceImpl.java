package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductVariationDao;
import com.a2zbill.domain.ProductVariation;
import com.a2zbill.services.ProductVariationService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductVariationServiceImpl implements ProductVariationService {

  @Autowired ProductVariationDao productVariationDao;

  @Override
  public void save(ProductVariation productVariation) {
    this.productVariationDao.save(productVariation);
  }

  @Override
  public boolean delete(long id) {
    return this.productVariationDao.delete(id);
  }

  @Override
  public boolean update(ProductVariation productVariation) {
    this.productVariationDao.update(productVariation);
    return true;
  }

  @Override
  public List<ProductVariation> getAllProductVariationDetails() {
    return this.productVariationDao.getAllProductVariationDetails();
  }

  @Override
  public List<ProductVariation> getProductVariationDetailsByOrgIdAndBranchId(
      long orgId, long branchId) {
    return this.productVariationDao.getProductVariationDetailsByOrgIdAndBranchId(orgId, branchId);
  }

  @Override
  public List<ProductVariation> getProductVariationDetailsByOrgId(long orgId) {
    return this.productVariationDao.getProductVariationDetailsByOrgId(orgId);
  }

  @Override
  public ProductVariation getProductVariationDetailsByProductVariationId(long productVariationId) {
    return this.productVariationDao.getProductVariationDetailsByProductVariationId(
        productVariationId);
  }

  @Override
  public List<Object[]> getproductVariationDetails(long orgId, long branchId) {

    return this.productVariationDao.getproductVariationDetails(orgId, branchId);
  }

  @Override
  public List<Object[]> getproductVariationDetailsByOrg(long orgId) {
    return this.productVariationDao.getproductVariationDetailsByOrg(orgId);
  }

  @Override
  public List<ProductVariation> getProductVariationByProductIdAndOrgBranchId(
      long productId, long orgId, long branchId) {

    return this.productVariationDao.getProductVariationByProductIdAndOrgBranchId(
        productId, orgId, branchId);
  }

  @Override
  public List<ProductVariation> getProductVariationByProductIdAndOrg(long productId, long orgId) {

    return this.productVariationDao.getProductVariationByProductIdAndOrg(productId, orgId);
  }
}
