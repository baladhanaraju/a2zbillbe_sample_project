package com.a2zbill.services.impl;

import com.a2zbill.dao.LooseProductDao;
import com.a2zbill.domain.LooseProduct;
import com.a2zbill.services.LooseProductService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class LooseProductServiceImpl implements LooseProductService {
  @Autowired LooseProductDao looseProductDao;

  @Override
  public void save(LooseProduct looseProduct) {
    this.looseProductDao.save(looseProduct);
  }

  @Override
  public List<LooseProduct> getAllLooseProductDetails() {
    return this.looseProductDao.getAllLooseProductDetails();
  }

  @Override
  public LooseProduct getLooseProductDetailsByCode(final String looseProductCode) {
    return this.looseProductDao.getLooseProductDetailsByCode(looseProductCode);
  }

  @Override
  public boolean update(LooseProduct looseProduct) {
    return this.looseProductDao.update(looseProduct);
  }

  @Override
  public LooseProduct getLooseProductDetailsByHsnCode(final String looseProductHsnCode) {
    return this.looseProductDao.getLooseProductDetailsByHsnCode(looseProductHsnCode);
  }

  @Override
  public LooseProduct getLooseProductDetailsById(final long looseProductId) {
    return this.looseProductDao.getLooseProductDetailsById(looseProductId);
  }
}
