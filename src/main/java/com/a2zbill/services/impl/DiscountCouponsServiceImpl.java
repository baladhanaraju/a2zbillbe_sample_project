package com.a2zbill.services.impl;

import com.a2zbill.dao.DiscountCouponsDao;
import com.a2zbill.domain.DiscountCoupons;
import com.a2zbill.services.DiscountCouponsService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DiscountCouponsServiceImpl implements DiscountCouponsService {

  @Autowired private DiscountCouponsDao discountCouponsDao;

  @Override
  public void save(final DiscountCoupons discountCoupons) {
    this.discountCouponsDao.save(discountCoupons);
  }

  @Override
  public boolean delete(final Long id) {
    return this.discountCouponsDao.delete(id);
  }

  @Override
  public boolean update(final DiscountCoupons productDeals) {
    this.discountCouponsDao.update(productDeals);
    return true;
  }

  @Override
  public DiscountCoupons getDiscountCouponsById(final Long id) {
    return null;
  }

  @Override
  public List<DiscountCoupons> getAllDiscountCoupons() {
    return this.discountCouponsDao.getAllDiscountCoupons();
  }

  @Override
  public List<Map<String, Object>> getAllDealNames() {
    return this.discountCouponsDao.getAllDealNames();
  }

  @Override
  public Map<String, Object> getDealNameById(final Long id) {
    return this.discountCouponsDao.getDealNameById(id);
  }

  @Override
  public DiscountCoupons getDiscountCouponsByOfferCode(final String offerCode) {
    return this.discountCouponsDao.getDiscountCouponsByOfferCode(offerCode);
  }
}
