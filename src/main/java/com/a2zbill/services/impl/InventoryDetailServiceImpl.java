package com.a2zbill.services.impl;

import com.a2zbill.dao.InventoryDetailDao;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import com.a2zbill.services.InventoryDetailService;
import java.math.BigDecimal;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventoryDetailServiceImpl implements InventoryDetailService {
  @Autowired InventoryDetailDao inventoryDetailDao;

  @Override
  public void save(InventoryDetail inventoryDetail) {
    this.inventoryDetailDao.save(inventoryDetail);
  }

  @Override
  public boolean delete(long id) {
    return false;
  }

  @Override
  public boolean update(InventoryDetail inventoryDetail) {
    return this.inventoryDetailDao.update(inventoryDetail);
  }

  @Override
  public List<InventoryDetail> getAllInventoryDetails() {
    return this.inventoryDetailDao.getAllInventoryDetails();
  }

  @Override
  public InventoryDetail getInventoryDetailsByProductId(final long productId) {
    return this.inventoryDetailDao.getInventoryDetailsByProductId(productId);
  }

  @Override
  public List<InventoryDetail> getInventoryDetailsByProductIds(final long productId) {
    return this.inventoryDetailDao.getInventoryDetailsByProductIds(productId);
  }

  @Override
  public InventoryDetail getInventoryDetailsByProductIdAndBranchId(
      final long productId, final long branchId) {
    return this.inventoryDetailDao.getInventoryDetailsByProductIdAndBranchId(productId, branchId);
  }

  @Override
  public InventoryDetail getInventoryDetailsByProductIdAndBranchIdAndOrgId(
      final long productId, final long branchId, final long orgId) {
    return this.inventoryDetailDao.getInventoryDetailsByProductIdAndBranchIdAndOrgId(
        productId, branchId, orgId);
  }

  @Override
  public Long getInventoryCurrentStockQTY() {
    return this.inventoryDetailDao.getInventoryCurrentStockQTY();
  }

  @Override
  public BigDecimal getInventoryDetailStockAmount() {
    return this.inventoryDetailDao.getInventoryDetailStockAmount();
  }

  @Override
  public InventoryDetail getInventoryDetailById(final long inventoryId) {
    return this.inventoryDetailDao.getInventoryDetailById(inventoryId);
  }

  @Override
  public List<InventoryDetail> getAllInventoryDetails(final long branchId, final long orgId) {
    return this.inventoryDetailDao.getAllInventoryDetails(branchId, orgId);
  }

  @Override
  public List<ReOrder> getReOrderDetailsById(final long productId) {
    return this.inventoryDetailDao.getReOrderDetailsById(productId);
  }

  @Override
  public List<ProductVendor> getProductVendorsbyId(final long productId) {
    return this.inventoryDetailDao.getProductVendorsbyId(productId);
  }

  @Override
  public List<InventoryDetail> getReOrderInventoryDetails(final long orgId, final long branchId) {
    return this.inventoryDetailDao.getReOrderInventoryDetails(orgId, branchId);
  }

  @Override
  public List<ReOrder> getReorderProductByStatus(
      final long orgId, final long branchId, final long productId) {
    return this.inventoryDetailDao.getReorderProductByStatus(orgId, branchId, productId);
  }

  @Override
  public BigDecimal getInventoryCurrentStockQTYByorgBranch(final long orgId, final long branchId) {
    return this.inventoryDetailDao.getInventoryCurrentStockQTYByorgBranch(orgId, branchId);
  }

  @Override
  public BigDecimal getInventoryDetailStockAmountByorgBranch(
      final long orgId, final long branchId) {
    return this.inventoryDetailDao.getInventoryDetailStockAmountByorgBranch(orgId, branchId);
  }

  @Override
  public InventoryDetail getInventoryDetailByIdOrgIdBranch(
      final long inventoryId, final long orgId, final long branchId) {
    return this.inventoryDetailDao.getInventoryDetailByIdOrgIdBranch(inventoryId, orgId, branchId);
  }

  @Override
  public List<InventoryDetail> getAllInventoryDetailsByOrgId(final long orgId) {
    return this.inventoryDetailDao.getAllInventoryDetailsByOrgId(orgId);
  }

  @Override
  public BigDecimal getInventoryCurrentStockQTYByorg(final long orgId) {
    return this.inventoryDetailDao.getInventoryCurrentStockQTYByorg(orgId);
  }

  @Override
  public BigDecimal getInventoryDetailStockAmountByorg(final long orgId) {
    return this.inventoryDetailDao.getInventoryDetailStockAmountByorg(orgId);
  }

  @Override
  public InventoryDetail getInventoryDetailsByProductIdAndOrgId(
      final long productId, final long orgId) {
    return this.inventoryDetailDao.getInventoryDetailsByProductIdAndOrgId(productId, orgId);
  }

  @Override
  public List<InventoryDetail> getReOrderInventoryDetailsByOrg(final long orgId) {
    return this.inventoryDetailDao.getReOrderInventoryDetailsByOrg(orgId);
  }

  @Override
  public List<ReOrder> getReorderProductByStatusByOrgId(final long orgId, final long productId) {
    return this.inventoryDetailDao.getReorderProductByStatusByOrgId(orgId, productId);
  }

  @Override
  public void updateInventoryQuantity(final long productId, final BigDecimal quantity) {
    this.inventoryDetailDao.updateInventoryQuantity(productId, quantity);
  }

  @Override
  public InventoryDetail getInventoryDetailByIdOrgId(long inventoryId, long orgId) {
    return this.inventoryDetailDao.getInventoryDetailByIdOrgId(inventoryId, orgId);
  }

  @Override
  public List<InventoryDetail> getInventoryDetailByBranch(long branchId) {
    return this.inventoryDetailDao.getInventoryDetailByBranch(branchId);
  }

  @Override
  public InventoryDetail getInventoryDetailByProductNameAndBranchIdAndOrgId(
      String productName, long branchId, long orgId) {
    return this.inventoryDetailDao.getInventoryDetailByProductNameAndBranchIdAndOrgId(
        productName, branchId, orgId);
  }
}
