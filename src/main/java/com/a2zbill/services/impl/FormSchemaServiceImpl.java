package com.a2zbill.services.impl;

import com.a2zbill.dao.FormSchemaDao;
import com.a2zbill.domain.FormSchema;
import com.a2zbill.services.FormSchemaService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FormSchemaServiceImpl implements FormSchemaService {

  @Autowired private FormSchemaDao formSchemaDao;

  @Override
  public void save(final FormSchema formSchema) {
    this.formSchemaDao.save(formSchema);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final FormSchema formSchema) {
    return false;
  }

  @Override
  public List<FormSchema> getAllFormSchema() {
    return this.formSchemaDao.getAllFormSchema();
  }

  @Override
  public Map<String, Object> getFormSchemaDetailsById(final long Id) {
    return this.formSchemaDao.getFormSchemaDetailsById(Id);
  }

  @Override
  public FormSchema getAllFormSchemaDetailsById(final long Id) {
    return this.formSchemaDao.getAllFormSchemaDetailsById(Id);
  }
}
