package com.a2zbill.services.impl;

import com.a2zbill.dao.FormTypeDao;
import com.a2zbill.domain.FormType;
import com.a2zbill.services.FormTypeService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FormTypeServiceImpl implements FormTypeService {

  @Autowired private FormTypeDao formTypeDao;

  @Override
  public void save(final FormType formType) {
    this.formTypeDao.save(formType);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final FormType FormType) {
    return false;
  }

  @Override
  public List<FormType> getAllFormTypes() {
    return this.formTypeDao.getAllFormTypes();
  }

  @Override
  public FormType getFormTypeById(final long formid) {
    return this.formTypeDao.getFormTypeById(formid);
  }

  @Override
  public Map<String, Object> getFormTypedetailsById(final long formid) {
    return this.formTypeDao.getFormTypedetailsById(formid);
  }
}
