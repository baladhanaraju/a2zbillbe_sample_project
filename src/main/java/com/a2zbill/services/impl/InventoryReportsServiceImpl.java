package com.a2zbill.services.impl;

import com.a2zbill.dao.InventoryReportsDao;
import com.a2zbill.domain.InventoryReports;
import com.a2zbill.services.InventoryReportsService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventoryReportsServiceImpl implements InventoryReportsService {

  @Autowired InventoryReportsDao inventoryReportsDao;

  @Override
  public InventoryReports getInventoryByDateAndInventoryId(
      final Date createdDate, final long inventoryId, final long orgId, final long branchId) {
    return inventoryReportsDao.getInventoryByDateAndInventoryId(
        createdDate, inventoryId, orgId, branchId);
  }

  @Override
  public void save(final InventoryReports inventoryReports) {

    inventoryReportsDao.save(inventoryReports);
  }

  @Override
  public boolean update(final InventoryReports inventoryReports) {
    return inventoryReportsDao.update(inventoryReports);
  }

  @Override
  public List<InventoryReports> getInventoryReportsByDate(final Date date, final long branchId) {
    return inventoryReportsDao.getInventoryReportsByDate(date, branchId);
  }
}
