package com.a2zbill.services.impl;

import com.a2zbill.dao.CancelItemDao;
import com.a2zbill.domain.CancelItem;
import com.a2zbill.services.CancelItemService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CancelItemServiceImpl implements CancelItemService {
  @Autowired private CancelItemDao cancelItemDao;

  @Override
  public void save(final CancelItem cancelItem) {
    this.cancelItemDao.save(cancelItem);
  }

  @Override
  public boolean delete(final long id) {
    this.cancelItemDao.delete(id);
    return true;
  }

  @Override
  public boolean update(final CancelItem cancelItem) {
    this.cancelItemDao.update(cancelItem);
    return true;
  }

  @Override
  public List<CancelItem> getAllCancelItemDetails() {
    return this.cancelItemDao.getAllCancelItemDetails();
  }

  @Override
  public List<CancelItem> getAllCancelItemDetailsByorgBranchId(
      final long orgId, final long branchId) {
    return this.cancelItemDao.getAllCancelItemDetailsByorgBranchId(orgId, branchId);
  }

  @Override
  public CancelItem getCancelItemDetailsByorgBranchIdAndGuid(
      final long orgId, final long branchId, final String guid) {
    return this.cancelItemDao.getCancelItemDetailsByorgBranchIdAndGuid(orgId, branchId, guid);
  }

  @Override
  public List<CancelItem> getAllCancelItemDetailsByorg(final long orgId) {
    return this.cancelItemDao.getAllCancelItemDetailsByorg(orgId);
  }
}
