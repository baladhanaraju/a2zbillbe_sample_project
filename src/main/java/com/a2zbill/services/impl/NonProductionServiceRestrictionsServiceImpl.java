package com.a2zbill.services.impl;

import com.a2zbill.dao.NonProductionServiceRestrictionsDao;
import com.a2zbill.domain.NonProductionServiceRestrictions;
import com.a2zbill.services.NonProductionServiceRestrictionsService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class NonProductionServiceRestrictionsServiceImpl
    implements NonProductionServiceRestrictionsService {

  @Autowired NonProductionServiceRestrictionsDao nonProductionServiceRestrictionsDao;

  @Override
  public void save(final NonProductionServiceRestrictions nonProductionServiceRestrictions) {
    this.nonProductionServiceRestrictionsDao.save(nonProductionServiceRestrictions);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final NonProductionServiceRestrictions nonProductionServiceRestrictions) {
    return false;
  }

  @Override
  public List<NonProductionServiceRestrictions> getAllNonProductionServiceRestrictions() {
    return this.nonProductionServiceRestrictionsDao.getAllNonProductionServiceRestrictions();
  }
}
