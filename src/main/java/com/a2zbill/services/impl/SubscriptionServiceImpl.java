package com.a2zbill.services.impl;

import com.a2zbill.dao.SubscriptionDao;
import com.a2zbill.domain.Subscription;
import com.a2zbill.services.FrequencyService;
import com.a2zbill.services.SubscriptionService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {

  @Autowired SubscriptionDao subscriptionDao;

  @Value("${jwt.header}")
  private String tokenHeader;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired EmployeeService employeeService;

  @Autowired ProductServiceImpl productsService;

  @Autowired BranchService branchService;

  @Autowired CustomerService customerService;

  @Autowired FrequencyService frequencyService;

  @Autowired SubscriptionService subscriptionService;

  @Override
  public void save(final Subscription subscription) {
    subscriptionDao.save(subscription);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final Subscription subscription) {
    subscriptionDao.update(subscription);
    return true;
  }

  @Override
  public List<Subscription> getAllSubscriptions() {
    return subscriptionDao.getAllSubscriptions();
  }

  @Override
  public Subscription getSubscriptionByFrequencyId(final long frequencyId) {
    return subscriptionDao.getSubscriptionByFrequencyId(frequencyId);
  }

  public Subscription saveSubscriptionDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      final String payload,
      final String mobileNumber)
      throws JSONException, ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Branch branch = employeeDetails.getBranch();
    final JSONObject subscriptionDetails = new JSONObject(payload);
    final Subscription subscription = new Subscription();
    @SuppressWarnings("deprecation")
    final Boolean autoRenewal = new Boolean(subscriptionDetails.getString("autoRenewal"));
    final String dateString = subscriptionDetails.getString("date");
    final String startDate = subscriptionDetails.getString("startDate");
    final String customerMObileNumber = subscriptionDetails.getString("mobileNumber");
    final Customer customer = customerService.getCustomerByMobileNumber(customerMObileNumber);
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date date = sdf.parse(dateString);
    final SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    final Date d = df.parse(startDate);
    final Calendar cal = Calendar.getInstance();
    cal.setTime(d);
    cal.add(Calendar.MINUTE, 30);
    try {
      subscription.setAutoRenewal(autoRenewal);
      subscription.setBranch(branch);
      subscription.setCreatedDate(new Date());
      subscription.setModifiedDate(new Date());
      subscription.setCustomer(customer);
      subscription.setDate(date);
      subscription.setEmployee(employeeDetails);
      subscription.setOrganisation(employeeDetails.getOrganisation());
      subscriptionService.save(subscription);
    } catch (final Exception ede) {
    }
    return subscription;
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndOrgId(
      final Date date, final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndOrgId(date, orgId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndOrgIdAndbranchId(
      final Date date, final long orgId, final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndOrgIdAndbranchId(date, orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndorgId(
      final Date date, final long employeeId, final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndEmployeeIdAndorgId(
        date, employeeId, orgId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndOrgIdAndbranchId(
      final Date date, final long employeeId, final long orgId, final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndEmployeeIdAndOrgIdAndbranchId(
        date, employeeId, orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndproductIdAndOrgId(
      final Date date, final long productId, final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndproductIdAndOrgId(date, productId, orgId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
      final Date date, final long productId, final long orgId, final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
        date, productId, orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgId(
      final Date date, final long employeeId, final long productId, final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgId(
        date, employeeId, productId, orgId);
  }

  @Override
  public List<Subscription>
      getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgIdAndBranchId(
          final Date date,
          final long employeeId,
          final long productId,
          final long orgId,
          final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgIdAndBranchId(
        date, employeeId, productId, orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgIdAndbranchId(
      final long employeeId, final long productId, final long orgId, final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgIdAndbranchId(
        employeeId, productId, orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgId(
      final long employeeId, final long productId, final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgId(
        employeeId, productId, orgId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByOrgId(final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByOrgId(orgId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByOrgIdAndBranchId(
      final long orgId, final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByOrgIdAndBranchId(orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndOrgIdAndBranchId(
      final long employeeId, final long orgId, final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByEmployeeIdAndOrgIdAndBranchId(
        employeeId, orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndOrgId(
      final long employeeId, final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByEmployeeIdAndOrgId(employeeId, orgId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
      final long productId, final long orgId, final long branchId) {
    return subscriptionDao.getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
        productId, orgId, branchId);
  }

  @Override
  public List<Subscription> getSubscriptionDetailsByProductIdAndOrgId(
      final long productId, final long orgId) {
    return subscriptionDao.getSubscriptionDetailsByProductIdAndOrgId(productId, orgId);
  }
}
