package com.a2zbill.services.impl;

import com.a2zbill.dao.CustomerCreditDao;
import com.a2zbill.domain.CustomerCredit;
import com.a2zbill.domain.CustomerCreditHistory;
import com.a2zbill.services.CustomerCreditService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CustomerCreditServiceImpl implements CustomerCreditService {

  @Autowired CustomerCreditDao customerCreditDao;

  @Override
  public void save(CustomerCredit customerCredit) {
    customerCreditDao.save(customerCredit);
  }

  @Override
  public void update(CustomerCredit customerCredit) {
    customerCreditDao.update(customerCredit);
  }

  @Override
  public CustomerCredit getCustomerCreditDetailsByCustomerId(long customerId) {
    return customerCreditDao.getCustomerCreditDetailsByCustomerId(customerId);
  }

  @Override
  public List<CustomerCreditHistory> getCustomerCreditHistoryByCustomerCreditId(
      long customerCreditId) {
    return customerCreditDao.getCustomerCreditHistoryByCustomerCreditId(customerCreditId);
  }

  @Override
  public List<CustomerCreditHistory> getAllCustomerCreditHistoryByOrgAndBranch(
      long orgId, long branchId) {
    return customerCreditDao.getAllCustomerCreditHistoryByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<CustomerCreditHistory> getAllCustomerCreditHistoryByOrg(long orgId) {
    return customerCreditDao.getAllCustomerCreditHistoryByOrg(orgId);
  }

  @Override
  public List<CustomerCredit> getAllCustomerCreditsByOrgAndBranch(long orgId, long branchId) {
    return customerCreditDao.getAllCustomerCreditsByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<CustomerCredit> getAllCustomerCreditsByOrg(long orgId) {
    return customerCreditDao.getAllCustomerCreditsByOrg(orgId);
  }

  @Override
  public void saveCustomerCreditHistory(CustomerCreditHistory customerCreditHistory) {
    customerCreditDao.saveCustomerCreditHistory(customerCreditHistory);
  }

  @Override
  public void updateCustomerCreditHistory(CustomerCreditHistory customerCreditHistory) {
    customerCreditDao.updateCustomerCreditHistory(customerCreditHistory);
  }
}
