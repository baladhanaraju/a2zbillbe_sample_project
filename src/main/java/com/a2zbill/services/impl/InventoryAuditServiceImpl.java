package com.a2zbill.services.impl;

import com.a2zbill.dao.InventoryAuditDao;
import com.a2zbill.domain.InventoryAudit;
import com.a2zbill.services.InventoryAuditService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventoryAuditServiceImpl implements InventoryAuditService {
  @Autowired private InventoryAuditDao inventoryAuditDao;

  @Override
  public void save(final InventoryAudit inventoryAudit) {
    this.inventoryAuditDao.save(inventoryAudit);
  }

  @Override
  public boolean delete(final InventoryAudit inventoryAudit) {
    return false;
  }

  @Override
  public boolean update(final InventoryAudit inventoryAudit) {
    return this.inventoryAuditDao.update(inventoryAudit);
  }

  @Override
  public List<InventoryAudit> getAllInventoryAudit() {
    return this.inventoryAuditDao.getAllInventoryAudit();
  }

  @Override
  public List<InventoryAudit> getAllInventoryAuditByOrgBranchId(
      final long orgId, final long branchId) {
    return this.inventoryAuditDao.getAllInventoryAuditByOrgBranchId(orgId, branchId);
  }

  @Override
  public List<InventoryAudit> getAllInventoryAuditByOrg(final long orgId) {
    return this.inventoryAuditDao.getAllInventoryAuditByOrg(orgId);
  }
}
