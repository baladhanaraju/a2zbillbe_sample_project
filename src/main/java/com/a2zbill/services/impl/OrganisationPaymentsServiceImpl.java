package com.a2zbill.services.impl;

import com.a2zbill.dao.OrganisationPaymentsDao;
import com.a2zbill.domain.OrganisationPayments;
import com.a2zbill.services.OrganisationPaymentsService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OrganisationPaymentsServiceImpl implements OrganisationPaymentsService {

  @Autowired private OrganisationPaymentsDao organisationPaymentsDao;

  @Override
  public void save(final OrganisationPayments organisationPayments) {
    organisationPaymentsDao.save(organisationPayments);
  }

  @Override
  public List<OrganisationPayments> getAllOrganisationPayments() {
    return organisationPaymentsDao.getAllOrganisationPayments();
  }

  @Override
  public void update(final OrganisationPayments organisationPayments) {
    organisationPaymentsDao.update(organisationPayments);
  }

  @Override
  public List<OrganisationPayments> getOrganisationPaymentsByOrgId(final long orgId) {
    return organisationPaymentsDao.getOrganisationPaymentsByOrgId(orgId);
  }

  @Override
  public OrganisationPayments getOrganisationPaymentsByOrgIdAndPaymentId(
      final long orgId, final long paymentId) {
    return organisationPaymentsDao.getOrganisationPaymentsByOrgIdAndPaymentId(orgId, paymentId);
  }
}
