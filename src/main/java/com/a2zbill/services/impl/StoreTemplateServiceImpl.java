package com.a2zbill.services.impl;

import com.a2zbill.dao.StoreTemplateDao;
import com.a2zbill.domain.StoreTemplate;
import com.a2zbill.services.StoreTemplateService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class StoreTemplateServiceImpl implements StoreTemplateService {
  @Autowired StoreTemplateDao storeTemplateDao;

  @Override
  public void save(StoreTemplate storeTemplate) {
    this.storeTemplateDao.save(storeTemplate);
  }

  @Override
  public boolean delete(Long id) {
    this.storeTemplateDao.delete(id);
    return true;
  }

  @Override
  public boolean update(StoreTemplate storeTemplate) {
    this.storeTemplateDao.update(storeTemplate);
    return true;
  }

  @Override
  public StoreTemplate getStoreTemplateById(final Long id) {
    return this.storeTemplateDao.getStoreTemplateById(id);
  }

  @Override
  public List<StoreTemplate> getAllStoreTemplate() {
    return this.storeTemplateDao.getAllStoreTemplate();
  }

  @Override
  public long getStoreTemplateByorgPathUrl(String pathUrl) {
    return this.storeTemplateDao.getStoreTemplateByorgPathUrl(pathUrl);
  }
}
