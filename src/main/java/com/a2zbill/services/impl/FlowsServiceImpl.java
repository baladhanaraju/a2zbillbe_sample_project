package com.a2zbill.services.impl;

import com.a2zbill.dao.FlowsDao;
import com.a2zbill.domain.Flows;
import com.a2zbill.services.FlowsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FlowsServiceImpl implements FlowsService {
  @Autowired private FlowsDao flowsDao;

  @Override
  public void save(final Flows flows) {
    this.flowsDao.save(flows);
  }

  @Override
  public Flows getFlowsById(final Long id) {
    return this.flowsDao.getFlowsById(id);
  }

  @Override
  public List<Flows> getFlowsDetails() {
    return this.flowsDao.getFlowsDetails();
  }
}
