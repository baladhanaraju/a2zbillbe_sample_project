package com.a2zbill.services.impl;

import com.a2zbill.dao.CounterDao;
import com.a2zbill.dao.CounterProductsDao;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.EmployeeCounter;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import com.a2zbill.services.CartService;
import com.a2zbill.services.CounterProductsService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CounterProductsServiceImpl implements CounterProductsService {
  @Autowired CounterProductsDao employeeCounterDao;
  @Autowired CounterDao counterDao;
  @Autowired EmployeeService employeeService;
  @Autowired CartService cartService;
  @Autowired CounterProductsService employeeCounterService;
  long counterid = 0;
  long employeeId = 0;

  @Override
  public void save(final EmployeeCounter employeeCounter) {
    this.employeeCounterDao.save(employeeCounter);
  }

  @Override
  public List<Long> getRemainCounter(final long employeeNumber) {
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();
    final List<Long> counters = this.counterDao.getAllCounterDetail(orgId, branchId);
    final List<Long> RemainCounters = new ArrayList<Long>();
    for (int i = 0; i < counters.size(); i++) {
      RemainCounters.add(counters.get(i));
    }
    final List<Long> assignCounterIds =
        this.employeeCounterDao.getAssignedCounters(branchId, orgId); // getAssignedCounters();
    RemainCounters.removeAll(assignCounterIds);
    final List<Long> assigncounters =
        this.employeeCounterDao.getEmployeeCounters(employeeDetails.getId());
    RemainCounters.addAll(assigncounters);
    return RemainCounters;
  }

  @Override
  public List<Long> getAssignedCounters(final long branchId, final long orgId) {
    return this.employeeCounterDao.getAssignedCounters(branchId, orgId);
  }

  public List<Long> getOpenCounters() {
    final List<Long> counterStatus = this.employeeCounterDao.getOpenCounters();
    for (int i = 0; i < counterStatus.size(); i++) {
      this.counterid = counterStatus.get(i);
      System.out.println("counter=======" + counterid);
    }
    System.out.println("counter=======" + counterid);
    return counterStatus;
  }

  @Override
  public long getEmployeeId(final String principal) {
    System.out.println("principal name+++++++++++" + principal);
    final Long employeeId = this.employeeCounterDao.getEmployeeId(principal);
    return employeeId;
  }

  @Override
  public List<Long> getEmployeeCounters(final String principal) {
    final long empId = this.employeeCounterService.getEmployeeId(principal);
    return this.employeeCounterDao.getEmployeeCounters(empId);
  }

  @Override
  public List<CartDetail> getCartProductDetails(final String username) {
    final List<CartDetail> listOfcarts = new ArrayList<CartDetail>();
    CartDetail cartDetails = null;
    final List<Long> counterId = this.employeeCounterService.getOpenCounters();
    Counter counter = null;
    Cart cart = null;
    long cartNumbers = 0;
    long counterNumber = 0;
    List<Cart> cartId = null;
    for (int i = 0; i < counterId.size(); i++) {
      counterNumber = counterId.get(i);
      cartId = this.cartService.getCartDetailsByCounterId(counterNumber);
      for (int j = 0; j < cartId.size(); j++) {
        counter = new Counter();
        cart = new Cart();
        cartDetails = new CartDetail();
        cartDetails.setCounterDetails(counter);
        cartNumbers = cartId.get(j).getCartId();
        cart.setCartId(cartNumbers);
        cartDetails.setCartDetails(cart);
        listOfcarts.add(cartDetails);
      }
    }
    return this.employeeCounterDao.getCartProductDetails(listOfcarts);
  }

  @Override
  public List<Long> getEmployeeCounterIds(final String userName) {
    final long empId = this.employeeCounterService.getEmployeeId(userName);
    return this.employeeCounterDao.getEmployeeCounterIds(empId);
  }

  @Override
  public List<BigDecimal> getCounterTotalBillAmount(final long cartId) {
    return this.employeeCounterDao.getCounterTotalBillAmount(cartId);
  }

  @Override
  public void updateEmployeeCounters(final EmployeeCounter employeeCounters) {
    this.employeeCounterDao.insertEmployeeCounterDetails(employeeCounters);
  }

  @Override
  public int deleteEmployeeCounters(final long employeeId) {
    return this.employeeCounterDao.deleteEmployeeCounters(employeeId);
  }

  @Override
  public void saveVendorProducts(final ProductVendor productVendor) {
    this.employeeCounterDao.saveVendorproducts(productVendor);
  }

  @Override
  public void saveReOrder(final ReOrder reOrder) {
    this.employeeCounterDao.saveReOrder(reOrder);
  }

  @Override
  public void updateVendorProducts(final ProductVendor productVendor) {
    this.employeeCounterDao.updateVendorProducts(productVendor);
  }

  @Override
  public int updateReOrderByProductIdAndReorderId(final long productId, final long reorderId) {
    return this.employeeCounterDao.updateReOrderByProductIdAndReorderId(productId, reorderId);
  }

  @Override
  public List<ProductVendor> viewVendorProducts(final long productId) {
    return this.employeeCounterDao.viewOrderProducts(productId);
  }

  @Override
  public List<ReOrder> viewReorderProducts(final long productId) {
    return this.employeeCounterDao.viewReorderDetails(productId);
  }

  @Override
  public List<Object[]> viewReorderDetailsByStatus(final long orgId, final long branchId) {
    return this.employeeCounterDao.viewReorderDetailsByStatus(orgId, branchId);
  }

  @Override
  public List<ReOrder> getReOrderDetailsByInvoiceId(final long invoiceId) {
    return this.employeeCounterDao.getReOrderDetailsByInvoiceId(invoiceId);
  }

  @Override
  public List<Object[]> viewReorderDetailsByStatus(final long orgId) {
    return this.employeeCounterDao.viewReorderDetailsByStatus(orgId);
  }

  @Override
  public List<ReOrder> getReorderDetailsByProductId(final long productId) {
    return this.employeeCounterDao.getReorderDetailsByProductId(productId);
  }

  @Override
  public List<ReOrder> getAllReOrderDetailsByInvoiceId(final long invoiceId) {
    return this.employeeCounterDao.getAllReOrderDetailsByInvoiceId(invoiceId);
  }

  @Override
  public ReOrder getReOrderDetailsById(final long id) {
    return this.employeeCounterDao.getReOrderDetailsById(id);
  }

  @Override
  public List<ReOrder> getAllReorderDetailsByOrgId(final long orgId) {
    return this.employeeCounterDao.getAllReOrderDetailsByInvoiceId(orgId);
  }

  @Override
  public List<ReOrder> getAllReorderDetailsByOrgIdAndBranchId(
      final long orgId, final long branchId) {
    return this.employeeCounterDao.getAllReorderDetailsByOrgIdAndBranchId(orgId, branchId);
  }

  @Override
  public ReOrder getReorderDetailsByReorderId(final long reOrderId) {
    return this.employeeCounterDao.getReorderDetailsByReorderId(reOrderId);
  }

  @Override
  public boolean updateReorder(final ReOrder reOrder) {
    return this.employeeCounterDao.updateReorder(reOrder);
  }

  @Override
  public List<ReOrder> getReorderListByInvoiceIdAndOrganisationId(
      final long invoiceId, final long orgId) {
    return this.employeeCounterDao.getReorderListByInvoiceIdAndOrganisationId(invoiceId, orgId);
  }

  @Override
  public List<ReOrder> getReorderListByInvoiceIdAndOrganisationIdAndBranchId(
      final long invoiceId, final long orgId, final long branchId) {
    return this.employeeCounterDao.getReorderListByInvoiceIdAndOrganisationIdAndBranchId(
        invoiceId, orgId, branchId);
  }
}
