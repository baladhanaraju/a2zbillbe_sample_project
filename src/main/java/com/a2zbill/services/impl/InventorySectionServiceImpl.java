package com.a2zbill.services.impl;

import com.a2zbill.dao.InventorySectionDao;
import com.a2zbill.domain.InventorySection;
import com.a2zbill.services.InventorySectionService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventorySectionServiceImpl implements InventorySectionService {
  @Autowired InventorySectionDao inventorySectionDao;

  @Override
  public void save(InventorySection inventorySection) {
    this.inventorySectionDao.save(inventorySection);
  }

  @Override
  public boolean delete(long id) {
    return false;
  }

  @Override
  public boolean update(InventorySection inventorySection) {
    return this.inventorySectionDao.update(inventorySection);
  }

  @Override
  public List<InventorySection> getAllInventorySection() {
    return this.inventorySectionDao.getAllInventorySection();
  }

  @Override
  public List<InventorySection> getAllInventorySectionByOrgIdBranchId(
      final long orgId, final long branchId) {
    return this.inventorySectionDao.getAllInventorySectionByOrgIdBranchId(orgId, branchId);
  }

  @Override
  public List<InventorySection> getAllInventorySectionByOrgIdBranchIdAndMasterFlag(
      final long orgId, final long branchId, final String masterCountFlag) {
    return this.inventorySectionDao.getAllInventorySectionByOrgIdBranchIdAndMasterFlag(
        orgId, branchId, masterCountFlag);
  }

  @Override
  public InventorySection getInventorySectionByOrgIdBranchIdAndMasterFlag(
      final long orgId, final long branchId, final String masterCountFlag) {
    return this.inventorySectionDao.getInventorySectionByOrgIdBranchIdAndMasterFlag(
        orgId, branchId, masterCountFlag);
  }

  @Override
  public InventorySection getInventorySectionByOrgIdBranchIdAndId(
      final long orgId, final long branchId, final long id) {
    return this.inventorySectionDao.getInventorySectionByOrgIdBranchIdAndId(orgId, branchId, id);
  }

  @Override
  public InventorySection getInventorySectionBySectionNameOrgIdBranchId(
      final String sectionName, final long orgId, final long branchId) {
    return this.inventorySectionDao.getInventorySectionBySectionNameOrgIdBranchId(
        sectionName, orgId, branchId);
  }

  @Override
  public InventorySection getInventorySectionByOrgIdAndId(final long orgId, final long id) {
    return this.inventorySectionDao.getInventorySectionByOrgIdAndId(orgId, id);
  }

  @Override
  public List<InventorySection> getAllInventorySectionByOrgIdBranchIdByStaticKotFlag(
      final long orgId, final long branchId) {
    return this.inventorySectionDao.getAllInventorySectionByOrgIdBranchIdByStaticKotFlag(
        orgId, branchId);
  }

  @Override
  public List<InventorySection> getAllInventorySectionByOrgId(final long orgId) {
    return this.inventorySectionDao.getAllInventorySectionByOrgId(orgId);
  }

  @Override
  public InventorySection getInventorySectionById(Long id) {

    return this.inventorySectionDao.getInventorySectionById(id);
  }

  @Override
  public List<Object[]> getInventorySectionNameByOrgId(long orgId) {
    return this.inventorySectionDao.getInventorySectionNameByOrgId(orgId);
  }

  @Override
  public List<Object[]> getInventorySectionNamesByOrgIdAndBranchId(long orgId, long branchId) {
    return this.inventorySectionDao.getInventorySectionNamesByOrgIdAndBranchId(orgId, branchId);
  }
}
