package com.a2zbill.services.impl.elasticsearch;

import com.a2zbill.dao.elasticsearch.ProductElasticSearchDao;
import com.a2zbill.domain.elasticsearch.ProductElasticSearch;
import com.a2zbill.services.elasticsearch.ProductElasticSearchService;
import java.math.BigDecimal;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductElasticSearchServiceImpl implements ProductElasticSearchService {

  @Autowired private ProductElasticSearchDao productElasticSearchDao;

  public ProductElasticSearch insertProduct(final ProductElasticSearch product) {
    return productElasticSearchDao.insertProduct(product);
  }

  public List<ProductElasticSearch> getProductByProductName(final String productName) {
    return productElasticSearchDao.getProductByProductName(productName);
  }

  public List<ProductElasticSearch> getProductByProductNameAndOrgId(
      final String searchKey, final long orgId) {
    return productElasticSearchDao.getProductByProductNameAndOrgId(searchKey, orgId);
  }

  public List<ProductElasticSearch> getProductByProductNameAndBranchIdAndOrgId(
      final String searchKey, final long branchId, final long orgId) {
    return productElasticSearchDao.getProductByProductNameAndBranchIdAndOrgId(
        searchKey, branchId, orgId);
  }

  public ProductElasticSearch getProductByProductIdAndOrgId(
      final Long productId, final long orgId) {
    return productElasticSearchDao.getProductByProductIdAndOrgId(productId, orgId);
  }

  public ProductElasticSearch getProductByProductIdAndBranchIdAndOrgId(
      final Long productId, long branchId, long orgId) {
    return productElasticSearchDao.getProductByProductIdAndBranchIdAndOrgId(
        productId, branchId, orgId);
  }

  public List<ProductElasticSearch> getProductsByBranchIdAndOrgIdProductNameAndPriceRange(
      final long branchId,
      final long orgId,
      final String productName,
      final BigDecimal lowPrice,
      final BigDecimal highPrice) {
    return productElasticSearchDao.getProductsByBranchIdAndOrgIdProductNameAndPriceRange(
        branchId, orgId, productName, lowPrice, highPrice);
  }

  public List<ProductElasticSearch> getProductsByOrgIdAndProductNameAndPriceRange(
      final long orgId,
      final String productName,
      final BigDecimal lowPrice,
      final BigDecimal highPrice) {
    return productElasticSearchDao.getProductsByOrgIdAndProductNameAndPriceRange(
        orgId, productName, lowPrice, highPrice);
  }
}
