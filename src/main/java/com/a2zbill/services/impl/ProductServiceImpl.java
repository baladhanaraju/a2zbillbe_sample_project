package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductDao;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductVariation;
import com.a2zbill.services.ProductCategoryService;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.StoreTemplateService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

  @Autowired ProductDao productsDao;

  @Autowired private StoreTemplateService storeTemplateService;

  @Autowired private ProductCategoryService productCategoryService;

  @Override
  public void save(final Product products) {
    this.productsDao.save(products);
  }

  @Override
  public boolean delete(final Long id) {
    return this.productsDao.delete(id);
  }

  @Override
  public boolean update(final Product products) {
    this.productsDao.update(products);
    return true;
  }

  @Override
  public Product getProductsById(final Long id) {
    return this.productsDao.getProductsById(id);
  }

  @Override
  public List<Product> getAllProducts() {
    return this.productsDao.getAllProducts();
  }

  @Override
  public Product getProductsByProductCode(final String productCode) {
    return this.productsDao.getProductsByProductCode(productCode);
  }

  @Override
  public Product getProductsByProductName(final String productName) {
    return this.productsDao.getProductsByProductName(productName);
  }

  @Override
  public List<Product> getAllProductsName() {
    return this.productsDao.getAllProductsName();
  }

  @Override
  public long getProductsCount() {
    return this.productsDao.getProductsCount();
  }

  @Override
  public List<Product> getAllProductsByName(final String productName) {
    return this.productsDao.getAllProductsByName(productName);
  }

  @Override
  public List<Product> getProductsByProductCategoryId(final long productCategoryId) {
    return this.productsDao.getProductsByProductCategoryId(productCategoryId);
  }

  @Override
  public Product getProductsByHsnCodeAndProductCodeAndProductName(
      final String hsnCode, final String productCode, final String productName) {
    return this.productsDao.getProductsByHsnCodeAndProductCodeAndProductName(
        hsnCode, productCode, productName);
  }

  @Override
  public Product getProductFeaturesById(final Long id) {
    return this.productsDao.getProductFeaturesById(id);
  }

  @Override
  public Product getProductsByHsnCodeAndProductCode(
      final String hsnCode, final String productCode) {
    return this.productsDao.getProductsByHsnCodeAndProductCode(hsnCode, productCode);
  }

  @Override
  public Product getProductsByOrgId(final Long orgId) {
    return this.productsDao.getProductsByOrgId(orgId);
  }

  @Override
  public Product getProductsByProductCodeAndOrgId(final String productCode, final Long orgId) {
    return this.productsDao.getProductsByProductCodeAndOrgId(productCode, orgId);
  }

  @Override
  public Product getProductsByProductCodeAndOrgIdAndBranchId(
      final String productCode, final Long orgId, final Long branchId) {
    return this.productsDao.getProductsByProductCodeAndOrgIdAndBranchId(
        productCode, orgId, branchId);
  }

  @Override
  public Product getProductsByHsnCodeAndProductName(
      final String hsnCode, final String productName) {
    return this.productsDao.getProductsByHsnCodeAndProductName(hsnCode, productName);
  }

  @Override
  public List<Product> getAllProducts(final long orgId, final long branchId) {
    return this.productsDao.getAllProducts(orgId, branchId);
  }

  @Override
  public long getProductsCount(final Long orgId) {
    return this.productsDao.getProductsCount(orgId);
  }

  @Override
  public long getProductsCounByOrgIdAndBranchId(final Long brnachId, final Long orgId) {
    return this.productsDao.getProductsCounByOrgIdAndBranchId(brnachId, orgId);
  }

  @Override
  public Product getProductsByProductNameAndOrgIdAndBranchId(
      final String productName, final long orgId, final long branchId) {
    return this.productsDao.getProductsByProductNameAndOrgIdAndBranchId(
        productName, orgId, branchId);
  }

  @Override
  public Product getProductsByProductNameAndOrgId(final String productName, final long orgId) {
    return this.productsDao.getProductsByProductNameAndOrgId(productName, orgId);
  }

  @Override
  public Product getProductsByProductIdAndOrgIdAndBranchId(
      final long productId, final long orgId, final long branchId) {
    return this.productsDao.getProductsByProductIdAndOrgIdAndBranchId(productId, orgId, branchId);
  }

  @Override
  public Product getProductsByHsnCodeAndProductNameByBranchIdAndOrgId(
      final String hsnCode, final String productName, final long branchId, final long orgId) {
    return this.productsDao.getProductsByHsnCodeAndProductNameByBranchIdAndOrgId(
        hsnCode, productName, branchId, orgId);
  }

  @Override
  public List<Product> getAllProductsByOrgId(final long orgId) {
    return this.productsDao.getAllProductsByOrgId(orgId);
  }

  @Override
  public Product getProductsByProductIdAndOrgId(final long productId, final long orgId) {
    return this.productsDao.getProductsByProductIdAndOrgId(productId, orgId);
  }

  @Override
  public List<Product> getAllProductsByHsnCode() {
    return this.productsDao.getAllProductsByHsnCode();
  }

  @Override
  public List<Product> getProductsByProductCategoryName(final String productCategoryName) {
    return this.productsDao.getProductsByProductCategoryName(productCategoryName);
  }

  @Override
  public List<Product> getProductsByProductCategoryNameIfRedeemFlagIsFalse(
      final String productCategoryName) {
    return this.productsDao.getProductsByProductCategoryNameIfRedeemFlagIsFalse(
        productCategoryName);
  }

  @Override
  public List<Product> getProductsBySizeVariationIdAndProductName(
      final long orgId, final long sizeId, final String productName) {
    return productsDao.getProductsBySizeVariationIdAndProductName(orgId, sizeId, productName);
  }

  public Product getProductsByProductNameAndOrgIdAndBranchIdAndSizeId(
      final String producName, final long orgId, final long branchId, final Long sizeVariationId) {
    return productsDao.getProductsByProductNameAndOrgIdAndBranchIdAndSizeId(
        producName, orgId, branchId, sizeVariationId);
  }

  @Override
  public List<ProductVariation> getAllProductsAndVariationDetails(
      final long orgId, final long branchId) {

    return productsDao.getAllProductsAndVariationDetails(orgId, branchId);
  }

  @Override
  public List<ProductVariation> getAllProductsAndVariationDetailsByOrg(final long orgId) {
    return productsDao.getAllProductsAndVariationDetailsByOrg(orgId);
  }

  @Override
  public List<Object[]> getAllProductByParentId(final long orgId, final long branchId) {

    return productsDao.getAllProductByParentId(orgId, branchId);
  }

  @Override
  public List<Object[]> getAllProductByOrgAndParentId(final long orgId) {

    return productsDao.getAllProductByOrgAndParentId(orgId);
  }

  @Override
  public List<Object[]> getProductByParentId(
      final long orgId, final long branchId, final long parentId) {
    return productsDao.getProductByParentId(orgId, branchId, parentId);
  }

  @Override
  public List<Object[]> getProductByParentIdAndOrgId(final long orgId, final long parentId) {
    return productsDao.getProductByParentIdAndOrgId(orgId, parentId);
  }

  @Override
  public Product getProductsByProductNameAndOrgAndBranch(
      final String productName, final long orgId, final long branchId) {
    return productsDao.getProductsByProductNameAndOrgAndBranch(productName, orgId, branchId);
  }

  @Override
  public Product getProductsByProductCodeAndProductNameAndOrgAndBranch(
      final String productCode, final String productName, final long orgId, final long branchId) {
    return productsDao.getProductsByProductCodeAndProductNameAndOrgAndBranch(
        productCode, productName, orgId, branchId);
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatus(final long orgId, final long branchId) {
    return productsDao.getProductByorgIdandBranchIdstatus(orgId, branchId);
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatus(final long orgId) {
    return productsDao.getProductByorgIdandBranchIdstatus(orgId);
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatus(
      final String productCategoryName, final long orgId) {
    return productsDao.getProductByorgIdandBranchIdstatus(productCategoryName, orgId);
  }

  @Override
  public List<Product> getMenuProductDetailsOrgIdandStatus(
      final String productCategoryName, final long orgId, final long branchId) {
    return productsDao.getMenuProductDetailsOrgIdandStatus(productCategoryName, orgId, branchId);
  }

  @Override
  public List<Product> getMenuProductDetailsOrgAndBranchAndCategory(
      final String productCategoryName,
      final String categoryName,
      final long orgId,
      final long branchId) {
    return productsDao.getMenuProductDetailsOrgAndBranchAndCategory(
        productCategoryName, categoryName, orgId, branchId);
  }

  @Override
  public List<Product> getMenuProductDetailsOrgAndCategory(
      final String productCategoryName, final String categoryName, final long orgId) {
    return productsDao.getMenuProductDetailsOrgAndCategory(
        productCategoryName, categoryName, orgId);
  }

  @Override
  public List<Map<String, Object>> getProductPackagingDetails() {
    return productsDao.getProductPackagingDetails();
  }

  @Override
  public Map<String, Object> getProductPackagingDetailsByProductId(final long productId) {
    return productsDao.getProductPackagingDetailsByProductId(productId);
  }

  @Override
  public List<Product> getProductById(final long productId) {
    return productsDao.getProductById(productId);
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatusActive(
      final long orgId, final long branchId) {
    return productsDao.getProductByorgIdandBranchIdstatusActive(orgId, branchId);
  }

  @Override
  public List<Product> getProductByorgIdandstatusActive(final long orgId) {
    return productsDao.getProductByorgIdandstatusActive(orgId);
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgIdpagination(
      final String productCategoryName,
      final long orgId,
      final int pageNum,
      final int numOfRecords) {
    return productsDao.getProductsByProductCategoryNameAndOrgIdpagination(
        productCategoryName, orgId, pageNum, numOfRecords);
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
      final String productCategoryName,
      final long orgId,
      final long branchId,
      final int pageNum,
      final int numOfRecords) {
    return productsDao.getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
        productCategoryName, orgId, branchId, pageNum, numOfRecords);
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgId(
      final String productCategoryName, final long orgId) {
    return productsDao.getProductsByProductCategoryNameAndOrgId(productCategoryName, orgId);
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchId(
      final String productCategoryName, final long orgId, final long branchId) {
    return productsDao.getProductsByProductCategoryNameAndOrgIdAndBranchId(
        productCategoryName, orgId, branchId);
  }

  @Override
  public Long getProductsCountByProductCategoryNameAndOrgId(
      final String productCategoryName, final long orgId) {
    return productsDao.getProductsCountByProductCategoryNameAndOrgId(productCategoryName, orgId);
  }

  @Override
  public long getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
      final String productCategoryName, final long orgId, final long branchId) {
    return productsDao.getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
        productCategoryName, orgId, branchId);
  }

  @Override
  public List<Map<String, Object>> getProductsByProductCategoryNameMap(
      final String productCategoryName) {
    final List<Map<String, Object>> lMaps = new ArrayList<>();
    try {
      final List<Object[]> producObjects =
          productsDao.getProductsByProductCategoryNameMap(productCategoryName);
      for (final Object[] product : producObjects) {
        final Map<String, Object> results =
            new HashMap<String, Object>() {
              private static final long serialVersionUID = 1L;

              {
                put("id", product[0]);
                put("productName", product[1]);
                put("productPrice", product[2]);
                put("code", product[3]);
                put("productCount", product[4]);
                put("packageFlag", product[5]);
                put("includingTaxFlag", product[6]);
                put("sgst", product[7]);
                put("cgst", product[8]);
                put("displayName", product[9]);
              }
            };
        lMaps.add(results);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return lMaps;
  }

  @Override
  public List<Map<String, Object>> getProductNameAndIdsByProductCategoryId(
      final long productCategoryId) {
    final List<Map<String, Object>> lMaps = new ArrayList<>();
    try {
      final List<Object[]> producObjects =
          productsDao.getProductNameAndIdsByProductCategoryId(productCategoryId);
      for (final Object[] product : producObjects) {
        final Map<String, Object> results =
            new HashMap<String, Object>() {
              private static final long serialVersionUID = 1L;

              {
                put("id", product[0]);
                put("productName", product[1]);
              }
            };
        lMaps.add(results);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return lMaps;
  }

  @Override
  public List<Map<String, Object>> getProductsByOrgPath(final String pathUrl) {
    final List<Map<String, Object>> productslist = new ArrayList<Map<String, Object>>();
    long storeTemplateId = storeTemplateService.getStoreTemplateByorgPathUrl(pathUrl);
    Integer[] object = productCategoryService.getCategoryTypesByStoreTemplateId(storeTemplateId);
    List<Long> productCategoryIds = new ArrayList<Long>();
    for (Integer categoryType : object) {
      List<Object[]> categories =
          productCategoryService.getCategoriesBycategoryType(categoryType.longValue(), pathUrl);
      for (int i = 0; i < categories.size(); i++) {
        long CategoryId = Long.valueOf(categories.get(i)[0].toString());
        productCategoryIds.add(CategoryId);
      }
    }
    for (Long productCategoryId : productCategoryIds) {
      final List<Object[]> products = productsDao.getProductsByOrgPath(pathUrl, productCategoryId);
      for (int i = 0; i < products.size(); i++) {
        Map<String, Object> mapProducts = new HashMap<String, Object>();
        mapProducts.put("id", products.get(i)[0]);
        mapProducts.put("productName", products.get(i)[1]);
        mapProducts.put("productPrice", products.get(i)[2]);
        mapProducts.put("productFeatures", products.get(i)[3]);
        mapProducts.put("image", products.get(i)[4]);
        mapProducts.put("variationName", products.get(i)[5]);
        mapProducts.put("quantityMeasureemnt", products.get(i)[6]);
        mapProducts.put("measurement", products.get(i)[7]);
        mapProducts.put("hsnNumber", products.get(i)[8]);
        mapProducts.put("code", products.get(i)[9]);
        mapProducts.put("CGST", products.get(i)[10]);
        mapProducts.put("SGST", products.get(i)[11]);
        mapProducts.put("size", products.size());
        productslist.add(mapProducts);
      }
    }
    return productslist;
  }

  @Override
  public List<Map<String, Object>> getProductsByCategoryId(long categoryId) {
    final List<Map<String, Object>> productslist = new ArrayList<Map<String, Object>>();
    final List<Object[]> products = productsDao.getProductsByCategoryId(categoryId);
    for (int i = 0; i < products.size(); i++) {
      Map<String, Object> mapProducts = new HashMap<String, Object>();
      mapProducts.put("id", products.get(i)[0]);
      mapProducts.put("productName", products.get(i)[1]);
      mapProducts.put("productPrice", products.get(i)[2]);
      mapProducts.put("productFeatures", products.get(i)[3]);
      mapProducts.put("image", products.get(i)[4]);
      mapProducts.put("variationName", products.get(i)[5]);
      mapProducts.put("quantityMeasureemnt", products.get(i)[6]);
      mapProducts.put("measurement", products.get(i)[7]);
      mapProducts.put("code", products.get(i)[8]);
      mapProducts.put("hsnNumber", products.get(i)[9]);
      mapProducts.put("SGST", products.get(i)[10]);
      mapProducts.put("CGST", products.get(i)[11]);
      productslist.add(mapProducts);
    }
    return productslist;
  }

  @Override
  public String getproductNamesByProductId(long productId) {
    return productsDao.getproductNamesByProductId(productId);
  }

  @Override
  public List<Product> getAllproductsByElastic() {
    return productsDao.getAllproductsByElastic();
  }
}
