package com.a2zbill.services.impl;

import com.a2zbill.dao.FlowTypeDao;
import com.a2zbill.domain.FlowType;
import com.a2zbill.services.FlowTypeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FlowTypeServiceImpl implements FlowTypeService {

  @Autowired private FlowTypeDao flowTypeDao;

  @Override
  public void save(final FlowType flowType) {
    this.flowTypeDao.save(flowType);
  }

  @Override
  public boolean delete(final Long id) {
    return this.flowTypeDao.delete(id);
  }

  @Override
  public boolean update(final FlowType flowType) {
    this.flowTypeDao.update(flowType);
    return true;
  }

  @Override
  public FlowType getFlowTypeById(final Long id) {
    return this.flowTypeDao.getFlowTypeById(id);
  }

  @Override
  public List<FlowType> getAllFlowTypeDetails() {
    return this.flowTypeDao.getAllFlowTypeDetails();
  }
}
