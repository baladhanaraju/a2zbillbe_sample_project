package com.a2zbill.services.impl;

import com.a2zbill.dao.ExpensesManagementDao;
import com.a2zbill.domain.ExpensesManagement;
import com.a2zbill.services.ExpensesManagementService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ExpensesManagementServiceImpl implements ExpensesManagementService {

  @Autowired private ExpensesManagementDao expensesManagementDao;

  @Override
  public void save(final ExpensesManagement expensesManagement) {
    this.expensesManagementDao.save(expensesManagement);
  }

  @Override
  public boolean delete(final ExpensesManagement expensesManagement) {
    return this.expensesManagementDao.delete(expensesManagement);
  }

  @Override
  public boolean update(final ExpensesManagement expensesManagement) {
    return this.expensesManagementDao.update(expensesManagement);
  }

  @Override
  public ExpensesManagement getExpensesManagementById(final long id) {
    return this.expensesManagementDao.getExpensesManagementById(id);
  }

  @Override
  public List<ExpensesManagement> getAllExpensesManagements() {
    return expensesManagementDao.getAllExpensesManagements();
  }

  @Override
  public List<ExpensesManagement> getExpensesByOrgIdAndBranchId(
      final long orgId, final long branchId) {
    return expensesManagementDao.getExpensesByOrgIdAndBranchId(orgId, branchId);
  }

  @Override
  public List<ExpensesManagement> getExpensesByOrgId(final long orgId) {
    return this.expensesManagementDao.getExpensesByOrgId(orgId);
  }

  @Override
  public List<ExpensesManagement> getExpensesByOrgIdAndBranchIdAndDate(
      final long orgId, final long branchId, final Date date) {
    return this.expensesManagementDao.getExpensesByOrgIdAndBranchIdAndDate(orgId, branchId, date);
  }

  @Override
  public List<ExpensesManagement> getExpensesByOrgIdAndDate(final long orgId, final Date date) {
    return this.expensesManagementDao.getExpensesByOrgIdAndDate(orgId, date);
  }

  @Override
  public List<ExpensesManagement> getExpensesDetailsByOrgAndBranch(
      final Date fromDate, final Date toDate, final long orgId, long branchId) {
    return this.expensesManagementDao.getExpensesDetailsByOrgAndBranch(
        fromDate, toDate, orgId, branchId);
  }

  @Override
  public List<ExpensesManagement> getExpensesDetailsByOrgAndBranchisNUll(
      final Date fromDate, final Date toDate, final long orgId) {
    return expensesManagementDao.getExpensesDetailsByOrgAndBranchisNUll(fromDate, toDate, orgId);
  }
}
