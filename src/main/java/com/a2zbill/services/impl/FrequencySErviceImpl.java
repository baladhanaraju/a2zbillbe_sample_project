package com.a2zbill.services.impl;

import com.a2zbill.dao.FrequencyDao;
import com.a2zbill.domain.Frequency;
import com.a2zbill.services.FrequencyService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FrequencySErviceImpl implements FrequencyService {

  @Autowired FrequencyDao frequencyDao;

  @Override
  public void save(final Frequency frequency) {
    frequencyDao.save(frequency);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final Frequency frequency) {
    return false;
  }

  @Override
  public List<Frequency> getAllFrequencys() {
    return frequencyDao.getAllFrequencys();
  }

  @Override
  public Frequency getFrequencyById(final long id) {
    return frequencyDao.getFrequencyById(id);
  }
}
