package com.a2zbill.services.impl;

import com.a2zbill.dao.ExchangeHistoryDao;
import com.a2zbill.domain.ExchangeHistory;
import com.a2zbill.services.ExchangeHistoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ExchangeHistoryServiceImpl implements ExchangeHistoryService {
  @Autowired ExchangeHistoryDao flowTypeDao;

  @Override
  public void save(final ExchangeHistory exchangeHistory) {
    this.flowTypeDao.save(exchangeHistory);
  }

  @Override
  public boolean delete(final long id) {
    return false;
  }

  @Override
  public boolean update(final ExchangeHistory exchangeHistory) {
    return this.flowTypeDao.update(exchangeHistory);
  }

  @Override
  public List<ExchangeHistory> getAllExchangeHistory() {
    return this.flowTypeDao.getAllExchangeHistory();
  }

  @Override
  public ExchangeHistory getExchangeHistoryById(final Long id) {
    return this.flowTypeDao.getExchangeHistoryById(id);
  }
}
