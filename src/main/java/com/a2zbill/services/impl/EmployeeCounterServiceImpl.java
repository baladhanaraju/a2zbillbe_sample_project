package com.a2zbill.services.impl;

import com.a2zbill.dao.EmployeeCounterDao;
import com.a2zbill.domain.EmployeeCounter;
import com.a2zbill.services.EmployeeCounterService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmployeeCounterServiceImpl implements EmployeeCounterService {
  @Autowired private EmployeeCounterDao employeeCounterDao;

  @Override
  public List<EmployeeCounter> getAllEmployeeCounterDetails() {
    return this.employeeCounterDao.getAllEmployeeCounterDetails();
  }

  @Override
  public List<EmployeeCounter> getAllEmployeeCounterDetailsByorgIdBranchId(
      final long orgId, final long branchId) {
    return this.employeeCounterDao.getAllEmployeeCounterDetailsByorgIdBranchId(orgId, branchId);
  }

  @Override
  public List<EmployeeCounter> getAllEmployeeCounterDetailsByorgId(final long orgId) {
    return this.employeeCounterDao.getAllEmployeeCounterDetailsByorgId(orgId);
  }
}
