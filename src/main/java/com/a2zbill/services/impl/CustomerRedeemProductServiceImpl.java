package com.a2zbill.services.impl;

import com.a2zbill.dao.CustomerRedeemProductDao;
import com.a2zbill.domain.CustomerRedeemProduct;
import com.a2zbill.services.CustomerRedeemProductService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CustomerRedeemProductServiceImpl implements CustomerRedeemProductService {
  @Autowired CustomerRedeemProductDao customerRedeemProductDao;

  @Override
  public void save(CustomerRedeemProduct customerRedeemProduct) {
    this.customerRedeemProductDao.save(customerRedeemProduct);
  }

  @Override
  public boolean delete(long id) {
    this.customerRedeemProductDao.delete(id);
    return true;
  }

  @Override
  public boolean update(CustomerRedeemProduct customerRedeemProduct) {
    this.customerRedeemProductDao.update(customerRedeemProduct);
    return true;
  }

  @Override
  public List<CustomerRedeemProduct> getAllCustomerRedeemProducts() {
    return this.customerRedeemProductDao.getAllCustomerRedeemProducts();
  }

  @Override
  public CustomerRedeemProduct getCustomerRedeemProductByproductIdAndCustId(
      final long productId, final long customerId) {
    return this.customerRedeemProductDao.getCustomerRedeemProductByproductIdAndCustId(
        productId, customerId);
  }

  @Override
  public List<CustomerRedeemProduct> getCustomerRedeemProductBycustomerId(final long customerid) {
    return this.customerRedeemProductDao.getCustomerRedeemProductBycustomerId(customerid);
  }

  @Override
  public List<Object[]> getCustomerRedeemProductById(final long productid) {
    return this.customerRedeemProductDao.getCustomerRedeemProductById(productid);
  }

  @Override
  public CustomerRedeemProduct getCustomerRedeemProductByproductIdOrgIdAndBranchId(
      final long productid, final long orgId, final long branchId) {
    return this.customerRedeemProductDao.getCustomerRedeemProductByproductIdOrgIdAndBranchId(
        productid, orgId, branchId);
  }

  @Override
  public List<CustomerRedeemProduct> getCustomerRedeemProductBycustomerIdByOrgIdAndBranchId(
      final long customerid, final long orgId, final long branchId) {
    return this.customerRedeemProductDao.getCustomerRedeemProductBycustomerIdByOrgIdAndBranchId(
        customerid, orgId, branchId);
  }

  @Override
  public CustomerRedeemProduct getCustomerRedeemProductByproductIdOrgId(
      final long productid, final long orgId) {
    return this.customerRedeemProductDao.getCustomerRedeemProductByproductIdOrgId(productid, orgId);
  }
}
