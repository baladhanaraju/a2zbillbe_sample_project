package com.a2zbill.services.impl;

import com.a2zbill.dao.ProductDealsDao;
import com.a2zbill.domain.ProductDeals;
import com.a2zbill.services.ProductDealsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductDealsServiceImpl implements ProductDealsService {
  @Autowired ProductDealsDao productDealsDao;

  @Override
  public void save(final ProductDeals productDeals) {
    this.productDealsDao.save(productDeals);
  }

  @Override
  public boolean delete(final Long id) {
    return this.productDealsDao.delete(id);
  }

  @Override
  public boolean update(final ProductDeals productDeals) {
    this.productDealsDao.update(productDeals);
    return true;
  }

  @Override
  public ProductDeals getProductDealsById(final Long id) {
    return null;
  }

  @Override
  public List<ProductDeals> getAllProductDeals() {
    return this.productDealsDao.getAllProductDeals();
  }

  @Override
  public ProductDeals getProductDealsByOfferCode(final String offerCode) {
    return this.productDealsDao.getProductDealsByOfferCode(offerCode);
  }

  @Override
  public ProductDeals getProductDealsByIdAndOfferCode(final Long id, final String offerCode) {
    return this.productDealsDao.getProductDealsByIdAndOfferCode(id, offerCode);
  }
}
