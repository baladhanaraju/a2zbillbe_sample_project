package com.a2zbill.services.impl;

import com.a2zbill.dao.InvoiceBillDao;
import com.a2zbill.domain.InvoiceBill;
import com.a2zbill.services.InvoiceBillService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InvoiceBillServiceImpl implements InvoiceBillService {

  @Autowired InvoiceBillDao invoiceBillDao;

  @Override
  public void save(final InvoiceBill invoiceBill) {
    this.invoiceBillDao.save(invoiceBill);
  }

  @Override
  public boolean update(final InvoiceBill invoiceBill) {
    this.invoiceBillDao.update(invoiceBill);
    return true;
  }

  @Override
  public boolean delete(final InvoiceBill invoiceBill) {
    this.invoiceBillDao.delete(invoiceBill);
    return false;
  }

  @Override
  public InvoiceBill getInvoiceBillByIdAndOrgId(final long invoiceBillId, final long orgId) {
    return this.invoiceBillDao.getInvoiceBillByIdAndOrgId(invoiceBillId, orgId);
  }

  @Override
  public InvoiceBill getInvoiceBillByIdAndOrgIdAndBranchId(
      final long invoiceBillId, final long orgId, final long branchId) {
    return this.invoiceBillDao.getInvoiceBillByIdAndOrgIdAndBranchId(
        invoiceBillId, orgId, branchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceBillsByOrgId(final long orgId) {
    return this.invoiceBillDao.getAllInvoiceBillsByOrgId(orgId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceBillsByOrgIdAndBranchId(
      final long orgId, final long branchId) {
    return this.invoiceBillDao.getAllInvoiceBillsByOrgIdAndBranchId(orgId, branchId);
  }

  @Override
  public InvoiceBill getInvoiceBillByInvoiceNumber(final String invoiceNumber) {
    return invoiceBillDao.getInvoiceBillByInvoiceNumber(invoiceNumber);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDates(
      final Date fromDate, final Date toDate, final long branchId) {
    return invoiceBillDao.getAllInvoiceDetailsByDates(fromDate, toDate, branchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsVendorNameTotalAmount(
      final Date fromDate, final Date toDate, final long branchId) {
    return invoiceBillDao.getAllInvoiceDetailsVendorNameTotalAmount(fromDate, toDate, branchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesVendor(
      final Date fromDate, final Date toDate, final long branchId, final long vendorId) {
    return invoiceBillDao.getAllInvoiceDetailsByDatesVendor(fromDate, toDate, branchId, vendorId);
  }

  @Override
  public List<Object[]> getInvoiceVendorDetails(
      final String invoiceNumber, final Date fromDate, final Date toDate, final long branchId) {
    return invoiceBillDao.getInvoiceVendorDetails(invoiceNumber, fromDate, toDate, branchId);
  }

  @Override
  public List<Object[]> getAllInvoiceVendorDetails(
      final Date fromDate, final Date toDate, final long branchId) {
    return this.getAllInvoiceVendorDetails(fromDate, toDate, branchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchAndBranchVendor(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long branchVendorId) {
    return this.invoiceBillDao.getAllInvoiceDetailsByDatesAndBranchAndBranchVendor(
        fromDate, toDate, orgId, branchId, branchVendorId);
  }

  @Override
  public InvoiceBill getAllInvoiceDetailsByDatesByRefernceNumber(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final String refernceNumber,
      final long branchId,
      final long branchvendorId) {
    return this.invoiceBillDao.getAllInvoiceDetailsByDatesByRefernceNumber(
        fromDate, toDate, orgId, refernceNumber, branchId, branchvendorId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchVendorAndOrganisation(
      final Date fromDate, final Date toDate, final long branchVendorId, final long orgId) {
    return invoiceBillDao.getAllInvoiceDetailsByDatesAndBranchVendorAndOrganisation(
        fromDate, toDate, branchVendorId, orgId);
  }

  @Override
  public List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDates(
      final Date formDate,
      final Date endDate,
      final String referenceNumber,
      final long branchId,
      final long orgId,
      final long frombranchId) {
    return invoiceBillDao.getInVoiceBillDetailsByBillIdAndDates(
        formDate, endDate, referenceNumber, branchId, orgId, frombranchId);
  }

  @Override
  public List<InvoiceBill> getInvoiceBillsTotalAmt(
      final Date formDate,
      final Date endDate,
      final long branchId,
      final long orgId,
      final long frombranchId) {
    return invoiceBillDao.getInvoiceBillsTotalAmt(formDate, endDate, branchId, orgId, frombranchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsVendorNameTotalAmount(
      final long orgId, final Date fromDate, final Date toDate, final long branchId) {
    return invoiceBillDao.getAllInvoiceDetailsVendorNameTotalAmount(
        orgId, fromDate, toDate, branchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesVendor(
      final long orgId,
      final Date fromDate,
      final Date toDate,
      final long branchId,
      final long vendorId) {
    return invoiceBillDao.getAllInvoiceDetailsByDatesVendor(
        orgId, fromDate, toDate, branchId, vendorId);
  }

  @Override
  public List<Object[]> getAllInvoiceDetailsBySection(
      final long orgId,
      final long branchId,
      final Date fromDate,
      final Date toDate,
      final long fromSectionId) {
    return invoiceBillDao.getAllInvoiceDetailsBySection(
        orgId, branchId, fromDate, toDate, fromSectionId);
  }

  @Override
  public List<Object[]> getAllInvoiceDetailsByFromBranch(
      final long orgId,
      final long branchId,
      final Date fromDate,
      final Date toDate,
      final long fromBranchId) {
    return invoiceBillDao.getAllInvoiceDetailsByFromBranch(
        orgId, branchId, fromDate, toDate, fromBranchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDateBydate(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {
    return invoiceBillDao.getAllInvoiceDetailsByDateBydate(fromDate, toDate, orgId, branchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByBranchAndVendorIsNull(
      final long orgId, final Date fromdate, final Date todate, final long branchId) {
    return invoiceBillDao.getAllInvoiceDetailsByBranchAndVendorIsNull(
        orgId, fromdate, todate, branchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchAndFromSectionId(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long fromSectionId) {
    return invoiceBillDao.getAllInvoiceDetailsByDatesAndBranchAndFromSectionId(
        fromDate, toDate, orgId, branchId, fromSectionId);
  }

  @Override
  public InvoiceBill getInvoiceDetailsByDatesAndRefernceNumberAndBranchAndFromSectionId(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final String referenceNumber,
      final long branchId,
      final long fromSectionId) {
    return invoiceBillDao.getInvoiceDetailsByDatesAndRefernceNumberAndBranchAndFromSectionId(
        fromDate, toDate, orgId, referenceNumber, branchId, fromSectionId);
  }

  @Override
  public List<InvoiceBill> getInvoiceBillByOrgIdBranchIdSectionId(
      final Date fromdate,
      final Date todate,
      final long fromSectionId,
      final long orgId,
      final long branchId) {
    return invoiceBillDao.getInvoiceBillByOrgIdBranchIdSectionId(
        fromdate, todate, fromSectionId, orgId, branchId);
  }

  @Override
  public List<InvoiceBill> getInvoiceBillByTransferOrgIdFromBranchId(
      final Date fromdate,
      final Date todate,
      final long fromBranchId,
      final long orgId,
      final long branchId) {
    return invoiceBillDao.getInvoiceBillByTransferOrgIdFromBranchId(
        fromdate, todate, fromBranchId, orgId, branchId);
  }

  @Override
  public List<InvoiceBill> getInvoiceBillByTransferOrgIdBranchIdSectionId(
      final Date fromdate,
      final Date todate,
      final long fromSectionId,
      final long orgId,
      final long branchId) {
    return invoiceBillDao.getInvoiceBillByTransferOrgIdBranchIdSectionId(
        fromdate, todate, fromSectionId, orgId, branchId);
  }

  @Override
  public List<InvoiceBill> getInvoiceBillByOrgIdFromBranchId(
      final Date fromdate,
      final Date todate,
      final long fromBranchId,
      final long orgId,
      final long branchId) {
    return invoiceBillDao.getInvoiceBillByOrgIdFromBranchId(
        fromdate, todate, fromBranchId, orgId, branchId);
  }

  @Override
  public List<Object[]> getAllInvoiceBillProductQuantityByFromToBranch(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long frombranchId) {
    return invoiceBillDao.getAllInvoiceBillProductQuantityByFromToBranch(
        orgId, branchId, fromdate, todate, frombranchId);
  }

  @Override
  public List<Object[]> getAllInvoiceBillProductQuantityByFromSectionToBranch(
      final long orgId,
      final long branchId,
      final Date fromdate,
      final Date todate,
      final long fromSectionId) {
    return invoiceBillDao.getAllInvoiceBillProductQuantityByFromSectionToBranch(
        orgId, branchId, fromdate, todate, fromSectionId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByBranchAndVendorIdIsNull(
      final long orgId, final Date fromdate, final Date todate, final long fromBranchId) {
    return invoiceBillDao.getAllInvoiceDetailsByBranchAndVendorIdIsNull(
        orgId, fromdate, todate, fromBranchId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByBranchAndSectionIdIsNull(
      final long orgId, final Date fromdate, final Date todate, final long fromSectionId) {
    return invoiceBillDao.getAllInvoiceDetailsByBranchAndSectionIdIsNull(
        orgId, fromdate, todate, fromSectionId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceBillBySection(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long fromsectionId) {
    return invoiceBillDao.getAllInvoiceBillBySection(
        fromDate, toDate, orgId, branchId, fromsectionId);
  }

  @Override
  public List<InvoiceBill> getAllInvoiceBillBySectionreference(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long fromsectionId,
      final String reference) {
    return invoiceBillDao.getAllInvoiceBillBySectionreference(
        fromDate, toDate, orgId, branchId, fromsectionId, reference);
  }

  @Override
  public List<Object[]> getAllInvoiceBillByTotalAmountSection(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long fromsectionId) {
    return invoiceBillDao.getAllInvoiceBillByTotalAmountSection(
        fromDate, toDate, orgId, branchId, fromsectionId);
  }

  @Override
  public List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDatesToBranchReference(
      final Date formDate,
      final Date endDate,
      final String referenceNumber,
      final long branchId,
      final long orgId,
      final long fromSectionId) {
    return invoiceBillDao.getInVoiceBillDetailsByBillIdAndDatesToBranchReference(
        formDate, endDate, referenceNumber, branchId, orgId, fromSectionId);
  }

  @Override
  public List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDatesToBranch(
      final Date formDate,
      final Date endDate,
      final long branchId,
      final long orgId,
      final long fromSectionId) {
    return invoiceBillDao.getInVoiceBillDetailsByBillIdAndDatesToBranch(
        formDate, endDate, branchId, orgId, fromSectionId);
  }

  @Override
  public List<Map<String, Object>> getInvoiceBillDetailsByLoginVendor(
      final long vendorId, final long orgId) {
    final List<Object[]> invoicebilldetails =
        invoiceBillDao.getInvoiceBillDetailsByLoginVendor(vendorId, orgId);
    final List<Map<String, Object>> invoicebillList = new ArrayList<Map<String, Object>>();
    for (final Object[] object : invoicebilldetails) {
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("id", object[0]);
      map.put("invoiceNumber", object[1]);
      map.put("note", object[2]);
      map.put("totalAmount", object[3]);
      map.put("taxableAmount", object[4]);
      map.put("discountAmount", object[5]);
      map.put("date", object[6]);
      map.put("vendorName", object[7]);
      map.put("branchName", object[8]);
      map.put("image", object[9]);
      invoicebillList.add(map);
    }
    return invoicebillList;
  }

  @Override
  public List<Object[]> getInvoiceBillDetailsByReport(
      Date fromDate, Date toDate, long orgId, long branchId) {
    // TODO Auto-generated method stub
    return invoiceBillDao.getInvoiceBillDetailsByReport(fromDate, toDate, orgId, branchId);
  }
}
