package com.a2zbill.services.impl;

import com.a2zbill.dao.CartProductDao;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.DiscountCoupons;
import com.a2zbill.domain.LooseProduct;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.Review;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CartProductService;
import com.a2zbill.services.CartService;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.DiscountCouponsService;
import com.a2zbill.services.LooseProductService;
import com.a2zbill.services.ReviewService;
import com.offers.domain.CustomerTransactionSummary;
import com.offers.domain.Offers;
import com.offers.domain.Reward;
import com.offers.domain.RewardHistory;
import com.offers.services.CustomerTransactionSummaryService;
import com.offers.services.OffersService;
import com.offers.services.RewardHistoryService;
import com.offers.services.RewardService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CartProductServiceImpl implements CartProductService {

  private static final Logger LOGGER = LoggerFactory.getLogger(CartProductServiceImpl.class);

  private static final String looseProductCode = "#w";
  private static final String CART_STATUS = "Open";
  private static final BigDecimal HUNDRED = new BigDecimal(100);

  private static final boolean STATUS_TRUE = true;

  @Autowired private CartProductDao cartProductDao;
  @Autowired private LooseProductService looseProductService;
  @Autowired private EmployeeService employeeService;

  @Autowired private BranchService branchService;
  @Autowired private OffersService offersService;
  @Autowired private DiscountCouponsService discountCouponsService;

  @Autowired private RewardService rewardService;
  @Autowired private OrganisationService organisationService;

  @Autowired private CartService cartService;
  @Autowired private BillingService billingService;
  @Autowired private CounterService counterService;
  @Autowired private ProductServiceImpl productsService;
  @Autowired private CartProductService cartProductService;

  @Autowired private ReviewService reviewService;

  @Autowired private CustomerTransactionSummaryService customerrewardsService;
  @Autowired private RewardHistoryService rewardHistoryService;

  @Autowired private CustomerService customerService;

  @Override
  public void save(final CartDetail cartDetails) {
    this.cartProductDao.save(cartDetails);
  }

  @Override
  public boolean delete(final Long id) {
    this.cartProductDao.delete(id);
    return true;
  }

  @Override
  public boolean update(final CartDetail cartDetails) {
    this.cartProductDao.update(cartDetails);
    return true;
  }

  @Override
  public List<CartDetail> getAllCounterDetails() {
    return new ArrayList<>();
  }

  @Override
  public List<CartDetail> getCartDetailsByCartId(final long cartId) {
    return this.cartProductDao.getCartDetailsByCartId(cartId);
  }

  @Override
  public List<CartDetail> getCartDetailsByCartIdAndDeptIdAndCartStatus(
      final Long cartId, final String cartStatus, final Long deptId) {
    return this.cartProductDao.getCartDetailsByCartIdAndDeptIdAndCartStatus(
        cartId, cartStatus, deptId);
  }

  @Override
  public List<CartDetail> getCartDetailsDeptIdAndCartStatus() {
    return this.cartProductDao.getCartDetailsDeptIdAndCartStatus();
  }

  @Override
  public List<CartDetail> getCartDetailsByCounterIdAndFlowId() {
    return this.cartProductDao.getCartDetailsByCounterIdAndFlowId();
  }

  @Override
  public List<CartDetail> getCartDetailsByCounterIdAndFlowId(
      final Long counterId, final Long flowId) {
    return this.cartProductDao.getCartDetailsByCounterIdAndFlowId(counterId, flowId);
  }

  @Override
  public List<CartDetail> getCartDetailsByFlowId() {
    return this.cartProductDao.getCartDetailsByFlowId();
  }

  @Override
  public CartDetail getCartDetailsByProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode) {
    return this.cartProductDao.getCartDetailsByProductNameAndHsnCode(
        counterId, productName, hsnCode);
  }

  @Override
  public CartDetail getCartDetailsByCounterIdProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode) {
    return this.cartProductDao.getCartDetailsByCounterIdProductNameAndHsnCode(
        counterId, productName, hsnCode);
  }

  @Override
  public List<CartDetail> getCartDetailsGroupByProductName(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {
    return this.cartProductDao.getCartDetailsGroupByProductName(fromDate, toDate, orgId, branchId);
  }

  public Map<String, Object> addingProductToCart(
      final long orgId, final long branchId, final String code) {

    final Long QTY = 1L;
    Product productDetails = null;
    final Map<String, Object> productDetails1 = new HashMap<>();
    try {
      if (code.contains(looseProductCode)) {
        final String[] codes = code.split(looseProductCode);
        final String loosecode = codes[1];
        @SuppressWarnings("unused")
        final LooseProduct looseProduct =
            this.looseProductService.getLooseProductDetailsByCode(loosecode);
      } else {
        if (branchId != 0l) {
          productDetails =
              this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                  code, orgId, branchId);
          if (productDetails == null) {
            productDetails1.put(
                "product", "Products Not Available For this Organisation and Branch");
            return productDetails1;
          }
        } else {
          productDetails = this.productsService.getProductsByProductCodeAndOrgId(code, orgId);
        }
      }
      if (productDetails != null) {
        productDetails1.put("ProductCode", productDetails.getCode());
        productDetails1.put("ProductName", productDetails.getProductName());
        productDetails1.put("HsnNumber", productDetails.getHsnNumber());
        productDetails1.put("cartId", 0);
        if (productDetails.getIncludingTaxFlag().equals("true")) {
          final BigDecimal price =
              productDetails
                  .getProductPrice()
                  .subtract(
                      (productDetails.getProductPrice())
                          .multiply(productDetails.getSGST().add(productDetails.getCGST()))
                          .divide(HUNDRED));
          productDetails1.put("ProductPrice", price);
          productDetails1.put("SGST", productDetails.getSGST());
          productDetails1.put("CGST", productDetails.getCGST());
        } else {
          productDetails1.put("ProductPrice", productDetails.getProductPrice());
          productDetails1.put("SGST", productDetails.getSGST());
          productDetails1.put("CGST", productDetails.getCGST());
        }
        productDetails1.put("Quantity", QTY);
        productDetails1.put("IGST", productDetails.getIGST());
        return productDetails1;
      } else {
        productDetails1.put("product", "Products Not Available");
        return productDetails1;
      }
    } catch (final Exception ex) {
      LOGGER.info("gettingRestaurentCartProductDetails1" + ex, ex);
    }
    return productDetails1;
  }

  public Map<String, Long> generateBill(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      final long employeeNumber,
      final String payload)
      throws JSONException {
    final Map<String, Long> map = new HashMap<>();
    final Billing billing = new Billing();
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Review review = new Review();
    final long orgId = employeeDetails.getOrganisation().getId();
    final String offerCode = request.getParameter("offerCode");
    if (offerCode == null) {
      LOGGER.error("offerCode does not exist");
    }
    final long counterNumber = Long.parseLong(request.getParameter("counterNumber"));
    final long flowsId = Long.parseLong(request.getParameter("flowsId"));
    final String splitbillFlag = request.getParameter("splitbillFlag");
    final Counter counterDetails = this.counterService.getCounterDetailsById(counterNumber);
    Long newCartId = null;
    Cart cartDetails = new Cart();
    if (flowsId == 3) {
      Cart cart = new Cart();
      if (splitbillFlag.equals("true")) {
        final long cartid = Long.parseLong(request.getParameter("cartid"));
        cart = this.cartService.getCartDetailsByCartId(cartid);
        if (cart.getCartStatus().equals("Open")) {
          cart.setCartStatus("Closed");
          this.cartService.update(cart);
        }
        if (orgId == cart.getOrganisation().getId()) {
          if (cart.getGuid() != null) {
            cart = this.cartService.getCartByGuid("guid");
          } else {
            cart = new Cart();
            cart.setCartStatus(CART_STATUS);
            final Date date = new Date();
            cart.setStartTime(date);
            cart.setEndTime(date);
            cart.setCounterDetails(counterDetails);
            cart.setParentguId("0");
            cart.setDescription("bill was splitted");
            this.cartService.save(cart);
            newCartId = cart.getCartId();
            final JSONArray ja = new JSONArray(payload);
            for (int i = 0; i < ja.length(); ++i) {
              final JSONObject jsonObject = ja.getJSONObject(i);
              final JSONObject products = jsonObject.getJSONObject("code");
              cart = this.cartService.getCartDetailsByCartId(newCartId);
              final CartDetail cartProductDetails = new CartDetail();
              cartProductDetails.setCartDetails(cart);
              cartProductDetails.setCounterDetails(cart.getCounterDetails());
              cartProductDetails.setDiscountAmount(null);
              cartProductDetails.setHsnCode(products.getString("HsnNumber"));
              cartProductDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
              cartProductDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));
              cartProductDetails.setProductName(products.getString("ProductName"));
              cartProductDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
              cartProductDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));
              final BigDecimal totalAmount =
                  new BigDecimal(products.getInt("ProductPrice"))
                      .multiply(new BigDecimal(products.getString("Quantity")));
              final BigDecimal taxableAmount =
                  new BigDecimal(products.getInt("CGST"))
                      .add(new BigDecimal(products.getInt("SGST")))
                      .multiply(new BigDecimal(products.getInt("ProductPrice")))
                      .divide(HUNDRED);
              cartProductDetails.setTotalAmount(totalAmount);
              cartProductDetails.setTaxableAmount(
                  taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
              final Date CartTime = new Date();
              cartProductDetails.setTime(CartTime);
              final Branch branch =
                  this.branchService.getBranchById(employeeDetails.getBranch().getId());
              cartProductDetails.setBranch(branch);
              final Organisation organisation =
                  this.organisationService.getOrganisationById(
                      employeeDetails.getOrganisation().getId());
              cartProductDetails.setOrganisation(organisation);
              this.cartProductService.save(cartProductDetails);
            }
          }
        } else {
          final JSONArray ja = new JSONArray(payload);
          for (int i = 0; i < ja.length(); ++i) {
            final JSONObject jsonObject = ja.getJSONObject(i);
            final JSONObject products = jsonObject.getJSONObject("code");
            review.setGuid(products.getString("guid"));
            review.setParentGuid(products.getString("parentGuid"));
            review.setBillDate(new Date());
            final Branch branch =
                this.branchService.getBranchById(employeeDetails.getBranch().getId());
            review.setBranch(branch);
            final Organisation organisation =
                this.organisationService.getOrganisationById(
                    employeeDetails.getOrganisation().getId());
            review.setOrganisation(organisation);
            review.setBillAmount(new BigDecimal(products.getInt("billingAmount")));
            review.setTaxableAmount(new BigDecimal(products.getInt("totalTaxableAmount")));
            this.reviewService.update(review);
          }
        }
      } else {
        cart = this.cartService.getCartDetailsOnStatus(CART_STATUS, counterNumber);
        if (cart != null) {
          cart.setCartStatus(CART_STATUS);
          final Date date = new Date();
          cart.setStartTime(date);
          cart.setEndTime(date);
          cart.setCounterDetails(counterDetails);
          cart.setParentguId("0");
          cart.setDescription("splitBillFlag is false");
          this.cartService.save(cart);
        }
      }
      final String customerMobileNumber = request.getParameter("customerMobileNumber");
      if (splitbillFlag.equals("true")) {
        billing.setCartId(cart);
      }
      final String discountPercentage = request.getParameter("discountPercentage");
      final String discountAmount = request.getParameter("discountAmount");
      final BigDecimal sumOfTotalAmount = new BigDecimal(request.getParameter("netAmount"));
      final String sumOfTaxableAmount = request.getParameter("taxableAmount");
      final String sumOfcgstAmount = request.getParameter("cgstAmount");
      final String sumOfsgstAmount = request.getParameter("sgstAmount");
      billing.setTotalAmount(sumOfTotalAmount);
      billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));
      billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
      billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
      billing.setBillDate(new Date());
      billing.setCounterDetails(cart.getCounterDetails());
      if (customerMobileNumber.equals("")) {
        billing.setCustomer(null);
      } else {
        billing.setCustomer(this.customerService.getCustomerByMobileNumber(customerMobileNumber));
        final Integer amount =
            this.billingService.getOffersByOfferCodeAndCustId(
                offerCode, customerMobileNumber, sumOfTotalAmount);
        final Long disAmount =
            Long.valueOf((sumOfTotalAmount.subtract(new BigDecimal(amount))).longValue());
        billing.setTotalAmount(sumOfTotalAmount);
        final List<Reward> rewards = this.rewardService.getrewardsBystatus(STATUS_TRUE);
        for (final Reward reward : rewards) {
          if (reward.getActive() == STATUS_TRUE) {
            final CustomerTransactionSummary customerReward = new CustomerTransactionSummary();
            final BigDecimal TotalAmount = sumOfTotalAmount;
            final long rewardpoints = TotalAmount.divide(HUNDRED).longValue();
            customerReward.setCustomer(billing.getCustomer().getCustomerId());
            customerReward.setRewardPoints(rewardpoints);
            customerReward.setCreateDate(new Date());
            customerReward.setModifiedDate(new Date());
            this.customerrewardsService.save(customerReward);
            final RewardHistory rewardHistory = new RewardHistory();
            final Date date = new Date();
            rewardHistory.setCustomer(billing.getCustomer());
            rewardHistory.setRewardPoints(rewardpoints);
            rewardHistory.setRedeemAmount(rewardpoints);
            rewardHistory.setDate(new Date());
            rewardHistory.setStartTime(date);
            this.rewardHistoryService.save(rewardHistory);
          }
        }
        map.put("discountAmount", disAmount);
      }
      billing.setEmployeeDetails(employeeDetails);
      billing.setDiscountPercentage(new BigDecimal(discountPercentage));
      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setOrganisation(employeeDetails.getOrganisation());
      billing.setBranch(employeeDetails.getBranch());
      billing.setCartId(cart);
      this.billingService.save(billing);
      cart.setCartStatus("Closed");
      this.cartService.update(cart);
      counterDetails.setCounterStatus("Closed");
      this.counterService.update(counterDetails);
      map.put("billingId", this.billingService.getBillingDetailsByCartId(cart.getCartId()).getId());
      return map;
    } else if (flowsId == 2) {
      final Cart cartDetails1 = new Cart();
      cartDetails1.setCartStatus(CART_STATUS);
      Date date = new Date();
      cartDetails1.setStartTime(date);
      cartDetails1.setEndTime(date);
      cartDetails1.setCounterDetails(counterDetails);
      cartDetails1.setParentguId("0");
      cartDetails1.setDescription("parcel flow");
      this.cartService.save(cartDetails1);
      final JSONArray ja = new JSONArray(payload);
      for (int i = 0; i < ja.length(); ++i) {
        final JSONObject jsonObject = ja.getJSONObject(i);
        final JSONObject products = jsonObject.getJSONObject("code");
        final Product product =
            this.productsService.getProductsByHsnCodeAndProductNameByBranchIdAndOrgId(
                products.getString("HsnNumber"), products.getString("ProductName"),
                employeeDetails.getBranch().getId(), employeeDetails.getOrganisation().getId());
        final long id = cartDetails1.getCartId();
        final Cart cart = this.cartService.getCartDetailsByCartId(id);
        final CartDetail cartProductDetails = new CartDetail();
        cartProductDetails.setCartDetails(cart);
        cartProductDetails.setCounterDetails(cart.getCounterDetails());
        cartProductDetails.setDiscountAmount(null);
        cartProductDetails.setHsnCode(products.getString("HsnNumber"));
        cartProductDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
        cartProductDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));
        cartProductDetails.setProductName(products.getString("ProductName"));
        cartProductDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
        cartProductDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));
        final BigDecimal totalAmount =
            new BigDecimal(products.getInt("ProductPrice"))
                .multiply(new BigDecimal(products.getString("Quantity")));
        final BigDecimal taxableAmount =
            new BigDecimal(products.getInt("CGST"))
                .add(new BigDecimal(products.getInt("SGST")))
                .multiply(new BigDecimal(products.getInt("ProductPrice")))
                .divide(HUNDRED);
        cartProductDetails.setTotalAmount(totalAmount);
        cartProductDetails.setTaxableAmount(
            taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
        final Date CartTime = new Date();
        cartProductDetails.setTime(CartTime);
        final Branch branch = this.branchService.getBranchById(employeeDetails.getBranch().getId());
        cartProductDetails.setBranch(branch);
        final Organisation organisation =
            this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
        cartProductDetails.setOrganisation(organisation);
        this.cartProductService.save(cartProductDetails);
        final String kafkadata =
            employeeDetails.getBranch().getId()
                + "|"
                + products.getString("ProductName")
                + "|"
                + products.getString("Quantity")
                + "|"
                + counterNumber
                + "|"
                + employeeDetails.getId()
                + "|"
                + product.getImage();
        LOGGER.info("parcel flow details sending to kafka:" + kafkadata);
        try {
          LOGGER.info("parcel flow details sent to kafka:" + kafkadata);
        } catch (final Exception ex) {
          LOGGER.error("parcel flow details send to kafka:" + ex, ex);
        }
      }
      long cartId = cartDetails1.getCartId();
      final Cart cart = this.cartService.getCartDetailsByCartId(cartId);
      final String customerMobileNumber = request.getParameter("customerMobileNumber");
      billing.setCartId(cart);
      final String discountPercentage = request.getParameter("discountPercentage");
      final String discountAmount = request.getParameter("discountAmount");
      final BigDecimal sumOfTotalAmount = new BigDecimal(request.getParameter("netAmount"));
      final String sumOfTaxableAmount = request.getParameter("taxableAmount");
      final String sumOfcgstAmount = request.getParameter("cgstAmount");
      final String sumOfsgstAmount = request.getParameter("sgstAmount");
      billing.setTotalAmount(sumOfTotalAmount);
      billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));
      billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
      billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
      billing.setBillDate(new Date());
      billing.setCounterDetails(cart.getCounterDetails());
      if (customerMobileNumber.equals("")) {
        billing.setCustomer(null);
      } else {
        billing.setCustomer(this.customerService.getCustomerByMobileNumber(customerMobileNumber));
        final Integer amount =
            this.billingService.getOffersByOfferCodeAndCustId(
                offerCode, customerMobileNumber, sumOfTotalAmount);
        final Long disAmount =
            Long.valueOf((sumOfTotalAmount.subtract(new BigDecimal(amount))).longValue());
        billing.setTotalAmount(sumOfTotalAmount);
        cartId = cartDetails.getCartId();
        if (offerCode != null) {
          if (offerCode.startsWith("F")) {
            final Offers offer = this.offersService.getOffersByOfferCode(offerCode);
            if (offer.getDiscountType().getId() == 1) {
              final BigDecimal bill =
                  sumOfTotalAmount.multiply((offer.getDiscountValue()).divide(HUNDRED));
              final BigDecimal billWithOffer = sumOfTotalAmount.subtract(bill);
              billing.setTotalAmount(billWithOffer);
            } else if (offer.getDiscountType().getId() == 2) {
              final BigDecimal bill = sumOfTotalAmount.subtract(offer.getDiscountValue());
              billing.setTotalAmount(bill);
            }
          } else if (offerCode.startsWith("p")) {
            @SuppressWarnings("unused")
            final DiscountCoupons discountCoupons =
                this.discountCouponsService.getDiscountCouponsByOfferCode(offerCode);
          }
        }
        final List<Reward> rewards = this.rewardService.getrewardsBystatus(STATUS_TRUE);
        for (final Reward reward : rewards) {
          if (reward.getActive() == STATUS_TRUE) {
            final CustomerTransactionSummary customerReward = new CustomerTransactionSummary();
            final BigDecimal TotalAmount = sumOfTotalAmount;
            final long rewardpoints = TotalAmount.divide(HUNDRED).longValue();
            customerReward.setCustomer(billing.getCustomer().getCustomerId());
            customerReward.setRewardPoints(rewardpoints);
            customerReward.setCreateDate(new Date());
            customerReward.setModifiedDate(new Date());
            this.customerrewardsService.save(customerReward);
            final RewardHistory rewardHistory = new RewardHistory();
            date = new Date();
            rewardHistory.setCustomer(billing.getCustomer());
            rewardHistory.setRewardPoints(rewardpoints);
            rewardHistory.setRedeemAmount(rewardpoints);
            rewardHistory.setDate(new Date());
            rewardHistory.setStartTime(date);
            this.rewardHistoryService.save(rewardHistory);
          }
        }
        map.put("discountAmount", disAmount);
      }
      billing.setEmployeeDetails(employeeDetails);
      billing.setDiscountPercentage(new BigDecimal(discountPercentage));
      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setOrganisation(employeeDetails.getOrganisation());
      billing.setBranch(employeeDetails.getBranch());
      this.billingService.save(billing);
      cart.setCartStatus("Closed");
      this.cartService.update(cart);
      map.put("billingId", this.billingService.getBillingDetailsByCartId(cartId).getId());
      return map;
    } else {
      cartDetails = new Cart();
      cartDetails.setCartStatus(CART_STATUS);
      Date date = new Date();
      cartDetails.setStartTime(date);
      cartDetails.setEndTime(date);
      cartDetails.setCounterDetails(counterDetails);
      cartDetails.setParentguId("0");
      cartDetails.setDescription("Dmart");
      this.cartService.save(cartDetails);
      final JSONArray ja = new JSONArray(payload);
      for (int i = 0; i < ja.length(); ++i) {
        final JSONObject jsonObject = ja.getJSONObject(i);
        final JSONObject products = jsonObject.getJSONObject("code");
        final long id = cartDetails.getCartId();
        final Cart cart = this.cartService.getCartDetailsByCartId(id);
        final CartDetail cartProductDetails = new CartDetail();
        cartProductDetails.setCartDetails(cart);
        cartProductDetails.setCounterDetails(cart.getCounterDetails());
        cartProductDetails.setDiscountAmount(null);
        cartProductDetails.setHsnCode(products.getString("HsnNumber"));
        cartProductDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
        cartProductDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));
        cartProductDetails.setProductName(products.getString("ProductName"));
        cartProductDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
        cartProductDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));
        final BigDecimal totalAmount =
            new BigDecimal(products.getInt("ProductPrice"))
                .multiply(new BigDecimal(products.getString("Quantity")));
        final BigDecimal taxableAmount =
            new BigDecimal(products.getInt("CGST"))
                .add(new BigDecimal(products.getInt("SGST")))
                .multiply(new BigDecimal(products.getInt("ProductPrice")))
                .divide(HUNDRED);
        cartProductDetails.setTotalAmount(totalAmount);
        cartProductDetails.setTaxableAmount(
            taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
        final Date CartTime = new Date();
        cartProductDetails.setTime(CartTime);
        final Branch branch = this.branchService.getBranchById(employeeDetails.getBranch().getId());
        cartProductDetails.setBranch(branch);
        final Organisation organisation =
            this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
        cartProductDetails.setOrganisation(organisation);
        this.cartProductService.save(cartProductDetails);
      }
      long cartId = cartDetails.getCartId();
      final Cart cart = this.cartService.getCartDetailsByCartId(cartId);
      final String customerMobileNumber = request.getParameter("customerMobileNumber");
      billing.setCartId(cart);
      final String discountPercentage = request.getParameter("discountPercentage");
      final String discountAmount = request.getParameter("discountAmount");
      final BigDecimal sumOfTotalAmount = new BigDecimal(request.getParameter("netAmount"));
      final String sumOfTaxableAmount = request.getParameter("taxableAmount");
      final String sumOfcgstAmount = request.getParameter("cgstAmount");
      final String sumOfsgstAmount = request.getParameter("sgstAmount");
      billing.setTotalAmount(sumOfTotalAmount);
      billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));
      billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
      billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
      billing.setBillDate(new Date());
      billing.setCounterDetails(cart.getCounterDetails());
      if (customerMobileNumber.equals("")) {
        billing.setCustomer(null);
      } else {
        billing.setCustomer(this.customerService.getCustomerByMobileNumber(customerMobileNumber));
        final Integer amount =
            this.billingService.getOffersByOfferCodeAndCustId(
                offerCode, customerMobileNumber, sumOfTotalAmount);
        final Long disAmount =
            Long.valueOf((sumOfTotalAmount.subtract(new BigDecimal(amount))).longValue());
        billing.setTotalAmount(sumOfTotalAmount);
        cartId = cartDetails.getCartId();
        if (offerCode != null) {
          if (offerCode.startsWith("F")) {
            final Offers offer = this.offersService.getOffersByOfferCode(offerCode);
            if (offer.getDiscountType().getId() == 1) {
              final BigDecimal bill =
                  sumOfTotalAmount.multiply((offer.getDiscountValue()).divide(HUNDRED));
              final BigDecimal billWithOffer = sumOfTotalAmount.subtract(bill);
              billing.setTotalAmount(billWithOffer);
            } else if (offer.getDiscountType().getId() == 2) {
              final BigDecimal bill = sumOfTotalAmount.subtract(offer.getDiscountValue());
              billing.setTotalAmount(bill);
            }
          } else if (offerCode.startsWith("p")) {
            @SuppressWarnings("unused")
            final DiscountCoupons discountCoupons =
                this.discountCouponsService.getDiscountCouponsByOfferCode(offerCode);
          }
        }
        final List<Reward> rewards = this.rewardService.getrewardsBystatus(STATUS_TRUE);
        for (final Reward reward : rewards) {
          if (reward.getActive() == STATUS_TRUE) {
            final CustomerTransactionSummary customerReward = new CustomerTransactionSummary();
            final BigDecimal TotalAmount = sumOfTotalAmount;
            final long rewardpoints = TotalAmount.divide(HUNDRED).longValue();
            customerReward.setCustomer(billing.getCustomer().getCustomerId());
            customerReward.setRewardPoints(rewardpoints);
            customerReward.setCreateDate(new Date());
            customerReward.setModifiedDate(new Date());
            this.customerrewardsService.save(customerReward);
            final RewardHistory rewardHistory = new RewardHistory();
            date = new Date();
            rewardHistory.setCustomer(billing.getCustomer());
            rewardHistory.setRewardPoints(rewardpoints);
            rewardHistory.setRedeemAmount(rewardpoints);
            rewardHistory.setDate(new Date());
            rewardHistory.setStartTime(date);
            this.rewardHistoryService.save(rewardHistory);
          }
        }
        map.put("discountAmount", disAmount);
      }
      billing.setEmployeeDetails(employeeDetails);
      billing.setDiscountPercentage(new BigDecimal(discountPercentage));
      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setOrganisation(employeeDetails.getOrganisation());
      billing.setBranch(employeeDetails.getBranch());
      this.billingService.save(billing);
      cart.setCartStatus("Closed");
      this.cartService.update(cart);
      map.put("billingId", this.billingService.getBillingDetailsByCartId(cartId).getId());
      return map;
    }
  }

  @Override
  public List<CartDetail> getproductQuantityByBranchIdAndDate(
      final long orgId, final Long branchId, final Date date) {
    return this.cartProductDao.getproductQuantityByBranchIdAndDate(orgId, branchId, date);
  }

  @Override
  public List<CartDetail> getCartDetailsGroupByProductNameifBrnachNull(
      final Date fromDate, final Date toDate, final long orgId) {
    return this.cartProductDao.getCartDetailsGroupByProductNameifBrnachNull(
        fromDate, toDate, orgId);
  }

  @Override
  public List<CartDetail> getproductQuantityByBranchAndOrgIdAndDate(
      final long orgId, final long branchId, final Date fromDate, final Date toDate) {
    return this.cartProductDao.getproductQuantityByBranchAndOrgIdAndDate(
        orgId, branchId, fromDate, toDate);
  }

  @Override
  public List<CartDetail> getproductQuantityByOrgIdAndDate(
      final long orgId, final Date fromDate, final Date toDate) {
    return this.cartProductDao.getproductQuantityByOrgIdAndDate(orgId, fromDate, toDate);
  }

  @Override
  public List<Object[]> getIngredientWiseProductQuantitySumByOrgIdAndDate(
      final long orgId, final long branchId, final Date date) {
    return this.cartProductDao.getIngredientWiseProductQuantitySumByOrgIdAndDate(
        orgId, branchId, date);
  }

  @Override
  public List<BigDecimal> getIngredientWiseProductQuantitySumByOrgIdBranchIdAndDate(
      final long orgId, final long branchId, final Date date) {
    return null;
  }

  @Override
  public List<CartDetail> getCartDetailsByProductName(
      final String productName, final long orgId, final long branchId) {
    return null;
  }

  @Override
  public CartDetail getCartDetailByCartId(final long cartId) {
    return cartProductDao.getCartDetailByCartId(cartId);
  }

  @Override
  public List<Object[]> getCartDetailsByCartDetailsId(long cartId) {
    return cartProductDao.getCartDetailsByCartDetailsId(cartId);
  }
}
