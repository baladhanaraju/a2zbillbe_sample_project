package com.a2zbill.services.impl;

import com.a2zbill.dao.ReviewDao;
import com.a2zbill.domain.Review;
import com.a2zbill.services.ReviewService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ReviewServiceImpl implements ReviewService {
  @Autowired ReviewDao reviewDao;

  @Override
  public void save(final Review review) {
    this.reviewDao.save(review);
  }

  @Override
  public boolean delete(final Long id) {
    this.reviewDao.delete(id);
    return true;
  }

  @Override
  public boolean update(final Review review) {
    this.reviewDao.update(review);
    return true;
  }

  @Override
  public List<Review> getAllReviewDetails() {
    return this.reviewDao.getAllReviewDetails();
  }

  @Override
  public List<Review> getAllReviewDetailsByOrgBranchId(final long orgId, final long branchId) {
    return this.reviewDao.getAllReviewDetailsByOrgBranchId(orgId, branchId);
  }

  @Override
  public List<Review> getAllReviewDetailsByOrg(final long orgId) {
    return this.reviewDao.getAllReviewDetailsByOrg(orgId);
  }
}
