package com.a2zbill.services.impl;

import com.a2zbill.dao.RulesDao;
import com.a2zbill.domain.Rules;
import com.a2zbill.services.RulesService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RulesServiceImpl implements RulesService {
  @Autowired RulesDao rulesDao;

  @Override
  public void save(Rules rules) {
    this.rulesDao.save(rules);
  }

  @Override
  public boolean delete(Rules rules) {
    return this.rulesDao.delete(rules);
  }

  @Override
  public boolean update(Rules rules) {
    return this.rulesDao.update(rules);
  }

  @Override
  public List<Rules> getAllRulesDetails() {
    return this.rulesDao.getAllRulesDetails();
  }

  @Override
  public Rules getRuleDetailsByRuleId(long ruleId) {
    return this.rulesDao.getRuleDetailsByRuleId(ruleId);
  }

  @Override
  public List<Rules> getRulesDetailsByOrgAndBranch(long orgId, long branchId) {
    return this.rulesDao.getRulesDetailsByOrgAndBranch(orgId, branchId);
  }

  @Override
  public List<Rules> getRulesDetailsByOrg(long orgId) {
    return this.rulesDao.getRulesDetailsByOrg(orgId);
  }
}
