package com.a2zbill.services;

import com.a2zbill.domain.DocumentsData;
import com.a2zbill.domain.ProductCategory;
import java.util.List;

public interface DocumentsDataService {
  void saveDocumentData(final DocumentsData documentsData);

  List<ProductCategory> getCategoryDetailsByCategoryType(final long orgId, final long branchId);

  List<ProductCategory> getCategoryDetailsByCategoryTypeByOrg(final long orgId);
}
