package com.a2zbill.services;

import com.a2zbill.domain.FormData;
import java.util.Date;
import java.util.List;

public interface FormDataService {
  void save(final FormData formData);

  boolean delete(final long id);

  boolean update(final FormData formData);

  List<FormData> getAllFormData();

  List<FormData> getFormDataByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId);

  List<FormData> getFormDataByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId);

  Long getFormDataByOrgandBranch(final long orgId, final long branchId);
}
