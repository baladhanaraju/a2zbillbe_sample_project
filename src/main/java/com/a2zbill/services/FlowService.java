package com.a2zbill.services;

import com.a2zbill.domain.Flow;
import java.util.List;

public interface FlowService {
  void save(final Flow flow);

  boolean delete(final Long id);

  boolean update(final Flow flow);

  Flow getFlowById(final Long id);

  Flow getFlowByRaking(final Long flowRanking);

  List<Flow> getFlowByOrgId(final Long orgId);

  Flow getFlowByFlowTypeId(final Long flowTypeId, final Long flowRanking);

  List<Flow> getAllFlowDetails();
}
