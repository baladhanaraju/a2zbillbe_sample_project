package com.a2zbill.services;

import com.a2zbill.domain.VariationOptions;
import java.util.List;

public interface VariationOptionsService {

  List<VariationOptions> getAllVariationOptionsDetails();

  VariationOptions getVariationOptionsById(long id);

  VariationOptions getVariationOptionsByName(String name);

  List<VariationOptions> getVariationOptionsByType(long typeId);
}
