package com.a2zbill.services;

import com.a2zbill.domain.DayClose;
import java.util.Date;
import java.util.List;

public interface DayCloseService {
  void save(DayClose dayClose);

  boolean update(DayClose dayClose);

  List<DayClose> getAllDayCloseDetails();

  List<DayClose> getDayCloseDatails(Date fromdate, Date toDate, long branchId);
}
