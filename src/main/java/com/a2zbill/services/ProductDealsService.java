package com.a2zbill.services;

import com.a2zbill.domain.ProductDeals;
import java.util.List;

public interface ProductDealsService {

  void save(ProductDeals productDeals);

  boolean delete(final Long id);

  boolean update(ProductDeals productDeals);

  ProductDeals getProductDealsById(final Long id);

  List<ProductDeals> getAllProductDeals();

  ProductDeals getProductDealsByOfferCode(final String offerCode);

  ProductDeals getProductDealsByIdAndOfferCode(final Long id, final String offerCode);
}
