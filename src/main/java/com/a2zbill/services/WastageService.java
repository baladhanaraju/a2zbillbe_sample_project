package com.a2zbill.services;

import com.a2zbill.domain.Wastage;
import java.util.List;

public interface WastageService {

  public void save(Wastage wastage);

  public Boolean update(Wastage wastage);

  public Boolean delete(long id);

  List<Wastage> getAllWastages();
}
