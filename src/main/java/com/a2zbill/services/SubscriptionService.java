package com.a2zbill.services;

import com.a2zbill.domain.Subscription;
import java.util.Date;
import java.util.List;

public interface SubscriptionService {

  void save(Subscription subscription);

  boolean delete(final long id);

  boolean update(final Subscription subscription);

  List<Subscription> getAllSubscriptions();

  Subscription getSubscriptionByFrequencyId(long frequencyId);

  List<Subscription> getSubscriptionDetailsByDateAndOrgId(Date date, long orgId);

  List<Subscription> getSubscriptionDetailsByDateAndOrgIdAndbranchId(
      Date date, long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndorgId(
      Date date, long employeeId, long orgId);

  List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndOrgIdAndbranchId(
      Date date, long employeeId, long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByDateAndproductIdAndOrgId(
      Date date, long productId, long orgId);

  List<Subscription> getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
      Date date, long productId, long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgId(
      Date date, long employeeId, long productId, long orgId);

  List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgIdAndBranchId(
      Date date, long employeeId, long productId, long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgIdAndbranchId(
      long employeeId, long productId, long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgId(
      long employeeId, long productId, long orgId);

  List<Subscription> getSubscriptionDetailsByOrgId(long orgId);

  List<Subscription> getSubscriptionDetailsByOrgIdAndBranchId(long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByEmployeeIdAndOrgIdAndBranchId(
      long employeeId, long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByEmployeeIdAndOrgId(long employeeId, long orgId);

  List<Subscription> getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
      long productId, long orgId, long branchId);

  List<Subscription> getSubscriptionDetailsByProductIdAndOrgId(long productId, long orgId);
}
