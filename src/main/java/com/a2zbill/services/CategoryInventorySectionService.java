package com.a2zbill.services;

import com.a2zbill.domain.CategoryInventorySection;
import java.util.List;

public interface CategoryInventorySectionService {
  void save(final CategoryInventorySection categoryInventorySection);

  boolean delete(final long id);

  boolean update(final CategoryInventorySection categoryInventorySection);

  List<CategoryInventorySection> getAllCategoryInventorySection();

  CategoryInventorySection getCategoryInventorySectionByCategoryIdAndInventorySectionId(
      final long categoryId, final long inventorySectionId);
}
