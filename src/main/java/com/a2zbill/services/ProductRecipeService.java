package com.a2zbill.services;

import com.a2zbill.domain.ProductRecipe;
import java.util.List;

public interface ProductRecipeService {
  void save(ProductRecipe productRecipe);

  boolean delete(Long id);

  boolean update(ProductRecipe productRecipe);

  List<ProductRecipe> getAllProductRecipeDetails();

  ProductRecipe getProductRecipesById(long recipeId);

  List<ProductRecipe> getAllProductRecipesByOrgAndBranch(long orgId, long branchId);

  List<ProductRecipe> getAllProductRecipesByOrg(long orgId);

  List<ProductRecipe> getAllProductRecipesByIngredientsId(long ingredientsId);

  List<ProductRecipe> getAllProductRecipesById(long id);

  ProductRecipe getProductRecipesByRecipeName(String recipeName);

  ProductRecipe getProductRecipesByRecipebyId(long id);
}
