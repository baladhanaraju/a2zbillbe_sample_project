package com.a2zbill.services;

import com.a2zbill.domain.Section;
import java.util.List;

public interface SectionService {
  void save(Section section);

  boolean delete(long id);

  boolean update(Section section);

  List<Section> getAllSectionDetails();

  List<Section> getSectionDetailsByOrgIdAndBranchId(long orgId, long branchId);

  Section getSectionDetailsBySectionId(long sectionId);

  List<Section> getSectionDetailsByOrgId(long orgId);

  List<Section> getSectionDetailsByOrgAndStatus(long orgId);

  List<Section> getSectionDetailsByOrgIdAndBranchAndStatus(long orgId, long branchId);

  Section getSectiondetails(String sectionName);

  Section getSectiondetailsdisplayname(String displayName);
}
