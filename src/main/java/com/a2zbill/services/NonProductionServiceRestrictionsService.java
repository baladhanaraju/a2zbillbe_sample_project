package com.a2zbill.services;

import com.a2zbill.domain.NonProductionServiceRestrictions;
import java.util.List;

public interface NonProductionServiceRestrictionsService {

  void save(NonProductionServiceRestrictions nonProductionServiceRestrictions);

  boolean delete(long id);

  boolean update(NonProductionServiceRestrictions nonProductionServiceRestrictions);

  List<NonProductionServiceRestrictions> getAllNonProductionServiceRestrictions();
}
