package com.a2zbill.services;

import com.a2zbill.domain.FlowType;
import java.util.List;

public interface FlowTypeService {

  void save(final FlowType flowType);

  boolean delete(final Long id);

  boolean update(final FlowType flowType);

  FlowType getFlowTypeById(final Long id);

  List<FlowType> getAllFlowTypeDetails();
}
