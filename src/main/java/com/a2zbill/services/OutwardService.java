package com.a2zbill.services;

import com.a2zbill.domain.Outward;
import java.util.List;

public interface OutwardService {
  void save(final Outward outward);

  boolean delete(final long id);

  boolean update(final Outward outward);

  List<Outward> getAllOutwardDetails();

  Outward getOutwardDetailByGstNumber(final String gstNumber);

  List<Outward> getOutwardDetailsByMonthAndYear(final String month, final String year);

  List<Outward> getOutwardDetailsByMonthAndYearAndGSTNumber(final String month, final String year);

  List<Outward> getOutwardDetailsByGstNumber(final String gstNumber);

  List<Outward> getOutwardDetailsByCustomerId(final long customerId);
}
