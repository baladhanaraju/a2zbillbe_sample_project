package com.a2zbill.services;

import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.EmployeeCounter;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import java.math.BigDecimal;
import java.util.List;

public interface CounterProductsService {

  void save(final EmployeeCounter employeeCounter);

  List<Long> getAssignedCounters(final long branchId, final long orgId);

  List<Long> getOpenCounters();

  long getEmployeeId(final String principal);

  List<Long> getEmployeeCounters(final String principal);

  List<CartDetail> getCartProductDetails(final String username);

  List<Long> getRemainCounter(final long employeeNumber);

  List<Long> getEmployeeCounterIds(final String userName);

  List<BigDecimal> getCounterTotalBillAmount(final long cartId);

  int deleteEmployeeCounters(final long employeeId);

  void updateEmployeeCounters(final EmployeeCounter counters);

  void saveVendorProducts(final ProductVendor productVendor);

  void saveReOrder(final ReOrder reOrder);

  void updateVendorProducts(final ProductVendor productVendor);

  List<ProductVendor> viewVendorProducts(final long productId);

  List<ReOrder> viewReorderProducts(final long productId);

  List<Object[]> viewReorderDetailsByStatus(final long orgId, final long branchId);

  List<Object[]> viewReorderDetailsByStatus(final long orgId);

  List<ReOrder> getReOrderDetailsByInvoiceId(final long invoiceId);

  List<ReOrder> getReorderDetailsByProductId(final long productId);

  List<ReOrder> getAllReOrderDetailsByInvoiceId(final long invoiceId);

  ReOrder getReOrderDetailsById(final long id);

  List<ReOrder> getAllReorderDetailsByOrgId(final long orgId);

  List<ReOrder> getAllReorderDetailsByOrgIdAndBranchId(final long orgId, final long branchId);

  ReOrder getReorderDetailsByReorderId(final long reOrderId);

  boolean updateReorder(final ReOrder reOrder);

  List<ReOrder> getReorderListByInvoiceIdAndOrganisationId(final long invoiceId, final long orgId);

  List<ReOrder> getReorderListByInvoiceIdAndOrganisationIdAndBranchId(
      final long invoiceId, final long orgId, final long branchId);

  int updateReOrderByProductIdAndReorderId(final long productId, final long reorderId);
}
