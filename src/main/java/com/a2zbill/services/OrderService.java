package com.a2zbill.services;

import com.a2zbill.domain.Order;
import com.a2zbill.domain.OrderStatus;
import java.util.List;

public interface OrderService {
  public void save(final Order order);

  public boolean delete(final long id);

  public boolean update(final Order order);

  List<Order> getAllOrderDetails();

  Order getOrderById(final long id);

  Order getOrderByGuid(final String guid);

  Order getOrdersDetailsByStatus(final String orderStatus, final long counterId);

  OrderStatus getOrderStatusById(final long id);
}
