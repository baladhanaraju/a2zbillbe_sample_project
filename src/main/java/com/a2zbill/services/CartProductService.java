package com.a2zbill.services;

import com.a2zbill.domain.CartDetail;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface CartProductService {
  void save(final CartDetail counter);

  boolean delete(Long id);

  boolean update(CartDetail counter);

  List<CartDetail> getAllCounterDetails();

  List<CartDetail> getCartDetailsByCartId(final long cartId);

  List<CartDetail> getCartDetailsByCartIdAndDeptIdAndCartStatus(
      Long cartId, String cartStatus, Long deptId);

  List<CartDetail> getCartDetailsDeptIdAndCartStatus();

  List<CartDetail> getCartDetailsByCounterIdAndFlowId();

  List<CartDetail> getCartDetailsByFlowId();

  CartDetail getCartDetailsByProductNameAndHsnCode(
      Long counterId, String productName, String hsnCode);

  CartDetail getCartDetailsByCounterIdProductNameAndHsnCode(
      Long counterId, String productName, String hsnCode);

  List<CartDetail> getCartDetailsByCounterIdAndFlowId(Long counterId, Long flowId);

  List<CartDetail> getCartDetailsGroupByProductName(
      Date fromDate, Date toDate, long orgId, long branchId);

  List<CartDetail> getCartDetailsGroupByProductNameifBrnachNull(
      Date fromDate, Date toDate, long orgId);

  List<CartDetail> getproductQuantityByBranchIdAndDate(long orgId, Long branchId, Date date);

  List<CartDetail> getproductQuantityByBranchAndOrgIdAndDate(
      long orgId, long branchId, Date fromDate, Date toDate);

  List<CartDetail> getproductQuantityByOrgIdAndDate(long orgId, Date fromDate, Date toDate);

  List<Object[]> getIngredientWiseProductQuantitySumByOrgIdAndDate(
      long orgId, long branchId, Date date);

  List<BigDecimal> getIngredientWiseProductQuantitySumByOrgIdBranchIdAndDate(
      long orgId, long branchId, Date date);

  List<CartDetail> getCartDetailsByProductName(String productName, long orgId, long branchId);

  CartDetail getCartDetailByCartId(long cartId);

  List<Object[]> getCartDetailsByCartDetailsId(long cartId);
}
