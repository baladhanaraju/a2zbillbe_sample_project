package com.a2zbill.services;

import com.a2zbill.domain.ExchangeHistory;
import java.util.List;

public interface ExchangeHistoryService {
  void save(ExchangeHistory exchangeHistory);

  boolean delete(long id);

  boolean update(ExchangeHistory exchangeHistory);

  List<ExchangeHistory> getAllExchangeHistory();

  ExchangeHistory getExchangeHistoryById(Long id);
}
