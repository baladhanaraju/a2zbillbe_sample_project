package com.a2zbill.services;

import com.a2zbill.domain.Cart;
import java.util.List;

public interface CartService {
  void save(Cart cart);

  boolean delete(Long id);

  boolean update(final Cart cart);

  List<Cart> getAllCartDetails();

  Cart getCartDetailsOnStatus(final String cartStatus, long counterId);

  List<Cart> getCartDetailsByCounterId(long counterId);

  Cart getCartDetailsByCartId(final long cartId);

  List<Cart> getCartDetailsByStatus();

  Cart getCartByGuid(final String guid);

  Cart getCartDetailsByStatus(final String cartStatus, final long customerId, final long orgId);
}
