package com.a2zbill.services;

import com.a2zbill.domain.KotHistory;

public interface KotHistoryService {
  void save(KotHistory kotHistory);

  void update(KotHistory kotHistory);
}
