package com.a2zbill.services;

import com.a2zbill.domain.ReOrderBranch;

public interface ReOrderBranchService {
  void save(ReOrderBranch reOrderBranch);
}
