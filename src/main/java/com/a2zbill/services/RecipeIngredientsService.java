package com.a2zbill.services;

import com.a2zbill.domain.RecipeIngredients;
import java.util.List;

public interface RecipeIngredientsService {

  void save(RecipeIngredients recipeIngredients);

  boolean delete(Long id);

  boolean update(RecipeIngredients recipeIngredients);

  List<RecipeIngredients> getAllRecipeIngredients();

  List<RecipeIngredients> getAllRecipeIngredientsByOrgAndBranch(long orgId, long branchId);

  List<RecipeIngredients> getAllRecipeIngredientsByOrg(long orgId);

  List<RecipeIngredients> getAllRecipeIngredientsByRecipeId(long recipeId);

  List<RecipeIngredients> getAllRecipeIngredientsByProductId(long productId);

  RecipeIngredients getRecipeIngredientsByRecipeIdAndProductId(long recipeId, long productId);
}
