package com.a2zbill.services;

import com.a2zbill.domain.BillingReport;
import java.util.Date;
import java.util.List;

public interface BillingReportService {

  void save(final BillingReport billingreport);

  boolean delete(final long id);

  boolean update(final BillingReport billingreport);

  List<BillingReport> getAllBillingReport();

  BillingReport getBillingReportByEmpid(final long empid, final Date date);
}
