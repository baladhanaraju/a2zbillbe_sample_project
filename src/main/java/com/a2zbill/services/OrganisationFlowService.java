package com.a2zbill.services;

import com.a2zbill.domain.OrganisationFlow;
import java.util.List;

public interface OrganisationFlowService {

  void save(final OrganisationFlow organisationFlow);

  OrganisationFlow getOrganisationFlowById(final Long id);

  List<OrganisationFlow> getOrganisationFlowByOrgId(final Long orgId);
}
