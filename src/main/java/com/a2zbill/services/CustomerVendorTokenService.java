package com.a2zbill.services;

import com.a2zbill.domain.CustomerVendorToken;
import java.util.List;

public interface CustomerVendorTokenService {

  void save(CustomerVendorToken customerVendorToken);

  List<CustomerVendorToken> getAllCustomerVendorToken();

  CustomerVendorToken getTokenAndRootNameBymobileNumber(
      final String mobileNumber, final long orgId);
}
