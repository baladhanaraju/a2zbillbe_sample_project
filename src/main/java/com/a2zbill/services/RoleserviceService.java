package com.a2zbill.services;

import com.tsss.basic.domain.Roles;
import java.util.List;

public interface RoleserviceService {
  void save(Roles role);

  boolean delete(long id);

  boolean update(Roles role);

  List<Roles> getAllRolesServiceDetails();
}
