package com.a2zbill.services;

import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductVariation;
import java.util.List;
import java.util.Map;

public interface ProductService {

  void save(Product products);

  boolean delete(Long id);

  boolean update(Product products);

  Product getProductsById(Long id);

  List<Product> getAllProducts();

  Product getProductsByProductCode(String productCode);

  Product getProductsByOrgId(Long orgId);

  List<Product> getProductsBySizeVariationIdAndProductName(
      long orgId, long sizeId, String productName);

  Product getProductsByProductCodeAndOrgId(String productCode, Long orgId);

  Product getProductsByProductCodeAndOrgIdAndBranchId(
      String productCode, Long orgId, Long branchId);

  List<Product> getAllProducts(long orgId, long branchId);

  List<Product> getAllProductsByOrgId(long orgId);

  List<Product> getAllProductsByHsnCode();

  Product getProductsByProductName(String productName);

  Product getProductsByProductNameAndOrgAndBranch(String productName, long orgId, long branchId);

  Product getProductsByProductNameAndOrgIdAndBranchId(
      String productName, long orgId, long branchId);

  Product getProductsByProductNameAndOrgId(String productName, long orgId);

  List<Product> getAllProductsName();

  long getProductsCount();

  long getProductsCount(Long orgId);

  long getProductsCounByOrgIdAndBranchId(Long brnachId, Long orgId);

  List<Product> getAllProductsByName(String productName);

  List<Product> getProductsByProductCategoryId(long productCategoryId);

  Product getProductsByHsnCodeAndProductCodeAndProductName(
      String hsnCode, String productCode, String productName);

  Product getProductsByProductCodeAndProductNameAndOrgAndBranch(
      String productCode, String productName, long orgId, long branchId);

  Product getProductsByHsnCodeAndProductName(String hsnCode, String productName);

  Product getProductFeaturesById(Long id);

  Product getProductsByHsnCodeAndProductCode(String hsnCode, String productCode);

  Product getProductsByProductIdAndOrgIdAndBranchId(long productId, long orgId, long branchId);

  Product getProductsByProductIdAndOrgId(long productId, long orgId);

  Product getProductsByHsnCodeAndProductNameByBranchIdAndOrgId(
      String hsnCode, String productName, long branchId, long orgId);

  List<Product> getProductsByProductCategoryName(String productCategoryName);

  List<Product> getProductsByProductCategoryNameIfRedeemFlagIsFalse(String productCategoryName);

  Product getProductsByProductNameAndOrgIdAndBranchIdAndSizeId(
      String producName, long orgId, long branchId, Long sizeVariationId);

  List<ProductVariation> getAllProductsAndVariationDetails(long orgId, long branchId);

  List<ProductVariation> getAllProductsAndVariationDetailsByOrg(long orgId);

  List<Object[]> getAllProductByParentId(long orgId, long branchId);

  List<Object[]> getAllProductByOrgAndParentId(long orgId);

  List<Object[]> getProductByParentId(long orgId, long branchId, long parentId);

  List<Object[]> getProductByParentIdAndOrgId(long orgId, long parentId);

  List<Product> getProductsByProductCategoryNameAndOrgId(
      final String productCategoryName, final long orgId);

  List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchId(
      final String productCategoryName, final long orgId, final long branchId);

  List<Product> getProductByorgIdandBranchIdstatus(long orgId, long branchId);

  List<Product> getProductByorgIdandBranchIdstatus(long orgId);

  List<Product> getProductByorgIdandBranchIdstatus(String productCategoryName, long orgId);

  List<Product> getMenuProductDetailsOrgIdandStatus(
      String productCategoryName, long orgId, long branchId);

  List<Product> getMenuProductDetailsOrgAndBranchAndCategory(
      String productCategoryName, String categoryName, long orgId, long branchId);

  List<Product> getMenuProductDetailsOrgAndCategory(
      String productCategoryName, String categoryName, long orgId);

  List<Map<String, Object>> getProductPackagingDetails();

  Map<String, Object> getProductPackagingDetailsByProductId(long productId);

  List<Product> getProductById(long productId);

  List<Product> getProductByorgIdandBranchIdstatusActive(long orgId, long branchId);

  List<Product> getProductByorgIdandstatusActive(long orgId);

  List<Product> getProductsByProductCategoryNameAndOrgIdpagination(
      final String productCategoryName,
      final long orgId,
      final int pageNum,
      final int numOfRecords);

  List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
      final String productCategoryName,
      final long orgId,
      final long branchId,
      final int pageNum,
      final int numOfRecords);

  Long getProductsCountByProductCategoryNameAndOrgId(String productCategoryName, long orgId);

  long getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
      String productCategoryName, long orgId, long branchId);

  List<Map<String, Object>> getProductsByProductCategoryNameMap(final String productCategoryName);

  List<Map<String, Object>> getProductNameAndIdsByProductCategoryId(final long productCategoryId);

  List<Map<String, Object>> getProductsByOrgPath(final String pathUrl);

  List<Map<String, Object>> getProductsByCategoryId(final long categoryId);

  String getproductNamesByProductId(final long productId);

  List<Product> getAllproductsByElastic();
}
