package com.a2zbill.services;

import com.a2zbill.domain.Frequency;
import java.util.List;

public interface FrequencyService {

  void save(Frequency frequency);

  boolean delete(final long id);

  boolean update(final Frequency frequency);

  List<Frequency> getAllFrequencys();

  Frequency getFrequencyById(long id);
}
