package com.a2zbill.services;

import com.a2zbill.domain.KotStatus;
import java.util.List;

public interface KotStatusService {

  void save(final KotStatus kotStatus);

  void update(final KotStatus kotStatus);

  void delete(final Long id);

  List<KotStatus> getKotStatusByCounterNumber(final long counterNumber, final long branchId);

  List<KotStatus> getAllKotStatus();

  KotStatus getKotStatusById(final long id);

  List<KotStatus> getKotStatusByCounterNumber(final long counterNumber);

  KotStatus getKotStatusByBranchId(
      final long branchId, final String productName, final int quantity, final long counterNumber);
}
