package com.a2zbill.services;

import com.a2zbill.domain.CustomerHistory;

public interface CustomerHistoryService {
  void save(CustomerHistory customerHistory);

  boolean delete(long id);

  boolean update(CustomerHistory customerHistory);
}
