package com.a2zbill.services;

import com.a2zbill.domain.ProductRewardPoint;
import java.util.List;

public interface ProductRewardPointsService {
  void save(ProductRewardPoint productRewardPoint);

  boolean delete(long id);

  boolean update(ProductRewardPoint productRewardPoint);

  List<ProductRewardPoint> getAllProductRewardPoints();

  ProductRewardPoint getAllProductRewardPointsByproductId(
      long productId, long orgId, long branchId);

  ProductRewardPoint getAllProductRewardPointsByproductIdOrgId(long productId, long orgId);

  ProductRewardPoint getProductRewardPointsByproductId(long productId);
}
