package com.a2zbill.services;

import com.a2zbill.domain.SecretKey;
import java.util.List;

public interface SecretKeyService {
  void save(SecretKey secret);

  void update(SecretKey secret);

  boolean delete(Long id);

  List<SecretKey> getAllSecret();

  SecretKey getSecretKeyById(long id);

  SecretKey getDetailsBySecretKey(final String secretKey);

  List<SecretKey> getAllSecretByOrgIdBranchId(long orgId, long branchId);

  List<SecretKey> getAllSecretByOrgId(long orgId);
}
