package com.a2zbill.services;

import com.a2zbill.domain.InventoryAudit;
import java.util.List;

public interface InventoryAuditService {
  void save(final InventoryAudit inventoryAudit);

  boolean delete(final InventoryAudit inventoryAudit);

  boolean update(final InventoryAudit inventoryAudit);

  List<InventoryAudit> getAllInventoryAudit();

  List<InventoryAudit> getAllInventoryAuditByOrg(final long orgId);

  List<InventoryAudit> getAllInventoryAuditByOrgBranchId(final long orgId, final long branchId);
}
