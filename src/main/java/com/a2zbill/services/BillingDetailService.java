package com.a2zbill.services;

import com.a2zbill.domain.BillingDetail;
import java.util.List;

public interface BillingDetailService {

  void save(BillingDetail billingDetails);

  boolean delete(long id);

  boolean update(BillingDetail billingDetails);

  List<BillingDetail> getAllBillingDetails();
}
