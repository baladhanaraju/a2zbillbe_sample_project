package com.a2zbill.services;

import com.a2zbill.domain.Counter;
import java.util.List;

public interface CounterService {
  void save(Counter counter);

  boolean delete(long id);

  boolean update(Counter counter);

  List<Counter> getAllCounterDetails();

  List<Long> getAllCounterDetail(long orgId, long branchId);

  List<Counter> getAllCounterDetailsByOrgId(long orgId);

  Counter getCounterDetailsById(final long id);

  List<Counter> getCounterDetailsOnStatus(String counterStatus);

  Counter getCounterDetailOnStatus(String counterStatus);

  List<Counter> getCounterDetailOnStatusByOrgIdandBranchId(
      long orgId, long branchId, String counterStatus);

  List<Counter> getCounterDetailOnStatusByOrgId(long orgId, String counterStatus);

  List<Counter> getAllCounterDetailsByOrgBranchId(long orgId, long branchId);

  List<Counter> getCounterDetailsByOrgBranchIdBySectionId(
      long orgId, long branchId, long sectionId);

  List<Counter> getCounterDetailsByOrgIdBySectionId(long orgId, long sectionId);

  Counter getCounterDetailsBycounterNumber(long orgId, long branchId, long counterNumber);

  List<Counter> getCounterDetailsByOrgIdandStatusActive(long orgId);

  List<Counter> getCounterDetailsByOrgIdandbranchIdStatusActive(long orgId, long branchId);

  public List<Counter> getCounterDetailsByOrgIdBranchIdandStatusInActive(long orgId, long branchId);

  public List<Counter> getCounterDetailsByOrgIdandStatusInActive(long orgId);

  public List<Counter> getCounterDetailsByOrgIdBranchIdandStatusActiveAndInActive(
      long orgId, long branchId);

  public List<Counter> getCounterDetailsByOrgIdandStatusActiveAndInActive(long orgId);

  public List<Counter> getCounterDetilsByOrgIdAndBranchId(long orgId);

  public List<Counter> getCounterDetailsBasedOnOrgIdAndBranchId(long orgId, long branchId);

  List<Counter> getAllCounterDetailByStatus(long orgId, long branchId);

  List<Counter> getCounterDetailsByOrgBranchIdBySectionAndStatus(
      long orgId, long branchId, long sectionId);

  public List<Counter> getCounterDetailsByOrgIdBranchIdSectionIdandOnline(
      long orgId, long branchId, long sectionId);

  public List<Counter> getCounterDetailsByOrgIdSectionIdandOnline(long orgId, long sectionId);

  public List<Counter> getCounterDetailsByOrgIdSectionIdandtrue(long orgId);

  public List<Counter> getCounterDetailsByOrgIdBranchIdSectionIdandtrue(long orgId, long branchId);

  Counter getCounterDetailOnlineStatus(boolean counterStatus);
}
