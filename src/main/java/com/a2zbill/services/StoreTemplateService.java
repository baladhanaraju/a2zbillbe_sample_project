package com.a2zbill.services;

import com.a2zbill.domain.StoreTemplate;
import java.util.List;

public interface StoreTemplateService {
  void save(StoreTemplate storeTemplate);

  boolean delete(Long id);

  boolean update(StoreTemplate storeTemplate);

  StoreTemplate getStoreTemplateById(final Long id);

  List<StoreTemplate> getAllStoreTemplate();

  long getStoreTemplateByorgPathUrl(final String pathUrl);
}
