package com.a2zbill.services;

import com.a2zbill.domain.AlteredBillings;
import java.util.List;

public interface AlteredBillingsService {

  public void save(final AlteredBillings alteredbillings);

  public Boolean update(final AlteredBillings alteredbillings);

  public Boolean delete(final long id);

  List<AlteredBillings> getAllAlteredBillings();

  AlteredBillings getAlteredBillingsbyid(final long id);
}
