package com.a2zbill.services;

import com.a2zbill.domain.Flows;
import java.util.List;

public interface FlowsService {
  void save(final Flows flows);

  Flows getFlowsById(final Long id);

  List<Flows> getFlowsDetails();
}
