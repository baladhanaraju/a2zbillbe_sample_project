package com.a2zbill.services;

import com.a2zbill.domain.Percentage;
import java.util.List;

public interface PercentageService {
  List<Percentage> getAllPercentages();
}
