package com.a2zbill;

import com.a2zbill.services.ProductService;
import com.a2zbill.services.elasticsearch.ProductElasticSearchService;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.model.time.ExecutionTime;
import com.cronutils.parser.CronParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.offers.domain.Campaign;
import com.offers.services.CampaignService;
import com.tsss.basic.config.KafkaSender;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EntityScan({"com.tsss.basic", "com.a2zbill", "com.feedback", "com.tsss.basic.paytm", "com.offers"})
@ComponentScan({
  "com.tsss.basic",
  "com.a2zbill",
  "com.feedback",
  "com.tsss.basic.paytm",
  "com.offers"
})
@EnableJpaRepositories({
  "com.tsss.basic",
  "com.a2zbill",
  "com.feedback",
  "com.tsss.basic.paytm",
  "com.offers"
})
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableCaching
@EnableScheduling
@SpringBootApplication
public class A2ZBillApplication implements CommandLineRunner {

  @PostConstruct
  void started() {
    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Kolkata"));
  }

  @Autowired CampaignService campaignService;

  @Autowired KafkaSender kafkaSender;

  @Autowired private ProductService productService;

  @Autowired private ProductElasticSearchService productElasticSearchService;

  private static final Logger LOGGER = Logger.getLogger(A2ZBillApplication.class.getName());

  public static void main(final String[] args) {

    SpringApplication.run(A2ZBillApplication.class);
  }

  @Value("${properties.file.name}")
  private String propertiesFileName;

  @Override
  public void run(final String... args) throws Exception {
    LOGGER.log(Level.SEVERE, "A2ZBillApplication-propertiesFileName ::  {0}", propertiesFileName);
  }

  @Bean
  public CacheManager createManager() {
    final ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager("greetings");
    return cacheManager;
  }

  @Scheduled(cron = "0 */10  *  ? *  *")
  public void job() {
    try {
      this.getCampaigns();
    } catch (final JsonProcessingException e) {

    }
  }

  //  @Scheduled(cron = "0 */10  *  * *  *")
  //  public void ProductElasticSearchJob() {
  //    try {
  //      final List<Product> productList = productService.getAllProducts();
  //      for (final Product product : productList) {
  //        final Long productId = product.getId();
  //        final Organisation organisation = product.getOrganisation();
  //        final Branch branch = product.getBranch();
  //        if (organisation != null) {
  //          final Long orgId = organisation.getId();
  //          if (branch != null) {
  //            final Long branchId = branch.getId();
  //            final ProductElasticSearch productElasticSearchList =
  //                productElasticSearchService.getProductByProductIdAndBranchIdAndOrgId(
  //                    productId, branchId, orgId);
  //            //            if (productElasticSearchList != null) {
  //            //              continue;
  //            //            }
  //          } else {
  //            final ProductElasticSearch productElasticSearchList =
  //                productElasticSearchService.getProductByProductIdAndOrgId(productId, orgId);
  //            //            if (productElasticSearchList != null) {
  //            //              continue;
  //            //            }
  //          }
  //        }
  //
  //        final ProductElasticSearch storeTypeSearch = new ProductElasticSearch();
  //        storeTypeSearch.setId(product.getId());
  //        storeTypeSearch.setCGST(product.getCGST());
  //        storeTypeSearch.setCode(product.getCode());
  //        storeTypeSearch.setHsnNumber(product.getHsnNumber());
  //        storeTypeSearch.setId(product.getId());
  //        storeTypeSearch.setIGST(product.getIGST());
  //        storeTypeSearch.setImage(product.getImage());
  //        storeTypeSearch.setIncludingTaxFlag(product.getIncludingTaxFlag());
  //        storeTypeSearch.setMeasurement(product.getMeasurement());
  //        storeTypeSearch.setMeasurement1(product.getMeasurement1());
  //        storeTypeSearch.setMeasurement2(product.getMeasurement2());
  //        storeTypeSearch.setParentId(product.getParentId());
  //        storeTypeSearch.setProductCount(product.getProductCount());
  //        storeTypeSearch.setProductName(product.getProductName());
  //        storeTypeSearch.setProductPrice(product.getProductPrice());
  //        storeTypeSearch.setSGST(product.getSGST());
  //        if (organisation != null) {
  //          storeTypeSearch.setOrganisationId(organisation.getId());
  //        }
  //        if (branch != null) {
  //          storeTypeSearch.setBranchId(branch.getId());
  //        }
  //        productElasticSearchService.insertProduct(storeTypeSearch);
  //      }
  //
  //    } catch (final Exception ex) {
  //      LOGGER.log(Level.SEVERE, "product Elastic Search save error ::: " + ex);
  //    }
  //  }

  public final List<Campaign> getCampaigns() throws JsonProcessingException {

    final CronDefinition cronDefinition =
        CronDefinitionBuilder.instanceDefinitionFor(CronType.QUARTZ);
    final CronParser parser = new CronParser(cronDefinition);

    final List<Long> campaignList = new ArrayList<>();
    final ZonedDateTime now = ZonedDateTime.now();

    final List<Campaign> activeCampaigns = campaignService.getActiveCampaigns();
    final ObjectMapper objMapper = new ObjectMapper();
    final Map<String, List<Long>> producerMap = new HashMap<>();

    for (final Campaign c : activeCampaigns) {

      final ExecutionTime executionTime = ExecutionTime.forCron(parser.parse(c.getSchedule()));

      final Optional<ZonedDateTime> nextExecution = executionTime.nextExecution(now);
      if (nextExecution.isPresent()) {
        final ZonedDateTime zonedDateTime = nextExecution.get();
        final long nowMinute = now.getMinute();
        final long nextMinute = zonedDateTime.getMinute();
        if ((nextMinute - nowMinute) < 10
            && now.getYear() == zonedDateTime.getYear()
            && now.getMonth() == zonedDateTime.getMonth()
            && now.getDayOfMonth() == zonedDateTime.getDayOfMonth()) {
          campaignList.add(c.getCampaignId());
          final Campaign campaign =
              campaignService.getCampaignDetailsByCampaignId(c.getCampaignId());
          campaign.setCurrentCount((campaign.getCurrentCount()) + 1);
          campaign.setLastRunTime((System.currentTimeMillis()));
          campaignService.update(campaign);
        }
      }
    }

    if (!campaignList.isEmpty()) {
      producerMap.put("campaigns", campaignList);
      final String kafkadata = objMapper.writeValueAsString(producerMap);
      kafkaSender.sendCampaign(kafkadata);
    }
    return activeCampaigns;
  }
}
