package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.KotStatus;
import com.a2zbill.services.KotStatusService;
import com.tsss.basic.security.jwt.Identifier;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KotStatusController {

  private static final Logger LOGGER = LoggerFactory.getLogger(KotStatusController.class);

  @Autowired private KotStatusService kotStatusService;

  @PostMapping(path = RouteConstants.KOT_STATUS_SAVE)
  public final KotStatus addKotStatus(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {

    final KotStatus kotStatus = new KotStatus();
    final JSONObject kotStatusDetails = new JSONObject(payload);
    try {
      kotStatus.setProductName(kotStatusDetails.getString("productName"));
      kotStatus.setQuantity(kotStatusDetails.getInt("quantity"));
      kotStatus.setCounterNumber(kotStatusDetails.getLong("counterNumber"));
      kotStatus.setBranchId(kotStatusDetails.getLong("branchId"));

      this.kotStatusService.save(kotStatus);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "KotStatusController in the addKotStatus() of" + ex);
    }
    return kotStatus;
  }

  @PostMapping(path = RouteConstants.KOT_STATUS_UPDATE)
  public final KotStatus updateKotStatus(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {
    KotStatus kotStatus = null;
    final JSONObject kotStatusDetails = new JSONObject(payload);
    try {
      kotStatus = this.kotStatusService.getKotStatusById(kotStatusDetails.getLong("id"));
      kotStatus.setProductName(kotStatusDetails.getString("productName"));
      kotStatus.setQuantity(kotStatusDetails.getInt("quantity"));
      kotStatus.setCounterNumber(kotStatusDetails.getLong("counterNumber"));
      kotStatus.setBranchId(kotStatusDetails.getLong("branchId"));

      this.kotStatusService.update(kotStatus);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "KotStatusController in the updateKotStatus() of" + ex);
    }
    return kotStatus;
  }

  @GetMapping(path = RouteConstants.GET_ALL_KOT_STATUS_DETAILS)
  public final List<String> getKotStatus() {

    final List<String> kotstatusDetails = new ArrayList<>();
    try {
      final List<KotStatus> kotStatus = this.kotStatusService.getAllKotStatus();
      for (KotStatus eachKotStatus : kotStatus) {
        String message =
            eachKotStatus.getBranchId()
                + "|"
                + eachKotStatus.getProductName()
                + "|"
                + eachKotStatus.getQuantity()
                + "|"
                + eachKotStatus.getCounterNumber()
                + "|"
                + eachKotStatus.getEmpId()
                + "|"
                + eachKotStatus.getProductImage()
                + "|"
                + eachKotStatus.getId();

        kotstatusDetails.add(message);
      }
    } catch (final Exception ex) {
      LOGGER.error("getKotStatus " + ex, ex);
    }
    return kotstatusDetails;
  }

  @GetMapping(path = RouteConstants.ALL_KOT_STATUS_DETAILS_BY_COUNTERNUMBER)
  public final ResponseEntity<List<String>> getKotStatusByCounterNumber(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<String> kotstatusDetails = new ArrayList<>();
    final long counterNumber = Long.parseLong(request.getParameter("counterNumber"));
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long branchId = identifier.getBranchId();
        final List<KotStatus> kotStatus =
            this.kotStatusService.getKotStatusByCounterNumber(counterNumber, branchId);
        for (KotStatus eachKotStatus : kotStatus) {
          String message =
              eachKotStatus.getBranchId()
                  + "|"
                  + eachKotStatus.getProductName()
                  + "|"
                  + eachKotStatus.getQuantity()
                  + "|"
                  + eachKotStatus.getCounterNumber()
                  + "|"
                  + eachKotStatus.getEmpId()
                  + "|"
                  + eachKotStatus.getProductImage()
                  + "|"
                  + eachKotStatus.getId();

          kotstatusDetails.add(message);
        }
      } else {
        return new ResponseEntity<>(kotstatusDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error("getKotStatus " + ex, ex);
    }
    return new ResponseEntity<>(kotstatusDetails, HttpStatus.OK);
  }
}
