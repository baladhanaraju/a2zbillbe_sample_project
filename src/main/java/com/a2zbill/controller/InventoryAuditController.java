package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.InventoryAudit;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.Product;
import com.a2zbill.services.InventoryAuditService;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.ProductService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryAuditController {

  private static final Logger LOGGER = LoggerFactory.getLogger(InventoryAuditController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private InventoryDetailService inventoryService;

  @Autowired private ProductService productsService;

  @Autowired private InventoryAuditService inventoryAuditService;

  @GetMapping(path = RouteConstants.GET_ALL_INVENTORY_AUDIT_DETAILS)
  public final ResponseEntity<List<InventoryAudit>> getInventoryAuditDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<InventoryAudit> inventoryAudit = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        if (branchId != 0) {
          inventoryAudit =
              this.inventoryAuditService.getAllInventoryAuditByOrgBranchId(orgId, branchId);
        } else {
          inventoryAudit = this.inventoryAuditService.getAllInventoryAuditByOrg(orgId);
        }
      } else {
        return new ResponseEntity<>(inventoryAudit, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found" + ne, ne);
    }
    return new ResponseEntity<>(inventoryAudit, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.SAVE_INVENTORY_AUDIT_DETAILS)
  public final Map<String, String> getsaveInventoryAuditDetails(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws JSONException {

    final Map<String, String> map = new HashMap<>();
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    try {
      Branch branch = employeeDetails.getBranch();
      final JSONArray jsonArray = new JSONArray(payload);
      for (int i = 0; i < jsonArray.length(); i++) {
        final JSONObject jsonObject = (JSONObject) jsonArray.get(i);

        final BigDecimal difference = BigDecimal.valueOf(jsonObject.getDouble("Difference"));
        final long inventoryId = jsonObject.getLong("InventoryId");

        InventoryAudit inventoryAudit = new InventoryAudit();
        inventoryAudit.setOrganisation(employeeDetails.getOrganisation());
        inventoryAudit.setBranch(employeeDetails.getBranch());
        inventoryAudit.setDate(new Date());
        inventoryAudit.setDifference(difference);
        inventoryAudit.setInventoryQuantity(
            BigDecimal.valueOf(jsonObject.getDouble("StockQuantity")));
        inventoryAudit.setPhysicalQuantity(
            BigDecimal.valueOf(jsonObject.getDouble("PhysicalQuantity")));
        final Long productId = jsonObject.getLong("ProductId");
        if (branch == null) {

          Product product = this.productsService.getProductsByProductIdAndOrgId(productId, orgId);
          inventoryAudit.setProduct(product);
          inventoryAudit.setProductName(product.getProductName());
          InventoryDetail inventorDetails =
              this.inventoryService.getInventoryDetailByIdOrgId(inventoryId, orgId);
          inventoryAudit.setProduct(inventorDetails.getProduct());
          inventoryAudit.setProductName(inventorDetails.getProduct().getProductName());
        } else {
          long branchId = branch.getId();
          Product product =
              this.productsService.getProductsByProductIdAndOrgIdAndBranchId(
                  productId, orgId, branchId);

          inventoryAudit.setProduct(product);
          inventoryAudit.setProductName(product.getProductName());
          InventoryDetail inventorDetails =
              this.inventoryService.getInventoryDetailByIdOrgIdBranch(inventoryId, orgId, branchId);

          inventoryAudit.setProduct(inventorDetails.getProduct());
          inventoryAudit.setProductName(inventorDetails.getProduct().getProductName());
        }
        try {
          if (difference.doubleValue() != 0) {
            inventoryAudit.setReason(jsonObject.getString("Reason"));
          } else {
            inventoryAudit.setReason("");
          }
        } catch (final JSONException je) {
          inventoryAudit.setReason("");
        }
        inventoryAudit.setReason(jsonObject.getString("Reason"));
        this.inventoryAuditService.save(inventoryAudit);
        map.put("invAudit", "Invnetory Audit Saved Successfully");
      }
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "/admin/save/inventoryaudit/details" + e);
    }
    return map;
  }
}
