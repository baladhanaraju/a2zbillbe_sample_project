package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.Section;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.SectionService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CounterController {

  private static final Logger LOGGER = LoggerFactory.getLogger(CounterController.class);

  private static final String COUNTER_STATUSOPEN = "Open";
  private static final String COUNTER_STATUSCLOSED = "Closed";
  private static final boolean STATUS_TRUE = true;
  private static final boolean STATUS_FALSE = false;

  @Autowired private CounterService counterService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private BranchService branchService;

  @Autowired private SectionService sectionService;

  @PostMapping(path = RouteConstants.SAVE_COUNTER_DETAILS)
  public final Map<String, Object> saveCounterDetails(
      @RequestBody final String payload, final HttpServletRequest request) {
    final Map<String, Object> map = new HashMap<>();

    try {
      final JSONObject jsonObject = new JSONObject(payload);
      final long counterNumber = jsonObject.getLong("counterNumber");
      final String displayName = jsonObject.getString("displayname");
      final long maxCapacity = jsonObject.getLong("maxcapacity");
      final long sectionId = jsonObject.getLong("sectionid");
      final long branchId = jsonObject.getLong("branchId");
      final Counter counterDetails = new Counter();
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employee = this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long orgId = employee.getOrganisation().getId();

      final Counter existCounterDetails =
          this.counterService.getCounterDetailsBycounterNumber(orgId, branchId, counterNumber);
      if (existCounterDetails != null) {
        map.put("Counter", "Counter Number Already Existed");
        return map;
      } else {
        final List<Counter> listOfcounters =
            this.counterService.getAllCounterDetailsByOrgBranchId(orgId, branchId);
        for (final Counter counter : listOfcounters) {
          if (counter.getDisplayName().equalsIgnoreCase(displayName)) {
            map.put("Counter", "Display Name Already Existed");
            return map;
          }
        }
        counterDetails.setCounterNumber(counterNumber);
        counterDetails.setCounterStatus(COUNTER_STATUSCLOSED);
        counterDetails.setMaxCapacity(maxCapacity);
        try {
          final Branch branch = this.branchService.getBranchById(branchId);
          counterDetails.setBranchId(branch);
        } catch (final NullPointerException ne) {
          counterDetails.setBranchId(null);
        }
        counterDetails.setOrgId(employee.getOrganisation());
        final Section section = this.sectionService.getSectionDetailsBySectionId(sectionId);
        counterDetails.setSection(section);
        counterDetails.setStatus(STATUS_TRUE);
        if (displayName.equals("")) {
          map.put("Counter", "Display name is mandatory");
        } else {
          counterDetails.setDisplayName(displayName);
          this.counterService.save(counterDetails);
          map.put("Counter", "Counter Added Successfully");
        }

        return map;
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "api/save/counter/details " + ex);
    }
    return map;
  }

  @PostMapping(path = RouteConstants.UPDATE_COUNTER_DETAILS)
  public final Map<String, Object> updateCounterDetail(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload) {
    final Map<String, Object> map = new HashMap<>();

    try {
      final JSONObject counter = new JSONObject(payload);
      Counter counterDetails = null;

      final Long id = counter.getLong("id");
      final String key = counter.getString("key");
      final String value = counter.getString("value");

      counterDetails = counterService.getCounterDetailsById(id);
      if (key.equals("displayName")) {
        if (value.equals("")) {
          map.put("counterDetails", "Display name is mandatory");
        } else {
          counterDetails.setDisplayName(value);
          counterService.update(counterDetails);
          map.put("counterDetails", "Counter Details updated successfully");
        }
      } else if (key.equals("maxCapacity")) {
        counterDetails.setMaxCapacity(Long.parseLong(value));
        counterService.update(counterDetails);
        map.put("counterDetails", "Counter Details updated successfully");
      } else if (key.equals("counterNumber")) {
        counterDetails.setCounterNumber(Long.parseLong(value));
        counterService.update(counterDetails);
        map.put("counterDetails", "Counter Details updated counterNumber");
      } else {
        map.put("counterDetails", "key Name does not match");
      }

    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "updateCounterDetail " + e);
      map.put("counterDetails", "Counter Details not updated");
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_DETAILS)
  public final ResponseEntity<Object> getCounterDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counterDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        counterDetails = this.counterService.getAllCounterDetailByStatus(orgId, branchId);
      } else {
        return new ResponseEntity<>(counterDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception re) {
      LOGGER.error(Constants.MARKER, "getBillDetails" + re);
    }
    return new ResponseEntity<>(counterDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_STATUS_OPEN)
  public final ResponseEntity<Object> getCounterstatusopen(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counter = new ArrayList<Counter>();
    try {
      counter = this.counterService.getCounterDetailsOnStatus(COUNTER_STATUSOPEN);
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "CounterDetails" + ex);
    }
    return new ResponseEntity<>(counter, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_STATUS_CLOSED)
  public final List<Counter> getCounterstatusclose(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counter = null;
    try {
      counter = this.counterService.getCounterDetailsOnStatus(COUNTER_STATUSCLOSED);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "CounterDetails" + ex);
    }
    return counter;
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_STATUS_LIST)
  public final ResponseEntity<Object> getAllCounters(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counter = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          counter = this.counterService.getCounterDetilsByOrgIdAndBranchId(orgId);
        } else {
          counter = this.counterService.getCounterDetailsBasedOnOrgIdAndBranchId(orgId, branchId);
        }
      } else {
        return new ResponseEntity<>(counter, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getCounterDetails" + ex);
    }
    return new ResponseEntity<>(counter, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_STATUS_CLOSED_ORGID_AND_BRANCHID)
  public final ResponseEntity<Object> getOrgBranchCounterstatusclose(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counter = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0) {
          counter =
              this.counterService.getCounterDetailOnStatusByOrgIdandBranchId(
                  orgId, branchId, COUNTER_STATUSOPEN); // counter
        } else {
          counter = this.counterService.getCounterDetailOnStatusByOrgId(orgId, COUNTER_STATUSOPEN);
        }
        this.counterService.getCounterDetailsOnStatus(COUNTER_STATUSCLOSED);
      } else {
        return new ResponseEntity<>(counter, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "CounterDetails" + ex);
    }
    return new ResponseEntity<>(counter, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_STATUS_OPEN_ORGID_AND_BRANCHID)
  public final ResponseEntity<Object> getOrgBrnachCounterstatusopen(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counter = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0) {
          counter =
              this.counterService.getCounterDetailOnStatusByOrgIdandBranchId(
                  orgId, branchId, COUNTER_STATUSOPEN);
        } else {
          counter = this.counterService.getCounterDetailOnStatusByOrgId(orgId, COUNTER_STATUSOPEN);
        }
      } else {
        return new ResponseEntity<>(counter, HttpStatus.UNAUTHORIZED);
      }

    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "CounterDetails" + ex);
    }
    return new ResponseEntity<>(counter, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_STATUS_DETAILS_ORGID_AND_BRANCHID)
  public final ResponseEntity<Object> getOrgBranchCounterDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counterDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0)
          counterDetails = this.counterService.getAllCounterDetailsByOrgBranchId(orgId, branchId);
        else counterDetails = this.counterService.getAllCounterDetailsByOrgId(orgId);
      } else {
        return new ResponseEntity<>(counterDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getBillDetails" + ex);
    }
    return new ResponseEntity<>(counterDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_LIST_ACTIVE)
  public final ResponseEntity<Object> getAllCountersActive(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counterDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0)
          counterDetails = counterService.getCounterDetailsByOrgIdandStatusActive(orgId);
        else
          counterDetails =
              counterService.getCounterDetailsByOrgIdandbranchIdStatusActive(orgId, branchId);
      } else {
        return new ResponseEntity<>(counterDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getCounterDetails " + e);
    }
    return new ResponseEntity<>(counterDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_LIST_INACTIVE)
  public final ResponseEntity<Object> getCounterDetailsInActive(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counterDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0)
          counterDetails = counterService.getCounterDetailsByOrgIdandStatusInActive(orgId);
        else
          counterDetails =
              counterService.getCounterDetailsByOrgIdBranchIdandStatusInActive(orgId, branchId);
      } else {
        return new ResponseEntity<>(counterDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getCounterDetails " + e);
    }
    return new ResponseEntity<>(counterDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_LIST_SORTING)
  public final ResponseEntity<Object> getAllcounterSorting(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counterDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0)
          counterDetails = counterService.getCounterDetailsByOrgIdandStatusActiveAndInActive(orgId);
        else
          counterDetails =
              counterService.getCounterDetailsByOrgIdBranchIdandStatusActiveAndInActive(
                  orgId, branchId);
      } else {
        return new ResponseEntity<>(counterDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getCounterDetails " + e);
    }
    return new ResponseEntity<>(counterDetails, HttpStatus.OK);
  }

  @DeleteMapping(path = RouteConstants.GET_COUNTER_DELETE)
  public final void deleteCounterDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final long counterId = Long.parseLong(request.getParameter("id"));
    try {
      this.counterService.delete(counterId);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "deleteCounterDetails" + ex);
    }
  }

  @PostMapping(path = RouteConstants.UPDATE_COUNTER_STATUS)
  public final Map<String, String> updateCounterstatus(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String counterDetails) {
    final Map<String, String> counterUpdate = new HashMap<>();
    try {
      final JSONObject counter = new JSONObject(counterDetails);
      final long id = counter.getLong("id");
      final String status = counter.getString("counterStatus");
      final Counter counters = this.counterService.getCounterDetailsById(id);
      if (status.equalsIgnoreCase("true")) {
        counters.setStatus(STATUS_FALSE);
        counterService.update(counters);
      } else {
        counters.setStatus(STATUS_TRUE);
        counterService.update(counters);
      }
      counterUpdate.put(
          "counterUpdate", "counterDetails Updated For   " + counters.getCounterNumber());
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "counterProductsDetails" + ex, ex);
    }
    return counterUpdate;
  }

  @GetMapping(path = RouteConstants.GET_COUNTER_LIST_TRUE)
  public final ResponseEntity<Object> getCounterDetailsByOrgIdSectionIdandtrue(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Counter> counterDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0)
          counterDetails = counterService.getCounterDetailsByOrgIdSectionIdandtrue(orgId);
        else
          counterDetails =
              counterService.getCounterDetailsByOrgIdBranchIdSectionIdandtrue(orgId, branchId);
      } else {
        return new ResponseEntity<>(counterDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getCounterDetailsByOrgIdSectionIdandtrue " + e);
    }
    return new ResponseEntity<>(counterDetails, HttpStatus.OK);
  }
}
