package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.LooseProduct;
import com.a2zbill.domain.Product;
import com.a2zbill.services.LooseProductService;
import com.a2zbill.services.impl.ProductServiceImpl;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LooseProductController {

  private static final Logger LOGGER = LoggerFactory.getLogger(LooseProductController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private ProductServiceImpl productsService;

  @Autowired private LooseProductService looseProductService;

  @PostMapping(path = RouteConstants.LOOSE_PRODUCT_DETAILS_SAVE)
  public final ResponseEntity<Object> savelooseProductDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetail) {
    final Map<String, Object> map = new HashMap<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final JSONObject product = new JSONObject(productDetail);
      final LooseProduct looseProduct = new LooseProduct();
      final String productCode = product.getString("code");
      final double weight = Double.parseDouble(product.getString("quantity"));
      final Product productDetails = this.productsService.getProductsByProductCode(productCode);
      if (productDetails != null) {
        looseProduct.setProductName(productDetails.getProductName());
        final BigDecimal big = BigDecimal.valueOf(weight);
        looseProduct.setQuantity(big.doubleValue());
        final BigDecimal price =
            productDetails.getProductPrice().multiply(BigDecimal.valueOf(weight));
        looseProduct.setPrice(price);
        looseProduct.setHsnCode(productDetails.getHsnNumber());
        looseProduct.setBranch(employeeDetails.getBranch());
        looseProduct.setOrganisation(employeeDetails.getOrganisation());
        looseProduct.setProduct(productDetails);
        this.looseProductService.save(looseProduct);
        map.put("message1", "Loose Product Details saved successfully");
        map.put("flag", "true");
      } else {
        map.put("message2", "Please Enter valid Product Code");
        map.put("flag", "false");
        return new ResponseEntity<Object>(map, HttpStatus.OK);
      }
      final LooseProduct looseProducts =
          this.looseProductService.getLooseProductDetailsById(looseProduct.getId());
      final String barcode = String.valueOf(looseProduct.getId());
      looseProducts.setBarCode(barcode);
      this.looseProductService.update(looseProducts);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "savelooseProductDetails" + ex);
    }
    return new ResponseEntity<Object>(map, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.LOOSE_PRODUCT_SAVE)
  public final Map<String, Object> addLooseProductDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload) {
    final Map<String, Object> map = new HashMap<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long orgId = employeeDetails.getOrganisation().getId();
      final JSONArray jsonArray = new JSONArray(payload);
      Product productDetail = null;
      for (int i = 0; i < jsonArray.length(); i++) {
        final LooseProduct looseProduct = new LooseProduct();
        final JSONObject looseProductlData = ((JSONObject) jsonArray.get(i));
        final String productCode = looseProductlData.getString("productcode");
        final double quantity = Double.parseDouble(looseProductlData.getString("quantity"));
        final String productName = looseProductlData.getString("productName");
        final double productPrice = looseProductlData.getDouble("productprice");
        if (employeeDetails.getBranch() != null) {
          productDetail =
              this.productsService.getProductsByProductNameAndOrgAndBranch(
                  productName, orgId, employeeDetails.getBranch().getId());
        } else {
          productDetail = this.productsService.getProductsByProductNameAndOrgId(productName, orgId);
        }
        looseProduct.setProductName(productName);
        looseProduct.setQuantity(quantity);
        looseProduct.setPrice(BigDecimal.valueOf(productPrice));
        looseProduct.setHsnCode(looseProductlData.getString("hsncode"));
        looseProduct.setBarCode(productCode);
        looseProduct.setProduct(productDetail);
        looseProduct.setBranch(employeeDetails.getBranch());
        looseProduct.setOrganisation(employeeDetails.getOrganisation());
        this.looseProductService.save(looseProduct);
        map.put("message", "Loose Product Details saved successfully");
      }
    } catch (final Exception re) {
      LOGGER.error("addLooseProductDetails" + re, re);
      map.put("message", "Invalid ProductName");
    }
    return map;
  }
}
