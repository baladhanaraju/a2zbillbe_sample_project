package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingPayments;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.Item;
import com.a2zbill.domain.Order;
import com.a2zbill.domain.OrderHistory;
import com.a2zbill.domain.OrderLineItems;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.Review;
import com.a2zbill.domain.Subscription;
import com.a2zbill.services.BillingPaymentsService;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CartProductService;
import com.a2zbill.services.CartService;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.OrderHistoryService;
import com.a2zbill.services.OrderLineItemsService;
import com.a2zbill.services.OrderService;
import com.a2zbill.services.ReviewService;
import com.a2zbill.services.impl.BillingNumDataServiceImpl;
import com.a2zbill.services.impl.ItemServiceImpl;
import com.a2zbill.services.impl.ProductServiceImpl;
import com.offers.domain.CustomerTransactionSummary;
import com.offers.domain.Reward;
import com.offers.domain.RewardHistory;
import com.offers.services.CustomerTransactionSummaryService;
import com.offers.services.RewardHistoryService;
import com.offers.services.RewardService;
import com.tsss.basic.domain.AuthProvider;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BasicTemplateServiceException;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestaurentController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RestaurentController.class);

  @Value("${jwt.header}")
  private String tokenHeader;

  @Autowired private CartProductService cartProductService;

  @Autowired private BillingPaymentsService billingPaymentsService;

  @Autowired private ProductServiceImpl productsService;

  @Autowired private CounterService counterService;

  @Autowired private BillingService billingService;

  @Autowired private CartService cartService;

  @Autowired private ItemServiceImpl itemsService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private CustomerService customerService;

  @Autowired private CustomerTransactionSummaryService customerTransactionSummaryService;

  @Autowired private RewardService rewardService;

  @Autowired private ReviewService reviewService;

  @Autowired private InventoryDetailService inventoryDetailService;

  @Autowired private BillingNumDataServiceImpl billingNumDataServiceImpl;

  @Autowired private OrderService orderService;

  @Autowired private OrderLineItemsService orderLineItemsService;

  @Autowired private RewardHistoryService rewardHistoryService;

  @Autowired private OrderHistoryService orderHistoryService;

  @GetMapping(path = RouteConstants.GET_RESTAURANT_COUNTER_STATUS)
  public final Cart getRestuarentCounterstatus(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final Cart cartDetails = new Cart();

    try {

      final long counterNumber = Long.parseLong(request.getParameter(Constants.COUNTERNUMBER));
      final Counter counterDetails = counterService.getCounterDetailsById(counterNumber);
      if (counterDetails != null) {
        counterDetails.setCounterStatus(Constants.CARTSTATUS);
        counterService.update(counterDetails);
      } else {
        cartDetails.setCartStatus(Constants.CARTSTATUS);
        final Date date = new Date();
        cartDetails.setStartTime(date);
        cartDetails.setEndTime(date);
        cartDetails.setCounterDetails(counterDetails);
        cartService.save(cartDetails);
        return cartDetails;
      }
    } catch (final Exception e) {
      LOGGER.info("RestuarentCounterDetails" + e, e);
    }
    return null;
  }

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.GET_POST_CART)
  public final void getPostCart(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws Exception {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();

    final Review review = new Review();
    Cart cart = new Cart();
    Order order = new Order();

    final JSONArray jsonArray = new JSONArray(payload);
    for (int i = 0; i < jsonArray.length(); i++) {
      final JSONObject cartData = ((JSONObject) jsonArray.get(i));

      final Counter counterDetails =
          counterService.getCounterDetailsBycounterNumber(
              orgId, branchId, cartData.getLong("counter"));
      final String guid = cartData.getString("guid");
      final String parentGuid = cartData.getString(Constants.PARENTGUID);
      final long maxCapacity = cartData.getLong("maxCapacity");
      final long occupiedCapacity = cartData.getLong("occupiedCapacity");
      final long createdTime = Long.parseLong(cartData.getString("createdTime"));
      try {
        final JSONObject billingData = cartData.getJSONObject("billing");
        final JSONObject discountDetails = cartData.getJSONObject("discountdetails");
        final JSONArray ja = new JSONArray(cartData.getString("products"));
        final String customermobileNumber = billingData.getString("phoneno");
        if (cartData.getLong("orgId") == orgId) {

          cart = cartService.getCartByGuid(guid);
          order = orderService.getOrderByGuid(guid);

          if (cart == null && order == null) {
            cart = new Cart();
            cart.setCartStatus(Constants.CARTSTATUS);
            final Date date = new Date(createdTime);
            cart.setStartTime(date);
            cart.setEndTime(date);
            cart.setCounterDetails(counterDetails);
            cart.setParentguId(parentGuid);
            cart.setGuid(guid);
            cart.setMaxCapacity(maxCapacity);
            cart.setOccupiedCapacity(occupiedCapacity);

            cart.setDescription("offline cart");
            cartService.save(cart);

            order = new Order();
            // order.setOrderStatus(Constants.ORDER_STATUS);
            final Date date1 = new Date(createdTime);
            order.setCreatedTs(date1);
            order.setModifiedTs(date1);
            order.setCounterDetails(counterDetails);
            order.setGuid(guid);
            order.setBranch(employeeDetails.getBranch());
            order.setOrganisation(employeeDetails.getOrganisation());
            final Map<String, Object> orderLocationAddressMap = new HashMap<>();

            if (employeeDetails.getBranch() == null) {
              orderLocationAddressMap.put(
                  Constants.ORDERLOCATIONADDRESS, employeeDetails.getOrganisation().getAddress());
              order.setOrderlocationAddress(orderLocationAddressMap);
            } else {
              orderLocationAddressMap.put(
                  "orderLocationAddress", employeeDetails.getBranch().getAddress());
              order.setOrderlocationAddress(orderLocationAddressMap);
            }

            final Customer customer =
                customerService.getCustomerByMobileNumberAndByOrgIdBranchId(
                    customermobileNumber, orgId, branchId);
            order.setCustomer(customer);
            if (customer != null) {
              order.setCustomer(customer);
              final Map<String, Object> deliveryAddressMap = new HashMap<>();
              deliveryAddressMap.put(Constants.DELIVERYADDRESS, customer.getAddress());
              order.setDeliveryAddress(deliveryAddressMap);
            } else {
              order.setCustomer(null);
              order.setDeliveryAddress(null);
            }
            order.setOrderdate(new Date());
            order.setEmployeeId(employeeDetails);
            final BigDecimal earningAmount = new BigDecimal(billingData.getString("total"));
            order.setOrderearningAmount(earningAmount);
            orderService.save(order);

            OrderHistory orderHistory = new OrderHistory();
            orderHistory.setDeliveryStatusTime(date);
            orderHistory.setBranch(employeeDetails.getBranch());
            orderHistory.setOrganisation(employeeDetails.getOrganisation());
            orderHistory.setCreatedTs(date);
            orderHistory.setModifiedTs(date);
            orderHistory.setDbcreatedTs(date);
            orderHistory.setDbmodifiedTs(date);
            orderHistory.setOrder(order);
            // orderHistory.setReason(reason);
            // orderHistory.setOrderNumber(orderNumber);
            orderHistoryService.save(orderHistory);
            Order orderObject = new Order();

            Cart cartObject = new Cart();
            for (int j = 0; j < ja.length(); ++j) {

              final JSONObject products = ja.getJSONObject(j);

              final long id = cart.getCartId();

              final long orderId = order.getId();

              orderObject = orderService.getOrderById(orderId);
              final OrderLineItems orderLineItems = new OrderLineItems();
              orderLineItems.setOrderId(orderObject);
              orderLineItems.setCounterDetails(orderObject.getCounterDetails());
              orderLineItems.setDiscountAmount(new BigDecimal(0));
              orderLineItems.setHsnCode(products.getString(Constants.HSNNUMBER));
              orderLineItems.setCgst(new BigDecimal(products.getInt("CGST")));
              orderLineItems.setSgst(new BigDecimal(products.getInt("SGST")));

              orderLineItems.setProductName(products.getString(Constants.BILLINGPRODUCTNAME));
              orderLineItems.setQuantity(Long.parseLong(products.getString(Constants.QUANTITY)));
              orderLineItems.setInstructions(products.getString("instructions"));
              orderLineItems.setProductPrice(
                  new BigDecimal(products.getInt(Constants.PRODUCTPRICE)));

              final BigDecimal orderTotalAmount =
                  new BigDecimal(products.getInt("ProductPrice"))
                      .multiply(new BigDecimal(products.getString("Quantity")));

              final BigDecimal orderTaxableAmount =
                  new BigDecimal(products.getInt("CGST"))
                      .add(new BigDecimal(products.getInt("SGST")))
                      .multiply(new BigDecimal(products.getInt("ProductPrice")))
                      .divide(Constants.HUNDRED);

              orderLineItems.setTotalAmount(orderTotalAmount);

              orderLineItems.setTaxableAmount(
                  orderTaxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
              final Date orderTime = new Date(createdTime);

              orderLineItems.setTime(orderTime);

              orderLineItems.setBranchId(employeeDetails.getBranch());

              orderLineItems.setOrganisation(employeeDetails.getOrganisation());
              orderLineItemsService.save(orderLineItems);
              cartObject = cartService.getCartDetailsByCartId(id);
              final CartDetail cartProductDetails = new CartDetail();
              cartProductDetails.setCartDetails(cartObject);
              cartProductDetails.setCounterDetails(cartObject.getCounterDetails());
              cartProductDetails.setDiscountAmount(new BigDecimal(0));
              cartProductDetails.setHsnCode(products.getString("HsnNumber"));
              cartProductDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
              cartProductDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));

              cartProductDetails.setProductName(products.getString("ProductName"));
              cartProductDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
              cartProductDetails.setInstructions(products.getString("instructions"));
              cartProductDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));

              final BigDecimal totalAmount =
                  new BigDecimal(products.getInt("ProductPrice"))
                      .multiply(new BigDecimal(products.getString("Quantity")));

              final BigDecimal taxableAmount =
                  new BigDecimal(products.getInt("CGST"))
                      .add(new BigDecimal(products.getInt("SGST")))
                      .multiply(new BigDecimal(products.getInt("ProductPrice")))
                      .divide(Constants.HUNDRED);

              cartProductDetails.setTotalAmount(totalAmount);

              cartProductDetails.setTaxableAmount(
                  taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
              final Date cartTime = new Date(createdTime);

              cartProductDetails.setTime(cartTime);

              cartProductDetails.setBranch(employeeDetails.getBranch());

              cartProductDetails.setOrganisation(employeeDetails.getOrganisation());
              cartProductService.save(cartProductDetails);
            }
            final Billing billing = new Billing();

            billing.setCartId(cartObject);
            final String customerMobileNumber = billingData.getString("phoneno");

            if (discountDetails.getString("discounttypeicon").equals("%")) {
              if (discountDetails.getString("discountvalue").equalsIgnoreCase("")) {
                billing.setDiscountPercentage(new BigDecimal(0));
              } else {
                billing.setDiscountPercentage(
                    new BigDecimal(discountDetails.getString("discountvalue")));
              }
            } else {
              billing.setDiscountPercentage(new BigDecimal(0));
            }

            final String discountAmount = discountDetails.getString("amount");
            final BigDecimal sumOfTotalAmount = new BigDecimal(billingData.getString("total"));
            final String sumOfTaxableAmount = billingData.getString("gsttotal");

            final String sumOfcgstAmount = billingData.getString("sgst");
            final String sumOfsgstAmount = billingData.getString("cgst");
            final String offerDescription = discountDetails.getString("reason");
            // withtax
            billing.setTotalAmount(sumOfTotalAmount);
            // totaltax
            billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));

            billing.setNetAmount(new BigDecimal(billingData.getString("netamount")));
            billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
            billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
            billing.setBillDate(new Date(createdTime));
            billing.setServiceCharge(billingData.getLong("servicechargeamount"));
            billing.setPrintCount(0);
            billing.setOfferDescription(offerDescription);
            billing.setCounterDetails(cartObject.getCounterDetails());
            billing.setEmployeeDetails(employeeDetails);

            // if discouunt null
            if (discountAmount.equalsIgnoreCase("")) billing.setDiscountAmount(new BigDecimal(0));
            else billing.setDiscountAmount(new BigDecimal(discountAmount));

            billing.setOrganisation(employeeDetails.getOrganisation());
            billing.setBranch(employeeDetails.getBranch());
            billing.setCartId(cart);
            // for time purpose 24hr only
            final Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(createdTime);
            final Date formattedDate = cal.getTime();
            final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            final String time = dateFormat.format(formattedDate);
            billing.setTime(time);

            billing.setBillingNumber(billingData.getString(Constants.BILLID));
            final String billNumber = billingData.getString("onlinebillid");
            if (billNumber.equals("")) {
              final long billNumberCount =
                  billingNumDataServiceImpl.saveBillingNumberTypeData(
                      "yearly", employeeDetails.getEmailAddress());
              billing.setBillNumberCount(String.valueOf(billNumberCount));
            } else {
              billing.setBillNumberCount(String.valueOf(billNumber));
            }

            if (customerMobileNumber.equals("")) {
              billing.setCustomer(null);

              billing.setRewards(0);
            } else {

              final Customer customerDetail =
                  customerService.getCustomerByMobileNumberAndByOrgIdBranchId(
                      customerMobileNumber, orgId, branchId);
              if (customerDetail != null) {
                billing.setCustomer(customerDetail);
              } else {

                final Customer customerDetails = new Customer();

                if (cartData.getString("customerName").equals("")) {
                  customerDetails.setCustomerName(null);
                } else {
                  customerDetails.setCustomerName(cartData.getString("customerName"));
                }
                customerDetails.setMobileNumber(customerMobileNumber);
                customerDetails.setSocialProvider(AuthProvider.local);
                customerService.save(customerDetails);
                billing.setCustomer(customerDetails);
              }

              billing.setRewards(billingData.getLong(Constants.REWARDPOINTS));
              final Customer customerDetails =
                  customerService.getCustomerByMobileNumberAndByOrgIdBranchId(
                      customerMobileNumber, orgId, branchId);
              CustomerTransactionSummary existCstomerTransactionSummary = null;
              if (customerDetails != null) {
                existCstomerTransactionSummary =
                    customerTransactionSummaryService.getCustomerTransactionSummaryByCustomerId(
                        customerDetails.getCustomerId(), orgId, branchId);
              }
              if (existCstomerTransactionSummary != null) {
                long noOfVisits = 1l;

                final long billingAmount =
                    sumOfTotalAmount.longValue()
                        + existCstomerTransactionSummary.getLifetimeBillingAmount(); // with
                // tax

                final long taxbleAmount =
                    new BigDecimal(sumOfTaxableAmount).longValue()
                        + existCstomerTransactionSummary.getLifetimeTaxAmount(); // only
                // tax
                // total

                final long totalAmount =
                    billingAmount
                        - taxbleAmount
                        + existCstomerTransactionSummary.getLifetimeTotalAmount(); // without
                // tax
                // total

                noOfVisits = existCstomerTransactionSummary.getLifetimeVisits() + noOfVisits;

                long lifetimeDiscountAmount = Constants.COUNT;

                if (discountAmount.equals(""))
                  lifetimeDiscountAmount =
                      existCstomerTransactionSummary.getLifetimeDiscountAmount();
                else
                  lifetimeDiscountAmount =
                      existCstomerTransactionSummary.getLifetimeDiscountAmount()
                          + Long.parseLong(discountAmount);

                final long lifetimeRewardpoints =
                    billingData.getLong("rewardPoints")
                        + existCstomerTransactionSummary.getLifetimeRewardPoints();
                final long rewardpoints =
                    billingData.getLong("rewardPoints")
                        + existCstomerTransactionSummary.getRewardPoints();

                final long redeemPoints = billingData.getLong("redeemPoints");

                final Reward rewardDetails =
                    rewardService.getRewardByOrgAndBranchAndstatus(
                        orgId, branchId, Constants.STATUSTRUE);
                final double rewardPerUnit = rewardDetails.getRewardPerUnit();
                final BigDecimal previousRedeemPoints = BigDecimal.valueOf(rewardPerUnit);

                final BigDecimal afterRedeemPoints = new BigDecimal(redeemPoints);
                final BigDecimal redeemAmount = afterRedeemPoints.divide(previousRedeemPoints);
                billing.setRedeemAmount(redeemAmount);

                if (redeemPoints > 0) {
                  final long minusredeem =
                      existCstomerTransactionSummary.getRewardPoints() - redeemPoints;
                  existCstomerTransactionSummary.setRewardPoints(minusredeem);

                } else {
                  existCstomerTransactionSummary.setRewardPoints(rewardpoints);
                }

                existCstomerTransactionSummary.setModifiedDate(new Date(createdTime));
                existCstomerTransactionSummary.setLastTransactionDate(new Date(createdTime));
                existCstomerTransactionSummary.setBranchId(employeeDetails.getBranch().getId());
                existCstomerTransactionSummary.setOrgId(employeeDetails.getOrganisation().getId());
                existCstomerTransactionSummary.setLifetimeTotalAmount(totalAmount);
                existCstomerTransactionSummary.setLifetimeTaxAmount(taxbleAmount);
                existCstomerTransactionSummary.setLifetimeBillingAmount(billingAmount);
                existCstomerTransactionSummary.setLastTransactionDate(new Date(createdTime));
                existCstomerTransactionSummary.setLifetimeVisits(noOfVisits);
                existCstomerTransactionSummary.setLifetimeDiscountAmount(lifetimeDiscountAmount);
                existCstomerTransactionSummary.setLifetimeRewardPoints(lifetimeRewardpoints);
                customerTransactionSummaryService.update(existCstomerTransactionSummary);
              } else {
                final CustomerTransactionSummary customerTransactionSummary =
                    new CustomerTransactionSummary();

                customerTransactionSummary.setCustomer(billing.getCustomer().getCustomerId());
                customerTransactionSummary.setRewardPoints(billingData.getLong("rewardPoints"));

                customerTransactionSummary.setCreateDate(new Date(createdTime));
                customerTransactionSummary.setModifiedDate(new Date(createdTime));
                customerTransactionSummary.setBranchId(employeeDetails.getBranch().getId());
                customerTransactionSummary.setOrgId(employeeDetails.getOrganisation().getId());
                customerTransactionSummary.setLastTransactionDate(new Date(createdTime));

                final double totalAmounts =
                    sumOfTotalAmount.doubleValue()
                        - new BigDecimal(sumOfTaxableAmount).doubleValue();

                customerTransactionSummary.setLifetimeTotalAmount(100);
                customerTransactionSummary.setLifetimeTaxAmount(
                    new BigDecimal(sumOfTaxableAmount).longValue());
                customerTransactionSummary.setLifetimeBillingAmount(sumOfTotalAmount.longValue());

                customerTransactionSummary.setLifetimeVisits(1);
                customerTransactionSummary.setLifetimeDiscountAmount(
                    new BigDecimal(discountAmount).longValue());
                customerTransactionSummary.setLifetimeRewardPoints(
                    billingData.getLong("rewardPoints"));

                customerTransactionSummaryService.save(customerTransactionSummary);
              }

              RewardHistory rewardHistory = new RewardHistory();
              rewardHistory.setCustomer(billing.getCustomer());
              rewardHistory.setDate(new Date());
              rewardHistory.setRewardPoints(billingData.getLong("rewardPoints"));
              rewardHistory.setStartTime(new Date());
              try {
                final long rpu =
                    (long)
                        rewardService
                            .getRewardByOrgAndBranchAndstatus(orgId, branchId, true)
                            .getRewardPerUnit();
                rewardHistory.setRedeemAmount(billingData.getLong("redeemPoints") / rpu);
                rewardHistoryService.save(rewardHistory);
              } catch (Exception e) {
                LOGGER.error("rewardHistory " + e);
              }
            }

            billingService.save(billing);

            final JSONArray billingPaymentsModeArray = billingData.getJSONArray("paymenttype");

            for (int s = 0; s < billingPaymentsModeArray.length(); s++) {

              final JSONObject listpaymentMode = ((JSONObject) billingPaymentsModeArray.get(s));
              final String paymentModes = listpaymentMode.getString("name");
              final long paymentAmount = listpaymentMode.getLong("Amount");
              final BillingPayments billingPayments = new BillingPayments();
              billingPayments.setBilling(billing);
              billingPayments.setAmount(paymentAmount);
              billingPayments.setPaymentMode(paymentModes);
              billingPayments.setOrganisation(employeeDetails.getOrganisation());
              billingPayments.setBranch(employeeDetails.getBranch());
              billingPayments.setDate(new Date(createdTime));
              billingPaymentsService.save(billingPayments);
            }

            // for updating remaing qty
            for (int j = 0; j < ja.length(); ++j) {

              final JSONObject products = ja.getJSONObject(j);

              final Product productDetails =
                  productsService.getProductsByProductNameAndOrgIdAndBranchId(
                      products.getString("ProductName"), orgId, branchId);
              try {
                final InventoryDetail inventoryDetail =
                    inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                        productDetails.getId(), branchId);
                if (inventoryDetail != null) {

                  // for updating remaing qty
                  final BigDecimal existingQty = inventoryDetail.getQuantity();
                  final long purchaseQty = Long.parseLong(products.getString("Quantity"));
                  final long remaingQty = existingQty.longValue() - purchaseQty;

                  inventoryDetail.setQuantity(new BigDecimal(remaingQty));
                  inventoryDetailService.update(inventoryDetail);
                } else {
                  LOGGER.info("Don't have Inventory");
                }
              } catch (final NullPointerException e) {
                LOGGER.error("inventoryDetails " + e, e);
              }
            }
            cart.setCartStatus(Constants.CLOSESTATUS);
            cartService.update(cart);
            counterDetails.setCounterStatus("Closed");
            counterService.update(counterDetails);

            // order.setOrderStatus("Closed");
            orderService.update(order);
            counterDetails.setCounterStatus("Closed");
            counterService.update(counterDetails);

          } else if (cart != null) {

            final long cardID = cart.getCartId();

            final List<CartDetail> existCartProducts =
                cartProductService.getCartDetailsByCartId(cardID);

            if (existCartProducts.size() == ja.length()) {

              for (int j = 0; j < existCartProducts.size(); j++) {

                for (int k = 0; k < ja.length(); k++) {

                  final JSONObject products = ja.getJSONObject(k);

                  final String ProductName = products.getString("ProductName");
                  final long Quantity = products.getLong("Quantity");

                  final CartDetail cartProductDetail = existCartProducts.get(i);
                  if (cartProductDetail.getProductName().equals(ProductName)
                      && cartProductDetail.getQuantity() == Quantity) {

                  } else {
                    System.out.println("product Name or Quantity does not matched");

                    review.setGuid(cartData.getString("guid"));
                    review.setParentGuid(cartData.getString("parentguid"));
                    review.setBillDate(new Date(createdTime));
                    review.setBranch(employeeDetails.getBranch());
                    review.setOrganisation(employeeDetails.getOrganisation());

                    review.setBillingId(billingData.getString("billid"));
                    review.setBillAmount(new BigDecimal(billingData.getString("total")));
                    review.setTaxableAmount(new BigDecimal(billingData.getInt("gsttotal")));
                    reviewService.save(review);
                    return;
                  }
                }
              }

            } else {
              System.out.println("Existed cartProduct details length not matched");
              for (int j = 0; j < ja.length(); j++) {

                final JSONObject products = ja.getJSONObject(j);

                review.setGuid(cartData.getString("guid"));
                review.setParentGuid(cartData.getString("parentguid"));
                review.setBillDate(new Date(createdTime));
                review.setBranch(employeeDetails.getBranch());
                review.setOrganisation(employeeDetails.getOrganisation());
                review.setBillingId(billingData.getString("billid"));
                review.setBillAmount(new BigDecimal(billingData.getInt("billingAmount")));
                review.setTaxableAmount(new BigDecimal(billingData.getInt("totalTaxableAmount")));
                reviewService.save(review);
                return;
              }
            }
          }

        } else {
          System.out.println("Organisation Number Not Matched");
        }
      } catch (final Exception e) {

        LOGGER.error(" rxdb rest controller " + e, e);
      }
    }
  }

  @GetMapping(path = RouteConstants.GET_BILL_NUMBER)
  public final Map<String, Object> getProudctCategoryDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final Map<String, Object> billCount = new HashMap<>();

    try {
      final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

      long billingNumData =
          billingNumDataServiceImpl.saveBillingNumberTypeData(
              "yearly", employeeDetails.getEmailAddress());
      billCount.put("count", billingNumData);

    } catch (final Exception e) {
      LOGGER.error("getBillNumber" + e);
    }
    return billCount;
  }

  @GetMapping(path = RouteConstants.EXIT_PRODUCT_DETAILS)
  public final List<CartDetail> getproductDetails(
      final HttpServletRequest request, final HttpServletResponse responce) throws JSONException {

    List<CartDetail> cartProductDetails = new ArrayList<>();
    Cart cartDetails = null;
    try {
      final String counterNumber = request.getParameter("counterNumber");
      if (counterNumber != null && !counterNumber.isEmpty()) {
        cartDetails =
            cartService.getCartDetailsOnStatus(Constants.CARTSTATUS, Long.parseLong(counterNumber));
        cartProductDetails = cartProductService.getCartDetailsByCartId(cartDetails.getCartId());
      }
    } catch (final Exception e) {
      LOGGER.error("Exist cartProductDetails" + e, e);
    }
    return cartProductDetails;
  }

  @GetMapping(path = RouteConstants.ADD_PRODUCT_DETAILS)
  public final Product getproductDetails1(
      final HttpServletRequest request, final HttpServletResponse responce) throws JSONException {

    final long counterNumber = Long.parseLong(request.getParameter("counterNumber"));
    final String code = request.getParameter("productcode");
    final String QTY = "1";

    Product productDetails = null;
    Item itemDetails = null;
    final CartDetail cartProductDetail = new CartDetail();
    Cart cartDetails = new Cart();

    try {

      cartDetails = cartService.getCartDetailsOnStatus(Constants.CARTSTATUS, counterNumber);
      System.out.println(cartDetails.getCartId());
      productDetails = productsService.getProductsByProductCode(code);
      itemDetails = itemsService.getItemsByHsnNo(productDetails.getHsnNumber());

      cartProductDetail.setCartDetails(cartDetails);
      cartProductDetail.setCounterDetails(cartDetails.getCounterDetails());
      cartProductDetail.setDiscountAmount(null);
      cartProductDetail.setHsnCode(productDetails.getHsnNumber());
      cartProductDetail.setCgstPercentage(itemDetails.getPercentageDetails().getCGST());
      cartProductDetail.setSgstPercentage(itemDetails.getPercentageDetails().getSGST());
      cartProductDetail.setIgstPercentage(itemDetails.getPercentageDetails().getIGST());
      cartProductDetail.setProductName(productDetails.getProductName());
      cartProductDetail.setQuantity(Long.parseLong(QTY));
      cartProductDetail.setProductPrice(productDetails.getProductPrice());

      final BigDecimal totalAmount = productDetails.getProductPrice().multiply(new BigDecimal(QTY));
      final BigDecimal taxableAmount =
          itemDetails
              .getPercentageDetails()
              .getCGST()
              .add(itemDetails.getPercentageDetails().getSGST())
              .add(itemDetails.getPercentageDetails().getIGST())
              .multiply(productDetails.getProductPrice())
              .divide(Constants.HUNDRED);

      cartProductDetail.setTotalAmount(totalAmount);
      cartProductDetail.setTaxableAmount(taxableAmount.multiply(new BigDecimal(QTY)));
      final Date cartTime = new Date();
      cartProductDetail.setTime(cartTime);

      cartProductService.save(cartProductDetail);
    } catch (final Exception e) {
      LOGGER.error("savecartProductDetails" + e, e);
    }
    return productDetails;
  }

  @PostMapping(path = RouteConstants.GET_PRODUCT_RESTUARANT_BILLING)
  public final long getProductRestaurantBilling(
      final HttpServletRequest request, final HttpServletResponse responce, final Billing billing)
      throws JSONException, BasicTemplateServiceException {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long counterNumber = Long.parseLong(request.getParameter("counterNumber"));

    final Counter counterDetails = counterService.getCounterDetailsById(counterNumber);
    Cart cart = new Cart();
    try {
      cart = cartService.getCartDetailsOnStatus(Constants.CARTSTATUS, counterNumber);
      if (cart == null) {
        cart = new Cart();
        cart.setCartStatus(Constants.CARTSTATUS);
        final Date date = new Date();
        cart.setStartTime(date);
        cart.setEndTime(date);
        cart.setCounterDetails(counterDetails);
        cartService.save(cart);
      }
    } catch (final Exception e) {
      LOGGER.error("generate RestaurentCart()" + e, e);
    }

    final String customerMobileNumber = request.getParameter("customerMobileNumber");

    billing.setCartId(cart);

    if (customerMobileNumber.equals("")) {
      billing.setCustomer(null);
    } else {
      billing.setCustomer(customerService.getCustomerByMobileNumber(customerMobileNumber));
    }

    final String discountPercentage = request.getParameter("discountPercentage");

    final String discountAmount = request.getParameter("discountAmount");

    final String sumOfTotalAmount = request.getParameter("netAmount");
    final String sumOfTaxableAmount = request.getParameter("taxableAmount");

    final String sumOfcgstAmount = request.getParameter("cgstAmount");
    final String sumOfsgstAmount = request.getParameter("sgstAmount");

    // withtax
    billing.setTotalAmount(new BigDecimal(sumOfTotalAmount));
    // totaltax
    billing.setTaxableAmount(new BigDecimal(sumOfTaxableAmount));

    billing.setCgstAmount(new BigDecimal(sumOfcgstAmount));
    billing.setSgstAmount(new BigDecimal(sumOfsgstAmount));
    billing.setBillDate(new Date());
    billing.setCounterDetails(cart.getCounterDetails());

    if (customerMobileNumber.equals("")) {
      billing.setCustomer(null);
    } else {
      billing.setCustomer(customerService.getCustomerByMobileNumber(customerMobileNumber));
    }

    billing.setEmployeeDetails(employeeDetails);
    billing.setDiscountPercentage(new BigDecimal(discountPercentage));
    billing.setDiscountAmount(new BigDecimal(discountAmount));
    billingService.save(billing);

    cart.setCartStatus("Closed");
    cartService.update(cart);
    counterDetails.setCounterStatus("Closed");
    counterService.update(counterDetails);

    return billingService.getBillingDetailsByCartId(cart.getCartId()).getId();
  }

  @GetMapping(path = RouteConstants.GET_RESERVATION_DETAILS_BY_PRINTCOUNT)
  public final ResponseEntity<Object> getReservationDetailsByprintcount(
      final HttpServletRequest request, final HttpServletResponse responce) {
    Billing billingDetails = new Billing();

    long printCount = 0;
    try {
      final String billingId = request.getParameter("billingId");
      if (billingId != null && !billingId.isEmpty()) {
        billingDetails = billingService.getBillingDetailsByBillId(Long.parseLong(billingId));
        printCount = billingDetails.getPrintCount();
        printCount++;
        billingDetails.setPrintCount(printCount);
        billingService.update(billingDetails);
      }
    } catch (final NullPointerException ne) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getBIllingDetails " + e);
    }
    return new ResponseEntity<>(billingDetails, HttpStatus.OK);
  }

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.GET_POST_CUSTOMER)
  public final void getPostCustomer(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws Exception {

    final String authToken = request.getHeader(tokenHeader);
    final String token = authToken.substring(7);
    final Identifier identifier = jwtTokenUtil.getSubjectFromToken(token);
    final long userId = identifier.getUserId();
    Customer customerDetails = null;
    if (identifier.getUserType().equalsIgnoreCase("C")) {
      customerDetails = customerService.getCustomerDetailsBycustomerId(userId);
    }

    final Review review = new Review();
    Order order = new Order();
    Cart cart = new Cart();
    final Subscription subscription = new Subscription();

    final JSONObject cartData = new JSONObject(payload);

    final Counter counterDetails =
        counterService.getCounterDetailOnlineStatus(Constants.STATUSTRUE);
    final String guid = cartData.getString("guid");

    try {

      final JSONArray ja = new JSONArray(cartData.getString("products"));

      if (customerDetails != null && customerDetails.getOrganisation() != null) {
        final long branchId = customerDetails.getBranch().getId();
        final long orgId = customerDetails.getOrganisation().getId();

        order = orderService.getOrderByGuid(guid);
        cart = cartService.getCartByGuid(guid);

        if (order == null && cart == null) {
          cart = new Cart();
          cart.setCartStatus(Constants.CARTSTATUS);
          final Date date1 = new Date(System.currentTimeMillis());
          cart.setStartTime(date1);
          cart.setEndTime(date1);
          cart.setCounterDetails(counterDetails);
          cart.setGuid(guid);
          cart.setDescription("customer cart");
          cartService.save(cart);

          order = new Order();
          // order.setOrderStatus(Constants.ORDER_STATUS);
          final Date date = new Date(System.currentTimeMillis());
          order.setCreatedTs(date);
          order.setModifiedTs(date);
          order.setCounterDetails(counterDetails);
          order.setGuid(guid);
          order.setBranch(customerDetails.getBranch());
          order.setOrganisation(customerDetails.getOrganisation());
          final Map<String, Object> orderLocationAddressMap = new HashMap<>();
          if (customerDetails.getBranch() == null) {
            orderLocationAddressMap.put(
                "orderLocationAddress", customerDetails.getOrganisation().getAddress());
            order.setOrderlocationAddress(orderLocationAddressMap);
          } else {
            orderLocationAddressMap.put(
                "orderLocationAddress", customerDetails.getBranch().getAddress());
            order.setOrderlocationAddress(orderLocationAddressMap);
          }
          final Customer customer =
              customerService.getCustomerByMobileNumberAndByOrgIdBranchId(
                  customerDetails.getMobileNumber(),
                  customerDetails.getOrganisation().getId(),
                  customerDetails.getBranch().getId());
          if (customer != null) {
            order.setCustomer(customer);
            final Map<String, Object> deliveryAddressMap = new HashMap<>();
            deliveryAddressMap.put("deliveryAddress", customer.getAddress());
            order.setDeliveryAddress(deliveryAddressMap);
          } else {
            order.setCustomer(null);
            order.setDeliveryAddress(null);
          }
          order.setOrderdate(new Date());

          orderService.save(order);

          OrderHistory orderHistory = new OrderHistory();
          orderHistory.setDeliveryStatusTime(date);
          orderHistory.setBranch(customerDetails.getBranch());
          orderHistory.setOrganisation(customerDetails.getOrganisation());
          orderHistory.setCreatedTs(date);
          orderHistory.setModifiedTs(date);
          orderHistory.setDbcreatedTs(date);
          orderHistory.setDbmodifiedTs(date);
          orderHistory.setOrder(order);
          // orderHistory.setReason(reason);
          // orderHistory.setOrderNumber(orderNumber);
          orderHistoryService.save(orderHistory);
          Order orderObject = new Order();
          Cart cartObject = new Cart();
          for (int j = 0; j < ja.length(); j++) {

            final JSONObject products = ja.getJSONObject(j);

            final long id = order.getId();

            orderObject = orderService.getOrderById(id);
            final OrderLineItems orderLineItems = new OrderLineItems();
            orderLineItems.setOrderId(orderObject);
            orderLineItems.setCounterDetails(orderObject.getCounterDetails());
            orderLineItems.setDiscountAmount(new BigDecimal(0));
            orderLineItems.setHsnCode(products.getString("HsnNumber"));
            orderLineItems.setCgst(new BigDecimal(products.getInt("CGST")));
            orderLineItems.setSgst(new BigDecimal(products.getInt("SGST")));

            orderLineItems.setProductName(products.getString("ProductName"));
            orderLineItems.setQuantity(Long.parseLong(products.getString("Quantity")));
            orderLineItems.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));

            final BigDecimal totalAmount =
                new BigDecimal(products.getInt("ProductPrice"))
                    .multiply(new BigDecimal(products.getString("Quantity")));

            final BigDecimal taxableAmount =
                new BigDecimal(products.getInt("CGST"))
                    .add(new BigDecimal(products.getInt("SGST")))
                    .multiply(new BigDecimal(products.getInt("ProductPrice")))
                    .divide(Constants.HUNDRED);

            orderLineItems.setTotalAmount(totalAmount);

            orderLineItems.setTaxableAmount(
                taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
            final Date orderTime = new Date(System.currentTimeMillis());

            orderLineItems.setTime(orderTime);

            orderLineItems.setBranchId(customerDetails.getBranch());

            orderLineItems.setOrganisation(customerDetails.getOrganisation());
            orderLineItemsService.save(orderLineItems);

            final long cartId = cart.getCartId();

            cartObject = cartService.getCartDetailsByCartId(cartId);
            final CartDetail cartProductDetails = new CartDetail();
            cartProductDetails.setCartDetails(cartObject);
            cartProductDetails.setCounterDetails(cartObject.getCounterDetails());
            cartProductDetails.setDiscountAmount(new BigDecimal(0));
            cartProductDetails.setHsnCode(products.getString("HsnNumber"));
            cartProductDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
            cartProductDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));

            cartProductDetails.setProductName(products.getString("ProductName"));
            cartProductDetails.setQuantity(Long.parseLong(products.getString("Quantity")));
            cartProductDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));

            final BigDecimal totalAmount1 =
                new BigDecimal(products.getInt("ProductPrice"))
                    .multiply(new BigDecimal(products.getString("Quantity")));

            final BigDecimal taxableAmount1 =
                new BigDecimal(products.getInt("CGST"))
                    .add(new BigDecimal(products.getInt("SGST")))
                    .multiply(new BigDecimal(products.getInt("ProductPrice")))
                    .divide(Constants.HUNDRED);

            cartProductDetails.setTotalAmount(totalAmount1);

            cartProductDetails.setTaxableAmount(
                taxableAmount1.multiply(new BigDecimal(products.getString("Quantity"))));
            final Date cartTime = new Date(System.currentTimeMillis());

            cartProductDetails.setTime(cartTime);

            cartProductDetails.setBranch(customerDetails.getBranch());

            cartProductDetails.setOrganisation(customerDetails.getOrganisation());
            cartProductService.save(cartProductDetails);
          }

        } else {

          Cart cartDetail = new Cart();
          cartDetail = cartService.getCartDetailsByCartId(cart.getCartId());
          cartDetail.setCartStatus(Constants.CARTSTATUS);
          final Date date1 = new Date(System.currentTimeMillis());
          cartDetail.setStartTime(date1);
          cartDetail.setEndTime(date1);
          cartDetail.setCounterDetails(counterDetails);
          cartDetail.setGuid(guid);
          cartDetail.setDescription("customer cart");
          cartService.update(cartDetail);

          Order orderDetail = new Order();
          orderDetail = orderService.getOrderById(order.getId());
          // orderDetail.setOrderStatus(Constants.ORDER_STATUS);
          final Date date = new Date(System.currentTimeMillis());
          orderDetail.setCreatedTs(date);
          orderDetail.setModifiedTs(date);
          orderDetail.setCounterDetails(counterDetails);
          orderDetail.setGuid(guid);
          orderDetail.setBranch(customerDetails.getBranch());
          orderDetail.setOrganisation(customerDetails.getOrganisation());
          final Map<String, Object> orderLocationAddressMap = new HashMap<>();
          if (customerDetails.getBranch() == null) {
            orderLocationAddressMap.put(
                "orderLocationAddress", customerDetails.getOrganisation().getAddress());
            orderDetail.setOrderlocationAddress(orderLocationAddressMap);
          } else {
            orderLocationAddressMap.put(
                "orderLocationAddress", customerDetails.getBranch().getAddress());
            orderDetail.setOrderlocationAddress(orderLocationAddressMap);
          }
          final Customer customer =
              customerService.getCustomerByMobileNumberAndByOrgIdBranchId(
                  customerDetails.getMobileNumber(),
                  customerDetails.getOrganisation().getId(),
                  customerDetails.getBranch().getId());
          if (customer != null) {

            orderDetail.setCustomer(customer);
            final Map<String, Object> deliveryAddressMap = new HashMap<>();
            deliveryAddressMap.put("deliveryAddress", customer.getAddress());
            orderDetail.setDeliveryAddress(deliveryAddressMap);
          } else {
            orderDetail.setCustomer(null);
            orderDetail.setDeliveryAddress(null);
          }
          orderDetail.setOrderdate(new Date());

          orderService.update(orderDetail);
          OrderHistory orderHistory = new OrderHistory();
          orderHistory.setDeliveryStatusTime(date);
          orderHistory.setBranch(customerDetails.getBranch());
          orderHistory.setOrganisation(customerDetails.getOrganisation());
          orderHistory.setCreatedTs(date);
          orderHistory.setModifiedTs(date);
          orderHistory.setDbcreatedTs(date);
          orderHistory.setDbmodifiedTs(date);
          orderHistory.setOrder(orderDetail);
          // orderHistory.setReason(reason);
          // orderHistory.setOrderNumber(orderNumber);
          orderHistoryService.save(orderHistory);
          Order orderObject = new Order();
          Cart cartObject = new Cart();
          for (int j = 0; j < ja.length(); j++) {

            final JSONObject products = ja.getJSONObject(j);

            final long id = orderDetail.getId();

            orderObject = orderService.getOrderById(id);

            final List<OrderLineItems> orderLineDetails =
                orderLineItemsService.getOrderLineItemsByorderId(id);
            for (final OrderLineItems orderLine : orderLineDetails) {

              orderLine.setOrderId(orderObject);
              orderLine.setCounterDetails(orderObject.getCounterDetails());
              orderLine.setDiscountAmount(new BigDecimal(0));
              orderLine.setHsnCode(products.getString("HsnNumber"));
              orderLine.setCgst(new BigDecimal(products.getInt("CGST")));
              orderLine.setSgst(new BigDecimal(products.getInt("SGST")));

              orderLine.setProductName(products.getString("ProductName"));
              orderLine.setQuantity(Long.parseLong(products.getString("Quantity")));

              orderLine.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));

              final BigDecimal totalAmount =
                  new BigDecimal(products.getInt("ProductPrice"))
                      .multiply(new BigDecimal(products.getString("Quantity")));

              final BigDecimal taxableAmount =
                  new BigDecimal(products.getInt("CGST"))
                      .add(new BigDecimal(products.getInt("SGST")))
                      .multiply(new BigDecimal(products.getInt("ProductPrice")))
                      .divide(Constants.HUNDRED);

              orderLine.setTotalAmount(totalAmount);

              orderLine.setTaxableAmount(
                  taxableAmount.multiply(new BigDecimal(products.getString("Quantity"))));
              final Date orderTime = new Date(System.currentTimeMillis());

              orderLine.setTime(orderTime);

              orderLine.setBranchId(customerDetails.getBranch());

              orderLine.setOrganisation(customerDetails.getOrganisation());
              orderLineItemsService.update(orderLine);
            }
            final long cartId = cartDetail.getCartId();

            cartObject = cartService.getCartDetailsByCartId(cartId);

            final List<CartDetail> cartProductDetails =
                cartProductService.getCartDetailsByCartId(cartId);

            for (final CartDetail cartProduct : cartProductDetails) {

              cartProduct.setCartDetails(cartObject);
              cartProduct.setCounterDetails(cartObject.getCounterDetails());
              cartProduct.setDiscountAmount(new BigDecimal(0));
              cartProduct.setHsnCode(products.getString("HsnNumber"));
              cartProduct.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
              cartProduct.setSgstPercentage(new BigDecimal(products.getInt("SGST")));

              cartProduct.setProductName(products.getString("ProductName"));
              cartProduct.setQuantity(Long.parseLong(products.getString("Quantity")));

              cartProduct.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));

              final BigDecimal totalAmount1 =
                  new BigDecimal(products.getInt("ProductPrice"))
                      .multiply(new BigDecimal(products.getString("Quantity")));

              final BigDecimal taxableAmount1 =
                  new BigDecimal(products.getInt("CGST"))
                      .add(new BigDecimal(products.getInt("SGST")))
                      .multiply(new BigDecimal(products.getInt("ProductPrice")))
                      .divide(Constants.HUNDRED);

              cartProduct.setTotalAmount(totalAmount1);

              cartProduct.setTaxableAmount(
                  taxableAmount1.multiply(new BigDecimal(products.getString("Quantity"))));
              final Date cartTime = new Date(System.currentTimeMillis());

              cartProduct.setTime(cartTime);

              cartProduct.setBranch(customerDetails.getBranch());

              cartProduct.setOrganisation(customerDetails.getOrganisation());
              cartProductService.update(cartProduct);
            }
          }
        }

      } else {
        System.out.println("Organisation Number Not Matched");
      }

    } catch (final Exception e) {
      LOGGER.error("getCustomerCartDetails " + e, e);
    }
  }

  @GetMapping(path = RouteConstants.GET_CART_CUSTOMER)
  public final ResponseEntity<Object> getProductCategoriesOfMenuCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    final String orgId = request.getParameter("orgId");
    final List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    final Map<String, Object> cartmap = new HashMap<String, Object>();

    Cart cartDetails = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long userId = identifier.getUserId();
        final long branchId = identifier.getBranchId();
        if (identifier.getUserType().equalsIgnoreCase("C")) {
          final Customer customerDetails = customerService.getCustomerDetailsBycustomerId(userId);
          cartDetails =
              cartService.getCartDetailsByStatus(
                  Constants.CARTSTATUS, customerDetails.getCustomerId(), Long.parseLong(orgId));

          final long cartId = cartDetails.getCartId();
          final List<Object[]> products = cartProductService.getCartDetailsByCartDetailsId(cartId);
          for (int i = 0; i < products.size(); i++) {

            final Product product =
                productsService.getProductsByProductNameAndOrgAndBranch(
                    products.get(i)[1].toString(), Long.parseLong(orgId), branchId);
            final Map<String, Object> map1 = new HashMap<String, Object>();

            map1.put("cartProductId", products.get(i)[0]);
            map1.put("productName", products.get(i)[1]);
            map1.put("quantity", products.get(i)[2]);

            map1.put("hsnCode", products.get(i)[3]);

            map1.put("productPrice", products.get(i)[4]);

            map1.put("discountAmount", products.get(i)[5]);

            map1.put("cgstPercentage", products.get(i)[6]);

            map1.put("sgstPercentage", products.get(i)[7]);

            map1.put("igstPercentage", products.get(i)[8]);
            map1.put("time", products.get(i)[9]);

            map1.put("totalAmount", products.get(i)[10]);
            map1.put("taxableAmount", products.get(i)[11]);
            map1.put("productImage", product.getImage());
            map1.put("productCode", product.getCode());

            map1.put("measurement", product.getMeasurement());

            map1.put("measurement1", product.getMeasurement1());
            map1.put("measurement2", product.getMeasurement2());

            map1.put(
                "variationType",
                product.getVariationType().getQuantityMeasurement().getDisplayName());

            list.add(map1);
          }

          final Map<String, Object> map = new HashMap<String, Object>();

          map.put("cartId", cartDetails.getCartId());

          map.put("startTime", cartDetails.getStartTime());

          map.put("guId", cartDetails.getGuid());

          map.put("endTime", cartDetails.getEndTime());

          map.put("cartStatus", cartDetails.getCartStatus());

          map.put("parentGuid", cartDetails.getParentguId());

          map.put("description", cartDetails.getDescription());

          map.put("maxCapacity", cartDetails.getMaxCapacity());

          map.put("occupiedCapacity", cartDetails.getOccupiedCapacity());

          cartmap.put("cartDetails", map);
          cartmap.put("products", list);
        }
      } else {
        return new ResponseEntity<>(cartmap, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProductsbycategory" + e, e);
    }
    return new ResponseEntity<>(cartmap, HttpStatus.OK);
  }

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.SYNC_CART_CUSTOMER)
  public final void getSyncCustomer(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws Exception {

    final String authToken = request.getHeader(tokenHeader);
    final String token = authToken.substring(7);
    final Identifier identifier = jwtTokenUtil.getSubjectFromToken(token);
    final long userId = identifier.getUserId();
    Customer customerDetails = null;
    if (identifier.getUserType().equalsIgnoreCase("C")) {
      customerDetails = customerService.getCustomerDetailsBycustomerId(userId);
    }
    final long branchId = customerDetails.getBranch().getId();
    final long orgId = customerDetails.getOrganisation().getId();

    Cart cart = new Cart();

    final JSONObject cartData = new JSONObject(payload);

    final Counter counterDetails =
        counterService.getCounterDetailOnlineStatus(Constants.STATUSTRUE);
    final String guid = cartData.getString("guid");

    try {

      final JSONArray ja = new JSONArray(cartData.getString("products"));

      if (customerDetails.getOrganisation() != null) {

        cart = cartService.getCartByGuid(guid);

        if (cart != null) {

          final long cartId = cart.getCartId();

          final List<CartDetail> cartDetails = cartProductService.getCartDetailsByCartId(cartId);

          for (final CartDetail cartProduct : cartDetails) {
            final long id = cartProduct.getId();
            cartProductService.delete(id);
          }

          final Cart cartDetail = new Cart();
          cartDetail.setCartStatus(Constants.CARTSTATUS);
          final Date date = new Date(System.currentTimeMillis());
          cartDetail.setStartTime(date);
          cartDetail.setEndTime(date);
          cartDetail.setCounterDetails(counterDetails);
          cartDetail.setGuid(guid);
          cartDetail.setDescription("customer cart");
          cartDetail.setBranch(customerDetails.getBranch());
          cartDetail.setOrganisation(customerDetails.getOrganisation());

          cartDetail.setCustomer(customerDetails);

          cartService.save(cartDetail);

          Cart cartObject = new Cart();
          for (int j = 0; j < ja.length(); j++) {

            final JSONObject products = ja.getJSONObject(j);

            final long cartid = cartDetail.getCartId();

            cartObject = cartService.getCartDetailsByCartId(cartid);
            final CartDetail cartProductDetails = new CartDetail();
            cartProductDetails.setCartDetails(cartObject);
            cartProductDetails.setCounterDetails(cartObject.getCounterDetails());
            cartProductDetails.setDiscountAmount(new BigDecimal(0));
            cartProductDetails.setHsnCode(products.getString("HsnNumber"));
            cartProductDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
            cartProductDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));

            cartProductDetails.setProductName(products.getString("ProductName"));
            cartProductDetails.setQuantity(Long.parseLong(products.getString("Quantity")));

            cartProductDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));

            final BigDecimal totalAmount1 =
                new BigDecimal(products.getInt("ProductPrice"))
                    .multiply(new BigDecimal(products.getString("Quantity")));

            final BigDecimal taxableAmount1 =
                new BigDecimal(products.getInt("CGST"))
                    .add(new BigDecimal(products.getInt("SGST")))
                    .multiply(new BigDecimal(products.getInt("ProductPrice")))
                    .divide(Constants.HUNDRED);

            cartProductDetails.setTotalAmount(totalAmount1);

            cartProductDetails.setTaxableAmount(
                taxableAmount1.multiply(new BigDecimal(products.getString("Quantity"))));
            final Date cartTime = new Date(System.currentTimeMillis());

            cartProductDetails.setTime(cartTime);

            cartProductDetails.setBranch(customerDetails.getBranch());

            cartProductDetails.setOrganisation(customerDetails.getOrganisation());
            cartProductService.save(cartProductDetails);
          }

        } else {

          final Cart cartDetail = new Cart();
          cartDetail.setCartStatus(Constants.CARTSTATUS);
          final Date date = new Date(System.currentTimeMillis());
          cartDetail.setStartTime(date);
          cartDetail.setEndTime(date);
          cartDetail.setCounterDetails(counterDetails);
          cartDetail.setGuid(guid);
          cartDetail.setDescription("customer cart");
          cartDetail.setBranch(customerDetails.getBranch());
          cartDetail.setOrganisation(customerDetails.getOrganisation());

          cartDetail.setCustomer(customerDetails);

          cartService.save(cartDetail);

          Cart cartObject = new Cart();
          for (int j = 0; j < ja.length(); j++) {

            final JSONObject products = ja.getJSONObject(j);

            final long cartId = cartDetail.getCartId();

            cartObject = cartService.getCartDetailsByCartId(cartId);
            final CartDetail cartProductDetails = new CartDetail();
            cartProductDetails.setCartDetails(cartObject);
            cartProductDetails.setCounterDetails(cartObject.getCounterDetails());
            cartProductDetails.setDiscountAmount(new BigDecimal(0));
            cartProductDetails.setHsnCode(products.getString("HsnNumber"));
            cartProductDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
            cartProductDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));

            cartProductDetails.setProductName(products.getString("ProductName"));
            cartProductDetails.setQuantity(Long.parseLong(products.getString("Quantity")));

            cartProductDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));

            final BigDecimal totalAmount1 =
                new BigDecimal(products.getInt("ProductPrice"))
                    .multiply(new BigDecimal(products.getString("Quantity")));

            final BigDecimal taxableAmount1 =
                new BigDecimal(products.getInt("CGST"))
                    .add(new BigDecimal(products.getInt("SGST")))
                    .multiply(new BigDecimal(products.getInt("ProductPrice")))
                    .divide(Constants.HUNDRED);

            cartProductDetails.setTotalAmount(totalAmount1);

            cartProductDetails.setTaxableAmount(
                taxableAmount1.multiply(new BigDecimal(products.getString("Quantity"))));
            final Date cartTime = new Date(System.currentTimeMillis());

            cartProductDetails.setTime(cartTime);

            cartProductDetails.setBranch(customerDetails.getBranch());

            cartProductDetails.setOrganisation(customerDetails.getOrganisation());
            cartProductService.save(cartProductDetails);
          }
        }

      } else {
        System.out.println("Organisation Number Not Matched");
      }

    } catch (final Exception e) {
      LOGGER.error("getCustomerCartDetails " + e, e);
    }
  }
}
