package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Reservation;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.ReservationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsss.basic.config.KafkaSender;
import com.tsss.basic.domain.AuthProvider;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReservationController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReservationController.class);

  @Autowired private CounterService counterService;

  @Autowired private ReservationService reservationService;

  @Autowired private CustomerService customerService;

  @Autowired private KafkaSender kafkaSender;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  /*
   * we are getting multiple reservation records in offline side i need to save
   * those all details
   */
  @PostMapping(path = RouteConstants.ADD_RESERVATION_DETAILS)
  public final Map<String, Object> addReservationDetails(
      @RequestBody final String reservationDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException, Exception {
    final JSONObject reservation = new JSONObject(reservationDetails);
    final String kidsCountString = reservation.getString(Constants.KIDSCOUNT);
    long kidsCount = 0;
    if (kidsCountString.contentEquals("")) {

    } else {
      kidsCount = Long.parseLong(kidsCountString);
    }

    final String adultCount = reservation.getString(Constants.ADULTCOUNT);
    final String starttime = reservation.getString(Constants.STARTTIME);
    final String reserveType = reservation.getString(Constants.RESERVATIONTYPE);
    final String note = reservation.getString("note");
    final String mobileNumber = reservation.getString(Constants.MOBILENUMBER);
    final String customerName = reservation.getString(Constants.CUSTOMERNAME);
    final String dateString = reservation.getString("date");
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final SimpleDateFormat sdftime = new SimpleDateFormat(Constants.SDFTIME);
    final Date date = sdf.parse(dateString);

    final Map<String, Object> producerMap = new HashMap<>();
    final ObjectMapper objMapper = new ObjectMapper();
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Branch branch = employeeDetails.getBranch();
    final long orgId = employeeDetails.getOrganisation().getId();
    final Reservation reservations = new Reservation();
    final Date startTime = sdftime.parse(starttime);
    final SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    final Date d = df.parse(starttime);
    final Calendar cal = Calendar.getInstance();
    cal.setTime(d);
    cal.add(Calendar.MINUTE, 15);
    cal.add(Calendar.MINUTE, -30);
    if (kidsCountString.equals("") && reservation.getString("adultCount").equals("")) {
      producerMap.put(Constants.RESERVATIONSTATUS, "Adult or kids count must enter");
      return producerMap;
    }
    if (!kidsCountString.isEmpty()) {
      reservations.setKidscount(kidsCount);
    } else {
      reservations.setKidscount(Constants.COUNT);
    }
    if (!(reservation.getString("adultCount")).isEmpty()) {
      reservations.setAdultcount(Long.parseLong(adultCount));
    } else {
      reservations.setAdultcount(Constants.COUNT);
    }
    final Customer customer =
        customerService.getCustomerByMobileNumberAndByOrgIdBranchId(
            mobileNumber, orgId, branch.getId());
    if (customer != null) {
      reservations.setCustomer(customer);
      producerMap.put("PERSON_NAME", customer.getCustomerName());
      producerMap.put("CustomerStatus", "Customer exists");
    } else {
      producerMap.put("CustomerStatus", "Customer doesn't exist");
      final Customer customer1 = new Customer();
      customer1.setMobileNumber(reservation.getString("mobileNumber"));
      customer1.setCustomerName(reservation.getString("customerName"));
      customer1.setBranch(branch);
      customer1.setOrganisation(employeeDetails.getOrganisation());
      customer1.setSocialProvider(AuthProvider.local);
      customerService.save(customer1);
      reservations.setCustomer(customer1);
      producerMap.put("PERSON_NAME", "Customer");
    }

    try {
      if (reservation.has(Constants.RESERVETABLE)) {
        final String reserveTable = reservation.getString("reserveTable");
        if (reserveTable.equals(" ")) {

          reservations.setReserveTable(null);

        } else {

          final Reservation exitreservations =
              reservationService.getReservationDetailsBySlotTimeBranch(
                  date, starttime, Long.parseLong(reserveTable), orgId, branch.getId());
          if (exitreservations != null) {
            if (exitreservations.getStatus().equalsIgnoreCase("Open")
                || exitreservations.getStatus().equalsIgnoreCase("allotted")
                || exitreservations.getStatus().equalsIgnoreCase("Waiting")) {

              producerMap.put(
                  "ReservationStatus",
                  "Reservation  already exists for the time " + df.format(startTime) + "!");
            } else {
              reservations.setStartTime(starttime);
              reservations.setReserveTable(
                  counterService.getCounterDetailsById(Long.parseLong(reserveTable)));
              reservations.setReserveDate(date);
              reservations.setKidscount(kidsCount);
              reservations.setAdultcount(Long.parseLong(adultCount));
              reservations.setStatus(Constants.OPEN_STATUS);
              reservations.setCreatedDate(new Date());
              reservations.setModifiedDate(new Date());
              reservations.setOrganisation(employeeDetails.getOrganisation());
              reservations.setBranch(branch);
              reservations.setReservationType(reserveType);
              reservations.setNote(note);
              reservationService.save(reservations);
              // to send kafka producer and topic is mail
              producerMap.put("ReservationStatus", Constants.MESSAGE);
              producerMap.put(Constants.TEMPLATE, Constants.RESERVATION);
              producerMap.put(Constants.ORGID, employeeDetails.getOrganisation().getId());
              producerMap.put(Constants.ORGNAME, employeeDetails.getOrganisation().getOrgName());
              producerMap.put(Constants.BRANCHID, branch.getId());
              producerMap.put(Constants.BRANCHNAME, branch.getBranchName());
              producerMap.put("mobileNumber", mobileNumber);
              producerMap.put("customerName", customerName);
              producerMap.put("reserveTable", reserveTable);
              producerMap.put("date", sdf.format(date));
              producerMap.put(Constants.ADULTCOUNT1, adultCount);
              producerMap.put("kidsCount", kidsCount);
              producerMap.put("startTime", starttime);
              producerMap.put(Constants.STATUS, Constants.OPENSTATUS);
              producerMap.put(Constants.CREATEDDATE, sdf.format(new Date()));
              producerMap.put(Constants.MODIFIEDDATE, sdf.format(new Date()));
              producerMap.put("reservationType", reserveType);
              producerMap.put("note", note);
              final String kafkadata = objMapper.writeValueAsString(producerMap);
              kafkaSender.sendMail(kafkadata);
            }
          } else {

            reservations.setReserveTable(
                counterService.getCounterDetailsById(Long.parseLong(reserveTable)));
            reservations.setStartTime(starttime);
            reservations.setReserveDate(date);
            reservations.setKidscount(kidsCount);
            reservations.setAdultcount(Long.parseLong(adultCount));
            reservations.setStatus(Constants.OPEN_STATUS);
            reservations.setCreatedDate(new Date());
            reservations.setModifiedDate(new Date());
            reservations.setOrganisation(employeeDetails.getOrganisation());
            reservations.setBranch(branch);
            reservations.setReservationType(reserveType);
            reservations.setNote(note);
            reservationService.save(reservations);
            // to send kafka producer and topic is mail
            producerMap.put("ReservationStatus", "Reservation is successful!!");
            producerMap.put("template", "reservation");
            producerMap.put("orgId", employeeDetails.getOrganisation().getId());
            producerMap.put("ORG_NAME", employeeDetails.getOrganisation().getOrgName());
            producerMap.put("branchId", branch.getId());
            producerMap.put("BRANCH_NAME", branch.getBranchName());
            producerMap.put("mobileNumber", mobileNumber);
            producerMap.put("customerName", customerName);
            producerMap.put("reserveTable", reserveTable);
            producerMap.put("date", sdf.format(date));
            producerMap.put("AdultCount", adultCount);
            producerMap.put("kidsCount", kidsCount);
            producerMap.put("startTime", starttime);
            producerMap.put("status", "OPEN_STATUS");
            producerMap.put("createdDate", sdf.format(new Date()));
            producerMap.put("modifiedDate", sdf.format(new Date()));
            producerMap.put("reservationType", reserveType);
            producerMap.put("note", note);
            final String kafkadata = objMapper.writeValueAsString(producerMap);
            kafkaSender.sendMail(kafkadata);
          }
        }
      } else {

        reservations.setStartTime(starttime);
        reservations.setReserveDate(date);
        reservations.setKidscount(kidsCount);
        reservations.setAdultcount(Long.parseLong(adultCount));
        reservations.setStatus(Constants.OPEN_STATUS);
        reservations.setCreatedDate(new Date());
        reservations.setModifiedDate(new Date());
        reservations.setOrganisation(employeeDetails.getOrganisation());
        reservations.setBranch(branch);
        reservations.setReservationType(reserveType);
        reservations.setNote(note);
        reservationService.save(reservations);
        // to send kafka producer and topic is mail
        producerMap.put("ReservationStatus", "Reservation is successful!!");
        producerMap.put("template", "reservation");
        producerMap.put("orgId", employeeDetails.getOrganisation().getId());
        producerMap.put("ORG_NAME", employeeDetails.getOrganisation().getOrgName());
        producerMap.put("branchId", branch.getId());
        producerMap.put("BRANCH_NAME", branch.getBranchName());
        producerMap.put("mobileNumber", mobileNumber);
        producerMap.put("customerName", customerName);
        producerMap.put("reserveTable", null);
        producerMap.put("date", sdf.format(date));
        producerMap.put("AdultCount", adultCount);
        producerMap.put("kidsCount", kidsCount);
        producerMap.put("startTime", starttime);
        producerMap.put("status", "OPEN_STATUS");
        producerMap.put("createdDate", sdf.format(new Date()));
        producerMap.put("modifiedDate", sdf.format(new Date()));
        producerMap.put("reservationType", reserveType);
        producerMap.put("note", note);
        final String kafkadata = objMapper.writeValueAsString(producerMap);
        kafkaSender.sendMail(kafkadata);
      }

    } catch (final RuntimeException ex) {
      LOGGER.info("reservation save" + ex, ex);
    }

    return producerMap;
  }

  @PostMapping(path = RouteConstants.UPDATE_RESERVATION_DETAILS)
  public final Map<String, String> updateReservationDetails(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException, IOException, ParseException {

    final JSONObject reservation = new JSONObject(reservationDetails);
    final Long id = reservation.getLong("id");
    final String key = reservation.getString("key");
    final String value = reservation.getString("value");

    final Map<String, String> reservationUpdate = new HashMap<>();
    try {
      final Reservation reservations = reservationService.getReservationDetailsByReservationid(id);
      if (key.equals("allocatedTable")) {
        reservations.setAllocatedTable(Integer.parseInt(value));
        reservations.setStatus(Constants.CLOSESTATUS);
        reservations.setModifiedDate(new Date(System.currentTimeMillis()));
        reservationUpdate.put("reservationUpdate", "reservationDetails updated for this id  " + id);

      } else {

        reservationUpdate.put("reservationUpdate", "key Name does not match");
      }
      reservationService.update(reservations);

    } catch (final RuntimeException e) {
      LOGGER.error("reservationController update" + e, e);
    }
    return reservationUpdate;
  }

  @GetMapping(path = RouteConstants.GET_RESERVE_DETAILS_BY_ORGBRANCH)
  public final ResponseEntity<List<Reservation>> getReservationDetailsByOrgAndBranch(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Reservation> reservation = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          reservation = reservationService.getReservationDetailsByOrg(orgId);
        } else {
          reservation = reservationService.getReservationDetailsByOrgAndBranch(orgId, branchId);
        }
      } else {
        return new ResponseEntity<>(reservation, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.info("getReservationDetails " + e, e);
    }
    return new ResponseEntity<>(reservation, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_ALL_RESERVATIONS_FROM_CURRENT_DATE)
  public final ResponseEntity<List<Reservation>> getAllReservationsFromCurrentDate(
      final HttpServletRequest request, final HttpServletResponse response) {
    final SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    List<Reservation> reservations = null;
    final Date date = new Date();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        reservations =
            reservationService.getAllReservationsFromCurrentDate(
                orgId, branchId, date, sdfTime.format(date));
      } else {
        return new ResponseEntity<>(reservations, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception em) {
      LOGGER.info("getAllReservationsFromCurrentDate" + em, em);
    }
    return new ResponseEntity<>(reservations, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.ALLOT_RESERVE_DETAILS_BY_ID)
  public final Map<String, String> allotReservationDetailsById(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException {

    final JSONObject reservation = new JSONObject(reservationDetails);
    final Long id = reservation.getLong("allotedid");
    final Map<String, String> reservationUpdate = new HashMap<>();
    try {
      final Reservation reservations = reservationService.getReservationDetailsByReservationid(id);
      reservations.setStatus("allotted");
      reservationService.update(reservations);
      reservationUpdate.put(
          "reservationUpdate",
          reservations.getReserveTable() + " Table ReservationDetails Allotted Successfully");
    } catch (final Exception em) {
      LOGGER.info("allotReservationDetailsById" + em, em);
    }
    return reservationUpdate;
  }

  @PostMapping(path = RouteConstants.UPDATE_RESERVE_DETAILS_BY_ID)
  public final Map<String, String> updateReservationDetailsByid(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException, IOException, ParseException {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final JSONObject reservation = new JSONObject(reservationDetails);
    final Long id = reservation.getLong("id");
    final Map<String, Object> producerMap = new HashMap<>();
    final ObjectMapper objMapper = new ObjectMapper();
    final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    final Map<String, String> reservationUpdate = new HashMap<>();
    try {
      final Reservation reservations = reservationService.getReservationDetailsByReservationid(id);
      if (reservations != null) {
        reservations.setStatus("Cancelled");
        reservationService.update(reservations);

        producerMap.put("template", "cancellation");
        producerMap.put("orgId", reservations.getOrganisation().getId());
        producerMap.put("ORG_NAME", employeeDetails.getOrganisation().getOrgName());
        if (reservations.getBranch() == null) {
          producerMap.put("branchId", "null");
          producerMap.put("BRANCH_NAME", "null");
        } else {
          producerMap.put("branchId", reservations.getBranch().getId());
          producerMap.put("BRANCH_NAME", reservations.getBranch().getBranchName());
        }
        producerMap.put("mobileNumber", reservations.getCustomer().getMobileNumber());
        producerMap.put("reserveTable", "");
        producerMap.put("PERSON_NAME", reservations.getCustomer().getCustomerName());
        producerMap.put("date", sdf.format(reservations.getReserveDate()));
        producerMap.put("AdultCount", reservations.getAdultcount());
        producerMap.put("kidsCount", reservations.getKidscount());
        producerMap.put("startTime", reservations.getStartTime());
        producerMap.put("status", "CLOSE_STATUS");
        producerMap.put("createdDate", sdf.format(reservations.getCreatedDate()));
        producerMap.put("modifiedDate", sdf.format(reservations.getModifiedDate()));
        producerMap.put("reservationType", reservations.getReservationType());
        producerMap.put("note", reservations.getNote());
        final String kafkadata = objMapper.writeValueAsString(producerMap);

        kafkaSender.sendMail(kafkadata);
        reservationUpdate.put(
            "reservationUpdate",
            reservations.getReserveTable() + " Table ReservationDetails Canceled Successfully");
      }
    } catch (final RuntimeException e) {
      LOGGER.error("reservationController update" + e, e);
    }
    return reservationUpdate;
  }

  @GetMapping(path = RouteConstants.GET_COUNTERS_BY_TIME)
  public final List<Map<String, Object>> getCountersByTime(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException, IOException, ParseException {
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final Date date = new Date();
    final long orgId = employeeDetails.getOrganisation().getId();
    final String branch = request.getParameter("branchId");
    long branchId = employeeDetails.getBranch().getId();
    if (branch != null) {
      branchId = Long.parseLong(branch);
    }
    final List<Map<String, Object>> data =
        reservationService.getAllReservationsByDate(date, orgId, branchId);

    return data;
  }

  @GetMapping(path = RouteConstants.GET_RESERVE_COUNT_BY_CURRENT_MONTH)
  public final ResponseEntity<Map<String, BigInteger>> getReservationCountByCurrentMonth(
      final HttpServletRequest request, final HttpServletResponse response) {
    final Map<String, BigInteger> map = new HashMap<>();
    final Identifier identifier = (Identifier) request.getAttribute("identifier");
    if (identifier != null) {
      final long orgId = identifier.getOrgId();
      final long branchId = identifier.getBranchId();
      if (branchId == 0) {
        map.put(
            "ReservationCount",
            reservationService.getCountOfReservationByOrganisationIdAndCurrentMonth(orgId));
      } else {
        map.put(
            "ReservationCount",
            reservationService.getCountOfReservationByOrganisationIdAndCurrentMonthBranchId(
                orgId, branchId));
      }
    } else {
      return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
    }
    return new ResponseEntity<>(map, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.UPDATE_RESERVE_DETAILS_BY_WAITING)
  public final Map<String, String> updateReservationDetailsBywaiting(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException, IOException, ParseException {

    final JSONObject reservation = new JSONObject(reservationDetails);
    final Long id = reservation.getLong("waitingid");

    final Map<String, String> reservationUpdate = new HashMap<>();
    try {
      final Reservation reservations = reservationService.getReservationDetailsByReservationid(id);
      reservations.setStatus(Constants.WAITING_STATUS);
      reservationService.update(reservations);

      reservationUpdate.put("reservationUpdate", "reservationDetails waiting");
    } catch (final RuntimeException e) {
      LOGGER.error("reservationController update" + e, e);
    }
    return reservationUpdate;
  }

  @PostMapping(path = RouteConstants.UPDATE_RESERVE_DETAILS_BY_ALLOTED)
  public final Map<String, String> updateReservationDetailsByallotted(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException, IOException, ParseException {

    final JSONObject reservation = new JSONObject(reservationDetails);
    final Long id = reservation.getLong("allottedid");

    final Map<String, String> reservationUpdate = new HashMap<>();
    try {
      final Reservation reservations = reservationService.getReservationDetailsByReservationid(id);
      reservations.setStatus(Constants.ALLOTTED_STATUS);
      reservationService.update(reservations);

      reservationUpdate.put("reservationUpdate", "reservationDetails are allotted");
    } catch (final RuntimeException e) {
      LOGGER.error("reservationController update" + e, e);
    }
    return reservationUpdate;
  }
}
