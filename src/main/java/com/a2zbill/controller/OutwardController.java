package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.B2B;
import com.a2zbill.domain.B2C;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.GSTR1;
import com.a2zbill.domain.Invoice;
import com.a2zbill.domain.ItemDetail;
import com.a2zbill.domain.Items;
import com.a2zbill.domain.Outward;
import com.a2zbill.services.CartProductService;
import com.a2zbill.services.OutwardService;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OutwardController {

  private static final Logger LOGGER = LoggerFactory.getLogger(OutwardController.class);

  @Autowired private CartProductService cartProductService;

  @Autowired private OutwardService outwardService;

  @Value("${outward.form}")
  private String gstrOutwardForm;

  @GetMapping(path = RouteConstants.GET_OUTWARD_DETAILS)
  public final List<Outward> getOutwardDetails(
      final HttpServletRequest request, final HttpServletResponse responce) throws JSONException {

    List<Outward> outward = null;

    try {
      outward = outwardService.getAllOutwardDetails();
      outwardPdf(outward);
    } catch (final Exception e) {
      LOGGER.info("getOutwardDetails" + e, e);
    }

    return outward;
  }

  @GetMapping(path = RouteConstants.GET_INWARD_DETAILS_BY_MONTHYEAR)
  public final GSTR1 getInwardDetailsbymonthandyear(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final List<B2B> listb2b = new ArrayList<>();
    final List<B2C> listb2c = new ArrayList<>();

    final List<Invoice> listInvoice = new ArrayList<>();
    List<Outward> outwardDetails = null;
    List<CartDetail> cartProduct = null;

    try {
      final String month = request.getParameter("month");
      final String year = request.getParameter("year");
      outwardDetails = outwardService.getOutwardDetailsByMonthAndYearAndGSTNumber(month, year);

      for (final Outward outwards : outwardDetails) {

        cartProduct =
            cartProductService.getCartDetailsByCartId(
                outwards.getBillingDetail().getCartId().getCartId());
        final List<Items> listItems = new ArrayList<>();
        int num = 1;
        for (final CartDetail cartDetail : cartProduct) {

          final Items items = new Items();
          items.setNum(num);

          final ItemDetail itemDetails = new ItemDetail();
          final BigDecimal camt =
              cartDetail
                  .getProductPrice()
                  .multiply(new BigDecimal(cartDetail.getQuantity()))
                  .multiply(cartDetail.getCgstPercentage())
                  .divide(Constants.HUNDRED);
          final BigDecimal samt =
              cartDetail
                  .getProductPrice()
                  .multiply(new BigDecimal(cartDetail.getQuantity()))
                  .multiply(cartDetail.getSgstPercentage())
                  .divide(Constants.HUNDRED);
          itemDetails.setCamt(camt.doubleValue());
          itemDetails.setSamt(samt.doubleValue());
          itemDetails.setRt(
              cartDetail.getCgstPercentage().multiply(cartDetail.getSgstPercentage()).longValue());
          itemDetails.setCsamt(0.0);
          itemDetails.setTaxval(camt.add(samt).doubleValue());

          items.setItm_det(itemDetails);
          listItems.add(items);

          num++;
        }

        final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final Date date = format.parse(outwards.getOutwardDate());
        final Invoice invoice = new Invoice();
        invoice.setInum(outwards.getBillingDetail().getId());
        invoice.setIdt(date);
        invoice.setVal(outwards.getBillingDetail().getTotalAmount().longValue());
        invoice.setPos(29);
        invoice.setRchrg("N");
        invoice.setItemsDetails(listItems);
        listInvoice.add(invoice);

        if (outwards.getCustomerGSTNumber().equals("null")) {
          final B2C b2c = new B2C();
          b2c.setCtin(outwards.getCustomerGSTNumber());
          b2c.setInv(invoice);
          listb2c.add(b2c);
        } else {
          final B2B b2b = new B2B();
          b2b.setCtin(outwards.getCustomerGSTNumber());
          b2b.setInv(invoice);
          listb2b.add(b2b);
        }
      }

      final GSTR1 gstr1 = new GSTR1();
      gstr1.setGstin(outwardDetails.get(0).getGstNumber());
      gstr1.setFp(month.concat(year));
      gstr1.setGt("0");
      gstr1.setCurGt("0");
      gstr1.setB2b(listb2b);
      gstr1.setB2c(listb2c);

      return gstr1;
    } catch (final Exception e) {
      LOGGER.info("getOutwardDetail " + e, e);
    }
    return null;
  }

  public void outwardPdf(final List<Outward> outwardDetails)
      throws FileNotFoundException, DocumentException {

    final Document document = new Document();
    document.open();

    final Font graycolour = new Font(FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.DARK_GRAY);
    final Font dark = new Font(FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.DARK_GRAY);
    final Font black = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);

    final Paragraph para1 = new Paragraph();
    final Chunk redText1 =
        new Chunk("Government of India/State Department of TSSS INFOTECH", graycolour);
    para1.add(redText1);
    para1.setAlignment(Element.ALIGN_CENTER);
    para1.getSpacingBefore();
    document.add(para1);
    document.add(new Paragraph("\n\n"));

    final Paragraph para2 = new Paragraph();
    final Chunk redText2 = new Chunk("FORM GSTR-1", graycolour);
    para2.add(redText2);
    para2.setAlignment(Element.ALIGN_CENTER);
    para2.getSpacingBefore();
    document.add(para2);
    document.add(new Paragraph("\n\n"));

    final Paragraph para3 = new Paragraph();
    final Chunk redText3 = new Chunk("DETAILS OF OUTWARD SUPPLIES ", graycolour);
    redText3.setBackground(BaseColor.GREEN);
    para3.add(redText3);
    para3.setAlignment(Element.ALIGN_CENTER);
    para3.getSpacingBefore();
    document.add(para3);

    document.add(new Paragraph("\n\n"));

    final Paragraph para4 = new Paragraph();
    final Chunk redText4 = new Chunk("1.	GSTIN	:	.......... ", black);
    para4.add(redText4);
    para4.setAlignment(Element.ALIGN_LEFT);
    para4.getSpacingBefore();
    document.add(para4);

    final Paragraph para5 = new Paragraph();
    final Chunk redText5 = new Chunk("2.	Name of the Taxable Person:	:	.......... ", black);
    para5.add(redText5);
    para5.setAlignment(Element.ALIGN_LEFT);
    para5.getSpacingBefore();
    document.add(para5);

    final Paragraph para6 = new Paragraph();
    final Chunk redText6 =
        new Chunk(
            "3.	Aggregate Turnover of the Taxable Person in the previous FY	:	.......... ", black);
    para6.add(redText6);
    para6.setAlignment(Element.ALIGN_LEFT);
    para6.getSpacingBefore();
    document.add(para6);

    final Paragraph para7 = new Paragraph();
    final Chunk redText7 = new Chunk("4.	Period	:	 ", black);
    final Chunk redText8 = new Chunk("Month........	:	", black);
    final Chunk redText9 = new Chunk("Year......	:	", black);
    para7.add(redText7);
    para7.add(redText8);
    para7.add(redText9);
    para7.setAlignment(Element.ALIGN_LEFT);
    para7.getSpacingBefore();
    document.add(para7);

    document.add(new Paragraph("\n\n"));

    final PdfPTable table = new PdfPTable(6);
    table.setHorizontalAlignment(Element.ALIGN_LEFT);

    PdfPCell hcell;
    hcell = new PdfPCell(new Phrase("GSTIN/UIN", dark));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("Invoice", dark));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("IGST", dark));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("CGST", dark));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("SGST", dark));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("POS", dark));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    document.add(table);

    document.close();
  }

  @GetMapping(path = RouteConstants.GET_OUTWARD_DETAILS_BY_MONTHYEAR)
  public final ResponseEntity<Object> getoutwardDetailsbymonthandyear(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final String month = request.getParameter("month");
    final String year = request.getParameter("year");

    List<Outward> outwardDetails = new ArrayList<>();

    try {
      outwardDetails = outwardService.getOutwardDetailsByMonthAndYear(month, year);
    } catch (final Exception e) {
      LOGGER.error("getOutwardDetail " + e, e);
    }
    return new ResponseEntity<>(outwardDetails, HttpStatus.OK);
  }
}
