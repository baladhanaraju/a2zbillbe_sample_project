package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.StoreType;
import com.a2zbill.services.StoreTypeService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StoreTypeController {

  private static final Logger LOGGER = LoggerFactory.getLogger(StoreTypeController.class);

  @Autowired private StoreTypeService storeTypeService;

  @GetMapping(path = RouteConstants.STORE_TYPE_LIST)
  public final ResponseEntity<Object> getAllStoreTypeDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<StoreType> storeType = new ArrayList<StoreType>();
    try {
      storeType = this.storeTypeService.getAllStoreTypeDetails();
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getStoreTypeDetails " + ex);
    }
    return new ResponseEntity<>(storeType, HttpStatus.OK);
  }
}
