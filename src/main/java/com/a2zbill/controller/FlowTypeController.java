package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.FlowType;
import com.a2zbill.services.FlowTypeService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlowTypeController {

  private static final Logger LOGGER = LoggerFactory.getLogger(FlowTypeController.class);

  @Autowired private FlowTypeService flowTypeService;

  @PostMapping(path = RouteConstants.ADD_FLOW_TYPE_DETAILS)
  public final Map<String, Object> addFlowTypeDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final Map<String, Object> map = new HashMap<String, Object>();

    try {
      final JSONObject floorType = new JSONObject(payload);

      if (floorType.has("flowTypeName")) {
        final FlowType floorTypeDetails = new FlowType();
        final String floorTypeName = floorType.getString("flowTypeName");
        floorTypeDetails.setTypeName(floorTypeName);
        this.flowTypeService.save(floorTypeDetails);
        map.put("message", floorTypeDetails);
      }

    } catch (final Exception ex) {
      LOGGER.error("addFloorTypeDetails" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_FLOW_TYPE_DETAILS)
  public final ResponseEntity<Object> getFlowTypeDetails() {

    List<FlowType> floorTypeDetails = new ArrayList<FlowType>();

    try {
      floorTypeDetails = this.flowTypeService.getAllFlowTypeDetails();

    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getFloorTypeDetails " + ex);
    }
    return new ResponseEntity<>(floorTypeDetails, HttpStatus.OK);
  }
}
