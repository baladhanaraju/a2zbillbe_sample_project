package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.CustomerRedeemProduct;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductRewardPoint;
import com.a2zbill.services.CustomerRedeemProductService;
import com.a2zbill.services.ProductRewardPointsService;
import com.a2zbill.services.ProductService;
import com.offers.domain.CustomerTransactionSummary;
import com.offers.domain.RewardHistory;
import com.offers.services.CustomerTransactionSummaryService;
import com.offers.services.RewardHistoryService;
import com.offers.services.RewardService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerRedeemProductController {
  private static final Logger LOGGER =
      LoggerFactory.getLogger(CustomerRedeemProductController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private CustomerRedeemProductService customerRedeemProductService;

  @Autowired private CustomerTransactionSummaryService customerTransactionSummaryService;

  @Autowired private ProductRewardPointsService productRewardPointsService;

  @Autowired private ProductService productsService;

  @Autowired private CustomerService customerService;

  @Autowired private RewardService rewardService;

  @Autowired private RewardHistoryService rewardHistoryService;

  @PostMapping(path = RouteConstants.SAVE_CUSTOMER_REDEEM_PRODUCT)
  public final void addCustproducts(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload) {
    try {
      final JSONObject ja = new JSONObject(payload);
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long customerId = ja.getLong("customerId");
      final long productId = ja.getLong("productId");
      final long orgId = employeeDetails.getOrganisation().getId();
      final CustomerRedeemProduct customerRedeemProduct = new CustomerRedeemProduct();
      final Branch branch = employeeDetails.getBranch();
      ProductRewardPoint productRewardPoint = null;
      CustomerRedeemProduct existCustomerRedeemProduct = null;
      CustomerTransactionSummary allCustomerTransactionSummary = null;
      Product product = null;
      if (branch.getId() != 0) {
        product =
            this.productsService.getProductsByProductIdAndOrgIdAndBranchId(
                productId, orgId, branch.getId());
      } else {
        product = this.productsService.getProductsByProductIdAndOrgId(productId, orgId);
      }
      final Customer customer = this.customerService.getCustomerDetailsBycustomerId(customerId);
      if (branch.getId() != 0) {
        existCustomerRedeemProduct = new CustomerRedeemProduct();
        existCustomerRedeemProduct =
            this.customerRedeemProductService.getCustomerRedeemProductByproductIdOrgIdAndBranchId(
                productId, orgId, branch.getId());
        existCustomerRedeemProduct.setCustomerId(customer);
      } else {
        existCustomerRedeemProduct =
            this.customerRedeemProductService.getCustomerRedeemProductByproductIdOrgId(
                productId, orgId);
      }
      if (existCustomerRedeemProduct != null) {
        if (branch.getId() != 0) {
          productRewardPoint = new ProductRewardPoint();
          productRewardPoint =
              this.productRewardPointsService.getAllProductRewardPointsByproductId(
                  productId, orgId, branch.getId());
        } else {
          productRewardPoint =
              this.productRewardPointsService.getAllProductRewardPointsByproductIdOrgId(
                  productId, orgId);
        }
        if (productRewardPoint != null) {
          customerRedeemProduct.setRedeemPoint(
              productRewardPoint.getRewardPonts() + existCustomerRedeemProduct.getRedeemPoint());
        } else {
          customerRedeemProduct.setRedeemPoint(existCustomerRedeemProduct.getRedeemPoint());
        }
        this.customerRedeemProductService.update(existCustomerRedeemProduct);
      } else {
        customerRedeemProduct.setProductId(product);
        customerRedeemProduct.setCustomerId(customer);
        customerRedeemProduct.setOrgId(employeeDetails.getOrganisation());
        customerRedeemProduct.setBranchId(employeeDetails.getBranch());
        if (branch.getId() != 0) {
          productRewardPoint =
              this.productRewardPointsService.getAllProductRewardPointsByproductId(
                  productId, orgId, branch.getId());
        } else {
          productRewardPoint =
              this.productRewardPointsService.getAllProductRewardPointsByproductIdOrgId(
                  productId, orgId);
        }
        if (productRewardPoint != null) {
          customerRedeemProduct.setRedeemPoint(productRewardPoint.getRewardPonts());
        } else {
          customerRedeemProduct.setRedeemPoint(0);
        }
        this.customerRedeemProductService.save(customerRedeemProduct);
      }
      if (branch.getId() != 0) {
        allCustomerTransactionSummary = new CustomerTransactionSummary();
        allCustomerTransactionSummary =
            this.customerTransactionSummaryService.getCustomerTransactionSummaryByCustomerId(
                customerId, orgId, branch.getId());
      } else {
        allCustomerTransactionSummary =
            this.customerTransactionSummaryService.getCustomerTransactionSummaryByCustomerIdOrgId(
                customerId, orgId);
      }
      final long rewardPoints =
          allCustomerTransactionSummary.getRewardPoints() - productRewardPoint.getRewardPonts();
      allCustomerTransactionSummary.setRewardPoints(rewardPoints);
      this.customerTransactionSummaryService.update(allCustomerTransactionSummary);

      RewardHistory rewardHistory = new RewardHistory();
      rewardHistory.setCustomer(customer);
      rewardHistory.setDate(new Date());
      rewardHistory.setRewardPoints(0);
      rewardHistory.setStartTime(new Date());
      try {
        final long rpu =
            (long)
                rewardService
                    .getRewardByOrgAndBranchAndstatus(orgId, customer.getBranch().getId(), true)
                    .getRewardPerUnit();
        rewardHistory.setRedeemAmount(productRewardPoint.getRewardPonts() / rpu);
        rewardHistoryService.save(rewardHistory);
      } catch (Exception e) {
        LOGGER.error("rewardHistory " + e);
      }
    } catch (final Exception ex) {
      LOGGER.error("savecustomeproducts" + ex);
    }
  }

  @GetMapping(path = RouteConstants.GET_CUSTOMER_REDEEM_ORODUCTS)
  public final ResponseEntity<Object> getCustomerRedeemproducts(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<CustomerRedeemProduct> customerRedeemProduct = new ArrayList<>();
    try {
      customerRedeemProduct = this.customerRedeemProductService.getAllCustomerRedeemProducts();

    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("CustomerRedeemProduct" + ex);
    }
    return new ResponseEntity<>(customerRedeemProduct, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_CUSTOMER_REDEEM_CUSTOMERID)
  public final List<CustomerRedeemProduct> getCustomerRedeemCustomersId(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<CustomerRedeemProduct> customerRedeemProduct = null;
    try {
      final String customerid = request.getParameter("customerId");

      if (customerid != null && !customerid.isEmpty()) {

        customerRedeemProduct =
            this.customerRedeemProductService.getCustomerRedeemProductBycustomerId(
                Long.parseLong(customerid));
      }
    } catch (final Exception ex) {
      LOGGER.error("getCustRedeemProduct" + ex);
    }
    return customerRedeemProduct;
  }

  @GetMapping(path = RouteConstants.CUSTOMER_REDEEM_CUST_TRANCTION_CUSTID)
  public final ResponseEntity<Object> getCustomerRedeemproductsId(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final Map<String, Object> map = new HashMap<>();

    try {
      final long customerId = Long.parseLong(request.getParameter("customerId"));
      List<CustomerRedeemProduct> customerRedeemProduct = null;
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        final CustomerTransactionSummary customerTransactionSummary =
            this.customerTransactionSummaryService.getCustomerTransactionSummaryByCustomerId(
                customerId, orgId, branchId);
        customerRedeemProduct =
            this.customerRedeemProductService
                .getCustomerRedeemProductBycustomerIdByOrgIdAndBranchId(
                    customerId, orgId, branchId);
        map.put("CustomerId", customerTransactionSummary.getCustomer());
        map.put("RewardPoints", customerTransactionSummary.getRewardPoints());
        map.put("LifeTimeTotalAmount", customerTransactionSummary.getLifetimeTotalAmount());
        map.put("LifeTimeTaxAmount", customerTransactionSummary.getLifetimeTaxAmount());
        map.put("LifeTimeBillingAmount", customerTransactionSummary.getLifetimeBillingAmount());
        map.put("LifeTimeVisits", customerTransactionSummary.getLifetimeVisits());
        map.put("LifeTimeDiscountAmount", customerTransactionSummary.getLifetimeDiscountAmount());
        map.put("LifeTimeRewardPoints", customerTransactionSummary.getLifetimeRewardPoints());
        map.put("Product", customerRedeemProduct);
      } else {
        return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      map.put("msg", "Customer Not Existed for this Organisation and Branch");
      LOGGER.error("customerId" + ex);
    }
    return new ResponseEntity<>(map, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.CUSTOMER_REDEEM_PRODUCTID)
  public final ResponseEntity<Object> getCustomerRedeemproductId(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Object[]> customerRedeemProduct = null;
    final List<Map<String, Object>> productslist = new ArrayList<Map<String, Object>>();

    try {
      final String productid = request.getParameter("productId");
      if (productid != null && !productid.isEmpty()) {
        customerRedeemProduct =
            this.customerRedeemProductService.getCustomerRedeemProductById(
                Long.parseLong(productid));
        for (int i = 0; i < customerRedeemProduct.size(); i++) {
          Map<String, Object> mapProducts = new HashMap<String, Object>();
          mapProducts.put("productId", customerRedeemProduct.get(i)[0]);
          mapProducts.put("productName", customerRedeemProduct.get(i)[1]);

          mapProducts.put("productCode", customerRedeemProduct.get(i)[2]);
          productslist.add(mapProducts);
        }
      }
    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getCustomerRedeem" + ex);
    }
    return new ResponseEntity<>(productslist, HttpStatus.OK);
  }
}
