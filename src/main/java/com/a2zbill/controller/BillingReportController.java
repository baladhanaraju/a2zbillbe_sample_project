package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingPayments;
import com.a2zbill.domain.BillingReport;
import com.a2zbill.services.BillingPaymentsService;
import com.a2zbill.services.BillingReportService;
import com.a2zbill.services.BillingService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BillingReportController {

  private static final Logger LOGGER = LoggerFactory.getLogger(BillingReportController.class);

  @Autowired private BillingReportService billingReportService;

  @Autowired private BillingService billingService;

  @Autowired private BillingPaymentsService billingPaymentsService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @PostMapping(path = RouteConstants.GET_BILLING_REPORT)
  public final void getBillingreport(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload) {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final BillingReport billingreport = new BillingReport();
    final BigDecimal openingbalance = new BigDecimal(request.getParameter("openingbalane"));
    final BigDecimal closingbalance = new BigDecimal(request.getParameter("closingbalane"));
    try {
      final BillingReport reportDetails =
          this.billingReportService.getBillingReportByEmpid(employeeDetails.getId(), new Date());
      if (reportDetails != null) {
        reportDetails.setClosing_balance(closingbalance);
        this.billingReportService.update(reportDetails);
      } else {
        billingreport.setOpening_balance(openingbalance);
        billingreport.setClosing_balance(closingbalance);
        billingreport.setEmployee(employeeDetails);
        billingreport.setDate(new Date());
        billingreport.setCreatedTime(new Date(System.currentTimeMillis()));
        billingreport.setModifiedTime(new Date(System.currentTimeMillis()));
        this.billingReportService.save(billingreport);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getBillingreport", ex);
    }
  }

  @GetMapping(path = RouteConstants.GET_BILLING_REPORT_DETAILS)
  public final ResponseEntity<Object> getBillingReportDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<BillingReport> billingreport = new ArrayList<>();
    try {
      billingreport = this.billingReportService.getAllBillingReport();
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getBillingreportDetail " + ex);
    }
    return new ResponseEntity<>(billingreport, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_BILLING_DETAILS_BY_DATE)
  public final List<Billing> getBillingDetailsByDate(
      final HttpServletRequest request, final HttpServletResponse responce) throws ParseException {
    final long orgId = this.jwtTokenUtil.getOrgIdFromToken(request);
    final String fromDate = request.getParameter("fromDate");
    final String endDate = request.getParameter("toDate");
    final String branchId = request.getParameter("branchid");
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date fromDate1 = new Date();
    Date endDate1 = new Date();
    List<Billing> billing = null;
    try {
      if (endDate != null && branchId != null) {
        fromDate1 = sdf.parse(fromDate);
        endDate1 = sdf.parse(endDate);
        billing =
            this.billingService.getBillingsByStartAndEndDateAndOrgAndBranch(
                fromDate1, endDate1, orgId, Long.parseLong(branchId));
      }
      if (endDate == null && branchId != null) {
        fromDate1 = sdf.parse(fromDate);
        billing =
            this.billingService.getAllBillingDetailsByDateAndBrachIdAndOrgId(
                fromDate1, Long.parseLong(branchId), orgId);
      }
      if (endDate != null && branchId == null) {
        fromDate1 = sdf.parse(fromDate);
        endDate1 = sdf.parse(endDate);
        billing = this.billingService.getBillingsByCurrentDateAndOrgId(fromDate1, endDate1, orgId);
      }
      if (endDate == null && branchId == null) {
        fromDate1 = sdf.parse(fromDate);
        billing = this.billingService.getBillingsByOrgIdByFromdate(fromDate1, orgId);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getBillingDetailsByDate " + ex);
    }
    return billing;
  }

  @GetMapping(path = RouteConstants.GET_BILLING_DETAILS_BY_REPORT)
  public final Map<String, Object> getBillingDetailsByReport(
      final HttpServletRequest request, final HttpServletResponse responce) throws ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final String date = request.getParameter("date");
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date dates = sdf.parse(date);
    List<Billing> billing = null;
    final Map<String, Object> map = new HashMap<>();
    final long orgId = employeeDetails.getOrganisation().getId();
    try {
      final Branch branchId = employeeDetails.getBranch();
      if (branchId == null) {
        billing = this.billingService.getBillingsByOrgIdByFromdate(dates, orgId);
        map.put("BillingDetails", billing);
        return map;
      } else {
        billing =
            this.billingService.getAllBillingDetailsByDateAndBrachIdAndOrgId(
                dates, employeeDetails.getBranch().getId(), orgId);
        map.put("BillingDetails", billing);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getBillingDetailsByReport " + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_BILLING_DETAILS_BY_CUSTOMER_ID)
  public final Map<String, Object> getBillingDetailsByCustomerId(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final long custId = Long.parseLong(request.getParameter("customerId"));
    final Map<String, Object> map = new HashMap<>();
    List<Billing> billing = null;
    List<Billing> billing1 = null;
    try {
      billing = this.billingService.getBillingDetailsByCustomerId(custId);
      if (billing.isEmpty()) map.put("NumberOfVisits", "Customer Visit The First Time");
      else map.put("NumberOfVisits", billing);
      billing1 = this.billingService.getBillingByCustomerId(custId);
      if (billing1.isEmpty()) map.put("Last Visited Date", billing1.get(0));
    } catch (final IndexOutOfBoundsException ex) {
      map.put("LastVisitedDate", "Customer Visit The First Time");
      LOGGER.error(Constants.MARKER, "getBillingDetailsByCustomerId " + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_BILLING_PAYMENT_DETAILS)
  public final ResponseEntity<Object> getBillingPaymentDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<BillingPayments> billingPayment = new ArrayList<>();

    final long billingId = Long.parseLong(request.getParameter("billingId"));
    try {
      billingPayment = this.billingPaymentsService.getBillingPaymentsDetailsByBillId(billingId);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getBillingDetail " + ex);
    }
    return new ResponseEntity<>(billingPayment, HttpStatus.OK);
  }
}
