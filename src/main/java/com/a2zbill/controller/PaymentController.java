package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.OrganisationPayments;
import com.a2zbill.domain.Payment;
import com.a2zbill.services.OrganisationPaymentsService;
import com.a2zbill.services.PaymentService;
import com.tsss.basic.security.jwt.Identifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {
  private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

  @Autowired private PaymentService paymentService;

  @Autowired private OrganisationPaymentsService organisationPaymentsService;

  @GetMapping(path = RouteConstants.GET_ALL_PAYMENT_DETAILS)
  public final List<Payment> getAllPaymentDetails() {
    List<Payment> payment = null;
    try {
      payment = this.paymentService.getAllPayments();
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getPaymentDetails" + ex);
    }
    return payment;
  }

  @GetMapping(path = RouteConstants.GET_ALL_PAYMENT_DETAILS_BYORG)
  public final ResponseEntity<List<Map<String, Object>>> getAllPaymentDetailsByOrganisation(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<Map<String, Object>> paydetails = new ArrayList<>();
    List<OrganisationPayments> organisationPayments = null;
    List<Payment> payments = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        Payment payment = null;
        organisationPayments = organisationPaymentsService.getOrganisationPaymentsByOrgId(orgId);
        if (!organisationPayments.isEmpty()) {
          for (final OrganisationPayments orgPay : organisationPayments) {
            final Map<String, Object> map = new HashMap<>();
            final long paymentId = orgPay.getPaymentId().getPaymentId();
            payment = paymentService.getPaymentById(paymentId);
            map.put("PaymentId", paymentId);
            map.put("PaymentType", payment.getPaymentType());
            paydetails.add(map);
          }
        } else {
          payments = paymentService.getAllPaymentsDefaultTrue();
          for (final Payment paymnt : payments) {
            final Map<String, Object> map = new HashMap<>();
            map.put("PaymentId", paymnt.getPaymentId());
            map.put("PaymentType", paymnt.getPaymentType());
            paydetails.add(map);
          }
        }
      } else {
        return new ResponseEntity<>(paydetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getAllPaymentDetailsByOrganisation" + ex);
    }
    return new ResponseEntity<>(paydetails, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.SAVE_PAYMENTS_DETAILS)
  public final Map<String, Object> savePaymentModes(
      @RequestBody final String paymentTypes,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {
    final JSONObject paymentDetails = new JSONObject(paymentTypes);
    final Map<String, Object> paymentStatus = new HashMap<>();
    final String paymentMode = paymentDetails.getString("paymentMode");
    final Payment payment = new Payment();
    Payment payments = null;
    try {
      payments = paymentService.getPaymentType(paymentMode.toUpperCase());
      if (payments != null && payments.getPaymentType().equalsIgnoreCase(paymentMode)) {
        paymentStatus.put("errorMessage", "paymentType Already Exists");
        paymentStatus.put("flag", "false");
        return paymentStatus;
      } else {
        payment.setPaymentType(paymentMode.toUpperCase());
        payment.setDefalt(paymentDetails.getBoolean("defalt"));
        payment.setCreatedDate(new Date());
        payment.setModifiedDate(new Date());
        this.paymentService.savePaymentModes(payment);
        paymentStatus.put("PaymentStatus", paymentMode + " Payment Mode successfully saved");
      }
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "payment modes save" + ex);
    }
    return paymentStatus;
  }

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.UPDATE_PAYMENTS_DETAILS)
  public final Map<String, Object> updateDefaultStatus(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {
    final Map<String, Object> map = new HashMap<>();
    Payment payments = new Payment();
    try {
      final JSONObject payment = new JSONObject(payload);
      final Long paymentId = payment.getLong("paymentId");
      if (paymentId != null) {
        payments = paymentService.getPaymentById(paymentId);
        if (payments.isDefalt() == Constants.STATUSTRUE) {
          payments.setDefalt(Constants.STATUSFALSE);
        } else {
          payments.setDefalt(Constants.STATUSTRUE);
        }
        paymentService.update(payments);
        map.put("payments", "Payments Default Status Updated Successfully.");
      } else {
        map.put("payments", "InValid Payment");
      }
    } catch (final Exception ed) {
      LOGGER.info(Constants.MARKER, "updateDefaultStatus" + ed);
    }
    return map;
  }
}
