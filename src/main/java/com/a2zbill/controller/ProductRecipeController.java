package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductRecipe;
import com.a2zbill.domain.QuantityMeasurements;
import com.a2zbill.domain.RecipeIngredients;
import com.a2zbill.domain.RecipeProduct;
import com.a2zbill.services.CartProductService;
import com.a2zbill.services.ProductRecipeService;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.QuantityMeasurementsService;
import com.a2zbill.services.RecipeIngredientsService;
import com.a2zbill.services.RecipeProductService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductRecipeController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductRecipeController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private ProductRecipeService productRecipeService;

  @Autowired private RecipeProductService recipeProductService;

  @Autowired private RecipeIngredientsService recipeIngredientsService;

  @Autowired private ProductService productService;

  @Autowired private CartProductService cartProductService;

  @Autowired private QuantityMeasurementsService quantityMeasuremetsService;

  @PostMapping(path = RouteConstants.SAVE_PRODUCT_RECIPE_DETAILS)
  public final ResponseEntity<Object> getProductRecipe(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String recipedetails)
      throws JSONException {

    final JSONObject receipe = new JSONObject(recipedetails);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Map<String, Object> map = new HashMap<>();
    ProductRecipe productRecipe = null;
    final String recipName = receipe.getString("recipeName");
    productRecipe = productRecipeService.getProductRecipesByRecipeName(recipName);
    if (productRecipe != null) {
      map.put("message", "RecipeName already existed");
      return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);

    } else {
      productRecipe = new ProductRecipe();
      final QuantityMeasurements qms =
          this.quantityMeasuremetsService.getQuantityMeasurementsById(
              receipe.getJSONObject("quantityType").getLong("id"));
      productRecipe.setRecipeName(recipName);
      productRecipe.setQuantity(receipe.getLong("quantity"));
      productRecipe.setQuantityMeasurements(qms);
      final Branch branch = employeeDetails.getBranch();
      if (branch != null) productRecipe.setBranch(branch);
      else productRecipe.setBranch(null);
      productRecipe.setOrganisation(employeeDetails.getOrganisation());
      productRecipe.setCteatedDate(new Date());
      productRecipe.setModifiedDate(new Date());
      productRecipe.setStatus(true);
      this.productRecipeService.save(productRecipe);
      final String data = receipe.getString("ingredients");
      final JSONArray ingredients = new JSONArray(data);
      for (int i = 0; i < ingredients.length(); i++) {
        final JSONObject ingredient = ((JSONObject) ingredients.get(i));
        final RecipeIngredients ingedients = new RecipeIngredients();
        final Integer productId = ingredient.getInt("id");

        final Product product = productService.getProductsById(productId.longValue());

        if (branch != null) ingedients.setBranch(branch);
        else ingedients.setBranch(null);
        ingedients.setCreatedDate(new Date());
        ingedients.setModifiedDate(new Date());
        ingedients.setOrganisation(employeeDetails.getOrganisation());
        ingedients.setProduct(product);
        ingedients.setProductRecipe(productRecipe);
        final BigDecimal quantity =
            new BigDecimal(ingredient.getInt(Constants.QUANTITY))
                .multiply(new BigDecimal(product.getMeasurement()));
        ingedients.setQuantity(quantity);
        recipeIngredientsService.save(ingedients);
        map.put("id", productRecipe.getId());
        return new ResponseEntity<Object>(map, HttpStatus.OK);
      }
    }

    return new ResponseEntity<Object>(map, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.RECIPE_PRODUCT_SAVE)
  public final Map<String, Object> getRecipeProduct(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    Map<String, Object> map = new HashMap<String, Object>();
    final JSONObject recipeProducts = new JSONObject(payload);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    RecipeProduct recipeProduct = new RecipeProduct();
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();
    String recipeIds = recipeProducts.getString(Constants.RECIPEID);
    ProductRecipe recipeId = null;
    if (!(recipeIds.isEmpty()) || recipeIds != null) {
      final long recipeproductId = Long.parseLong(recipeIds);
      recipeId = productRecipeService.getProductRecipesById(recipeproductId);
    }
    try {
      final List<RecipeProduct> recipe =
          recipeProductService.getAllRecipeProductByRecipeId(recipeId.getId());
      if (recipe.size() == 0) {

        final JSONArray recipeDetails = recipeProducts.getJSONArray("Products");
        for (int i = 0; i < recipeDetails.length(); i++) {
          final JSONObject jsonObject = (JSONObject) recipeDetails.get(i);
          final String productName = jsonObject.getString(Constants.PRODUCTNAME);
          final Long quantity = Long.parseLong(jsonObject.getString(Constants.QUANTITY));

          recipeProduct.setBranch(employeeDetails.getBranch());
          recipeProduct.setCreatedDate(new Date());
          recipeProduct.setModifiedDate(new Date());
          recipeProduct.setOrganisation(employeeDetails.getOrganisation());
          final Product product =
              productService.getProductsByProductNameAndOrgAndBranch(productName, orgId, branchId);
          recipeProduct.setProduct(product);
          recipeProduct.setProductRecipe(recipeId);
          recipeProduct.setQuantity(quantity);
          recipeProductService.save(recipeProduct);
          map.put("message", "Recipe products added successfully");
        }
      } else {

        final JSONArray recipeDetails = recipeProducts.getJSONArray("Products");
        for (int i = 0; i < recipeDetails.length(); i++) {
          final JSONObject jsonObject = (JSONObject) recipeDetails.get(i);
          final Long quantity = Long.parseLong(jsonObject.getString(Constants.QUANTITY));

          final Product product = productService.getProductsById(jsonObject.getLong("id"));

          recipeProduct =
              recipeProductService.getRecipeProductByRecipeIdAndProductId(
                  recipeId.getId(), product.getId());

          if (recipeProduct != null) {
            recipeProduct.setBranch(employeeDetails.getBranch());
            recipeProduct.setCreatedDate(new Date());
            recipeProduct.setModifiedDate(new Date());
            recipeProduct.setOrganisation(employeeDetails.getOrganisation());
            recipeProduct.setProduct(product);
            recipeProduct.setProductRecipe(recipeId);
            recipeProduct.setQuantity(quantity);
            recipeProductService.update(recipeProduct);
            map.put("message", "Recipe products updated successfully");
          } else {

            final RecipeProduct recipeProduct2 = new RecipeProduct();

            recipeProduct2.setBranch(employeeDetails.getBranch());
            recipeProduct2.setCreatedDate(new Date());
            recipeProduct2.setModifiedDate(new Date());
            recipeProduct2.setOrganisation(employeeDetails.getOrganisation());
            recipeProduct2.setProduct(product);
            recipeProduct2.setProductRecipe(recipeId);
            recipeProduct2.setQuantity(quantity);
            recipeProductService.save(recipeProduct2);
            map.put("message", "Recipe products added successfully");
          }
        }
      }
    } catch (final Exception e) {
      final JSONArray recipeDetails = recipeProducts.getJSONArray("Products");
      for (int i = 0; i < recipeDetails.length(); i++) {
        final JSONObject jsonObject = (JSONObject) recipeDetails.get(i);
        final String productName = jsonObject.getString(Constants.PRODUCTNAME);
        final Long quantity = Long.parseLong(jsonObject.getString(Constants.QUANTITY));

        recipeProduct.setBranch(employeeDetails.getBranch());
        recipeProduct.setCreatedDate(new Date());
        recipeProduct.setModifiedDate(new Date());
        recipeProduct.setOrganisation(employeeDetails.getOrganisation());
        final Product product =
            productService.getProductsByProductNameAndOrgAndBranch(productName, orgId, branchId);
        recipeProduct.setProduct(product);
        recipeProduct.setProductRecipe(recipeId);
        recipeProduct.setQuantity(quantity);
        recipeProductService.save(recipeProduct);
        map.put("message", "Recipe products added successfully");
      }
    }

    return map;
  }

  @GetMapping(path = RouteConstants.GET_ALL_RECIPE_DETAILS)
  public final ResponseEntity<Object> getRecipeDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<ProductRecipe> recipeDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute(Constants.IDENTIFIER);
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) recipeDetails = this.productRecipeService.getAllProductRecipesByOrg(orgId);
        else
          recipeDetails =
              this.productRecipeService.getAllProductRecipesByOrgAndBranch(orgId, branch);

      } else {
        return new ResponseEntity<>(recipeDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info(Constants.NORECORDFOUND);
    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getProductRecipeDetails" + e);
    }
    return new ResponseEntity<>(recipeDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_ALL_MEASUREMENT_DETAILS)
  public final List<QuantityMeasurements> getMeasurements(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<QuantityMeasurements> measurementsDetails = null;
    try {

      measurementsDetails = this.quantityMeasuremetsService.getAllQuantityMeasurements();

    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getQuantityDetails" + ex);
    }
    return measurementsDetails;
  }

  @GetMapping(path = RouteConstants.GALL_RECIPEPRODUCT_DETAILS)
  public final ResponseEntity<Object> getRecipeProductDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<RecipeProduct> productRecipe = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute(Constants.IDENTIFIER);
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) productRecipe = this.recipeProductService.getAllRecipeProductByOrg(orgId);
        else
          productRecipe =
              this.recipeProductService.getAllRecipeProductByOrgAndBranch(orgId, branch);
      } else {
        return new ResponseEntity<>(productRecipe, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info(Constants.NORECORDFOUND);
    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getProductRecipeDetails" + e);
    }
    return new ResponseEntity<>(productRecipe, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ALL_INGREDIENTS_DETAILS)
  public final ResponseEntity<Object> getIngredientsDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<RecipeIngredients> ingredientsDetails = new ArrayList<>();
    try {

      final Identifier identifier = (Identifier) request.getAttribute(Constants.IDENTIFIER);
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          ingredientsDetails = this.recipeIngredientsService.getAllRecipeIngredientsByOrg(orgId);
        } else {
          ingredientsDetails =
              this.recipeIngredientsService.getAllRecipeIngredientsByOrgAndBranch(orgId, branch);
        }
      } else {
        return new ResponseEntity<>(ingredientsDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info(Constants.NORECORDFOUND);
    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getRecipeIngredientDetails" + e);
    }
    return new ResponseEntity<>(ingredientsDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_RECIPE_INGREDIENTS)
  public final ResponseEntity<Object> getIngredients(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> list = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute(Constants.IDENTIFIER);
      if (identifier != null) {
        final List<ProductRecipe> recipedetails = productRecipeService.getAllProductRecipeDetails();

        for (final ProductRecipe receipe : recipedetails) {

          final Map<String, Object> map = new HashMap<>();

          map.put("recipeId", receipe.getId());
          map.put("recipeName", receipe.getRecipeName());
          map.put("recipeQuantity", receipe.getQuantity());
          map.put("recipeQuantityType", receipe.getQuantityMeasurements());
          map.put(
              "ingredients",
              recipeIngredientsService.getAllRecipeIngredientsByRecipeId(receipe.getId()));
          list.add(map);
        }

      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info(Constants.NORECORDFOUND);
    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getRecipeIngredientDetails" + e);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_RECIPE_PRODUCTS)
  public final ResponseEntity<Object> getProducts(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> list = new ArrayList<>();
    try {

      final Identifier identifier = (Identifier) request.getAttribute(Constants.IDENTIFIER);
      if (identifier != null) {
        final List<RecipeIngredients> recipedetails =
            recipeIngredientsService.getAllRecipeIngredients();

        for (final RecipeIngredients receipe : recipedetails) {

          final Map<String, Object> map = new HashMap<>();

          map.put("recipeId", receipe.getProductRecipe().getId());
          map.put(
              "products",
              recipeIngredientsService.getAllRecipeIngredientsByProductId(
                  receipe.getProduct().getId()));
          list.add(map);
        }

      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info(Constants.NORECORDFOUND);
    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getRecipeIngredientDetails" + e);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_WISE_RECIPE_INGREDIENTS)
  public final Map<Long, Object> getProductSumIngredients(
      final HttpServletRequest request, final HttpServletResponse response) throws ParseException {
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();

    final String date = request.getParameter("date");
    final Date dateTime = new SimpleDateFormat("yyyy-MM-dd").parse(date);

    List<Object[]> listCartDetails = null;
    List<RecipeProduct> recipeProducts = null;
    List<RecipeIngredients> recipeIngredients = null;
    final Map<Long, Object> globalMap = new HashMap<>();
    try {
      listCartDetails =
          cartProductService.getIngredientWiseProductQuantitySumByOrgIdAndDate(
              orgId, branchId, dateTime);
      for (int i = 0; i < listCartDetails.size(); i++) {
        final String productName = listCartDetails.get(i)[0].toString();
        final BigDecimal sumofquanty = new BigDecimal(listCartDetails.get(i)[1].toString());

        final Product product =
            productService.getProductsByProductNameAndOrgIdAndBranchId(
                productName, orgId, branchId);
        final long productId = product.getId();
        recipeProducts =
            recipeProductService.getAllRecipeProductsProductIdAndOrgAndBranch(
                orgId, branchId, productId);
        final long recipeId = recipeProducts.get(0).getProductRecipe().getId();
        final BigDecimal quantity = new BigDecimal(recipeProducts.get(0).getQuantity());

        recipeIngredients = recipeIngredientsService.getAllRecipeIngredientsByRecipeId(recipeId);
        for (final RecipeIngredients ingredients : recipeIngredients) {
          final BigDecimal singleQuantity = ingredients.getQuantity().divide(quantity);
          final Map<String, Object> map = new HashMap<>();
          if (globalMap.containsKey(ingredients.getProduct().getId())) {
            @SuppressWarnings("rawtypes")
            final Map m = (Map) globalMap.get(ingredients.getProduct().getId());
            final BigDecimal q = (BigDecimal) m.get(Constants.QUANTITY);
            final BigDecimal finalQuantity =
                q.add(singleQuantity.multiply(sumofquanty.multiply(sumofquanty))); // *sumofquanty
            map.put("productId", ingredients.getProduct().getId());
            map.put(Constants.PRODUCTNAME, ingredients.getProduct().getProductName());
            map.put(Constants.QUANTITY, finalQuantity);
            globalMap.put(ingredients.getProduct().getId(), map);
          } else {
            map.put("productId", ingredients.getProduct().getId());
            map.put(Constants.PRODUCTNAME, ingredients.getProduct().getProductName());
            map.put(Constants.QUANTITY, singleQuantity.multiply(sumofquanty)); // *sumofquanty
            globalMap.put(ingredients.getProduct().getId(), map);
          }
        }
      }
    } catch (final EmptyResultDataAccessException ex) {
      LOGGER.info(Constants.MARKER, " /api/productwise/recipe/ingredients  " + ex);
    }
    return globalMap;
  }

  @DeleteMapping(path = RouteConstants.DELETE_PRODUCTRECIPE_DETAILS)
  public final Boolean deleteProductRecipeDetails(
      final HttpServletRequest request, final HttpServletResponse responce) throws Exception {

    final long recipeId = Long.parseLong(request.getParameter("recipeId"));

    Boolean result = null;
    ProductRecipe productRecipe = null;
    try {

      productRecipe = this.productRecipeService.getProductRecipesById(recipeId);

      result = this.productRecipeService.delete(productRecipe.getId());

    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "updateProductRecipedetails" + ex);
    }
    return result;
  }

  @PostMapping(value = "/api/productrecipe/update/falseState")
  public final Map<String, Object> updateProductRecipeToFalseState(
      HttpServletRequest request, HttpServletResponse responce, @RequestBody String payload)
      throws JSONException {

    final Map<String, Object> map = new HashMap<>();
    final JSONObject emp = new JSONObject(payload);
    final long id = emp.getLong("recipeId");

    try {

      ProductRecipe productRecipe = this.productRecipeService.getProductRecipesById(id);
      productRecipe.setCteatedDate(productRecipe.getCteatedDate());
      productRecipe.setModifiedDate(productRecipe.getModifiedDate());

      if (productRecipe.isStatus() != true) {
        productRecipe.setStatus(Constants.STATUSFALSE);

      } else {
        productRecipe.setStatus(Constants.STATUSFALSE);
      }
      productRecipeService.update(productRecipe);
      map.put("message", "productRecipe details updated to false state");
    } catch (final Exception ex) {
      LOGGER.error("productRecipe  details", ex);
    }
    return map;
  }

  @PostMapping(value = "/api/productrecipeingredients/update")
  public final Map<String, Object> getProductRecipedetails(
      HttpServletRequest request, HttpServletResponse responce, @RequestBody String payload)
      throws JSONException {

    // for getting login Credentials
    JSONObject receipe = new JSONObject(payload);
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Map<String, Object> map = new HashMap<String, Object>();
    ProductRecipe productRecipe = new ProductRecipe();
    JSONObject quantityType = receipe.getJSONObject("quantityType");
    final long recipeId = receipe.getLong("recipeId");
    final String recipName = receipe.getString("recipeName");

    productRecipe = productRecipeService.getProductRecipesById(recipeId);

    final QuantityMeasurements qms =
        this.quantityMeasuremetsService.getQuantityMeasurementsById(
            Long.parseLong(quantityType.getString("id")));
    productRecipe.setRecipeName(recipName);
    productRecipe.setQuantity(receipe.getLong("quantity"));
    productRecipe.setQuantityMeasurements(qms);
    final Branch branch = employeeDetails.getBranch();
    if (branch != null) {
      productRecipe.setBranch(branch);
    } else {
      productRecipe.setBranch(null);
    }
    productRecipe.setOrganisation(employeeDetails.getOrganisation());
    productRecipe.setCteatedDate(new Date());
    productRecipe.setModifiedDate(new Date());
    this.productRecipeService.update(productRecipe);
    try {
      final String data = receipe.getString("ingredients");
      final JSONArray ingredients = new JSONArray(data);
      for (int i = 0; i < ingredients.length(); i++) {
        final JSONObject ingredient = ((JSONObject) ingredients.get(i));
        RecipeIngredients ingedients = new RecipeIngredients();
        final Long productId = ingredient.getLong("id");
        Product product = new Product();
        product = productService.getProductsById(productId);
        ingedients =
            recipeIngredientsService.getRecipeIngredientsByRecipeIdAndProductId(
                recipeId, product.getId());
        if (ingedients != null) {

          ingedients.setCreatedDate(new Date());
          ingedients.setModifiedDate(new Date());
          ingedients.setOrganisation(employeeDetails.getOrganisation());
          ingedients.setProduct(product);
          ingedients.setProductRecipe(productRecipe);
          if (branch != null) {
            ingedients.setBranch(branch);
          } else {
            ingedients.setBranch(null);
          }
          BigDecimal quantity =
              new BigDecimal(ingredient.getInt("Quantity"))
                  .multiply(new BigDecimal(product.getMeasurement()));
          ingedients.setQuantity(quantity);
          recipeIngredientsService.update(ingedients);
          map.put("message", "Recipe ingredients details are updated successfully");
        } else {
          RecipeIngredients ingredents = new RecipeIngredients();

          if (branch != null) {
            ingredents.setBranch(branch);
          } else {
            ingredents.setBranch(null);
          }
          ingredents.setCreatedDate(new Date());
          ingredents.setModifiedDate(new Date());
          ingredents.setOrganisation(employeeDetails.getOrganisation());
          ingredents.setProduct(product);
          ingredents.setProductRecipe(productRecipe);
          final BigDecimal quantity =
              new BigDecimal(ingredient.getInt("Quantity"))
                  .multiply(new BigDecimal(product.getMeasurement()));
          ingredents.setQuantity(quantity);
          recipeIngredientsService.save(ingredents);
          map.put("message", "Recipe ingredients details are saved successfully");
        }
      }
    } catch (final Exception e) {
      LOGGER.error("updateproductrecipedetails " + e);
    }

    return map;
  }
}
