package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.KotHistory;
import com.a2zbill.services.KotHistoryService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KotHistoryController {

  private static final Logger LOGGER = LoggerFactory.getLogger(KotHistoryController.class);

  @Autowired private KotHistoryService kotHistoryService;

  @PostMapping(value = RouteConstants.KOT_HISTORY_SAVE)
  public final KotHistory addKotHistory(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {

    final KotHistory kotHistory = new KotHistory();
    final JSONObject kotHistoryDetails = new JSONObject(payload);
    try {
      kotHistory.setProductName(kotHistoryDetails.getString("productName"));
      kotHistory.setQuantity(kotHistoryDetails.getInt("quantity"));
      kotHistory.setCounterNumber(kotHistoryDetails.getLong("counterNumber"));
      kotHistory.setBranchId(kotHistoryDetails.getLong("branchId"));
      kotHistory.setSection("service");
      this.kotHistoryService.save(kotHistory);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "KotHistoryController in the addKotHistory() of" + ex);
    }
    return kotHistory;
  }
}
