package com.a2zbill.controller.elasticsearch;

import com.a2zbill.domain.Product;
import com.a2zbill.domain.elasticsearch.ProductElasticSearch;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.elasticsearch.ProductElasticSearchService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductSearchController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductSearchController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;
  @Autowired private EmployeeService employeeService;

  @Autowired private ProductElasticSearchService productElasticSearchService;

  @Autowired private ProductService productService;

  @Autowired private CustomerService customerService;

  @RequestMapping(value = "/api/products/search/get", method = RequestMethod.GET)
  public final List<ProductElasticSearch> getProductDetailsBySearch(
      final HttpServletRequest request, final HttpServletResponse responce) {

    try {
      final String searchKey = request.getParameter("searchKey");
      final Employee employeeDetails = getEmployeeDetailsByHttpServletRequest(request);
      if (employeeDetails != null) {
        final long organisationId = employeeDetails.getOrganisation().getId();
        final Branch branch = employeeDetails.getBranch();
        if (branch != null) {
          if (searchKey != null && !searchKey.isEmpty()) {
            final Long branchId = branch.getId();
            final List<ProductElasticSearch> productElasticSearchList =
                productElasticSearchService.getProductByProductNameAndBranchIdAndOrgId(
                    searchKey, branchId, organisationId);
            if (productElasticSearchList != null) {
              return productElasticSearchList;
            }
          }
        } else {
          final List<ProductElasticSearch> productElasticSearchList =
              productElasticSearchService.getProductByProductNameAndOrgId(
                  searchKey, organisationId);
          if (productElasticSearchList != null) {
            return productElasticSearchList;
          }
        }
      }

    } catch (final Exception ex) {
      LOGGER.error("Product Elastic Search result :: " + ex);
    }
    return new ArrayList<>();
  }

  @RequestMapping(value = "/api/products/search/price/range", method = RequestMethod.GET)
  public final List<ProductElasticSearch> getProductDetailsByProductNameAndPriceRange(
      final HttpServletRequest request, final HttpServletResponse responce) {

    try {
      final String productName = request.getParameter("productName");
      final String minPrice = request.getParameter("minPrice");
      final String maxPrice = request.getParameter("maxPrice");
      final BigDecimal minPriceValue = new BigDecimal(minPrice).setScale(2, RoundingMode.HALF_EVEN);
      final BigDecimal maxPriceValue = new BigDecimal(maxPrice).setScale(2, RoundingMode.HALF_EVEN);
      final Employee employeeDetails = getEmployeeDetailsByHttpServletRequest(request);
      if (employeeDetails != null) {
        final long organisationId = employeeDetails.getOrganisation().getId();
        final Branch branch = employeeDetails.getBranch();
        if (branch != null) {
          if (productName != null && !productName.isEmpty()) {
            final Long branchId = branch.getId();
            final List<ProductElasticSearch> productElasticSearchList =
                productElasticSearchService.getProductsByBranchIdAndOrgIdProductNameAndPriceRange(
                    branchId, organisationId, productName, minPriceValue, maxPriceValue);

            if (productElasticSearchList != null) {
              return productElasticSearchList;
            }
          }
        } else {
          final List<ProductElasticSearch> productElasticSearchList =
              productElasticSearchService.getProductsByOrgIdAndProductNameAndPriceRange(
                  organisationId, productName, minPriceValue, maxPriceValue);
          if (productElasticSearchList != null) {
            return productElasticSearchList;
          }
        }
      }

    } catch (final Exception ex) {
      LOGGER.error("product Elastic Search result ::: " + ex);
    }
    return new ArrayList<>();
  }

  @RequestMapping(value = "/products/save/elastic", method = RequestMethod.GET)
  public void ProductElasticSearch(
      final HttpServletRequest request, final HttpServletResponse responce) {
    try {
      final List<Product> productList = productService.getAllproductsByElastic();
      for (final Product product : productList) {
        final Long productId = product.getId();
        final Organisation organisation = product.getOrganisation();
        final Branch branch = product.getBranch();
        if (organisation != null) {
          final Long orgId = organisation.getId();
          if (branch != null) {
            final Long branchId = branch.getId();
            final ProductElasticSearch productElasticSearchList =
                productElasticSearchService.getProductByProductIdAndBranchIdAndOrgId(
                    productId, branchId, orgId);
            /*  if (productElasticSearchList != null) {
              continue;
            }*/
          } else {
            final ProductElasticSearch productElasticSearchList =
                productElasticSearchService.getProductByProductIdAndOrgId(productId, orgId);
            /* if (productElasticSearchList != null) {
              continue;
            }*/
          }
        }

        final ProductElasticSearch storeTypeSearch = new ProductElasticSearch();
        storeTypeSearch.setId(product.getId());
        storeTypeSearch.setCGST(product.getCGST());
        storeTypeSearch.setCode(product.getCode());
        storeTypeSearch.setHsnNumber(product.getHsnNumber());
        storeTypeSearch.setId(product.getId());
        storeTypeSearch.setIGST(product.getIGST());
        storeTypeSearch.setImage(product.getImage());
        storeTypeSearch.setIncludingTaxFlag(product.getIncludingTaxFlag());
        storeTypeSearch.setMeasurement(product.getMeasurement());
        storeTypeSearch.setMeasurement1(product.getMeasurement1());
        storeTypeSearch.setMeasurement2(product.getMeasurement2());
        storeTypeSearch.setParentId(product.getParentId());
        storeTypeSearch.setProductCount(product.getProductCount());
        storeTypeSearch.setProductName(product.getProductName());
        storeTypeSearch.setProductPrice(product.getProductPrice());
        storeTypeSearch.setSGST(product.getSGST());
        if (organisation != null) {
          storeTypeSearch.setOrganisationId(organisation.getId());
        }
        if (branch != null) {
          storeTypeSearch.setBranchId(branch.getId());
        }
        productElasticSearchService.insertProduct(storeTypeSearch);
      }

    } catch (final Exception ex) {
      LOGGER.error("product Elastic Search save error ::: " + ex);
    }
  }

  @RequestMapping(value = "/products/search/price/range", method = RequestMethod.GET)
  public final List<ProductElasticSearch> getCustomerProductDetailsByProductNameAndPriceRange(
      final HttpServletRequest request, final HttpServletResponse responce) {

    try {
      final String productName = request.getParameter("productName");
      final String minPrice = request.getParameter("minPrice");
      final String maxPrice = request.getParameter("maxPrice");
      final BigDecimal minPriceValue = new BigDecimal(minPrice).setScale(2, RoundingMode.HALF_EVEN);
      final BigDecimal maxPriceValue = new BigDecimal(maxPrice).setScale(2, RoundingMode.HALF_EVEN);
      final Customer customerDetails = getCustomerByHttpServletRequest(request);
      if (customerDetails != null) {
        final long organisationId = customerDetails.getOrganisation().getId();
        final Branch branch = customerDetails.getBranch();
        if (branch != null) {
          if (productName != null && !productName.isEmpty()) {
            final Long branchId = branch.getId();
            final List<ProductElasticSearch> productElasticSearchList =
                productElasticSearchService.getProductsByBranchIdAndOrgIdProductNameAndPriceRange(
                    branchId, organisationId, productName, minPriceValue, maxPriceValue);

            if (productElasticSearchList != null) {
              return productElasticSearchList;
            }
          }
        } else {
          final List<ProductElasticSearch> productElasticSearchList =
              productElasticSearchService.getProductsByOrgIdAndProductNameAndPriceRange(
                  organisationId, productName, minPriceValue, maxPriceValue);
          if (productElasticSearchList != null) {
            return productElasticSearchList;
          }
        }
      }

    } catch (final Exception ex) {
      LOGGER.error("product Elastic Search result ::: " + ex);
    }
    return new ArrayList<>();
  }

  private Employee getEmployeeDetailsByHttpServletRequest(final HttpServletRequest request) {
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      return employeeDetails;
    } catch (final Exception e) {
      LOGGER.error("Error :::" + e);
    }
    return null;
  }

  private Customer getCustomerByHttpServletRequest(final HttpServletRequest request) {

    Customer customerDetails = null;

    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");

      if (identifier != null) {
        final long userId = identifier.getOrgId();
        customerDetails = customerService.getCustomerDetailsBycustomerId(userId);
      } else {
        return null;
      }

    } catch (final Exception e) {
      LOGGER.error("Error :::" + e);
    }
    return customerDetails;
  }
}
