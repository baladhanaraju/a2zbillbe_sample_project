package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Flows;
import com.a2zbill.services.FlowsService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlowsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(FlowsController.class);

  @Autowired private FlowsService flowsService;

  @RequestMapping(value = "/api/add/flowsDetails", method = RequestMethod.POST)
  public final ResponseEntity<Object> addFlowDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final Map<String, Object> flowMap = new HashMap<String, Object>();

    try {
      final JSONObject flow = new JSONObject(payload);
      Flows flowsDetails = new Flows();

      flowsDetails.setFlowName(flow.getString("flowName"));
      flowsDetails.setDescription(flow.getString("description"));

      this.flowsService.save(flowsDetails);
      flowMap.put("message", flowsDetails);

    } catch (final Exception ex) {
      LOGGER.error("addFlowTypeDetails" + ex);
    }
    return new ResponseEntity<>(flowMap, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_FLOWS_DETAILS)
  public final List<Flows> getFlowDetails() {

    List<Flows> flowsDetails = null;

    try {
      flowsDetails = this.flowsService.getFlowsDetails();

    } catch (final Exception ex) {
      LOGGER.error("getFlowsDetails " + ex);
    }
    return flowsDetails;
  }
}
