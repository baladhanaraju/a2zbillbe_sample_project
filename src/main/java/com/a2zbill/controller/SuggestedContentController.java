package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.SuggestedContent;
import com.a2zbill.services.impl.SuggestedContentServiceImpl;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SuggestedContentController {

  private static final Logger LOGGER = LoggerFactory.getLogger(SuggestedContentController.class);

  @Autowired private SuggestedContentServiceImpl suggestedContentService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @GetMapping(value = RouteConstants.GET_SUGGESTEDCONTENT_DETAILS)
  public final ResponseEntity<Object> getSuggestedContentDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<SuggestedContent> suggestedContentDetails = new ArrayList<>();
    ;

    try {
      suggestedContentDetails = suggestedContentService.getAllSuggestedContent();
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getSuggestedContentDetails " + e);
    }
    return new ResponseEntity<>(suggestedContentDetails, HttpStatus.OK);
  }

  @PostMapping(value = RouteConstants.HASH_DETAILS)
  public final Map<String, String> getHash(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final String mobileNumber = employeeDetails.getMobileNumber();
    final String firstName = employeeDetails.getFirstname();
    final String email = employeeDetails.getEmailAddress();

    final Map<String, String> map = new HashMap<>();
    final JSONObject hash = new JSONObject(payload);
    final String txnId = hash.getString("ORDER_ID");
    final String amount = hash.getString("TXN_AMOUNT");
    final String productinfo = hash.getString("productinfo");
    final String sucessUrl = hash.getString("surl");
    final String failureUrl = hash.getString("furl");

    try {
      final Map<String, String> s =
          suggestedContentService.hashCalMethod(
              txnId, amount, productinfo, firstName, email, mobileNumber, sucessUrl, failureUrl);
      map.putAll(s);
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getSuggestedContentDetails " + e);
    }
    return map;
  }
}
