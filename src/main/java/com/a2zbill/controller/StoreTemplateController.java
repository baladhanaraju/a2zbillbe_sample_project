package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.StoreTemplate;
import com.a2zbill.domain.StoreType;
import com.a2zbill.services.StoreTemplateService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Link;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.LinkService;
import com.tsss.basic.service.OrganisationService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("unchecked")
@RestController
public class StoreTemplateController {

  private static final Logger LOGGER = LoggerFactory.getLogger(StoreTemplateController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private StoreTemplateService storeTemplateService;

  @Autowired private OrganisationService organisationService;

  @Autowired private LinkService linkService;

  @GetMapping(
      value = {RouteConstants.GET_STORE_TEMPLATE_DETAILS, RouteConstants.GET_STORE_TEMPLATE})
  public final List<StoreTemplate> getStoreTemplate() {

    List<StoreTemplate> storeTemplate = null;
    try {
      storeTemplate = this.storeTemplateService.getAllStoreTemplate();
    } catch (final Exception ex) {
      LOGGER.error("getStoreTemplateDetails" + ex, ex);
    }
    return storeTemplate;
  }

  @GetMapping(path = RouteConstants.GET_STORE_TEMPLATE_BY_ID)
  public final StoreTemplate getStoreTemplateById(
      final HttpServletRequest request, final HttpServletResponse response) {

    StoreTemplate storeTemplate = null;
    try {
      final String storeTemplateId = request.getParameter("storeTemplateId");
      if (storeTemplateId != null && !storeTemplateId.isEmpty()) {
        storeTemplate =
            this.storeTemplateService.getStoreTemplateById(Long.parseLong(storeTemplateId));
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("GstTempalteDetailsById" + ex);
    }
    return storeTemplate;
  }

  @GetMapping(path = RouteConstants.GET_STORE_TEMPLATE_DETAILS_BY_ORG)
  public final StoreTemplate getStoreTemplateDetailsByOrg(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long storeTemplateId = employeeDetails.getOrganisation().getStoreTemplateId();
    StoreTemplate storeTemplate = new StoreTemplate();

    try {
      storeTemplate = this.storeTemplateService.getStoreTemplateById(storeTemplateId);
    } catch (final Exception ex) {
      LOGGER.error("getStoreTemplateDetailsByOrg" + ex, ex);
    }
    return storeTemplate;
  }

  @GetMapping(path = RouteConstants.GET_STORE_TYPE_DETAILS_BY_ORG)
  public final List<Map<String, Object>> getStoreTypeDetailsByOrg(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long orgId = this.jwtTokenUtil.getOrgIdFromToken(request);
    final Organisation organisation = this.organisationService.getOrganisationById(orgId);

    final long storeTempalteId = organisation.getStoreTemplateId();
    final StoreTemplate storeTempalte = storeTemplateService.getStoreTemplateById(storeTempalteId);
    final StoreType storeType = storeTempalte.getStoreType();

    final Integer[] linkArray = storeType.getLink();
    final List<Map<String, Object>> list = new ArrayList<>();

    for (final Integer linkId : linkArray) {
      Map<String, Object> map = new HashMap<>();
      final Link link = linkService.getLinkDetailsById(linkId);
      if (link.getParentId() == 0) {
        map.put("id", link.getId());
        map.put("name", link.getName());
        map.put(Constants.TITLE, link.getTitle());
        map.put("icon", link.getIcon());
        map.put(Constants.ROUTER, link.isRouter());
        list.add(map);
      } else {
        final long parentid = link.getParentId();
        if (list.size() == 0) {
          final List<Map<String, Object>> list1 = new ArrayList<>();
          final Link parentlink = linkService.getLinkDetailsById(parentid);
          map = new HashMap<>();
          map.put("id", parentlink.getId());
          map.put("name", parentlink.getName());
          map.put(Constants.TITLE, parentlink.getTitle());
          map.put("icon", parentlink.getIcon());
          map.put(Constants.ROUTER, link.isRouter());
          list.add(map);

          final Map<String, Object> map1 = new HashMap<>();
          map1.put("id", link.getId());
          map1.put("name", link.getName());
          map1.put(Constants.TITLE, link.getTitle());
          map1.put("icon", link.getIcon());
          list1.add(map1);
          map.put(Constants.ROUTER, link.isRouter());
          map.put("icon", list1);

        } else {
          int parentidcount = 0;
          for (int i = 0; i < list.size(); i++) {
            if (Long.parseLong(list.get(i).get("id").toString()) == parentid) {
              parentidcount = i;
              break;
            }
          }
          if (parentidcount == 0) {
            final List<Map<String, Object>> list1 = new ArrayList<>();
            final Link parentlink = linkService.getLinkDetailsById(parentid);
            map = new HashMap<>();
            map.put("id", parentlink.getId());
            map.put("name", parentlink.getName());
            map.put(Constants.TITLE, parentlink.getTitle());
            map.put("icon", parentlink.getIcon());
            map.put(Constants.ROUTER, link.isRouter());

            list.add(map);

            final Map<String, Object> map1 = new HashMap<>();
            map1.put("id", link.getId());
            map1.put("name", link.getName());
            map1.put(Constants.TITLE, link.getTitle());
            map1.put("icon", link.getIcon());
            map1.put("component", link.getName());
            map1.put(Constants.ROUTER, link.isRouter());
            list1.add(map1);
            map.put("items", list1);
          } else {
            final Map<String, Object> map2 = list.get(parentidcount);

            if (map2.containsKey("items")) {

              final List<Map<String, Object>> existingMapList =
                  (List<Map<String, Object>>) map2.get("items");
              final Map<String, Object> map1 = new HashMap<>();
              map1.put("id", link.getId());
              map1.put("name", link.getName());
              map1.put(Constants.TITLE, link.getTitle());
              map1.put("icon", link.getIcon());
              map1.put(Constants.ROUTER, link.isRouter());
              map1.put("component", link.getName());
              existingMapList.add(map1);
              map2.put("items", existingMapList);
              list.set(parentidcount, map2);

            } else {
              final List<Map<String, Object>> existingMapList = new ArrayList<>();
              final Map<String, Object> map1 = new HashMap<>();

              map1.put("id", link.getId());
              map1.put("name", link.getName());
              map1.put(Constants.TITLE, link.getTitle());
              map1.put("icon", link.getIcon());
              map1.put(Constants.ROUTER, link.isRouter());
              map1.put("component", link.getName());
              existingMapList.add(map1);
              map2.put("items", existingMapList);
              list.set(parentidcount, map2);
            }
          }
        }
      }
    }
    return list;
  }

  @GetMapping(path = RouteConstants.GET_SUPERADMIN_DETAILS_BY_ORG)
  public final List<Map<String, Object>> getSuperAdminDetailsByOrg(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Link> linkArray = null;
    try {
      linkArray = linkService.getAllLink();
    } catch (final Exception ex) {
      LOGGER.info("getDetailsBySuperAdmin", ex);
    }
    final List<Map<String, Object>> list = new ArrayList<>();
    for (final Link link : linkArray) {
      Map<String, Object> map = new HashMap<>();
      if (link.getRole().toString().equals("ROLE_SUPER_ADMIN"))
        if (link.getParentId() == 0) {
          map.put("id", link.getId());
          map.put("name", link.getName());
          map.put(Constants.TITLE, link.getTitle());
          map.put("icon", link.getIcon());
          map.put(Constants.ROUTER, link.isRouter());
          list.add(map);
        } else {
          final long parentid = link.getParentId();
          if (list.size() == 0) {
            final List<Map<String, Object>> list1 = new ArrayList<>();
            final Link parentlink = linkService.getLinkDetailsById(parentid);
            map = new HashMap<>();
            map.put("id", parentlink.getId());
            map.put("name", parentlink.getName());
            map.put(Constants.TITLE, parentlink.getTitle());
            map.put("icon", parentlink.getIcon());
            map.put(Constants.ROUTER, link.isRouter());
            list.add(map);

            final Map<String, Object> map1 = new HashMap<>();
            map1.put("id", link.getId());
            map1.put("name", link.getName());
            map1.put(Constants.TITLE, link.getTitle());
            map1.put("icon", link.getIcon());
            list1.add(map1);
            map.put(Constants.ROUTER, link.isRouter());
            map.put("icon", list1);

          } else {
            int parentidcount = 0;
            for (int i = 0; i < list.size(); i++) {
              if (Long.parseLong(list.get(i).get("id").toString()) == parentid) {
                parentidcount = i;
                break;
              }
            }
            if (parentidcount == 0) {
              final List<Map<String, Object>> list1 = new ArrayList<>();
              final Link parentlink = linkService.getLinkDetailsById(parentid);
              map = new HashMap<>();
              map.put("id", parentlink.getId());
              map.put("name", parentlink.getName());
              map.put(Constants.TITLE, parentlink.getTitle());
              map.put("icon", parentlink.getIcon());
              map.put(Constants.ROUTER, link.isRouter());

              list.add(map);

              final Map<String, Object> map1 = new HashMap<>();
              map1.put("id", link.getId());
              map1.put("name", link.getName());
              map1.put(Constants.TITLE, link.getTitle());
              map1.put("icon", link.getIcon());
              map1.put("component", link.getName());
              map1.put(Constants.ROUTER, link.isRouter());
              list1.add(map1);
              map.put("items", list1);
            } else {
              final Map<String, Object> map2 = list.get(parentidcount);

              if (map2.containsKey("items")) {

                final List<Map<String, Object>> existingMapList =
                    (List<Map<String, Object>>) map2.get("items");
                final Map<String, Object> map1 = new HashMap<>();
                map1.put("id", link.getId());
                map1.put("name", link.getName());
                map1.put(Constants.TITLE, link.getTitle());
                map1.put("icon", link.getIcon());
                map1.put(Constants.ROUTER, link.isRouter());
                map1.put("component", link.getName());
                existingMapList.add(map1);
                map2.put("items", existingMapList);
                list.set(parentidcount, map2);

              } else {
                final List<Map<String, Object>> existingMapList = new ArrayList<>();
                final Map<String, Object> map1 = new HashMap<>();

                map1.put("id", link.getId());
                map1.put("name", link.getName());
                map1.put(Constants.TITLE, link.getTitle());
                map1.put("icon", link.getIcon());
                map1.put(Constants.ROUTER, link.isRouter());
                map1.put("component", link.getName());
                existingMapList.add(map1);
                map2.put("items", existingMapList);
                list.set(parentidcount, map2);
              }
            }
          }
        }
    }
    return list;
  }

  @GetMapping(path = RouteConstants.GET_ROLE_ADMIN_DETAILS_BY_ORG)
  public final List<Map<String, Object>> getRoleAdminDetailsByOrg(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long orgId = this.jwtTokenUtil.getOrgIdFromToken(request);
    final Organisation organisation = this.organisationService.getOrganisationById(orgId);

    final long storeTempalteId = organisation.getStoreTemplateId();
    final StoreTemplate storeTempalte = storeTemplateService.getStoreTemplateById(storeTempalteId);
    final StoreType storeType = storeTempalte.getStoreType();

    final Integer[] linkArray = storeType.getLink();
    final List<Map<String, Object>> list = new ArrayList<>();

    for (final Integer linkId : linkArray) {
      Map<String, Object> map = new HashMap<>();
      try {
        final Link link = linkService.getLinkDetailsById(linkId);

        if (link.getRole().toString().equals("ROLE_ADMIN"))
          if (link.getParentId() == 0) {
            map.put("id", link.getId());
            map.put("name", link.getName());
            map.put(Constants.TITLE, link.getTitle());
            map.put("icon", link.getIcon());
            map.put(Constants.ROUTER, link.isRouter());
            list.add(map);
          } else {
            final long parentid = link.getParentId();
            if (list.size() == 0) {
              final List<Map<String, Object>> list1 = new ArrayList<>();
              final Link parentlink = linkService.getLinkDetailsById(parentid);
              map = new HashMap<>();
              map.put("id", parentlink.getId());
              map.put("name", parentlink.getName());
              map.put(Constants.TITLE, parentlink.getTitle());
              map.put("icon", parentlink.getIcon());
              map.put(Constants.ROUTER, link.isRouter());
              list.add(map);

              final Map<String, Object> map1 = new HashMap<>();
              map1.put("id", link.getId());
              map1.put("name", link.getName());
              map1.put(Constants.TITLE, link.getTitle());
              map1.put("icon", link.getIcon());
              list1.add(map1);
              map.put(Constants.ROUTER, link.isRouter());
              map.put("icon", list1);

            } else {
              int parentidcount = 0;
              for (int i = 0; i < list.size(); i++) {
                if (Long.parseLong(list.get(i).get("id").toString()) == parentid) {
                  parentidcount = i;
                  break;
                }
              }
              if (parentidcount == 0) {
                final List<Map<String, Object>> list1 = new ArrayList<>();
                final Link parentlink = linkService.getLinkDetailsById(parentid);
                map = new HashMap<>();
                map.put("id", parentlink.getId());
                map.put("name", parentlink.getName());
                map.put(Constants.TITLE, parentlink.getTitle());
                map.put("icon", parentlink.getIcon());
                map.put(Constants.ROUTER, link.isRouter());

                list.add(map);

                final Map<String, Object> map1 = new HashMap<>();
                map1.put("id", link.getId());
                map1.put("name", link.getName());
                map1.put(Constants.TITLE, link.getTitle());
                map1.put("icon", link.getIcon());
                map1.put("component", link.getName());
                map1.put(Constants.ROUTER, link.isRouter());
                list1.add(map1);
                map.put("items", list1);
              } else {
                final Map<String, Object> map2 = list.get(parentidcount);

                if (map2.containsKey("items")) {

                  final List<Map<String, Object>> existingMapList =
                      (List<Map<String, Object>>) map2.get("items");
                  final Map<String, Object> map1 = new HashMap<>();
                  map1.put("id", link.getId());
                  map1.put("name", link.getName());
                  map1.put(Constants.TITLE, link.getTitle());
                  map1.put("icon", link.getIcon());
                  map1.put(Constants.ROUTER, link.isRouter());
                  map1.put("component", link.getName());
                  existingMapList.add(map1);
                  map2.put("items", existingMapList);
                  list.set(parentidcount, map2);

                } else {
                  final List<Map<String, Object>> existingMapList = new ArrayList<>();
                  final Map<String, Object> map1 = new HashMap<>();

                  map1.put("id", link.getId());
                  map1.put("name", link.getName());
                  map1.put(Constants.TITLE, link.getTitle());
                  map1.put("icon", link.getIcon());
                  map1.put(Constants.ROUTER, link.isRouter());
                  map1.put("component", link.getName());
                  existingMapList.add(map1);
                  map2.put("items", existingMapList);
                  list.set(parentidcount, map2);
                }
              }
            }
          }
      } catch (final Exception erda) {
        map.put("items", "No items selected for selected organisation link");
        LOGGER.error("Organisation link details", erda, erda);
      }
    }

    return list;
  }

  @GetMapping(path = RouteConstants.GET_ROLE_EMPLOYEE_DETAILS_BY_ORG)
  public final List<Map<String, Object>> getRoleEmployeeDetailsByOrg(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long orgId = this.jwtTokenUtil.getOrgIdFromToken(request);
    final Organisation organisation = this.organisationService.getOrganisationById(orgId);

    final long storeTempalteId = organisation.getStoreTemplateId();
    final StoreTemplate storeTempalte = storeTemplateService.getStoreTemplateById(storeTempalteId);
    final StoreType storeType = storeTempalte.getStoreType();

    final Integer[] linkArray = storeType.getLink();
    final List<Map<String, Object>> list = new ArrayList<>();

    for (final Integer linkId : linkArray) {
      Map<String, Object> map = new HashMap<>();
      final Link link = linkService.getLinkDetailsById(linkId);
      if (link.getRole().toString().equals("ROLE_EMPLOYEE"))
        if (link.getParentId() == 0) {
          map.put("id", link.getId());
          map.put("name", link.getName());
          map.put(Constants.TITLE, link.getTitle());
          map.put("icon", link.getIcon());
          map.put(Constants.ROUTER, link.isRouter());
          list.add(map);
        } else {
          final long parentid = link.getParentId();
          if (list.size() == 0) {
            final List<Map<String, Object>> list1 = new ArrayList<>();
            final Link parentlink = linkService.getLinkDetailsById(parentid);
            map = new HashMap<>();
            map.put("id", parentlink.getId());
            map.put("name", parentlink.getName());
            map.put(Constants.TITLE, parentlink.getTitle());
            map.put("icon", parentlink.getIcon());
            map.put(Constants.ROUTER, link.isRouter());
            list.add(map);

            final Map<String, Object> map1 = new HashMap<>();
            map1.put("id", link.getId());
            map1.put("name", link.getName());
            map1.put(Constants.TITLE, link.getTitle());
            map1.put("icon", link.getIcon());
            list1.add(map1);
            map.put(Constants.ROUTER, link.isRouter());
            map.put("icon", list1);

          } else {
            int parentidcount = 0;
            for (int i = 0; i < list.size(); i++) {
              if (Long.parseLong(list.get(i).get("id").toString()) == parentid) {
                parentidcount = i;
                break;
              }
            }
            if (parentidcount == 0) {
              final List<Map<String, Object>> list1 = new ArrayList<>();
              final Link parentlink = linkService.getLinkDetailsById(parentid);
              map = new HashMap<>();
              map.put("id", parentlink.getId());
              map.put("name", parentlink.getName());
              map.put(Constants.TITLE, parentlink.getTitle());
              map.put("icon", parentlink.getIcon());
              map.put(Constants.ROUTER, link.isRouter());

              list.add(map);

              final Map<String, Object> map1 = new HashMap<>();
              map1.put("id", link.getId());
              map1.put("name", link.getName());
              map1.put(Constants.TITLE, link.getTitle());
              map1.put("icon", link.getIcon());
              map1.put("component", link.getName());
              map1.put(Constants.ROUTER, link.isRouter());
              list1.add(map1);
              map.put("items", list1);
            } else {
              final Map<String, Object> map2 = list.get(parentidcount);

              if (map2.containsKey("items")) {

                final List<Map<String, Object>> existingMapList =
                    (List<Map<String, Object>>) map2.get("items");
                final Map<String, Object> map1 = new HashMap<>();
                map1.put("id", link.getId());
                map1.put("name", link.getName());
                map1.put(Constants.TITLE, link.getTitle());
                map1.put("icon", link.getIcon());
                map1.put(Constants.ROUTER, link.isRouter());
                map1.put("component", link.getName());
                existingMapList.add(map1);
                map2.put("items", existingMapList);
                list.set(parentidcount, map2);

              } else {
                final List<Map<String, Object>> existingMapList = new ArrayList<>();
                final Map<String, Object> map1 = new HashMap<>();

                map1.put("id", link.getId());
                map1.put("name", link.getName());
                map1.put(Constants.TITLE, link.getTitle());
                map1.put("icon", link.getIcon());
                map1.put(Constants.ROUTER, link.isRouter());
                map1.put("component", link.getName());
                existingMapList.add(map1);
                map2.put("items", existingMapList);
                list.set(parentidcount, map2);
              }
            }
          }
        }
    }
    return list;
  }

  @GetMapping(path = RouteConstants.GET_OFFLINE_ADMIN_LINKS)
  public final List<Map<String, Object>> getOfflineAdminLinks(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long orgId = this.jwtTokenUtil.getOrgIdFromToken(request);
    final Organisation organisation = this.organisationService.getOrganisationById(orgId);

    final long storeTempalteId = organisation.getStoreTemplateId();
    final StoreTemplate storeTempalte = storeTemplateService.getStoreTemplateById(storeTempalteId);
    final StoreType storeType = storeTempalte.getStoreType();

    final Integer[] offlinelinks = storeType.getOfflineLink();
    final List<Map<String, Object>> list = new ArrayList<>();

    for (final Integer linkId : offlinelinks) {
      Map<String, Object> map = new HashMap<>();
      try {
        final Link link = linkService.getLinkDetailsById(linkId);
        if (link.getRole().toString().equals("OFFLINE_ROLE_ADMIN"))
          if (link.getParentId() == 0) {
            map.put("id", link.getId());
            map.put("name", link.getName());
            map.put(Constants.TITLE, link.getTitle());
            map.put("icon", link.getIcon());
            map.put(Constants.ROUTER, link.isRouter());
            list.add(map);
          } else {
            final long parentid = link.getParentId();
            if (list.size() == 0) {
              final List<Map<String, Object>> list1 = new ArrayList<>();
              final Link parentlink = linkService.getLinkDetailsById(parentid);
              map = new HashMap<>();
              map.put("id", parentlink.getId());
              map.put("name", parentlink.getName());
              map.put(Constants.TITLE, parentlink.getTitle());
              map.put("icon", parentlink.getIcon());
              map.put(Constants.ROUTER, link.isRouter());
              list.add(map);

              final Map<String, Object> map1 = new HashMap<>();
              map1.put("id", link.getId());
              map1.put("name", link.getName());
              map1.put(Constants.TITLE, link.getTitle());
              map1.put("icon", link.getIcon());
              list1.add(map1);
              map.put(Constants.ROUTER, link.isRouter());
              map.put("icon", list1);

            } else {
              int parentidcount = 0;
              for (int i = 0; i < list.size(); i++) {
                if (Long.parseLong(list.get(i).get("id").toString()) == parentid) {
                  parentidcount = i;
                  break;
                }
              }
              if (parentidcount == 0) {
                final List<Map<String, Object>> list1 = new ArrayList<>();
                final Link parentlink = linkService.getLinkDetailsById(parentid);
                map = new HashMap<>();
                map.put("id", parentlink.getId());
                map.put("name", parentlink.getName());
                map.put(Constants.TITLE, parentlink.getTitle());
                map.put("icon", parentlink.getIcon());
                map.put(Constants.ROUTER, link.isRouter());

                list.add(map);

                final Map<String, Object> map1 = new HashMap<>();
                map1.put("id", link.getId());
                map1.put("name", link.getName());
                map1.put(Constants.TITLE, link.getTitle());
                map1.put("icon", link.getIcon());
                map1.put("component", link.getName());
                map1.put(Constants.ROUTER, link.isRouter());
                list1.add(map1);
                map.put("items", list1);
              } else {
                final Map<String, Object> map2 = list.get(parentidcount);

                if (map2.containsKey("items")) {

                  final List<Map<String, Object>> existingMapList =
                      (List<Map<String, Object>>) map2.get("items");
                  final Map<String, Object> map1 = new HashMap<>();
                  map1.put("id", link.getId());
                  map1.put("name", link.getName());
                  map1.put(Constants.TITLE, link.getTitle());
                  map1.put("icon", link.getIcon());
                  map1.put(Constants.ROUTER, link.isRouter());
                  map1.put("component", link.getName());
                  existingMapList.add(map1);
                  map2.put("items", existingMapList);
                  list.set(parentidcount, map2);

                } else {
                  final List<Map<String, Object>> existingMapList = new ArrayList<>();
                  final Map<String, Object> map1 = new HashMap<>();

                  map1.put("id", link.getId());
                  map1.put("name", link.getName());
                  map1.put(Constants.TITLE, link.getTitle());
                  map1.put("icon", link.getIcon());
                  map1.put(Constants.ROUTER, link.isRouter());
                  map1.put("component", link.getName());
                  existingMapList.add(map1);
                  map2.put("items", existingMapList);
                  list.set(parentidcount, map2);
                }
              }
            }
          }
      } catch (final Exception erda) {
        map.put("items", "No items selected for selected organisation link");
        LOGGER.error("Organisation link details", erda, erda);
      }
    }
    return list;
  }

  @GetMapping(path = RouteConstants.GET_OFFLINE_EMPLOYEE_LINKS)
  public final List<Map<String, Object>> getOfflineEmployeeLinks(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long orgId = this.jwtTokenUtil.getOrgIdFromToken(request);
    final Organisation organisation = this.organisationService.getOrganisationById(orgId);

    final long storeTempalteId = organisation.getStoreTemplateId();
    final StoreTemplate storeTempalte = storeTemplateService.getStoreTemplateById(storeTempalteId);
    final StoreType storeType = storeTempalte.getStoreType();

    final Integer[] offlinelinks = storeType.getOfflineLink();
    final List<Map<String, Object>> list = new ArrayList<>();

    for (final Integer linkId : offlinelinks) {
      Map<String, Object> map = new HashMap<>();
      try {
        final Link link = linkService.getLinkDetailsById(linkId);
        if (link.getRole().toString().equals("OFFLINE_ROLE_EMPLOYEE"))
          if (link.getParentId() == 0) {
            map.put("id", link.getId());
            map.put("name", link.getName());
            map.put(Constants.TITLE, link.getTitle());
            map.put("icon", link.getIcon());
            map.put(Constants.ROUTER, link.isRouter());
            list.add(map);
          } else {
            final long parentid = link.getParentId();
            if (list.size() == 0) {
              final List<Map<String, Object>> list1 = new ArrayList<>();
              final Link parentlink = linkService.getLinkDetailsById(parentid);
              map = new HashMap<>();
              map.put("id", parentlink.getId());
              map.put("name", parentlink.getName());
              map.put(Constants.TITLE, parentlink.getTitle());
              map.put("icon", parentlink.getIcon());
              map.put(Constants.ROUTER, link.isRouter());
              list.add(map);

              final Map<String, Object> map1 = new HashMap<>();
              map1.put("id", link.getId());
              map1.put("name", link.getName());
              map1.put(Constants.TITLE, link.getTitle());
              map1.put("icon", link.getIcon());
              list1.add(map1);
              map.put(Constants.ROUTER, link.isRouter());
              map.put("icon", list1);

            } else {
              int parentidcount = 0;
              for (int i = 0; i < list.size(); i++) {
                if (Long.parseLong(list.get(i).get("id").toString()) == parentid) {
                  parentidcount = i;
                  break;
                }
              }
              if (parentidcount == 0) {
                final List<Map<String, Object>> list1 = new ArrayList<>();
                final Link parentlink = linkService.getLinkDetailsById(parentid);
                map = new HashMap<>();
                map.put("id", parentlink.getId());
                map.put("name", parentlink.getName());
                map.put(Constants.TITLE, parentlink.getTitle());
                map.put("icon", parentlink.getIcon());
                map.put(Constants.ROUTER, link.isRouter());

                list.add(map);

                final Map<String, Object> map1 = new HashMap<>();
                map1.put("id", link.getId());
                map1.put("name", link.getName());
                map1.put(Constants.TITLE, link.getTitle());
                map1.put("icon", link.getIcon());
                map1.put("component", link.getName());
                map1.put(Constants.ROUTER, link.isRouter());
                list1.add(map1);
                map.put("items", list1);
              } else {
                final Map<String, Object> map2 = list.get(parentidcount);

                if (map2.containsKey("items")) {

                  final List<Map<String, Object>> existingMapList =
                      (List<Map<String, Object>>) map2.get("items");
                  final Map<String, Object> map1 = new HashMap<>();
                  map1.put("id", link.getId());
                  map1.put("name", link.getName());
                  map1.put(Constants.TITLE, link.getTitle());
                  map1.put("icon", link.getIcon());
                  map1.put(Constants.ROUTER, link.isRouter());
                  map1.put("component", link.getName());
                  existingMapList.add(map1);
                  map2.put("items", existingMapList);
                  list.set(parentidcount, map2);

                } else {
                  final List<Map<String, Object>> existingMapList = new ArrayList<>();
                  final Map<String, Object> map1 = new HashMap<>();

                  map1.put("id", link.getId());
                  map1.put("name", link.getName());
                  map1.put(Constants.TITLE, link.getTitle());
                  map1.put("icon", link.getIcon());
                  map1.put(Constants.ROUTER, link.isRouter());
                  map1.put("component", link.getName());
                  existingMapList.add(map1);
                  map2.put("items", existingMapList);
                  list.set(parentidcount, map2);
                }
              }
            }
          }
      } catch (final Exception erda) {
        map.put("items", "No items selected for selected organisation link");
        LOGGER.error("Organisation link details", erda, erda);
      }
    }
    return list;
  }
}
