package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.InvoiceDetails;
import com.a2zbill.domain.ReOrder;
import com.a2zbill.domain.ReverseOrder;
import com.a2zbill.services.CounterProductsService;
import com.a2zbill.services.InvoiceDetailsService;
import com.a2zbill.services.ReverseOrderService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReverseOrderController {
  private static final Logger LOGGER = LoggerFactory.getLogger(ReverseOrderController.class);

  @Autowired JwtTokenUtil jwtTokenUtil;
  @Autowired EmployeeService employeeService;
  @Autowired ReverseOrderService reverseOrderService;
  @Autowired InvoiceDetailsService invoiceDetailsService;
  @Autowired CounterProductsService counterProductsService;

  @PostMapping(path = RouteConstants.SAVE_REVERSE_ORDER_DETAILS)
  public final void saveReverseOrder(
      @RequestBody String payload,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {
    long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    Employee employeeDetails = this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    JSONArray reOrderArray = new JSONArray(payload);
    ReverseOrder reverseOrder = new ReverseOrder();
    try {
      for (int i = 0; i < reOrderArray.length(); i++) {
        JSONObject reOrderjson = (JSONObject) reOrderArray.get(i);
        long reOrderId = reOrderjson.getLong("id");
        ReOrder reOrder = this.counterProductsService.getReorderDetailsByReorderId(reOrderId);
        reOrder.setStatus(Constants.REVERSED);
        this.counterProductsService.updateReorder(reOrder);
        InvoiceDetails invoice = new InvoiceDetails();
        invoice.setBranch(employeeDetails.getBranch());
        invoice.setOrganisation(employeeDetails.getOrganisation());
        invoice.setEndDate(new Date());
        invoice.setStartDate(new Date());
        invoice.setStatus(Constants.REVERSED);
        invoice.setReason(reOrderjson.getString(Constants.REASON));
        this.invoiceDetailsService.save(invoice);
        reverseOrder.setBranch(employeeDetails.getBranch());
        reverseOrder.setOrganisation(employeeDetails.getOrganisation());
        reverseOrder.setInvoiceId(reOrder.getInvoiceDetails());
        reverseOrder.setReorderId(reOrder);
        reverseOrder.setReverseInvoiceId(invoice);
        reverseOrder.setReason(reOrderjson.getString(Constants.REASON));
        this.reverseOrderService.save(reverseOrder);
      }
    } catch (Exception ex) {
      LOGGER.error(Constants.MARKER, "save ReverseOrder" + ex);
    }
  }

  @PostMapping(path = RouteConstants.REVERSE_INVOICEID)
  public final void reverseInvoiceId(
      @RequestBody String payload, final HttpServletRequest request, HttpServletResponse response)
      throws JSONException {
    long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    Employee employeeDetails = this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    JSONObject jsonObject = new JSONObject(payload);
    ReverseOrder reverseOrder = new ReverseOrder();
    try {
      long invoiceId = jsonObject.getLong("invoiceId");
      Organisation organisation = employeeDetails.getOrganisation();
      Branch branch = employeeDetails.getBranch();
      List<ReOrder> list = null;
      if (branch == null) {
        list =
            this.counterProductsService.getReorderListByInvoiceIdAndOrganisationId(
                invoiceId, organisation.getId());
      } else {
        list =
            this.counterProductsService.getReorderListByInvoiceIdAndOrganisationIdAndBranchId(
                invoiceId, organisation.getId(), branch.getId());
      }
      for (int i = 0; i < list.size(); i++) {
        ReOrder reOrder = list.get(i);
        reOrder.setStatus(Constants.REVERSED);
        this.counterProductsService.updateReorder(reOrder);
        InvoiceDetails invoice = new InvoiceDetails();
        invoice.setBranch(employeeDetails.getBranch());
        invoice.setOrganisation(employeeDetails.getOrganisation());
        invoice.setEndDate(new Date());
        invoice.setStartDate(new Date());
        invoice.setStatus(Constants.REVERSED);
        invoice.setReason(jsonObject.getString(Constants.REASON));
        this.invoiceDetailsService.save(invoice);
        reverseOrder.setBranch(employeeDetails.getBranch());
        reverseOrder.setOrganisation(employeeDetails.getOrganisation());
        reverseOrder.setInvoiceId(reOrder.getInvoiceDetails());
        reverseOrder.setReorderId(reOrder);
        reverseOrder.setReverseInvoiceId(invoice);
        reverseOrder.setReason(jsonObject.getString(Constants.REASON));
        this.reverseOrderService.save(reverseOrder);
      }
    } catch (Exception ex) {
      LOGGER.error(Constants.MARKER, "save ReverseOrder" + ex);
    }
  }

  @PostMapping(path = RouteConstants.CANCLE_INVOICEID)
  public final void cancelInvoiceId(
      @RequestBody String payload, final HttpServletRequest request, HttpServletResponse response)
      throws JSONException {
    long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    Employee employeeDetails = this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    JSONObject jsonObject = new JSONObject(payload);
    ReverseOrder reverseOrder = new ReverseOrder();
    try {
      long invoiceId = jsonObject.getLong("invoiceId");
      Organisation organisation = employeeDetails.getOrganisation();
      Branch branch = employeeDetails.getBranch();
      List<ReOrder> list = null;
      if (branch == null) {
        list =
            this.counterProductsService.getReorderListByInvoiceIdAndOrganisationId(
                invoiceId, organisation.getId());
      } else {
        list =
            this.counterProductsService.getReorderListByInvoiceIdAndOrganisationIdAndBranchId(
                invoiceId, organisation.getId(), branch.getId());
      }
      for (int i = 0; i < list.size(); i++) {
        ReOrder reOrder = list.get(i);
        reOrder.setStatus(Constants.CANCLED);
        this.counterProductsService.updateReorder(reOrder);
        InvoiceDetails invoice = new InvoiceDetails();
        invoice.setBranch(employeeDetails.getBranch());
        invoice.setOrganisation(employeeDetails.getOrganisation());
        invoice.setEndDate(new Date());
        invoice.setStartDate(new Date());
        invoice.setStatus(Constants.CANCLED);
        invoice.setReason(jsonObject.getString(Constants.REASON));
        this.invoiceDetailsService.save(invoice);
        reverseOrder.setBranch(employeeDetails.getBranch());
        reverseOrder.setOrganisation(employeeDetails.getOrganisation());
        reverseOrder.setInvoiceId(reOrder.getInvoiceDetails());
        reverseOrder.setReorderId(reOrder);
        reverseOrder.setReverseInvoiceId(invoice);
        reverseOrder.setReason(jsonObject.getString(Constants.REASON));
        this.reverseOrderService.save(reverseOrder);
      }
    } catch (Exception ex) {
      LOGGER.info(Constants.MARKER, "save ReverseOrder" + ex);
    }
  }

  @PostMapping(path = RouteConstants.CANCLE_INVOICE)
  public final void cancelInvoice(
      @RequestBody String payload,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {
    long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    Employee employeeDetails = this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    JSONArray reOrderArray = new JSONArray(payload);
    ReverseOrder reverseOrder = new ReverseOrder();
    try {
      for (int i = 0; i < reOrderArray.length(); i++) {
        JSONObject reOrderjson = (JSONObject) reOrderArray.get(i);
        long reOrderId = reOrderjson.getLong("id");
        ReOrder reOrder = this.counterProductsService.getReorderDetailsByReorderId(reOrderId);
        reOrder.setStatus(Constants.CANCLED);
        this.counterProductsService.updateReorder(reOrder);
        InvoiceDetails invoice = new InvoiceDetails();
        invoice.setBranch(employeeDetails.getBranch());
        invoice.setOrganisation(employeeDetails.getOrganisation());
        invoice.setEndDate(new Date());
        invoice.setStartDate(new Date());
        invoice.setStatus(Constants.CANCLED);
        invoice.setReason(reOrderjson.getString(Constants.REASON));
        this.invoiceDetailsService.save(invoice);
        reverseOrder.setBranch(employeeDetails.getBranch());
        reverseOrder.setOrganisation(employeeDetails.getOrganisation());
        reverseOrder.setInvoiceId(reOrder.getInvoiceDetails());
        reverseOrder.setReorderId(reOrder);
        reverseOrder.setReverseInvoiceId(invoice);
        reverseOrder.setReason(reOrderjson.getString(Constants.REASON));
        this.reverseOrderService.save(reverseOrder);
      }
    } catch (Exception ex) {
      LOGGER.info(Constants.MARKER, "cancel Invoice" + ex);
    }
  }
}
