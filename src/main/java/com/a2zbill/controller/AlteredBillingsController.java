package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.AlteredBillings;
import com.a2zbill.services.AlteredBillingsService;
import com.a2zbill.services.BillingService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("unused")
public class AlteredBillingsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(AlteredBillingsController.class);

  @Autowired private AlteredBillingsService alteredbillingsservice;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private BillingService billingService;

  @Autowired private EmployeeService employeeService;

  @PostMapping(path = RouteConstants.ALTER_BILL_SAVE)
  public final AlteredBillings addAlteredBillings(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException, Exception {
    final AlteredBillings alteredBillings = new AlteredBillings();
    final Map<String, Object> map = new HashMap<>();
    try {
      final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final JSONObject billings = new JSONObject(payload);

      final long billId = billings.getLong("billing id");
      final String dateString = billings.getString("date");
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      Date date = sdf.parse(dateString);
      final String itemStatus = billings.getString("itemStatus");
      map.put("itemsAdded", billings.getString("itemsAdded"));
      map.put("itemsRemoved", billings.getString("itemsRemoved"));
      String oldTotalAmount = billings.getString("oldTotalAmount");
      String newTotalAmount = billings.getString("newTotalAmount");

      alteredBillings.setDate(date);
      alteredBillings.setItemsAdded(map);
      alteredBillings.setItemsRemoved(map);
      alteredBillings.setCreatedDate(new Date());
      alteredBillings.setModifiedDate(new Date());
      alteredBillings.setItemstatus(itemStatus);
      alteredBillings.setNewTotalAmount(newTotalAmount);
      alteredBillings.setOldTotalAmount(oldTotalAmount);
      alteredBillings.setBillingDetails(billingService.getBillingDetailsByBillId(billId));
      this.alteredbillingsservice.save(alteredBillings);

    } catch (final Exception ex) {
      LOGGER.error("addAlteredbillings" + ex);
    }
    return alteredBillings;
  }

  @GetMapping(path = RouteConstants.ALTER_BILL_DETAILS)
  public final ResponseEntity<Object> getAlteredbillingsDetails() {

    List<AlteredBillings> alteredBillingsDetails = new ArrayList<>();
    try {
      alteredBillingsDetails = this.alteredbillingsservice.getAllAlteredBillings();

    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getAlteredBillingDetails" + e);
    }

    return new ResponseEntity<>(alteredBillingsDetails, HttpStatus.OK);
  }
}
