package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.Counter;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CartService;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.impl.TokenServiceImpl;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CounterPaymentController {
  private static final Logger LOGGER = LoggerFactory.getLogger(CounterPaymentController.class);
  private static final String CART_STATUS = "Open";

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private CounterService counterService;

  @Autowired private CartService cartService;

  @Autowired private BillingService billingService;

  @Autowired private TokenServiceImpl tokenServiceImpl;

  @GetMapping(path = RouteConstants.CREATE_CART_LIST)
  public final Cart generateCart(
      final HttpServletRequest request, final HttpServletResponse responce) {

    Cart cartDetails = null;
    try {
      final long counterNumber = Long.parseLong(request.getParameter("counterNumber"));
      final Counter counterDetails = this.counterService.getCounterDetailsById(counterNumber);
      cartDetails = this.cartService.getCartDetailsOnStatus(CART_STATUS, counterNumber);
      if (cartDetails != null) {
        return cartDetails;
      } else {
        cartDetails = new Cart();
        cartDetails.setCartStatus(CART_STATUS);
        final Date date = new Date();
        cartDetails.setStartTime(date);
        cartDetails.setEndTime(date);
        cartDetails.setCounterDetails(counterDetails);
        this.cartService.save(cartDetails);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "generateCart" + ex);
    }
    return cartDetails;
  }

  public final List<Billing> gerReports(final String reports, final long orgId)
      throws ParseException {
    List<Billing> billingDetails = null;
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date todate = new Date();
    final Calendar cal = Calendar.getInstance();
    cal.setTime(todate);
    final int month = cal.get(Calendar.MONTH) + 1;
    final int year = cal.get(Calendar.YEAR);
    if (reports.equalsIgnoreCase("day")) {
      billingDetails = this.billingService.getBillingsByOrgIdByFromdate(todate, orgId);
    }
    if (reports.equalsIgnoreCase("month")) {
      final String monthDate = year + "-" + month + "-" + "01";
      final Date fromDate = sdf.parse(monthDate);
      billingDetails =
          this.billingService.getBillingsByCurrentDateAndOrgId(fromDate, todate, orgId);
    }
    if (reports.equalsIgnoreCase("year")) {
      final String yearDate = year + "-" + "01-" + "01";
      final Date fromDate = sdf.parse(yearDate);
      billingDetails =
          this.billingService.getBillingsByCurrentDateAndOrgId(fromDate, todate, orgId);
    }
    return billingDetails;
  }

  @PostMapping(path = RouteConstants.COUNTER_PAYMENT_LIST)
  public final List<Billing> getCounterPaymentDetails(
      @RequestBody final String payload, final HttpServletRequest request)
      throws JSONException, ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    List<Billing> billingDetails = new ArrayList<Billing>();

    try {

      final JSONObject jsonObject = new JSONObject(payload);
      final List<Date> billDates = this.tokenServiceImpl.getReportDates(jsonObject);
      final String branchId = jsonObject.getString("branchId");
      final String counterId = jsonObject.getString("counterId");
      final Date fromDate = billDates.get(0);
      final Date toDate = billDates.get(1);

      if (branchId == null) {
        if (counterId.isEmpty()) {
          billingDetails =
              this.billingService.getBillingsByCurrentDateAndOrgId(fromDate, toDate, orgId);
        } else {
          final long counterNumber = Long.parseLong(counterId);
          billingDetails =
              this.billingService.getBillingDetailsByDatesAndOrgIdAndCounterNumber(
                  fromDate, toDate, orgId, counterNumber);
        }
      } else {
        if (counterId.isEmpty()) {
          billingDetails =
              this.billingService.getBillingsByStartAndEndDateAndOrgAndBranch(
                  fromDate, toDate, orgId, Long.parseLong(branchId));
        } else {
          final long counterNumber = Long.parseLong(counterId);
          billingDetails =
              this.billingService.getBillingDetailsByDatesAndOrgIdAndBranchIdAndCounterNumber(
                  fromDate, toDate, orgId, Long.parseLong(branchId), counterNumber);
        }
      }
      if (billingDetails == null) {
        LOGGER.warn("Currenty Not Available any Transactions for this date and counter");
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getCounterPaymentDetails" + ex);
    }
    return billingDetails;
  }
}
