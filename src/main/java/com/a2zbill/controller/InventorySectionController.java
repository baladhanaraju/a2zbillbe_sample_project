package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.InventoryHistory;
import com.a2zbill.domain.InventoryReports;
import com.a2zbill.domain.InventorySection;
import com.a2zbill.domain.InventorySectionData;
import com.a2zbill.domain.InventorySectionDataHistory;
import com.a2zbill.domain.InvoiceBill;
import com.a2zbill.domain.Product;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.InventoryHistoryService;
import com.a2zbill.services.InventoryReportsService;
import com.a2zbill.services.InventorySectionDataHistoryService;
import com.a2zbill.services.InventorySectionDataService;
import com.a2zbill.services.InventorySectionService;
import com.a2zbill.services.InvoiceBillService;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.impl.TokenServiceImpl;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventorySectionController {

  private static final Logger LOGGER = LoggerFactory.getLogger(InventorySectionController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private InventorySectionService inventorySectionService;

  @Autowired private InventorySectionDataHistoryService inventorySectionDataHistoryService;

  @Autowired private InventoryDetailService inventoryService;

  @Autowired private InventorySectionDataService inventorySectionDataService;

  @Autowired private ProductService productsService;

  @Autowired private BranchService branchService;

  @Autowired private TokenServiceImpl tokenServiceImpl;

  @Autowired private InvoiceBillService invoiceBillService;

  @Autowired private InventoryHistoryService inventoryHistoryService;

  @Autowired private InventoryDetailService inventoryDetailService;

  @Autowired private InventoryReportsService inventoryReportsService;

  @GetMapping(path = RouteConstants.INVENTORY_SECTION_BY_ORG)
  public final List<InventorySection> getInventorySectionByOrganisation(
      final HttpServletRequest request) throws Exception {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    List<InventorySection> list = null;
    final long orgId = employeeDetails.getOrganisation().getId();

    try {
      final Branch branch = employeeDetails.getBranch();
      if (branch == null) {
        list = this.inventorySectionService.getAllInventorySectionByOrgId(orgId);
      } else {
        list =
            this.inventorySectionService.getAllInventorySectionByOrgIdBranchId(
                orgId, branch.getId());
      }

    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInventorySectionByOrganisation " + ex);
    }
    return list;
  }

  @PostMapping(path = RouteConstants.SAVE_INVENTORY_SECTION_DETAILS)
  public final Map<String, Object> saveInventorySection(
      @RequestBody final String payload, final HttpServletRequest request) throws JSONException {
    final Map<String, Object> map = new HashMap<>();
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    final InventorySection inventorySection = new InventorySection();
    final JSONObject json = new JSONObject(payload);
    final String inventorySectionName = json.getString("inventorySectionName");
    final String consumedFlag = json.getString(Constants.CONSUMEDFLAG);
    final String kotFlag = json.getString(Constants.KOTFLAG);
    final Branch branch = this.branchService.getBranchById(json.getLong(Constants.BRANCH));
    final InventorySection inventorySectionDetails =
        this.inventorySectionService.getInventorySectionBySectionNameOrgIdBranchId(
            inventorySectionName, orgId, json.getLong(Constants.BRANCH));
    if (inventorySectionDetails != null) {
      map.put(Constants.MESSAGE, "Already Section Name Exist For this Organisation and Branch");
      return map;
    } else {
      inventorySection.setConsumedFlag(Boolean.parseBoolean(consumedFlag));
      inventorySection.setKotFlag(Boolean.parseBoolean(kotFlag));
      inventorySection.setSectionName(inventorySectionName);
      inventorySection.setBranch(branch);
      inventorySection.setOrganisation(employeeDetails.getOrganisation());
      this.inventorySectionService.save(inventorySection);
      map.put(Constants.MESSAGE, inventorySectionName + " Section Details saved successfully");
    }

    return map;
  }

  @SuppressWarnings("null")
  @PostMapping(path = RouteConstants.SAVE_INVENTORY_SECTION_DATA)
  public final Map<String, String> saveInventorySectionDataByOrganisation(
      @RequestBody final String payload, final HttpServletRequest request)
      throws JSONException, ParseException {
    final Map<String, String> map = new HashMap<>();
    final JSONObject json = new JSONObject(payload);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final String referenceNumber = json.getString(Constants.REFERENCENUMBER);
    final String date = json.getString("date");
    final long fromBranchId = json.getLong(Constants.FORMID);
    final long toBranchId = json.getLong("toId"); // to branchId
    final JSONArray ja = new JSONArray(json.getString("items"));
    Branch branch = null;
    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
    final Date movedDate = sdf.parse(date);
    final Date inventorySectionDate = new Date();
    final long orgId = employeeDetails.getOrganisation().getId();
    final String fromType = json.getString(Constants.FORMTYPE);
    final String toType = json.getString(Constants.TOTYPE); // totype
    if (toType.contentEquals(Constants.SECTION)) {
      List<InventorySectionDataHistory> list = null;
      list =
          inventorySectionDataHistoryService
              .getInventorySectionDataHistoryByOrgIdAndReferenceNumber(orgId, referenceNumber);
      if (list.size() > 0) {
        map.put(Constants.MESSAGE, Constants.ALREADYEXISTS);
        map.put("flag", Constants.FALSE);
        return map;
      }
    } else if (toType.contentEquals(Constants.BRANCH)) {

      InvoiceBill invoiceBill = invoiceBillService.getInvoiceBillByInvoiceNumber(referenceNumber);
      if (invoiceBill != null) {
        map.put(Constants.MESSAGE, Constants.ALREADYEXISTS);
        map.put("flag", Constants.FALSE);
        return map;
      }
    }
    if (fromType.contentEquals(Constants.SECTION)) {
      try {
        final long inventorySectionId = json.getLong(Constants.FROMSECTIONID);
        branch = branchService.getBranchById(fromBranchId);
        for (int i = 0; i < ja.length(); ++i) {
          Product product = null;
          InventorySection consumedFlag = null;
          InventorySectionData inventorySectionData = null;
          final JSONObject items = ja.getJSONObject(i);
          product = this.productsService.getProductFeaturesById(items.getLong(Constants.PRODUCTID));
          consumedFlag =
              this.inventorySectionService.getInventorySectionByOrgIdBranchIdAndId(
                  orgId, fromBranchId, inventorySectionId);
          inventorySectionData =
              this.inventorySectionDataService.getInventorySectionDataByproductIdAndOrgIdBranchId(
                  consumedFlag.getSectionName(), product.getId(), orgId, fromBranchId);
          if (inventorySectionData.getQuantity().doubleValue()
              >= BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)).doubleValue()) {
            inventorySectionData.setModifiedDate(inventorySectionDate);
            final BigDecimal qty =
                inventorySectionData
                    .getQuantity()
                    .subtract(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
            inventorySectionData.setQuantity(qty);
            this.inventorySectionDataService.update(inventorySectionData);

          } else {
            map.put(
                Constants.MESSAGE,
                "product Quantity in inventorySectionData is lessthan to Your Given Quantity ");
            return map;
          }
        }
      } catch (final Exception e) {
        LOGGER.error(Constants.MARKER, Constants.SAVEINVENTORYSECTION + e);
      }
    } else if (fromType.contentEquals(Constants.BRANCH)) {
      try {
        for (int i = 0; i < ja.length(); ++i) {
          Product product = null;
          InventoryDetail inventoryDetail = null;
          final JSONObject items = ja.getJSONObject(i);
          product = this.productsService.getProductFeaturesById(items.getLong(Constants.PRODUCTID));
          inventoryDetail =
              this.inventoryService.getInventoryDetailsByProductIdAndBranchIdAndOrgId(
                  product.getId(), fromBranchId, orgId);

          if (inventoryDetail.getQuantity().doubleValue()
              >= BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)).doubleValue()) {
            final BigDecimal reducedQuantity =
                inventoryDetail
                    .getQuantity()
                    .subtract(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
            inventoryDetail.setQuantity(reducedQuantity);
            this.inventoryService.update(inventoryDetail);
            if (items.has(Constants.EXPIRYDATE)
                && !items.getString(Constants.EXPIRYDATE).equals("")) {
              final Date expiryDate = sdf.parse(items.getString(Constants.EXPIRYDATE));
              final List<InventoryHistory> list =
                  inventoryHistoryService
                      .getInventoryHsitoryDetailsByExpiryDateAndProductIdAndOrgIdAndBranchIdOrderByAvailableQuantity(
                          expiryDate, product.getId(), orgId, fromBranchId);
              BigDecimal quantity = BigDecimal.valueOf(items.getDouble(Constants.QUANTITY));
              for (final int j = 0; quantity.doubleValue() > 0; ) {
                final InventoryHistory inventoryHistory = list.get(j);
                final BigDecimal availableQuantity =
                    inventoryHistory
                        .getQuantity()
                        .subtract(
                            inventoryHistory
                                .getConsumedCount()
                                .add(inventoryHistory.getExpiryCount()));
                if (availableQuantity.doubleValue() > quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = new BigDecimal(0);
                } else if (availableQuantity.doubleValue() < quantity.doubleValue()) {
                  quantity = quantity.subtract(availableQuantity);
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(availableQuantity));
                } else if (availableQuantity.doubleValue() == quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = BigDecimal.valueOf(0);
                }
                inventoryHistoryService.update(inventoryHistory);
              }
            } else {
              final List<InventoryHistory> list =
                  inventoryHistoryService
                      .getInventoryHistoryDetailsByProductIdAndOrgIdAndBranchIdOrderByExpiryDate(
                          product.getId(), orgId, fromBranchId);
              BigDecimal quantity = BigDecimal.valueOf(items.getDouble(Constants.QUANTITY));
              for (final int j = 0; quantity.doubleValue() > 0; ) {
                final InventoryHistory inventoryHistory = list.get(j);
                final BigDecimal availableQuantity =
                    inventoryHistory
                        .getQuantity()
                        .subtract(
                            inventoryHistory
                                .getConsumedCount()
                                .add(inventoryHistory.getExpiryCount()));
                if (availableQuantity.doubleValue() > quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = new BigDecimal(0);
                } else if (availableQuantity.doubleValue() < quantity.doubleValue()) {
                  quantity = quantity.subtract(availableQuantity);
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(availableQuantity));
                } else if (availableQuantity.doubleValue() == quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = BigDecimal.valueOf(0);
                }
                inventoryHistoryService.update(inventoryHistory);
              }
            }
          } else {
            map.put(
                Constants.MESSAGE,
                "product Quantity in invetory is lessthan to Your Given Quantity ");
            return map;
          }
        }
      } catch (final Exception e) {
        LOGGER.error(Constants.MARKER, Constants.SAVEINVENTORYSECTION + e);
      }
    }
    if (toType.contentEquals(Constants.SECTION)) {
      try {
        final long inventorySectionId = json.getLong(Constants.TOSECTIONID);
        branch = branchService.getBranchById(toBranchId);
        for (int i = 0; i < ja.length(); ++i) {
          Product product = null;
          final JSONObject items = ja.getJSONObject(i);
          InventorySection consumedFlag = null;
          InventorySectionData inventorySectionData = null;
          final InventorySectionDataHistory inventorySectionDataHistory =
              new InventorySectionDataHistory();
          product = this.productsService.getProductFeaturesById(items.getLong(Constants.PRODUCTID));
          consumedFlag =
              this.inventorySectionService.getInventorySectionByOrgIdBranchIdAndId(
                  orgId, toBranchId, inventorySectionId);

          inventorySectionData =
              this.inventorySectionDataService.getInventorySectionDataByproductIdAndOrgIdBranchId(
                  consumedFlag.getSectionName(), product.getId(), orgId, toBranchId);
          if (inventorySectionData != null) {
            inventorySectionData.setModifiedDate(inventorySectionDate);
            final BigDecimal qty =
                inventorySectionData
                    .getQuantity()
                    .add(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
            inventorySectionData.setQuantity(qty);
            this.inventorySectionDataService.update(inventorySectionData);
            // update or save inventory
            final InventoryDetail inventoryDetail =
                inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                    product.getId(), employeeDetails.getBranch().getId());
            if (inventoryDetail != null) {
              final InventoryReports inventoryReportDetails =
                  inventoryReportsService.getInventoryByDateAndInventoryId(
                      new Date(),
                      inventoryDetail.getInventoryId(),
                      orgId,
                      employeeDetails.getBranch().getId());
              if (inventoryReportDetails != null) {
                final BigDecimal reportAvailableConsumedQuantity =
                    inventoryReportDetails.getConsumedQuantity();

                final BigDecimal presentConsumedQuantity =
                    reportAvailableConsumedQuantity.add(
                        BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
                inventoryReportsService.update(inventoryReportDetails);
              } else {
                final InventoryReports inventoryReports = new InventoryReports();
                inventoryReports.setInventoryDetail(inventoryDetail);
                inventoryReports.setStockQuantity(new BigDecimal(0));
                inventoryReports.setConsumedQuantity(
                    BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReports.setCreatedDate(new Date());
                inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setOrganisation(employeeDetails.getOrganisation());
                inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setBranch(branch);
                inventoryReportsService.save(inventoryReports);
              }
            } // if inventorydetails not null

          } else {
            inventorySectionData = new InventorySectionData();
            inventorySectionData.setSectionName(consumedFlag.getSectionName());
            inventorySectionData.setQuantity(
                BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
            inventorySectionData.setInventorySectionId(consumedFlag);
            inventorySectionData.setProductId(product);
            inventorySectionData.setCreatedDate(inventorySectionDate);
            inventorySectionData.setModifiedDate(inventorySectionDate);
            inventorySectionData.setBranch(branch);
            inventorySectionData.setOrganisation(employeeDetails.getOrganisation());
            this.inventorySectionDataService.save(inventorySectionData);
            // update or save inventory
            final InventoryDetail inventoryDetail =
                inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                    product.getId(), employeeDetails.getBranch().getId());
            if (inventoryDetail != null) {
              final InventoryReports inventoryReportDetails =
                  inventoryReportsService.getInventoryByDateAndInventoryId(
                      new Date(),
                      inventoryDetail.getInventoryId(),
                      orgId,
                      employeeDetails.getBranch().getId());
              if (inventoryReportDetails != null) {
                final BigDecimal reportAvailableConsumedQuantity =
                    inventoryReportDetails.getConsumedQuantity();

                final BigDecimal presentConsumedQuantity =
                    reportAvailableConsumedQuantity.add(
                        BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
                inventoryReportsService.update(inventoryReportDetails);
              } else {
                final InventoryReports inventoryReports = new InventoryReports();
                inventoryReports.setInventoryDetail(inventoryDetail);
                inventoryReports.setStockQuantity(new BigDecimal(0));
                inventoryReports.setConsumedQuantity(
                    BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReports.setCreatedDate(new Date());
                inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setOrganisation(employeeDetails.getOrganisation());
                inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setBranch(branch);
                inventoryReportsService.save(inventoryReports);
              }
            } // if inventorydetails not null
          }
          inventorySectionDataHistory.setSectionName(consumedFlag.getSectionName());
          inventorySectionDataHistory.setQuantity(
              BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
          inventorySectionDataHistory.setInventorySectionId(consumedFlag);
          inventorySectionDataHistory.setProductId(product);
          inventorySectionDataHistory.setCreatedDate(inventorySectionDate);
          inventorySectionDataHistory.setModifiedDate(inventorySectionDate);
          inventorySectionDataHistory.setDate(movedDate);
          if (fromType.contentEquals(Constants.SECTION)) {
            inventorySectionDataHistory.setFromSectionId(
                inventorySectionService.getInventorySectionById(
                    json.getLong(Constants.FROMSECTIONID)));
          } else if (fromType.contentEquals(Constants.BRANCH)) {
            inventorySectionDataHistory.setFromBranchId(branchService.getBranchById(fromBranchId));
          }
          inventorySectionDataHistory.setReferenceNumber(referenceNumber);
          final BigDecimal amount = BigDecimal.valueOf(items.getDouble(Constants.AMOUNT));
          final BigDecimal taxPercent = BigDecimal.valueOf(items.getDouble("Tax"));
          final BigDecimal taxableAmount =
              (amount.multiply(taxPercent)).divide(new BigDecimal(100));
          inventorySectionDataHistory.setAmount(amount.add(taxableAmount));
          inventorySectionDataHistory.setTaxableAmount(taxableAmount);
          inventorySectionDataHistory.setBuyingPrice(
              BigDecimal.valueOf(items.getDouble(Constants.BUYINGPRICE)));
          inventorySectionDataHistory.setBranch(branch);
          inventorySectionDataHistory.setOrganisation(employeeDetails.getOrganisation());
          this.inventorySectionDataHistoryService.save(inventorySectionDataHistory);
        }
      } catch (final Exception ex) {
        LOGGER.error(Constants.MARKER, Constants.SAVEINVENTORYSECTION + ex);
      }
      return null;
    } else if (toType.contentEquals(Constants.BRANCH)) {

      final InvoiceBill invoiceBill = new InvoiceBill();
      invoiceBill.setInvoiceNumber(referenceNumber);
      invoiceBill.setDate(sdf.parse(date));
      invoiceBill.setCreatedDate(new Date());
      invoiceBill.setModifiedDate(new Date());
      BigDecimal totalAmount = new BigDecimal(0);
      for (int i = 0; i < ja.length(); ++i) {
        final JSONObject items = ja.getJSONObject(i);
        totalAmount = totalAmount.add(BigDecimal.valueOf(items.getDouble(Constants.AMOUNT)));
      }
      final BigDecimal taxableAmount = new BigDecimal(json.getString("TotalTax"));
      invoiceBill.setTaxableAmount(taxableAmount);
      totalAmount = totalAmount.add(taxableAmount);
      invoiceBill.setTotalAmount(totalAmount);
      if (fromType.contentEquals(Constants.BRANCH)) {
        invoiceBill.setBranchVendor(branchService.getBranchById(fromBranchId));

      } else if (fromType.contentEquals(Constants.SECTION)) {
        invoiceBill.setFromSectionId(
            inventorySectionService.getInventorySectionById(json.getLong(Constants.FROMSECTIONID)));
      }
      invoiceBill.setBranch(branchService.getBranchById(toBranchId));
      invoiceBill.setOrganisation(employeeDetails.getOrganisation());
      invoiceBillService.save(invoiceBill);

      for (int i = 0; i < ja.length(); ++i) {
        Product product = null;
        final JSONObject items = ja.getJSONObject(i);
        final InventoryHistory inventoryHistory = null;
        InventoryDetail inventoryDetails = new InventoryDetail();
        product = this.productsService.getProductFeaturesById(items.getLong(Constants.PRODUCTID));

        inventoryDetails =
            inventoryService.getInventoryDetailByProductNameAndBranchIdAndOrgId(
                product.getProductName(), toBranchId, orgId);
        if (inventoryDetails != null) {
          final BigDecimal totalQuantity =
              inventoryDetails
                  .getQuantity()
                  .add(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
          inventoryDetails.setQuantity(totalQuantity);
          inventoryDetails.setBuyingPrice(BigDecimal.valueOf(items.getDouble("buyingPrice")));
          inventoryDetails.setDate(new Date());
          inventoryService.update(inventoryDetails);
          // update or save inventory
          final InventoryDetail inventoryDetail =
              inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                  product.getId(), employeeDetails.getBranch().getId());
          if (inventoryDetail != null) {
            final InventoryReports inventoryReportDetails =
                inventoryReportsService.getInventoryByDateAndInventoryId(
                    new Date(),
                    inventoryDetail.getInventoryId(),
                    orgId,
                    employeeDetails.getBranch().getId());
            if (inventoryReportDetails != null) {
              final BigDecimal reportAvailableConsumedQuantity =
                  inventoryReportDetails.getConsumedQuantity();

              final BigDecimal presentConsumedQuantity =
                  reportAvailableConsumedQuantity.add(
                      BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
              inventoryReportsService.update(inventoryReportDetails);
            } else {
              final InventoryReports inventoryReports = new InventoryReports();
              inventoryReports.setInventoryDetail(inventoryDetail);
              inventoryReports.setStockQuantity(new BigDecimal(0));
              inventoryReports.setConsumedQuantity(
                  BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReports.setCreatedDate(new Date());
              inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setOrganisation(employeeDetails.getOrganisation());
              inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setBranch(branch);
              inventoryReportsService.save(inventoryReports);
            }
          } // if inventorydetails not null

        } else {
          inventoryDetails = new InventoryDetail();
          inventoryDetails.setProduct(product);
          inventoryDetails.setBuyingPrice(BigDecimal.valueOf(items.getDouble("buyingPrice")));
          inventoryDetails.setBranchDetail(branchService.getBranchById(toBranchId));
          inventoryDetails.setOrganisation(employeeDetails.getOrganisation());
          inventoryDetails.setQuantity(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
          inventoryDetails.setDate(new Date(System.currentTimeMillis()));
          inventoryService.save(inventoryDetails);
          // update or save inventory
          final InventoryDetail inventoryDetail =
              inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                  product.getId(), employeeDetails.getBranch().getId());
          if (inventoryDetail != null) {
            final InventoryReports inventoryReportDetails =
                inventoryReportsService.getInventoryByDateAndInventoryId(
                    new Date(),
                    inventoryDetail.getInventoryId(),
                    orgId,
                    employeeDetails.getBranch().getId());
            if (inventoryReportDetails != null) {
              final BigDecimal reportAvailableConsumedQuantity =
                  inventoryReportDetails.getConsumedQuantity();

              final BigDecimal presentConsumedQuantity =
                  reportAvailableConsumedQuantity.add(
                      BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
              inventoryReportsService.update(inventoryReportDetails);
            } else {
              final InventoryReports inventoryReports = new InventoryReports();
              inventoryReports.setInventoryDetail(inventoryDetail);
              inventoryReports.setStockQuantity(new BigDecimal(0));
              inventoryReports.setConsumedQuantity(
                  BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReports.setCreatedDate(new Date());
              inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setOrganisation(employeeDetails.getOrganisation());
              inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setBranch(branch);
              inventoryReportsService.save(inventoryReports);
            }
          } // if inventorydetails not null
        }

        inventoryHistory.setInventoryId(inventoryDetails);
        inventoryHistory.setInvoiceBillId(invoiceBill);
        inventoryHistory.setQuantity(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
        final BigDecimal tax = BigDecimal.valueOf(items.getDouble("Tax") / 2);
        inventoryHistory.setCgst(tax);
        inventoryHistory.setIgst(new BigDecimal(0));
        inventoryHistory.setSgst(tax);
        inventoryHistory.setConsumedCount(new BigDecimal(0));
        inventoryHistory.setExpiryCount(new BigDecimal(0));
        inventoryHistory.setBranch(branchService.getBranchById(toBranchId));
        inventoryHistory.setOrganisation(employeeDetails.getOrganisation());
        inventoryHistory.setBuyingPrice(BigDecimal.valueOf(items.getDouble("buyingPrice")));
        inventoryHistory.setInventoryHistoryDate(new Date(System.currentTimeMillis()));
        inventoryHistoryService.save(inventoryHistory);
      }
    }
    return null;
  }

  @PostMapping(path = RouteConstants.SAVE_ISSUER_DATA)
  public final Map<String, String> saveIssuerByOrganisation(
      @RequestBody final String payload, final HttpServletRequest request)
      throws JSONException, ParseException {
    final Map<String, String> map = new HashMap<>();
    final JSONObject json = new JSONObject(payload);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final String referenceNumber = json.getString(Constants.REFERENCENUMBER);
    final String date = json.getString("date");
    final long fromBranchId = json.getLong(Constants.FORMID);
    final long toBranchId = json.getLong("toId"); // to branchId
    final JSONArray ja = new JSONArray(json.getString("items"));
    Branch branch = null;
    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
    final Date movedDate = sdf.parse(date);
    final Date inventorySectionDate = new Date();
    final long orgId = employeeDetails.getOrganisation().getId();
    final String fromType = json.getString(Constants.FORMTYPE);
    final String toType = json.getString(Constants.TOTYPE); // totype
    if (toType.contentEquals(Constants.SECTION)) {
      List<InventorySectionDataHistory> list = null;
      list =
          inventorySectionDataHistoryService
              .getInventorySectionDataHistoryByOrgIdAndReferenceNumber(orgId, referenceNumber);
      if (list.size() > 0) {
        map.put(Constants.MESSAGE, Constants.ALREADYEXISTS);
        map.put("flag", Constants.FALSE);
        return map;
      } else {

      }
    } else if (toType.contentEquals(Constants.BRANCH)) {

      InvoiceBill invoiceBill = invoiceBillService.getInvoiceBillByInvoiceNumber(referenceNumber);
      if (invoiceBill != null) {
        map.put(Constants.MESSAGE, Constants.ALREADYEXISTS);
        map.put("flag", Constants.FALSE);
        return map;
      } else {

      }
    }
    if (fromType.contentEquals(Constants.SECTION)) {
      try {
        final long inventorySectionId = json.getLong(Constants.FROMSECTIONID);
        branch = branchService.getBranchById(fromBranchId);
        for (int i = 0; i < ja.length(); ++i) {
          // InventorySection consumedFlag = new InventorySection();
          // Product product = new Product();
          // InventorySectionData inventorySectionData = new InventorySectionData();
          final JSONObject items = ja.getJSONObject(i);
          Product product =
              this.productsService.getProductFeaturesById(
                  items
                      .getJSONObject(Constants.SELECTEDITEMNAME)
                      .getJSONObject(Constants.PRODUCT)
                      .getLong("id"));
          InventorySection consumedFlag =
              this.inventorySectionService.getInventorySectionByOrgIdBranchIdAndId(
                  orgId, fromBranchId, inventorySectionId);
          InventorySectionData inventorySectionData =
              this.inventorySectionDataService.getInventorySectionDataByproductIdAndOrgIdBranchId(
                  consumedFlag.getSectionName(), product.getId(), orgId, fromBranchId);
          if (inventorySectionData.getQuantity().doubleValue()
              >= BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)).doubleValue()) {
            inventorySectionData.setModifiedDate(inventorySectionDate);
            final BigDecimal qty =
                inventorySectionData
                    .getQuantity()
                    .subtract(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
            inventorySectionData.setQuantity(qty);
            this.inventorySectionDataService.update(inventorySectionData);
            // update or save inventory reports

          } else {
            map.put(
                Constants.MESSAGE,
                "product Quantity in inventorySectionData is lessthan to Your Given Quantity ");
            return map;
          }
        }
      } catch (final Exception e) {
        LOGGER.error(Constants.MARKER, Constants.SAVEINVENTORYSECTION + e);
      }
    } else if (fromType.contentEquals(Constants.BRANCH)) {
      try {
        for (int i = 0; i < ja.length(); ++i) {
          final JSONObject items = ja.getJSONObject(i);
          // InventoryDetail inventoryDetail = new InventoryDetail();
          // Product product = new Product();
          Product product =
              this.productsService.getProductFeaturesById(
                  items
                      .getJSONObject(Constants.SELECTEDITEMNAME)
                      .getJSONObject(Constants.PRODUCT)
                      .getLong("id"));
          InventoryDetail inventoryDetail =
              this.inventoryService.getInventoryDetailsByProductIdAndBranchIdAndOrgId(
                  product.getId(), fromBranchId, orgId);
          if (inventoryDetail != null) {
            if (inventoryDetail.getQuantity().doubleValue()
                >= BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)).doubleValue()) {
              final BigDecimal reducedQuantity =
                  inventoryDetail
                      .getQuantity()
                      .subtract(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryDetail.setQuantity(reducedQuantity);
              this.inventoryService.update(inventoryDetail);
            }
            // list<InvHistory>
            if (items.has(Constants.EXPIRYDATE)
                && !items.getString(Constants.EXPIRYDATE).equals("")) {
              final Date expiryDate = sdf.parse(items.getString(Constants.EXPIRYDATE));
              final List<InventoryHistory> list =
                  inventoryHistoryService
                      .getInventoryHsitoryDetailsByExpiryDateAndProductIdAndOrgIdAndBranchIdOrderByAvailableQuantity(
                          expiryDate, product.getId(), orgId, fromBranchId);
              BigDecimal quantity = BigDecimal.valueOf(items.getDouble(Constants.QUANTITY));
              for (final int j = 0; quantity.doubleValue() > 0; ) {
                final InventoryHistory inventoryHistory = list.get(j);
                final BigDecimal availableQuantity =
                    inventoryHistory
                        .getQuantity()
                        .subtract(
                            inventoryHistory
                                .getConsumedCount()
                                .add(inventoryHistory.getExpiryCount()));
                if (availableQuantity.doubleValue() > quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = new BigDecimal(0);
                } else if (availableQuantity.doubleValue() < quantity.doubleValue()) {
                  quantity = quantity.subtract(availableQuantity);
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(availableQuantity));
                } else if (availableQuantity.doubleValue() == quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = BigDecimal.valueOf(0);
                }
                inventoryHistoryService.update(inventoryHistory);
                final InventoryDetail inventoryDetails =
                    inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                        product.getId(), employeeDetails.getBranch().getId());
                inventoryHistory.setInventoryId(inventoryDetails);
              }
            } else {
              final List<InventoryHistory> list =
                  inventoryHistoryService
                      .getInventoryHistoryDetailsByProductIdAndOrgIdAndBranchIdOrderByExpiryDate(
                          product.getId(), orgId, fromBranchId);
              BigDecimal quantity = BigDecimal.valueOf(items.getDouble(Constants.QUANTITY));
              for (final int j = 0; quantity.doubleValue() > 0; ) {
                final InventoryHistory inventoryHistory = list.get(j);
                final BigDecimal availableQuantity =
                    inventoryHistory
                        .getQuantity()
                        .subtract(
                            inventoryHistory
                                .getConsumedCount()
                                .add(inventoryHistory.getExpiryCount()));
                if (availableQuantity.doubleValue() > quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = BigDecimal.valueOf(0);
                } else if (availableQuantity.doubleValue() < quantity.doubleValue()) {
                  quantity = quantity.subtract(availableQuantity);
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(availableQuantity));
                } else if (availableQuantity.doubleValue() == quantity.doubleValue()) {
                  inventoryHistory.setConsumedCount(
                      inventoryHistory.getConsumedCount().add(quantity));
                  quantity = BigDecimal.valueOf(0);
                }
                inventoryHistoryService.update(inventoryHistory);
              }
            }
          } else {
            map.put(
                Constants.MESSAGE,
                "product Quantity in invetory is lessthan to Your Given Quantity ");
            return map;
          }
        }
      } catch (final Exception e) {
        LOGGER.error(Constants.MARKER, Constants.SAVEINVENTORYSECTION + e);
      }
    }
    if (toType.contentEquals(Constants.SECTION)) {
      try {
        final long inventorySectionId = json.getLong(Constants.TOSECTIONID);
        branch = branchService.getBranchById(toBranchId);

        for (int i = 0; i < ja.length(); ++i) {
          // InventorySection consumedFlag = new InventorySection();
          // InventorySectionData inventorySectionData = new InventorySectionData();
          // Product product = new Product();
          final JSONObject items = ja.getJSONObject(i);

          final InventorySectionDataHistory inventorySectionDataHistory =
              new InventorySectionDataHistory();

          Product product =
              this.productsService.getProductFeaturesById(
                  items
                      .getJSONObject(Constants.SELECTEDITEMNAME)
                      .getJSONObject(Constants.PRODUCT)
                      .getLong("id"));
          InventorySection consumedFlag =
              this.inventorySectionService.getInventorySectionByOrgIdBranchIdAndId(
                  orgId, toBranchId, inventorySectionId);

          InventorySectionData inventorySectionData =
              this.inventorySectionDataService.getInventorySectionDataByproductIdAndOrgIdBranchId(
                  consumedFlag.getSectionName(), product.getId(), orgId, toBranchId);
          if (inventorySectionData != null) {
            inventorySectionData.setModifiedDate(inventorySectionDate);
            final BigDecimal qty =
                inventorySectionData
                    .getQuantity()
                    .add(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
            inventorySectionData.setQuantity(qty);
            this.inventorySectionDataService.update(inventorySectionData);
            // update or save inventory
            final InventoryDetail inventoryDetail =
                inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                    product.getId(), employeeDetails.getBranch().getId());
            if (inventoryDetail != null) {
              final InventoryReports inventoryReportDetails =
                  inventoryReportsService.getInventoryByDateAndInventoryId(
                      new Date(),
                      inventoryDetail.getInventoryId(),
                      orgId,
                      employeeDetails.getBranch().getId());
              if (inventoryReportDetails != null) {
                final BigDecimal reportAvailableConsumedQuantity =
                    inventoryReportDetails.getConsumedQuantity();

                final BigDecimal presentConsumedQuantity =
                    reportAvailableConsumedQuantity.add(
                        BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
                inventoryReportsService.update(inventoryReportDetails);
              } else {
                final InventoryReports inventoryReports = new InventoryReports();
                inventoryReports.setInventoryDetail(inventoryDetail);
                inventoryReports.setStockQuantity(new BigDecimal(0));
                inventoryReports.setConsumedQuantity(
                    BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReports.setCreatedDate(new Date());
                inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setOrganisation(employeeDetails.getOrganisation());
                inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setBranch(branch);
                inventoryReportsService.save(inventoryReports);
              }
            } // if inventorydetails not null

          } else {
            inventorySectionData = new InventorySectionData();
            inventorySectionData.setSectionName(consumedFlag.getSectionName());
            inventorySectionData.setQuantity(
                BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
            inventorySectionData.setInventorySectionId(consumedFlag);
            inventorySectionData.setProductId(product);
            inventorySectionData.setCreatedDate(inventorySectionDate);
            inventorySectionData.setModifiedDate(inventorySectionDate);
            inventorySectionData.setBranch(branch);
            inventorySectionData.setOrganisation(employeeDetails.getOrganisation());
            this.inventorySectionDataService.save(inventorySectionData);
            // save or update inventory reports
            final InventoryDetail inventoryDetail =
                inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                    product.getId(), employeeDetails.getBranch().getId());
            if (inventoryDetail != null) {
              final InventoryReports inventoryReportDetails =
                  inventoryReportsService.getInventoryByDateAndInventoryId(
                      new Date(),
                      inventoryDetail.getInventoryId(),
                      orgId,
                      employeeDetails.getBranch().getId());
              if (inventoryReportDetails != null) {
                final BigDecimal reportAvailableConsumedQuantity =
                    inventoryReportDetails.getConsumedQuantity();

                final BigDecimal presentConsumedQuantity =
                    reportAvailableConsumedQuantity.add(
                        BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
                inventoryReportsService.update(inventoryReportDetails);
              } else {
                final InventoryReports inventoryReports = new InventoryReports();
                inventoryReports.setInventoryDetail(inventoryDetail);
                inventoryReports.setStockQuantity(new BigDecimal(0));
                inventoryReports.setConsumedQuantity(
                    BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
                inventoryReports.setCreatedDate(new Date());
                inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setOrganisation(employeeDetails.getOrganisation());
                inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
                inventoryReports.setBranch(branch);
                inventoryReportsService.save(inventoryReports);
              }
            } // if inventorydetails not null
          }
          inventorySectionDataHistory.setSectionName(consumedFlag.getSectionName());
          inventorySectionDataHistory.setQuantity(
              BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
          inventorySectionDataHistory.setInventorySectionId(consumedFlag);
          inventorySectionDataHistory.setProductId(product);
          inventorySectionDataHistory.setCreatedDate(inventorySectionDate);
          inventorySectionDataHistory.setModifiedDate(inventorySectionDate);
          inventorySectionDataHistory.setDate(movedDate);
          if (fromType.contentEquals(Constants.SECTION)) {
            inventorySectionDataHistory.setFromSectionId(
                inventorySectionService.getInventorySectionById(
                    json.getLong(Constants.FROMSECTIONID)));
          } else if (fromType.contentEquals(Constants.BRANCH)) {
            inventorySectionDataHistory.setFromBranchId(branchService.getBranchById(fromBranchId));
          }
          inventorySectionDataHistory.setReferenceNumber(referenceNumber);
          final BigDecimal amount = BigDecimal.valueOf(items.getDouble(Constants.AMOUNT));
          final BigDecimal taxPercent = BigDecimal.valueOf(items.getDouble("Tax"));
          final BigDecimal taxableAmount =
              (amount.multiply(taxPercent)).divide(new BigDecimal(100));
          inventorySectionDataHistory.setAmount(amount);
          inventorySectionDataHistory.setTaxableAmount(taxableAmount);
          inventorySectionDataHistory.setBuyingPrice(
              BigDecimal.valueOf(items.getDouble("buyingPrice")));
          inventorySectionDataHistory.setBranch(branch);
          inventorySectionDataHistory.setOrganisation(employeeDetails.getOrganisation());
          this.inventorySectionDataHistoryService.save(inventorySectionDataHistory);
        }
      } catch (final Exception ex) {
        LOGGER.error(Constants.MARKER, Constants.SAVEINVENTORYSECTION + ex);
      }
      return null;
    } else if (toType.contentEquals(Constants.BRANCH)) {

      final InvoiceBill invoiceBill = new InvoiceBill();

      invoiceBill.setInvoiceNumber(referenceNumber);
      invoiceBill.setDate(sdf.parse(date));
      invoiceBill.setCreatedDate(new Date());
      invoiceBill.setModifiedDate(new Date());
      BigDecimal totalAmount = new BigDecimal(0);
      for (int i = 0; i < ja.length(); ++i) {
        final JSONObject items = ja.getJSONObject(i);
        totalAmount = totalAmount.add(BigDecimal.valueOf(items.getDouble(Constants.AMOUNT)));
      }
      final BigDecimal taxableAmount = new BigDecimal(json.getString("TotalTax"));
      invoiceBill.setTaxableAmount(taxableAmount);
      invoiceBill.setTotalAmount(totalAmount);
      if (fromType.contentEquals(Constants.BRANCH)) {
        invoiceBill.setBranchVendor(branchService.getBranchById(fromBranchId)); // from
        // branchId
      } else if (fromType.contentEquals(Constants.SECTION)) {
        invoiceBill.setFromSectionId(
            inventorySectionService.getInventorySectionById(json.getLong(Constants.FROMSECTIONID)));
      }
      invoiceBill.setBranch(branchService.getBranchById(toBranchId));
      invoiceBill.setOrganisation(employeeDetails.getOrganisation());
      invoiceBillService.save(invoiceBill);

      for (int i = 0; i < ja.length(); ++i) {
        final JSONObject items = ja.getJSONObject(i);
        final InventoryHistory inventoryHistory = new InventoryHistory();
        // InventoryDetail inventoryDetails = new InventoryDetail();
        // Product product = new Product();
        Product product =
            this.productsService.getProductFeaturesById(
                items
                    .getJSONObject(Constants.SELECTEDITEMNAME)
                    .getJSONObject(Constants.PRODUCT)
                    .getLong("id"));

        InventoryDetail inventoryDetails =
            inventoryService.getInventoryDetailByProductNameAndBranchIdAndOrgId(
                product.getProductName(), toBranchId, orgId);
        if (inventoryDetails != null) {
          final BigDecimal totalQuantity =
              inventoryDetails
                  .getQuantity()
                  .add(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
          inventoryDetails.setQuantity(totalQuantity);
          inventoryDetails.setBuyingPrice(BigDecimal.valueOf(items.getDouble("buyingPrice")));
          inventoryDetails.setDate(new Date(System.currentTimeMillis()));
          inventoryService.update(inventoryDetails);
          // update or save inventory reports
          final InventoryDetail inventoryDetail =
              inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                  product.getId(), employeeDetails.getBranch().getId());
          if (inventoryDetail != null) {
            final InventoryReports inventoryReportDetails =
                inventoryReportsService.getInventoryByDateAndInventoryId(
                    new Date(),
                    inventoryDetail.getInventoryId(),
                    orgId,
                    employeeDetails.getBranch().getId());
            if (inventoryReportDetails != null) {
              final BigDecimal reportAvailableConsumedQuantity =
                  inventoryReportDetails.getConsumedQuantity();

              final BigDecimal presentConsumedQuantity =
                  reportAvailableConsumedQuantity.add(
                      BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
              inventoryReportsService.update(inventoryReportDetails);
            } else {
              final InventoryReports inventoryReports = new InventoryReports();
              inventoryReports.setInventoryDetail(inventoryDetail);
              inventoryReports.setStockQuantity(new BigDecimal(0));
              inventoryReports.setConsumedQuantity(
                  BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReports.setCreatedDate(new Date());
              inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setOrganisation(employeeDetails.getOrganisation());
              inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setBranch(branch);
              inventoryReportsService.save(inventoryReports);
            }
          } // if inventorydetails not null

        } else {
          inventoryDetails = new InventoryDetail();
          inventoryDetails.setProduct(product);
          inventoryDetails.setBuyingPrice(BigDecimal.valueOf(items.getDouble("buyingPrice")));
          inventoryDetails.setBranchDetail(branchService.getBranchById(toBranchId));
          inventoryDetails.setOrganisation(employeeDetails.getOrganisation());
          inventoryDetails.setQuantity(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
          inventoryDetails.setDate(new Date(System.currentTimeMillis()));
          inventoryService.save(inventoryDetails);
          final InventoryDetail inventoryDetail =
              inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                  product.getId(), employeeDetails.getBranch().getId());
          if (inventoryDetail != null) {
            final InventoryReports inventoryReportDetails =
                inventoryReportsService.getInventoryByDateAndInventoryId(
                    new Date(),
                    inventoryDetail.getInventoryId(),
                    orgId,
                    employeeDetails.getBranch().getId());
            if (inventoryReportDetails != null) {
              final BigDecimal reportAvailableConsumedQuantity =
                  inventoryReportDetails.getConsumedQuantity();

              final BigDecimal presentConsumedQuantity =
                  reportAvailableConsumedQuantity.add(
                      BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReportDetails.setConsumedQuantity(presentConsumedQuantity);
              inventoryReportsService.update(inventoryReportDetails);
            } else {
              final InventoryReports inventoryReports = new InventoryReports();
              inventoryReports.setInventoryDetail(inventoryDetail);
              inventoryReports.setStockQuantity(new BigDecimal(0));
              inventoryReports.setConsumedQuantity(
                  BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
              inventoryReports.setCreatedDate(new Date());
              inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setOrganisation(employeeDetails.getOrganisation());
              inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setBranch(branch);
              inventoryReportsService.save(inventoryReports);
            }
          } // if inventorydetails not null
        }

        inventoryHistory.setInventoryId(inventoryDetails);
        inventoryHistory.setInvoiceBillId(invoiceBill);
        inventoryHistory.setQuantity(BigDecimal.valueOf(items.getDouble(Constants.QUANTITY)));
        inventoryHistory.setBranch(branchService.getBranchById(toBranchId));
        final BigDecimal tax = BigDecimal.valueOf((items.getDouble("Tax")) / 2);
        inventoryHistory.setCgst(tax);
        inventoryHistory.setIgst(new BigDecimal(0));
        inventoryHistory.setSgst(tax);
        inventoryHistory.setConsumedCount(new BigDecimal(0));
        inventoryHistory.setExpiryCount(new BigDecimal(0));
        inventoryHistory.setOrganisation(employeeDetails.getOrganisation());
        inventoryHistory.setBuyingPrice(BigDecimal.valueOf(items.getDouble("buyingPrice")));
        inventoryHistory.setInventoryHistoryDate(new Date(System.currentTimeMillis()));
        inventoryHistoryService.save(inventoryHistory);
      }
    }
    return null;
  }

  @GetMapping(path = RouteConstants.GET_INVENTORY_SECTIONDATA_DETAILS)
  public final List<InventorySectionData> getAllInventorySectionData(
      final HttpServletRequest request) throws Exception {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    List<InventorySectionData> inventorySectionData = null;
    try {
      final Branch branch = employeeDetails.getBranch();
      if (branch != null) {
        inventorySectionData =
            this.inventorySectionDataService.getAllInventorySectionDataByOrgIdBranchId(
                orgId, branch.getId());
      } else {
        inventorySectionData =
            this.inventorySectionDataService.getAllInventorySectionDataByOrgId(orgId);
      }
    } catch (final Exception ex) {

      LOGGER.error(Constants.MARKER, "/api/inventorysectiondata/details  " + ex);
    }

    return inventorySectionData;
  }

  @GetMapping(path = RouteConstants.INVENTORYSECTIONDATA_KOTFLAG_TRUE_DETAILS)
  public final List<InventorySection> getInventorySectionDataBykotFlagTrue(
      final HttpServletRequest request) throws Exception {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    List<InventorySection> inventorySection = null;
    try {
      final Branch branch = employeeDetails.getBranch();
      if (branch != null) {
        inventorySection =
            this.inventorySectionService.getAllInventorySectionByOrgIdBranchIdByStaticKotFlag(
                orgId, branch.getId());
      } else {
        inventorySection = this.inventorySectionService.getAllInventorySectionByOrgId(orgId);
      }
    } catch (final Exception ex) {

      LOGGER.error(Constants.MARKER, "/admin/inventorysectiondata/kotflag/true/details  " + ex);
    }

    return inventorySection;
  }

  @PostMapping(path = RouteConstants.UPDATE_INVENTORYSECTION)
  public final Map<String, String> updateInventorySectionDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String InventorySectionDetails)
      throws JSONException {
    final JSONObject inventorysection = new JSONObject(InventorySectionDetails);
    final Long id = inventorysection.getLong("id");
    final String key = inventorysection.getString("key");
    final String value = inventorysection.getString("value");

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();
    final Map<String, String> inventorySectionUpdate = new HashMap<>();
    try {
      final List<InventorySection> inventorySections =
          this.inventorySectionService.getAllInventorySectionByOrgIdBranchId(orgId, branchId);
      for (final InventorySection inv : inventorySections) {
        if (inv.getSectionName().equalsIgnoreCase(value)) {
          inventorySectionUpdate.put(
              Constants.INVENTORYSECTIONUPDATE,
              "This SectionName Already Existed! Please Change With Another.");
          return inventorySectionUpdate;
        }
      }
      final InventorySection inventorySection =
          this.inventorySectionService.getInventorySectionById(id);
      if (key.equals("sectionName")) {
        inventorySection.setSectionName(value);
        inventorySectionUpdate.put(
            Constants.INVENTORYSECTIONUPDATE,
            Constants.UPDATEFORSECTION + inventorySection.getSectionName());
      } else if (key.equals(Constants.CONSUMEDFLAG)) {
        inventorySection.setConsumedFlag(Boolean.parseBoolean(value));
        inventorySectionUpdate.put(
            Constants.INVENTORYSECTIONUPDATE,
            Constants.UPDATEFORSECTION + inventorySection.getSectionName());
      } else if (key.equals(Constants.KOTFLAG)) {
        inventorySection.setKotFlag(Boolean.parseBoolean(value));
        inventorySectionUpdate.put(
            Constants.INVENTORYSECTIONUPDATE,
            Constants.UPDATEFORSECTION + inventorySection.getSectionName());
      } else {
        inventorySectionUpdate.put(Constants.INVENTORYSECTIONUPDATE, "key Name does not match");
      }
      this.inventorySectionService.update(inventorySection);

    } catch (final RuntimeException ex) {
      LOGGER.error("updateInventorySectionDetails" + ex, ex);
    }
    return inventorySectionUpdate;
  }

  @GetMapping(path = RouteConstants.INVENTORYSECTIONDATA_DEPARTEMENTWISE_REPORTS)
  public final ResponseEntity<List<Map<String, Object>>> getInventorySectionDataHistoryReports(
      final HttpServletRequest request) throws ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final List<Map<String, Object>> listOfDeptDetails = new ArrayList<>();
    try {
      final long orgId = employeeDetails.getOrganisation().getId();
      final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
      final Date fromdate = sdf.parse(request.getParameter(Constants.FROMDATE));
      final Date todate = sdf.parse(request.getParameter(Constants.TODATE));
      final long frombranchId = Long.parseLong(request.getParameter(Constants.FORMID));
      final long tobranchId = Long.parseLong(request.getParameter("toId"));
      final long toSectionId = Long.parseLong(request.getParameter(Constants.TOSECTIONID));
      final long fromSectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));
      final String toTypesection = request.getParameter(Constants.TOTYPE);
      final String fromTypesection = request.getParameter(Constants.FORMTYPE);
      List<Object[]> inventorySectionDataHistory = null;
      List<Object[]> invoiceBillDetails = null;
      try {
        if (toTypesection.equals(Constants.SECTION) && fromTypesection.equals(Constants.SECTION)) {

          inventorySectionDataHistory =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSectionId(
                      orgId, tobranchId, fromdate, todate, toSectionId, fromSectionId);
          for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
            final Map<String, Object> totalDepAmt = new HashMap<String, Object>();
            totalDepAmt.put(Constants.TOTALAMOUNT, inventorySectionDataHistory.get(i)[0]);
            totalDepAmt.put("date", sdf.format(inventorySectionDataHistory.get(i)[2]));
            totalDepAmt.put(Constants.TOBRANCHNAME, inventorySectionDataHistory.get(i)[3]);
            totalDepAmt.put(Constants.TOSECTIONNAME, inventorySectionDataHistory.get(i)[1]);
            totalDepAmt.put(Constants.FROMSECTIONNAME, inventorySectionDataHistory.get(i)[4]);
            listOfDeptDetails.add(totalDepAmt);
          }

        } else if (toTypesection.equals(Constants.SECTION)
            && fromTypesection.equals(Constants.BRANCH)) {

          inventorySectionDataHistory =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryByDepartmentWiseOrgIdFromBranchId(
                      orgId, tobranchId, fromdate, todate, toSectionId, frombranchId);
          for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
            final Map<String, Object> totalDepAmt = new HashMap<>();
            totalDepAmt.put(Constants.TOTALAMOUNT, inventorySectionDataHistory.get(i)[0]);
            totalDepAmt.put("date", sdf.format(inventorySectionDataHistory.get(i)[1]));
            totalDepAmt.put(Constants.TOBRANCHNAME, inventorySectionDataHistory.get(i)[2]);
            totalDepAmt.put(Constants.FROMSECTIONNAME, inventorySectionDataHistory.get(i)[3]);
            totalDepAmt.put(Constants.FROMBRANCHNAME, inventorySectionDataHistory.get(i)[4]);
            listOfDeptDetails.add(totalDepAmt);
          }

        } else if (toTypesection.equals(Constants.BRANCH)
            && fromTypesection.equals(Constants.SECTION)) {
          invoiceBillDetails =
              invoiceBillService.getAllInvoiceDetailsBySection(
                  orgId, tobranchId, fromdate, todate, fromSectionId);
          for (int i = 0; i < invoiceBillDetails.size(); i++) {
            final Map<String, Object> totalDepAmt = new HashMap<>();
            totalDepAmt.put(Constants.TOTALAMOUNT, invoiceBillDetails.get(i)[0]);
            totalDepAmt.put(Constants.TOBRANCHNAME, invoiceBillDetails.get(i)[2]);
            totalDepAmt.put(Constants.FROMSECTIONNAME, invoiceBillDetails.get(i)[1]);
            totalDepAmt.put("date", sdf.format(invoiceBillDetails.get(i)[3]));

            listOfDeptDetails.add(totalDepAmt);
          }
        } else if (toTypesection.equals(Constants.BRANCH)
            && fromTypesection.equals(Constants.BRANCH)) {
          invoiceBillDetails =
              invoiceBillService.getAllInvoiceDetailsByFromBranch(
                  orgId, tobranchId, fromdate, todate, frombranchId);
          for (int i = 0; i < invoiceBillDetails.size(); i++) {
            final Map<String, Object> totalDepAmt = new HashMap<>();
            totalDepAmt.put(Constants.TOTALAMOUNT, invoiceBillDetails.get(i)[0]);
            totalDepAmt.put(Constants.TOBRANCHNAME, invoiceBillDetails.get(i)[2]);
            totalDepAmt.put(Constants.FROMBRANCHNAME, invoiceBillDetails.get(i)[1]);
            totalDepAmt.put("date", sdf.format(invoiceBillDetails.get(i)[3]));

            listOfDeptDetails.add(totalDepAmt);
          }
        }

      } catch (final Exception ex) {
        LOGGER.error(Constants.MARKER, "getInventorySectionDataHistoryReports" + ex);
      }
    } catch (Exception e) {
      LOGGER.error(
          "get department wise consolidated Reports  " + "select required branch or section");
      return new ResponseEntity<>(listOfDeptDetails, HttpStatus.OK);
    }
    return new ResponseEntity<>(listOfDeptDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVENTORYSECTIONDATA_PRODUCT_REPORTS)
  public final List<Map<String, Object>> getInventorySectionDataHistoryReport(
      final HttpServletRequest request) throws ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
    final Date fromdate = sdf.parse(request.getParameter(Constants.FROMDATE));
    final Date todate = sdf.parse(request.getParameter(Constants.TODATE));
    final long branchId = Long.parseLong(request.getParameter(Constants.FORMID));
    final String fromType = request.getParameter(Constants.FORMTYPE);
    final long fromSectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));
    List<InventorySectionDataHistory> inventorySectionDataHistory = null;
    Map<String, Object> map = null;
    List<InvoiceBill> invoiceBill = null;
    List<InventoryHistory> inventoryHistory = null;
    final List<Map<String, Object>> billdetails = new ArrayList<>();
    try {
      if (fromType.equals(Constants.BRANCH)) {
        invoiceBill =
            invoiceBillService.getAllInvoiceDetailsByBranchAndVendorIdIsNull(
                orgId, fromdate, todate, branchId);
        for (final InvoiceBill invb : invoiceBill) {
          inventoryHistory =
              inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                  branchId, orgId, invb.getId());
          for (final InventoryHistory invhis : inventoryHistory) {
            map = new HashMap<>();
            map.put(Constants.PRODUCTNAME, invhis.getInventoryId().getProduct().getProductName());
            map.put(Constants.QUANTITY, invhis.getQuantity());
            map.put(Constants.BUYINGPRICE, invhis.getInventoryId().getBuyingPrice());
            map.put(
                Constants.AMOUNT,
                (invhis.getInventoryId().getBuyingPrice().multiply(invhis.getQuantity())));
            final String toName = invb.getBranch().getBranchName();
            map.put(Constants.TONAME, toName);
            map.put("date", invhis.getInventoryHistoryDate());
            billdetails.add(map);
          }
        }

        inventorySectionDataHistory =
            inventorySectionDataHistoryService
                .getInventorySectionDataHistoryByItemWiseOrgIdBranchIdAndFromSectionId(
                    orgId, fromdate, todate, branchId);
        for (final InventorySectionDataHistory inv : inventorySectionDataHistory) {
          map = new HashMap<>();
          map.put(Constants.PRODUCTNAME, inv.getProductId().getProductName());
          map.put(Constants.QUANTITY, inv.getQuantity());
          map.put(Constants.BUYINGPRICE, inv.getBuyingPrice());
          map.put(Constants.AMOUNT, inv.getAmount());
          map.put("date", inv.getDate());
          final String toName =
              inv.getBranch().getBranchName() + "/" + inv.getInventorySectionId().getSectionName();
          map.put(Constants.TONAME, toName);
          billdetails.add(map);
        }

      } else {

        invoiceBill =
            invoiceBillService.getAllInvoiceDetailsByBranchAndSectionIdIsNull(
                orgId, fromdate, todate, fromSectionId);
        for (final InvoiceBill invb : invoiceBill) {
          inventoryHistory =
              inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                  branchId, orgId, invb.getId());
          for (final InventoryHistory invhis : inventoryHistory) {
            map = new HashMap<>();
            map.put(Constants.PRODUCTNAME, invhis.getInventoryId().getProduct().getProductName());
            map.put(Constants.QUANTITY, invhis.getQuantity());
            map.put(Constants.BUYINGPRICE, invhis.getInventoryId().getBuyingPrice());
            map.put(
                Constants.AMOUNT,
                (invhis.getInventoryId().getBuyingPrice().multiply(invhis.getQuantity())));
            final String toName = invb.getBranch().getBranchName();
            map.put(Constants.TONAME, toName);
            map.put("date", invhis.getInventoryHistoryDate());
            billdetails.add(map);
          }
        }
        inventorySectionDataHistory =
            inventorySectionDataHistoryService
                .getInventorySectionDataHistoryByItemWiseConsolidatedOrgIdBranchIdAndSectionId(
                    orgId, branchId, fromdate, todate, fromSectionId);
        for (final InventorySectionDataHistory inv : inventorySectionDataHistory) {
          map = new HashMap<>();
          map.put(Constants.PRODUCTNAME, inv.getProductId().getProductName());
          map.put(Constants.QUANTITY, inv.getQuantity());
          map.put(Constants.BUYINGPRICE, inv.getBuyingPrice());
          map.put(Constants.AMOUNT, inv.getAmount());
          map.put("date", inv.getDate());
          final String toName =
              inv.getBranch().getBranchName() + "/" + inv.getInventorySectionId().getSectionName();
          map.put(Constants.TONAME, toName);
          billdetails.add(map);
        }
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInventorySectionDataHistoryReports" + ex);
    }
    return billdetails;
  }

  @GetMapping(path = RouteConstants.INVENTORYSECTIONDATA_PRODUCTCONSOLIDATED_REPORTS)
  public final ResponseEntity<Map<String, Map<String, Object>>>
      getInventorySectionDataHistoryConsolidatedReports(final HttpServletRequest request)
          throws ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
    final Map<String, Map<String, Object>> billdetails = new HashMap<>();
    try {
      final Date fromdate = sdf.parse(request.getParameter(Constants.FROMDATE));
      final Date todate = sdf.parse(request.getParameter(Constants.TODATE));
      final long branchId = Long.parseLong(request.getParameter(Constants.FORMID));
      final String fromType = request.getParameter(Constants.FORMTYPE);
      final long fromSectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));
      List<InventorySectionDataHistory> inventorySectionDataHistory = null;
      Map<String, Object> map = null;
      List<InvoiceBill> invoiceBill = null;
      List<InventoryHistory> inventoryHistory = null;
      try {
        if (fromType.equals(Constants.BRANCH)) {
          invoiceBill =
              invoiceBillService.getAllInvoiceDetailsByBranchAndVendorIdIsNull(
                  orgId, fromdate, todate, branchId);
          for (final InvoiceBill invb : invoiceBill) {
            inventoryHistory =
                inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                    branchId, orgId, invb.getId());
            for (final InventoryHistory invhis : inventoryHistory) {
              map = new HashMap<>();
              map.put(Constants.PRODUCTNAME, invhis.getInventoryId().getProduct().getProductName());
              map.put(Constants.BUYINGPRICE, invhis.getInventoryId().getBuyingPrice());
              map.put(
                  Constants.AMOUNT,
                  (invhis.getInventoryId().getBuyingPrice().multiply(invhis.getQuantity())));
              final String toName = invb.getBranch().getBranchName();
              map.put(Constants.TONAME, toName);
              if (billdetails.containsKey(invhis.getInventoryId().getProduct().getProductName())) {
                final long qty =
                    (long)
                        billdetails
                            .get(invhis.getInventoryId().getProduct().getProductName())
                            .get(Constants.QUANTITY);
                map.put(Constants.QUANTITY, qty + invhis.getQuantity().longValue());
              } else {
                map.put(Constants.QUANTITY, invhis.getQuantity());
              }
              billdetails.put(invhis.getInventoryId().getProduct().getProductName(), map);
            }
          }
          inventorySectionDataHistory =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryByItemWiseOrgIdBranchIdAndFromSectionId(
                      orgId, fromdate, todate, branchId);
          for (final InventorySectionDataHistory inv : inventorySectionDataHistory) {
            map = new HashMap<>();
            map.put(Constants.PRODUCTNAME, inv.getProductId().getProductName());
            map.put(Constants.BUYINGPRICE, inv.getBuyingPrice());
            map.put(Constants.AMOUNT, inv.getAmount());
            if (billdetails.containsKey(inv.getProductId().getProductName())) {
              final long qty =
                  (long)
                      billdetails.get(inv.getProductId().getProductName()).get(Constants.QUANTITY);
              map.put(Constants.QUANTITY, qty + inv.getQuantity().longValue());
            } else {
              map.put(Constants.QUANTITY, inv.getQuantity());
            }
            billdetails.put(inv.getProductId().getProductName(), map);
          }

        } else {
          invoiceBill =
              invoiceBillService.getAllInvoiceDetailsByBranchAndSectionIdIsNull(
                  orgId, fromdate, todate, fromSectionId);
          for (final InvoiceBill invb : invoiceBill) {
            inventoryHistory =
                inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                    branchId, orgId, invb.getId());
            for (final InventoryHistory invhis : inventoryHistory) {
              map = new HashMap<>();
              map.put(Constants.PRODUCTNAME, invhis.getInventoryId().getProduct().getProductName());
              map.put(Constants.QUANTITY, invhis.getQuantity());
              map.put(Constants.BUYINGPRICE, invhis.getInventoryId().getBuyingPrice());
              map.put(
                  Constants.AMOUNT,
                  (invhis.getInventoryId().getBuyingPrice().multiply(invhis.getQuantity())));
              if (billdetails.containsKey(invhis.getInventoryId().getProduct().getProductName())) {
                final long qty =
                    (long)
                        billdetails
                            .get(invhis.getInventoryId().getProduct().getProductName())
                            .get(Constants.QUANTITY);
                map.put(Constants.QUANTITY, qty + invhis.getQuantity().longValue());
              } else {
                map.put(Constants.QUANTITY, invhis.getQuantity());
              }
              billdetails.put(invhis.getInventoryId().getProduct().getProductName(), map);
            }
          }
          inventorySectionDataHistory =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryByItemWiseConsolidatedOrgIdBranchIdAndSectionId(
                      orgId, branchId, fromdate, todate, fromSectionId);
          for (final InventorySectionDataHistory inv : inventorySectionDataHistory) {
            map = new HashMap<>();
            map.put(Constants.PRODUCTNAME, inv.getProductId().getProductName());
            map.put(Constants.BUYINGPRICE, inv.getBuyingPrice());
            map.put(Constants.AMOUNT, inv.getAmount());
            final String toName =
                inv.getBranch().getBranchName()
                    + "/"
                    + inv.getInventorySectionId().getSectionName();
            map.put(Constants.TONAME, toName);
            if (billdetails.containsKey(inv.getProductId().getProductName())) {
              final long qty =
                  (long)
                      billdetails.get(inv.getProductId().getProductName()).get(Constants.QUANTITY);
              map.put(Constants.QUANTITY, qty + inv.getQuantity().longValue());
            } else {
              map.put(Constants.QUANTITY, inv.getQuantity());
            }
            billdetails.put(inv.getProductId().getProductName(), map);
          }
        }

      } catch (final Exception ex) {
        LOGGER.error(Constants.MARKER, "getInventorySectionDataHistoryReports" + ex);
      }
    } catch (Exception e) {
      LOGGER.error("getInventorySectionDataHistoryReports " + "All parameters are not passed");
      return new ResponseEntity<>(billdetails, HttpStatus.OK);
    }
    return new ResponseEntity<>(billdetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.TRANSFER_BILLWISE_DETAILS)
  public final ResponseEntity<List<Map<String, Object>>> TransferBillWiseList(
      final HttpServletRequest request, final HttpServletResponse response)
      throws Exception, ParseException {

    final long orgId = jwtTokenUtil.getOrgIdFromToken(request);
    final List<Map<String, Object>> list = new ArrayList<>();

    try {
      final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
      final Date fromDate = sdf.parse(request.getParameter(Constants.FROMDATE));
      final Date toDate = sdf.parse(request.getParameter(Constants.TODATE));
      final long fromBranchId = Long.parseLong(request.getParameter(Constants.FORMID));
      final long toBranchId = Long.parseLong(request.getParameter("toId"));
      final long toSectionId = Long.parseLong(request.getParameter(Constants.TOSECTIONID));
      final long fromSectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));
      final String toType = request.getParameter(Constants.TOTYPE);
      final String fromType = request.getParameter(Constants.FORMTYPE);
      final String referenceNumber = request.getParameter("refernceNumber");
      if (toType.contentEquals(Constants.BRANCH)) {
        List<InvoiceBill> invoiceBills = null;
        if (fromType.contentEquals(Constants.BRANCH)) {
          if (referenceNumber == null || referenceNumber == "") {
            invoiceBills =
                invoiceBillService.getAllInvoiceDetailsByDatesAndBranchAndBranchVendor(
                    fromDate, toDate, orgId, toBranchId, fromBranchId);
            for (final InvoiceBill invoiceBill : invoiceBills) {
              final Map<String, Object> map = new HashMap<>();
              map.put("date", sdf.format(invoiceBill.getDate()));
              map.put(Constants.AMOUNT, invoiceBill.getTotalAmount());
              map.put("from", invoiceBill.getBranchVendor().getBranchName());
              map.put("to", invoiceBill.getBranch().getBranchName());
              map.put(Constants.REFERENCENUMBER, invoiceBill.getInvoiceNumber());
              list.add(map);
            }
          } else {
            final InvoiceBill invoiceBill =
                invoiceBillService.getAllInvoiceDetailsByDatesByRefernceNumber(
                    fromDate, toDate, orgId, referenceNumber, toBranchId, fromBranchId);
            final Map<String, Object> map = new HashMap<>();
            map.put("date", sdf.format(invoiceBill.getDate()));
            map.put(Constants.AMOUNT, invoiceBill.getTotalAmount());
            map.put("from", invoiceBill.getBranchVendor().getBranchName());
            map.put("to", invoiceBill.getBranch().getBranchName());
            map.put(Constants.REFERENCENUMBER, invoiceBill.getInvoiceNumber());
            list.add(map);
          }
        } else if (fromType.contentEquals(Constants.SECTION)) {
          if (referenceNumber.equals("")) {
            invoiceBills =
                invoiceBillService.getAllInvoiceDetailsByDatesAndBranchAndFromSectionId(
                    fromDate, toDate, orgId, toBranchId, fromSectionId);
            for (final InvoiceBill invoiceBill : invoiceBills) {
              final Map<String, Object> map = new HashMap<>();
              map.put("date", sdf.format(invoiceBill.getDate()));
              map.put(Constants.AMOUNT, invoiceBill.getTotalAmount());
              map.put("from", invoiceBill.getFromSectionId().getSectionName());
              map.put("to", invoiceBill.getBranch().getBranchName());
              map.put(Constants.REFERENCENUMBER, invoiceBill.getInvoiceNumber());
              list.add(map);
            }
          } else {
            final InvoiceBill invoiceBill =
                invoiceBillService
                    .getInvoiceDetailsByDatesAndRefernceNumberAndBranchAndFromSectionId(
                        fromDate, toDate, orgId, referenceNumber, toBranchId, fromSectionId);
            final Map<String, Object> map = new HashMap<>();
            map.put("date", sdf.format(invoiceBill.getDate()));
            map.put(Constants.AMOUNT, invoiceBill.getTotalAmount());
            map.put("from", invoiceBill.getFromSectionId().getSectionName());
            map.put("to", invoiceBill.getBranch().getBranchName());
            map.put(Constants.REFERENCENUMBER, invoiceBill.getInvoiceNumber());
            list.add(map);
          }
        }

      } else if (toType.contentEquals(Constants.SECTION)) {
        List<Object[]> inventorySectionDataHistories = null;
        if (fromType.contentEquals(Constants.BRANCH)) {
          if (referenceNumber == null || referenceNumber == "") {
            inventorySectionDataHistories =
                inventorySectionDataHistoryService
                    .getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromBranchId(
                        fromDate, toDate, orgId, toSectionId, fromBranchId);
            for (int i = 0; i < inventorySectionDataHistories.size(); i++) {
              final Map<String, Object> map = new HashMap<>();
              map.put("date", inventorySectionDataHistories.get(i)[0]);
              map.put(Constants.AMOUNT, inventorySectionDataHistories.get(i)[1]);
              final InventorySection inventorySection =
                  inventorySectionService.getInventorySectionById(
                      Long.parseLong(inventorySectionDataHistories.get(i)[3].toString()));
              map.put(
                  "from",
                  branchService
                      .getBranchById(
                          Long.parseLong(inventorySectionDataHistories.get(i)[2].toString()))
                      .getBranchName());
              map.put(
                  "to",
                  inventorySection.getBranch().getBranchName()
                      + "/"
                      + inventorySection.getSectionName());
              map.put(Constants.REFERENCENUMBER, inventorySectionDataHistories.get(i)[4]);
              list.add(map);
            }
          } else {
            final Object[] inventorySectionDataHistory =
                inventorySectionDataHistoryService
                    .getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromBranchIdAndReferenceNumber(
                        fromDate, toDate, orgId, toSectionId, fromBranchId, referenceNumber);
            final Map<String, Object> map = new HashMap<>();
            map.put("date", inventorySectionDataHistory[0]);
            map.put(Constants.AMOUNT, inventorySectionDataHistory[1]);
            final InventorySection inventorySection =
                inventorySectionService.getInventorySectionById(
                    Long.parseLong(inventorySectionDataHistory[3].toString()));
            map.put(
                "from",
                branchService
                    .getBranchById(Long.parseLong(inventorySectionDataHistory[2].toString()))
                    .getBranchName());
            map.put(
                "to",
                inventorySection.getBranch().getBranchName()
                    + "/"
                    + inventorySection.getSectionName());
            map.put(Constants.REFERENCENUMBER, inventorySectionDataHistory[4]);
            list.add(map);
          }

        } else if (fromType.contentEquals(Constants.SECTION)) {
          if (referenceNumber == null || referenceNumber == "") {
            inventorySectionDataHistories =
                inventorySectionDataHistoryService
                    .getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromSectionId(
                        fromDate, toDate, orgId, toSectionId, fromSectionId);
            for (int i = 0; i < inventorySectionDataHistories.size(); i++) {
              final Map<String, Object> map = new HashMap<>();
              map.put("date", inventorySectionDataHistories.get(i)[0]);
              map.put(Constants.AMOUNT, inventorySectionDataHistories.get(i)[1]);
              final InventorySection fromIventorySection =
                  inventorySectionService.getInventorySectionById(
                      Long.parseLong(inventorySectionDataHistories.get(i)[2].toString()));
              final InventorySection toInventorySection =
                  inventorySectionService.getInventorySectionById(
                      Long.parseLong(inventorySectionDataHistories.get(i)[3].toString()));
              map.put(
                  "from",
                  fromIventorySection.getBranch().getBranchName()
                      + "/"
                      + fromIventorySection.getSectionName());
              map.put(
                  "to",
                  toInventorySection.getBranch().getBranchName()
                      + "/"
                      + toInventorySection.getSectionName());
              map.put(Constants.REFERENCENUMBER, inventorySectionDataHistories.get(i)[4]);
              list.add(map);
            }
          } else {
            final Object[] inventorySectionDataHistory =
                inventorySectionDataHistoryService
                    .getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromSectionIdAndReferenceNumber(
                        fromDate, toDate, orgId, toSectionId, fromSectionId, referenceNumber);
            final Map<String, Object> map = new HashMap<>();
            map.put("date", inventorySectionDataHistory[0]);
            map.put(Constants.AMOUNT, inventorySectionDataHistory[1]);
            final InventorySection fromIventorySection =
                inventorySectionService.getInventorySectionById(
                    Long.parseLong(inventorySectionDataHistory[2].toString()));
            final InventorySection toInventorySection =
                inventorySectionService.getInventorySectionById(
                    Long.parseLong(inventorySectionDataHistory[3].toString()));
            map.put(
                "from",
                fromIventorySection.getBranch().getBranchName()
                    + "/"
                    + fromIventorySection.getSectionName());
            map.put(
                "to",
                toInventorySection.getBranch().getBranchName()
                    + "/"
                    + toInventorySection.getSectionName());
            map.put(Constants.REFERENCENUMBER, inventorySectionDataHistory[4]);
            list.add(map);
          }
        }
      }

    } catch (Exception e) {
      LOGGER.error("getTransfer bill Reports  " + "select required branch or section");
      return new ResponseEntity<>(list, HttpStatus.OK);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.TRANSFER_ITEMWISE_REPORTS_DETAILS)
  public final ResponseEntity<Map<String, Map<String, Object>>> transferItemWiseReport(
      final HttpServletRequest request, final HttpServletResponse response)
      throws Exception, ParseException {
    final long orgId = jwtTokenUtil.getOrgIdFromToken(request);
    final Map<String, Map<String, Object>> map = new HashMap<>();
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
      final Date fromDate = sdf.parse(request.getParameter(Constants.FROMDATE));
      final Date toDate = sdf.parse(request.getParameter(Constants.TODATE));
      final long fromBranchId = Long.parseLong(request.getParameter(Constants.FORMID));
      final long toBranchId = Long.parseLong(request.getParameter("toId"));
      final long toSectionId = Long.parseLong(request.getParameter(Constants.TOSECTIONID));
      final long fromSectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));

      final String toType = request.getParameter(Constants.TOTYPE);
      final String fromType = request.getParameter(Constants.FORMTYPE);
      if (toType.contentEquals(Constants.BRANCH)) {
        List<InvoiceBill> invoiceBills = null;
        if (fromType.contentEquals(Constants.BRANCH)) {
          invoiceBills =
              invoiceBillService.getAllInvoiceDetailsByDatesAndBranchAndBranchVendor(
                  fromDate, toDate, orgId, toBranchId, fromBranchId);
          for (final InvoiceBill invoiceBill : invoiceBills) {
            final List<InventoryHistory> inventoryHistoryList =
                inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                    orgId, invoiceBill.getId());
            for (final InventoryHistory inventoryHistory : inventoryHistoryList) {
              if (map.containsKey(
                  inventoryHistory.getInventoryId().getProduct().getProductName())) {
                final Map<String, Object> productMap =
                    map.get(inventoryHistory.getInventoryId().getProduct().getProductName());
                final BigDecimal qty = (BigDecimal) productMap.get("qty");
                productMap.put("qty", inventoryHistory.getQuantity().add(qty));
                productMap.put(
                    Constants.PRICE,
                    ((BigDecimal) productMap.get("qty"))
                        .multiply(
                            inventoryHistory.getInventoryId().getProduct().getProductPrice()));
                productMap.put(
                    "name", inventoryHistory.getInventoryId().getProduct().getProductName());
                map.put(
                    inventoryHistory.getInventoryId().getProduct().getProductName(), productMap);
              } else {
                final Map<String, Object> productMap = new HashMap<>();
                productMap.put("qty", inventoryHistory.getQuantity());
                productMap.put(
                    Constants.PRICE,
                    inventoryHistory
                        .getInventoryId()
                        .getProduct()
                        .getProductPrice()
                        .multiply((BigDecimal) productMap.get("qty")));
                productMap.put(
                    "name", inventoryHistory.getInventoryId().getProduct().getProductName());
                map.put(
                    inventoryHistory.getInventoryId().getProduct().getProductName(), productMap);
              }
            }
          }
        } else if (fromType.contentEquals(Constants.SECTION)) {
          invoiceBills =
              invoiceBillService.getAllInvoiceDetailsByDatesAndBranchAndFromSectionId(
                  fromDate, toDate, orgId, toBranchId, fromSectionId);
          for (final InvoiceBill invoiceBill : invoiceBills) {
            final List<InventoryHistory> inventoryHistoryList =
                inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                    orgId, invoiceBill.getId());
            for (final InventoryHistory inventoryHistory : inventoryHistoryList) {
              if (map.containsKey(
                  inventoryHistory.getInventoryId().getProduct().getProductName())) {
                final Map<String, Object> productMap =
                    map.get(inventoryHistory.getInventoryId().getProduct().getProductName());
                final BigDecimal qty = (BigDecimal) productMap.get("qty");
                productMap.put("qty", inventoryHistory.getQuantity().add(qty));
                productMap.put(
                    Constants.PRICE,
                    ((BigDecimal) productMap.get("qty"))
                        .multiply(
                            inventoryHistory.getInventoryId().getProduct().getProductPrice()));
                productMap.put(
                    "name", inventoryHistory.getInventoryId().getProduct().getProductName());
                map.put(
                    inventoryHistory.getInventoryId().getProduct().getProductName(), productMap);
              } else {
                final Map<String, Object> productMap = new HashMap<>();
                productMap.put("qty", inventoryHistory.getQuantity());
                productMap.put(
                    Constants.PRICE,
                    inventoryHistory
                        .getInventoryId()
                        .getProduct()
                        .getProductPrice()
                        .multiply((BigDecimal) productMap.get("qty")));
                productMap.put(
                    "name", inventoryHistory.getInventoryId().getProduct().getProductName());
                map.put(
                    inventoryHistory.getInventoryId().getProduct().getProductName(), productMap);
              }
            }
          }
        }
      } else if (toType.contentEquals(Constants.SECTION)) {
        List<InventorySectionDataHistory> inventorySectionDataHistories = null;
        if (fromType.contentEquals(Constants.BRANCH)) {
          inventorySectionDataHistories =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromBranchId(
                      fromDate, toDate, orgId, toSectionId, fromBranchId);
          for (final InventorySectionDataHistory inventorySectionDataHistory :
              inventorySectionDataHistories) {
            if (map.containsKey(inventorySectionDataHistory.getProductId().getProductName())) {
              final Map<String, Object> productMap =
                  map.get(inventorySectionDataHistory.getProductId().getProductName());
              final BigDecimal qty = (BigDecimal) productMap.get("qty");
              productMap.put("qty", inventorySectionDataHistory.getQuantity().add(qty));
              productMap.put(
                  Constants.PRICE,
                  ((BigDecimal) productMap.get("qty"))
                      .multiply(inventorySectionDataHistory.getProductId().getProductPrice()));
              productMap.put("name", inventorySectionDataHistory.getProductId().getProductName());
              map.put(inventorySectionDataHistory.getProductId().getProductName(), productMap);
            } else {
              final Map<String, Object> productMap = new HashMap<>();
              productMap.put("qty", inventorySectionDataHistory.getQuantity());
              productMap.put(
                  Constants.PRICE,
                  inventorySectionDataHistory
                      .getProductId()
                      .getProductPrice()
                      .multiply((BigDecimal) productMap.get("qty")));
              productMap.put("name", inventorySectionDataHistory.getProductId().getProductName());
              map.put(inventorySectionDataHistory.getProductId().getProductName(), productMap);
            }
          }
        } else if (fromType.contentEquals(Constants.SECTION)) {
          inventorySectionDataHistories =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromSectionId(
                      fromDate, toDate, orgId, toSectionId, fromSectionId);
          for (final InventorySectionDataHistory inventorySectionDataHistory :
              inventorySectionDataHistories) {
            if (map.containsKey(inventorySectionDataHistory.getProductId().getProductName())) {
              final Map<String, Object> productMap =
                  map.get(inventorySectionDataHistory.getProductId().getProductName());
              final BigDecimal qty = (BigDecimal) productMap.get("qty");
              productMap.put("qty", inventorySectionDataHistory.getQuantity().add(qty));
              productMap.put(
                  Constants.PRICE,
                  ((BigDecimal) productMap.get("qty"))
                      .multiply(inventorySectionDataHistory.getProductId().getProductPrice()));
              productMap.put("name", inventorySectionDataHistory.getProductId().getProductName());
              map.put(inventorySectionDataHistory.getProductId().getProductName(), productMap);
            } else {
              final Map<String, Object> productMap = new HashMap<>();
              productMap.put("qty", inventorySectionDataHistory.getQuantity());
              productMap.put(
                  Constants.PRICE,
                  inventorySectionDataHistory
                      .getProductId()
                      .getProductPrice()
                      .multiply((BigDecimal) productMap.get("qty")));
              productMap.put("name", inventorySectionDataHistory.getProductId().getProductName());
              map.put(inventorySectionDataHistory.getProductId().getProductName(), productMap);
            }
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error("getissue bill Reports  " + "select required branch or section");
      return new ResponseEntity<>(map, HttpStatus.OK);
    }
    return new ResponseEntity<>(map, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVENTORYSECTION_BY_BRANCH)
  public final List<Map<String, Object>> getInventorySectionNamesByBranchAndOrganisationId(
      final HttpServletRequest request) {
    final List<Map<String, Object>> list = new ArrayList<>();

    final long orgId = jwtTokenUtil.getOrgIdFromToken(request);

    final List<Object[]> branches = branchService.getBranchNamesByOrgId(orgId);

    for (int i = 0; i < branches.size(); i++) {
      final Map<String, Object> map = new HashMap<>();
      map.put("id", branches.get(i)[0]);
      map.put("name", branches.get(i)[1]);
      map.put(Constants.SECTIONID, 0);
      map.put("type", Constants.BRANCH);
      list.add(map);
    }
    final List<Object[]> inventorySections =
        inventorySectionService.getInventorySectionNameByOrgId(orgId);
    for (int j = 0; j < inventorySections.size(); j++) {
      final Map<String, Object> map = new HashMap<>();
      map.put("id", inventorySections.get(j)[2]);
      map.put("name", inventorySections.get(j)[3] + "/" + inventorySections.get(j)[1]);
      map.put(Constants.SECTIONID, inventorySections.get(j)[0]);
      map.put("type", Constants.SECTION);
      list.add(map);
    }

    return list;
  }

  @GetMapping(path = RouteConstants.INVENTORY_SECTION_LIST)
  public final List<Map<String, Object>> getInventorySectionsByBranchAndOrganisation(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> list = new ArrayList<>();

    final long orgId = jwtTokenUtil.getOrgIdFromToken(request);
    final long branchId = jwtTokenUtil.getBranchIdFromToken(request);
    if (branchId == 0) {
      final List<Object[]> branches = branchService.getBranchNamesByOrgId(orgId);

      for (int i = 0; i < branches.size(); i++) {
        final Map<String, Object> map = new HashMap<>();
        map.put("id", branches.get(i)[0]);
        map.put("name", branches.get(i)[1]);
        map.put(Constants.SECTIONID, 0);
        map.put("type", Constants.BRANCH);
        list.add(map);
      }
      final List<Object[]> inventorySections =
          inventorySectionService.getInventorySectionNameByOrgId(orgId);
      for (int j = 0; j < inventorySections.size(); j++) {
        final Map<String, Object> map = new HashMap<>();
        map.put("id", inventorySections.get(j)[2]);
        map.put("name", inventorySections.get(j)[3] + "/" + inventorySections.get(j)[1]);
        map.put(Constants.SECTIONID, inventorySections.get(j)[0]);
        map.put("type", Constants.SECTION);
        list.add(map);
      }

    } else {
      final Map<String, Object> branchMap = new HashMap<>();
      branchMap.put("id", branchId);
      branchMap.put("name", branchService.getBranchById(branchId).getBranchName());
      branchMap.put(Constants.SECTIONID, 0);
      branchMap.put("type", Constants.BRANCH);
      list.add(branchMap);
      final List<Object[]> inventorySections =
          inventorySectionService.getInventorySectionNamesByOrgIdAndBranchId(orgId, branchId);
      for (int k = 0; k < inventorySections.size(); k++) {
        final Map<String, Object> map = new HashMap<>();
        map.put("id", inventorySections.get(k)[2]);
        map.put("name", inventorySections.get(k)[3] + "/" + inventorySections.get(k)[1]);
        map.put(Constants.SECTIONID, inventorySections.get(k)[0]);
        map.put("type", Constants.SECTION);
        list.add(map);
      }
    }
    return list;
  }

  @GetMapping(path = RouteConstants.TRANSFERBILL_CONSOLIDATEDREPORT)
  public final ResponseEntity<List<Map<String, Object>>> getInvoicebillReports(
      final HttpServletRequest request) throws ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final List<Map<String, Object>> billdetails = new ArrayList<>();

    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
    try {
      final Date fromdate = sdf.parse(request.getParameter(Constants.FROMDATE));

      final Date todate = sdf.parse(request.getParameter(Constants.TODATE));
      final long frombranchId = Long.parseLong(request.getParameter(Constants.FORMID));
      final long tobranchId = Long.parseLong(request.getParameter("toId"));
      final long toSectionId = Long.parseLong(request.getParameter(Constants.TOSECTIONID));
      final long fromSectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));
      final String toTypesection = request.getParameter(Constants.TOTYPE);
      final String fromTypesection = request.getParameter(Constants.FORMTYPE);

      List<InvoiceBill> invoice = null;
      List<Object[]> inventorySectionDataHistory = null;

      if (toTypesection.equals(Constants.SECTION) && fromTypesection.equals(Constants.SECTION)) {

        inventorySectionDataHistory =
            inventorySectionDataHistoryService
                .getInventorySectionDataHistoryByOrgIdBranchIdSectionId(
                    fromdate, todate, toSectionId, fromSectionId, orgId, tobranchId);

        for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
          final Map<String, Object> inventoryMap = new HashMap<>();
          inventoryMap.put(Constants.AMOUNT, inventorySectionDataHistory.get(i)[0]);
          inventoryMap.put("date", sdf.format(inventorySectionDataHistory.get(i)[6]));
          billdetails.add(inventoryMap);
        }

      } else if (toTypesection.equals(Constants.SECTION)
          && fromTypesection.equals(Constants.BRANCH)) {

        inventorySectionDataHistory =
            inventorySectionDataHistoryService.getInventorySectionDataHistoryByOrgIdBranchIdSection(
                fromdate, todate, toSectionId, frombranchId, orgId, tobranchId);

        for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
          final Map<String, Object> inventoryMap = new HashMap<>();
          inventoryMap.put(Constants.AMOUNT, inventorySectionDataHistory.get(i)[0]);
          inventoryMap.put("date", sdf.format(inventorySectionDataHistory.get(i)[6]));
          billdetails.add(inventoryMap);
        }

      } else if (toTypesection.equals(Constants.BRANCH)
          && fromTypesection.equals(Constants.SECTION)) {
        invoice =
            invoiceBillService.getInvoiceBillByOrgIdBranchIdSectionId(
                fromdate, todate, fromSectionId, orgId, tobranchId);
        for (final InvoiceBill invoicedetails : invoice) {
          final Map<String, Object> invoiceMap = new HashMap<>();
          invoiceMap.put(Constants.AMOUNT, invoicedetails.getTotalAmount());
          invoiceMap.put("date", sdf.format(invoicedetails.getDate()));
          billdetails.add(invoiceMap);
        }
      } else if (toTypesection.equals(Constants.BRANCH)
          && fromTypesection.equals(Constants.BRANCH)) {
        invoice =
            invoiceBillService.getInvoiceBillByOrgIdFromBranchId(
                fromdate, todate, frombranchId, orgId, tobranchId);
        for (final InvoiceBill invoicedetails : invoice) {
          final Map<String, Object> invoiceMap = new HashMap<>();
          invoiceMap.put(Constants.AMOUNT, invoicedetails.getTotalAmount());
          invoiceMap.put("date", sdf.format(invoicedetails.getDate()));
          billdetails.add(invoiceMap);
        }
      }

    } catch (final Exception e) {
      LOGGER.error("getInventorySectionDataHistoryReports  " + "All parameters are not passed ");
      return new ResponseEntity<>(billdetails, HttpStatus.OK);
    }
    return new ResponseEntity<>(billdetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.TRASFERBILL_OUTWISE_REPORTS)
  public final ResponseEntity<List<Map<String, Object>>> getInvoicebillReportdetails(
      final HttpServletRequest request) throws ParseException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final List<Map<String, Object>> billdetails = new ArrayList<>();
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
      final Date fromdate = sdf.parse(request.getParameter(Constants.FROMDATE));
      final Date todate = sdf.parse(request.getParameter(Constants.TODATE));
      final long frombranchId = Long.parseLong(request.getParameter(Constants.FORMID));
      final long tobranchId = Long.parseLong(request.getParameter("toId"));
      final long toSectionId = Long.parseLong(request.getParameter(Constants.TOSECTIONID));
      final long fromSectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));
      final String toTypesection = request.getParameter(Constants.TOTYPE);
      final String fromTypesection = request.getParameter(Constants.FORMTYPE);

      List<InvoiceBill> invoicebill = null;
      List<Object[]> inventorySectionDataHistory = null;

      if (toTypesection.equals(Constants.SECTION) && fromTypesection.equals(Constants.SECTION)) {

        inventorySectionDataHistory =
            inventorySectionDataHistoryService
                .getInventorySectionDataHistoryByTransferOrgIdBranchIdSectionId(
                    fromdate, todate, toSectionId, fromSectionId, orgId, tobranchId);

        for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
          final Map<String, Object> inventoryMap = new HashMap<>();
          inventoryMap.put(Constants.TOTALAMOUNT, inventorySectionDataHistory.get(i)[0]);
          inventoryMap.put(Constants.REFERENCENUMBER, inventorySectionDataHistory.get(i)[1]);
          inventoryMap.put("fromSection", inventorySectionDataHistory.get(i)[2]);
          inventoryMap.put(Constants.FROMSECTIONNAME, inventorySectionDataHistory.get(i)[3]);
          inventoryMap.put("toSection", inventorySectionDataHistory.get(i)[4]);
          inventoryMap.put(Constants.TOSECTIONNAME, inventorySectionDataHistory.get(i)[5]);
          inventoryMap.put("date", sdf.format(inventorySectionDataHistory.get(i)[6]));
          inventoryMap.put(Constants.TOBRANCH, inventorySectionDataHistory.get(i)[11]);
          inventoryMap.put(Constants.TOBRANCHNAME, inventorySectionDataHistory.get(i)[12]);

          inventoryMap.put(
              "from",
              inventorySectionDataHistory.get(i)[12] + "/" + inventorySectionDataHistory.get(i)[3]);
          inventoryMap.put(
              "to",
              inventorySectionDataHistory.get(i)[12] + "/" + inventorySectionDataHistory.get(i)[5]);

          billdetails.add(inventoryMap);
        }

      } else if (toTypesection.equals(Constants.SECTION)
          && fromTypesection.equals(Constants.BRANCH)) {

        inventorySectionDataHistory =
            inventorySectionDataHistoryService
                .getInventorySectionDataHistoryByTransferOrgIdBranchIdSection(
                    fromdate, todate, toSectionId, frombranchId, orgId, tobranchId);

        for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
          final Map<String, Object> inventoryMap = new HashMap<>();
          inventoryMap.put(Constants.TOTALAMOUNT, inventorySectionDataHistory.get(i)[0]);
          inventoryMap.put(Constants.REFERENCENUMBER, inventorySectionDataHistory.get(i)[1]);
          inventoryMap.put("fromBranch", inventorySectionDataHistory.get(i)[2]);
          inventoryMap.put(Constants.FROMBRANCHNAME, inventorySectionDataHistory.get(i)[3]);
          inventoryMap.put("toSection", inventorySectionDataHistory.get(i)[4]);
          inventoryMap.put(Constants.TOSECTIONNAME, inventorySectionDataHistory.get(i)[5]);
          inventoryMap.put(Constants.TOBRANCH, inventorySectionDataHistory.get(i)[11]);
          inventoryMap.put(Constants.TOBRANCHNAME, inventorySectionDataHistory.get(i)[12]);
          inventoryMap.put("date", sdf.format(inventorySectionDataHistory.get(i)[6]));

          inventoryMap.put("from", inventorySectionDataHistory.get(i)[3]);
          inventoryMap.put(
              "to",
              inventorySectionDataHistory.get(i)[3] + "/" + inventorySectionDataHistory.get(i)[5]);

          billdetails.add(inventoryMap);
        }

      } else if (toTypesection.equals(Constants.BRANCH)
          && fromTypesection.equals(Constants.SECTION)) {
        invoicebill =
            invoiceBillService.getInvoiceBillByTransferOrgIdBranchIdSectionId(
                fromdate, todate, fromSectionId, orgId, tobranchId);
        for (final InvoiceBill invoice : invoicebill) {
          final Map<String, Object> invoiceMap = new HashMap<>();
          invoiceMap.put(Constants.TOTALAMOUNT, invoice.getTotalAmount());
          invoiceMap.put(Constants.REFERENCENUMBER, invoice.getInvoiceNumber());
          invoiceMap.put(Constants.TOBRANCH, invoice.getBranch().getId());
          invoiceMap.put(Constants.TOBRANCHNAME, invoice.getBranch().getBranchName());
          invoiceMap.put("fromSection", invoice.getFromSectionId().getId());
          invoiceMap.put(Constants.FROMSECTIONNAME, invoice.getFromSectionId().getSectionName());
          invoiceMap.put("date", sdf.format(invoice.getDate()));

          invoiceMap.put(
              "from",
              invoice.getBranch().getBranchName()
                  + "/"
                  + invoice.getFromSectionId().getSectionName());
          invoiceMap.put("to", invoice.getBranch().getBranchName());

          billdetails.add(invoiceMap);
        }
      } else if (toTypesection.equals(Constants.BRANCH)
          && fromTypesection.equals(Constants.BRANCH)) {
        invoicebill =
            invoiceBillService.getInvoiceBillByTransferOrgIdFromBranchId(
                fromdate, todate, frombranchId, orgId, tobranchId);
        for (final InvoiceBill invoice : invoicebill) {
          final Map<String, Object> invoiceMap = new HashMap<>();
          invoiceMap.put(Constants.TOTALAMOUNT, invoice.getTotalAmount());
          invoiceMap.put(Constants.REFERENCENUMBER, invoice.getInvoiceNumber());
          invoiceMap.put(Constants.TOBRANCH, invoice.getBranch().getId());
          invoiceMap.put(Constants.TOBRANCHNAME, invoice.getBranch().getBranchName());
          invoiceMap.put("fromBranch", invoice.getBranchVendor().getId());
          invoiceMap.put(Constants.FROMBRANCHNAME, invoice.getBranchVendor().getBranchName());
          invoiceMap.put("date", sdf.format(invoice.getDate()));

          invoiceMap.put("from", invoice.getBranchVendor().getBranchName());
          invoiceMap.put("to", invoice.getBranch().getBranchName());

          billdetails.add(invoiceMap);
        }
      }

    } catch (Exception e) {
      LOGGER.error("getTransfer bill Reports  " + "select required branch or section");
      return new ResponseEntity<>(billdetails, HttpStatus.OK);
    }
    return new ResponseEntity<>(billdetails, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.UPDATE_INVENTORY_SECTION_DETAILS)
  public final Map<String, String> updateInventorySection(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws JSONException {

    final JSONObject product = new JSONObject(productDetails);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();
    final Long id = product.getLong("id");
    final String key = product.getString("key");
    final String value = product.getString("value");

    final Map<String, String> inventorySectionUpdate = new HashMap<>();
    try {
      final List<InventorySection> inventorySections =
          this.inventorySectionService.getAllInventorySectionByOrgIdBranchId(orgId, branchId);
      for (final InventorySection inv : inventorySections) {
        if (inv.getSectionName().equalsIgnoreCase(value)) {
          inventorySectionUpdate.put(
              Constants.INVENTORYSECTIONUPDATE,
              "This SectionName Already Existed! Please Change With Another.");
          return inventorySectionUpdate;
        }
      }

      final InventorySection inventorySection =
          this.inventorySectionService.getInventorySectionById(id);
      if (key.equals("sectionName")) {
        inventorySection.setSectionName(value);
        inventorySectionUpdate.put(
            Constants.INVENTORYSECTIONUPDATE,
            Constants.UPDATEFORSECTION + inventorySection.getSectionName());
      } else if (key.equals(Constants.CONSUMEDFLAG)) {
        inventorySection.setConsumedFlag(Boolean.parseBoolean(value));
        inventorySectionUpdate.put(
            Constants.INVENTORYSECTIONUPDATE,
            Constants.UPDATEFORSECTION + inventorySection.getSectionName());
      } else if (key.equals(Constants.KOTFLAG)) {
        inventorySection.setKotFlag(Boolean.parseBoolean(value));
        inventorySectionUpdate.put(
            Constants.INVENTORYSECTIONUPDATE,
            Constants.UPDATEFORSECTION + inventorySection.getSectionName());
      } else {
        inventorySectionUpdate.put(Constants.INVENTORYSECTIONUPDATE, "key Name does not match");
      }
      this.inventorySectionService.update(inventorySection);

    } catch (final RuntimeException ex) {
      LOGGER.error("updateProductsDetails" + ex, ex);
    }
    return inventorySectionUpdate;
  }

  @GetMapping(path = RouteConstants.PRODUCT_BY_SECTIONORINVENTORY)
  public final List<Map<String, Object>> getProductsBySectionOrInvenvtory(
      final HttpServletRequest request, final HttpServletResponse response) {

    final long orgId = this.jwtTokenUtil.getOrgIdFromToken(request);
    final long branchId = Long.parseLong(request.getParameter(Constants.FORMID));
    final long sectionId = Long.parseLong(request.getParameter(Constants.FROMSECTIONID));
    final String fromType = request.getParameter(Constants.FORMTYPE);
    final List<Map<String, Object>> list = new ArrayList<>();

    try {
      if (fromType.contentEquals(Constants.BRANCH)) {
        final List<InventoryDetail> inventoryDetailList =
            inventoryDetailService.getAllInventoryDetails(branchId, orgId);
        for (final InventoryDetail inventoryDetail : inventoryDetailList) {
          final Map<String, Object> map = new HashMap<>();
          map.put(Constants.QUANTITY, inventoryDetail.getQuantity());
          map.put(Constants.BUYINGPRICE, inventoryDetail.getBuyingPrice());
          map.put(Constants.PRODUCT, inventoryDetail.getProduct());
          list.add(map);
        }
      } else if (fromType.contentEquals(Constants.SECTION)) {
        final InventorySection inventorySection =
            inventorySectionService.getInventorySectionById(sectionId);
        final List<InventorySectionData> inventorySectionDataList =
            inventorySectionDataService.getInventorySectionDataByOrgIdAndBranchIdAndSectionName(
                orgId, branchId, inventorySection.getSectionName());
        for (final InventorySectionData inventorySectionData : inventorySectionDataList) {
          final Map<String, Object> map = new HashMap<>();
          map.put(Constants.QUANTITY, inventorySectionData.getQuantity());
          map.put(Constants.BUYINGPRICE, inventorySectionData.getProductId().getProductPrice());
          map.put(Constants.PRODUCT, inventorySectionData.getProductId());
          list.add(map);
        }
      }

    } catch (final Exception em) {
      LOGGER.error(Constants.MARKER, "api/products/bysectionorinventory exception is :" + em);
    }
    return list;
  }
}
