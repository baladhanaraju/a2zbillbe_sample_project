package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.EmployeeCounter;
import com.a2zbill.services.EmployeeCounterService;
import com.tsss.basic.security.jwt.Identifier;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeCounterController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

  @Autowired private EmployeeCounterService employeeCounterService;

  @GetMapping(path = RouteConstants.EMPLOYEE_COUNTER_DETAILS)
  public final ResponseEntity<List<EmployeeCounter>> gettingEmployeeCounterServiceDetails1(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<EmployeeCounter> AllEmployeeCounter = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        if (branchId != 0) {
          AllEmployeeCounter =
              this.employeeCounterService.getAllEmployeeCounterDetailsByorgIdBranchId(
                  orgId, branchId);
        } else {
          AllEmployeeCounter =
              this.employeeCounterService.getAllEmployeeCounterDetailsByorgId(orgId);
        }
      } else {
        return new ResponseEntity<>(AllEmployeeCounter, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "gettingEmployeeCounterServiceDetails" + ex);
    }
    return new ResponseEntity<>(AllEmployeeCounter, HttpStatus.OK);
  }
}
