package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.DeliveryOptions;
import com.a2zbill.services.DeliveryOptionsService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeliveryOptionsController {
  private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryOptionsController.class);

  @Autowired private DeliveryOptionsService deliveryOptionsService;

  @GetMapping(path = RouteConstants.GET_DELIVERY_OPTION_DETAILS)
  public final ResponseEntity<Object> getAllDeliveryOptionsDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<DeliveryOptions> DeliveryOptionsDetails = new ArrayList<>();

    try {

      DeliveryOptionsDetails = this.deliveryOptionsService.getAllDeliveryOptions();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getDelivery" + ex);
    }
    return new ResponseEntity<>(DeliveryOptionsDetails, HttpStatus.OK);
  }
}
