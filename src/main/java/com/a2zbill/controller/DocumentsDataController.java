package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.DocumentsData;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductCategory;
import com.a2zbill.services.DocumentsDataService;
import com.a2zbill.services.ProductCategoryService;
import com.a2zbill.services.ProductService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import io.grpc.internal.IoUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class DocumentsDataController {

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private DocumentsDataService documentsDataService;

  @Autowired private ProductCategoryService productCategoryService;

  @Autowired private ProductService productService;

  @PostMapping(path = RouteConstants.SAVE_DOCUMENT)
  public final void saveDocumentsData(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final MultipartFile file,
      @RequestParam("data") final String DocumentDetails)
      throws JSONException, IOException {

    final JSONObject documentdetails = new JSONObject(DocumentDetails);
    final String documentName = documentdetails.getString("documentName");
    final String imageText = documentdetails.getString("imageText");
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final DocumentsData documentsData = new DocumentsData();
    final ByteArrayInputStream image = new ByteArrayInputStream(file.getBytes());
    documentsData.setImage(IoUtils.toByteArray(image));
    documentsData.setDocumentName(documentName);
    final ProductCategory category =
        productCategoryService.getProductCategoryById(
            Long.parseLong(documentdetails.getString("categoryId")));
    documentsData.setCategoryId(category);
    documentsData.setImageData(imageText);
    documentsData.setBranchId(employeeDetails.getBranch());
    documentsData.setOrgId(employeeDetails.getOrganisation());
    documentsData.setCreatedDate(new Date());
    documentsDataService.saveDocumentData(documentsData);
  }

  @PostMapping(path = RouteConstants.SAVE_MULTIPLE_DOCUMENT)
  public final Map<String, Object> saveMultipleDocumentsData(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestParam("files") final MultipartFile[] files,
      @RequestParam("data") final String DocumentDetails)
      throws JSONException, IOException {
    final JSONObject documentdetails = new JSONObject(DocumentDetails);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Map<String, Object> documentMap = new HashMap<>();
    final JSONArray ja = documentdetails.getJSONArray("documents");
    System.out.println("ja lengtah" + ja.length());
    System.out.println("files" + files.length);
    if (files.length == ja.length()) {
      for (int i = 0; i < files.length; i++) {
        final DocumentsData documentsData = new DocumentsData();
        final MultipartFile file = files[i];
        final ByteArrayInputStream image = new ByteArrayInputStream(file.getBytes());
        documentsData.setImage(IoUtils.toByteArray(image));
        final String imageText = ja.getJSONObject(i).getString("imageText");
        documentsData.setImageData(imageText);
        final String documentName = ja.getJSONObject(i).getString("documentName");
        documentsData.setDocumentName(documentName);
        final String categoryId = ja.getJSONObject(i).getString("categoryId");
        final ProductCategory category =
            productCategoryService.getProductCategoryById(Long.parseLong(categoryId));
        documentsData.setCategoryId(category);
        documentsData.setBranchId(employeeDetails.getBranch());
        documentsData.setOrgId(employeeDetails.getOrganisation());
        documentsData.setCreatedDate(new Date());
        documentsData.setModifiedDate(new Date());
        documentsDataService.saveDocumentData(documentsData);
        documentMap.put("Message", "files uploaded successfully");
      }
    } else {
      documentMap.put("Message", "mismatch number of files to documents description");
    }

    return documentMap;
  }

  @GetMapping(path = RouteConstants.GET_DOCUMENT_DETAILS)
  public final ResponseEntity<Object> getDocumentsDetails(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<ProductCategory> productCategorieslist = new ArrayList<>();
    final List<Map<String, Object>> documentDetails = new ArrayList<>();
    final Identifier identifier = (Identifier) request.getAttribute("identifier");
    if (identifier != null) {
      final long orgId = identifier.getOrgId();
      final long branchId = identifier.getBranchId();
      if (branchId != 0) {
        productCategorieslist =
            documentsDataService.getCategoryDetailsByCategoryType(orgId, branchId);
      } else {
        productCategorieslist = documentsDataService.getCategoryDetailsByCategoryTypeByOrg(orgId);
      }
      for (final ProductCategory productCategory : productCategorieslist) {
        System.out.println("productCategorieslist" + productCategorieslist.size());
        final Map<String, Object> productsByCategoryMap = new HashMap<>();
        final List<Product> product =
            productService.getProductsByProductCategoryId(productCategory.getId());
        if (product.isEmpty()) {
          productsByCategoryMap.put("ProductCategoryName", productCategory.getProductTypeName());
          productsByCategoryMap.put("Value", "");
          documentDetails.add(productsByCategoryMap);
        } else {
          productsByCategoryMap.put("ProductCategoryName", productCategory.getProductTypeName());
          productsByCategoryMap.put("Value", product);
          documentDetails.add(productsByCategoryMap);
        }
      }
    } else {
      return new ResponseEntity<>(documentDetails, HttpStatus.UNAUTHORIZED);
    }
    return new ResponseEntity<>(documentDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_DOCUMENT_DATA)
  public final ResponseEntity<Object> getDocumentsDetail(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<ProductCategory> productCategorieslist = new ArrayList<>();
    final Identifier identifier = (Identifier) request.getAttribute("identifier");
    if (identifier != null) {
      final long orgId = identifier.getOrgId();
      final long branchId = identifier.getBranchId();
      if (branchId != 0) {
        productCategorieslist =
            documentsDataService.getCategoryDetailsByCategoryType(orgId, branchId);
      } else {
        productCategorieslist = documentsDataService.getCategoryDetailsByCategoryTypeByOrg(orgId);
      }
    } else {
      return new ResponseEntity<>(productCategorieslist, HttpStatus.UNAUTHORIZED);
    }
    return new ResponseEntity<>(productCategorieslist, HttpStatus.OK);
  }
}
