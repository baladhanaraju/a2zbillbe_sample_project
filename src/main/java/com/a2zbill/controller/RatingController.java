package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.Rating;
import com.a2zbill.services.impl.ProductServiceImpl;
import com.a2zbill.services.impl.RatingServiceImpl;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BasicTemplateServiceException;
import com.tsss.basic.service.EmployeeService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RatingController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RatingController.class);

  @Autowired JwtTokenUtil jwtTokenUtil;

  @Autowired EmployeeService employeeService;

  @Autowired RatingServiceImpl ratingService;

  @Autowired ProductServiceImpl productsService;

  @PostMapping(path = RouteConstants.SAVE_RATING_DETAILS)
  public final Map<String, Object> addRatingDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException, BasicTemplateServiceException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final JSONObject rating = new JSONObject(payload);
    final Rating ratingDetails = new Rating();
    final Map<String, Object> map = new HashMap<>();

    final Long productId = rating.getLong("productId");

    Product products = null;
    try {
      products = this.productsService.getProductsById(productId);
      final Rating ratingDetailss =
          this.ratingService.getRatingByEmpIdAndProductId(productId, employeeDetails.getId());
      if (ratingDetailss != null) {
        if (ratingDetailss.isRatingStatus()) {
          ratingDetailss.setRatingStatus(Constants.STATUSFALSE);
        } else {
          ratingDetailss.setRatingStatus(Constants.STATUSTRUE);
        }
        this.ratingService.update(ratingDetailss);
        map.put("ratingDetailss", "Rating Details updated successfully");
        return map;
      } else {
        ratingDetails.setRatingStatus(Constants.STATUSTRUE);
        final Employee emp = this.employeeService.getMemberById(employeeDetails.getId());

        ratingDetails.setEmpDetails(emp);
        ratingDetails.setProduct(products);
        map.put("ratingDetails", "Rating Details Added sussesfully");
        this.ratingService.save(ratingDetails);
      }
    } catch (final Exception ex) {
      LOGGER.error("saveRatingDetails" + ex, ex);
    }
    return map;
  }

  @PostMapping(path = RouteConstants.UPDATE_RATING_DETAILS)
  public final Rating updateRating(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {

    Rating ratingDetails = new Rating();

    try {

      final JSONObject rating = new JSONObject(payload);
      ratingDetails = this.ratingService.getRatingByRatingId(rating.getLong("id"));
      if (ratingDetails.isRatingStatus()) {
        ratingDetails.setRatingStatus(Constants.STATUSFALSE);

      } else {
        ratingDetails.setRatingStatus(Constants.STATUSTRUE);
      }
      final Product products =
          this.productsService.getProductsById(rating.getLong(Constants.PRODUCT));
      ratingDetails.setProduct(products);
      final Employee emp = this.employeeService.getMemberById(rating.getLong("empDetails"));
      ratingDetails.setEmpDetails(emp);

      this.ratingService.update(ratingDetails);

    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "updateRating " + ex);
    }
    return ratingDetails;
  }

  @GetMapping(path = RouteConstants.GET_ALL_RATING_DETAILS)
  public final List<Rating> getRatingDetails() {
    List<Rating> ratingDetails = new ArrayList<Rating>();

    try {
      ratingDetails = this.ratingService.getAllRatingDetails();

    } catch (final Exception ex) {
      LOGGER.error(Constants.GETRATINGDETAILS + ex);
    }
    return ratingDetails;
  }

  @GetMapping(path = RouteConstants.GET_RATING_BY_PRODUCTID)
  public final Rating getRatingDetailsByProductId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    Rating ratingDetails = null;

    try {
      final String productId = request.getParameter(Constants.PRODUCT);
      if (productId != null && !productId.isEmpty()) {
        ratingDetails = this.ratingService.getRatingByProductId(Long.parseLong(productId));
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error(Constants.GETRATINGDETAILS + ex);
    }
    return ratingDetails;
  }

  @GetMapping(path = RouteConstants.GET_RATING_BY_PRODUCTANDCUSTOMER)
  public final Rating getRatingDetailsByProductIdAndCustomerId(
      final HttpServletRequest request, final HttpServletResponse responce) throws Exception {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long productId = Long.parseLong(request.getParameter(Constants.PRODUCT));
    Rating ratingDetails = null;

    try {
      ratingDetails =
          this.ratingService.getRatingByEmpIdAndProductId(productId, employeeDetails.getId());

    } catch (final RuntimeException ex) {
      LOGGER.error(Constants.GETRATINGDETAILS + ex, ex);
    }
    return ratingDetails;
  }
}
