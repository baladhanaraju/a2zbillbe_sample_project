package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.ExpensesManagement;
import com.a2zbill.services.ExpensesManagementService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.VendorService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExpensesManagementController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExpensesManagementController.class);

  @Autowired private ExpensesManagementService expensesManagementService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private VendorService vendorService;

  @PostMapping(path = RouteConstants.ADD_EXPENSES_DETAILS)
  public final ExpensesManagement addExpencesDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException, ParseException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final JSONObject expenses = new JSONObject(payload);
    ExpensesManagement expensesManagement = new ExpensesManagement();
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    try {
      expensesManagement.setAmount(new BigDecimal(expenses.getString("amount")));
      expensesManagement.setDate(sdf.parse(expenses.getString("date")));
      expensesManagement.setOrganisation(employeeDetails.getOrganisation());
      expensesManagement.setName(expenses.getString("name"));
      expensesManagement.setInvoiceId(expenses.getString("invoiceId"));
      expensesManagement.setCreatedDate(new Date());
      expensesManagement.setModifiedDate(new Date());
      expensesManagement.setVendor(
          vendorService.getVendorDetailsById(expenses.getLong("vendorId")));
      expensesManagement.setBranch(employeeDetails.getBranch());
      expensesManagement.setOrganisation(employeeDetails.getOrganisation());

      this.expensesManagementService.save(expensesManagement);
    } catch (final Exception ex) {
      LOGGER.error("expensesManagement save" + ex, ex);
    }
    return expensesManagement;
  }

  @GetMapping(path = RouteConstants.GET_EXPENSES_MANAGEMENT_DETAILS)
  public final List<ExpensesManagement> getExpencesManagementDetails() {
    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement = this.expensesManagementService.getAllExpensesManagements();

    } catch (final Exception ex) {
      LOGGER.error("getExpencesManagementDetails " + ex);
    }
    return expensesManagement;
  }

  @GetMapping(path = RouteConstants.GET_EXPENSES_MANAGEMENT_DETAILS_BY_DATE)
  public final List<ExpensesManagement> getExpensesManagementDetailsByDate(
      final HttpServletRequest request, final HttpServletResponse response) throws ParseException {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = Long.parseLong(request.getParameter("branchId"));
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date date = sdf.parse(request.getParameter("date"));
    try {
      expensesManagement =
          expensesManagementService.getExpensesByOrgIdAndBranchIdAndDate(orgId, branchId, date);
    } catch (final Exception ex) {
      LOGGER.error("getExpensesManagementDetailsByDate " + ex);
    }
    return expensesManagement;
  }

  @PostMapping(path = RouteConstants.UPDATE_EXPENSES_MANAGEMENT)
  public final Map<String, String> updateExpensesManagement(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String expensesDetails)
      throws JSONException {

    final JSONObject jsonObject = new JSONObject(expensesDetails);
    final Long id = jsonObject.getLong("id");
    final String key = jsonObject.getString("key");
    final String value = jsonObject.getString("value");

    final Map<String, String> expensesUpdate = new HashMap<>();
    try {
      ExpensesManagement expenses = expensesManagementService.getExpensesManagementById(id);
      if (key.equals("name")) {
        expenses.setName(value);
        expenses.setModifiedDate(new Date());
        expensesUpdate.put(Constants.EXPENSESUPDATE, Constants.EXPENSESMESSAGE + id);
      } else if (key.equals("amount")) {
        expenses.setAmount(new BigDecimal(value));
        expenses.setModifiedDate(new Date());
        expensesUpdate.put(Constants.EXPENSESUPDATE, Constants.EXPENSESMESSAGE + id);
      } else if (key.equals("invoiceId")) {
        expenses.setInvoiceId(value);
        expenses.setModifiedDate(new Date());
        expensesUpdate.put(Constants.EXPENSESUPDATE, Constants.EXPENSESMESSAGE + id);
      } else {
        expensesUpdate.put("expensesUpdate", "key Name does not match");
      }
      expensesManagementService.update(expenses);

    } catch (final Exception em) {
      LOGGER.error("ExpensesManagement update " + em);
    }
    return expensesUpdate;
  }

  @PostMapping(path = RouteConstants.DELETE_EXPENSES_DETAILS_BY_ID)
  public final void deleteExpensesDetailsById(
      final HttpServletRequest request, final HttpServletResponse response) {

    try {
      final Long id = Long.parseLong(request.getParameter("id"));
      expensesManagementService.delete(expensesManagementService.getExpensesManagementById(id));
    } catch (final Exception em) {
      LOGGER.error("delete exception Management " + em);
    }
  }
}
