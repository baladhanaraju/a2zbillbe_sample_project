package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductPackaging;
import com.a2zbill.domain.Subscription;
import com.a2zbill.services.ProductPackagingService;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.SubscriptionService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductPackagingController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductPackagingController.class);
  @Autowired private JwtTokenUtil jwtTokenUtil;
  @Autowired private EmployeeService employeeService;
  @Autowired private ProductPackagingService productPackagingService;
  @Autowired private ProductService productService;
  @Autowired private SubscriptionService subscriptionService;

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.SAVE_PRODUCT_PACKAGING_DETAILS)
  public void saveProductPackagingDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {

    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final JSONObject json = new JSONObject(payload);
      final long productId = json.getLong(Constants.PRODUCTID);
      final JSONArray ja = new JSONArray(json.getString("menuproductsId"));
      for (int i = 0; i < ja.length(); i++) {
        final ProductPackaging productPackagingDetails = new ProductPackaging();
        final JSONObject productJson = ja.getJSONObject(i);
        productPackagingDetails.setParentproductId(productService.getProductsById(productId));
        productPackagingDetails.setProduct(
            productService.getProductsById(productJson.getLong("id")));
        productPackagingService.save(productPackagingDetails);
      }
    } catch (final Exception ex) {
      LOGGER.error("saveProductPackgingDetails" + ex);
    }
  }

  @PostMapping(path = RouteConstants.SAVE_PRODUCTPACKAGING_DETAILS)
  @SuppressWarnings("unused")
  public final Map<String, Object> saveProductPackaging(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws Exception {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    // final Employee employeeDetails =
    // this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final JSONObject json = new JSONObject(payload);
    final List<Long> productList = new ArrayList<>();
    final Map<String, Object> productMap = new HashMap<>();
    Product product = new Product();
    final long productId = json.getLong(Constants.PRODUCTID);
    try {
      final JSONArray ja = new JSONArray(json.getString("menuproductsId"));
      for (int i = 0; i < ja.length(); i++) {
        final JSONObject productJson = ja.getJSONObject(i);
        Long id = productJson.getLong("id");
        productList.add(id);
      }
      product = productService.getProductsById(productId);
      productMap.put("packageProducts", productList);
      product.setProductPackaging(productMap);
      productService.save(product);
    } catch (final RuntimeException ex) {
      LOGGER.error("saveProductPackgingDetails" + ex, ex);
    }
    return productMap;
  }

  @GetMapping(path = RouteConstants.GET_ALL_PRODUCTPACKAGING_DETAILS)
  @SuppressWarnings({"unused", "unchecked"})
  public final Map<String, List<String>> getProductPackagingDetails(
      final HttpServletRequest request, final HttpServletResponse responce) throws Exception {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Branch branch = employeeDetails.getBranch();
    final long orgId = employeeDetails.getOrganisation().getId();
    final List<Integer> productList = new ArrayList<>();
    final List<String> customerList = new ArrayList<>();
    final Map<String, List<String>> map = new HashMap<>();
    final String dateString = request.getParameter("date");
    final Long productId = Long.parseLong(request.getParameter(Constants.PRODUCTID));
    final Long childrenProductId = Long.parseLong(request.getParameter("childrenProductId"));
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date date = null;
    try {
      if (dateString.equals("0")) {
      } else {
        date = sdf.parse(dateString);
      }
    } catch (final ParseException ex) {
      LOGGER.info(Constants.MARKER, "context", ex);
    }
    List<Subscription> subscriptions = null;
    try {
      if (date == null && productId == 0l) {
        if (branch == null) {
          subscriptions = this.subscriptionService.getSubscriptionDetailsByOrgId(orgId);
        } else {
          subscriptions =
              this.subscriptionService.getSubscriptionDetailsByOrgIdAndBranchId(
                  orgId, branch.getId());
        }
      } else if (date == null && productId != 0l) {
        if (branch == null) {

          subscriptions =
              this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgId(productId, orgId);
        } else {
          subscriptions =
              this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
                  productId, orgId, branch.getId());
        }
      } else if (date != null && productId == 0l) {
        if (branch == null) {
          subscriptions =
              this.subscriptionService.getSubscriptionDetailsByDateAndOrgId(date, orgId);
        } else {
          subscriptions =
              this.subscriptionService.getSubscriptionDetailsByDateAndOrgIdAndbranchId(
                  date, orgId, branch.getId());
        }
      } else if (date != null && productId != 0l) {
        if (branch == null) {
          subscriptions =
              this.subscriptionService.getSubscriptionDetailsByDateAndproductIdAndOrgId(
                  date, productId, orgId);
        } else {
          subscriptions =
              this.subscriptionService.getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
                  date, productId, orgId, branch.getId());
        }
      }
      for (Subscription subscription : subscriptions) {
        final Map<String, Object> subscriptionData = new HashMap<>();
        subscriptionData.put("subscriptionId", subscription.getId());
        subscriptionData.put("employeeId", subscription.getEmployee());
        subscriptionData.put("customer", subscription.getCustomer());
        subscriptionData.put("autoRenewal", subscription.isAutoRenewal());
        subscriptionData.put("products", subscription.getProduct());
        final Map<String, Object> productPack = subscription.getProduct().getProductPackaging();
        if (productPack != null) {
          final List<Integer> list = (List<Integer>) productPack.get("packageProducts");
          for (Integer id : list) {
            if (childrenProductId.longValue() == id) {
              customerList.add(subscription.getCustomer().getCustomerName());
            }
            map.put("customer", customerList);
          }
        }
      }
    } catch (final RuntimeException ex) {
      LOGGER.error("getProductPackgingDetails" + ex, ex);
    }
    return map;
  }
}
