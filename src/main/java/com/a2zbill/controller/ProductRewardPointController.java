package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductRewardPoint;
import com.a2zbill.services.ProductRewardPointsService;
import com.a2zbill.services.ProductService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductRewardPointController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductRewardPointController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private ProductRewardPointsService productRewardPointsService;

  @Autowired private ProductService productService;

  @PostMapping(path = RouteConstants.SAVE_PRODUCT_REWARDS_POINTS_DETAILS)
  public final ProductRewardPoint addProductRewardPoint(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final JSONObject productReward = new JSONObject(payload);
    final ProductRewardPoint productRewardPoint = new ProductRewardPoint();
    try {
      final Product products = productService.getProductsById(productReward.getLong("productId"));
      productRewardPoint.setProduct(products);
      productRewardPoint.setRewardPonts(productReward.getLong("rewardPoints"));
      productRewardPoint.setBranchId(employeeDetails.getBranch());
      productRewardPoint.setOrgId(employeeDetails.getOrganisation());
      productRewardPointsService.save(productRewardPoint);
    } catch (final RuntimeException e) {
      LOGGER.error("addProductRewardDetails" + e, e);
    }
    return productRewardPoint;
  }

  @GetMapping(path = RouteConstants.GET_ALL_PRODUCT_REWARDS_POINTS)
  public final ResponseEntity<Object> getallFormTypeDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<ProductRewardPoint> productRewardPoint = new ArrayList<ProductRewardPoint>();

    try {
      productRewardPoint = productRewardPointsService.getAllProductRewardPoints();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProductRewardDetails" + e);
    }
    return new ResponseEntity<>(productRewardPoint, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_PRODUCTREWARDSPOINTS_BY_ORGANDBRANCH)
  public final List<ProductRewardPoint> getallproductrewardDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<ProductRewardPoint> productRewardPoint = null;

    try {
      productRewardPoint = productRewardPointsService.getAllProductRewardPoints();

    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getProductRewardDetails" + e);
    }
    return productRewardPoint;
  }
}
