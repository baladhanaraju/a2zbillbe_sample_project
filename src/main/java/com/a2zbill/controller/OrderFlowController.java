package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.Flow;
import com.a2zbill.domain.OrderFlow;
import com.a2zbill.domain.Product;
import com.a2zbill.services.CartService;
import com.a2zbill.services.FlowService;
import com.a2zbill.services.OrderFlowService;
import com.a2zbill.services.ProductService;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderFlowController {

  private static final Logger LOGGER = LoggerFactory.getLogger(OrderFlowController.class);

  private static final String CART_STATUS = "Open";

  @Autowired private OrderFlowService orderFlowService;

  @Autowired private ProductService productsService;

  @Autowired private CartService cartService;

  @Autowired private FlowService flowService;

  @PostMapping(path = RouteConstants.ADD_ORDER_FLOW_DETAILS)
  public final OrderFlow addOrderFlowDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload) {

    final OrderFlow orderFlowDetails = new OrderFlow();
    try {
      final JSONObject jsonObject = new JSONObject(payload);
      final String code = jsonObject.getString("productcode");
      final Long counterNumber = jsonObject.getLong("counterNumber");
      final Product productDetails = this.productsService.getProductsByProductCode(code);
      final Cart cart = this.cartService.getCartDetailsOnStatus(CART_STATUS, counterNumber);
      final Flow flowId = this.flowService.getFlowById(jsonObject.getLong("flowId"));
      orderFlowDetails.setProducts(productDetails);
      orderFlowDetails.setCartDetails(cart);
      orderFlowDetails.setFlow(flowId);
      orderFlowDetails.setTime(new Date(System.currentTimeMillis()));
      orderFlowDetails.setQuantity(jsonObject.getLong("quantity"));
      this.orderFlowService.save(orderFlowDetails);
    } catch (final Exception ex) {
      LOGGER.error("addOrderFlowDetails" + ex, ex);
    }
    return orderFlowDetails;
  }

  @GetMapping(path = RouteConstants.GET_ORDER_FLOW)
  public final ResponseEntity<Object> getOffers() {

    List<OrderFlow> orderFlow = null;

    try {
      orderFlow = this.orderFlowService.getAllOrderFlow();
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.info("getOfferDetails " + ex);
    }
    return new ResponseEntity<>(orderFlow, HttpStatus.OK);
  }
}
