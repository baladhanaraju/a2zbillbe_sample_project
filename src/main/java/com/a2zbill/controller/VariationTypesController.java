package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.VariationTypes;
import com.a2zbill.services.VariationTypesService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VariationTypesController {

  private static final Logger LOGGER = LoggerFactory.getLogger(VariationTypesController.class);

  @Autowired private VariationTypesService variationTypesService;

  @GetMapping(path = RouteConstants.GET_ALL_VARIATION_TYPES)
  public final ResponseEntity<Object> getAllVariationTypes() {

    List<VariationTypes> list = new ArrayList<VariationTypes>();
    try {
      list = this.variationTypesService.getAllVariationTypesDetails();

    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (Exception em) {
      LOGGER.error(Constants.MARKER, "Get All VariatonTypes" + em);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_VARIATION_TYPE_OPTIONS)
  public final List<Map<String, Object>> getOptionsByVariationType(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> optionsmap = new ArrayList<>();
    final long variationTypeId = Long.parseLong(request.getParameter("variationTypeId"));
    final List<VariationTypes> variationTypes =
        this.variationTypesService.getOptionsByVariationType(variationTypeId);

    for (VariationTypes variationType : variationTypes) {

      Map<String, Object> map = new HashMap<>();
      map.put("options", variationType.getPredefinedOptions());
      optionsmap.add(map);
    }
    return optionsmap;
  }
}
