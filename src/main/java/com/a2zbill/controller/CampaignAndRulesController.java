/*package com.a2zbill.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.a2zbill.domain.Campaign;
import com.a2zbill.domain.Offers;
import com.a2zbill.domain.Rules;
import com.a2zbill.services.CampaignService;
import com.a2zbill.services.OffersService;
import com.a2zbill.services.RulesService;
import com.a2zbill.services.impl.TokenServiceImpl;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Template;
import com.tsss.basic.service.TemplateService;


@RestController
public class CampaignAndRulesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CampaignAndRulesController.class);

	@Autowired
	TokenServiceImpl tokenServiceImpl;

	@Autowired
	CampaignService campaignService;

	@Autowired
	OffersService offersService;

	@Autowired
	RulesService rulesService;

	@Autowired
	TemplateService templateService;


	@RequestMapping(value = "/api/save/campaignandrules/details",method = RequestMethod.POST)
	public final Campaign addCampaignDetails(@RequestBody String campaignDetails,HttpServletRequest request,
			        HttpServletResponse response) throws JSONException,Exception {

		JSONObject campaign = new JSONObject(campaignDetails);
		JSONObject rulesDetails  = (JSONObject)campaign.get("groups");
		// for getting login Credentials
				Employee employeeDetails = tokenServiceImpl.getLoginCredentials(request);

		Campaign campaigns = new Campaign();

		try {
			String startDateString = campaign.getString("startDate");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = sdf.parse(startDateString);

			String endDateString = campaign.getString("endDate");
			Date endDate = sdf.parse(endDateString);

			Offers offers = offersService.getOffersById(campaign.getLong("offerId"));
			Rules rulesObj = new Rules();

			try {
				rulesObj.setRuleName(campaign.getString("ruleName"));
				rulesObj.setBranchId(employeeDetails.getBranch());
				rulesObj.setOrgId(employeeDetails.getOrganisation());

				if (rulesDetails.has("billingoperator") && rulesDetails.has("visitsoperator") && rulesDetails.has("rewardsoperator")) {

					String billingOperator = rulesDetails.getString("billingoperator");
					String billingAmount = rulesDetails.getString("billingamount");
					String visitsOperator = rulesDetails.getString("visitsoperator");
					String visitsNumber = rulesDetails.getString("visitsnumber");
					String rewardsOperator = rulesDetails.getString("rewardsoperator");
					String rewardPoints = rulesDetails.getString("rewardpoints");
				    String value = "total_amount"+billingOperator+billingAmount+" AND total_visits"+visitsOperator+visitsNumber+" AND total_rewards"+rewardsOperator+rewardPoints;
					String queryExample = "select customer_id from final_rewards2 where "+value;
					rulesObj.setQuery(queryExample);
				}
				else if(rulesDetails.has("billingoperator") && rulesDetails.has("visitsoperator")) {

					String billingOperator = rulesDetails.getString("billingoperator");
					String billingAmount = rulesDetails.getString("billingamount");
					String visitsOperator = rulesDetails.getString("visitsoperator");
					String visitsNumber = rulesDetails.getString("visitsnumber");
				    String value = "total_amount"+billingOperator+billingAmount+" AND total_visits"+visitsOperator+visitsNumber;
					String queryExample = "select customer_id from final_rewards2 where "+value;
					rulesObj.setQuery(queryExample);
				}
		        else if(rulesDetails.has("billingoperator") && rulesDetails.has("rewardsoperator")) {

					String billingOperator = rulesDetails.getString("billingoperator");
					String billingAmount = rulesDetails.getString("billingamount");
					String rewardsOperator = rulesDetails.getString("rewardsoperator");
					String rewardPoints = rulesDetails.getString("rewardpoints");
				    String value = "total_amount"+billingOperator+billingAmount+" AND total_rewards"+rewardsOperator+rewardPoints;
					String queryExample = "select customer_id from final_rewards2 where "+value;
					rulesObj.setQuery(queryExample);
				}
		        else if(rulesDetails.has("visitsoperator") && rulesDetails.has("rewardsoperator")) {

		        	String rewardsOperator = rulesDetails.getString("rewardsoperator");
		        	String rewardPoints = rulesDetails.getString("rewardpoints");
					String visitsOperator = rulesDetails.getString("visitsoperator");
					String visitsNumber = rulesDetails.getString("visitsnumber");
					String value = "total_visits"+visitsOperator+visitsNumber+" AND total_rewards"+rewardsOperator+rewardPoints;
					String queryExample = "select customer_id from final_rewards2 where "+value;
					rulesObj.setQuery(queryExample);
				}
		        else if(rulesDetails.has("billingoperator")) {
		        	String billingOperator = rulesDetails.getString("billingoperator");
		        	String billingAmount = rulesDetails.getString("billingamount");
		        	String value = "total_amount"+billingOperator+billingAmount;
					String queryExample = "select customer_id from final_rewards2 where "+value;
					rulesObj.setQuery(queryExample);
		        }
		        else if(rulesDetails.has("rewardsoperator")) {
		        	String rewardsOperator = rulesDetails.getString("rewardsoperator");
		        	String rewardPoints = rulesDetails.getString("rewardpoints");
		        	String value = "total_rewards"+rewardsOperator+rewardPoints;
					String queryExample = "select customer_id from final_rewards2 where "+value;
					rulesObj.setQuery(queryExample);
		        }
		        else if(rulesDetails.has("visitsoperator")) {
		        	String visitsOperator = rulesDetails.getString("visitsoperator");
		        	String visitsNumber = rulesDetails.getString("visitsnumber");
		        	String value = "total_visits"+visitsOperator+visitsNumber;
					String queryExample = "select customer_id from final_rewards2 where "+value;
					rulesObj.setQuery(queryExample);
		        }

				rulesService.save(rulesObj);
			}catch (final Exception e) {
				LOGGER.error("RulesControlerr in the save() of" + e);
			}
			Rules rules = rulesService.getRuleDetailsByRuleId(rulesObj.getRuleId());
			Template template = templateService.getTemplateDetailsByTemplateId(campaign.getLong("templateId"));

			campaigns.setCampaignName(campaign.getString("campaignName"));
			campaigns.setDescription(campaign.getString("description"));
			campaigns.setStartDate(startDate);
			campaigns.setEndDate(endDate);
			campaigns.setOfferId(offers);
			campaigns.setRuleId(rules);
			campaigns.setTemplateId(template);
			campaigns.setBranchId(employeeDetails.getBranch());
			campaigns.setOrgId(employeeDetails.getOrganisation());

			campaignService.save(campaigns);
		}catch (final Exception e) {
			LOGGER.error("CampaignController in the save() of" + e);
		}
		return campaigns;
	}


	@SuppressWarnings("unused")
	@RequestMapping(value = "/api/orgbranch/campaignandrules/details",method = RequestMethod.GET)
	public final List<Campaign> getCampaignAndRulesDetailsByOrgAndBranch(HttpServletRequest request,HttpServletResponse responce)
	{
		// for getting login Credentials
				Employee employeeDetails = tokenServiceImpl.getLoginCredentials(request);

		Long orgId = employeeDetails.getOrganisation().getId();
		Branch branch = null;
		branch = employeeDetails.getBranch();

		List<Campaign> campaign = null;


		try {
			if(branch == null) {
				campaign = campaignService.getCampaignDetailsByOrg(orgId);


			}
			else {
				Long branchId = employeeDetails.getBranch().getId();
				campaign = campaignService.getCampaignDetailsByOrgAndBranch(orgId, branchId);

			}
		}catch(Exception e) {
			LOGGER.info("getCampaignAndRulesDeatilsByOrgAndBranch" +e);
		}



		return campaign;
	}
}
*/
