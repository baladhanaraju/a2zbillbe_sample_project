package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductCategory;
import com.a2zbill.domain.StoreTemplate;
import com.a2zbill.services.ProductCategoryService;
import com.a2zbill.services.StoreTemplateService;
import com.a2zbill.services.impl.ProductServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsss.basic.config.KafkaSender;
import com.tsss.basic.domain.CategoryType;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.MyJson;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.CategoryTypeService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.impl.OrganisationServiceImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductCategoryController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductCategoryController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private ProductServiceImpl ProductService;

  @Autowired private OrganisationServiceImpl organisationService;

  @Autowired private ProductCategoryService productCategoryService;

  @Autowired private StoreTemplateService storeTemplateService;

  @Autowired private CategoryTypeService categoryTypeService;

  @Autowired private KafkaSender kafkaSender;

  @GetMapping(path = RouteConstants.PRODUCT_CATEGORY_DETAILS)
  public final ResponseEntity<Object> getProudctCategoryDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        productCategory = productCategoryService.getProductCategorys(orgId);
      } else {
        return new ResponseEntity<>(productCategory, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error("ProductCategoryDetails" + e);
    }
    return new ResponseEntity<>(productCategory, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_PRODUCT_CATEGORY_DETAILS)
  public final ResponseEntity<Object> getProudctCategory(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Product> productCategory = new ArrayList<>();
    try {
      final String productCategoryId = request.getParameter("productCategoryId");
      if (productCategoryId != null && !productCategoryId.isEmpty()) {
        productCategory =
            ProductService.getProductsByProductCategoryId(Long.parseLong(productCategoryId));
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("proudctsproductcategorydetails" + e);
    }
    return new ResponseEntity<>(productCategory, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_CATEGORY_DETAILS_ORGBRANCHID)
  public final ResponseEntity<Object> getProudctCategoryDetailsByBranchOrg(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Object[]> productCategory = new ArrayList<>();
    final List<Map<String, Object>> categorylist = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) productCategory = productCategoryService.getProductCategorysByOrg(orgId);
        else
          productCategory = productCategoryService.getProductCategorysByOrgAndBranch(orgId, branch);
        for (int i = 0; i < productCategory.size(); i++) {
          final Map<String, Object> categorydata = new HashMap<String, Object>();

          categorydata.put("categoryId", productCategory.get(i)[0]);

          categorydata.put("productTypeName", productCategory.get(i)[1]);
          categorylist.add(categorydata);
        }
      } else {
        return new ResponseEntity<>(categorylist, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProudctCategoryDetails" + e);
    }
    return new ResponseEntity<>(categorylist, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_CATEGORY_FEATURE_DETAILS_ID)
  public final MyJson getFeaturesDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    MyJson productCategory = new MyJson();
    final long id = Long.parseLong(request.getParameter("id"));
    try {
      productCategory = productCategoryService.getFeaturesById(id);
    } catch (final Exception e) {
      LOGGER.error("FeatureDetails" + e);
    }
    return productCategory;
  }

  @PostMapping(path = RouteConstants.SAVE_PRODUCT_CATEGORY)
  public final ResponseEntity<Object> addProductCategoryDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productcategoryDetails) {

    final Map<String, Object> categoryMap = new HashMap<String, Object>();
    final Map<String, Object> productType = new HashMap<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final ProductCategory productCategory = new ProductCategory();
        ProductCategory productTypeDetails = null;
        final Employee employeeDetails =
            employeeService.getEmployeeByEmployeeNumber(identifier.getUserId());
        final JSONObject product = new JSONObject(productcategoryDetails);
        final String productTypeName = product.getString("productTypeName");
        productTypeDetails =
            productCategoryService.getProductCategoryByProductTypeName(
                productTypeName.toUpperCase());
        if (productTypeDetails != null
            && productTypeDetails.getProductTypeName().equalsIgnoreCase(productTypeName)) {
          productType.put("message", "This Category Already Exists");
          productType.put("flag", "false");
          return new ResponseEntity<>(productType, HttpStatus.OK);
        }
        productCategory.setProductTypeName(productTypeName.toUpperCase());
        if (employeeDetails != null) {
          if (employeeDetails.getBranch() != null) {
            productCategory.setBranch(employeeDetails.getBranch());
          } else {
            productCategory.setBranch(null);
          }
          productCategory.setOrganisation(employeeDetails.getOrganisation());
        }
        productCategory.setProductTypeDescription(product.getString("productTypeDescription"));
        if (product != null && product.has("parentId")) {
          productCategory.setParentId(product.getLong("parentId"));
        } else {
          productCategory.setParentId(0l);
        }
        if (product != null && product.has("productImage")) {
          productCategory.setImage(product.getString("productImage"));
        } else {
          productCategory.setImage("");
        }
        final CategoryType categoryType =
            categoryTypeService.getCategoriesById(product.getLong("categorytypes"));
        productCategory.setCategoryType(categoryType);
        if (product != null && product.has("redeemFlag")) {
          productCategory.setRedeemFlag(product.getBoolean("redeemFlag"));
        } else {
          productCategory.setRedeemFlag(false);
        }
        productCategoryService.save(productCategory);
        productType.put("message", "product category suceesfully saved ");
        categoryMap.put("categories", productTypeName);
        categoryMap.put("type", "category");

        final ObjectMapper objMapper = new ObjectMapper();
        final String kafkadata = objMapper.writeValueAsString(categoryMap);
        kafkaSender.sendMail(kafkadata);
      } else {
        return new ResponseEntity<>(productType, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      productType.put("message", "product category unable to saved ");
      LOGGER.error("saveproductcategory" + e);
    }
    return new ResponseEntity<>(productType, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCTCATEGORY_ORGID_BRANCHID)
  public final ResponseEntity<Object> getProudctCategoryDetailsByBranchIdByOrgId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Object[]> productCategory = new ArrayList<>();
    final List<Map<String, Object>> categoriesdata = new ArrayList<>();

    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) productCategory = productCategoryService.getProductCategorysByOrg(orgId);
        else
          productCategory = productCategoryService.getProductCategorysByOrgAndBranch(orgId, branch);
        for (int i = 0; i < productCategory.size(); i++) {
          final Map<String, Object> category = new HashMap<String, Object>();
          category.put("id", productCategory.get(i)[0]);

          category.put("categoryName", productCategory.get(i)[1]);
          categoriesdata.add(category);
        }

      } else {
        return new ResponseEntity<>(categoriesdata, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProudctCategoryDetails" + e);
    }
    return new ResponseEntity<>(categoriesdata, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_CATEGORY_CHILD_DETAILS)
  public final ResponseEntity<Object> getProudctCategoryAndChildData(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final List<Map<String, Object>> categoriesdata = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        final List<Map<String, Object>> productCategorys =
            productCategoryService.getProductCategoryNameAndIdMap(branch, orgId);
        for (final Map<String, Object> category : productCategorys) {
          if (category != null && category.get("id") != null) {
            final Map<String, Object> categorydata = new HashMap<String, Object>();
            final Long catId = (Long) category.get("id");
            categorydata.put("id", catId);
            categorydata.put("name", category.get("productTypeName"));
            final List<Map<String, Object>> products =
                ProductService.getProductNameAndIdsByProductCategoryId(catId);
            categorydata.put("children", products);
            categoriesdata.add(categorydata);
          }
        }
      } else {
        return new ResponseEntity<>(categoriesdata, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("/api/productcategory/child/details" + e);
    }
    return new ResponseEntity<>(categoriesdata, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCTCATEGORY_CATEGORYTYPEID)
  public final ResponseEntity<Object> getProudctCategoryDetailsByCategoryTypeId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final List<ProductCategory> finalProductCategory = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        final long storeTemplateId =
            organisationService.getOrganisationById(orgId).getStoreTemplateId();
        final StoreTemplate storeTemplate =
            storeTemplateService.getStoreTemplateById(storeTemplateId);
        final Integer[] categoryTypeIds = storeTemplate.getCategoryType();
        for (final Integer categoryTypeId : categoryTypeIds) {
          List<ProductCategory> productCategory = new ArrayList<>();
          if (branch == 0) {
            productCategory = productCategoryService.getProductCategorys(orgId);
          } else {
            productCategory =
                productCategoryService.getProductCategorysBasedBranchIdAndOrgId(
                    branch, orgId, categoryTypeId);
          }
          if (productCategory != null && !productCategory.isEmpty()) {
            finalProductCategory.addAll(productCategory);
          }
        }
      } else {
        return new ResponseEntity<>(finalProductCategory, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("/api/productcategory/bycategorytypeid  " + e);
    }
    return new ResponseEntity<>(finalProductCategory, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.REDEEMFLAG_TRUE_PRODUCTS)
  public final ResponseEntity<Object> getProductsForRedeemFlagIsTrue(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> list = new ArrayList<>();
    List<ProductCategory> categories = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0)
          categories = productCategoryService.getProductCategoriesRedeemFlagIsTrue(orgId);
        else
          categories = productCategoryService.getProductCategoriesIfRedeemFlagIsTrue(branch, orgId);
        for (final ProductCategory category : categories) {
          final Long categoryId = category.getId();
          final Map<String, Object> map = new HashMap<>();
          final List<Product> products = ProductService.getProductsByProductCategoryId(categoryId);
          if (products.isEmpty()) {
          } else {
            map.put("categoryId", categoryId);
            map.put("products", products);
            list.add(map);
          }
        }
      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProductsForRedeemFlagIsTrue" + e, e);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCTCATEGORY_FEEDBACK_DETAILS)
  public final ResponseEntity<Object> getProudctCategoryAndChildDataOnlyfeedbackCategory(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long categoryTypeId = 3l;
    final long parentId = 0l;
    final List<Map<String, Object>> categoriesdata = new ArrayList<>();
    List<ProductCategory> productCategorys = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0)
          productCategorys =
              productCategoryService.getProductCategorysBasedOnOrgIdAndParentId(
                  orgId, categoryTypeId, parentId);
        else
          productCategorys =
              productCategoryService.getProductCategorysBasedBranchIdAndOrgIdAndParentId(
                  branch, orgId, categoryTypeId, parentId);
        for (final ProductCategory category : productCategorys) {
          final Map<String, Object> categorydata = new HashMap<>();
          final List<Map<String, Object>> mapdata = new ArrayList<>();
          categorydata.put("id", category.getId());
          categorydata.put("name", category.getProductTypeName());
          List<ProductCategory> childProductCategory = null;
          if (branch == 0)
            childProductCategory =
                productCategoryService.getProductCategoryByBranchIdAndOrgIdAndParentId(
                    branch, orgId, category.getId());
          else
            childProductCategory =
                productCategoryService.getProductCategoryByOrgIdAndParentId(
                    orgId, category.getId());
          for (final ProductCategory childcategory : childProductCategory) {
            final Map<String, Object> productData = new HashMap<>();
            productData.put("id", childcategory.getId());
            productData.put("name", childcategory.getProductTypeName());
            mapdata.add(productData);
          }
          categorydata.put("children", mapdata);
          categoriesdata.add(categorydata);
        }
      } else {
        return new ResponseEntity<>(categoriesdata, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("/api/productcategory/OnlyfeedbackCategory/details" + e);
    }
    return new ResponseEntity<>(categoriesdata, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_CATEGORY_MENUCATEGORY_DETAILS)
  public final ResponseEntity<Object> getProudctCategoryAndChildDataOnlyMenuCategory(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long categoryTypeId = 2l;
    final long parentId = 0l;
    final List<Map<String, Object>> categoriesdata = new ArrayList<>();
    List<ProductCategory> productCategorys = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0)
          productCategorys =
              productCategoryService.getProductCategorysBasedOnOrgIdAndParentId(
                  orgId, categoryTypeId, parentId);
        else
          productCategorys =
              productCategoryService.getProductCategorysBasedBranchIdAndOrgIdAndParentId(
                  branch, orgId, categoryTypeId, parentId);
        for (final ProductCategory category : productCategorys) {
          final Map<String, Object> categorydata = new HashMap<>();
          final List<Map<String, Object>> mapdata = new ArrayList<>();
          categorydata.put("id", category.getId());
          categorydata.put("name", category.getProductTypeName());
          List<ProductCategory> childProductCategory = new ArrayList<>();
          if (branch == 0)
            childProductCategory =
                productCategoryService.getProductCategoryByBranchIdAndOrgIdAndParentId(
                    branch, orgId, category.getId());
          else
            childProductCategory =
                productCategoryService.getProductCategoryByOrgIdAndParentId(
                    orgId, category.getId());
          for (final ProductCategory childcategory : childProductCategory) {
            final Map<String, Object> productData = new HashMap<>();
            productData.put("id", childcategory.getId());
            productData.put("name", childcategory.getProductTypeName());
            mapdata.add(productData);
          }
          categorydata.put("children", mapdata);
          categoriesdata.add(categorydata);
        }
      } else {
        return new ResponseEntity<>(categoriesdata, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.info("/api/productcategory/OnlyfeedbackCategory/details  " + e);
    }
    return new ResponseEntity<>(categoriesdata, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.CATEGORY_PRODUCTS)
  public final ResponseEntity<Object> getProducts(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> list = new ArrayList<>();
    List<ProductCategory> categories = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0)
          categories =
              productCategoryService.getProductCategoriesIfRedeemFlagIsFalseAndOrgId(orgId);
        else
          categories =
              productCategoryService.getProductCategoriesIfRedeemFlagIsFalseAndOrgIdAndBranchId(
                  branch, orgId);
        for (final ProductCategory category : categories) {
          final Long categoryId = category.getId();
          final Map<String, Object> map = new HashMap<>();
          final List<Product> products = ProductService.getProductsByProductCategoryId(categoryId);
          if (products.isEmpty()) {
          } else {
            map.put("categoryId", categoryId);
            map.put("products", products);
            list.add(map);
          }
        }
      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProductsbycategory" + e, e);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.MENUCATEGORY_PRODUCTS)
  public final List<Map<String, Object>> getmenuProducts(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> list = new ArrayList<>();
    List<ProductCategory> categories = new ArrayList<>();
    try {
      final CategoryType categoryType =
          categoryTypeService.getCategoriesByCategoryName("MenuCategory");
      categories =
          productCategoryService.getProductCategoriesByCategoryTypeId(categoryType.getId());
      if (categories != null) {
        for (final ProductCategory category : categories) {
          final String categoryName = category.getCategoryType().getCategoryName();
          final Map<String, Object> map = new HashMap<>();
          final List<Product> products =
              ProductService.getProductsByProductCategoryNameIfRedeemFlagIsFalse("MenuCategory");
          if (!products.isEmpty()) {
            map.put("categoryName", categoryName);
            map.put("products", products);
            list.add(map);
          }
        }
      }
    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProductsbycategory" + e, e);
    }
    return list;
  }

  @GetMapping(path = RouteConstants.CATEGORYNAME_PRODUCTS)
  public final ResponseEntity<Object> getProductsByCategoryName(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<Map<String, Object>> list = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        long categoryTypeId = 0;
        if (orgId == 0) {
          final CategoryType categoryType =
              categoryTypeService.getCategoriesByCategoryName("InventoryCategory");
          if (categoryType != null) {
            categoryTypeId = categoryType.getId();
          }
        }
        final List<String> categoriesNames =
            productCategoryService.getProductCategoryNames(branchId, orgId, categoryTypeId);

        for (final String category : categoriesNames) {
          if (category != null) {
            final Map<String, Object> map = new HashMap<>();
            final List<Map<String, Object>> products =
                ProductService.getProductsByProductCategoryNameMap(category);
            if (!products.isEmpty()) {
              map.put("categoryName", category);
              map.put("products", products);
              list.add(map);
            }
          }
        }
      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error("getProductsbycategory" + e, e);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.MENUCTEGORIES_CATEGORIES)
  public final ResponseEntity<Object> getProductCategoriesOfMenuCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<ProductCategory> categories = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final CategoryType categoryType =
            categoryTypeService.getCategoriesByCategoryName("MenuCategory");
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          categories =
              productCategoryService.getProductCategoriesBasedOnOrgIdAndCategoryType(
                  orgId, categoryType.getId());
        } else {
          categories =
              productCategoryService.getProductCategorysBasedBranchIdAndOrgId(
                  branch, orgId, categoryType.getId());
        }
      } else {
        return new ResponseEntity<>(categories, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(categories, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.MENUCATEGORY_PRODUCT)
  public final ResponseEntity<Object> getProductsOfMenuCategory(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<Product> products = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products = ProductService.getProductsByProductCategoryNameAndOrgId("MenuCategory", orgId);
        } else {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdAndBranchId(
                  "MenuCategory", orgId, branch);
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.MENUCATEGORY_PRODUCT_COUNT)
  public final ResponseEntity<Object> getProductsCountOfMenuCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    final Map<String, Object> ProductsCount = new HashMap<>();
    long products = 0;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsCountByProductCategoryNameAndOrgId("MenuCategory", orgId);
          ProductsCount.put("menuCategoryCount", products);
        } else {
          products =
              ProductService.getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
                  "MenuCategory", orgId, branch);
          ProductsCount.put("menuCategoryCount", products);
        }
      } else {
        return new ResponseEntity<>(ProductsCount, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(ProductsCount, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVENTORY_CATEGORY_PRODUCTS_COUNT)
  public final ResponseEntity<Object> getProductsCountOfInventoryCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    final Map<String, Object> ProductsCount = new HashMap<>();
    long products = 0;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsCountByProductCategoryNameAndOrgId(
                  "InventoryCategory", orgId);
          ProductsCount.put("inventoryCategoryCount", products);
        } else {
          products =
              ProductService.getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
                  "InventoryCategory", orgId, branch);
          ProductsCount.put("inventoryCategoryCount", products);
        }
      } else {
        return new ResponseEntity<>(ProductsCount, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(ProductsCount, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.SERVICECATEGORY_PRODUCTS_COUNT)
  public final ResponseEntity<Object> getProductsCountOfSErviceCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    final Map<String, Object> ProductsCount = new HashMap<>();
    long products = 0;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsCountByProductCategoryNameAndOrgId(
                  "ServiceCategory", orgId);
          ProductsCount.put("serviceCategoryCount", products);
        } else {
          products =
              ProductService.getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
                  "ServiceCategory", orgId, branch);
          ProductsCount.put("serviceCategoryCount", products);
        }
      } else {
        return new ResponseEntity<>(ProductsCount, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(ProductsCount, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.MENUCATEGORY_PRODUCTS_PAGINATION)
  public final ResponseEntity<Object> getPaginationProductsOfMenuCategory(
      final @PathVariable int pageNumber,
      final @PathVariable int maxRecords,
      final HttpServletRequest request,
      final HttpServletResponse response) {
    List<Product> products = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdpagination(
                  "MenuCategory", orgId, pageNumber, maxRecords);
        } else {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
                  "MenuCategory", orgId, branch, pageNumber, maxRecords);
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVENTORYCATEGORY_CATEGORIES)
  public final ResponseEntity<Object> getProductCategoriesOfInventoryCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<ProductCategory> categories = new ArrayList<>();
    try {
      final CategoryType categoryType =
          categoryTypeService.getCategoriesByCategoryName("InventoryCategory");
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          categories =
              productCategoryService.getProductCategoriesBasedOnOrgIdAndCategoryType(
                  orgId, categoryType.getId());
        } else {
          categories =
              productCategoryService.getProductCategorysBasedBranchIdAndOrgId(
                  branch, orgId, categoryType.getId());
        }
      } else {
        return new ResponseEntity<>(categories, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(categories, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.SERVICECATEGORY_CATEGORY)
  public final ResponseEntity<Object> getProductCategoriesOfServiceCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<ProductCategory> categories = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();

        final CategoryType categoryType =
            categoryTypeService.getCategoriesByCategoryName("ServiceCategory");
        final long categoryTypeId = categoryType.getId();
        if (branch == 0) {
          categories =
              productCategoryService.getProductCategoriesBasedOnOrgIdAndCategoryType(
                  orgId, categoryTypeId);
        } else {
          categories =
              productCategoryService.getProductCategorysBasedBranchIdAndOrgId(
                  branch, orgId, categoryTypeId);
        }
      } else {
        return new ResponseEntity<>(categories, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(categories, HttpStatus.UNAUTHORIZED);
  }

  @GetMapping(path = RouteConstants.INVENTORYCATEGORIES_PRODUCTS)
  public final ResponseEntity<Object> getProductsOfInventoryCategory(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<Product> products = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgId("InventoryCategory", orgId);
        } else {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdAndBranchId(
                  "InventoryCategory", orgId, branch);
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVENTORYCATEGORY_PRODUCTS_PAGINATION)
  public final ResponseEntity<Object> getPaginationProductsOfInventoryCategory(
      final @PathVariable int pageNumber,
      final @PathVariable int maxRecords,
      final HttpServletRequest request,
      final HttpServletResponse response) {

    List<Product> products = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdpagination(
                  "InventoryCategory", orgId, pageNumber, maxRecords);
        } else {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
                  "InventoryCategory", orgId, branch, pageNumber, maxRecords);
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.SERVICE_CATEGORY_PRODUCTS)
  public final ResponseEntity<Object> getProductsOfServiceCategory(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<Product> products = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgId("ServiceCategory", orgId);
        } else {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdAndBranchId(
                  "ServiceCategory", orgId, branch);
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.SERVICECATEGORY_PRODUCTS_PAGINATION)
  public final ResponseEntity<Object> getPaginationProductsOfServiceCategory(
      final @PathVariable int pageNumber,
      final @PathVariable int maxRecords,
      final HttpServletRequest request,
      final HttpServletResponse response) {

    List<Product> products = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        if (branch == 0) {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdpagination(
                  "ServiceCategory", orgId, pageNumber, maxRecords);
        } else {
          products =
              ProductService.getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
                  "ServiceCategory", orgId, branch, pageNumber, maxRecords);
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRRODUCTCATEGORY_BYCATEGORYTYPE)
  public final ResponseEntity<Object> getProudctCategoryDetailsByCategorysfinal(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final List<ProductCategory> finalProductCategory = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        final long storeTemplateId =
            organisationService.getOrganisationById(orgId).getStoreTemplateId();
        final StoreTemplate storeTemplate =
            storeTemplateService.getStoreTemplateById(storeTemplateId);
        final Integer[] categoryTypeIds = storeTemplate.getCategoryType();
        for (final Integer categoryTypeId : categoryTypeIds) {
          List<ProductCategory> productCategory = new ArrayList<>();
          if (branch == 0) {
            productCategory = productCategoryService.getProductCategorys(orgId);
          } else {
            productCategory =
                productCategoryService.getProductCategorysBasedBranchIdAndOrgId(
                    branch, orgId, categoryTypeId);
          }
          if (productCategory != null && !productCategory.isEmpty()) {
            finalProductCategory.addAll(productCategory);
          }
        }
      } else {
        return new ResponseEntity<>(finalProductCategory, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "/api/productcategory/bycategorytypeid  " + e);
    }
    return new ResponseEntity<>(finalProductCategory, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.CATEGORYTYPEID_PRODUCT_CATEGORIES)
  public final ResponseEntity<Object> getProductCategoriesByCategoryTypeId(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<ProductCategory> productsCategory = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();
        final long categoryTypeId = Long.parseLong(request.getParameter("categoryTypeId"));
        if (branch == 0) {
          productsCategory =
              productCategoryService.getProductCategoriesBasedOnOrgIdAndCategoryType(
                  categoryTypeId, orgId);
        } else {
          productsCategory =
              productCategoryService.getProductCategorysBasedBranchIdAndOrgId(
                  branch, orgId, categoryTypeId);
        }
      } else {
        return new ResponseEntity<>(productsCategory, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(productsCategory, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCT_CATEGORY)
  public final Map<String, String> updateProductCategory(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse response) {

    final Map<String, String> productCategoryMap = new HashMap<>();
    try {

      final JSONObject jsonObject = new JSONObject(payload);
      final Long id = jsonObject.getLong("id");
      final String key = jsonObject.getString("key");
      final String value = jsonObject.getString("value");

      final ProductCategory productCategory = productCategoryService.getProductCategoryById(id);

      if (key.equals("name")) {
        productCategory.setProductTypeName(value);
        productCategoryMap.put("productCategoryUpdate", "updated for this id " + id);
      } else if (key.equals("productTypeName")) {
        productCategory.setProductTypeName(value);
        productCategoryMap.put("productCategoryUpdate", "updated for this id " + id);
      } else {
        productCategoryMap.put("productCategoryUpdate", "key Name does not match");
      }
      productCategoryService.update(productCategory);
    } catch (final Exception e) {
      LOGGER.error("productCategoryController update" + e, e);
    }
    return productCategoryMap;
  }

  @GetMapping(path = RouteConstants.OFFLINE_SERVICE_PRODUCTS)
  public final List<Product> getProductsOfOrganisationByStoreTemplate(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Product> productList = new ArrayList<Product>();
    final long orgId = jwtTokenUtil.getOrgIdFromToken(request);
    final long storeTemplateId =
        organisationService.getOrganisationById(orgId).getStoreTemplateId();
    try {
      final Integer[] categoryTypeIds =
          storeTemplateService.getStoreTemplateById(storeTemplateId).getCategoryType();
      for (final Integer categoryTypeId : categoryTypeIds) {

        final String productCategoryName =
            categoryTypeService.getCategoriesById(categoryTypeId).getCategoryName();
        final List<Product> products =
            ProductService.getProductsByProductCategoryNameAndOrgId(productCategoryName, orgId);
        productList.addAll(products);
      }
    } catch (final Exception em) {

    }
    return productList;
  }

  @GetMapping(path = RouteConstants.SERVICES_PRODUCT)
  public final ResponseEntity<Object> getProductsdetailsOrganisationByStoreTemplate(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<Product> productList = new ArrayList<Product>();
    Integer[] categoryTypeIds = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        long orgId = identifier.getOrgId();
        if (orgId == 0) {
          orgId = Long.parseLong(request.getParameter("organizationId"));
        }
        final long storeTemplateId =
            organisationService.getOrganisationById(orgId).getStoreTemplateId();
        categoryTypeIds =
            storeTemplateService.getStoreTemplateById(storeTemplateId).getCategoryType();

        for (final Integer categoryTypeId : categoryTypeIds) {

          final String productCategoryName =
              categoryTypeService.getCategoriesById(categoryTypeId).getCategoryName();
          final List<Product> products =
              ProductService.getProductsByProductCategoryNameAndOrgId(productCategoryName, orgId);
          productList.addAll(products);
        }

      } else {
        return new ResponseEntity<>(productList, HttpStatus.UNAUTHORIZED);
      }

    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return new ResponseEntity<>(productList, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ALL_CATEGORY_ORG)
  public final ResponseEntity<Object> getProductCategoriesByOrganisation(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<ProductCategory> productCategory = new ArrayList<ProductCategory>();

    List<ProductCategory> productCategories = new ArrayList<ProductCategory>();
    Integer[] categoryTypeIds = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        long orgId = identifier.getOrgId();
        if (orgId == 0) {
          orgId = Long.parseLong(request.getParameter("organizationId"));
        }
        final long storeTemplateId =
            organisationService.getOrganisationById(orgId).getStoreTemplateId();

        categoryTypeIds =
            storeTemplateService.getStoreTemplateById(storeTemplateId).getCategoryType();
        for (final Integer categoryTypeId : categoryTypeIds) {
          productCategories =
              productCategoryService.getProductCategoriesBasedOnOrgIdAndCategoryType(
                  orgId, categoryTypeId);
          productCategory.addAll(productCategories);
        }
      } else {
        return new ResponseEntity<>(productCategory, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error("productCategoryController " + e, e);
    }
    return new ResponseEntity<>(productCategory, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.CATEGORYNAME_PRODUCTS_RECIPE)
  public final ResponseEntity<Object> getProductsByCategoryNameForReceipe(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<Map<String, Object>> list = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        long categoryTypeId = 0;
        if (orgId == 0) {
          final CategoryType categoryType =
              categoryTypeService.getCategoriesByCategoryName("InventoryCategory");
          if (categoryType != null) {
            categoryTypeId = categoryType.getId();
          }
        }
        final List<Long> categoryIds =
            productCategoryService.getProductCategoryIds(branchId, orgId, categoryTypeId);

        for (final Long category : categoryIds) {
          if (category != null) {
            final Map<String, Object> map = new HashMap<>();
            final List<Map<String, Object>> products =
                ProductService.getProductNameAndIdsByProductCategoryId(category);
            if (!products.isEmpty()) {
              map.put("categoryName", category);
              map.put("products", products);
              list.add(map);
            }
          }
        }
      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error("getProductsbycategory" + e, e);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @RequestMapping(path = RouteConstants.ORG_CATEGORIES)
  public final List<Map<String, Object>> getCategoriesBYOrganisationPath(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @PathVariable("organisation") final String pathUrl) {
    long storeTemplateId = storeTemplateService.getStoreTemplateByorgPathUrl(pathUrl);
    Integer[] object = productCategoryService.getCategoryTypesByStoreTemplateId(storeTemplateId);
    List<Map<String, Object>> listOfCategories = new ArrayList<>();
    for (Integer categoryType : object) {
      List<Object[]> categories =
          productCategoryService.getCategoriesBycategoryType(categoryType.longValue(), pathUrl);

      for (int i = 0; i < categories.size(); i++) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", categories.get(i)[0]);
        map.put("productTypeName", categories.get(i)[1]);
        listOfCategories.add(map);
      }
    }
    return listOfCategories;
  }
}
