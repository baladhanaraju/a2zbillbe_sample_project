package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.DiscountCoupons;
import com.a2zbill.services.DiscountCouponsService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DiscountCouponsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(DiscountCouponsController.class);

  @Autowired private DiscountCouponsService discountCouponsService;

  @PostMapping(path = RouteConstants.SAVE_DISCOUNT_COUPONS)
  public final Map<String, Object> addDiscountCoupons(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload) {
    final Map<String, Object> map = new HashMap<>();
    DiscountCoupons discounts = new DiscountCoupons();
    try {
      final JSONObject discountCupons = new JSONObject(payload);
      final Map<String, Object> map1 = new HashMap<>();
      map1.put("dealName", discountCupons.getString("dealName"));

      List<DiscountCoupons> allDeals = this.discountCouponsService.getAllDiscountCoupons();
      final String offerCode = discountCupons.getString("offerCode");
      if (allDeals != null) {
        for (DiscountCoupons arrayDeals : allDeals) {
          if (arrayDeals.getOfferCode().toUpperCase().equals(offerCode.toUpperCase())) {
            if (arrayDeals.getOfferCode().equals(offerCode)) {
              String message = " Deal already existed ";
              map.put("message", message);
              return map;
            }
          }
        }

        discounts.setDealName(map1);
        discounts.setOfferCode(offerCode);
        discounts.setDiscountAmount(new BigDecimal(discountCupons.getLong("discountAmount")));
        discounts.setFreeItems(discountCupons.getLong("freeItems"));
        discounts.setSoldItems(discountCupons.getLong("soldItems"));
        discounts.setPercentage(discountCupons.getString("percentage"));
        discounts.setStartTime(new Date(System.currentTimeMillis()));
        discounts.setEndTime(new Date(System.currentTimeMillis()));

        map.put("discounts", discounts);
        this.discountCouponsService.save(discounts);
      }
    } catch (final Exception ex) {
      LOGGER.error("DiscountCouponsController in the addDeals() of" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_DISCOUNTCOUPONS)
  public final ResponseEntity<Object> getDiscountCoupons() {

    List<DiscountCoupons> discounts = new ArrayList<>();

    try {
      discounts = this.discountCouponsService.getAllDiscountCoupons();

    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getDiscountCouponsDetails" + ex);
    }
    return new ResponseEntity<>(discounts, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_DEALNAME_BY_ID)
  public final Map<String, Object> getDealNameById(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final Long id = Long.parseLong(request.getParameter("id"));
    Map<String, Object> dealName = null;
    try {
      dealName = this.discountCouponsService.getDealNameById(id);

    } catch (final Exception ex) {
      LOGGER.error("DealNameById" + ex);
    }
    return dealName;
  }

  @GetMapping(path = RouteConstants.GET_ALL_DEALNAMES)
  public final List<Map<String, Object>> getAllDealNames(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Map<String, Object>> dealName = new ArrayList<>();
    try {
      dealName = this.discountCouponsService.getAllDealNames();

    } catch (final Exception ex) {
      LOGGER.error("DealNameDetails" + ex);
    }
    return dealName;
  }
}
