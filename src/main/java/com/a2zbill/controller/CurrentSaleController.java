package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.InventorySection;
import com.a2zbill.domain.InventorySectionDataHistory;
import com.a2zbill.domain.InvoiceBill;
import com.a2zbill.domain.Product;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CartProductService;
import com.a2zbill.services.InventorySectionDataHistoryService;
import com.a2zbill.services.InventorySectionService;
import com.a2zbill.services.InvoiceBillService;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.impl.TokenServiceImpl;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrentSaleController {
  private static final Logger LOGGER = LoggerFactory.getLogger(CurrentSaleController.class);

  @Autowired private BillingService billingService;

  @Autowired private CartProductService cartProductService;

  @Autowired private BranchService branchService;

  @Autowired private ProductService productsService;

  @Autowired private InventorySectionDataHistoryService inventorySectionDataHistoryService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private TokenServiceImpl tokenServiceImpl;

  @Autowired private InvoiceBillService invoiceBillService;

  @Autowired private InventorySectionService inventorySectionService;

  @PostMapping(path = RouteConstants.ADMIN_CURRENTDAY_SALES_PRODUCTWISE_REPORT)
  public final Map<String, Object> getcurrrentDaySaleDetails(
      @RequestBody final String payload, final HttpServletRequest request)
      throws ParseException, JSONException {

    final JSONObject jsonObject = new JSONObject(payload);
    final Map<String, Object> map = new HashMap<>();
    final List<Date> billDates = this.tokenServiceImpl.getReportDates(jsonObject);
    final String branchId = jsonObject.getString("branchId");
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long orgId = employeeDetails.getOrganisation().getId();
      final Date fromDate = billDates.get(0);
      final Date toDate = billDates.get(1);
      List<CartDetail> listCartDetails = null;
      if (branchId == null) {
        listCartDetails =
            this.cartProductService.getCartDetailsGroupByProductNameifBrnachNull(
                fromDate, toDate, orgId);
        if (listCartDetails.isEmpty()) {
          map.put("itemReports", "No Records");
        } else {
          map.put("itemReports", listCartDetails);
        }
        return map;
      } else {
        listCartDetails =
            this.cartProductService.getCartDetailsGroupByProductName(
                fromDate, toDate, orgId, Long.parseLong(branchId));
        if (listCartDetails.isEmpty()) {
          map.put("itemReports", "No Records");
        } else {
          map.put("itemReports", listCartDetails);
        }
        return map;
      }
    } catch (final NullPointerException ne) {
      map.put("itemReports", "Something Went Wrong? Please Enter Correct Details..");
      LOGGER.error(
          Constants.MARKER,
          "/admin/currrentDay/sale/productwise/details  " + "Something Went Wrong?");
      return map;
    }
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @PostMapping(path = RouteConstants.ADMIN_CURRENTDAY_SALES_BRANCHWISE_REPORT)
  public final List<Map<String, Object>> getcurrrentDaySaleBranchwise(
      @RequestBody final String payload, final HttpServletRequest request)
      throws ParseException, JSONException {

    final Map branchMap = new HashMap<>();
    final List<Map<String, Object>> mapdata = new ArrayList<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long orgId = employeeDetails.getOrganisation().getId();
      final List<Branch> allBranch = this.branchService.getBranchesByOrganisationId(orgId);
      final JSONObject jsonObject = new JSONObject(payload);
      final List<Date> billDates = this.tokenServiceImpl.getReportDates(jsonObject);
      final String branchId = jsonObject.getString("branchId");
      final Date fromDate = billDates.get(0);
      final Date toDate = billDates.get(1);
      List<BigDecimal> PurchaseAmount = null;
      if (branchId == null) {
        PurchaseAmount =
            this.billingService.getSumOfBillingDetailsByOrgBranchisNull(fromDate, toDate, orgId);
        if (PurchaseAmount.isEmpty()) {
          branchMap.put("branchreport", "No one Report is found");
          mapdata.add(branchMap);
        } else {
          branchMap.put("OrganisationName", allBranch.get(0).getOrganisation().getOrgName());
          Branch branch = this.branchService.getBranchById(Long.parseLong(branchId));
          branchMap.put("BranchName", branch.getBranchName());
          branchMap.put("PurchaseAmount", PurchaseAmount);
          mapdata.add(branchMap);
        }
      } else {
        PurchaseAmount =
            this.billingService.getSumOfBillingDetailsByBranchAndOrg(
                fromDate, toDate, orgId, Long.parseLong(branchId));
        if (PurchaseAmount.isEmpty()) {
          branchMap.put("branchreport", "No Report is found");
          mapdata.add(branchMap);
        } else {
          branchMap.put("OrganisationName", allBranch.get(0).getOrganisation().getOrgName());
          Branch branch = this.branchService.getBranchById(Long.parseLong(branchId));
          branchMap.put("BranchName", branch.getBranchName());
          branchMap.put("PurchaseAmount", PurchaseAmount);
          mapdata.add(branchMap);
        }
      }
    } catch (final NullPointerException ne) {
      branchMap.put("msg", "Something Went Wrong? Please Enter Correct Details..");
      LOGGER.error(
          Constants.MARKER,
          "/admin/currrentDay/sale/productwise/details  " + "Something Went Wrong?");
      return mapdata;
    }
    return mapdata;
  }

  @PostMapping(
      path = RouteConstants.ADMIN_CURRENTDAY_INVENTORYSECTIONDATAHISTORY_PRODUCTWISE_REPORT)
  public final List<Map<String, Object>> getcurrrentDaySaleproductwise(
      @RequestBody final String payload, final HttpServletRequest request)
      throws ParseException, JSONException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final JSONObject reportDates = new JSONObject(payload);
    final String branchId = reportDates.getString("branchId");
    final List<Map<String, Object>> list = new ArrayList<>();
    final List<Date> dates = this.tokenServiceImpl.getReportDates(reportDates);
    final Map<String, Object> map = new HashMap<>();
    List<Object[]> inventorySectionDataHistory = null;
    inventorySectionDataHistory =
        this.inventorySectionDataHistoryService.getSumOfInventoryHistoryDetailsByOrgBranch(
            dates.get(0), dates.get(1), orgId, Long.parseLong(branchId));
    if (inventorySectionDataHistory.isEmpty()) {
      map.put("InventorySectionDataHistoryReport", "No Report is found");
      list.add(map);
    } else {
      for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
        map.put("productName", inventorySectionDataHistory.get(i)[2].toString());
        map.put("quantity", inventorySectionDataHistory.get(i)[0].toString());
        map.put("totalAmount", inventorySectionDataHistory.get(i)[1].toString());
        list.add(map);
      }
    }
    return list;
  }

  @PostMapping(path = RouteConstants.ADMIN_CURRENTDAY_INVENTORYDATEHISTORY_ITEMWISE)
  public final Map<String, Object> getcurrrentDayItemwiseInventorySectionDataHistory(
      @RequestBody final String reportDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws ParseException, JSONException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final JSONObject reportDates = new JSONObject(reportDetails);
    final Map<String, Object> inventorySectionDataHistoryDetailsMap = new HashMap<>();
    final List<Date> dates = this.tokenServiceImpl.getReportDates(reportDates);
    List<InventorySectionDataHistory> inventorySectionDataHistory = null;
    String branchId = null;

    if (reportDates.has("branchId")) {
      branchId = reportDates.getString("branchId");
      inventorySectionDataHistory =
          this.inventorySectionDataHistoryService
              .getInventorySectionDataHistoryByItemWiseOrgIdBranchIdByDateWise(
                  orgId, Long.parseLong(branchId), dates.get(0), dates.get(1));
    } else if ((employeeDetails.getBranch()) != null) {
      inventorySectionDataHistory =
          this.inventorySectionDataHistoryService
              .getInventorySectionDataHistoryByItemWiseOrgIdBranchIdByDateWise(
                  orgId, employeeDetails.getBranch().getId(), dates.get(0), dates.get(1));
    } else {
      inventorySectionDataHistory =
          this.inventorySectionDataHistoryService
              .getInventorySectionDataHistoryByItemWiseOrgIdByDateWise(
                  orgId, dates.get(0), dates.get(1));
    }
    inventorySectionDataHistoryDetailsMap.put(
        "InventorySectionDetails", inventorySectionDataHistory);
    return inventorySectionDataHistoryDetailsMap;
  }

  @PostMapping(path = RouteConstants.ADMIN_NETSALE_BILLING)
  public final List<Map<String, Object>> getNetsaledetailsbyBilling(
      @RequestBody final String payload, final HttpServletRequest request)
      throws ParseException, JSONException {

    final List<Map<String, Object>> billing = new ArrayList<>();
    final Map<String, Object> map = new HashMap<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long orgId = employeeDetails.getOrganisation().getId();
      final JSONObject jsonObject = new JSONObject(payload);
      final List<Date> billDates = this.tokenServiceImpl.getReportDates(jsonObject);
      final String branchId = jsonObject.getString("branchId");
      final Date fromDate = billDates.get(0);
      final Date toDate = billDates.get(1);

      if (branchId == null) {
        List<Object[]> allNetsale =
            this.billingService.getnetsaleOfBillingDetailsByOrgAndBranchisNull(
                fromDate, toDate, orgId);
        List<Object[]> allTaxesamount =
            this.billingService.getAllTxesOfBillingDetailsByOrgAndBranchisNull(
                fromDate, toDate, orgId);
        List<Object[]> allDiscount =
            this.billingService.getAllDiscountOfBillingDetailsByOrgAndBranchisNull(
                fromDate, toDate, orgId);
        List<Object[]> allGrossSales =
            this.billingService.getAllGrossSalesOfBillingDetailsByOrgAndBranchisNull(
                fromDate, toDate, orgId);
        List<Object[]> allServiceCharges =
            this.billingService.getAllServiceChargesOfBillingDetailsByOrg(fromDate, toDate, orgId);
        List<Object[]> summary =
            this.billingService
                .getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndGroupedByPaymentMode(
                    fromDate, toDate, orgId);
        Map<String, Object> map1 = new HashMap<String, Object>();
        map1.put("name", "NetSale");
        map1.put("count", allNetsale.get(0)[0]);
        map1.put("value", new BigDecimal(allNetsale.get(0)[1].toString()));
        billing.add(map1);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("name", "TaxesAmount");
        map2.put("count", allTaxesamount.get(0)[0]);
        map2.put("value", new BigDecimal(allTaxesamount.get(0)[1].toString()));
        billing.add(map2);

        Map<String, Object> map3 = new HashMap<>();
        map3.put("name", "DiscountAmount");
        map3.put("count", allDiscount.get(0)[0]);
        map3.put("value", new BigDecimal(allTaxesamount.get(0)[1].toString()));
        billing.add(map3);

        Map<String, Object> map6 = new HashMap<>();
        map6.put("name", "ServiceCharge");
        map6.put("count", allServiceCharges.get(0)[0]);
        map6.put("value", new BigDecimal(allServiceCharges.get(0)[1].toString()));
        billing.add(map6);

        Map<String, Object> map4 = new HashMap<>();
        map4.put("name", "GrossAmount");
        map4.put("count", allGrossSales.get(0)[0]);
        map4.put(
            "value",
            new BigDecimal(allGrossSales.get(0)[1].toString()).setScale(0, RoundingMode.HALF_UP));
        billing.add(map4);

        Map<String, Object> map7 = new HashMap<>();
        map7.put("name", "RoundOff");
        map7.put("count", "");
        map7.put(
            "value",
            new BigDecimal(allGrossSales.get(0)[1].toString())
                .setScale(0, RoundingMode.HALF_UP)
                .subtract(new BigDecimal(allGrossSales.get(0)[1].toString())));
        billing.add(map7);

        for (int i = 0; i < summary.size(); i++) {
          Map<String, Object> map5 = new HashMap<>();
          map5.put("name", summary.get(i)[0].toString());
          map5.put("count", summary.get(i)[2].toString());
          map5.put("value", summary.get(i)[1].toString());
          billing.add(map5);
        }
      } else {

        List<Object[]> allNetsale =
            this.billingService.getnetsaleOfBillingDetailsByBranchAndOrg(
                fromDate, toDate, orgId, Long.parseLong(branchId));
        List<Object[]> allTaxesamount =
            this.billingService.getAllTxesOfBillingDetailsByBranchAndOrg(
                fromDate, toDate, orgId, Long.parseLong(branchId));
        List<Object[]> allDiscount =
            this.billingService.getAllDiscountOfBillingDetailsByBranchAndOrg(
                fromDate, toDate, orgId, Long.parseLong(branchId));

        List<Object[]> allGrossSales =
            this.billingService.getAllGrossSalesOfBillingDetailsByBranchAndOrg(
                fromDate, toDate, orgId, Long.parseLong(branchId));
        List<Object[]> allServiceCharges =
            this.billingService.getAllServiceChargesOfBillingDetailsByBranchAndOrg(
                fromDate, toDate, orgId, Long.parseLong(branchId));
        List<Object[]> summary =
            this.billingService
                .getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndBranchIdGroupedByPaymentMode(
                    fromDate, toDate, orgId, Long.parseLong(branchId));

        Map<String, Object> map1 = new HashMap<>();
        map1.put("name", "NetSale");
        map1.put("count", allNetsale.get(0)[0]);
        map1.put("value", new BigDecimal(allNetsale.get(0)[1].toString()));
        billing.add(map1);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("name", "TaxesAmount");
        map2.put("count", allTaxesamount.get(0)[0]);
        map2.put("value", new BigDecimal(allTaxesamount.get(0)[1].toString()));
        billing.add(map2);

        Map<String, Object> map3 = new HashMap<>();
        map3.put("name", "DiscountAmount");
        map3.put("count", allDiscount.get(0)[0]);
        map3.put("value", new BigDecimal(allDiscount.get(0)[1].toString()));
        billing.add(map3);

        Map<String, Object> map6 = new HashMap<>();
        map6.put("name", "serviceCharge");
        map6.put("count", allServiceCharges.get(0)[0]);
        map6.put("value", new BigDecimal(allServiceCharges.get(0)[1].toString()));
        billing.add(map6);

        Map<String, Object> map4 = new HashMap<>();
        map4.put("name", "GrossAmount");
        map4.put("count", allGrossSales.get(0)[0]);
        map4.put(
            "value",
            new BigDecimal(allGrossSales.get(0)[1].toString()).setScale(0, RoundingMode.HALF_UP));
        billing.add(map4);

        Map<String, Object> map7 = new HashMap<>();
        map7.put("name", "RoundOff");
        map7.put("count", "");
        map7.put(
            "value",
            new BigDecimal(allGrossSales.get(0)[1].toString())
                .setScale(0, RoundingMode.HALF_UP)
                .subtract(new BigDecimal(allGrossSales.get(0)[1].toString())));
        billing.add(map7);

        for (int i = 0; i < summary.size(); i++) {
          Map<String, Object> map5 = new HashMap<>();
          map5.put("name", summary.get(i)[0].toString());
          map5.put("count", summary.get(i)[2].toString());
          map5.put("value", summary.get(i)[1].toString());
          billing.add(map5);
        }
      }
    } catch (final NullPointerException ne) {
      map.put("msg", "Something Went Wrong? Please Enter Correct Details..");
      LOGGER.error(
          Constants.MARKER,
          "/admin/currrentDay/sale/productwise/details  " + "Something Went Wrong?");
      return billing;
    }
    return billing;
  }

  @PostMapping(path = RouteConstants.ADMIN_CURRENTDAY_PRODUCTWISE_QUANTITY)
  public final Map<String, Object> getCurrentDayProductsQuantity(
      @RequestBody final String productquantityDetails, final HttpServletRequest request)
      throws ParseException, JSONException {

    final Map<String, Object> productQuantitymap = new HashMap<>();
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final JSONObject reportDates = new JSONObject(productquantityDetails);
    String branchId = null;
    List<CartDetail> productWiseQuantity = null;
    final List<Date> dates = this.tokenServiceImpl.getReportDates(reportDates);
    if (reportDates.has("branchId")) {
      branchId = reportDates.getString("branchId");
      productWiseQuantity =
          this.cartProductService.getproductQuantityByBranchAndOrgIdAndDate(
              orgId, Long.parseLong(branchId), dates.get(0), dates.get(1));
    } else if ((employeeDetails.getBranch()) != null) {
      productWiseQuantity =
          this.cartProductService.getproductQuantityByBranchAndOrgIdAndDate(
              orgId, employeeDetails.getBranch().getId(), dates.get(0), dates.get(1));
    } else {
      productWiseQuantity =
          this.cartProductService.getproductQuantityByOrgIdAndDate(
              orgId, dates.get(0), dates.get(1));
    }
    productQuantitymap.put("productQuantityWiseQuanty", productWiseQuantity);
    return productQuantitymap;
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @PostMapping(path = RouteConstants.CATEGORYWISE_QUANTITY_REPORT)
  public final List<Map<String, Object>> getBillAmount(
      @RequestBody String payload, HttpServletRequest request)
      throws ParseException, JSONException {

    final List<Map<String, Object>> listOfcategoryBills = new ArrayList<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long orgId = employeeDetails.getOrganisation().getId();
      final JSONObject jsonObject = new JSONObject(payload);
      final List<Date> billDates = this.tokenServiceImpl.getReportDates(jsonObject);
      final String branchId = jsonObject.getString("branchId");
      final Date fromDate = billDates.get(0);
      final Date toDate = billDates.get(1);
      List<Billing> billing = null;
      long cartId = 0;

      if (branchId == null) {
        billing = this.billingService.getBillingsByCurrentDateAndOrgId(fromDate, toDate, orgId);
      } else {
        billing =
            this.billingService.getBillingsByStartAndEndDateAndOrgAndBranch(
                fromDate, toDate, orgId, Long.parseLong(branchId));
      }
      for (Billing bill : billing) {
        cartId = bill.getCartId().getCartId();
        List<CartDetail> cartProduct = this.cartProductService.getCartDetailsByCartId(cartId);
        for (CartDetail cartDetails : cartProduct) {
          Map<String, Object> CategoryBill = new HashMap<String, Object>();
          String productName = cartDetails.getProductName();
          Product product = null;
          if (branchId == null) {
            product = this.productsService.getProductsByProductNameAndOrgId(productName, orgId);
          } else {
            product =
                this.productsService.getProductsByProductNameAndOrgIdAndBranchId(
                    productName, orgId, Long.parseLong(branchId));
          }
          String productCategoryName = product.getProductCategory().getProductTypeName();
          int j = 0;
          for (int i = 0; i < listOfcategoryBills.size(); i++) {
            Map m = listOfcategoryBills.get(i);
            if (m.get("category").equals(productCategoryName)) {
              j = 1;
              final long sumqty = (long) m.get("quantity") + cartDetails.getQuantity();
              final BigDecimal price =
                  ((BigDecimal) m.get("price")).add((BigDecimal) cartDetails.getTotalAmount());
              m.put("category", productCategoryName);
              m.put("quantity", sumqty);
              m.put("price", price);
              listOfcategoryBills.set(i, m);
            }
          }
          if (j == 0) {
            CategoryBill.put("category", productCategoryName);
            CategoryBill.put("quantity", cartDetails.getQuantity());
            CategoryBill.put("price", cartDetails.getTotalAmount());
            listOfcategoryBills.add(CategoryBill);
          }
        }
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getBillingDetails " + ex);
    }
    return listOfcategoryBills;
  }

  @GetMapping(path = RouteConstants.INVOICE_PURCHASE_REPORTS)
  public final ResponseEntity<Object> getInvoicePurchaseDetails(final HttpServletRequest request)
      throws ParseException {

    final List<Map<String, Object>> billdetails = new ArrayList<>();
    try {

      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final Date fromDate = sdf.parse(request.getParameter("fromDate"));
        final Date toDate = sdf.parse(request.getParameter("toDate"));
        final String formType = request.getParameter("fromType");
        final long fromSectionId = Long.parseLong(request.getParameter("fromSectionId"));
        List<Object[]> invoicebill = null;
        List<Object[]> invoiceDetails = null;

        if (formType.equals("section")) {
          invoiceDetails =
              inventorySectionDataHistoryService.getInventorySectionDataHistoryByTotalAmt(
                  fromDate, toDate, fromSectionId, orgId, branchId);
          for (int i = 0; i < invoiceDetails.size(); i++) {
            Map<String, Object> invoiceMap = new HashMap<>();
            invoiceMap.put("totalAmount", invoiceDetails.get(i)[0]);
            invoiceMap.put("fromVendorName", invoiceDetails.get(i)[1]);
            invoiceMap.put("refernceNumber", invoiceDetails.get(i)[2]);
            invoiceMap.put("fromBranchName", invoiceDetails.get(i)[3]);
            invoiceMap.put("fromInvenorySectionName", invoiceDetails.get(i)[4]);
            invoiceMap.put("date", sdf.format(invoiceDetails.get(i)[5]));
            invoiceMap.put("organisationName", invoiceDetails.get(i)[6]);
            invoiceMap.put(
                "purchasedBy", invoiceDetails.get(i)[3] + "/" + invoiceDetails.get(i)[4]);
            invoiceMap.put("purchasedFrom", invoiceDetails.get(i)[1]);
            billdetails.add(invoiceMap);
          }
        } else {
          if (formType.equals("branch")) {
            invoicebill =
                invoiceBillService.getInvoiceBillDetailsByReport(fromDate, toDate, orgId, branchId);

            for (int i = 0; i < invoicebill.size(); i++) {
              Map<String, Object> invoiceMap = new HashMap<String, Object>();
              invoiceMap.put("totalAmount", invoicebill.get(i)[0]);
              invoiceMap.put("refernceNumber", invoicebill.get(i)[1]);
              invoiceMap.put("date", invoicebill.get(i)[2]);
              invoiceMap.put("fromVendorName", invoicebill.get(i)[6]);
              invoiceMap.put("fromBranchName", invoicebill.get(i)[3]);
              invoiceMap.put("organisationName", invoicebill.get(i)[5]);
              invoiceMap.put("purchasedBy", invoicebill.get(i)[3]);
              invoiceMap.put("purchasedFrom", invoicebill.get(i)[6]);
              billdetails.add(invoiceMap);
            }
          }
        }
      } else {
        return new ResponseEntity<>(billdetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInvoicebillpurchaseReports" + ex);
    }
    return new ResponseEntity<>(billdetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVOICEBILL_REPORT_VENDORNAME)
  public final ResponseEntity<Object> getInvoiceBillDetails(final HttpServletRequest request)
      throws ParseException {

    final Map<String, Object> map = new HashMap<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final Date fromDate = sdf.parse(request.getParameter("fromDate"));
        final Date toDate = sdf.parse(request.getParameter("toDate"));
        final long fromId = Long.parseLong(request.getParameter("fromId"));
        final String fromType = request.getParameter("fromType");
        final long fromSectionId = Long.parseLong(request.getParameter("fromSectionId"));
        List<InventorySectionDataHistory> inventorySectionDataHistory = null;
        List<InvoiceBill> invoiceDetails = null;

        if (fromType.equals("branch")) {
          invoiceDetails =
              invoiceBillService.getAllInvoiceDetailsVendorNameTotalAmount(
                  orgId, fromDate, toDate, fromId);
          map.put("invoiceDetails", invoiceDetails);
        } else {
          inventorySectionDataHistory =
              inventorySectionDataHistoryService.getInventorySectionDataHistoryByBranchId(
                  orgId, fromId, fromDate, toDate, fromSectionId);
          map.put("invoiceDetails", inventorySectionDataHistory);
        }
      } else {
        return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInvoiceBillDetails" + ex);
    }
    return new ResponseEntity<>(map, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVOICEBILL_REPORT)
  public final ResponseEntity<Object> getInvoiceBillDetailsVendorId(
      final HttpServletRequest request) throws ParseException {

    final Map<String, Object> map = new HashMap<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final Date fromDate = sdf.parse(request.getParameter("fromDate"));
        final Date toDate = sdf.parse(request.getParameter("toDate"));
        final long fromId = Long.parseLong(request.getParameter("fromId"));
        final long vendorId = Long.parseLong(request.getParameter("vendorId"));
        final String fromType = request.getParameter("fromType");
        final long fromSectionId = Long.parseLong(request.getParameter("fromSectionId"));
        List<InventorySectionDataHistory> inventorySectionDataHistory = null;
        List<InvoiceBill> invoiceDetails = null;

        if (fromType.equals("branch")) {
          invoiceDetails =
              invoiceBillService.getAllInvoiceDetailsByDatesVendor(
                  orgId, fromDate, toDate, fromId, vendorId);
          map.put("invoiceDetails", invoiceDetails);
        } else {
          inventorySectionDataHistory =
              inventorySectionDataHistoryService.getInventorySectionDataHistoryByVendorIdBranchId(
                  orgId, fromId, fromDate, toDate, fromSectionId, vendorId);
          map.put("invoiceDetails", inventorySectionDataHistory);
        }
      } else {
        return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInvoiceBillDetails" + ex);
    }
    return new ResponseEntity<>(map, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ISSUE_BILL_REPORTS)
  public final ResponseEntity<List<Map<String, Object>>> getBillWiseIssue(
      final HttpServletRequest request) throws ParseException {

    final List<Map<String, Object>> billdetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final Date fromDate = sdf.parse(request.getParameter("fromDate"));
        final Date toDate = sdf.parse(request.getParameter("toDate"));
        final long frombranchId = Long.parseLong(request.getParameter("fromId"));
        final long tobranchId = Long.parseLong(request.getParameter("toId"));
        final long toSectionId = Long.parseLong(request.getParameter("toSectionId"));
        final long fromSectionId = Long.parseLong(request.getParameter("fromSectionId"));
        final String toTypesection = request.getParameter("toType");
        final String fromTypesection = request.getParameter("fromType");
        final String referenceNumber = request.getParameter("Reference");
        List<Object[]> inventorySectionDataHistory = null;
        List<InvoiceBill> billissuedata = null;

        final Map<String, Object> billMap = new HashMap<>();

        if (toTypesection.equals("section") && fromTypesection.equals("section")) {
          if (referenceNumber.equals("")) {
            inventorySectionDataHistory =
                inventorySectionDataHistoryService.getInventorySectionDataHistoryTotalAmt(
                    fromDate, toDate, branchId, orgId, fromSectionId, toSectionId);
            for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
              billMap.put("TotalAmount", inventorySectionDataHistory.get(i)[0]);
              billMap.put("referenceNumber", inventorySectionDataHistory.get(i)[1]);
              billMap.put("date", sdf.format(inventorySectionDataHistory.get(i)[2]));
              billMap.put("createdDate", sdf.format(inventorySectionDataHistory.get(i)[3]));
              billMap.put("modifiedDate", sdf.format(inventorySectionDataHistory.get(i)[4]));
              billMap.put("organisationName", inventorySectionDataHistory.get(i)[5]);
              billMap.put("branchName", inventorySectionDataHistory.get(i)[6]);
              billMap.put("tosectionName", inventorySectionDataHistory.get(i)[7]);
              billMap.put("FromsectionName", inventorySectionDataHistory.get(i)[8]);
              billdetails.add(billMap);
            }
          } else {
            inventorySectionDataHistory =
                inventorySectionDataHistoryService.getInventorySectionDataHistoryByBillIdAndDates(
                    fromDate, toDate, referenceNumber, branchId, orgId, fromSectionId, toSectionId);
            for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
              billMap.put("TotalAmount", inventorySectionDataHistory.get(i)[0]);
              billMap.put("referenceNumber", inventorySectionDataHistory.get(i)[1]);
              billMap.put("date", sdf.format(inventorySectionDataHistory.get(i)[2]));
              billMap.put("createdDate", sdf.format(inventorySectionDataHistory.get(i)[3]));
              billMap.put("modifiedDate", sdf.format(inventorySectionDataHistory.get(i)[4]));
              billMap.put("organisationName", inventorySectionDataHistory.get(i)[5]);
              billMap.put("branchName", inventorySectionDataHistory.get(i)[6]);
              billMap.put("tosectionName", inventorySectionDataHistory.get(i)[7]);
              billMap.put("FromsectionName", inventorySectionDataHistory.get(i)[8]);
              billdetails.add(billMap);
            }
          }
        } else if (toTypesection.equals("section") && fromTypesection.equals("branch")) {
          if (referenceNumber.equals("")) {
            inventorySectionDataHistory =
                inventorySectionDataHistoryService.getInventorySectionDataHistoryTotalAmtToSection(
                    fromDate, toDate, branchId, orgId, toSectionId, frombranchId);
          } else {
            inventorySectionDataHistory =
                inventorySectionDataHistoryService
                    .getInventorySectionDataHistoryByBillIdAndDatesToSectionReference(
                        fromDate,
                        toDate,
                        referenceNumber,
                        branchId,
                        orgId,
                        frombranchId,
                        toSectionId);
          }
          for (int i = 0; i < inventorySectionDataHistory.size(); i++) {
            billMap.put("TotalAmount", inventorySectionDataHistory.get(i)[0]);
            billMap.put("referenceNumber", inventorySectionDataHistory.get(i)[1]);
            billMap.put("date", sdf.format(inventorySectionDataHistory.get(i)[2]));
            billMap.put("createdDate", sdf.format(inventorySectionDataHistory.get(i)[3]));
            billMap.put("modifiedDate", sdf.format(inventorySectionDataHistory.get(i)[4]));
            billMap.put("organisationName", inventorySectionDataHistory.get(i)[5]);
            billMap.put("branchName", inventorySectionDataHistory.get(i)[6]);
            billMap.put("tosectionName", inventorySectionDataHistory.get(i)[7]);
            billMap.put("FrombranchName", inventorySectionDataHistory.get(i)[8]);
            billdetails.add(billMap);
          }
        } else if (toTypesection.equals("branch") && fromTypesection.equals("section")) {
          if (referenceNumber.equals("")) {
            billissuedata =
                invoiceBillService.getInVoiceBillDetailsByBillIdAndDatesToBranch(
                    fromDate, toDate, tobranchId, orgId, fromSectionId);
          } else {
            billissuedata =
                invoiceBillService.getInVoiceBillDetailsByBillIdAndDatesToBranchReference(
                    fromDate, toDate, referenceNumber, tobranchId, orgId, fromSectionId);
          }
          for (InvoiceBill invoiceBill : billissuedata) {
            billMap.put("TotalAmount", invoiceBill.getTotalAmount());
            billMap.put("referenceNumber", invoiceBill.getInvoiceNumber());
            billMap.put("date", sdf.format(invoiceBill.getDate()));
            billMap.put("createdDate", sdf.format(invoiceBill.getCreatedDate()));
            billMap.put("modifiedDate", sdf.format(invoiceBill.getModifiedDate()));
            billMap.put("organisationId", invoiceBill.getOrganisation().getId());
            billMap.put("organisationName", invoiceBill.getOrganisation().getOrgName());
            billMap.put("branchId", invoiceBill.getBranch().getId());
            billMap.put("branchName", invoiceBill.getBranch().getBranchName());
            billMap.put("FromsectionName", invoiceBill.getFromSectionId().getSectionName());
            billdetails.add(billMap);
          }
        } else if (toTypesection.equals("branch") && fromTypesection.equals("branch")) {
          if (referenceNumber.equals("")) {
            billissuedata =
                invoiceBillService.getInvoiceBillsTotalAmt(
                    fromDate, toDate, tobranchId, orgId, frombranchId);
          } else {
            billissuedata =
                invoiceBillService.getInVoiceBillDetailsByBillIdAndDates(
                    fromDate, toDate, referenceNumber, tobranchId, orgId, frombranchId);
          }
          for (InvoiceBill invoiceBill : billissuedata) {
            billMap.put("TotalAmount", invoiceBill.getTotalAmount());
            billMap.put("referenceNumber", invoiceBill.getInvoiceNumber());
            billMap.put("date", sdf.format(invoiceBill.getDate()));
            billMap.put("createdDate", sdf.format(invoiceBill.getCreatedDate()));
            billMap.put("modifiedDate", sdf.format(invoiceBill.getModifiedDate()));
            billMap.put("organisationId", invoiceBill.getOrganisation().getId());
            billMap.put("organisationName", invoiceBill.getOrganisation().getOrgName());
            billMap.put("branchId", invoiceBill.getBranch().getId());
            billMap.put("branchName", invoiceBill.getBranch().getBranchName());
            billdetails.add(billMap);
          }
        }
      } else {
        return new ResponseEntity<>(billdetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (Exception e) {
      LOGGER.error("getissue bill Reports  " + "select required branch or section");
      return new ResponseEntity<>(billdetails, HttpStatus.OK);
    }
    return new ResponseEntity<>(billdetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.STOCK_ITEM_REPORTS)
  public final List<Map<String, Object>> getStockItemReports(final HttpServletRequest request) {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = Long.parseLong(request.getParameter("fromId"));
    final String fromType = request.getParameter("fromType");
    final long fromSectionId = Long.parseLong(request.getParameter("fromSectionId"));
    final List<Map<String, Object>> itemReports = new ArrayList<>();

    if (fromType.equals("section")) {
      List<InventorySectionDataHistory> inventorySectionDataHistory =
          inventorySectionDataHistoryService.getInventorySectionDataHistoryBySection(
              orgId, branchId, fromSectionId);
      for (InventorySectionDataHistory itemReportData : inventorySectionDataHistory) {
        Map<String, Object> stockReports = new HashMap<>();
        stockReports.put("Name", itemReportData.getProductId().getProductName());
        stockReports.put("Amount", itemReportData.getAmount());
        stockReports.put("branchName", itemReportData.getBranch().getBranchName());
        stockReports.put("Date", itemReportData.getDate());
        stockReports.put("sectionName", itemReportData.getInventorySectionId().getSectionName());
        stockReports.put("createdDate", itemReportData.getCreatedDate());
        stockReports.put("modifiedDate", itemReportData.getModifiedDate());
        stockReports.put("Quantity", itemReportData.getQuantity());
        stockReports.put("referenceNumber", itemReportData.getReferenceNumber());
        itemReports.add(stockReports);
      }
    }

    return itemReports;
  }

  @GetMapping(path = RouteConstants.INVOICE_VENDOR_REPORTS)
  public final List<Map<String, Object>> getInvoiceVendors(final HttpServletRequest request)
      throws ParseException {
    final long branchId = Long.parseLong(request.getParameter("branchId"));
    final String referenceNumber = request.getParameter("Reference");
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date fromDate = sdf.parse(request.getParameter("fromDate"));
    final Date toDate = sdf.parse(request.getParameter("toDate"));
    final List<Map<String, Object>> billVendordetails = new ArrayList<>();
    List<Object[]> billvendorsdata = null;
    if (referenceNumber.equals("")) {
      billvendorsdata = invoiceBillService.getAllInvoiceVendorDetails(fromDate, toDate, branchId);
    } else {
      billvendorsdata =
          invoiceBillService.getInvoiceVendorDetails(referenceNumber, fromDate, toDate, branchId);
    }
    for (int i = 0; i < billvendorsdata.size(); i++) {
      Map<String, Object> billMap = new HashMap<>();
      billMap.put("vendorsCount", billvendorsdata.get(i)[0]);
      billMap.put("vendorId", billvendorsdata.get(i)[1]);
      billMap.put("vendorName", billvendorsdata.get(i)[2]);
      billMap.put("referenceNumber", billvendorsdata.get(i)[3]);
      billMap.put("branchId", billvendorsdata.get(i)[4]);
      billMap.put("branchName", billvendorsdata.get(i)[5]);
      billVendordetails.add(billMap);
    }
    return billVendordetails;
  }

  @PostMapping(path = RouteConstants.ADMIN_DATES_SALES_REPORTS)
  public final List<Map<String, Object>> getSaleReports(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException, ParseException {

    final List<Map<String, Object>> list = new ArrayList<>();
    try {
      final long orgId = jwtTokenUtil.getOrgIdFromToken(request);
      final JSONObject jsonObject = new JSONObject(payload);
      final long branchId = jsonObject.getLong("branchId");
      final List<Date> billDates = this.tokenServiceImpl.getReportDates(jsonObject);
      final Date fromDate = billDates.get(0);
      final Date toDate = billDates.get(1);
      List<Object[]> saleReports = null;

      saleReports =
          billingService.getSaleReportBetweenTwoDatesByOrgIdAndBranchIdAndDates(
              fromDate, toDate, orgId, branchId);

      for (int i = 0; i < saleReports.size(); i++) {
        Map<String, Object> map = new HashMap<>();

        map.put("Total", saleReports.get(i)[0]);
        map.put("Gst", saleReports.get(i)[1]);
        map.put("Discount", saleReports.get(i)[2]);
        map.put("Service", saleReports.get(i)[3]);
        map.put("NetAmount", saleReports.get(i)[4]);
        map.put("Date", saleReports.get(i)[5]);
        map.put("NoOfSales", saleReports.get(i)[6]);
        map.put("Guests", saleReports.get(i)[7]);
        list.add(map);
      }

    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "/admin/dates/sales/report " + e);
    }
    return list;
  }

  @GetMapping(path = RouteConstants.TRANSFER_ITEMWISE_REPORTS)
  public final ResponseEntity<List<Map<String, Object>>> getItemTransferOutReport(
      final HttpServletRequest request) throws ParseException {

    final List<Map<String, Object>> transferQtyReport = new ArrayList<>();
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      final Date fromDate = sdf.parse(request.getParameter("fromDate"));
      final Date toDate = sdf.parse(request.getParameter("toDate"));
      final long frombranchId = Long.parseLong(request.getParameter("fromId"));
      final String fromType = request.getParameter("fromType");
      final long fromSectionId = Long.parseLong(request.getParameter("fromSectionId"));
      final long tobranchId = Long.parseLong(request.getParameter("toId"));
      final long toSectionId = Long.parseLong(request.getParameter("toSectionId"));
      final String toType = request.getParameter("toType");

      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();

        if (toType.equals("section") && fromType.equals("section")) {
          List<Object[]> inventorydata =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryQuantityByProductAndFromToSection(
                      orgId, tobranchId, fromDate, toDate, fromSectionId, toSectionId);
          for (int i = 0; i < inventorydata.size(); i++) {
            Map<String, Object> inventorydataMap = new HashMap<>();
            inventorydataMap.put("Totalquantity", inventorydata.get(i)[0]);
            inventorydataMap.put("productName", inventorydata.get(i)[1]);
            inventorydataMap.put("date", sdf.format(inventorydata.get(i)[2]));
            inventorydataMap.put("fromsectionName", inventorydata.get(i)[3]);
            inventorydataMap.put("toBranchName", inventorydata.get(i)[4]);
            inventorydataMap.put("tosectionName", inventorydata.get(i)[5]);
            transferQtyReport.add(inventorydataMap);
          }
        } else if (toType.equals("section") && fromType.equals("branch")) {
          List<Object[]> inventorydata =
              inventorySectionDataHistoryService
                  .getInventorySectionDataHistoryQuantityByProductAndFromBranchToSection(
                      orgId, tobranchId, fromDate, toDate, frombranchId, toSectionId);
          for (int i = 0; i < inventorydata.size(); i++) {
            Map<String, Object> inventorydataMap = new HashMap<>();
            inventorydataMap.put("Totalquantity", inventorydata.get(i)[0]);
            inventorydataMap.put("productName", inventorydata.get(i)[1]);
            inventorydataMap.put("date", sdf.format(inventorydata.get(i)[2]));
            inventorydataMap.put("fromBranchName", inventorydata.get(i)[3]);
            inventorydataMap.put("toBranchName", inventorydata.get(i)[4]);
            inventorydataMap.put("tosectionName", inventorydata.get(i)[5]);
            transferQtyReport.add(inventorydataMap);
          }
        } else if (toType.equals("branch") && fromType.equals("branch")) {
          List<Object[]> invoicedatahistory =
              invoiceBillService.getAllInvoiceBillProductQuantityByFromToBranch(
                  orgId, tobranchId, fromDate, toDate, frombranchId);
          for (int i = 0; i < invoicedatahistory.size(); i++) {
            Map<String, Object> inventorydataMap = new HashMap<>();
            Branch fromBranch =
                branchService.getBranchById(
                    Long.parseLong(invoicedatahistory.get(i)[3].toString()));
            Branch toBranch =
                branchService.getBranchById(
                    Long.parseLong(invoicedatahistory.get(i)[4].toString()));
            Product product =
                productsService.getProductsById(
                    Long.parseLong(invoicedatahistory.get(i)[1].toString()));
            inventorydataMap.put("Totalquantity", invoicedatahistory.get(i)[0]);
            inventorydataMap.put("productName", product.getProductName());
            inventorydataMap.put("date", sdf.format(invoicedatahistory.get(i)[2]));
            inventorydataMap.put("fromBranchName", fromBranch.getBranchName());
            inventorydataMap.put("toBranchName", toBranch.getBranchName());
            transferQtyReport.add(inventorydataMap);
          }
        } else if (toType.equals("branch") && fromType.equals("section")) {
          List<Object[]> invoicedatahistory =
              invoiceBillService.getAllInvoiceBillProductQuantityByFromSectionToBranch(
                  orgId, tobranchId, fromDate, toDate, fromSectionId);
          for (int i = 0; i < invoicedatahistory.size(); i++) {
            Map<String, Object> inventorydataMap = new HashMap<>();
            Branch branch =
                branchService.getBranchById(
                    Long.parseLong(invoicedatahistory.get(i)[4].toString()));
            InventorySection inventorySection =
                inventorySectionService.getInventorySectionById(
                    Long.parseLong(invoicedatahistory.get(i)[3].toString()));
            inventorydataMap.put("Totalquantity", invoicedatahistory.get(i)[0]);
            inventorydataMap.put("productName", invoicedatahistory.get(i)[1]);
            inventorydataMap.put("date", sdf.format(invoicedatahistory.get(i)[2]));
            inventorydataMap.put("fromSectionName", inventorySection.getSectionName());
            inventorydataMap.put("toBranchName", branch.getBranchName());
            transferQtyReport.add(inventorydataMap);
          }
        }
      } else {

        return new ResponseEntity<>(transferQtyReport, HttpStatus.UNAUTHORIZED);
      }
    } catch (Exception e) {
      LOGGER.error("get item transfer out report  " + "select required branch or section");
      return new ResponseEntity<>(transferQtyReport, HttpStatus.OK);
    }
    return new ResponseEntity<>(transferQtyReport, HttpStatus.OK);
  }
}
