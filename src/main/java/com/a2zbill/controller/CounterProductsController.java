package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.CustomerVendorToken;
import com.a2zbill.domain.EmployeeCounter;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.InvoiceDetails;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import com.a2zbill.services.CartService;
import com.a2zbill.services.CounterProductsService;
import com.a2zbill.services.CustomerVendorTokenService;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.InvoiceBillService;
import com.a2zbill.services.InvoiceDetailsService;
import com.a2zbill.services.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Value;
import com.tsss.basic.config.KafkaSender;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Vendor;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.VendorService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CounterProductsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(CounterProductsController.class);

  @Value("${customerportalurl}")
  private String url;

  @Autowired private CounterProductsService employeeCounterService;

  @Autowired private CartService cartService;

  @Autowired private InvoiceDetailsService invoiceDetailsService;

  @Autowired private ProductService productService;

  @Autowired private VendorService vendorService;

  @Autowired private CustomerVendorTokenService customerVendorTokenService;

  @Autowired private KafkaSender kafkaSender;

  @Autowired private InventoryDetailService inventoryDetailService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private InvoiceBillService invoiceBillService;

  @GetMapping(path = RouteConstants.EMPLOYEE_COUNTER_DETAILS_VIEW)
  public final List<Long> getEmployeeCounters(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<Long> counters = new ArrayList<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);

      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      counters =
          this.employeeCounterService.getEmployeeCounterIds(employeeDetails.getEmailAddress());
    } catch (final Exception ex) {
      LOGGER.error("empCounterDetails" + ex);
    }
    return counters;
  }

  @GetMapping(path = RouteConstants.EMPLOYEE_COUNTER_PRODUCT_DETAILS)
  public final List<CartDetail> getCounterProductDetails(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      cartDetails =
          this.employeeCounterService.getCartProductDetails(employeeDetails.getEmailAddress());
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "empCounterProductDetails" + ex);
    }
    return cartDetails;
  }

  @GetMapping(path = RouteConstants.RESTURANT_COUNTER_LIST_VIEW)
  public final List<Long> getRemainCounters(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<Long> counterids = new ArrayList<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      counterids = this.employeeCounterService.getRemainCounter(employeeNumber);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "resturentCounterList" + ex);
    }
    return counterids;
  }

  @GetMapping(path = RouteConstants.RESTURANT_COUNTERBILL_LIST_VIEW)
  public final List<Map<String, String>> getCounterTotalBillAmount(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<Map<String, String>> counterData = new ArrayList<>();
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    long cartId = 0;
    final List<Long> counters =
        this.employeeCounterService.getEmployeeCounters(employeeDetails.getEmailAddress());
    for (final long counter : counters) {
      final Map<String, String> counterBillReport = new HashMap<>();
      final String counterNumber = String.valueOf(counter);
      try {
        final Cart cartDetails = this.cartService.getCartDetailsOnStatus("Open", counter);
        cartId = cartDetails.getCartId();
        counterBillReport.put("counterNumber", counterNumber);
        final List<BigDecimal> counterBillAmount =
            this.employeeCounterService.getCounterTotalBillAmount(cartId);
        for (final BigDecimal bill : counterBillAmount) {
          counterBillReport.put("amount", bill.toString());
          counterData.add(counterBillReport);
        }
      } catch (final Exception ex) {
        counterBillReport.put("counterNumber", counterNumber);
        counterBillReport.put("amount", "vacant");
        counterData.add(counterBillReport);
      }
    }
    return counterData;
  }

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.RESTURANT_COUNTERS_UPDATE)
  public final void updateEmployeeCounters(
      @RequestBody final String counters,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    long employeeId = 0;
    int deleteStatus = 0;
    employeeId = employeeDetails.getId();
    deleteStatus = this.employeeCounterService.deleteEmployeeCounters(employeeId);
    final JSONArray ja = new JSONArray(counters);
    for (int i = 0; i < ja.length(); i++) {
      final EmployeeCounter employeeCounter = new EmployeeCounter();
      final Object number = ja.get(i);
      final Integer counterNumber = (Integer) number;
      employeeCounter.setCounter(counterNumber);
      employeeCounter.setDate(new Date());
      employeeCounter.setEmployee(employeeDetails);
      final Branch branch = employeeDetails.getBranch();
      if (branch != null) {
        employeeCounter.setBranchId(branch);
      } else {
        employeeCounter.setBranchId(null);
      }
      employeeCounter.setOrgId(employeeDetails.getOrganisation());
      this.employeeCounterService.save(employeeCounter);
    }
  }

  @PostMapping(path = RouteConstants.SAVE_VENDOR_PRODUCTS)
  public final void saveVendorproducts(
      @RequestBody final String vendorProductsDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {

    final JSONObject product = new JSONObject(vendorProductsDetails);
    final String productCode = product.getString("productcode");
    final long vendorId = Long.parseLong(product.getString("vendorid"));
    final Product products = this.productService.getProductsByProductCode(productCode);
    final ProductVendor productVendor = new ProductVendor();
    final Vendor vendor = this.vendorService.getVendorDetailsById(vendorId);
    productVendor.setProduct(products);
    productVendor.setVendor(vendor);
    productVendor.setExpectedDuration(1);
    this.employeeCounterService.saveVendorProducts(productVendor);
  }

  @PostMapping(path = RouteConstants.REORDER_PRODUCT_DETAILS)
  public final List<Map<String, Object>> saveReOrders(
      @RequestBody final String ReOrderDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException, JsonProcessingException {
    final Set<Long> vendors = new HashSet<>();
    final ObjectMapper objMapper = new ObjectMapper();
    final List<Map<String, Object>> data = new ArrayList<>();
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final JSONArray reOrderProduct = new JSONArray(ReOrderDetails);
    for (int i = 0; i < reOrderProduct.length(); i++) {
      final JSONObject jsonObject = ((JSONObject) reOrderProduct.get(i));
      Long vendorId = 0L;
      try {
        final JSONArray vendorName = jsonObject.getJSONArray("selectedVendor");
        vendorId = vendorName.getLong(0);
      } catch (JSONException jsone) {
        final String vendorName = jsonObject.getString("selectedVendor");
        Vendor vendor = vendorService.getVendorDetailsByVendorName(vendorName);
        vendorId = vendor.getId();
      }
      vendors.add(vendorId);
    }
    final Iterator<Long> itr = vendors.iterator();
    while (itr.hasNext()) {
      final InvoiceDetails invoicedetail = new InvoiceDetails();
      invoicedetail.setBranch(employeeDetails.getBranch());
      invoicedetail.setOrganisation(employeeDetails.getOrganisation());
      final Vendor vendor = this.vendorService.getVendorDetailsById(itr.next());
      invoicedetail.setStartDate(new Date());
      invoicedetail.setEndDate(new Date());
      invoicedetail.setVendor(vendor);
      invoicedetail.setStatus("false");
      this.invoiceDetailsService.save(invoicedetail);
      final Long invoiceId = invoicedetail.getId();
      for (int i = 0; i < reOrderProduct.length(); i++) {
        Long vendorId = 0L;
        final JSONObject jsonObject = ((JSONObject) reOrderProduct.get(i));
        final Long productId = Long.parseLong(jsonObject.getString("ProductId"));
        try {
          final JSONArray vendorName = jsonObject.getJSONArray("selectedVendor");
          vendorId = vendorName.getLong(0);
        } catch (JSONException e) {
          final String vendorName = jsonObject.getString("selectedVendor");
          Vendor vendorDetails = vendorService.getVendorDetailsByVendorName(vendorName);
          vendorId = vendorDetails.getId();
        }

        if (vendorId == vendor.getId()) {
          final double quantity = Double.parseDouble(jsonObject.getString("Quantity"));
          if (quantity < 0) {
            Map<String, Object> errorMap = new HashMap<String, Object>();
            errorMap.put("errorMessage", "Invalid quantity please enter valid reorderdetails");
            data.add(errorMap);
            return data;
          }
          final Product product = this.productService.getProductsById(productId);
          final ReOrder reorder = new ReOrder();
          reorder.setProduct(product);
          reorder.setVendor(vendor);
          reorder.setOrderDate(new Date());
          reorder.setQuantity(quantity);
          reorder.setStatus("false");
          reorder.setOrganisation(employeeDetails.getOrganisation());
          reorder.setInvoiceDetails(invoiceDetailsService.getInvoiceDetailsByInvoiceId(invoiceId));
          final Branch branch = employeeDetails.getBranch();
          if (branch != null) {
            reorder.setBranch(branch);
          } else {
            reorder.setBranch(null);
            reorder.setInvoiceDetails(
                invoiceDetailsService.getInvoiceDetailsByInvoiceId(invoiceId));
          }
          this.employeeCounterService.saveReOrder(reorder);
          Map<String, Object> successMap = new HashMap<>();
          successMap.put("successMessage", "Reorderrd successfully");
        }
      }
      final Map<String, Object> map = new HashMap<>();
      final Random random = new Random();
      final int randomNum = random.nextInt(9000000);
      final long randomNumber = randomNum + 1000000;
      final String token = "v" + randomNumber;
      final CustomerVendorToken customerVendorToken = new CustomerVendorToken();
      final Branch branch = employeeDetails.getBranch();
      if (branch != null) {
        customerVendorToken.setBranch(branch);
      } else {
        customerVendorToken.setBranch(null);
        customerVendorToken.setOrganisation(employeeDetails.getOrganisation());
        customerVendorToken.setMobileNumber(vendor.getMobileNumber());
        customerVendorToken.setRootName("Invoice");
        customerVendorToken.setToken(token);
      }
      url = "https://a2zbill.tsssinfotech.com/";
      this.customerVendorTokenService.save(customerVendorToken);
      map.put("url", this.url + "v/t/" + token + "/" + invoiceId);
      map.put("invoiceId", invoiceId);
      map.put("vendorMailAddress", vendor.getEmailAddress());
      map.put("orgId", vendor.getOrganisation().getId());
      map.put("branchId", vendor.getBranch().getId());
      map.put("mobileNumber", vendor.getMobileNumber());
      map.put("vendorId", vendor.getId());
      map.put("template", "invoice");
      data.add(map);
      final String kafkadata = objMapper.writeValueAsString(map);
      this.kafkaSender.sendMail(kafkadata);
    }
    return data;
  }

  @PostMapping(path = RouteConstants.VENDOR_PRODUCTS_UPDATE)
  public final void updateVendorproducts(
      @RequestBody final String vendorProductsDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {
    final JSONObject product = new JSONObject(vendorProductsDetails);
    final String productCode = product.getString("productcode");
    final long vendorId = Long.parseLong(product.getString("vendorid"));
    final int expectedDate = Integer.parseInt(product.getString("expectedproductelivery"));
    final Product products = this.productService.getProductsByProductCode(productCode);
    final Vendor vendor = this.vendorService.getVendorDetailsById(vendorId);
    final ProductVendor productVendor = new ProductVendor();
    productVendor.setProduct(products);
    productVendor.setVendor(vendor);
    productVendor.setExpectedDuration(expectedDate);
    this.employeeCounterService.updateVendorProducts(productVendor);
  }

  @PostMapping(path = RouteConstants.REORDERPRODUCT_DETAILS_UPDATE)
  public final Map<String, Object> updateReOrder(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {
    final JSONObject product = new JSONObject(payload);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();
    BigDecimal quantity = null;
    if (product.has("quantity")) {
      final double quantityDouble = product.getDouble("quantity");
      quantity = BigDecimal.valueOf(quantityDouble);
    }
    final long reorderId = Long.parseLong(product.getString("reorderId"));
    final ReOrder reorder = employeeCounterService.getReorderDetailsByReorderId(reorderId);
    final Map<String, Object> reorderUpdate = new HashMap<>();
    if (reorder != null) {
      final long productId = reorder.getProduct().getId();

      final InventoryDetail inventoryDetail =
          this.inventoryDetailService.getInventoryDetailsByProductIdAndBranchIdAndOrgId(
              productId, branchId, orgId);
      final BigDecimal previousQuantity = inventoryDetail.getQuantity();
      quantity = previousQuantity.add(quantity);
      try {
        final int updateStatus =
            this.employeeCounterService.updateReOrderByProductIdAndReorderId(productId, reorderId);
        if (updateStatus > 0) {
          this.inventoryDetailService.updateInventoryQuantity(productId, quantity);
          reorderUpdate.put("Message", " Reoredr Id " + reorderId + " Update successfully");
        }
      } catch (final Exception ex) {
        LOGGER.error(Constants.MARKER, "Update ReOrder", ex);
      }
    } else {
      reorderUpdate.put(
          "Message", " Reoredr Id " + reorderId + " Not Updated enter correct details");
    }
    return reorderUpdate;
  }

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.INVOICE_UPDATE)
  public final void updateInvoice(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {
    final JSONObject invoice = new JSONObject(payload);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    InvoiceDetails invoiceDetails = new InvoiceDetails();
    final long invoiceId = Long.parseLong(invoice.getString("invoiceId"));
    try {
      invoiceDetails = this.invoiceDetailsService.getInvoiceDetailsByInvoiceId(invoiceId);
      if (invoiceDetails.getStatus().equalsIgnoreCase("false")) {
        invoiceDetails.setStatus("true");
        this.invoiceDetailsService.update(invoiceDetails);
        final List<ReOrder> reorder =
            this.employeeCounterService.getAllReOrderDetailsByInvoiceId(invoiceId);
        for (final ReOrder reorderDetails : reorder) {
          if (reorderDetails.getStatus().equalsIgnoreCase("false")) {
            reorderDetails.setStatus("delivered");
          }
          this.employeeCounterService.updateReorder(reorderDetails);
        }
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Not Found");
    } catch (final Exception ex) {
      LOGGER.error("update ReOrder details", ex);
    }
  }

  @SuppressWarnings("unused")
  @PostMapping(path = RouteConstants.INVOICE_CANCEL_DETAILS)
  public final void cancelInvoice(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {
    final JSONObject invoice = new JSONObject(payload);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    InvoiceDetails invoiceDetails = new InvoiceDetails();
    final long invoiceId = Long.parseLong(invoice.getString("invoiceId"));
    final String cancel = invoice.getString("reason");
    try {
      invoiceDetails = this.invoiceDetailsService.getInvoiceDetailsByInvoiceId(invoiceId);
      invoiceDetails.setReason(cancel);
      this.invoiceDetailsService.update(invoiceDetails);
      final List<ReOrder> reorder =
          this.employeeCounterService.getAllReOrderDetailsByInvoiceId(invoiceId);
      for (final ReOrder reorderDetails : reorder) {
        reorderDetails.setStatus(cancel);
        this.employeeCounterService.updateReorder(reorderDetails);
      }
    } catch (final Exception ex) {
      LOGGER.error("update ReOrder by invoiceId", ex);
    }
  }

  @GetMapping(path = RouteConstants.VENDOR_PRODUCTS_VIEW)
  public final List<ProductVendor> viewVendorProducts(
      final HttpServletRequest request, final HttpServletResponse response) {
    final String productCode = request.getParameter("productcode");
    final Product product = this.productService.getProductsByProductCode(productCode);
    final long productId = product.getId();
    final List<ProductVendor> productVendor =
        this.employeeCounterService.viewVendorProducts(productId);
    return productVendor;
  }

  @GetMapping(path = RouteConstants.REORDER_PRODUCT_DETAILS_VIEW)
  public final List<ReOrder> viewReOrder(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<ReOrder> reOrder = new ArrayList<>();
    try {
      final String productCode = request.getParameter("productcode");
      final Product product = this.productService.getProductsByProductCode(productCode);
      final long productId = product.getId();
      reOrder = this.employeeCounterService.viewReorderProducts(productId);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "exception " + ex);
    }
    return reOrder;
  }

  @GetMapping(path = RouteConstants.REORDER_STATUS_VIEW)
  public final ResponseEntity<Object> getReOrderDetails(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<Map<String, Object>> listOfreorders = new ArrayList<>();
    List<Object[]> reorderProducts = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0)
          reorderProducts = this.employeeCounterService.viewReorderDetailsByStatus(orgId, branchId);
        else reorderProducts = this.employeeCounterService.viewReorderDetailsByStatus(orgId);
        for (final Object[] reorder : reorderProducts) {
          final Map<String, Object> reodrerlists = new HashMap<>();
          reodrerlists.put("reorderId", reorder[0]);
          reodrerlists.put("productId", reorder[1]);
          reodrerlists.put("productName", reorder[2]);
          reodrerlists.put("vendorId", reorder[3]);
          reodrerlists.put("vendorName", reorder[4]);
          reodrerlists.put("orderdate", reorder[5]);
          reodrerlists.put("deliveryDate", reorder[6]);
          reodrerlists.put("status", reorder[7]);
          reodrerlists.put("quantity", reorder[8]);
          reodrerlists.put("organisationId", reorder[9]);
          reodrerlists.put("organisationName", reorder[10]);
          reodrerlists.put("branchId", reorder[11]);
          reodrerlists.put("branchName", reorder[12]);
          listOfreorders.add(reodrerlists);
        }
      } else {
        return new ResponseEntity<>(listOfreorders, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    }
    return new ResponseEntity<>(listOfreorders, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ALL_INVOICE_DETAILS)
  public final List<InvoiceDetails> getInvoiceDetails() {
    List<InvoiceDetails> invoiceDetails = new ArrayList<>();
    try {
      invoiceDetails = this.invoiceDetailsService.getAllInvoiceDetails();

    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getInvoiceDetails" + e);
    }
    return invoiceDetails;
  }

  @GetMapping(path = RouteConstants.INVOICE_DETAILS)
  public final ResponseEntity<Object> getInvoiceDetails(
      final HttpServletRequest request, final HttpServletResponse response) throws JSONException {
    List<InvoiceDetails> invoiceDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          invoiceDetails = this.invoiceDetailsService.getAllInvoiceDetailsByOrg(orgId);
        } else {
          invoiceDetails =
              this.invoiceDetailsService.getAllInvoiceDetailsByOrgAndBranch(orgId, branchId);
        }
      } else {
        return new ResponseEntity<>(invoiceDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error("/admin/invoice/details  " + ex);
    }
    return new ResponseEntity<>(invoiceDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVOICE_REORDER)
  public final ResponseEntity<Object> getProducts(
      final HttpServletRequest request, final HttpServletResponse response) {
    final List<Map<String, Object>> list = new ArrayList<>();
    List<InvoiceDetails> invoiceDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          invoiceDetails = this.invoiceDetailsService.getAllInvoiceDetailsByOrg(orgId);
        } else {
          invoiceDetails =
              this.invoiceDetailsService.getAllInvoiceDetailsByOrgAndBranch(orgId, branchId);
        }
        for (final InvoiceDetails invoice : invoiceDetails) {
          final Long invoiceId = invoice.getId();
          final Map<String, Object> map = new HashMap<String, Object>();
          final List<ReOrder> reorder =
              this.employeeCounterService.getAllReOrderDetailsByInvoiceId(invoiceId);
          if (reorder.isEmpty()) {
          } else {
            map.put("invoiceId", invoiceId);
            map.put("reOrder", reorder);
            map.put("status", invoice.getStatus());
            list.add(map);
          }
        }
      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getInvoiceDetails" + ex, ex);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.REORDER_INVOICE_LIST_VIEW)
  public Map<String, Object> getReOrderListByInvoiceId(@PathVariable final Long invoiceId) {
    final Map<String, Object> map = new HashMap<>();
    List<ReOrder> list = new ArrayList<>();
    try {
      list = employeeCounterService.getReOrderDetailsByInvoiceId(invoiceId);
    } catch (final Exception e) {
      LOGGER.error("/reOrders/List/invoiceId/{invoiceId} " + e);
    }
    map.put("reOrderList", list);
    return map;
  }

  @GetMapping(path = RouteConstants.INVOICE_BILL_DETAILS_VENDOR)
  public ResponseEntity<Object> getInvoiceDetailsByVendor(final HttpServletRequest request) {

    List<Map<String, Object>> vendorInvoiceDetails = new ArrayList<Map<String, Object>>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null && "V".equalsIgnoreCase(identifier.getUserType())) {

        final long vendorId = identifier.getUserId();

        long orgId = identifier.getOrgId();
        if (orgId == 0) {
          orgId = Long.parseLong(request.getParameter("orgId"));
        }
        vendorInvoiceDetails =
            invoiceBillService.getInvoiceBillDetailsByLoginVendor(vendorId, orgId);
      } else {
        return new ResponseEntity<>(vendorInvoiceDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getInvoiceDetails" + ex);
    }
    return new ResponseEntity<>(vendorInvoiceDetails, HttpStatus.OK);
  }
}
