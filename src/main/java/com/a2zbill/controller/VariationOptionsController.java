package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.VariationOptions;
import com.a2zbill.services.VariationOptionsService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VariationOptionsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(VariationOptionsController.class);

  @Autowired private VariationOptionsService variationOptionsService;

  @GetMapping(path = RouteConstants.GET_ALL_VARIATION_OPTIONS)
  public final ResponseEntity<Object> getAllVariationOptions() {
    List<VariationOptions> list = new ArrayList<VariationOptions>();
    try {
      list = this.variationOptionsService.getAllVariationOptionsDetails();

    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (Exception em) {
      LOGGER.error(Constants.MARKER, "Get All VariatonOptions" + em);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_VARIATION_OPTIONS_DEATAILS)
  public final ResponseEntity<Object> getVariationOptionsById(
      HttpServletRequest request, HttpServletResponse response) {
    List<VariationOptions> list = new ArrayList<VariationOptions>();
    try {
      final String variationTypeId = request.getParameter("variationTypeId");
      if (variationTypeId != null && !variationTypeId.isEmpty()) {
        list =
            this.variationOptionsService.getVariationOptionsByType(Long.parseLong(variationTypeId));
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (Exception em) {
      LOGGER.error(Constants.MARKER, "Get All VariatonOptions" + em);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }
}
