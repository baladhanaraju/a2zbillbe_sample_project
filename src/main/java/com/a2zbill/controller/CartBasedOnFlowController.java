package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingDetail;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.Flows;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.Item;
import com.a2zbill.domain.LooseProduct;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.SecretKey;
import com.a2zbill.domain.StoreTemplate;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CartProductService;
import com.a2zbill.services.CartService;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.FlowsService;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.LooseProductService;
import com.a2zbill.services.SecretKeyService;
import com.a2zbill.services.StoreTemplateService;
import com.a2zbill.services.impl.CartProductServiceImpl;
import com.a2zbill.services.impl.ItemServiceImpl;
import com.a2zbill.services.impl.ProductServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BasicTemplateServiceException;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CartBasedOnFlowController {

  private static final Logger LOGGER = LoggerFactory.getLogger(CartBasedOnFlowController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private LooseProductService looseProductService;

  @Autowired private OrganisationService organisationService;

  @Autowired private InventoryDetailService inventoryDetailService;

  @Autowired private CartService cartService;

  @Autowired private BillingService billingService;

  @Autowired private CounterService counterService;

  @Autowired private ProductServiceImpl productsService;

  @Autowired private CartProductService cartProductService;

  @Autowired private ItemServiceImpl itemsService;

  @Autowired private FlowsService flowsService;

  @Autowired private SecretKeyService secretKeyService;

  @Autowired private StoreTemplateService storeTemplateService;

  @Autowired private CartProductServiceImpl cartProductServiceImpl;

  @GetMapping(path = RouteConstants.GET_RESTUARANT_CART_PRODUCTS_DETAILS)
  public final Map<String, Object> gettingRestaurentCartProductDetails1(
      final HttpServletRequest request, final HttpServletResponse responce) {
    Map<String, Object> productDetails1 = new HashMap<>();
    final long orgId = jwtTokenUtil.getOrgIdFromToken(request);
    final long branchId = jwtTokenUtil.getBranchIdFromToken(request);
    final String code = request.getParameter("productcode");
    try {
      productDetails1 = this.cartProductServiceImpl.addingProductToCart(orgId, branchId, code);
    } catch (final Exception ex) {
      LOGGER.error("gettingRestaurentCartProductDetails1" + ex, ex);
    }
    return productDetails1;
  }

  @PostMapping(path = RouteConstants.GET_PRODUCT_BILLING_ON_FLOW_ID)
  public final Map<String, Long> getProductBillingBasedOnFlowsId(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload,
      final Billing billing,
      final BillingDetail billingDetails)
      throws JSONException, BasicTemplateServiceException, JsonProcessingException {

    Map<String, Long> map = new HashMap<>();
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    try {
      map = this.cartProductServiceImpl.generateBill(request, responce, employeeNumber, payload);
    } catch (final Exception ex) {
      LOGGER.error("getProductBillingBasedOnFlowsId" + ex);
    }
    return map;
  }

  @PostMapping(path = RouteConstants.SAVE_CART_PRODUCT_AND_UPDATE_BILLING)
  public final List<Map<String, Object>> saveCartproductAndUpdateBilling(
      @RequestBody final String json,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws JSONException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final JSONObject jsonPayload = new JSONObject(json);
    final JSONObject data = jsonPayload.getJSONObject("data");
    final long cartId = data.getLong("cartId");
    final long billId = data.getLong("billId");
    final long totalAmount = data.getLong("totalAmount");
    final long discountAmount = data.getLong("discountAmount");
    final long discountPercentage = data.getLong("discountPercentage");
    final String secretKey = data.getString("secretKey");
    final JSONArray ja = jsonPayload.getJSONArray("payload");
    final List<Map<String, Object>> list = new ArrayList<>();
    final Map<String, Object> map = new HashMap<>();
    try {
      final SecretKey secret = this.secretKeyService.getDetailsBySecretKey(secretKey);
      if (secret == null) {
        map.put("message", "secret key does not match");
        list.add(map);
        return list;
      }

      final List<CartDetail> cartProductDetails =
          this.cartProductService.getCartDetailsByCartId(cartId);
      for (final CartDetail cartProduct : cartProductDetails) {
        this.cartProductService.delete(cartProduct.getId());
      }
      long cgstAmount = Constants.ZERO;
      long sgstAmount = Constants.ZERO;
      for (int i = 0; i < ja.length(); i++) {
        final CartDetail cartDetails = new CartDetail();
        final JSONObject jsonObject = ja.getJSONObject(i);
        final JSONObject products = jsonObject.getJSONObject("code");
        final Branch branch = employeeDetails.getBranch();
        if (branch != null) {

          cartDetails.setBranch(branch);
        } else {
          cartDetails.setBranch(null);
        }
        cartDetails.setOrganisation(employeeDetails.getOrganisation());
        final Cart cart = this.cartService.getCartDetailsByCartId(cartId);
        cartDetails.setCartDetails(cart);
        cartDetails.setCounterDetails(cart.getCounterDetails());
        cartDetails.setCgstPercentage(new BigDecimal(products.getInt("CGST")));
        cartDetails.setSgstPercentage(new BigDecimal(products.getInt("SGST")));
        cgstAmount = cgstAmount + products.getInt("CGST");
        sgstAmount = sgstAmount + products.getInt("SGST");
        cartDetails.setHsnCode(products.getString("HsnNumber"));
        cartDetails.setProductName(products.getString("ProductName"));
        cartDetails.setProductPrice(new BigDecimal(products.getInt("ProductPrice")));
        cartDetails.setTaxableAmount(new BigDecimal(data.getInt("taxableAmount")));
        cartDetails.setQuantity(products.getLong("Quantity"));
        cartDetails.setTotalAmount(new BigDecimal(data.getInt("totalAmount")));
        cartDetails.setTime(new Date(System.currentTimeMillis()));
        this.cartProductService.save(cartDetails);
      }
      final Billing billing = this.billingService.getBillingDetailsByBillId(billId);
      final Cart cart = this.cartService.getCartDetailsByCartId(cartId);
      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setBranch(employeeDetails.getBranch());
      billing.setOrganisation(employeeDetails.getOrganisation());
      billing.setBillDate(new Date());

      billing.setDiscountAmount(new BigDecimal(discountAmount));
      billing.setDiscountPercentage(new BigDecimal(discountPercentage));
      billing.setCgstAmount(new BigDecimal(cgstAmount));
      billing.setSgstAmount(new BigDecimal(cgstAmount));
      billing.setTotalAmount(new BigDecimal(totalAmount));
      billing.setCartId(cart);
      billing.setCounterDetails(cart.getCounterDetails());
      this.billingService.update(billing);
      map.put("billingDetails", billing);
      list.add(map);
    } catch (final Exception ex) {
      LOGGER.error("saveCartproductAndUpdateBilling" + ex, ex);
    }
    return list;
  }

  @SuppressWarnings({"null", "unused"})
  @PostMapping(path = RouteConstants.SAVE_CART_PRODUCT_DETAILS)
  public final Map<String, Object> saveCartProductDetails(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws JSONException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long flowsId = Long.parseLong(request.getParameter("flowsId"));
    final String code = request.getParameter("productcode");
    final JSONObject jsonObject = new JSONObject(payload);
    final Long QTY = 1L;
    final Long counterNumber = jsonObject.getLong("counterNumber");
    Product productDetails = null;
    Item itemDetails = null;
    Cart cart = new Cart();
    InventoryDetail inventoryDetail = null;
    final Map<String, Object> productDetails1 = new HashMap<>();

    final Counter counterDetails = this.counterService.getCounterDetailsById(counterNumber);
    try {
      if (counterDetails != null) {
        cart = this.cartService.getCartDetailsOnStatus(Constants.CARTSTATUS, counterNumber);
      } else {
        cart.setCartStatus(Constants.CARTSTATUS);
        final Date date = new Date();
        cart.setStartTime(date);
        cart.setEndTime(date);
        cart.setCounterDetails(counterDetails);
        this.cartService.save(cart);
      }

      final Organisation orgDetails =
          this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
      final Flows flows = this.flowsService.getFlowsById(flowsId);
      if (productDetails != null) {
        itemDetails = this.itemsService.getItemsByHsnNo(productDetails.getHsnNumber());
      }
      final long storeTemplateId = orgDetails.getStoreTemplateId();
      final StoreTemplate storeTemplateDetails =
          this.storeTemplateService.getStoreTemplateById(storeTemplateId);
      final double d = 0.0;
      BigDecimal cgst = BigDecimal.valueOf(d);
      BigDecimal sgst = BigDecimal.valueOf(d);
      BigDecimal igst = BigDecimal.valueOf(d);

      final BigDecimal d1 = BigDecimal.valueOf(0.00);
      if (itemDetails != null) {
        if (storeTemplateDetails.isZeroPercentTax() == Constants.STATUSTRUE) {

          cgst = itemDetails.getPercentageDetails().getCGST();

          if (cgst.equals(d1)) {

            cgst = itemDetails.getPercentageDetails().getCGST();
            sgst = itemDetails.getPercentageDetails().getSGST();
            igst = itemDetails.getPercentageDetails().getIGST();
          } else {

            sgst = storeTemplateDetails.getSGST();

            if (sgst.equals(d1)) {

              cgst = itemDetails.getPercentageDetails().getCGST();
              sgst = itemDetails.getPercentageDetails().getSGST();
              igst = itemDetails.getPercentageDetails().getIGST();
            } else {

              sgst = storeTemplateDetails.getSGST();
              cgst = storeTemplateDetails.getCGST();
            }
          }
        } else {

          sgst = storeTemplateDetails.getSGST();
          if (sgst.equals(d1)) {

            cgst = itemDetails.getPercentageDetails().getCGST();
            sgst = itemDetails.getPercentageDetails().getSGST();
            igst = itemDetails.getPercentageDetails().getIGST();
          } else {

            sgst = storeTemplateDetails.getSGST();
            cgst = storeTemplateDetails.getCGST();
          }
        }
      }
      try {
        if (code.contains(Constants.LOOSEPRODUCTCODE)) {
          final String[] codes = code.split(Constants.LOOSEPRODUCTCODE);
          final String loosecode = codes[1];
          final LooseProduct looseProduct =
              this.looseProductService.getLooseProductDetailsByCode(loosecode);
        } else {
          if (employeeDetails.getBranch() != null) {
            productDetails =
                this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                    code,
                    employeeDetails.getOrganisation().getId(),
                    employeeDetails.getBranch().getId());
          } else {
            productDetails =
                this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                    code, employeeDetails.getOrganisation().getId(), null);
          }
          inventoryDetail =
              this.inventoryDetailService.getInventoryDetailsByProductIdAndBranchIdAndOrgId(
                  productDetails.getId(),
                  employeeDetails.getBranch().getId(),
                  employeeDetails.getOrganisation().getId());

          if (inventoryDetail.getQuantity().doubleValue() <= 0) {
            productDetails1.put("product", "Products Not Available");
            return productDetails1;
          }
        }
        productDetails1.put("ProductCode", productDetails.getCode());
        productDetails1.put("ProductName", productDetails.getProductName());
        productDetails1.put("HsnNumber", productDetails.getHsnNumber());

        if (productDetails.getIncludingTaxFlag().equals("true")) {

          final BigDecimal price =
              productDetails
                  .getProductPrice()
                  .subtract(
                      (productDetails.getProductPrice())
                          .multiply(sgst.add(cgst))
                          .divide(Constants.HUNDRED));
          productDetails1.put("ProductPrice", price);

          productDetails1.put("SGST", sgst);
          productDetails1.put("CGST", cgst);

        } else {

          productDetails1.put("ProductPrice", productDetails.getProductPrice());
          productDetails1.put("SGST", sgst);
          productDetails1.put("CGST", cgst);
        }
        productDetails1.put("Quantity", QTY);
        productDetails1.put("IGST", itemDetails.getPercentageDetails().getIGST());
        return productDetails1;
      } catch (final Exception empe) {

        productDetails1.put("product", "Products Not Available");
        return productDetails1;
      }
    } catch (final Exception ex) {
      LOGGER.error("saveCartProductDetails" + ex, ex);
    }

    return productDetails1;
  }
}
