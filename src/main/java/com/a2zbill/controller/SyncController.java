package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Sync;
import com.a2zbill.services.SyncService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SyncController {

  private static final Logger LOGGER = LoggerFactory.getLogger(SyncController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private SyncService syncService;

  @PostMapping(path = RouteConstants.SYNC_DETAILS_SAVE)
  public final void saveSyncDetails(
      final HttpServletRequest request, @RequestBody final String payload)
      throws IOException, JSONException {

    final Sync syncDetails = new Sync();

    try {

      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

      final long orgId = employeeDetails.getOrganisation().getId();
      final long branchId = employeeDetails.getBranch().getId();
      final JSONObject jsonObject = new JSONObject(payload);
      final String syncUrl = jsonObject.getString("syncUrl");
      if (jsonObject.has("syncUrl") && !jsonObject.isNull("syncUrl")) {
        final Sync existSync =
            this.syncService.getSyncDetailsByOrgBranchIdAndSyncUrl(orgId, branchId, syncUrl);
        if (existSync != null) {
          existSync.setModifiedDate(new Date());
          this.syncService.update(existSync);
        } else {
          syncDetails.setSyncUrl(syncUrl);
          syncDetails.setOrganisation(employeeDetails.getOrganisation());
          syncDetails.setBranch(employeeDetails.getBranch());
          syncDetails.setCreatedDate(new Date());
          syncDetails.setModifiedDate(new Date());
          this.syncService.save(syncDetails);
        }
      }
    } catch (final RuntimeException ex) {
      LOGGER.error(Constants.MARKER, " /api/save/sync/details   " + ex);
    }
  }

  @GetMapping(path = RouteConstants.GET_ALL_SYNC_DETAILS)
  public final ResponseEntity<Object> getSyncDetails(final HttpServletRequest request)
      throws IOException {
    List<Sync> syncDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0)
          syncDetails = this.syncService.getAllSyncDetailsByOrgBranchId(orgId, branchId);
        else syncDetails = this.syncService.getAllSyncDetailsByOrg(orgId);
      } else {
        return new ResponseEntity<>(syncDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final RuntimeException ex) {
      LOGGER.error(Constants.MARKER, " /api/sync/details  " + ex);
    }
    return new ResponseEntity<>(syncDetails, HttpStatus.OK);
  }
}
