package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.DayClose;
import com.a2zbill.domain.FormData;
import com.a2zbill.domain.FormSchema;
import com.a2zbill.domain.FormType;
import com.a2zbill.services.DayCloseService;
import com.a2zbill.services.FormDataService;
import com.a2zbill.services.FormSchemaService;
import com.a2zbill.services.FormTypeService;
import com.a2zbill.services.impl.TokenServiceImpl;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FormController {

  private static final Logger LOGGER = LoggerFactory.getLogger(FormController.class);

  @Autowired private DayCloseService dayCloseService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private FormTypeService formTypeService;

  @Autowired private FormDataService formDataService;

  @Autowired private BranchService branchService;

  @Autowired private FormSchemaService formSchemaService;

  @Autowired private TokenServiceImpl tokenServiceImpl;

  @PostMapping(path = RouteConstants.ADD_FORM_TYPE)
  public final Map<String, Object> addFormType(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {

    final Map<String, Object> map = new HashMap<>();

    try {
      final JSONObject formType = new JSONObject(payload);
      final FormType formTypeDetails = new FormType();
      map.put("formType", formType.getString("formType"));

      formTypeDetails.setFormtype(map);
      formTypeDetails.setCreatedDate(new Date(System.currentTimeMillis()));
      formTypeDetails.setModifiedDate(new Date(System.currentTimeMillis()));
      formTypeDetails.setFormname(formType.getString("formname"));
      this.formTypeService.save(formTypeDetails);
      map.put("message", formTypeDetails);

    } catch (final Exception ex) {
      LOGGER.error("saveformSchemaDetails" + ex);
    }
    return map;
  }

  @PostMapping(path = RouteConstants.ADD_FORM_SCHEMA)
  public final Map<String, Object> addFormschema(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {

    final Map<String, Object> map = new HashMap<>();

    try {

      final JSONObject formSchema = new JSONObject(payload);
      final FormSchema formSchemaDetails = new FormSchema();
      final FormData formDataDetails = new FormData();
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

      map.put(Constants.FORMSCHEMA, formSchema.getString("formSchema"));

      final FormType formType = this.formTypeService.getFormTypeById(formSchema.getLong("formid"));

      formSchemaDetails.setFormtype(formType);
      formSchemaDetails.setFormschema(map);
      formSchemaDetails.setCreatedDate(new Date(System.currentTimeMillis()));
      formSchemaDetails.setModifiedDate(new Date(System.currentTimeMillis()));

      this.formSchemaService.save(formSchemaDetails);

      formDataDetails.setFormschema(map);
      formDataDetails.setFormtype(formType);
      formDataDetails.setFormschemaid(formSchemaDetails);
      formDataDetails.setBranch(employeeDetails.getBranch());
      formDataDetails.setOrganisation(employeeDetails.getBranch().getOrganisation());
      formDataDetails.setCreatedDate(new Date(System.currentTimeMillis()));
      formDataDetails.setModifiedDate(new Date(System.currentTimeMillis()));
      formDataDetails.setEmployee(employeeDetails);

      this.formDataService.save(formDataDetails);
      map.put("message", formDataDetails);

    } catch (final Exception ex) {
      LOGGER.error("saveformSchemaDetails" + ex);
    }
    return map;
  }

  @PostMapping(path = RouteConstants.ADD_FORM_DATA)
  public final Map<String, Object> addFormdata(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {

    final Map<String, Object> map = new HashMap<>();

    try {
      final JSONObject formdata = new JSONObject(payload);

      final FormData formDataDetails = new FormData();
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

      map.put("formdata", formdata.getString("formdata"));

      final FormType formType = this.formTypeService.getFormTypeById(formdata.getLong("formid"));

      formDataDetails.setFormschema(map);
      formDataDetails.setFormtype(formType);
      final FormSchema formSchemaDetails =
          this.formSchemaService.getAllFormSchemaDetailsById(formdata.getLong("formSchemaId"));
      formDataDetails.setFormschemaid(formSchemaDetails);
      final Branch branch = employeeDetails.getBranch();
      if (branch != null) {
        formDataDetails.setBranch(branch);
      } else {
        formDataDetails.setBranch(null);
        formDataDetails.setOrganisation(employeeDetails.getBranch().getOrganisation());
        formDataDetails.setCreatedDate(new Date(System.currentTimeMillis()));
        formDataDetails.setModifiedDate(new Date(System.currentTimeMillis()));

        this.formDataService.save(formDataDetails);
        map.put("message", formDataDetails);
      }
    } catch (final Exception ex) {
      LOGGER.error("saveformDataDetails" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_ALL_FORM_TYPE_DETAILS)
  public final ResponseEntity<Object> getallFormTypeDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<FormType> formTypedetails = new ArrayList<FormType>();

    try {
      formTypedetails = this.formTypeService.getAllFormTypes();

    } catch (final Exception ex) {
      LOGGER.error("getFormTypeDetails" + ex);
    }
    return new ResponseEntity<>(formTypedetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_FORM_SCHEMA_DETAILS)
  public final List<FormSchema> getFormschemaDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<FormSchema> formSchemadetails = new ArrayList<FormSchema>();

    try {
      formSchemadetails = this.formSchemaService.getAllFormSchema();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getFormSchemaDetails" + ex);
    }
    return formSchemadetails;
  }

  @GetMapping(path = RouteConstants.GET_FORM_TYPE_DETAILS)
  public final FormType getFormTypeDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    FormType formTypedetails = null;
    try {
      final String formid = request.getParameter("formid");
      if (formid != null && !formid.isEmpty()) {
        formTypedetails = this.formTypeService.getFormTypeById(Long.parseLong(formid));
      }

    } catch (final Exception ex) {
      LOGGER.error("getFormTypeDetails" + ex);
    }
    return formTypedetails;
  }

  @GetMapping(path = RouteConstants.GET_FORM_TYPE_DETAILS_NAME)
  public final Map<String, Object> getFormTypeDetailsname(
      final HttpServletRequest request, final HttpServletResponse responce) {

    Map<String, Object> formTypedetails = null;
    final long formid = Long.parseLong(request.getParameter("formid"));
    try {
      formTypedetails = this.formTypeService.getFormTypedetailsById(formid);

    } catch (final Exception ex) {
      LOGGER.error("getFormTypeDetails" + ex, ex);
    }
    return formTypedetails;
  }

  @PostMapping(path = RouteConstants.GET_FORM_DATA_DETAILS)
  public final ResponseEntity<Object> getFormdataDetail(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws ParseException, JSONException {

    final List<Map<String, Object>> dayclosedetails = new ArrayList<Map<String, Object>>();
    List<FormData> formdataDetails = new ArrayList<FormData>();

    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();

        final JSONObject jsonObject = new JSONObject(payload);
        final List<Date> billDates = this.tokenServiceImpl.getReportDates(jsonObject);

        final String branchId = jsonObject.getString("branchId");
        final Date fromDate = billDates.get(0);
        final Date toDate = billDates.get(1);
        final Map<String, Object> formData = new HashMap<>();
        if (branch == 0) {
          formdataDetails =
              this.formDataService.getFormDataByOrgAndBranchisNull(fromDate, toDate, orgId);
        } else {
          formdataDetails =
              this.formDataService.getFormDataByBranchAndOrg(
                  fromDate, toDate, orgId, Long.parseLong(branchId));
        }

        if (formdataDetails.size() == 0) {
          formData.put("message", "No Record is found");
          dayclosedetails.add(formData);
        } else {

          for (final FormData dayclose : formdataDetails) {

            final JSONObject details = new JSONObject(dayclose.getFormschema().get("formSchema"));
            final String amount = details.getString("cash");
            final String creditCard = details.getString("creditcard");
            final String debitCard = details.getString(Constants.DEBITCARD);

            final Branch branchDetails = this.branchService.getBranchById(Long.parseLong(branchId));
            formData.put("branch", branchDetails.getBranchName());
            formData.put("cashAmount", amount);
            formData.put("creditCard", creditCard);
            formData.put("debitcard", debitCard);
            dayclosedetails.add(formData);
          }
        }
      } else {
        return new ResponseEntity<>(dayclosedetails, HttpStatus.UNAUTHORIZED);
      }

    } catch (final Exception ex) {
      LOGGER.error("getFormdataDetails" + ex);
    }
    return new ResponseEntity<>(dayclosedetails, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.GET_SAVE_DAY_CLOSE_DETAILS)
  public final void getSaveDayCloseDetails(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws JSONException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final JSONArray jsonArray = new JSONArray(payload);

    try {
      for (int i = 0; i < jsonArray.length(); i++) {
        final JSONObject jsonObject = ((JSONObject) jsonArray.get(i));
        final DayClose dayClose = new DayClose();
        dayClose.setPaymentId(jsonObject.getLong("id"));
        dayClose.setBranchId(employeeDetails.getBranch());
        dayClose.setDate(new Date());
        dayClose.setOrgId(employeeDetails.getOrganisation());
        final double amountDouble = jsonObject.getDouble("amount");
        final BigDecimal amount =
            BigDecimal.valueOf(amountDouble).setScale(2, RoundingMode.HALF_EVEN);
        dayClose.setPaymentAmount(amount);
        dayClose.setPaymentType(jsonObject.getString("type"));
        dayCloseService.save(dayClose);
      }
    } catch (final Exception ex) {
      LOGGER.error("getFormTypeDetails" + ex, ex);
    }
  }

  @GetMapping(path = RouteConstants.GET_FORM_DATA_DETAILS_BY_BRANCH)
  public final ResponseEntity<Object> getFormdataDetailByBranch(
      final HttpServletRequest request, final HttpServletResponse responce)
      throws ParseException, JSONException {

    List<DayClose> daycloseDetails = new ArrayList<DayClose>();
    try {

      final String branchId = request.getParameter("branchId");
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      final Date fromDate = sdf.parse(request.getParameter("fromDate"));
      final Date toDate = sdf.parse(request.getParameter("toDate"));
      daycloseDetails =
          dayCloseService.getDayCloseDatails(fromDate, toDate, Long.parseLong(branchId));
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getDaycloseDetails" + e);
    }
    return new ResponseEntity<>(daycloseDetails, HttpStatus.OK);
  }
}
