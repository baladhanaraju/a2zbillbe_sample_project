package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.CategoryInventorySection;
import com.a2zbill.domain.InventorySection;
import com.a2zbill.domain.ProductCategory;
import com.a2zbill.services.CategoryInventorySectionService;
import com.a2zbill.services.InventorySectionService;
import com.a2zbill.services.ProductCategoryService;
import com.tsss.basic.security.jwt.Identifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryInventorySectionController {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(CategoryInventorySectionController.class);

  @Autowired private CategoryInventorySectionService categoryInventorySectionService;

  @Autowired private ProductCategoryService productCategoryService;

  @Autowired private InventorySectionService inventorySectionService;

  @GetMapping(path = RouteConstants.GET_CATEGORY_INVENTORY_SECTION)
  public final ResponseEntity<Object> getCategoryInventorySection(
      final HttpServletRequest request) {
    List<CategoryInventorySection> categoryInventorySection = new ArrayList<>();
    try {
      categoryInventorySection =
          this.categoryInventorySectionService.getAllCategoryInventorySection();
    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getCategoryInventorySection" + ex);
    }
    return new ResponseEntity<>(categoryInventorySection, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.SAVE_CATEGORY_INVENTORY_SECTION)
  public final ResponseEntity<Object> saveCategoryInventorySection(
      @RequestBody final String payload, final HttpServletRequest request) throws JSONException {

    final Map<String, Object> map = new HashMap<>();

    StringBuilder result = new StringBuilder();
    String strToAppend = "";
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        final JSONObject jsonObject = new JSONObject(payload);

        final JSONArray categories = jsonObject.getJSONArray("categoryId");
        for (int i = 0; i < categories.length(); i++) {

          final JSONObject categoryJsonObject = (JSONObject) categories.get(i);
          final Integer categoryId = categoryJsonObject.getInt("id");
          final ProductCategory productCategory =
              this.productCategoryService.getProductCategoryById(categoryId.longValue());
          final InventorySection inventorySection =
              this.inventorySectionService.getInventorySectionByOrgIdBranchIdAndId(
                  orgId, branchId, jsonObject.getLong("sectionId"));
          final CategoryInventorySection categoryInventorySection =
              categoryInventorySectionService
                  .getCategoryInventorySectionByCategoryIdAndInventorySectionId(
                      categoryId, jsonObject.getLong("sectionId"));
          if (categoryInventorySection != null) {

            map.put("message", "The Category Mapped details already added!");

          } else {
            final CategoryInventorySection categoryInventorySection1 =
                new CategoryInventorySection();
            categoryInventorySection1.setCategoryId(productCategory);
            categoryInventorySection1.setInventorySection(inventorySection);
            this.categoryInventorySectionService.save(categoryInventorySection1);
            map.put("message", "The Category Mapped details added successfully!");
          }
        }
      } else {
        return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error("saveCategoryInventorySection " + ex);
    }
    return new ResponseEntity<>(map, HttpStatus.OK);
  }
}
