package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CartProductService;
import com.tsss.basic.security.jwt.Identifier;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemReturnController {
  private static final Logger LOGGER = LoggerFactory.getLogger(ItemReturnController.class);
  @Autowired private BillingService billingService;
  @Autowired private CartProductService cartProductService;

  @GetMapping(path = RouteConstants.GET_PRODUCT_ITEM_RETURN)
  public final ResponseEntity<Object> getBillingInfo(
      final HttpServletRequest request, final HttpServletResponse responce) throws Exception {
    Billing billingdetails = null;
    final String billNumber = request.getParameter("billNumber");
    final String dateString = request.getParameter("billDate");
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date date = sdf.parse(dateString);
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        if (billNumber.contains("_")) {

          if (branchId == 0)
            billingdetails =
                this.billingService.getBillingDetailsByBillNumberAndOrg(billNumber, orgId, date);
          else
            billingdetails =
                this.billingService.getBillingDetailsByBillNumberAndOrgAndBranch(
                    billNumber, orgId, branchId, date);

        } else {
          if (branchId == 0)
            billingdetails =
                this.billingService.getBillingDetailsByBillNumberCountAndOrg(
                    billNumber, orgId, date);
          else
            billingdetails =
                this.billingService.getBillingDetailsByBillNumberCountAndOrgAndBranch(
                    billNumber, orgId, branchId, date);
        }
        if (billingdetails != null) {
          final List<CartDetail> cartProductDetails =
              this.cartProductService.getCartDetailsByCartId(
                  billingdetails.getCartId().getCartId());
          return new ResponseEntity<>(cartProductDetails, HttpStatus.OK);
        } else {
          LOGGER.warn("There is no Bill for this BillId");
        }
      } else {
        return new ResponseEntity<>(billingdetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "/api/product/return " + ex);
    }
    return new ResponseEntity<>(billingdetails, HttpStatus.OK);
  }
}
