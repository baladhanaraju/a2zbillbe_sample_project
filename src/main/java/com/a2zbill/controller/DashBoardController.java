package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.Product;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.impl.ProductServiceImpl;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.impl.OrganisationServiceImpl;
import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("rawtypes")
@RestController
public class DashBoardController {
  private static final Logger LOGGER = LoggerFactory.getLogger(DashBoardController.class);

  @Autowired private InventoryDetailService inventoryDetailService;

  @Autowired private BranchService branchService;

  @Autowired private BillingService billingService;

  @Autowired private ProductServiceImpl productsService;

  @Autowired private OrganisationServiceImpl organisationService;

  @GetMapping(path = RouteConstants.GET_DASHBOARD_DETAILS)
  public final ResponseEntity<Object> getDashBoardDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final Map<String, Object> map = new HashMap<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        final Date date = new Date();
        final String billDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        final Date billingDate = new SimpleDateFormat("yyyy-MM-dd").parse(billDate);
        final Calendar now = Calendar.getInstance();
        final int month = now.get(Calendar.MONTH) + 1;
        final int year = now.get(Calendar.YEAR);

        BigDecimal stockQTY = null;
        BigDecimal stockAmount = null;
        BigDecimal currentDaySale = null;
        BigDecimal currentMonthSale = null;

        if (branchId != 0) {
          stockQTY =
              this.inventoryDetailService.getInventoryCurrentStockQTYByorgBranch(orgId, branchId);
          stockAmount =
              this.inventoryDetailService.getInventoryDetailStockAmountByorgBranch(orgId, branchId);
          currentDaySale =
              this.billingService.getBillingCurrentDaySaleAmountByOrgBranch(
                  billingDate, orgId, branchId);
          currentMonthSale =
              this.billingService.getBillingCurrentMonthSaleAmountorgBranch(
                  month, year, orgId, branchId);
        } else {
          stockQTY = this.inventoryDetailService.getInventoryCurrentStockQTYByorg(orgId);
          stockAmount = this.inventoryDetailService.getInventoryDetailStockAmountByorg(orgId);
          currentDaySale =
              this.billingService.getBillingCurrentDaySaleAmountByOrg(billingDate, orgId);
          currentMonthSale =
              this.billingService.getBillingCurrentMonthSaleAmountorg(month, year, orgId);
        }
        if (currentDaySale == null && currentMonthSale == null) {
          map.put("currentDaySale", 0);
          map.put("currentMonthSale", 0);
        } else {
          map.put("currentDaySale", currentDaySale);
          map.put("currentMonthSale", currentMonthSale);
        }
        map.put("stockAmount", stockAmount);
        map.put("stockQTY", stockQTY);
      } else {
        return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error("dashBoardDetails" + ex);
    }
    return new ResponseEntity<>(map, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.DASHBOARD_SALE_DETAILS)
  public final Map<String, Object> getSalesDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final Map<String, Object> saleDetails = new HashMap<>();
    try {
      final Calendar now = Calendar.getInstance();
      final int month = now.get(Calendar.MONTH) + 1;
      int year = now.get(Calendar.YEAR);
      final DateFormatSymbols dfs = new DateFormatSymbols();
      final String[] months = dfs.getMonths();
      int saleMonth = month;
      for (int i = 0; i < 6; i++) {
        if (saleMonth == 1) {
          saleMonth = 12;
          year = year - 1;
        } else {
          saleMonth = saleMonth - 1;
        }
        BigDecimal monthSale =
            this.billingService.getBillingLastSixMonthSaleAmount(saleMonth, year);
        if (monthSale == null) {
          monthSale = new BigDecimal(0);
        }
        saleDetails.put(months[saleMonth - 1], monthSale);
      }
    } catch (final Exception ex) {
      LOGGER.error("dashBoardSales" + ex);
    }
    return saleDetails;
  }

  @GetMapping(path = RouteConstants.ADD_QUANTITY_INVENTORY)
  public final ResponseEntity<Object> findCurrentDaySale(
      final HttpServletRequest request, final HttpServletResponse responce) {
    InventoryDetail inventoryDetail = null;
    Product productDetails = null;
    String products = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long branchId = identifier.getBranchId();
        final String productCode = request.getParameter("productcode");
        final BigDecimal quantity = new BigDecimal(request.getParameter("quantity"));
        productDetails = this.productsService.getProductsByProductCode(productCode);
        inventoryDetail =
            this.inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                productDetails.getId(), branchId);
        final BigDecimal existingQuantity = inventoryDetail.getQuantity();
        final BigDecimal totalQuantity = existingQuantity.add(quantity);
        inventoryDetail.setQuantity(totalQuantity);
        this.inventoryDetailService.update(inventoryDetail);
        products = " Successfully added quantity for that Product ";
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error("findCurrentDaySale" + ex);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ADMIN_COUNT_ORG_AND_BRANCHID)
  public final ResponseEntity<Object> findCountByOrgIdAndBrnachId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final List<Object> listOfBrnachCount = new ArrayList<>();
    final Identifier identifier = (Identifier) request.getAttribute("identifier");
    if (identifier != null) {
      final long orgId = identifier.getOrgId();
      long productsCountByOrgId = 0;
      final List<Branch> brnachDetails = this.branchService.getBranchesByOrganisationId(orgId);
      for (final Branch branch : brnachDetails) {
        final Map<String, Object> branchWiseProductsCount = new HashMap<>();
        productsCountByOrgId =
            this.productsService.getProductsCounByOrgIdAndBranchId(branch.getId(), orgId);
        branchWiseProductsCount.put("branchName", branch.getBranchName());
        branchWiseProductsCount.put("Count", productsCountByOrgId);
        listOfBrnachCount.add(branchWiseProductsCount);
      }
    } else {
      return new ResponseEntity<>(listOfBrnachCount, HttpStatus.UNAUTHORIZED);
    }
    return new ResponseEntity<>(listOfBrnachCount, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.SUPERADMIN_COUNT_BY_ORG)
  public final List findCountByOrgId(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final List<Object> listOfOrgProductCounts = new ArrayList<>();
    final List<Organisation> organisationDetails = this.organisationService.getAllOrganisation();
    long productsCountByOrgId = 0;
    for (final Organisation organisation : organisationDetails) {
      final Map<String, Object> orgWiseProductsCount = new HashMap<>();
      productsCountByOrgId = this.productsService.getProductsCount(organisation.getId());
      orgWiseProductsCount.put("OrganisationName", organisation.getOrgName());
      orgWiseProductsCount.put("Count", productsCountByOrgId);
      listOfOrgProductCounts.add(orgWiseProductsCount);
    }
    return listOfOrgProductCounts;
  }
}
