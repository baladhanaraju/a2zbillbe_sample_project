package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Review;
import com.a2zbill.services.ReviewService;
import com.tsss.basic.security.jwt.Identifier;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReviewController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReviewController.class);

  @Autowired private ReviewService reviewService;

  @GetMapping(path = RouteConstants.GET_REVIEW_DETAILS)
  public final ResponseEntity<Object> getAllReviewDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Review> allReviewDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0)
          allReviewDetails = reviewService.getAllReviewDetailsByOrgBranchId(orgId, branchId);
        else allReviewDetails = reviewService.getAllReviewDetailsByOrg(orgId);
      } else {
        return new ResponseEntity<>(allReviewDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "/api/review/details    " + ex);
    }
    return new ResponseEntity<>(allReviewDetails, HttpStatus.OK);
  }
}
