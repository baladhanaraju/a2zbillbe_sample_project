package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Flow;
import com.a2zbill.domain.FlowType;
import com.a2zbill.services.FlowService;
import com.a2zbill.services.FlowTypeService;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.service.OrganisationService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlowController {

  private static final Logger LOGGER = LoggerFactory.getLogger(FlowController.class);

  @Autowired private FlowTypeService flowTypeService;

  @Autowired private FlowService flowService;

  @Autowired private OrganisationService organisationService;

  @PostMapping(path = RouteConstants.ADD_FLOW_DETAILS)
  public final Map<String, Object> addFlowDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final Map<String, Object> map = new HashMap<String, Object>();

    try {
      final JSONObject floor = new JSONObject(payload);
      Flow floorDetails = new Flow();

      floorDetails.setFlowName(floor.getString("flowName"));
      floorDetails.setFlowRanking(floor.getLong("flowRanking"));
      final FlowType flowType = this.flowTypeService.getFlowTypeById(floor.getLong("flowTypeId"));
      final Organisation orgDetails =
          this.organisationService.getOrganisationById(floor.getLong("orgId"));
      floorDetails.setFlowType(flowType);
      floorDetails.setOrganisation(orgDetails);
      this.flowService.save(floorDetails);
      map.put("message", floorDetails);
    } catch (final Exception ex) {
      LOGGER.error("addFlowTypeDetails" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_FLOW_DETAILS)
  public final ResponseEntity<Object> getFlowDetails() {

    List<Flow> floorDetails = new ArrayList<Flow>();

    try {
      floorDetails = this.flowService.getAllFlowDetails();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getFlowDetails" + e);
    }
    return new ResponseEntity<>(floorDetails, HttpStatus.OK);
  }
}
