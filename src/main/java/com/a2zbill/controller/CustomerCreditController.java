package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.CustomerCredit;
import com.a2zbill.domain.CustomerCreditHistory;
import com.a2zbill.services.CustomerCreditService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerCreditController {

  private static final Logger logger = LoggerFactory.getLogger(CustomerCreditController.class);

  @Value("${jwt.header}")
  private String tokenHeader;

  @Autowired private CustomerCreditService customerCreditService;

  @Autowired private CustomerService customerService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @PostMapping(
      value = RouteConstants.CUSTOMER_CREDIT_SAVE,
      produces = Constants.CONTENT_TYPE_JSON,
      consumes = Constants.CONTENT_TYPE_JSON)
  public ResponseEntity<Object> saveCustCredit(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final Map<String, Object> map = new HashMap<>();
    try {
      Customer customer = null;
      final JSONObject customercreditData = new JSONObject(payload);
      final CustomerCredit customerCredit = new CustomerCredit();
      CustomerCredit customerCreditDetails = null;
      if (customercreditData.has("id")) {
        if (customercreditData.getString("id") != null) {
          customer =
              customerService.getCustomerDetailsBycustomerId(
                  Long.parseLong(customercreditData.getString("id")));
        }
      }
      final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      BigDecimal creditLimit = new BigDecimal(0);
      final BigDecimal payableAmount = new BigDecimal(0);
      if (customer != null) {
        customerCreditDetails =
            customerCreditService.getCustomerCreditDetailsByCustomerId(customer.getCustomerId());
      }
      if (customerCreditDetails != null) {
        if (customercreditData.has("creditLimitAmt")) {

          creditLimit = new BigDecimal(customercreditData.getString("creditLimitAmt"));
          final BigDecimal previousCreditLimit = customerCreditDetails.getCreditLimit();
          if (previousCreditLimit != null && creditLimit != null) {
            customerCreditDetails.setCreditLimit(previousCreditLimit.add(creditLimit));
          } else if (previousCreditLimit == null) {
            customerCreditDetails.setCreditLimit(creditLimit);
          }
        }
        BigDecimal creditAmount = creditLimit;

        final BigDecimal previousCreditAmount = customerCreditDetails.getCreditAmount();
        if (previousCreditAmount != null) {
          creditAmount = creditAmount.add(previousCreditAmount).subtract(payableAmount);
          customerCreditDetails.setCreditAmount(creditAmount);
        }
        customerCreditService.update(customerCreditDetails);
        map.put("success", "Customer credits details updated");
      } else {
        if (employeeDetails != null) {
          final Organisation organisation = employeeDetails.getOrganisation();
          if (organisation != null) {
            customerCredit.setOrganisationId(organisation);
          }
          final Branch branch = employeeDetails.getBranch();
          customerCredit.setBranchId(branch);
        }
        if (customer != null) {
          customerCredit.setCustomerId(customer);
          customerCredit.setCreatedTs(new Date(System.currentTimeMillis()));
          customerCredit.setModifiedTs(new Date(System.currentTimeMillis()));
          customerCredit.setCreditAmount(
              new BigDecimal(customercreditData.getString("creditLimitAmt"))
                  .subtract(payableAmount));
          customerCredit.setCreditLimit(
              new BigDecimal(customercreditData.getString("creditLimitAmt")));
          customerCredit.setPayableAmount(payableAmount);
          customerCreditService.save(customerCredit);
          map.put("success", "Customer credits added");
        } else {
          map.put("error", "Customer must select");
        }
      }
    } catch (final Exception e) {
      map.put("error", "Customer credits not added please check once");
      logger.error(e.toString(), e);
    }
    if (map.containsKey("success")) {
      return new ResponseEntity<Object>(map, HttpStatus.OK);
    } else {
      return new ResponseEntity<Object>(map, HttpStatus.OK);
    }
  }

  @GetMapping(value = RouteConstants.GET_ALL_CUSTOMER_CREDITS_HISTORY)
  public ResponseEntity<Object> getAllCustomerCrediHistorytDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final Identifier identifier = (Identifier) request.getAttribute("identifier");

    List<CustomerCreditHistory> customerCreditHistory = null;
    try {
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        customerCreditHistory = new ArrayList<CustomerCreditHistory>();

        if (branchId != 0) {
          customerCreditHistory =
              customerCreditService.getAllCustomerCreditHistoryByOrgAndBranch(orgId, branchId);
        } else {
          customerCreditHistory = customerCreditService.getAllCustomerCreditHistoryByOrg(orgId);
        }
      } else {
        return new ResponseEntity<>(customerCreditHistory, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      logger.error("getAllCustomerCreditsHistory", ex);
    }
    return new ResponseEntity<>(customerCreditHistory, HttpStatus.OK);
  }

  @GetMapping(value = RouteConstants.GET_CUSTOMER_CREDITS_HISTORY)
  public List<CustomerCreditHistory> getCustomerCreditHistoryDetailsByCustomer(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final String authToken = request.getHeader(tokenHeader);
    final String token = authToken.substring(7);
    final Identifier identifier = jwtTokenUtil.getSubjectFromToken(token);
    final long userId = identifier.getUserId();
    List<CustomerCreditHistory> customerCreditHistory = new ArrayList<CustomerCreditHistory>();
    if (identifier.getUserType().equalsIgnoreCase("C")) {
      final Customer customer = customerService.getCustomerDetailsBycustomerId(userId);
      if (customer != null) {
        final CustomerCredit customerCredit =
            customerCreditService.getCustomerCreditDetailsByCustomerId(customer.getCustomerId());
        if (customerCredit != null) {
          customerCreditHistory =
              customerCreditService.getCustomerCreditHistoryByCustomerCreditId(
                  customerCredit.getId());
        }
      }
    }
    return customerCreditHistory;
  }

  @GetMapping(value = RouteConstants.GET_ALL_CUSTOMER_CREDITS)
  public ResponseEntity<Object> getAllCustomerCreditDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final Identifier identifier = (Identifier) request.getAttribute("identifier");
    List<CustomerCredit> customerCredits = null;

    try {
      customerCredits = new ArrayList<>();
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0) {
          customerCredits =
              customerCreditService.getAllCustomerCreditsByOrgAndBranch(orgId, branchId);
        } else {
          customerCredits = customerCreditService.getAllCustomerCreditsByOrg(orgId);
        }
      } else {
        return new ResponseEntity<>(customerCredits, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      logger.error("getAllCustomerCreditsHistory", ex);
    }
    return new ResponseEntity<>(customerCredits, HttpStatus.OK);
  }

  @PostMapping(
      value = RouteConstants.CUSTOMER_CREDIT_OFFLINE_UPDATE,
      produces = Constants.CONTENT_TYPE_JSON,
      consumes = Constants.CONTENT_TYPE_JSON)
  public ResponseEntity<Object> updateOfflineCustomerCredits(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final JSONArray jsonarray = new JSONArray(payload);
    final List<String> listSuccess = new ArrayList<>();
    final List<String> listError = new ArrayList<>();
    final Map<String, Object> mapMesz = new HashMap<>();
    try {
      for (int creditArraySize = 0; creditArraySize < jsonarray.length(); creditArraySize++) {
        final JSONObject customercreditData = jsonarray.getJSONObject(creditArraySize);
        final CustomerCredit customerCredit =
            customerCreditService.getCustomerCreditDetailsByCustomerId(
                Long.parseLong(customercreditData.getString("customerid")));
        if (customerCredit != null) {
          customerCredit.setCreditAmount(
              new BigDecimal(customercreditData.getString("creditamount")));
          customerCredit.setCreditLimit(
              new BigDecimal(customercreditData.getString("creditlimit")));
          customerCredit.setPayableAmount(
              new BigDecimal(customercreditData.getString("payableamount")));
          customerCredit.setModifiedTs(new Date(System.currentTimeMillis()));
          customerCreditService.update(customerCredit);
          listSuccess.add(customercreditData.getString("mobilenumber"));
          mapMesz.put("success", "customer credits updated for" + listSuccess);
        } else {
          listError.add(customercreditData.getString("mobilenumber"));
          mapMesz.put("error", "customer credits not updated for" + listError);
        }
      }

    } catch (final Exception e) {

      logger.error(e.toString(), e);
    }
    if (mapMesz.containsKey("success")) {
      return new ResponseEntity<Object>(mapMesz, HttpStatus.OK);
    } else {
      return new ResponseEntity<Object>(mapMesz, HttpStatus.OK);
    }
  }

  @PostMapping(
      value = RouteConstants.CUSTOMER_CREDIT_HISTORY_SAVE,
      produces = Constants.CONTENT_TYPE_JSON,
      consumes = Constants.CONTENT_TYPE_JSON)
  public ResponseEntity<Object> savePaidCustomerCreditHistory(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {
    final Map<String, Object> map = new HashMap<>();
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Organisation organisation = employeeDetails.getOrganisation();
    final Branch branch = employeeDetails.getBranch();
    try {
      final JSONObject customercreditHistoryData = new JSONObject(payload);
      final String data = customercreditHistoryData.getString("paymentmode");
      final JSONArray paymentMode = new JSONArray(data);
      for (int i = 0; i < paymentMode.length(); i++) {
        final JSONObject pay = ((JSONObject) paymentMode.get(i));
        final String paymentType = pay.getString("paymentType");

        final CustomerCredit customerCreditDetails =
            customerCreditService.getCustomerCreditDetailsByCustomerId(
                Long.parseLong(customercreditHistoryData.getString("id")));
        final BigDecimal amount = new BigDecimal(customercreditHistoryData.getString("amount"));
        if (customerCreditDetails != null) {
          final BigDecimal previousCreditAmount = customerCreditDetails.getCreditAmount();
          final BigDecimal previouspayableAmount = customerCreditDetails.getPayableAmount();
          customerCreditDetails.setCreditAmount(previousCreditAmount.add(amount));
          final int j = previouspayableAmount.compareTo(amount);
          if (j > 0) {
            customerCreditDetails.setPayableAmount(previouspayableAmount.subtract(amount));
          } else {
            customerCreditDetails.setPayableAmount(amount.subtract(previouspayableAmount));
          }
        }
        final CustomerCreditHistory customerCreditHistory = new CustomerCreditHistory();
        customerCreditHistory.setCustomerCreditId(customerCreditDetails);
        customerCreditHistory.setPaidAmount(amount);
        customerCreditHistory.setCreatedTs(new Date(System.currentTimeMillis()));
        customerCreditHistory.setModifiedTs(new Date(System.currentTimeMillis()));
        customerCreditHistory.setPaymentMode(paymentType);
        customerCreditHistory.setSpendAmount(new BigDecimal(0));
        if (branch != null) {
          customerCreditHistory.setBranchId(branch);
        }
        if (organisation != null) {
          customerCreditHistory.setOrganisationId(organisation);
        }
        customerCreditService.saveCustomerCreditHistory(customerCreditHistory);
        map.put("success", "Customer credits history are added");
      }
    } catch (final Exception e) {
      map.put("error", "Customer credits history are not added once check details");
      logger.error(e.toString(), e);
    }

    if (map.containsKey("success")) {
      return new ResponseEntity<Object>(map, HttpStatus.OK);
    } else {
      return new ResponseEntity<Object>(map, HttpStatus.OK);
    }
  }

  @PostMapping(
      value = RouteConstants.CUSTOMER_CREDIT_HISTORY_SAVE_OFFLINE,
      produces = Constants.CONTENT_TYPE_JSON,
      consumes = Constants.CONTENT_TYPE_JSON)
  public ResponseEntity<Object> updateOfflineCustomerCreditHistory(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final Map<String, Object> map = new HashMap<>();
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Organisation organisation = employeeDetails.getOrganisation();
    final Branch branch = employeeDetails.getBranch();
    final JSONArray jsonarray = new JSONArray(payload);
    final List<String> listSuccess = new ArrayList<String>();
    final List<String> listError = new ArrayList<String>();
    final SimpleDateFormat sdf = new SimpleDateFormat("s");
    final CustomerCreditHistory customerCreditHistory = new CustomerCreditHistory();
    try {
      for (int creditArraySize = 0; creditArraySize < jsonarray.length(); creditArraySize++) {
        final JSONObject customercreditHistoryData = jsonarray.getJSONObject(creditArraySize);
        final Customer customer =
            customerService.getCustomerByMobileNumber(
                customercreditHistoryData.getString("mobilenumber"));
        if (customer != null) {
          final CustomerCredit customerCreditDetails =
              customerCreditService.getCustomerCreditDetailsByCustomerId(customer.getCustomerId());
          customerCreditHistory.setCustomerCreditId(customerCreditDetails);

          customerCreditHistory.setCreatedTs(
              sdf.parse(customercreditHistoryData.getString("createdTime")));
          customerCreditHistory.setModifiedTs(new Date(System.currentTimeMillis()));
          customerCreditHistory.setPaidAmount(
              new BigDecimal(customercreditHistoryData.getString("paidamount")));
          customerCreditHistory.setSpendAmount(
              new BigDecimal(customercreditHistoryData.getString("spendamount")));
          customerCreditHistory.setPaymentMode(customercreditHistoryData.getString("paymentmode"));
          if (branch != null) {
            customerCreditHistory.setBranchId(branch);
          }
          if (organisation != null) {
            customerCreditHistory.setOrganisationId(organisation);
          }
          customerCreditService.saveCustomerCreditHistory(customerCreditHistory);
          listSuccess.add(customercreditHistoryData.getString("mobilenumber"));
          map.put("success", "Customer credits history are added for " + listSuccess);
        } else {
          listError.add(customercreditHistoryData.getString("mobilenumber"));
          map.put("error", "Customer credits history not added for " + listError);
        }
      }

    } catch (final Exception e) {
      logger.error(e.toString(), e);
    }
    if (map.containsKey("success")) {
      return new ResponseEntity<Object>(map, HttpStatus.OK);
    } else {
      return new ResponseEntity<Object>(map, HttpStatus.OK);
    }
  }
}
