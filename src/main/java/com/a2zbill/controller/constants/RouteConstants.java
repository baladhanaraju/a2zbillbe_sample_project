package com.a2zbill.controller.constants;

public class RouteConstants {

  private RouteConstants() {}

  // AlterBill
  public static final String ALTER_BILL_SAVE = "/api/alteredbillings/save";
  public static final String ALTER_BILL_DETAILS = "/api/getAllAlteredbillings/Details";

  // BillingReport
  public static final String GET_BILLING_REPORT = "/api/billing/report";
  public static final String GET_BILLING_REPORT_DETAILS = "/api/billingreport/details";
  public static final String GET_BILLING_DETAILS_BY_DATE = "/api/billingdetails/Date";
  public static final String GET_BILLING_DETAILS_BY_REPORT = "/api/billingdetails/Report";
  public static final String GET_BILLING_DETAILS_BY_CUSTOMER_ID = "/api/billing/details/bycustomer";
  public static final String GET_BILLING_PAYMENT_DETAILS = "/api/billingpayment/details";

  // cancelItem
  public static final String SAVE_CANCEL_ITEM_DETAILS = "/api/save/cancelitem/details";

  // CartBasedOnFlow
  public static final String GET_RESTUARANT_CART_PRODUCTS_DETAILS =
      "/api/cartproduct/detailsbyFlowId";
  public static final String GET_PRODUCT_BILLING_ON_FLOW_ID = "/api/product/billingBasedOnFlowsId";
  public static final String SAVE_CART_PRODUCT_AND_UPDATE_BILLING =
      "/api/saveCartproductAndUpdateBilling/details";
  public static final String SAVE_CART_PRODUCT_DETAILS = "/api/savingcartproduct/details";

  // categoryInventorySection
  public static final String GET_CATEGORY_INVENTORY_SECTION =
      "/api/category/inventorysection/details";
  public static final String SAVE_CATEGORY_INVENTORY_SECTION =
      "/api/category/inventorySection/save/details";

  // COUNTER
  public static final String SAVE_COUNTER_DETAILS = "/api/save/counter/details";
  public static final String UPDATE_COUNTER_DETAILS = "/api/update/counter";
  public static final String GET_COUNTER_DETAILS = "/api/counter/details";
  public static final String GET_COUNTER_STATUS_OPEN = "/api/counterstatus/open";
  public static final String GET_COUNTER_STATUS_CLOSED = "/api/counterstatus/closed";
  public static final String GET_COUNTER_STATUS_LIST = "/api/counter/list";
  public static final String GET_COUNTER_STATUS_CLOSED_ORGID_AND_BRANCHID =
      "/api/orgbranch/counterstatus/closed";
  public static final String GET_COUNTER_STATUS_OPEN_ORGID_AND_BRANCHID =
      "/api/orgbranch/counterstatus/open";
  public static final String GET_COUNTER_STATUS_DETAILS_ORGID_AND_BRANCHID =
      "/api/orgbranch/counter/details";
  public static final String GET_COUNTER_LIST_ACTIVE = "/api/counter/list/active";
  public static final String GET_COUNTER_LIST_INACTIVE = "/api/counter/list/inactive";
  public static final String GET_COUNTER_LIST_SORTING = "/api/counter/list/sorting";
  public static final String GET_COUNTER_DELETE = "/api/counter/delete";
  public static final String UPDATE_COUNTER_STATUS = "/api/update/counter/status";
  public static final String GET_COUNTER_LIST_TRUE = "/api/counter/list/true";

  // counterpayment
  public static final String CREATE_CART_LIST = "/api/createCart";
  public static final String COUNTER_PAYMENT_LIST = "/api/counterPayment/details";

  // COUNTER PRODUCT
  public static final String EMPLOYEE_COUNTER_DETAILS_VIEW = "/api/employeecounterdeatils/view";
  public static final String EMPLOYEE_COUNTER_PRODUCT_DETAILS = "/api/employeecounterProducts/view";
  public static final String RESTURANT_COUNTER_LIST_VIEW = "/api/restaurant/counterslist/view";
  public static final String RESTURANT_COUNTERBILL_LIST_VIEW = "/api/restaurant/countersbill/view";
  public static final String RESTURANT_COUNTERS_UPDATE = "/api/restaurant/counters/update";
  public static final String SAVE_VENDOR_PRODUCTS = "/api/vendorproducts/save";
  public static final String REORDER_PRODUCT_DETAILS = "/api/reorderproducts/save";
  public static final String VENDOR_PRODUCTS_UPDATE = "/api/vendorproducts/update";
  public static final String REORDERPRODUCT_DETAILS_UPDATE = "/api/reorderproducts/update";
  public static final String INVOICE_UPDATE = "/api/invoice/update";
  public static final String INVOICE_CANCEL_DETAILS = "/api/invoice/cancel";
  public static final String VENDOR_PRODUCTS_VIEW = "/api/vendorproducts/view";
  public static final String REORDER_PRODUCT_DETAILS_VIEW = "/api/reorderproducts/view";
  public static final String REORDER_STATUS_VIEW = "/api/reorderstatus/view";
  public static final String ALL_INVOICE_DETAILS = "/api/allInvoice/details";
  public static final String INVOICE_DETAILS = "/api/invoice/details";
  public static final String INVOICE_REORDER = "/api/invoice/reorder";
  public static final String REORDER_INVOICE_LIST_VIEW = "/reOrders/List/invoiceId/{invoiceId}";
  public static final String INVOICE_BILL_DETAILS_VENDOR = "/vendor/invoicebill/details";

  // CURRENT SALES REPORTS
  public static final String ADMIN_CURRENTDAY_SALES_PRODUCTWISE_REPORT =
      "/admin/currrentDay/sale/productwise/details";
  public static final String ADMIN_CURRENTDAY_SALES_BRANCHWISE_REPORT =
      "/admin/currrentDay/sale/branchwise";
  public static final String ADMIN_CURRENTDAY_INVENTORYSECTIONDATAHISTORY_PRODUCTWISE_REPORT =
      "/admin/currrentDay/inventorysectiondatahistory/productwise";
  public static final String ADMIN_CURRENTDAY_INVENTORYDATEHISTORY_ITEMWISE =
      "/admin/currrentDay/invsectiondateHistory/itemwise";
  public static final String ADMIN_NETSALE_BILLING = "/admin/NetSales/billing";
  public static final String ADMIN_CURRENTDAY_PRODUCTWISE_QUANTITY =
      "/admin/currrentDay/productwiseq/quantity";
  public static final String CATEGORYWISE_QUANTITY_REPORT = "/api/categorywise/quantity";
  public static final String INVOICE_PURCHASE_REPORTS = "/api/invoice/purchase/reports";
  public static final String INVOICEBILL_REPORT_VENDORNAME = "/api/invoicebill/reports/vendorName";
  public static final String INVOICEBILL_REPORT = "/api/invoicebill/reports";
  public static final String ISSUE_BILL_REPORTS = "/api/issue/bill/reports";
  public static final String STOCK_ITEM_REPORTS = "/api/stock/item/reports";
  public static final String INVOICE_VENDOR_REPORTS = "/api/invoice/vendors/reports";
  public static final String ADMIN_DATES_SALES_REPORTS = "/admin/dates/sales/report";
  public static final String TRANSFER_ITEMWISE_REPORTS = "/api/transfer/itemout/reports";

  // Customer Credit
  public static final String CUSTOMER_CREDIT_SAVE = "/admin/customercredit/save";
  public static final String GET_ALL_CUSTOMER_CREDITS_HISTORY = "/admin/all/customercredithistory";
  public static final String GET_CUSTOMER_CREDITS_HISTORY =
      "/customer/customercredithistory/details";
  public static final String GET_ALL_CUSTOMER_CREDITS = "/api/all/customercredits";
  public static final String GET_CUSTOMER_CREDITS = "/customer/customercredits/details";
  public static final String CUSTOMER_CREDIT_OFFLINE_UPDATE =
      "/api/offline/update/customercredit/details";
  public static final String CUSTOMER_CREDIT_HISTORY_SAVE = "/api/customercredithistory/save";
  public static final String CUSTOMER_CREDIT_HISTORY_SAVE_OFFLINE =
      "/api/offline/update/customercredithistory/details";

  // Customer credits
  public static final String SAVE_CUSTOMERCREDIT_DETAILS = "/api/save/customerCreditsDetails";
  public static final String CUSTOMER_CREDIT_ORG = "/customer/credits/org";

  // Customer Events
  public static final String SAVE_CUSTOMER_EVENT_DETAILS = "/api/save/customerevents/details";
  public static final String GET_ALL_CUSTOMER_EVENTS_DETAILS = "/api/getAllCustomerEvents/Details";
  public static final String CUSTOMEREVENT_COUNT_CURRENTMONTH =
      "/api/CustomerEventscount/currentMonth";

  // CustomerRedeemProduct
  public static final String SAVE_CUSTOMER_REDEEM_PRODUCT = "/api/save/customer/redeem/products";
  public static final String GET_CUSTOMER_REDEEM_ORODUCTS = "/api/customer/redeem/products";
  public static final String GET_CUSTOMER_REDEEM_CUSTOMERID = "/api/customer/redeem/customerId";
  public static final String CUSTOMER_REDEEM_CUST_TRANCTION_CUSTID =
      "/api/customer/redeem/and/customertransaction/customerId";
  public static final String CUSTOMER_REDEEM_PRODUCTID = "/api/customer/redeem/productId";

  // Customer Vendor Tocken
  public static final String SAVE_CUSTVENDOR_TOKEN = "/api/CustomerVendorToken/save";
  public static final String GET_CUSTOMER_VENDOR_TOKEN = "/api/CustomerVendorToken/getALLDetails";
  public static final String CUSTOMER_VENDOR_TOKEN = "/api/CustomerVendorToken/getProductRootName";

  // DasBoard
  public static final String GET_DASHBOARD_DETAILS = "/api/dashboard/details";
  public static final String DASHBOARD_SALE_DETAILS = "/api/dashboard/sale/details";
  public static final String ADD_QUANTITY_INVENTORY = "/api/addqty/inv";
  public static final String ADMIN_COUNT_ORG_AND_BRANCHID = "/admin/countbyorg/and/branchId";
  public static final String SUPERADMIN_COUNT_BY_ORG = "/superadmin/countbyorg";

  // Delivery Options
  public static final String GET_DELIVERY_OPTION_DETAILS = "/api/getAllDeliveryOptionsDetails";

  // DiscountCoupons
  public static final String SAVE_DISCOUNT_COUPONS = "/api/save/discountCoupons";
  public static final String GET_DISCOUNTCOUPONS = "/api/allDiscountCoupons/Details";
  public static final String GET_DEALNAME_BY_ID = "/api/getDealNameById";
  public static final String GET_ALL_DEALNAMES = "/api/getAllDealNames";

  // DOCUMENT DATA
  public static final String SAVE_DOCUMENT = "/api/document/save";
  public static final String SAVE_MULTIPLE_DOCUMENT = "/api/document/multiple/save";
  public static final String GET_DOCUMENT_DETAILS = "/api/document/details";
  public static final String GET_DOCUMENT_DATA = "/api/document/category/details";

  // Employee Counter
  public static final String EMPLOYEE_COUNTER_DETAILS = "/api/empcounter/details";

  // EventType
  public static final String ADD_EVENT_TYPE_DETAILS = "/api/save/eventtype/details";
  public static final String GET_EVENT_TYPE_DETAILS = "/api/getAllEventTypeDetails";

  // ExpenseManagement
  public static final String ADD_EXPENSES_DETAILS = "/api/save/expenses";
  public static final String GET_EXPENSES_MANAGEMENT_DETAILS = "/api/getAllExpensesDetails";
  public static final String GET_EXPENSES_MANAGEMENT_DETAILS_BY_DATE = "/api/getExpenses/byDate";
  public static final String UPDATE_EXPENSES_MANAGEMENT = "/api/update/expenses";
  public static final String DELETE_EXPENSES_DETAILS_BY_ID = "/api/delete/expenses";

  // Flow
  public static final String ADD_FLOW_DETAILS = "/api/add/flowDetails";
  public static final String GET_FLOW_DETAILS = "/api/getAllFlowDetails";

  // Flows
  public static final String ADD_FLOWS_DETAILS = "/api/add/flowsDetails";
  public static final String GET_FLOWS_DETAILS = "/api/getAllFlowsDetails";

  // FlowType
  public static final String ADD_FLOW_TYPE_DETAILS = "/api/add/flowType/details";
  public static final String GET_FLOW_TYPE_DETAILS = "/api/allFlowTypeDetails";

  // Form
  public static final String ADD_FORM_TYPE = "/admin/save/formtype/details";
  public static final String ADD_FORM_SCHEMA = "/admin/save/formschema/details";
  public static final String ADD_FORM_DATA = "/admin/save/formsdata/details";
  public static final String GET_ALL_FORM_TYPE_DETAILS = "/admin/allformtypedetails";
  public static final String GET_FORM_SCHEMA_DETAILS = "/admin/formschemadetails";
  public static final String GET_FORM_TYPE_DETAILS = "/admin/formstype/details";
  public static final String GET_FORM_TYPE_DETAILS_NAME = "/admin/formstypedetails/name";
  public static final String GET_FORM_DATA_DETAILS = "/admin/dayclosedetails/branchandorg";
  public static final String GET_SAVE_DAY_CLOSE_DETAILS = "/admin/save/dayclose/details";
  public static final String GET_FORM_DATA_DETAILS_BY_BRANCH = "/admin/dayclosedetails/branch";

  // Inventory Audit
  public static final String GET_ALL_INVENTORY_AUDIT_DETAILS = "/admin/inventoryaudit/details";
  public static final String SAVE_INVENTORY_AUDIT_DETAILS = "/admin/save/inventoryaudit/details";

  // Inventory Details
  public static final String INVENTORY_CARTPRODUCT_DETAILS = "/api/inventory/cartproduct/details";
  public static final String SAVE_INVENTORY = "/api/save/inventory";
  public static final String INVENTORY_HISTORY_DETAILS = "/api/inventory/history/details";
  public static final String INVENTORY_ID = "/api/inventory/{id}";
  public static final String NOTIFICATION_INVENTORY_DETAILS =
      "/admin/inventory/notification/details";
  public static final String INVENTORY_DETAILS = "/api/inventory/details";
  public static final String REORDER_INVENTORY_DETAILS = "/api/reorder/inventory/details";
  public static final String INVENTORY_NAMES = "/api/inventory/names";
  public static final String PRODUCTCODE_DETAILS = "/api/productcode/details";
  public static final String ADD_INVENTORY_DETAILS = "/api/add/inventory/details";
  public static final String ADMIN_INVENTORY_BRANCHES = "/admin/inventory/details/branches";
  public static final String ADIMN_DATES_VENDOR_PRODUCT_REPORT =
      "/admin/dates/vendor/product/report";
  public static final String INVENTORY_REPORT_DETAILS = "/api/inventoryreports/details";
  public static final String INVENTORY_HISTORY_AVAILQUANTITY =
      "/api/inventoryhistory/availableQuantity/productId";
  public static final String INVENTORYHISTORY_INVOICEID = "/api/inventoryHistoryDetailsByInvoiceId";
  public static final String INVENTORYHISTORY_PRODUCTID = "/api/inventoryHistoryDetailsByProductId";
  public static final String INVENTORY_DETAILS_ALL = "/api/inventory/details/products";
  public static final String INVENTORY_HISTORY_DATES_VENDOR = "/api/inventoryHistory/dates/vendor";

  // Inventory Section
  public static final String INVENTORY_SECTION_BY_ORG = "/api/inventorysection/names";
  public static final String SAVE_INVENTORY_SECTION_DETAILS = "/admin/save/inventorysection";
  public static final String SAVE_INVENTORY_SECTION_DATA = "/api/save/inventorysectiondata";
  public static final String SAVE_ISSUER_DATA = "/api/save/issuer/data";
  public static final String GET_INVENTORY_SECTIONDATA_DETAILS =
      "/admin/inventorysectiondata/details";
  public static final String INVENTORYSECTIONDATA_KOTFLAG_TRUE_DETAILS =
      "/api/inventorysectiondata/kotflag/true/details";
  public static final String UPDATE_INVENTORYSECTION = "/api/update/inventorysection";
  public static final String INVENTORYSECTIONDATA_DEPARTEMENTWISE_REPORTS =
      "/api/inventorysectiondata/departmentwise/reports";
  public static final String INVENTORYSECTIONDATA_PRODUCT_REPORTS =
      "/api/inventorysectiondata/product/reports";
  public static final String INVENTORYSECTIONDATA_PRODUCTCONSOLIDATED_REPORTS =
      "/api/inventorysectiondata/productconsolidated/reports";
  public static final String TRANSFER_BILLWISE_DETAILS = "/api/transferbillwise/details";
  public static final String TRANSFER_ITEMWISE_REPORTS_DETAILS = "/api/transfer/itemwise/report";
  public static final String INVENTORYSECTION_BY_BRANCH = "/api/inventorySection/branch";
  public static final String INVENTORY_SECTION_LIST = "/api/inventorySections/list";
  public static final String TRANSFERBILL_CONSOLIDATEDREPORT =
      "/api/transferbill/consolidatedreport";
  public static final String TRASFERBILL_OUTWISE_REPORTS = "/api/transferbill/outwise/report";
  public static final String UPDATE_INVENTORY_SECTION_DETAILS =
      "/api/update/inventorysection/details";
  public static final String PRODUCT_BY_SECTIONORINVENTORY = "/api/products/bysectionorinventory";

  // Inward
  public static final String SAVE_INWARD_DETAILS = "/api/save/inwards";
  public static final String GET_ALL_INWARD_DETAILS = "/api/inward/details";
  public static final String GET_INWARD_DETAILSBY_MONTHANDYEAR = "/api/inward/details/monthandyear";

  // Item Return
  public static final String GET_PRODUCT_ITEM_RETURN = "/api/product/return";

  // KotHistory
  public static final String KOT_HISTORY_SAVE = "/admin/save/kot/History";

  // KotStatus
  public static final String KOT_STATUS_SAVE = "/admin/save/kot/status";
  public static final String KOT_STATUS_UPDATE = "/admin/update/kot/status";
  public static final String GET_ALL_KOT_STATUS_DETAILS = "/api/allKotStatus/Details";
  public static final String ALL_KOT_STATUS_DETAILS_BY_COUNTERNUMBER =
      "/api/allKotStatus/DetailsByCounterNumber";

  // LooseProduct
  public static final String LOOSE_PRODUCT_DETAILS_SAVE = "/api/looseProduct/details";
  public static final String LOOSE_PRODUCT_SAVE = "/api/looseProduct";

  // OfflineReservation
  public static final String SAVE_OFFLINE_RESERVATION_DETAILS =
      "/api/save/offline/reservation/details";
  public static final String UPDATE_OFFLINE_RESERVATION_DETAILS = "/api/offline/update/reservation";
  public static final String GET_ALL_OFFLINE_RESERVATION_DETAILS =
      "/api/offline/AllReservation/details";
  public static final String CANCLE_OFFLINE_RESERVATION_DETAILS = "/api/offline/cancel/reservation";

  // OrderFlow
  public static final String ADD_ORDER_FLOW_DETAILS = "/api/add/orderFlow/details";
  public static final String GET_ORDER_FLOW = "/api/allOrderFlow/Details";

  // OrganisationFlow
  public static final String ADD_ORG_FLOW_DETAILS = "/api/add/OrganisationFlowDetails";
  public static final String GET_ORG_FLOW_DETAILS = "/api/getOrganisationFlowDetails";

  // OrganisationMessage
  public static final String GET_ORGANISATION_FLOW_DETAILS =
      "/api/getorganisation/MessageServices/Details";
  public static final String GET_ORG_MESSAGE_BY_ID =
      "/api/update/getorganisation/MessageServices/Details";

  // OrganisationPayments
  public static final String GET_ALL_ORG_PAYMENT_DETAILS = "/admin/organisationpayment/list";
  public static final String SAVE_ORG_PAYMENT_MODES = "/admin/organisationpaymentmodes";

  // Outward
  public static final String GET_OUTWARD_DETAILS = "/api/outward/details";
  public static final String GET_INWARD_DETAILS_BY_MONTHYEAR = "/api/jsonoutward/details/monthyear";
  public static final String GET_OUTWARD_DETAILS_BY_MONTHYEAR = "/api/outward/details/monthandyear";

  // Payment
  public static final String GET_ALL_PAYMENT_DETAILS = "/api/payment/details";
  public static final String GET_ALL_PAYMENT_DETAILS_BYORG = "/api/paymentModes/list";
  public static final String SAVE_PAYMENTS_DETAILS = "/superadmin/paymentmodes/save";
  public static final String UPDATE_PAYMENTS_DETAILS = "/api/update/defaultStatus";

  // product category
  public static final String PRODUCT_CATEGORY_DETAILS = "/api/productcategory/details";
  public static final String PRODUCT_PRODUCT_CATEGORY_DETAILS =
      "/api/proudcts/productcategory/details";
  public static final String PRODUCT_CATEGORY_DETAILS_ORGBRANCHID =
      "/api/productcategory/details/orgbranchIds";
  public static final String PRODUCT_CATEGORY_FEATURE_DETAILS_ID =
      "/api/productcategory/featuresdetailsById";
  public static final String SAVE_PRODUCT_CATEGORY = "/api/save/productcategory";
  public static final String PRODUCTCATEGORY_ORGID_BRANCHID =
      "/api/productcategory/detailsByBranchIdAndOrgId";
  public static final String PRODUCT_CATEGORY_CHILD_DETAILS = "/api/productcategory/child/details";
  public static final String PRODUCTCATEGORY_CATEGORYTYPEID =
      "/api/productcategory/bycategorytypeid";
  public static final String REDEEMFLAG_TRUE_PRODUCTS = "/api/redeemflag/true/products";
  public static final String PRODUCTCATEGORY_FEEDBACK_DETAILS =
      "/api/productcategory/OnlyfeedbackCategory/details";
  public static final String PRODUCT_CATEGORY_MENUCATEGORY_DETAILS =
      "/api/productcategory/OnlyMenuCategory/details";
  public static final String CATEGORY_PRODUCTS = "/api/category/products";
  public static final String MENUCATEGORY_PRODUCTS = "/api/menucategory/products";
  public static final String CATEGORYNAME_PRODUCTS = "/api/categoryname/products";
  public static final String MENUCTEGORIES_CATEGORIES = "/api/menuCategory/categories";
  public static final String MENUCATEGORY_PRODUCT = "/api/menuCategory/products";
  public static final String MENUCATEGORY_PRODUCT_COUNT = "/api/menuCategory/products/count";
  public static final String INVENTORY_CATEGORY_PRODUCTS_COUNT =
      "/api/inventoryCategory/products/count";
  public static final String SERVICECATEGORY_PRODUCTS_COUNT = "/api/serviceCategory/products/count";
  public static final String MENUCATEGORY_PRODUCTS_PAGINATION =
      "/api/menuCategory/products/pagination/{pageNumber}/{maxRecords}";
  public static final String INVENTORYCATEGORY_CATEGORIES = "/api/inventoryCategory/categories";
  public static final String SERVICECATEGORY_CATEGORY = "/api/serviceCategory/categories";
  public static final String INVENTORYCATEGORIES_PRODUCTS = "/api/inventoryCategory/products";
  public static final String INVENTORYCATEGORY_PRODUCTS_PAGINATION =
      "/api/inventoryCategory/products/pagination/{pageNumber}/{maxRecords}";
  public static final String SERVICE_CATEGORY_PRODUCTS = "/api/serviceCategory/products";
  public static final String SERVICECATEGORY_PRODUCTS_PAGINATION =
      "/api/serviceCategory/products/pagination/{pageNumber}/{maxRecords}";
  public static final String PRRODUCTCATEGORY_BYCATEGORYTYPE =
      "/api/productcategory/bycategorytype";
  public static final String CATEGORYTYPEID_PRODUCT_CATEGORIES =
      "/api/CategoryTypeId/productCategories";
  public static final String UPDATE_PRODUCT_CATEGORY = "/api/update/produtCategory";
  public static final String OFFLINE_SERVICE_PRODUCTS = "/api/offline/Service/products";
  public static final String SERVICES_PRODUCT = "/Service/products";
  public static final String ALL_CATEGORY_ORG = "/allCategories/organsation";
  public static final String CATEGORYNAME_PRODUCTS_RECIPE = "/api/categoryname/products/recipe";
  public static final String ORG_CATEGORIES = "/org/categories/{organisation}";

  // Product
  public static final String PRODUCT_MENU_DETAILS = "/api/menu/details";
  public static final String ALL_PRODUCT_NAME_DETAILS = "/api/allProduct/name/details";
  public static final String PRODUCT_CATEGORYDETAILS = "/api/product/category/details";
  public static final String CART_PRODUCT_DETAILS = "/api/cartproduct/details";
  public static final String SAVE_PRODUCT_DETAILS = "/api/save/products";
  public static final String SAVE_PRODUCT_EXCEL = "/api/save/products/excel";
  public static final String UPDATE_PRODUCT_DETAILS = "/api/update/products/{id}";
  public static final String UPDATE_IMMAGE = "/api/update/image";
  public static final String UPDATE_PRODUCTS_DETAILS = "/api/update/products";
  public static final String SINAGLE_PRODUCT_DETAILS = "/api/singleProduct";
  public static final String PRODUCT_PRODUCTFEATURE_DETAILS_ID =
      "/api/product/productFeaturesDetails/{id}";
  public static final String ALL_PRODUCT_DETAILS = "/api/allProduct/details";
  public static final String ALL_ITEM_DETAILS = "/api/allItem/details";
  public static final String UPDATE_PRODUCT_PAGE_CODE = "/admin/updatePage/{code}";
  public static final String DELETE_PRODUCT_PAGE_CODE = "/admin/deletePage/{code}";
  public static final String GET_PRODUCT_DETAILS_BY_NAME = "/api/product/names";
  public static final String PRODUCT_BILLING_DETAILS = "/api/product/billing/details";
  public static final String PRODUCT_BILLING_PERCENTAGE_DETAILS =
      "/api/product/billing/percentages/details";
  public static final String GET_ALL_CARTPRODUCT_DETAILS_COUNTRERID =
      "/api/getAllCartProduct/details/counterId";
  public static final String GET_ALL_ORDERFLOW_DETAILS = "/api/getAllOrderFlow/details/counterId";
  public static final String GET_ALL_CARTPRODUCT_DETAILSSUMMARY =
      "/api/getAllCartProduct/detailsSummary";
  public static final String GET_ALL_ORDERFLOW_DETAILSSUMMARY =
      "/api/getAllOrderFlow/detailsSummary";
  public static final String UPDATE_CARTPRODUCT_DETAILS = "/api/updateCartProduct/details";
  public static final String UPDATE_CARTPRODUCT_BYQUANTITY_DETAILS =
      "/api/updateCartProductByQuantity/details";
  public static final String DELETE_CARTPRODUCT_DETAILS = "/api/deleteCartProduct/details";
  public static final String CARTPRODUCT_DETAILS_BY_ORGID = "/api/cartproduct/detailsbyorgId";
  public static final String UPDATE_PRODUCT_BY_HSNNUMBERANDCODE =
      "/api/update/productsByHsnNumberAndCode";
  public static final String ALL_PRODUCTDETAILS_WITHSGST = "/api/allProduct/detailsWithSgst";
  public static final String MENUCATEGORY_PRODUCT_ACTIVE = "/api/menuCategory/products/active";
  public static final String PRODUCT_LIST_ACTIVE = "/api/product/list/active";
  public static final String PRODUCT_LIST_SORTING = "/api/product/list/sorting";
  public static final String ALL_PRODUCT_OPTIONS_TYPES = "/api/allProducts/options/types";
  public static final String UPDATE_PRODUCT_VARIATIONS = "/api/update/productvariations";
  public static final String UPDATE_PRODUCT_STATUS = "/api/update/product/status";
  public static final String ORG_BRANCHES_PRODUCT_ORGANISATIONNAME =
      "/org/branches/products/{organisationName}";
  public static final String ORG_PRODUCT_ORGANISATION = "/org/products/{organisation}";
  public static final String ORG_CATEGORYPRODUCT_CATEGORYID = "/org/categoryproducts/{categoryId}";
  public static final String GET_PRODUCT_BY_PRODUCTID = "/product/{productId}";

  // Product Deals
  public static final String SAVE_PRODUCT_DEALS_DETAILS = "/api/save/deal";
  public static final String GET_ALL_DEALS_DETAILS = "/api/allDeal/Details";
  public static final String UPDATE_PRODUCT_DEALS = "/api/update/productDeals";

  // ProductPackaging
  public static final String SAVE_PRODUCT_PACKAGING_DETAILS = "/api/save/productpackging/details";
  public static final String SAVE_PRODUCTPACKAGING_DETAILS = "/api/save/productpackging";
  public static final String GET_ALL_PRODUCTPACKAGING_DETAILS = "/api/productpackaging/name";

  // ProductRecipe
  public static final String SAVE_PRODUCT_RECIPE_DETAILS = "/api/productrecipe/save";
  public static final String RECIPE_PRODUCT_SAVE = "/api/recipeproduct/save";
  public static final String GET_ALL_RECIPE_DETAILS = "/api/allproductrecipe/details";
  public static final String GET_ALL_MEASUREMENT_DETAILS = "/api/allmeasuremets/details";
  public static final String GALL_RECIPEPRODUCT_DETAILS = "/api/allrecipeproduct/details";
  public static final String ALL_INGREDIENTS_DETAILS = "/api/allingredients/details";
  public static final String GET_RECIPE_INGREDIENTS = "/api/recipe/ingredients";
  public static final String GET_RECIPE_PRODUCTS = "/api/recipe/products";
  public static final String PRODUCT_WISE_RECIPE_INGREDIENTS =
      "/api/productwise/recipe/ingredients";
  public static final String DELETE_PRODUCTRECIPE_DETAILS = "/api/deleteProductrecipe/details";

  // ProductRewardPoint
  public static final String SAVE_PRODUCT_REWARDS_POINTS_DETAILS = "/api/save/productrewardpoints";
  public static final String GET_ALL_PRODUCT_REWARDS_POINTS = "/api/allproductrewardpoints";
  public static final String GET_PRODUCTREWARDSPOINTS_BY_ORGANDBRANCH =
      "/api/productrewardpoints/organdbranch";

  // Product Variation
  public static final String SAVE_PRODUCT_VARIATION_DETAILS = "/api/save/productVariation";
  public static final String GET_ALL_PRODUCTVARIATION_DETAILS = "/api/allProductVariation/details";
  public static final String UPDATE_PRODUCT_VARIATION_DETAILS = "/api/update/productVariation";
  public static final String GET_PRODUCT_VARIATION_DETAILS = "/api/Product/variation/details";
  public static final String GET_ALLPRODUCTS_VARIATION_DETAILS =
      "/api/allProducts/variation/details";

  // Rating Controller
  public static final String SAVE_RATING_DETAILS = "/api/save/rating";
  public static final String UPDATE_RATING_DETAILS = "/api/update/Rating";
  public static final String GET_ALL_RATING_DETAILS = "/api/allRating/details";
  public static final String GET_RATING_BY_PRODUCTID = "/api/RatingByProductId/details";
  public static final String GET_RATING_BY_PRODUCTANDCUSTOMER = "/api/RatingByProAndEmp/details";

  // Reservation
  public static final String ADD_RESERVATION_DETAILS = "/api/save/reservation/details";
  public static final String UPDATE_RESERVATION_DETAILS = "/api/update/reservation";
  public static final String GET_RESERVE_DETAILS_BY_ORGBRANCH = "/api/AllReservation/details";
  public static final String GET_ALL_RESERVATIONS_FROM_CURRENT_DATE =
      "/api/allResevations/fromCurrentDate";
  public static final String ALLOT_RESERVE_DETAILS_BY_ID = "/api/allot/reservation";
  public static final String UPDATE_RESERVE_DETAILS_BY_ID = "/api/cancel/reservation";
  public static final String GET_COUNTERS_BY_TIME = "/api/restaurant/reservation/tables";
  public static final String GET_RESERVE_COUNT_BY_CURRENT_MONTH =
      "/api/Reservationscount/currentMonth";
  public static final String UPDATE_RESERVE_DETAILS_BY_WAITING = "/api/wait/reservation";
  public static final String UPDATE_RESERVE_DETAILS_BY_ALLOTED = "/api/allotted/reservation";

  // Restaurant
  public static final String GET_RESTAURANT_COUNTER_STATUS = "/api/restuarent";
  public static final String GET_POST_CART = "/api/rxcart/billing";
  public static final String EXIT_PRODUCT_DETAILS = "/api/existproducts/cart";
  public static final String ADD_PRODUCT_DETAILS = "/api/addproducts/tocart";
  public static final String GET_PRODUCT_RESTUARANT_BILLING = "/api/product/restaurant/billing";
  public static final String GET_RESERVATION_DETAILS_BY_PRINTCOUNT =
      "/api/billingdetails/printcount";
  public static final String GET_POST_CUSTOMER = "/customer/cartdata";
  public static final String GET_CART_CUSTOMER = "/customer/cartproductDetails";
  public static final String SYNC_CART_CUSTOMER = "/customer/cartproductDetails/sync";
  public static final String GET_BILL_NUMBER = "/api/bill/number";
  // Reverse Order
  public static final String SAVE_REVERSE_ORDER_DETAILS = "/api/save/reverseOrder";
  public static final String REVERSE_INVOICEID = "/api/reverse/invoiceId";
  public static final String CANCLE_INVOICEID = "/api/cancel/invoiceId";
  public static final String CANCLE_INVOICE = "/api/cancel/invoice";

  // Review
  public static final String GET_REVIEW_DETAILS = "/api/review/details";

  // Rules
  public static final String SAVE_RULES_DETAILS = "/api/save/rules/details";
  public static final String UPDATE_RULES_DETAILS = "/admin/update/rules";
  public static final String GET_RULES_DETAILS = "/api/rules/details";
  public static final String GET_ALL_RULES_DETAILS = "/api/allRules/details";
  public static final String RULES_DETAILS_BY_ORGIDANDBRANCHID = "/api/orgbranch/rules/details";

  // Secret
  public static final String SECRET_SAVE = "/admin/save/secret";
  public static final String SECRET_DETAILS_UPDATE = "/admin/update/secret/details";
  public static final String GET_ALL_SECRET_DETAILS = "/admin/secret/details";
  public static final String GET_SECRET_DETAILS = "/api/secret/details";

  // Section
  public static final String SECTION_SAVE = "/api/save/Section";
  public static final String SECTION_DETAILS = "/api/section/details";
  public static final String SECTION_COUNTER_DETAILS = "/api/section/counter/details";
  public static final String UPDATE_SECTION_DETAILS = "/api/update/section";
  public static final String DELETE_SECTION_DETAILS = "/api/section/delete";
  public static final String UPDATE_SECTION_STATUS = "/api/update/section/status";

  // StoreTemplate
  public static final String GET_STORE_TEMPLATE = "/api/allStoreTemplate/Details";
  public static final String GET_STORE_TEMPLATE_DETAILS = "/superadmin/allStoreTemplate/Details";
  public static final String GET_STORE_TEMPLATE_BY_ID = "/api/StoreTemplate/details";
  public static final String GET_STORE_TEMPLATE_DETAILS_BY_ORG =
      "/api/StoreTemplate/organisation/Details";
  public static final String GET_STORE_TYPE_DETAILS_BY_ORG =
      "/api/StoreTemplate/storetype/linkArray/list";
  public static final String GET_SUPERADMIN_DETAILS_BY_ORG = "/superadmin/getLinks/list";
  public static final String GET_ROLE_ADMIN_DETAILS_BY_ORG = "/admin/getLinks/list";
  public static final String GET_ROLE_EMPLOYEE_DETAILS_BY_ORG = "/api/getLinks/roleEmployee";
  public static final String GET_OFFLINE_ADMIN_LINKS = "/admin/offline/getLinks/list";
  public static final String GET_OFFLINE_EMPLOYEE_LINKS = "/api/offline/getEmployeeLinks/list";

  // StoreType
  public static final String STORE_TYPE_LIST = "/api/storetype/list";

  // Subscription
  public static final String GET_SUBSCRIPTION_DETAILS = "/api/subscription/list";
  public static final String GET_FREQUENCE_DETAILS = "/api/frequency/list";
  public static final String GET_ALL_SUBSCRIPTIONDETAILS_BY_CURRENTDATE_LIST =
      "/api/allsubscriptionDetails/currentDate/list";
  public static final String GET_ALL_SUBSCRIPTIONDETAILS_BY_PRODUCT_LIST =
      "/api/allsubscriptionDetails/product/list";
  public static final String GET_SUBSCRIPTIONDETAILS_BY_SERVICEPACK =
      "/api/allsubscriptionDetails/servicepack";
  public static final String GET_ALL_SUBSCRIPTION_DETAILS_BY_CURRENTDATE =
      "/api/allsubscriptionDetails/currentDate";

  // SuggestedContent
  public static final String GET_SUGGESTEDCONTENT_DETAILS = "/api/SuggestedContent/details";
  public static final String HASH_DETAILS = "/hash/details";

  // Sync
  public static final String SYNC_DETAILS_SAVE = "/api/save/sync/details";
  public static final String GET_ALL_SYNC_DETAILS = "/api/sync/details";

  // VariationOptions
  public static final String GET_ALL_VARIATION_OPTIONS = "/api/allVariationOptions";
  public static final String GET_VARIATION_OPTIONS_DEATAILS = "/api/VariationOptions/details";

  // VariationTypes
  public static final String GET_ALL_VARIATION_TYPES = "/api/allVariationTypes";
  public static final String GET_VARIATION_TYPE_OPTIONS = "/api/VariationTypes/options";

  // Wastage
  public static final String WESTAGE_DETAILS_SAVE = "/api/wastage/save";
  public static final String GET_ALL_WESTAGE_DETAILS = "/api/getAllWastages/Details";

  public static final String CUSTOMER_ORDER_STATUS = "/customer/order/status/{orderId}";
}
