package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.CustomerVendorToken;
import com.a2zbill.domain.ReOrder;
import com.a2zbill.services.CounterProductsService;
import com.a2zbill.services.CustomerVendorTokenService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerVendorTokenController {

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerVendorTokenController.class);

  @Autowired private CustomerVendorTokenService customerVendorTokenService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private CounterProductsService employeeCounterService;

  @PostMapping(path = RouteConstants.SAVE_CUSTVENDOR_TOKEN)
  public Map<String, Object> saveCustomerVendorToken(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse response) {

    final Map<String, Object> map = new HashMap<>();
    try {
      final JSONObject customerVendorToken = new JSONObject(payload);
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee empDetails = this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final String mobileNumber = customerVendorToken.getString("mobileNumber");
      final String tokens = customerVendorToken.getString("token");
      final CustomerVendorToken customerVendorDetails =
          this.customerVendorTokenService.getTokenAndRootNameBymobileNumber(
              mobileNumber, empDetails.getOrganisation().getId());
      if (customerVendorDetails != null) {
        map.put("Success", customerVendorDetails);
      } else {
        final CustomerVendorToken customerVendorTokenDetails = new CustomerVendorToken();
        customerVendorTokenDetails.setMobileNumber(mobileNumber);
        customerVendorTokenDetails.setToken(tokens);
        customerVendorTokenDetails.setRootName("vendor");
        customerVendorTokenDetails.setOrganisation(empDetails.getOrganisation());
        final Branch branch = empDetails.getBranch();
        if (branch != null) {
          customerVendorTokenDetails.setBranch(branch);
        } else {
          customerVendorTokenDetails.setBranch(null);
        }
        this.customerVendorTokenService.save(customerVendorTokenDetails);
        map.put("Success", customerVendorTokenDetails);
      }
    } catch (final Exception ex) {
      LOGGER.error("exception" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_CUSTOMER_VENDOR_TOKEN)
  public Map<String, Object> getAllDetails(
      final HttpServletRequest request, final HttpServletResponse response) {
    final Map<String, Object> customerVendorTokenStatus = new HashMap<>();

    final List<CustomerVendorToken> allCustomerVendorTokenDetails =
        this.customerVendorTokenService.getAllCustomerVendorToken();
    if (allCustomerVendorTokenDetails != null) {
      customerVendorTokenStatus.put("customerVendorTokenStatus", allCustomerVendorTokenDetails);
    } else {
      customerVendorTokenStatus.put("customerVendorTokenStatus", "this customer does n't exist");
    }
    return customerVendorTokenStatus;
  }

  @PostMapping(path = RouteConstants.CUSTOMER_VENDOR_TOKEN)
  public Map<String, Object> getProductRootName(
      final HttpServletRequest request, final HttpServletResponse response) {

    final Map<String, Object> map1 = new HashMap<>();
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee empDetails = this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final String token = request.getParameter("token");
    final String mobileNumber = request.getParameter("mobileNumber");
    final long inVoiceId = Long.parseLong(request.getParameter("invoiceId"));
    CustomerVendorToken customerVendorToken = null;
    List<ReOrder> invoiceDetails = new ArrayList<>();
    final long orgId = empDetails.getOrganisation().getId();
    try {
      customerVendorToken =
          this.customerVendorTokenService.getTokenAndRootNameBymobileNumber(mobileNumber, orgId);
      if (customerVendorToken != null) {
        if (token.equals(customerVendorToken.getToken())) {
          final char[] customerToken = (customerVendorToken.getToken()).toCharArray();
          if (customerToken[0] == 'v') {
            invoiceDetails = this.employeeCounterService.getReOrderDetailsByInvoiceId(inVoiceId);
            map1.put("customerRootName", customerVendorToken.getRootName());
            map1.put("customerVendorToken", customerVendorToken.getToken());
            map1.put("invoiceDetails", invoiceDetails);
            map1.put("Status", 200);
          } else {
            map1.put("token", "token does not match");
          }
        } else {
          map1.put("token", token + "  token does not match");
        }
      } else {
        map1.put("message", "customer does not exist");
      }
    } catch (final Exception ex) {
      map1.put("message", "customer does not exist");
    }
    return map1;
  }
}
