package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Frequency;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.Subscription;
import com.a2zbill.services.FrequencyService;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.SubscriptionService;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubscriptionController {

  private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private ProductService productService;

  @Autowired private FrequencyService frequencyService;

  @Autowired private SubscriptionService subscriptionService;

  @GetMapping(path = RouteConstants.GET_SUBSCRIPTION_DETAILS)
  public final ResponseEntity<Object> getSubscriptionDetails() {

    List<Subscription> subscriptionDetails = new ArrayList<Subscription>();

    try {
      subscriptionDetails = this.subscriptionService.getAllSubscriptions();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getSubscriptionDetails" + ex);
    }
    return new ResponseEntity<>(subscriptionDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_FREQUENCE_DETAILS)
  public final ResponseEntity<Object> getFrequencyDetails() {

    List<Frequency> frequencyDetails = new ArrayList<Frequency>();

    try {
      frequencyDetails = this.frequencyService.getAllFrequencys();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getfrequencyDetails" + ex);
    }
    return new ResponseEntity<>(frequencyDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_ALL_SUBSCRIPTIONDETAILS_BY_CURRENTDATE_LIST)
  public final ResponseEntity<Object> getSubscriptionDetailsByDate(
      final HttpServletRequest request, final HttpServletResponse responce)
      throws Exception, ParseException {

    final List<Map<String, Object>> products = new ArrayList<>();

    List<Subscription> subscriptions = new ArrayList<Subscription>();

    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        final String dateString = request.getParameter("date");
        final Long productId = Long.parseLong(request.getParameter(Constants.PRODUCTID));
        final Long employeeId = Long.parseLong(request.getParameter(Constants.EMPLOYEEID));

        final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
        final Date date = sdf.parse(dateString);
        if (employeeId == 0l && productId == 0l) {

          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgId(date, orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgIdAndbranchId(
                    date, orgId, branchId);
          }

        } else if (employeeId != 0l && productId == 0l) {
          if (branchId == 0) {

            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndEmployeeIdAndorgId(
                    date, employeeId, orgId);
          } else {

            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndEmployeeIdAndOrgIdAndbranchId(
                        date, employeeId, orgId, branchId);
          }
        } else if (productId != 0l && employeeId == 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndproductIdAndOrgId(
                    date, productId, orgId);
          } else {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
                        date, productId, orgId, branchId);
          }

        } else if (employeeId != 0l && productId != 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgId(
                        date, employeeId, productId, orgId);
          } else {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgIdAndBranchId(
                        date, employeeId, productId, orgId, branchId);
          }
        }

        for (final Subscription subscription : subscriptions) {

          final List<Map<String, Object>> mapdata = new ArrayList<>();
          final Map<String, Object> subscriptionData = new HashMap<>();

          subscriptionData.put(Constants.SUBSCRIPTIONID, subscription.getId());
          subscriptionData.put(Constants.EMPLOYEEID, subscription.getEmployee());
          subscriptionData.put(Constants.CUSTOMER, subscription.getCustomer());
          subscriptionData.put(Constants.AUTORENWEWAL, subscription.isAutoRenewal());
          subscriptionData.put(Constants.PRODUCT, subscription.getProduct());

          final Map<String, Object> productPack = subscription.getProduct().getProductPackaging();

          if (productPack != null) {

            @SuppressWarnings("unchecked")
            final List<Integer> list = (List<Integer>) productPack.get(Constants.PACKAGEPRODUCTS);
            for (final Integer id : list) {
              final Product product = productService.getProductsById(id.longValue());
              final Map<String, Object> packagingData = new HashMap<>();
              packagingData.put(Constants.PACKAGEPRODUCTS, product);
              mapdata.add(packagingData);
            }
            subscriptionData.put(Constants.CHILDRENS, mapdata);
            products.add(subscriptionData);
          }
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }

    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getSubscriptionDetails " + e);
    }

    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @SuppressWarnings("unchecked")
  @GetMapping(path = RouteConstants.GET_ALL_SUBSCRIPTIONDETAILS_BY_PRODUCT_LIST)
  public final ResponseEntity<Object> getSubscriptionDetailsByProducts(
      final HttpServletRequest request, final HttpServletResponse responce)
      throws Exception, ParseException {
    final List<Map<String, Object>> products = new ArrayList<>();
    final String dateString = request.getParameter("date");
    final Long productId = Long.parseLong(request.getParameter(Constants.PRODUCTID));
    final Long employeeId = Long.parseLong(request.getParameter(Constants.EMPLOYEEID));

    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);

    Date date = null;

    try {
      if (dateString.equals("0")) {
        //
      } else {
        date = sdf.parse(dateString);
      }
    } catch (final ParseException ex) {
      LOGGER.info("context" + ex, ex);
    }

    List<Subscription> subscriptions = null;

    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        if (date == null && employeeId == 0l && productId == 0l) {

          if (branchId == 0) {
            subscriptions = this.subscriptionService.getSubscriptionDetailsByOrgId(orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByOrgIdAndBranchId(orgId, branchId);
          }
        } else if (date == null && employeeId == 0l && productId != 0l) {
          if (branchId == 0) {

            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgId(
                    productId, orgId);
          } else {

            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
                    productId, orgId, branchId);
          }
        } else if (date == null && productId != 0l && employeeId == 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgId(
                    productId, orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
                    productId, orgId, branchId);
          }

        } else if (date == null && employeeId != 0l && productId != 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgId(
                    employeeId, productId, orgId);
          } else {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgIdAndbranchId(
                        employeeId, productId, orgId, branchId);
          }

        } else if (date != null && employeeId == 0l && productId == 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgId(date, orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgIdAndbranchId(
                    date, orgId, branchId);
          }

        } else if (date != null && employeeId == 0l && productId != 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndproductIdAndOrgId(
                    date, productId, orgId);
          } else {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
                        date, productId, orgId, branchId);
          }

        } else if (date != null && employeeId != 0l && productId == 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndEmployeeIdAndorgId(
                    date, employeeId, orgId);
          } else {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndEmployeeIdAndOrgIdAndbranchId(
                        date, employeeId, orgId, branchId);
          }

        } else if (date != null && employeeId != 0l && productId != 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgId(
                        date, employeeId, productId, orgId);
          } else {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgIdAndBranchId(
                        date, employeeId, productId, orgId, branchId);
          }
        }

        for (final Subscription subscription : subscriptions) {

          final List<Map<String, Object>> mapdata = new ArrayList<>();
          final Map<String, Object> subscriptionData = new HashMap<>();

          subscriptionData.put(Constants.SUBSCRIPTIONID, subscription.getId());
          subscriptionData.put(Constants.EMPLOYEEID, subscription.getEmployee());
          subscriptionData.put(Constants.CUSTOMER, subscription.getCustomer());
          subscriptionData.put(Constants.AUTORENWEWAL, subscription.isAutoRenewal());
          subscriptionData.put(Constants.PRODUCT, subscription.getProduct());

          final Map<String, Object> productPack = subscription.getProduct().getProductPackaging();

          if (productPack != null) {

            final List<Integer> list = (List<Integer>) productPack.get(Constants.PACKAGEPRODUCTS);
            for (final Integer id : list) {
              final Product product = productService.getProductsById(id.longValue());
              final Map<String, Object> packagingData = new HashMap<>();
              packagingData.put(Constants.PACKAGEPRODUCTS, product);

              mapdata.add(packagingData);
            }
            subscriptionData.put(Constants.CHILDRENS, mapdata);
            products.add(subscriptionData);
          }
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getSubscriptionDetails " + e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @SuppressWarnings("unchecked")
  @GetMapping(path = RouteConstants.GET_SUBSCRIPTIONDETAILS_BY_SERVICEPACK)
  public final ResponseEntity<Object> getSubscriptionDetailsByProductsByServicepack(
      final HttpServletRequest request, final HttpServletResponse responce)
      throws Exception, ParseException {
    final Map<String, Long> globalMap = new HashMap<>();

    final String dateString = request.getParameter("date");
    final Long productId = Long.parseLong(request.getParameter(Constants.PRODUCTID));

    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);

    Date date = null;

    try {
      if (dateString.equals("0")) {

      } else {
        date = sdf.parse(dateString);
      }
    } catch (final ParseException ex) {
      LOGGER.error("context" + ex, ex);
    }
    List<Subscription> subscriptions = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        if (date == null && productId == 0l) {

          if (branchId == 0) {
            subscriptions = this.subscriptionService.getSubscriptionDetailsByOrgId(orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByOrgIdAndBranchId(orgId, branchId);
          }

        } else if (date == null && productId != 0l) {
          if (branchId == 0) {

            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgId(
                    productId, orgId);
          } else {

            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
                    productId, orgId, branchId);
          }
        } else if (date != null && productId == 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgId(date, orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgIdAndbranchId(
                    date, orgId, branchId);
          }

        } else if (date != null && productId != 0l) {
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndproductIdAndOrgId(
                    date, productId, orgId);
          } else {
            subscriptions =
                this.subscriptionService
                    .getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
                        date, productId, orgId, branchId);
          }
        }

        for (final Subscription subscription : subscriptions) {

          final Map<String, Object> subscriptionData = new HashMap<>();

          subscriptionData.put(Constants.SUBSCRIPTIONID, subscription.getId());
          subscriptionData.put(Constants.EMPLOYEEID, subscription.getEmployee());
          subscriptionData.put(Constants.CUSTOMER, subscription.getCustomer());
          subscriptionData.put(Constants.AUTORENWEWAL, subscription.isAutoRenewal());
          subscriptionData.put(Constants.PRODUCT, subscription.getProduct());

          final Map<String, Object> productPack = subscription.getProduct().getProductPackaging();

          if (productPack != null) {

            final List<Integer> list = (List<Integer>) productPack.get(Constants.PACKAGEPRODUCTS);
            for (final Integer id : list) {
              final Product product = productService.getProductsById(id.longValue());

              if (globalMap.containsKey(product.getProductName())) {

                final long value = globalMap.get(product.getProductName());
                globalMap.put(product.getProductName(), value + 1);

              } else {

                globalMap.put(product.getProductName(), 1l);
              }
            }
          }
        }
      } else {
        return new ResponseEntity<>(globalMap, HttpStatus.UNAUTHORIZED);
      }

    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getSubscriptionDetails " + e);
    }

    return new ResponseEntity<>(globalMap, HttpStatus.OK);
  }

  @SuppressWarnings("unchecked")
  @GetMapping(path = RouteConstants.GET_ALL_SUBSCRIPTION_DETAILS_BY_CURRENTDATE)
  public final ResponseEntity<Object> getSubscriptionDetailsBycurrentDate(
      final HttpServletRequest request, final HttpServletResponse responce)
      throws Exception, ParseException {
    final List<Map<String, Object>> products = new ArrayList<>();
    final String dateString = request.getParameter("date");
    final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SYSDATE);
    Date date = null;
    List<Subscription> subscriptions = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        if (dateString.equals("")) {

          if (branchId == 0) {
            subscriptions = this.subscriptionService.getSubscriptionDetailsByOrgId(orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByOrgIdAndBranchId(orgId, branchId);
          }
        } else {
          date = sdf.parse(dateString);
          if (branchId == 0) {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgId(date, orgId);
          } else {
            subscriptions =
                this.subscriptionService.getSubscriptionDetailsByDateAndOrgIdAndbranchId(
                    date, orgId, branchId);
          }
        }

        for (final Subscription subscription : subscriptions) {

          final List<Map<String, Object>> mapdata = new ArrayList<>();
          final Map<String, Object> subscriptionData = new HashMap<>();

          subscriptionData.put(Constants.SUBSCRIPTIONID, subscription.getId());
          subscriptionData.put(Constants.EMPLOYEEID, subscription.getEmployee());
          subscriptionData.put(Constants.CUSTOMER, subscription.getCustomer());
          subscriptionData.put(Constants.AUTORENWEWAL, subscription.isAutoRenewal());
          subscriptionData.put(Constants.PRODUCT, subscription.getProduct());

          final Map<String, Object> productPack = subscription.getProduct().getProductPackaging();

          if (productPack != null) {

            final List<Integer> list = (List<Integer>) productPack.get(Constants.PACKAGEPRODUCTS);
            for (final Integer id : list) {
              final Product product = productService.getProductsById(id.longValue());
              final Map<String, Object> packagingData = new HashMap<>();
              packagingData.put(Constants.PACKAGEPRODUCTS, product);
              mapdata.add(packagingData);
            }
            subscriptionData.put(Constants.CHILDRENS, mapdata);
            products.add(subscriptionData);
          }
        }
      } else {
        return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getSubscriptionDetails " + e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }
}
