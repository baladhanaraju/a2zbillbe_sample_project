package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Inward;
import com.a2zbill.services.InwardService;
import com.itextpdf.text.Chunk;
import com.tsss.basic.domain.Vendor;
import com.tsss.basic.service.VendorService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InwardController {

  private static final Logger LOGGER = LoggerFactory.getLogger(InwardController.class);

  public static final Chunk NEWLINE = new Chunk("\n");

  @Autowired private InwardService inwardService;

  @Autowired private VendorService vendorService;

  @Value("${outward.form}")
  private String gstrOutwardForm;

  @PostMapping(path = RouteConstants.SAVE_INWARD_DETAILS)
  public final void saveInwards(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws ParseException, JSONException {

    final JSONObject products = new JSONObject(payload);
    final Vendor vendor =
        this.vendorService.getVendorDetailsByVendorName(products.getString("vendor"));
    final Inward inwards = new Inward();

    final String startDate = request.getParameter("date");
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    final Date date = sdf.parse(startDate);

    final Calendar cal = Calendar.getInstance();
    cal.setTime(date);

    final String month = Integer.toString(cal.get(Calendar.MONTH) + 1);

    final String year = Integer.toString(cal.get(Calendar.YEAR));

    try {
      inwards.setBillingid(products.getLong("billId"));
      inwards.setBillingamount(new BigDecimal(products.getString("billingAmount")));
      inwards.setVendorid(vendor);
      inwards.setGst_number(vendor.getGstNumber());
      inwards.setDate(date);
      inwards.setCgst_amount(new BigDecimal(products.getString("cgstAmount")));
      inwards.setSgst_amount(new BigDecimal(products.getString("sgstAmount")));
      inwards.setIgst_amount(new BigDecimal(products.getString("igstAmount")));
      inwards.setMonth(month);
      inwards.setYear(year);

      this.inwardService.save(inwards);

    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInvwordsDetail " + ex);
    }
  }

  @GetMapping(path = RouteConstants.GET_ALL_INWARD_DETAILS)
  public final ResponseEntity<Object> getInwardDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Inward> inwardDetails = new ArrayList<Inward>();
    try {
      inwardDetails = this.inwardService.getAllInwards();

    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInwardDetail " + ex);
    }
    return new ResponseEntity<>(inwardDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_INWARD_DETAILSBY_MONTHANDYEAR)
  public final ResponseEntity<Object> getInwardDetailsbymonthandyear(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Inward> inwardDetails = new ArrayList<Inward>();

    try {
      final String month = request.getParameter("month");
      final String year = request.getParameter("year");

      inwardDetails = this.inwardService.getInwardDetailsByMonthAndYear(month, year);
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getInwardDetail " + ex);
    }
    return new ResponseEntity<>(inwardDetails, HttpStatus.OK);
  }
}
