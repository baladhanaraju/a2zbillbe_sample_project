package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductDeals;
import com.a2zbill.services.ProductDealsService;
import com.a2zbill.services.ProductService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductDealsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductDealsController.class);

  @Autowired private ProductService productsService;

  @Autowired private ProductDealsService productDealsService;

  @PostMapping(path = RouteConstants.SAVE_PRODUCT_DEALS_DETAILS)
  public final Map<String, Object> addDeals(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {

    final Map<String, Object> map = new HashMap<>();
    final ProductDeals deals = new ProductDeals();
    try {
      final JSONObject deal = new JSONObject(payload);

      final List<ProductDeals> allDeals = this.productDealsService.getAllProductDeals();
      final String offerCode = deal.getString("offerCode");
      if (allDeals != null) {
        for (ProductDeals arrayDeals : allDeals) {
          if (arrayDeals.getOfferCode().equalsIgnoreCase(offerCode)) {
            final String message = " Deal already existed ";
            map.put("message", message);
            return map;
          }
        }
      }
      deals.setOfferCode(offerCode);
      deals.setProductCode(deal.getString("productCode"));
      deals.setOfferTitle(deal.getString("offerTitle"));
      deals.setStatus(deal.getBoolean("status"));
      deals.setFlatDiscount(deal.getDouble(Constants.FLATEDISCOUNT));
      deals.setCreatedDate(new Date(System.currentTimeMillis()));
      deals.setEndDate(new Date(System.currentTimeMillis()));
      final Product productId =
          this.productsService.getProductsByProductCode(deal.getString("productId"));
      deals.setProductId(productId);
      map.put("Deals", deals);
      this.productDealsService.save(deals);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "DealsController in the addDeals() of" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_ALL_DEALS_DETAILS)
  public final ResponseEntity<Object> getDeals() {
    List<ProductDeals> dealsDetails = new ArrayList<ProductDeals>();
    try {
      dealsDetails = this.productDealsService.getAllProductDeals();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getDealDetails " + ex);
    }
    return new ResponseEntity<>(dealsDetails, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCT_DEALS)
  public final ProductDeals updateProductDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {

    ProductDeals productDeals = null;

    try {
      final JSONObject product = new JSONObject(payload);
      final Long id = product.getLong("id");
      final String offerCode = product.getString("offerCode");
      productDeals = this.productDealsService.getProductDealsByIdAndOfferCode(id, offerCode);

      productDeals.setProductCode(product.getString("productCode"));
      productDeals.setOfferTitle(product.getString("offerTitle"));
      productDeals.setFlatDiscount(product.getDouble(Constants.FLATEDISCOUNT));
      productDeals.setFlatDiscount(product.getDouble(Constants.FLATEDISCOUNT));
      productDeals.setStatus(product.getBoolean("status"));
      this.productDealsService.update(productDeals);
    } catch (final Exception ex) {
      LOGGER.error("getproductsDetails" + ex);
    }
    return productDeals;
  }
}
