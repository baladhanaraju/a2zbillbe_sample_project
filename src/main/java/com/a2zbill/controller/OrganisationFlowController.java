package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Flows;
import com.a2zbill.domain.OrganisationFlow;
import com.a2zbill.services.FlowsService;
import com.a2zbill.services.OrganisationFlowService;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.service.OrganisationService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrganisationFlowController {

  private static final Logger LOGGER = LoggerFactory.getLogger(OrganisationFlowController.class);

  @Autowired private FlowsService flowsService;

  @Autowired private OrganisationFlowService organisationFlowService;

  @Autowired private OrganisationService organisationService;

  @PostMapping(path = RouteConstants.ADD_ORG_FLOW_DETAILS)
  public final OrganisationFlow addOrganisationFlowDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload) {

    final OrganisationFlow organisationFlowDetails = new OrganisationFlow();
    try {
      final JSONObject organisationFlow = new JSONObject(payload);
      final Flows flows = flowsService.getFlowsById(organisationFlow.getLong("flowId"));
      final Organisation orgDetails =
          organisationService.getOrganisationById(organisationFlow.getLong("orgId"));
      organisationFlowDetails.setFlows(flows);
      organisationFlowDetails.setOrganisation(orgDetails);
      organisationFlowService.save(organisationFlowDetails);

    } catch (final Exception e) {
      LOGGER.error("addFlowTypeDetails" + e);
    }
    return organisationFlowDetails;
  }

  @GetMapping(path = RouteConstants.GET_ORG_FLOW_DETAILS)
  public final ResponseEntity<Object> getOrganisationFlowDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<OrganisationFlow> organisationFlowDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        organisationFlowDetails = organisationFlowService.getOrganisationFlowByOrgId(orgId);
      } else {
        return new ResponseEntity<>(organisationFlowDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.info("getFlowTypeDetails " + e);
    }
    return new ResponseEntity<>(organisationFlowDetails, HttpStatus.OK);
  }
}
