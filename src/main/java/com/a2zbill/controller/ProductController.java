package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingDetail;
import com.a2zbill.domain.Cart;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.Category;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.Flow;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.InventoryHistory;
import com.a2zbill.domain.Item;
import com.a2zbill.domain.LooseProduct;
import com.a2zbill.domain.OrderFlow;
import com.a2zbill.domain.Outward;
import com.a2zbill.domain.Percentage;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductCategory;
import com.a2zbill.domain.Retailer;
import com.a2zbill.domain.VariationTypes;
import com.a2zbill.services.BillingDetailService;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CartProductService;
import com.a2zbill.services.CartService;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.FlowService;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.LooseProductService;
import com.a2zbill.services.OrderFlowService;
import com.a2zbill.services.OutwardService;
import com.a2zbill.services.PercentageService;
import com.a2zbill.services.ProductCategoryService;
import com.a2zbill.services.RetailerService;
import com.a2zbill.services.VariationTypesService;
import com.a2zbill.services.impl.CategoryServiceImpl;
import com.a2zbill.services.impl.ItemServiceImpl;
import com.a2zbill.services.impl.ProductServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BasicTemplateServiceException;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.CategoryTypeService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

  @Autowired private PercentageService percentageService;

  @Autowired private LooseProductService looseProductService;

  @Autowired private BillingDetailService billingDetailsService;

  @Autowired private OrderFlowService orderFlowService;

  @Autowired private BranchService branchService;

  @Autowired private OrganisationService organisationService;

  @Autowired private InventoryDetailService inventoryDetailService;

  @Autowired private CartService cartService;

  @Autowired private BillingService billingService;

  @Autowired private CounterService counterService;

  @Autowired private ProductServiceImpl productsService;

  @Autowired private CartProductService cartProductService;

  @Autowired private CategoryServiceImpl categoryService;

  @Autowired private ItemServiceImpl itemsService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private ProductCategoryService productCategoryService;

  @Autowired private OutwardService outwardService;

  @Autowired private RetailerService retailerService;

  @Autowired private FlowService flowService;

  @Autowired private VariationTypesService variationTypeService;

  @Autowired private CategoryTypeService categoryTypeService;

  @GetMapping(path = RouteConstants.PRODUCT_MENU_DETAILS)
  public final ResponseEntity<Object> getMenuProduct(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final List<Map<String, Object>> menuDetails = new ArrayList<>();
    List<ProductCategory> productCategories = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId != 0) {
          productCategories =
              this.productCategoryService.getProductCategorysByOrgAndBranchId(branchId, orgId);
        } else {
          productCategories = this.productCategoryService.getProductCategorys(orgId);
        }
        for (final ProductCategory productCategory : productCategories) {
          final Map<String, Object> productList = new HashMap<>();

          final List<Map<String, Object>> product =
              this.productsService.getProductNameAndIdsByProductCategoryId(productCategory.getId());

          if (product.isEmpty()) {
          } else {
            productList.put("categoryName", productCategory.getProductTypeName());
            productList.put("products", product);
            menuDetails.add(productList);
          }
        }
      } else {
        return new ResponseEntity<>(menuDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getmenuDetails " + ex);
    }
    return new ResponseEntity<>(menuDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ALL_PRODUCT_NAME_DETAILS)
  public final ResponseEntity<Object> getproductNameDetails(final HttpServletResponse response)
      throws Exception {
    List<Product> productDetails = new ArrayList<Product>();

    try {
      productDetails = this.productsService.getAllProductsName();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getProductDetails" + ex);
    }

    return new ResponseEntity<>(productDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_CATEGORYDETAILS)
  public final ResponseEntity<Object> getDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Category> categoryDetails = new ArrayList<Category>();

    try {
      categoryDetails = this.categoryService.getAllCategories();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getCategoryDetails" + ex);
    }
    return new ResponseEntity<>(categoryDetails, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.CART_PRODUCT_DETAILS)
  public final Map<String, Object> getproductDetails(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws JSONException {

    Product productDetails = null;
    Item itemDetails = null;
    final Map<String, Object> productDetails1 = new HashMap<>();
    InventoryDetail inventoryDetail = null;
    LooseProduct looseProduct = null;
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

      final JSONObject jsonObject = new JSONObject(payload);
      final String QTY = "1";
      final String code = jsonObject.getString("productcode");
      if (code.contains(Constants.LOOSEPRODUCTCODE)) {
        final String[] codes = code.split(Constants.LOOSEPRODUCTCODE);
        final String loosecode = codes[1];
        looseProduct = this.looseProductService.getLooseProductDetailsByCode(loosecode);
      } else {
        productDetails = this.productsService.getProductsByProductCode(code);
        final Branch branch = employeeDetails.getBranch();
        if (branch != null) {
          inventoryDetail =
              this.inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                  productDetails.getId(), branch.getId());
        } else {
          inventoryDetail =
              this.inventoryDetailService.getInventoryDetailsByProductIdAndOrgId(
                  productDetails.getId(), employeeDetails.getOrganisation().getId());
        }

        // if condition for if quantity is null then returns products
        // not available
        if (inventoryDetail.getQuantity().doubleValue() <= 0) {
          productDetails1.put(Constants.PRODUCT, Constants.PRODUCTNOTAVAILABLE);
          return productDetails1;
        }
      }
      if (productDetails != null || looseProduct != null) {
        itemDetails = this.itemsService.getItemsByHsnNo(productDetails.getHsnNumber());

        productDetails1.put("ProductCode", productDetails.getCode());
        productDetails1.put("ProductName", productDetails.getProductName());
        productDetails1.put("HsnNumber", productDetails.getHsnNumber());
        productDetails1.put("ProductPrice", productDetails.getProductPrice());

        productDetails1.put("Quantity", QTY);
        productDetails1.put("SGST", itemDetails.getPercentageDetails().getCGST());
        productDetails1.put("CGST", itemDetails.getPercentageDetails().getSGST());
        productDetails1.put("IGST", itemDetails.getPercentageDetails().getIGST());

      } else {
        productDetails1.put(Constants.PRODUCT, Constants.PRODUCTNOTAVAILABLE);
      }

    } catch (final Exception ex) {
      LOGGER.error("savecartProductDetails" + ex);
    }
    return productDetails1;
  }

  @PostMapping(path = RouteConstants.SAVE_PRODUCT_DETAILS)
  public final Map<String, String> addProductvaritionDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws Exception {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    final Map<String, String> message = new HashMap<>();
    final JSONObject product = new JSONObject(productDetails);
    final Product products = new Product();
    final Map<String, Object> map = new HashMap<>();
    final String branchId = product.getString("branchId");
    final Branch branch = branchService.getBranchById(Long.parseLong(branchId));

    final String hsnNumber = product.getString(Constants.HSNNUMBERS);
    final String productCode = product.getString(Constants.PRODUCTCODE);
    final String productName = product.getString(Constants.PRODUCTNAME);
    // product.getString(Constants.branchId);
    // final Branch branch = branchService.getBranchById(Long.parseLong(Constants.branchId));
    final String measurement = product.getString(Constants.MEASUREMENT);
    final String measurement1 = product.getString(Constants.MEASUREMENT1);
    final String measurement2 = product.getString(Constants.MEASUREMENT2);
    final String image = product.getString(Constants.IMAGE);
    map.put("productFeatures", product.getString("productFeatures"));

    final Product productCodeDetails =
        this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
            productCode, orgId, Long.parseLong(branchId));
    if (productCodeDetails != null) {
      if (productCodeDetails.getCode().equalsIgnoreCase(productCode) && !(productCode.isEmpty())) {
        message.put(Constants.PRODUCTMESSAGE, "ProductCode already exists!");
        return message;
      } else if (productCodeDetails.getCode().equalsIgnoreCase(productCode)
          && productCode.isEmpty()) {
        products.setCode("");
      }
    } // productCodeDetails not null
    else {
      products.setCode(productCode);
    }

    final Product productDetail =
        this.productsService.getProductsByProductNameAndOrgAndBranch(
            productName, orgId, Long.parseLong(branchId));
    if (productDetail != null) {
      message.put(Constants.PRODUCTMESSAGE, "ProductName already exists!!");
    } else {
      products.setProductName(productName);
      try {

        products.setProductFeatures(map);
        products.setImage(image);
        if (branch == null) {
          products.setBranch(null);
        } else {
          products.setBranch(branch);
        }
        products.setHsnNumber(hsnNumber);
        products.setStatus(Constants.STATUSTRUE);
        products.setProductPrice(BigDecimal.valueOf(product.getDouble("productPrice")));
        products.setOrganisation(employeeDetails.getOrganisation());
        products.setPackageFlag(product.getBoolean("packageFlag"));
        products.setIncludingTaxFlag(product.getString("includingTaxFlag"));
        products.setCGST(BigDecimal.valueOf(product.getDouble("cgst")));
        products.setSGST(BigDecimal.valueOf(product.getDouble("sgst")));
        products.setIGST(BigDecimal.valueOf(product.getDouble("igst")));
        products.setVegFlag(product.getBoolean(Constants.VEGFLAG));

        final ProductCategory productCategoryId =
            this.productCategoryService.getProductCategoryById(
                Long.parseLong(product.getString("productCategory")));
        products.setProductCategory(productCategoryId);

        if (product.has(Constants.VARIATIONTYPEID)) {

          if (product.getString(Constants.VARIATIONTYPEID).equals("")) {
            final VariationTypes variationType =
                variationTypeService.getVariationTypesById(Long.parseLong("1"));
            products.setVariationType(variationType);
            if (variationType.getDisplayName().equals(Constants.DEFALUT)) {
              products.setMeasurement("1");
            } // if measurement
          } // if has variationtype
          else {
            final VariationTypes variationTypes =
                variationTypeService.getVariationTypesById(
                    Long.parseLong((product.getString(Constants.VARIATIONTYPEID))));
            products.setVariationType(variationTypes);
            if (measurement.equals("")) {
              message.put(Constants.PRODUCTMESSAGE, Constants.MEASUREMENTSELECT);
              return message;
            } // measurement empty
            else {
              products.setMeasurement(measurement);
            }
          }
        } else {
          final VariationTypes variationTypes =
              variationTypeService.getVariationTypesById(Long.parseLong("1"));
          products.setVariationType(variationTypes);
          if (variationTypes.getDisplayName().equals(Constants.DEFALUT)) {
            products.setMeasurement("1");
          } // if measurement
          else {
            products.setMeasurement(measurement);
          }
        } // variation type id
        if (product.has(Constants.VARIATIONTYPEID1)) {

          if (product.getString(Constants.VARIATIONTYPEID1).equals("")) {
            final VariationTypes variationType =
                variationTypeService.getVariationTypesById(Long.parseLong("1"));
            products.setVariationType1(variationType);
            if (variationType.getDisplayName().equals(Constants.DEFALUT)) {
              products.setMeasurement1("1");
            } // if measurement
          } // if has variationtype
          else {
            final VariationTypes variationTypes =
                variationTypeService.getVariationTypesById(
                    Long.parseLong((product.getString(Constants.VARIATIONTYPEID1))));
            products.setVariationType1(variationTypes);
            if (measurement1.equals("")) {
              message.put(Constants.PRODUCTMESSAGE, Constants.MEASUREMENTSELECT);
              return message;
            } // measurement empty
            else {
              products.setMeasurement1(measurement1);
            } // if m not empty
          }
        } else {
          final VariationTypes variationTypes =
              variationTypeService.getVariationTypesById(Long.parseLong("1"));
          products.setVariationType1(variationTypes);
          if (variationTypes.getDisplayName().equals(Constants.DEFALUT)) {
            products.setMeasurement1("1");
          } // if measurement
        } // variation type id1
        if (product.has(Constants.VARIATIONTYPEID2)) {

          if (product.getString(Constants.VARIATIONTYPEID2).equals("")) {
            final VariationTypes variationType =
                variationTypeService.getVariationTypesById(Long.parseLong("1"));
            products.setVariationType2(variationType);
            if (variationType.getDisplayName().equals(Constants.DEFALUT)) {
              products.setMeasurement2("1");
            } // if measurement
          } // if has variationtype
          else {
            final VariationTypes variationTypes =
                variationTypeService.getVariationTypesById(
                    Long.parseLong((product.getString(Constants.VARIATIONTYPEID2))));
            products.setVariationType2(variationTypes);
            if (measurement2.equals("")) {
              message.put(Constants.PRODUCTMESSAGE, Constants.MEASUREMENTSELECT);
              return message;
            } // measurement empty
            else {
              products.setMeasurement2(measurement2);
            }
          }
        } else {
          final VariationTypes variationTypes =
              variationTypeService.getVariationTypesById(Long.parseLong("1"));
          products.setVariationType2(variationTypes);
          if (variationTypes.getDisplayName().equals(Constants.DEFALUT)) {
            products.setMeasurement2("1");
          } // if measurement
        } // variation type i
        final VariationTypes variationTypes1 =
            variationTypeService.getVariationTypesById(Long.parseLong("1"));
        products.setVariationType1(variationTypes1);
        final VariationTypes variationTypes2 =
            variationTypeService.getVariationTypesById(Long.parseLong("1"));
        products.setVariationType2(variationTypes2);
        if (product.has("parentId")) {
          final String parentId = product.getString("parentId");
          if (parentId.equals("")) {
            products.setParentId(0l);
          } else {
            products.setParentId(Long.parseLong(parentId));
          }
        } else {
          products.setParentId(0l);
        }
        this.productsService.save(products);
        message.put(Constants.PRODUCTMESSAGE, "Product Successfully Saved");
        return message;
      } catch (final Exception e) {
        message.put(Constants.PRODUCTMESSAGE, "Product is not added, Please enter valid data.");
        LOGGER.error(Constants.SAVEPRODUCTDETAILS + e, e);
        return message;
      }
    }
    return message;
  }

  @PostMapping(path = RouteConstants.SAVE_PRODUCT_EXCEL)
  public final Map<String, Object> addProductDetailsByarrayType(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws JSONException {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();

    final Map<String, Object> message = new HashMap<>();

    final List<String> list1 = new ArrayList<>();

    final JSONObject json = new JSONObject(productDetails);

    final JSONObject prod = json.getJSONObject("products");

    final long branchId = json.getLong("branchId");
    final List<String> productCodeList = new ArrayList<>();
    final List<String> productNameList = new ArrayList<>();
    final List<String> productNameList1 = new ArrayList<>();
    final List<String> invalidDataList1 = new ArrayList<>();
    final JSONArray jsonarray = prod.getJSONArray("Sheet1");
    int count = 0;
    for (int arraySize = 0; arraySize < jsonarray.length(); arraySize++) {
      final JSONObject product = jsonarray.getJSONObject(arraySize);
      final Product products = new Product();
      final Map<String, Object> map = new HashMap<>();
      final String productName = product.getString("productName");
      final Branch branch = branchService.getBranchById(branchId);

      if (product.has("code")) {
        final String productCode = product.getString("code");
        final Product productCodeDetail =
            this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                productCode, orgId, branchId);
        if (productCodeDetail != null) {
          if (productCodeDetail.getCode().equalsIgnoreCase(productCode)
              && !(productCode.isEmpty())) {
            final String codeMessage = productCodeDetail.getCode();
            productCodeList.add(codeMessage);
          } else if (productCodeDetail.getCode().equalsIgnoreCase(productCode)
              && productCode.isEmpty()) {
            products.setCode("");
          }
        } // productCodeDetail not null
        else {
          products.setCode("");
        }
      } else {
        products.setCode("");
      }

      final Product productDetail =
          this.productsService.getProductsByProductNameAndOrgAndBranch(
              productName, orgId, branchId);
      if (productDetail != null) {

        final String nameMessage = productName;
        productNameList.add(nameMessage);
      } else {
        final String nameMessage1 = productName;
        productNameList1.add(nameMessage1);
        products.setProductName(productName);
        try {

          products.setProductFeatures(map);
          products.setImage(null);
          if (branch == null) {
            products.setBranch(null);
          } else {
            products.setBranch(branch);
          }
          products.setCode(product.getString("code"));
          products.setProductPrice(BigDecimal.valueOf(product.getDouble("productPrice")));
          products.setOrganisation(employeeDetails.getOrganisation());
          products.setCGST(BigDecimal.valueOf(product.getDouble("cgst")));
          products.setSGST(BigDecimal.valueOf(product.getDouble("sgst")));
          products.setVegFlag(product.getBoolean(Constants.VEGFLAG));
          products.setMeasurement(product.getString("measurement"));
          products.setVariationType1(
              variationTypeService.getVariationTypesById(Long.parseLong("1")));
          products.setVariationType2(
              variationTypeService.getVariationTypesById(Long.parseLong("1")));
          final String category = product.getString(Constants.CATEGORYNAME);
          final ProductCategory productCategoryId =
              this.productCategoryService.getProductCategoryByProductCategoryNameorgIdAndbrnachId(
                  product.getString(Constants.CATEGORYNAME), orgId, branchId);
          if (productCategoryId != null) {
            if (productCategoryId.getCategoryType().getCategoryName().equalsIgnoreCase(category)) {
              products.setProductCategory(productCategoryId);
            } else {
              final ProductCategory productCategory = new ProductCategory();
              productCategory.setBranch(branch);
              productCategory.setCategoryType(categoryTypeService.getCategoriesById(1L));
              productCategory.setOrganisation(employeeDetails.getOrganisation());
              productCategory.setParentId(0L);
              productCategory.setRanking(0L);
              this.productCategoryService.save(productCategory);
              final ProductCategory categoryId =
                  this.productCategoryService
                      .getProductCategoryByProductCategoryNameorgIdAndbrnachId(
                          product.getString(Constants.CATEGORYNAME), orgId, branchId);
              products.setProductCategory(categoryId);
            }

          } else {
            final ProductCategory productCategory = new ProductCategory();
            productCategory.setBranch(branch);
            productCategory.setProductTypeName(product.getString(Constants.CATEGORYNAME));
            productCategory.setCategoryType(categoryTypeService.getCategoriesById(1L));
            productCategory.setOrganisation(employeeDetails.getOrganisation());
            productCategory.setParentId(0L);
            productCategory.setRanking(0L);
            this.productCategoryService.save(productCategory);
            final ProductCategory categoryId =
                this.productCategoryService.getProductCategoryByProductCategoryNameorgIdAndbrnachId(
                    product.getString(Constants.CATEGORYNAME), orgId, branchId);
            products.setProductCategory(categoryId);
          }

          if (product.getString(Constants.VARIATIONTYPEID).equals("")) {
            final VariationTypes variationType =
                variationTypeService.getVariationTypesById(Long.parseLong("1"));
            products.setVariationType(variationType);
          } else {
            final VariationTypes variationTypes =
                variationTypeService.getVariationTypesById(
                    Long.parseLong((product.getString(Constants.VARIATIONTYPEID))));
            products.setVariationType(variationTypes);
          }
          count++;
        } catch (final Exception e) {
          final String invalidDataProductName = product.getString("productName");
          invalidDataList1.add(invalidDataProductName);
          LOGGER.error(Constants.SAVEPRODUCTDETAILS + e, e);
        }
      }
    }

    if (count == jsonarray.length()) {
      for (int arraySize = 0; arraySize < jsonarray.length(); arraySize++) {
        final JSONObject product = jsonarray.getJSONObject(arraySize);
        final Product products = new Product();
        final Map<String, Object> map = new HashMap<>();
        final String productName = product.getString("productName");
        final Branch branch = branchService.getBranchById(branchId);
        if (product.has("code")) {
          final String productCode = product.getString("code");
          final Product productCodeDetail =
              this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                  productCode, orgId, branchId);
          if (productCodeDetail != null) {
            if (productCodeDetail.getCode().equalsIgnoreCase(productCode)
                && !(productCode.isEmpty())) {
              final String codeMessage = productCodeDetail.getCode();
              productCodeList.add(codeMessage);
            } else if (productCodeDetail.getCode().equalsIgnoreCase(productCode)
                && productCode.isEmpty()) {
              products.setCode("");
            }
          } // productCodeDetail not null
          else {
            products.setCode("");
          }
        } else {
          products.setCode("");
        }

        final Product productDetail =
            this.productsService.getProductsByProductNameAndOrgAndBranch(
                productName, orgId, branchId);
        if (productDetail == null) {
          products.setProductName(productName);
          try {
            products.setProductFeatures(map);
            products.setImage(null);
            if (branch == null) {
              products.setBranch(null);
            } else {
              products.setBranch(branch);
            }
            products.setHsnNumber(null);
            products.setCode(product.getString("code"));
            products.setStatus(Constants.STATUSTRUE);
            products.setProductPrice(BigDecimal.valueOf(product.getDouble("productPrice")));
            products.setOrganisation(employeeDetails.getOrganisation());
            products.setPackageFlag(Constants.STATUSFALSE);
            products.setIncludingTaxFlag(null);
            products.setCGST(BigDecimal.valueOf(product.getDouble("cgst")));
            products.setSGST(BigDecimal.valueOf(product.getDouble("sgst")));
            products.setIGST(new BigDecimal(0));
            products.setVegFlag(product.getBoolean(Constants.VEGFLAG));
            products.setMeasurement(product.getString("measurement"));
            products.setMeasurement1("1");
            products.setMeasurement2("1");
            products.setVariationType1(
                variationTypeService.getVariationTypesById(Long.parseLong("1")));
            products.setVariationType2(
                variationTypeService.getVariationTypesById(Long.parseLong("1")));
            final String category = product.getString(Constants.CATEGORYNAME);

            final ProductCategory productCategoryId =
                this.productCategoryService.getProductCategoryByProductCategoryNameorgIdAndbrnachId(
                    product.getString(Constants.CATEGORYNAME), orgId, branchId);
            if (productCategoryId != null) {
              if (productCategoryId
                  .getCategoryType()
                  .getCategoryName()
                  .equalsIgnoreCase(category)) {
                products.setProductCategory(productCategoryId);
              } else {
                final ProductCategory productCategory = new ProductCategory();
                productCategory.setBranch(branch);
                productCategory.setCategoryType(categoryTypeService.getCategoriesById(1L));
                productCategory.setOrganisation(employeeDetails.getOrganisation());
                productCategory.setParentId(0L);
                productCategory.setRanking(0L);
                this.productCategoryService.save(productCategory);
                final ProductCategory categoryId =
                    this.productCategoryService
                        .getProductCategoryByProductCategoryNameorgIdAndbrnachId(
                            product.getString(Constants.CATEGORYNAME), orgId, branchId);
                products.setProductCategory(categoryId);
              }

            } else {
              final ProductCategory productCategory = new ProductCategory();
              productCategory.setBranch(branch);
              productCategory.setProductTypeName(product.getString(Constants.CATEGORYNAME));
              productCategory.setCategoryType(categoryTypeService.getCategoriesById(1L));
              productCategory.setOrganisation(employeeDetails.getOrganisation());
              productCategory.setParentId(0L);
              productCategory.setRanking(0L);
              this.productCategoryService.save(productCategory);
              final ProductCategory categoryId =
                  this.productCategoryService
                      .getProductCategoryByProductCategoryNameorgIdAndbrnachId(
                          product.getString(Constants.CATEGORYNAME), orgId, branchId);
              products.setProductCategory(categoryId);
            }

            products.setVariationType(null);
            products.setParentId(0l);
            if (product.getString(Constants.VARIATIONTYPEID).equals("")) {
              final VariationTypes variationType =
                  variationTypeService.getVariationTypesById(Long.parseLong("1"));
              products.setVariationType(variationType);
            } else {
              final VariationTypes variationTypes =
                  variationTypeService.getVariationTypesById(
                      Long.parseLong((product.getString(Constants.VARIATIONTYPEID))));
              products.setVariationType(variationTypes);
            }
            this.productsService.save(products);
            list1.add("Products saved sucessfully");
          } catch (final Exception e) {
            LOGGER.error(Constants.SAVEPRODUCTDETAILS + e, e);
            if (!(productCodeList.isEmpty())) {
              message.put(Constants.MESSAGE, productCodeList + Constants.PRODUCTCODEALREADYEXISTS);
            } // productCodeList
            else if (!(productNameList.isEmpty())) {
              message.put(Constants.MESSAGE, productNameList + Constants.PRODUCTCODEALREADYEXISTS);
            } else if (!(productCodeList.isEmpty()) || !(productNameList.isEmpty())) {
              message.put(
                  Constants.MESSAGE,
                  productNameList
                      + Constants.PRODUCTCODEALREADYEXISTS
                      + productCodeList
                      + Constants.PRODUCTCODEALREADYEXISTS);
            }
          }
        }
      }

    } // if count equals
    else {
      if (!(productCodeList.isEmpty())) {
        message.put(Constants.MESSAGE, productCodeList + Constants.PRODUCTCODEALREADYEXISTS);
      } // productCodeList
      else if (!(productNameList.isEmpty())) {
        message.put(Constants.MESSAGE, productNameList + Constants.PRODUCTNAMEALREADYEXISTS);
      } else if (!(productCodeList.isEmpty()) || !(productNameList.isEmpty())) {
        message.put(
            Constants.MESSAGE,
            productNameList
                + Constants.PRODUCTNAMEALREADYEXISTS
                + productCodeList
                + Constants.PRODUCTCODEALREADYEXISTS);
      } else if (!(invalidDataList1.isEmpty())) {
        message.put(Constants.MESSAGE, "for " + productNameList1 + Constants.ENTERVALIDDATA);
      } else if (!(productCodeList.isEmpty())
          || !(productNameList.isEmpty())
          || !(invalidDataList1.isEmpty())) {
        message.put(
            Constants.MESSAGE,
            productNameList
                + Constants.PRODUCTNAMEALREADYEXISTS
                + productCodeList
                + Constants.PRODUCTCODEALREADYEXISTS
                + productNameList1
                + Constants.ENTERVALIDDATA);
      } else if (!(productCodeList.isEmpty()) || !(invalidDataList1.isEmpty())) {
        message.put(
            Constants.MESSAGE,
            productCodeList
                + Constants.PRODUCTCODEALREADYEXISTS
                + productNameList1
                + Constants.ENTERVALIDDATA);
      } else if (!(productNameList.isEmpty()) || !(invalidDataList1.isEmpty())) {
        message.put(
            Constants.MESSAGE,
            productNameList
                + Constants.PRODUCTNAMEALREADYEXISTS
                + productNameList1
                + Constants.ENTERVALIDDATA);
      }
    } // if count not else
    if (!(list1.isEmpty())) {
      message.put(Constants.MESSAGE, list1.size() + " products added successfully");
    }

    return message;
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCT_DETAILS)
  public final Product updateProductDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails,
      @PathVariable("id") final Long id)
      throws JSONException {

    final JSONObject product = new JSONObject(productDetails);
    final Map<String, Object> map = new HashMap<>();
    final Product products = this.productsService.getProductFeaturesById(id);
    final String hsnNumber = product.getString("hsnNumber");
    final String productCode = product.getString("code");
    map.put("productFeatures", product.getString("productFeatures"));

    try {

      products.setHsnNumber(hsnNumber);
      products.setCode(productCode);
      products.setProductName(product.getString("productName"));

      products.setProductFeatures(map);
      products.setImage(product.getString("image"));
      products.setProductPrice(BigDecimal.valueOf(product.getDouble("productPrice")));

      products.setPackageFlag(product.getBoolean("packageFlag"));

      this.productsService.update(products);

    } catch (final Exception ex) {
      LOGGER.error("getproductsDetails" + ex);
    }
    return products;
  }

  @PostMapping(path = RouteConstants.UPDATE_IMMAGE)
  public final Product updateImage(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws JSONException {

    Product products = new Product();
    try {
      final JSONObject product = new JSONObject(productDetails);

      if (product.has("code") && !product.isNull("code")) {
        final String productCode = product.getString("code");
        products = this.productsService.getProductsByProductCode(productCode);

        products.setImage(product.getString(Constants.IMAGE));

        this.productsService.update(products);
      }
    } catch (final Exception ex) {
      LOGGER.error("updateImage" + ex);
    }
    return products;
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCTS_DETAILS)
  public final Map<String, String> updateProductDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws JSONException {

    final JSONObject product = new JSONObject(productDetails);
    final Long id = product.getLong("id");
    final String key = product.getString("key");
    final String value = product.getString("value");

    final Map<String, String> productUpdate = new HashMap<>();
    try {
      final Product products = this.productsService.getProductsById(id);
      final String productName = products.getProductName();
      if (key.equals("productPrice")) {
        products.setProductPrice(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals("productName")) {
        products.setProductName(value);
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals("packageFlag")) {

        if (value.equalsIgnoreCase("true")) {
          products.setPackageFlag(Constants.STATUSTRUE);

        } else {
          products.setPackageFlag(Constants.STATUSFALSE);
        }

        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals("image")) {
        products.setImage(value);
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals("cgst")) {
        products.setCGST(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals("sgst")) {
        products.setSGST(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals("igst")) {
        products.setIGST(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals("hsnNumber")) {
        products.setHsnNumber(value);
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else if (key.equals(Constants.VEGFLAG)) {
        products.setVegFlag(Boolean.parseBoolean(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFOR + productName);
      } else {
        productUpdate.put(Constants.PRODUCTUPDATE, "key Name does not match");
      }
      this.productsService.update(products);
    } catch (final RuntimeException ex) {
      LOGGER.error(Constants.UPDATEPRODUCTDETAILS + ex, ex);
    }
    return productUpdate;
  }

  @GetMapping(path = RouteConstants.SINAGLE_PRODUCT_DETAILS)
  public final Product getSingleProductDetails(
      final HttpServletRequest request, final HttpServletResponse responce) throws Exception {

    Product products = new Product();
    try {
      final String code = request.getParameter("productCode");

      if (code != null && !code.isEmpty()) {
        products = this.productsService.getProductsByProductCode(code);
      }

    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getproductsDetails" + ex);
    }
    return products;
  }

  @GetMapping(path = RouteConstants.PRODUCT_PRODUCTFEATURE_DETAILS_ID)
  public final Product getProudctFeaturesDetails(
      @PathVariable("id") final Long id,
      final HttpServletRequest request,
      final HttpServletResponse responce) {

    Product product = null;
    try {
      product = this.productsService.getProductFeaturesById(id);

    } catch (final Exception ex) {
      LOGGER.info("productCategoryFeatureDetails" + ex);
    }
    return product;
  }

  @GetMapping(path = RouteConstants.ALL_PRODUCT_DETAILS)
  public final ResponseEntity<Object> getproductDetails(final HttpServletRequest request)
      throws Exception {
    List<Product> productDetails = new ArrayList<Product>();

    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          productDetails = this.productsService.getAllProductsByOrgId(orgId);
          return new ResponseEntity<>(productDetails, HttpStatus.OK);
        } else {
          productDetails = this.productsService.getAllProducts(orgId, branchId);
          return new ResponseEntity<>(productDetails, HttpStatus.OK);
        }
      } else {
        return new ResponseEntity<>(productDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error("getemployeePage" + ex);
    }
    return new ResponseEntity<>(productDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ALL_ITEM_DETAILS)
  public final ResponseEntity<Object> getItemDetails() {
    List<Item> itemDetails = new ArrayList<Item>();

    try {
      itemDetails = this.itemsService.getAllItemsName();

    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("getItemDetails" + ex);
    }
    return new ResponseEntity<>(itemDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.UPDATE_PRODUCT_PAGE_CODE)
  public final ResponseEntity<Object> geteditProductPage(
      @PathVariable("code") final String code,
      final HttpServletRequest request,
      final HttpServletResponse responce) {

    Product productById = null;
    try {
      if (code == null || !NumberUtils.isNumber(code)) {
        return new ResponseEntity<>("Invalid ", HttpStatus.BAD_REQUEST);
      }

      productById = this.productsService.getProductsByProductCode(code);

    } catch (final NullPointerException ne) {
      LOGGER.error("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("update Product Details" + ex);
    }
    return new ResponseEntity<>(productById, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_PRODUCT_DETAILS_BY_NAME)
  public final List<Product> getProductDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Product> products = null;
    try {
      final String ProductName = request.getParameter("productName");

      if (ProductName != null && !ProductName.isEmpty()) {
        products = this.productsService.getAllProductsByName(ProductName);
      }
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "ProductDetails" + ex);
    }
    return products;
  }

  @GetMapping(path = RouteConstants.PRODUCT_BILLING_DETAILS)
  public final List<CartDetail> getBillingInfo(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      final Billing billing,
      final BillingDetail billingDetails)
      throws BasicTemplateServiceException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long billId = Long.parseLong(request.getParameter("billId"));
    final Billing billingdetails = this.billingService.getBillingDetailsByBillId(billId);

    final List<CartDetail> cartProductDetails =
        this.cartProductService.getCartDetailsByCartId(billingdetails.getCartId().getCartId());

    try {
      // saving outward deatils for Balance Sheet
      final Date startDate = billingdetails.getBillDate();
      final String[] billDate = startDate.toString().split("-");

      final Outward outward = new Outward();
      outward.setBillingDetail(billingdetails);
      outward.setCustomer(billingdetails.getCustomer());
      final Retailer retailerDetails = this.retailerService.getRetailerDetails().get(0);
      outward.setGstNumber(retailerDetails.getRetailerGstNumber());
      outward.setTaxableAmount(
          billingdetails.getTotalAmount().subtract(billingdetails.getTaxableAmount()));
      outward.setCGSTAmount(billingdetails.getCgstAmount());
      outward.setSGSTAmount(billingdetails.getSgstAmount());
      outward.setIGSTAmount(BigDecimal.valueOf(0.0));
      outward.setTotalAmount(billingdetails.getTotalAmount());
      outward.setOutwardDate(billingdetails.getBillDate().toString());
      outward.setMonth(billDate[1]);
      outward.setYear(billDate[0]);

      if (billingdetails.getCustomer() == null) {
        outward.setCustomerGSTNumber("null");
      } else {
        outward.setCustomerGSTNumber(billingdetails.getCustomer().getCustomerGstNumber());
      }
      this.outwardService.save(outward);

      for (int i = 0; i < cartProductDetails.size(); ++i) {

        final CartDetail cartDetails = cartProductDetails.get(i);

        // for updating remaing qty
        final Product productDetails =
            this.productsService.getProductsByProductName(cartDetails.getProductName());
        final InventoryDetail inventoryDetail =
            this.inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                productDetails.getId(), employeeDetails.getBranch().getId());

        // for updating remaing qty
        final BigDecimal existingQty = inventoryDetail.getQuantity();
        final long purchaseQty = cartDetails.getQuantity();
        final BigDecimal remaingQty = existingQty.subtract(new BigDecimal(purchaseQty));

        billingDetails.setInventoryDetails(inventoryDetail);
        billingDetails.setVendor(null);
        billingDetails.setQuantityStock(existingQty.longValue());
        billingDetails.setQuantityRemaing(remaingQty.longValue());
        billingDetails.setQuantityUse(purchaseQty);
        billingDetails.setCreateDate(new Date());
        billingDetails.setModifiedDate(new Date());
        billingDetails.setOrderdDate(new Date());
        billingDetails.setBranch(employeeDetails.getBranch());
        billingDetails.setBillingDetails(billingdetails);

        this.billingDetailsService.save(billingDetails);

        inventoryDetail.setQuantity(remaingQty);
        this.inventoryDetailService.update(inventoryDetail);
      }

    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getBillingInfo" + ex);
    }
    return cartProductDetails;
  }

  @GetMapping(path = RouteConstants.PRODUCT_BILLING_PERCENTAGE_DETAILS)
  public final Map<String, Map<String, CartDetail>> getBillingInfoByPercentages(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      final Billing billing,
      final InventoryHistory inventoryHistory)
      throws BasicTemplateServiceException {

    final Map<String, Map<String, CartDetail>> map = new HashMap<>();

    final long billId = Long.parseLong(request.getParameter("billId"));
    final Billing billingdetails = this.billingService.getBillingDetailsByBillId(billId);

    final List<CartDetail> cartProductDetails =
        this.cartProductService.getCartDetailsByCartId(billingdetails.getCartId().getCartId());

    final List<Percentage> percentageDetails = this.percentageService.getAllPercentages();

    try {

      for (final Percentage percentage : percentageDetails) {
        final Map<String, CartDetail> cartData = new HashMap<>();

        for (final CartDetail cart : cartProductDetails) {

          if (cart.getSgstPercentage().compareTo(percentage.getSGST()) == 0) {
            cartData.put(cart.getProductName(), cart);
          }
        }
        if (cartData.size() != 0) {
          map.put(percentage.getGST().toString(), cartData);
        }
      }
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getBillingInfo" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.GET_ALL_CARTPRODUCT_DETAILS_COUNTRERID)
  public final ResponseEntity<Object> getCartProductDetailsByCounterId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<CartDetail> cartDetail = null;
    try {

      cartDetail = this.cartProductService.getCartDetailsByCounterIdAndFlowId();

    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getCartProductDetailsByCounterId" + ex);
    }
    return new ResponseEntity<>(cartDetail, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_ALL_ORDERFLOW_DETAILS)
  public final ResponseEntity<Object> getOrderFlowByCounterId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<OrderFlow> orderFlow = new ArrayList<OrderFlow>();
    try {

      orderFlow = this.orderFlowService.getOrderFlowByCounterIdAndFlowId();
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getOrderFlowByCounterId" + ex);
    }
    return new ResponseEntity<>(orderFlow, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_ALL_CARTPRODUCT_DETAILSSUMMARY)
  public final List<CartDetail> getCartProductDetailsSummaryPageByFlowId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<CartDetail> cartDetail = null;
    try {

      cartDetail = this.cartProductService.getCartDetailsByFlowId();

    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getCartProductDetailsSummaryPageByFlowId" + ex);
    }
    return cartDetail;
  }

  @GetMapping(path = RouteConstants.GET_ALL_ORDERFLOW_DETAILSSUMMARY)
  public final List<OrderFlow> getCartProSummaryPageByFlowId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<OrderFlow> orderFlow = null;
    try {

      orderFlow = this.orderFlowService.getOrderFlowByFlowId();

    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getCartProductDetailsSummaryPageByFlowId" + ex);
    }
    return orderFlow;
  }

  @PostMapping(path = RouteConstants.UPDATE_CARTPRODUCT_DETAILS)
  public final Map<String, Object> updateCartProductDetailsByProductNameAndHsnCode(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException {
    final long cartId = Long.parseLong(request.getParameter("cartId"));
    final long productId = Long.parseLong(request.getParameter("productId"));
    final JSONObject jsonObject = new JSONObject(payload);
    final Long qnty = jsonObject.getLong("qnty");
    final Map<String, Object> map = new HashMap<>();
    final Map<String, Object> map1 = new HashMap<>();
    final Map<String, Object> map2 = new HashMap<>();
    Cart cart = new Cart();
    Flow flow = null;
    OrderFlow orderFlow1 = new OrderFlow();
    try {

      final Product products = this.productsService.getProductsById(productId);
      cart = this.cartService.getCartDetailsByCartId(cartId);
      final OrderFlow orderFlow =
          this.orderFlowService.getOrderFlowByCartIdAndProductIdAndFlowId(cartId, productId);

      orderFlow1 =
          this.orderFlowService.getOrderFlowByCartIdAndProductIdAndCartIdByServiceSection(
              cartId, productId);

      if (orderFlow1 != null) {

      } else {
        flow = this.flowService.getFlowById(2L);
        final long qty = orderFlow.getQuantity() - qnty;
        orderFlow1 = new OrderFlow();
        orderFlow1.setFlow(flow);
        orderFlow1.setQuantity(qty);
        orderFlow1.setCartDetails(cart);
        orderFlow1.setProducts(products);
        orderFlow1.setTime(new Date(System.currentTimeMillis()));
        this.orderFlowService.save(orderFlow1);
        map1.put("servicemessage", orderFlow1);

        map.putAll(map1);
      }
      orderFlow.setQuantity(qnty);
      this.orderFlowService.update(orderFlow);
      map2.put("kitchenmessage", orderFlow);
      map.putAll(map2);

    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "updateCartProductDetailsByProductNameAndHsnCode" + ex);
    }
    return map;
  }

  @PostMapping(path = RouteConstants.UPDATE_CARTPRODUCT_BYQUANTITY_DETAILS)
  public final Map<String, Object> updateCartProductDetailsByquantity(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws Exception {
    final long counterId = Long.parseLong(request.getParameter("counterId"));
    final String productName = request.getParameter("productName");
    final String hsnCode = request.getParameter("hsnCode");
    final JSONObject jsonObject = new JSONObject(payload);
    final Long qnty = jsonObject.getLong("qnty");
    final Map<String, Object> map = new HashMap<>();
    final Map<String, Object> map1 = new HashMap<>();
    final Map<String, Object> map2 = new HashMap<>();
    CartDetail cartDetail = null;
    OrderFlow orderFlow = null;

    try {

      cartDetail =
          this.cartProductService.getCartDetailsByProductNameAndHsnCode(
              counterId, productName, hsnCode);

      if (cartDetail == null) {
        map.put(Constants.MESSAGE, Constants.PRODUCTNOTAVAILABLE);
      }

      final Product products =
          this.productsService.getProductsByHsnCodeAndProductName(hsnCode, productName);
      if (products != null && cartDetail != null) {
        orderFlow =
            this.orderFlowService.getOrderFlowByCartIdAndProductIdAndFlowId(
                cartDetail.getCartDetails().getCartId(), products.getId());
      } else {
        map.put(Constants.MESSAGE, Constants.PRODUCTNOTAVAILABLE);
      }
      if (orderFlow != null) {
        orderFlow.setQuantity(qnty);
        this.orderFlowService.update(orderFlow);
        map2.put("orderFlowDetails", orderFlow);
      }
      map.putAll(map2);
      cartDetail.setQuantity(qnty);
      map1.put("cartDetails", cartDetail);
      this.cartProductService.update(cartDetail);
      map.putAll(map1);
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "updateCartProductDetailsByProductNameAndHsnCode" + ex);
    }
    return map;
  }

  @DeleteMapping(path = RouteConstants.DELETE_CARTPRODUCT_DETAILS)
  public final Boolean deleteCartProductDetailsByProductNameAndHsnCode(
      final HttpServletRequest request, final HttpServletResponse responce) throws Exception {

    final long counterId = Long.parseLong(request.getParameter("counterId"));
    final String productName = request.getParameter("productName");

    final String hsnCode = request.getParameter("hsnCode");
    Boolean result = null;
    CartDetail cartDetail = null;
    try {

      cartDetail =
          this.cartProductService.getCartDetailsByCounterIdProductNameAndHsnCode(
              counterId, productName, hsnCode);

      result = this.cartProductService.delete(cartDetail.getId());

    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "updateCartProductDetailsByProductNameAndHsnCode" + ex);
    }
    return result;
  }

  @SuppressWarnings("null")
  @PostMapping(path = RouteConstants.CARTPRODUCT_DETAILS_BY_ORGID)
  public final Map<String, Object> gettingRestaurentCartProductDetails1(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws JSONException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final String code = request.getParameter("productcode");
    final JSONObject jsonObject = new JSONObject(payload);
    final Long QTY = 1L;
    final Long counterNumber = jsonObject.getLong("counterNumber");
    Boolean productFlag = false;
    Product productDetails = null;
    Item itemDetails = null;
    OrderFlow orderFlow = new OrderFlow();

    final Long floorTypeId = 1L;
    final Long floorRanking = 1L;
    InventoryDetail inventoryDetail = null;
    Cart cart = null;
    final Map<String, Object> productDetails1 = new HashMap<>();
    final Counter counterDetails = this.counterService.getCounterDetailsById(counterNumber);
    try {
      cart = this.cartService.getCartDetailsOnStatus(Constants.CARTSTATUS, counterNumber);
      if (cart != null) {
        cart = new Cart();
        cart.setCartStatus(Constants.CARTSTATUS);
        final Date date = new Date();
        cart.setStartTime(date);
        cart.setEndTime(date);
        cart.setCounterDetails(counterDetails);
        this.cartService.save(cart);
      }

      final List<Flow> flow =
          this.flowService.getFlowByOrgId(employeeDetails.getOrganisation().getId());
      LooseProduct looseProduct = null;
      final CartDetail cartProductDetail = new CartDetail();
      if (flow.isEmpty()) {

        if (code.contains(Constants.LOOSEPRODUCTCODE)) {
          final String[] codes = code.split(Constants.LOOSEPRODUCTCODE);
          final String loosecode = codes[1];
          looseProduct = this.looseProductService.getLooseProductDetailsByCode(loosecode);
        } else {
          if (employeeDetails.getBranch() != null) {
            productDetails =
                this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                    code,
                    employeeDetails.getOrganisation().getId(),
                    employeeDetails.getBranch().getId());
          } else {
            productDetails =
                this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                    code, employeeDetails.getOrganisation().getId(), null);
          }
          inventoryDetail =
              this.inventoryDetailService.getInventoryDetailsByProductIdAndBranchIdAndOrgId(
                  productDetails.getId(),
                  employeeDetails.getBranch().getId(),
                  employeeDetails.getOrganisation().getId());
          // if condition for if quantity is null then returns
          // products
          // not available
          if (inventoryDetail.getQuantity().doubleValue() <= 0) {
            productDetails1.put(Constants.PRODUCT, Constants.PRODUCTNOTAVAILABLE);
            return productDetails1;
          }
        }
        if (productDetails != null || looseProduct != null) {
          itemDetails = this.itemsService.getItemsByHsnNo(productDetails.getHsnNumber());

          productDetails1.put("ProductCode", productDetails.getCode());
          productDetails1.put("ProductName", productDetails.getProductName());
          productDetails1.put("HsnNumber", productDetails.getHsnNumber());
          productDetails1.put("ProductPrice", productDetails.getProductPrice());
          productDetails1.put("Quantity", QTY);
          productDetails1.put("SGST", itemDetails.getPercentageDetails().getCGST());
          productDetails1.put("CGST", itemDetails.getPercentageDetails().getSGST());
          productDetails1.put("IGST", itemDetails.getPercentageDetails().getIGST());
          return productDetails1;
        } else {

          productDetails1.put(Constants.PRODUCT, Constants.PRODUCTNOTAVAILABLE);
          return productDetails1;
        }

      } else {
        if (employeeDetails.getBranch() != null) {

          productDetails =
              this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                  code,
                  employeeDetails.getOrganisation().getId(),
                  employeeDetails.getBranch().getId());
          if (productDetails != null) {

          } else {
            productDetails1.put(Constants.PRODUCT, Constants.PRODUCTNOTAVAILABLE);
          }
        } else {

          productDetails =
              this.productsService.getProductsByProductCodeAndOrgIdAndBranchId(
                  code, employeeDetails.getOrganisation().getId(), null);
          if (productDetails != null) {

          } else {
            productDetails1.put(Constants.PRODUCT, Constants.PRODUCTNOTAVAILABLE);
          }
        }
        final String productName = productDetails.getProductName();
        final String hsnNumber = productDetails.getHsnNumber();
        final List<CartDetail> cartProductDetail1 =
            this.cartProductService.getCartDetailsByCartId(cart.getCartId());

        orderFlow =
            this.orderFlowService.getOrderFlowByCartIdAndProductIdAndFlowId(
                cart.getCartId(), productDetails.getId());
        if (orderFlow != null) {
          final Long qnty = orderFlow.getQuantity() + QTY;
          orderFlow.setQuantity(qnty);
          this.orderFlowService.update(orderFlow);
        } else {
          orderFlow.setQuantity(QTY);
          orderFlow.setCartDetails(cart);
          final Flow floorId = this.flowService.getFlowByFlowTypeId(floorTypeId, floorRanking);
          orderFlow.setFlow(floorId);
          orderFlow.setProducts(productDetails);
          orderFlow.setTime(new Date(System.currentTimeMillis()));
          this.orderFlowService.save(orderFlow);
        }
        // if(cartProductDetail1.size() > 0)
        if (cartProductDetail1.isEmpty()) {

          for (final CartDetail cartProduct : cartProductDetail1) {

            if ((cartProduct.getProductName()).equals(productName)
                && (cartProduct.getHsnCode()).equals(hsnNumber)) {
              productFlag = true;
              final Long qnty = cartProduct.getQuantity() + QTY;
              cartProduct.setQuantity(qnty);
              this.cartProductService.update(cartProduct);
              productDetails1.put(Constants.MESSAGE, cartProduct);
            }
          }
          if (productFlag) {
            cartProductDetail.setQuantity(QTY);
            itemDetails = this.itemsService.getItemsByHsnNo(productDetails.getHsnNumber());
            cartProductDetail.setCartDetails(cart);
            cartProductDetail.setCounterDetails(cart.getCounterDetails());
            cartProductDetail.setDiscountAmount(null);
            cartProductDetail.setHsnCode(productDetails.getHsnNumber());
            cartProductDetail.setCgstPercentage(itemDetails.getPercentageDetails().getCGST());
            cartProductDetail.setSgstPercentage(itemDetails.getPercentageDetails().getSGST());
            cartProductDetail.setIgstPercentage(itemDetails.getPercentageDetails().getIGST());
            cartProductDetail.setProductName(productDetails.getProductName());
            final Branch branch =
                this.branchService.getBranchById(employeeDetails.getBranch().getId());
            cartProductDetail.setBranch(branch);
            final Organisation organisation =
                this.organisationService.getOrganisationById(
                    employeeDetails.getOrganisation().getId());
            cartProductDetail.setOrganisation(organisation);
            cartProductDetail.setProductPrice(productDetails.getProductPrice());

            final BigDecimal totalAmount =
                productDetails.getProductPrice().multiply(new BigDecimal(QTY));
            final BigDecimal taxableAmount =
                itemDetails
                    .getPercentageDetails()
                    .getCGST()
                    .add(itemDetails.getPercentageDetails().getSGST())
                    .add(itemDetails.getPercentageDetails().getIGST())
                    .multiply(productDetails.getProductPrice())
                    .divide(Constants.HUNDRED);

            cartProductDetail.setTotalAmount(totalAmount);
            cartProductDetail.setTaxableAmount(taxableAmount.multiply(new BigDecimal(QTY)));
            final Date cartTime = new Date();
            cartProductDetail.setTime(cartTime);
            productDetails1.put(Constants.MESSAGE, cartProductDetail);
            this.cartProductService.save(cartProductDetail);
            return productDetails1;
          }
        } else {
          cartProductDetail.setQuantity(QTY);
          itemDetails = this.itemsService.getItemsByHsnNo(productDetails.getHsnNumber());

          cartProductDetail.setCartDetails(cart);
          cartProductDetail.setCounterDetails(cart.getCounterDetails());
          cartProductDetail.setDiscountAmount(null);
          cartProductDetail.setHsnCode(productDetails.getHsnNumber());
          cartProductDetail.setCgstPercentage(itemDetails.getPercentageDetails().getCGST());
          cartProductDetail.setSgstPercentage(itemDetails.getPercentageDetails().getSGST());
          cartProductDetail.setIgstPercentage(itemDetails.getPercentageDetails().getIGST());
          cartProductDetail.setProductName(productDetails.getProductName());
          final Branch branch =
              this.branchService.getBranchById(employeeDetails.getBranch().getId());
          cartProductDetail.setBranch(branch);
          final Organisation organisation =
              this.organisationService.getOrganisationById(
                  employeeDetails.getOrganisation().getId());
          cartProductDetail.setOrganisation(organisation);
          cartProductDetail.setProductPrice(productDetails.getProductPrice());

          final BigDecimal totalAmount =
              productDetails.getProductPrice().multiply(new BigDecimal(QTY));
          final BigDecimal taxableAmount =
              itemDetails
                  .getPercentageDetails()
                  .getCGST()
                  .add(itemDetails.getPercentageDetails().getSGST())
                  .add(itemDetails.getPercentageDetails().getIGST())
                  .multiply(productDetails.getProductPrice())
                  .divide(Constants.HUNDRED);

          cartProductDetail.setTotalAmount(totalAmount);
          cartProductDetail.setTaxableAmount(taxableAmount.multiply(new BigDecimal(QTY)));
          final Date cartTime = new Date();
          cartProductDetail.setTime(cartTime);
          productDetails1.put(Constants.MESSAGE, cartProductDetail);
          this.cartProductService.save(cartProductDetail);
          return productDetails1;
        }
      }

    } catch (final Exception ex) {
      LOGGER.error("gettingRestaurentCartProductDetails1" + ex, ex);
    }

    return productDetails1;
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCT_BY_HSNNUMBERANDCODE)
  public final Map<String, String> updateProductDetailsbasedonId(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws JSONException {

    final Map<String, String> message = new HashMap<String, String>();
    final JSONObject product = new JSONObject(productDetails);
    final Map<String, Object> map = new HashMap<String, Object>();
    Product products = new Product();
    product.getString(Constants.HSNNUMBERS);
    product.getString(Constants.PRODUCTCODE);
    map.put("productFeatures", product.getString("productFeatures"));

    try {
      try {
        products =
            this.productsService.getProductsByHsnCodeAndProductCode(
                Constants.HSNNUMBERS, Constants.PRODUCTCODE);
      } catch (final Exception ex) {
        message.put(Constants.MESSAGE, "produt not available");
      }
      products.setHsnNumber(Constants.HSNNUMBERS);
      products.setCode(Constants.PRODUCTCODE);
      products.setProductName(product.getString(Constants.PRODUCTNAME));

      products.setProductFeatures(map);
      products.setImage(product.getString(Constants.IMAGE));
      products.setProductPrice(BigDecimal.valueOf(product.getDouble("productPrice")));
      products.setPackageFlag(product.getBoolean("packageFlag"));

      products.setIncludingTaxFlag(product.getString("includingTaxFlag"));
      this.productsService.update(products);
      message.put(Constants.MESSAGE, "product updated successfully");
    } catch (final RuntimeException ex) {
      LOGGER.error("updateProductDetailsbasedonId" + ex, ex);
    }
    return message;
  }

  @GetMapping(path = RouteConstants.ALL_PRODUCTDETAILS_WITHSGST)
  public final List<Map<String, Object>> getproductDetailsWithSgst(
      final HttpServletRequest request) {
    List<Product> productDetails = null;

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final List<Map<String, Object>> productdata = new ArrayList<>();
    final Map<String, Object> map = new HashMap<>();
    final Map<String, Object> map1 = new HashMap<>();
    Item itemDetails = null;
    BigDecimal CGST = BigDecimal.valueOf(0.0);
    BigDecimal SGST = BigDecimal.valueOf(0.0);
    BigDecimal IGST = BigDecimal.valueOf(0.0);

    final long orgId = employeeDetails.getOrganisation().getId();

    try {
      final Branch branch = employeeDetails.getBranch();
      if (branch != null) {
        productDetails =
            this.productsService.getAllProducts(orgId, branch.getId()); // getAllProducts()
      } else {
        productDetails = this.productsService.getAllProductsByOrgId(orgId);
      }
      for (final Product product : productDetails) {
        itemDetails = this.itemsService.getItemsByHsnNo(product.getHsnNumber());
        CGST = itemDetails.getPercentageDetails().getCGST();
        SGST = itemDetails.getPercentageDetails().getSGST();
        IGST = itemDetails.getPercentageDetails().getIGST();
        map1.put(Constants.PRODUCT, product);
        map1.put("CGST", CGST);
        map1.put("SGST", SGST);
        map1.put("IGST", IGST);

        productdata.add(map1);
      }
      map.put("productDetails", productDetails);
    } catch (final RuntimeException ex) {
      LOGGER.error("getproductDetailsWithSgst" + ex, ex);
    }
    return productdata;
  }

  @GetMapping(path = RouteConstants.MENUCATEGORY_PRODUCT_ACTIVE)
  public final ResponseEntity<Object> getProductsOfMenuCategory(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<Product> products = null;
    final Identifier identifier = (Identifier) request.getAttribute("identifier");
    if (identifier != null) {
      final long orgId = identifier.getOrgId();
      final long branchId = identifier.getBranchId();
      if (branchId == 0) {
        products = productsService.getProductByorgIdandBranchIdstatus("MenuCategory", orgId);
      } else {
        products =
            productsService.getMenuProductDetailsOrgIdandStatus("MenuCategory", orgId, branchId);
      }
    } else {
      return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_LIST_ACTIVE)
  public final ResponseEntity<Object> getAllProductsActive(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Product> productDetails = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          productDetails = productsService.getProductByorgIdandstatusActive(orgId);
        } else {
          productDetails =
              productsService.getProductByorgIdandBranchIdstatusActive(orgId, branchId);
        }
      } else {
        return new ResponseEntity<>(productDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getProductDetails " + e);
    }
    return new ResponseEntity<>(productDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.PRODUCT_LIST_SORTING)
  public final ResponseEntity<Object> getAllProducts(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Product> productDetails = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          productDetails = productsService.getProductByorgIdandBranchIdstatus(orgId);
        } else {
          productDetails = productsService.getProductByorgIdandBranchIdstatus(orgId, branchId);
        }
      } else {
        return new ResponseEntity<>(productDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      LOGGER.info(Constants.MARKER, "getProductDetails " + e);
    }
    return new ResponseEntity<>(productDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ALL_PRODUCT_OPTIONS_TYPES)
  public final ResponseEntity<Object> getAllproductDetails(
      final HttpServletRequest request, final HttpServletResponse response) {
    List<Object[]> listOfproducts = null;
    final List<Map<String, Object>> products = new ArrayList<>();
    final Identifier identifier = (Identifier) request.getAttribute("identifier");
    if (identifier != null) {
      final long orgId = identifier.getOrgId();
      final long branchId = identifier.getBranchId();
      if (branchId != 0) {
        listOfproducts = productsService.getAllProductByParentId(orgId, branchId);
        for (int i = 0; i < listOfproducts.size(); i++) {
          final Map<String, Object> productsMap = new HashMap<>();
          final Map<String, Object> productListMap = new HashMap<String, Object>();
          productListMap.put("id", listOfproducts.get(i)[0]);
          productListMap.put("productName", listOfproducts.get(i)[1]);
          productListMap.put("hsnNumber", listOfproducts.get(i)[2]);
          productListMap.put("productPrice", listOfproducts.get(i)[3]);
          final ObjectMapper m = new ObjectMapper();
          final Map<String, Object> productpackaging = new HashMap<String, Object>();
          final List<String> productPackNames = new ArrayList<String>();
          if (listOfproducts.get(i)[4] != null) {
            final Map<String, Object> props = m.convertValue(listOfproducts.get(i)[4], Map.class);
            for (final Map.Entry<String, Object> entry : props.entrySet()) {
              final String productPackIdArray = entry.getValue().toString();
              final String str = productPackIdArray.replaceAll("[^a-zA-Z0-9]", " ");
              final String strvalue = str.trim().replaceAll(" +", " ");
              final String[] productPackIds = strvalue.split(" ");
              for (final String productPackId : productPackIds) {
                productPackNames.add(
                    productsService.getproductNamesByProductId(Long.valueOf(productPackId)));
              }

              productpackaging.put("packageProducts", productPackNames);
            }
          } else {
            productpackaging.put("packageProducts", productPackNames);
          }
          productListMap.put("productPackaging", productpackaging);
          productListMap.put("productTypeName", listOfproducts.get(i)[5]);
          productListMap.put("displayName", listOfproducts.get(i)[6]);
          productListMap.put("measurement", listOfproducts.get(i)[7]);
          productListMap.put("CGST", listOfproducts.get(i)[8]);
          productListMap.put("SGST", listOfproducts.get(i)[9]);
          productListMap.put("IGST", listOfproducts.get(i)[10]);
          productListMap.put("image", listOfproducts.get(i)[11]);
          final List<Object[]> childProducts =
              productsService.getProductByParentId(
                  orgId, branchId, Long.valueOf(listOfproducts.get(i)[0].toString()));

          for (int j = 0; j < childProducts.size(); j++) {
            final Map<String, Object> childProductListMap = new HashMap<String, Object>();
            childProductListMap.put("id", childProducts.get(j)[0]);
            childProductListMap.put("productName", childProducts.get(j)[1]);
            childProductListMap.put("hsnNumber", childProducts.get(j)[2]);
            childProductListMap.put("productPrice", childProducts.get(j)[3]);
            if (childProducts.get(j)[4] != null) {
              final Map<String, Object> props = m.convertValue(childProducts.get(j)[4], Map.class);
              for (final Map.Entry<String, Object> entry : props.entrySet()) {
                final String productPackIdArray = entry.getValue().toString();
                final String str = productPackIdArray.replaceAll("[^a-zA-Z0-9]", " ");
                final String strvalue = str.trim().replaceAll(" +", " ");
                final String[] productPackIds = strvalue.split(" ");
                for (final String productPackId : productPackIds) {
                  productPackNames.add(
                      productsService.getproductNamesByProductId(Long.valueOf(productPackId)));
                }
                productpackaging.put("packageProducts", productPackNames);
              }
            } else {
              productpackaging.put("packageProducts", productPackNames);
            }
            childProductListMap.put("productPackaging", productpackaging);
            childProductListMap.put("productTypeName", childProducts.get(j)[5]);
            childProductListMap.put("displayName", childProducts.get(j)[6]);
            childProductListMap.put("measurement", childProducts.get(j)[7]);
            childProductListMap.put("CGST", childProducts.get(j)[8]);
            childProductListMap.put("SGST", childProducts.get(j)[9]);
            childProductListMap.put("IGST", childProducts.get(j)[10]);
            childProductListMap.put("image", childProducts.get(j)[11]);
            productsMap.put("ChildProducts", childProductListMap);
          }

          productsMap.put("ParentProduct", productListMap);
          products.add(productsMap);
        }
      } else {
        listOfproducts = productsService.getAllProductByOrgAndParentId(orgId);
        for (int i = 0; i < listOfproducts.size(); i++) {
          final Map<String, Object> productsMap = new HashMap<>();
          final Map<String, Object> productListMap = new HashMap<String, Object>();
          productListMap.put("id", listOfproducts.get(i)[0]);
          productListMap.put("productName", listOfproducts.get(i)[1]);
          productListMap.put("hsnNumber", listOfproducts.get(i)[2]);
          productListMap.put("productPrice", listOfproducts.get(i)[3]);
          final ObjectMapper m = new ObjectMapper();
          final Map<String, Object> productpackaging = new HashMap<String, Object>();
          final List<String> productPackNames = new ArrayList<String>();
          if (listOfproducts.get(i)[4] != null) {
            final Map<String, Object> props = m.convertValue(listOfproducts.get(i)[4], Map.class);
            for (final Map.Entry<String, Object> entry : props.entrySet()) {
              final String productPackIdArray = entry.getValue().toString();
              final String str = productPackIdArray.replaceAll("[^a-zA-Z0-9]", " ");
              final String strvalue = str.trim().replaceAll(" +", " ");
              final String[] productPackIds = strvalue.split(" ");
              for (final String productPackId : productPackIds) {
                productPackNames.add(
                    productsService.getproductNamesByProductId(Long.valueOf(productPackId)));
              }
              productpackaging.put("packageProducts", productPackNames);
            }
          } else {
            productpackaging.put("packageProducts", productPackNames);
          }
          productListMap.put("productPackaging", productpackaging);
          productListMap.put("productTypeName", listOfproducts.get(i)[5]);
          productListMap.put("displayName", listOfproducts.get(i)[6]);
          productListMap.put("measurement", listOfproducts.get(i)[7]);
          productListMap.put("CGST", listOfproducts.get(i)[8]);
          productListMap.put("SGST", listOfproducts.get(i)[9]);
          productListMap.put("IGST", listOfproducts.get(i)[10]);
          productListMap.put("image", listOfproducts.get(i)[11]);

          final List<Object[]> childProducts =
              productsService.getProductByParentIdAndOrgId(
                  orgId, Long.valueOf(listOfproducts.get(i)[0].toString()));
          for (int j = 0; j < childProducts.size(); j++) {
            final Map<String, Object> childProductListMap = new HashMap<String, Object>();
            childProductListMap.put("id", childProducts.get(j)[0]);
            childProductListMap.put("productName", childProducts.get(j)[1]);
            childProductListMap.put("hsnNumber", childProducts.get(j)[2]);
            childProductListMap.put("productPrice", childProducts.get(j)[3]);
            if (childProducts.get(j)[4] != null) {
              final Map<String, Object> props = m.convertValue(childProducts.get(j)[4], Map.class);
              for (final Map.Entry<String, Object> entry : props.entrySet()) {
                final String productPackIdArray = entry.getValue().toString();
                final String str = productPackIdArray.replaceAll("[^a-zA-Z0-9]", " ");
                final String strvalue = str.trim().replaceAll(" +", " ");
                final String[] productPackIds = strvalue.split(" ");
                for (final String productPackId : productPackIds) {
                  productPackNames.add(
                      productsService.getproductNamesByProductId(Long.valueOf(productPackId)));
                }
                productpackaging.put("packageProducts", productPackNames);
              }
            } else {
              productpackaging.put("packageProducts", productPackNames);
            }
            childProductListMap.put("productPackaging", productpackaging);
            childProductListMap.put("productTypeName", childProducts.get(j)[5]);
            childProductListMap.put("displayName", childProducts.get(j)[6]);
            childProductListMap.put("measurement", childProducts.get(j)[7]);
            childProductListMap.put("CGST", childProducts.get(j)[8]);
            childProductListMap.put("SGST", childProducts.get(j)[9]);
            childProductListMap.put("IGST", childProducts.get(j)[10]);
            childProductListMap.put("image", childProducts.get(j)[11]);
            productsMap.put("ChildProducts", childProductListMap);
          }
          productsMap.put("ParentProduct", productListMap);

          products.add(productsMap);
        }
      }
    } else {
      return new ResponseEntity<>(products, HttpStatus.UNAUTHORIZED);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCT_VARIATIONS)
  public final Map<String, String> updateProductVariationDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws JSONException {

    final JSONObject product = new JSONObject(productDetails);
    final Long id = product.getLong("id");
    final String key = product.getString("key");
    final String value = product.getString("value");

    final Map<String, String> productUpdate = new HashMap<>();
    try {
      final Product products = this.productsService.getProductsById(id);
      if (key.equals("productPrice")) {
        products.setProductPrice(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("productName")) {
        products.setProductName(value);
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("packageFlag")) {

        if (value.equalsIgnoreCase("true")) {
          products.setPackageFlag(Constants.STATUSTRUE);
        } else {
          products.setPackageFlag(Constants.STATUSFALSE);
        }
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("image")) {
        products.setImage(value);
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("cgst")) {
        products.setCGST(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("sgst")) {
        products.setSGST(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("igst")) {
        products.setIGST(new BigDecimal(value));
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("hsnNumber")) {
        products.setHsnNumber(value);
        productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);
      } else if (key.equals("variationType")) {
        final VariationTypes variationType = variationTypeService.getVariationTypesByName(value);
        products.setVariationType(variationType);
      } else {
        productUpdate.put(Constants.PRODUCTUPDATE, "key Name does not match");
      }
      this.productsService.update(products);
    } catch (final RuntimeException ex) {
      LOGGER.error(Constants.UPDATEPRODUCTDETAILS + ex, ex);
    }
    return productUpdate;
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCT_STATUS)
  public final Map<String, String> updateProductstatus(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String productDetails)
      throws JSONException {

    final JSONObject product = new JSONObject(productDetails);
    final long id = product.getLong("id");
    final String status = product.getString("productStatus");

    final Map<String, String> productUpdate = new HashMap<>();
    try {
      final Product products = this.productsService.getProductsById(id);

      if (status.equalsIgnoreCase("true")) {
        products.setStatus(Constants.STATUSFALSE);
        productsService.update(products);

      } else {

        products.setStatus(Constants.STATUSTRUE);
        productsService.update(products);
      }

      productUpdate.put(Constants.PRODUCTUPDATE, Constants.PRODUCTDETAILSUPDATEFORID + id);

    } catch (final RuntimeException ex) {
      LOGGER.error(Constants.UPDATEPRODUCTDETAILS + ex, ex);
    }
    return productUpdate;
  }

  @GetMapping(path = RouteConstants.ORG_BRANCHES_PRODUCT_ORGANISATIONNAME)
  public final ResponseEntity<Object> getProductsBYOrganisationAndBranches(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @PathVariable("organisationName") final String organisationName) {
    List<Product> products = new ArrayList<>();

    try {
      if (organisationName == null || !NumberUtils.isNumber(organisationName)) {
        return new ResponseEntity<>("Invalid OrganisationName", HttpStatus.BAD_REQUEST);
      }
      final String branchId = request.getParameter("branchId");
      if (branchId != null && !branchId.isEmpty()) {
        final Organisation organisation =
            organisationService.getOrganisationByName(organisationName);
        final long orgId = organisation.getId();
        products = productsService.getAllProducts(orgId, Long.parseLong(branchId));
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProductDetails" + e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ORG_PRODUCT_ORGANISATION)
  public final ResponseEntity<Object> getProductsBYOrganisationPath(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @PathVariable("organisation") final String pathUrl) {
    List<Map<String, Object>> products = new ArrayList<>();
    try {
      if (pathUrl == null || !NumberUtils.isNumber(pathUrl)) {
        return new ResponseEntity<>("Invalid Path Url", HttpStatus.BAD_REQUEST);
      }
      products = productsService.getProductsByOrgPath(pathUrl);
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getOrgDetails" + e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.ORG_CATEGORYPRODUCT_CATEGORYID)
  public final ResponseEntity<Object> getProductsBYCategory(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @PathVariable("categoryId") final String id) {
    List<Map<String, Object>> products = new ArrayList<>();
    try {
      if (id == null || !NumberUtils.isNumber(id)) {
        return new ResponseEntity<>("Invalid Id", HttpStatus.BAD_REQUEST);
      }
      products = productsService.getProductsByCategoryId(Long.parseLong(id));
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getCategoryDetails" + e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_PRODUCT_BY_PRODUCTID)
  public final ResponseEntity<Object> getProductsBYProductId(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @PathVariable("productId") final String id) {
    Product products = null;

    try {
      if (id == null || !NumberUtils.isNumber(id)) {
        return new ResponseEntity<>("Invalid Id", HttpStatus.BAD_REQUEST);
      }
      products = productsService.getProductsById(Long.parseLong(id));
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProductDetails" + e);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
  }
}
