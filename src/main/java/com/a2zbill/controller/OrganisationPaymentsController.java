package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.OrganisationPayments;
import com.a2zbill.domain.Payment;
import com.a2zbill.services.OrganisationPaymentsService;
import com.a2zbill.services.PaymentService;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.service.OrganisationService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrganisationPaymentsController {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(OrganisationPaymentsController.class);

  @Autowired private OrganisationPaymentsService organisationPaymentsService;

  @Autowired private PaymentService paymentService;

  @Autowired private OrganisationService organisationService;

  @GetMapping(path = RouteConstants.GET_ALL_ORG_PAYMENT_DETAILS)
  public final ResponseEntity<Object> getAllOrganisationPaymentDetails(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<OrganisationPayments> organisationPayments = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        organisationPayments = organisationPaymentsService.getOrganisationPaymentsByOrgId(orgId);
      } else {
        return new ResponseEntity<>(organisationPayments, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.info("getAllOrganisationPaymentDetails" + ex, ex);
    }
    return new ResponseEntity<>(organisationPayments, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.SAVE_ORG_PAYMENT_MODES)
  public final ResponseEntity<Object> saveOrganisationPaymentModes(
      @RequestBody final String organisationPayment,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {
    final List<String> list = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final JSONObject orgPaymentDetails = new JSONObject(organisationPayment);
        final JSONArray array = new JSONArray(orgPaymentDetails.getString("payments"));
        Payment payment = null;
        for (int i = 0; i < array.length(); i++) {
          final JSONObject paym = array.getJSONObject(i);
          final long paymentId = paym.getLong("paymentId");
          final OrganisationPayments organisationPaymen =
              organisationPaymentsService.getOrganisationPaymentsByOrgIdAndPaymentId(
                  orgId, paymentId);
          if (organisationPaymen != null) {
            list.add(organisationPaymen.getPaymentId().getPaymentType());
          } else {
            final OrganisationPayments organisationPayments = new OrganisationPayments();
            payment = paymentService.getPaymentById(paymentId);
            organisationPayments.setPaymentId(payment);
            organisationPayments.setCreatedDate(new Date());
            organisationPayments.setModifiedDate(new Date());
            organisationPayments.setOrgId(organisationService.getOrganisationById(orgId));
            organisationPaymentsService.save(organisationPayments);
          }
        }
      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
      return new ResponseEntity<>(list, HttpStatus.OK);
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "saveOrganisationPaymentModes" + ex, ex);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }
}
