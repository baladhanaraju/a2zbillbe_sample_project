package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.CancelItem;
import com.a2zbill.services.CancelItemService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CancelItemController {

  private static final Logger LOGGER = LoggerFactory.getLogger(CancelItemController.class);

  @Autowired private CancelItemService cancelItemService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @PostMapping(path = RouteConstants.SAVE_CANCEL_ITEM_DETAILS)
  public final void addCancelItemDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload) {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    try {
      final JSONArray jsonArray = new JSONArray(payload);
      for (int i = 0; i < jsonArray.length(); i++) {
        final CancelItem cancelItemDetails = new CancelItem();
        final JSONObject cancelData = ((JSONObject) jsonArray.get(i));

        cancelItemDetails.setGuid(cancelData.getString("guid"));

        final Branch branch = employeeDetails.getBranch();
        if (branch != null) {
          cancelItemDetails.setBranch(employeeDetails.getBranch());
        } else {
          cancelItemDetails.setBranch(null);
        }
        cancelItemDetails.setCounternumber(cancelData.getLong("counterNumber"));
        cancelItemDetails.setDate(new Date());
        cancelItemDetails.setOrganisation(employeeDetails.getOrganisation());
        cancelItemDetails.setProductname(cancelData.getString("productName"));
        cancelItemDetails.setReason(cancelData.getString("reason"));

        this.cancelItemService.save(cancelItemDetails);
      }
    } catch (final Exception re) {
      LOGGER.error("addCancelItemDetailsDetails" + re);
    }
  }
}
