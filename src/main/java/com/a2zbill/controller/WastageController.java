package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.Wastage;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.WastageService;
import com.tsss.basic.config.KafkaSender;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("unused")
public class WastageController {

  private static final Logger LOGGER = LoggerFactory.getLogger(WastageController.class);

  @Value("${jwt.header}")
  private String tokenHeader;

  @Autowired private WastageService wastageService;

  @Autowired private AuthenticationManager authenticationManager;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private KafkaSender kafkaSender;

  @Autowired private OrganisationService organisationservice;

  @Autowired private BranchService branchService;

  @Autowired private ProductService productsService;

  @PostMapping(path = RouteConstants.WESTAGE_DETAILS_SAVE)
  public final ResponseEntity<Object> addWastageDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String payload)
      throws JSONException, Exception {
    final Map<String, Object> wastageMap = new HashMap<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final Employee employeeDetails =
            employeeService.getEmployeeByEmployeeNumber(identifier.getUserId());
        final JSONObject json = new JSONObject(payload);
        final Wastage wastage = new Wastage();
        final Map<String, String> map = new HashMap<>();
        final String dateString = json.getString("date");
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final Date date = sdf.parse(dateString);
        final long branchId = json.getLong("branchId");
        final JSONArray ja = new JSONArray(json.getString("items"));
        for (int i = 0; i < ja.length(); i++) {
          final JSONObject items = ja.getJSONObject(i);
          final long Quantity = items.getLong("Quantity");
          final JSONObject prod = items.getJSONObject("selectedItemName");
          final long productId = prod.getLong("id");
          final Product productDetails = productsService.getProductFeaturesById(productId);
          final BigDecimal totalAmount =
              productDetails.getProductPrice().multiply(new BigDecimal(Quantity));
          wastage.setDate(date);
          wastage.setQuantity(Quantity);
          wastage.setBranch(branchService.getBranchById(branchId));
          wastage.setProduct(productsService.getProductsById(productId));
          wastage.setOrganisation(employeeDetails.getOrganisation());
          wastage.setCreatedDate(new Date());
          wastage.setModifiedDate(new Date());
          this.wastageService.save(wastage);
          wastageMap.put("message", "wastage details suceesfully saved ");
        }
      } else {
        return new ResponseEntity<>(wastageMap, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception e) {
      wastageMap.put("message", "wastage details unable to saved ");
      LOGGER.error("api/wastage/save  " + e);
    }
    return new ResponseEntity<>(wastageMap, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_ALL_WESTAGE_DETAILS)
  public final ResponseEntity<Object> getAllWastageDetails() {
    List<Wastage> wastageDetails = new ArrayList<Wastage>();
    try {
      wastageDetails = this.wastageService.getAllWastages();
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getAlteredbillingsDetails " + ex);
    }

    return new ResponseEntity<>(wastageDetails, HttpStatus.OK);
  }
}
