package com.a2zbill.controller;

import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.OrderStatus;
import com.a2zbill.services.OrderService;
import com.tsss.basic.security.jwt.Identifier;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

  private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

  @Autowired private OrderService orderService;

  @GetMapping(value = RouteConstants.CUSTOMER_ORDER_STATUS)
  public final ResponseEntity<Object> getCustomerOrderStatusByOrderId(
      final HttpServletRequest request, final HttpServletResponse response
      // @PathVariable("orderId") final long orderId
      ) {

    try {
      final String orderId = request.getParameter("orderId");

      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null && "C".equalsIgnoreCase(identifier.getUserType())) {
        final OrderStatus res = orderService.getOrderStatusById(Long.parseLong(orderId));
        if (res != null) return new ResponseEntity<>(res.getOrderStatus(), HttpStatus.OK);
      } else {
        return new ResponseEntity<>("invalid credentials", HttpStatus.UNAUTHORIZED);
      }

    } catch (final Exception e) {
      LOGGER.error("getCustomerOrderStatusByOrderId :: " + e);
    }
    return new ResponseEntity<>("Invalid orderId", HttpStatus.BAD_REQUEST);
  }
}
