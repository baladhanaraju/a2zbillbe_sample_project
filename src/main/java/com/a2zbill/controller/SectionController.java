package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Counter;
import com.a2zbill.domain.Section;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.SectionService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SectionController {

  private static final Logger LOGGER = LoggerFactory.getLogger(SectionController.class);

  @Autowired private SectionService sectionService;

  @Autowired private BranchService branchService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private CounterService counterService;

  @PostMapping(path = RouteConstants.SECTION_SAVE)
  public final void saveSection(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {

    try {
      final JSONObject section = new JSONObject(payload);
      final Section sectionDetails = new Section();

      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      sectionDetails.setSectionName(section.getString("sectionName"));
      sectionDetails.setDisplayName(section.getString("displayName"));
      sectionDetails.setOrganisation(employeeDetails.getOrganisation());
      final Branch branch = this.branchService.getBranchById(section.getLong("branch"));
      sectionDetails.setBranch(branch);
      sectionDetails.setCreatedDate(new Date());
      sectionDetails.setModifiedDate(new Date());
      sectionDetails.setStatus(Constants.STATUSTRUE);
      this.sectionService.save(sectionDetails);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "/api/save/Section " + ex);
    }
  }

  @GetMapping(path = RouteConstants.SECTION_DETAILS)
  public final ResponseEntity<Object> getSectionDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Section> sectionDetails = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0)
          sectionDetails = this.sectionService.getSectionDetailsByOrgAndStatus(orgId);
        else
          sectionDetails =
              this.sectionService.getSectionDetailsByOrgIdAndBranchAndStatus(orgId, branchId);
      } else {
        return new ResponseEntity<>(sectionDetails, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException ex) {
      LOGGER.error("No Records Found" + ex, ex);
    }
    return new ResponseEntity<>(sectionDetails, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.SECTION_COUNTER_DETAILS)
  public final ResponseEntity<Object> getCounterDetailsBySectionId(
      final HttpServletRequest request, final HttpServletResponse responce) {
    final List<Map<String, Object>> list = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        List<Section> sectionDetails = null;
        if (branchId != 0) {
          sectionDetails = this.sectionService.getSectionDetailsByOrgIdAndBranchId(orgId, branchId);
        } else {
          sectionDetails = this.sectionService.getSectionDetailsByOrgId(orgId);
        }
        if (sectionDetails != null) {
          for (final Section section : sectionDetails) {
            final Map<String, Object> map = new HashMap<>();
            List<Counter> counter = new ArrayList<>();
            if (branchId != 0) {
              counter =
                  this.counterService.getCounterDetailsByOrgBranchIdBySectionId(
                      orgId, branchId, section.getId());
            } else {
              counter =
                  this.counterService.getCounterDetailsByOrgIdBySectionId(orgId, section.getId());
            }
            map.put("SectionName", section.getSectionName());
            map.put("DisplayName", section.getDisplayName());
            map.put("counter", counter);
            list.add(map);
          }
        }
      } else {
        return new ResponseEntity<>(list, HttpStatus.UNAUTHORIZED);
      }
    } catch (final RuntimeException ex) {
      LOGGER.error("getSectionDetails" + ex, ex);
    }
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.UPDATE_SECTION_DETAILS)
  public final Map<String, String> updateSectionDetails(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String sectionDetails)
      throws JSONException, IOException {

    final JSONObject section = new JSONObject(sectionDetails);
    final Long id = section.getLong("id");
    final String key = section.getString("key");
    final String value = section.getString("value");

    final Map<String, String> sectionUpdate = new HashMap<>();
    try {
      final Section sections = this.sectionService.getSectionDetailsBySectionId(id);
      if (key.equals("sectionName")) {
        sections.setSectionName(value);
        sectionUpdate.put("sectionUpdate", "sectionDetails updated for this id" + id);

      } else if (key.equals("displayName")) {
        sections.setDisplayName(value);
        sectionUpdate.put("sectionUpdate", "sectionDetails updated for this id" + id);
      } else {

        sectionUpdate.put("sectionUpdate", "key Name does not match");
      }
      this.sectionService.update(sections);

    } catch (final RuntimeException ex) {
      LOGGER.error("sectionController update" + ex, ex);
    }
    return sectionUpdate;
  }

  @SuppressWarnings("unused")
  @DeleteMapping(path = RouteConstants.DELETE_SECTION_DETAILS)
  public final void deleteSectionDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long sectionId = Long.parseLong(request.getParameter("id"));
    Section section = null;
    Boolean result = null;
    try {
      // section = this.sectionService.delete(sectionId);
      section = this.sectionService.getSectionDetailsBySectionId(sectionId);
      result = this.sectionService.delete(section.getId());
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "deleteSectionDetails" + ex);
    }
  }

  @PostMapping(path = RouteConstants.UPDATE_SECTION_STATUS)
  public final Map<String, String> updateCounterstatus(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String sectionDetails)
      throws JSONException, IOException {

    final JSONObject section = new JSONObject(sectionDetails);
    final long id = section.getLong("id");
    final String status = section.getString("sectionStatus");

    final Map<String, String> sectionUpdate = new HashMap<>();
    try {

      final Section sections = this.sectionService.getSectionDetailsBySectionId(id);

      if (status.equalsIgnoreCase("true")) {
        sections.setStatus(Constants.STATUSFALSE);
        sectionService.update(sections);

      } else {

        sections.setStatus(Constants.STATUSTRUE);
        sectionService.update(sections);
      }

      sectionUpdate.put(
          "sectionUpdate", "Section Details Updated For   " + sections.getSectionName());

    } catch (final RuntimeException ex) {
      LOGGER.error("sectionUpdateDetails" + ex, ex);
    }
    return sectionUpdate;
  }
}
