package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Rules;
import com.a2zbill.services.RulesService;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RulesController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RulesController.class);

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private RulesService rulesService;

  @PostMapping(path = RouteConstants.SAVE_RULES_DETAILS)
  public final Rules addRulesDetails(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws Exception {

    final JSONObject rules = new JSONObject(payload);
    final JSONObject rulesDetails = (JSONObject) rules.get("groups");
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final Rules rulesObj = new Rules();

    try {
      rulesObj.setRuleName(request.getParameter("ruleName"));
      rulesObj.setBranchId(employeeDetails.getBranch());
      rulesObj.setOrgId(employeeDetails.getOrganisation());
      if (rulesDetails.has(Constants.BILLINGOPERATOR)
          && rulesDetails.has(Constants.VISITSOPERATOR)
          && rulesDetails.has(Constants.REWARDSOPERATOR)) {

        final String billingOperator = rulesDetails.getString(Constants.BILLINGOPERATOR);
        final String billingAmount = rulesDetails.getString(Constants.BILLINGAMOUNT);
        final String visitsOperator = rulesDetails.getString(Constants.VISITSOPERATOR);
        final String visitsNumber = rulesDetails.getString(Constants.VISITSNUMBER);
        final String rewardsOperator = rulesDetails.getString(Constants.REWARDSOPERATOR);
        final String rewardPoints = rulesDetails.getString(Constants.REWARDPOINT);
        final String value =
            Constants.TOTALAMOUNT
                + billingOperator
                + billingAmount
                + " AND total_visits"
                + visitsOperator
                + visitsNumber
                + Constants.TOTALREWARDS
                + rewardsOperator
                + rewardPoints;
        final String queryExample = Constants.SELECTMESSAGE + value;
        rulesObj.setQuery(queryExample);
      } else if (rulesDetails.has(Constants.BILLINGOPERATOR)
          && rulesDetails.has(Constants.VISITSOPERATOR)) {

        final String billingOperator = rulesDetails.getString(Constants.BILLINGOPERATOR);
        final String billingAmount = rulesDetails.getString(Constants.BILLINGAMOUNT);
        final String visitsOperator = rulesDetails.getString(Constants.VISITSOPERATOR);
        final String visitsNumber = rulesDetails.getString(Constants.VISITSNUMBER);
        final String value =
            Constants.TOTALAMOUNT
                + billingOperator
                + billingAmount
                + " AND total_visits"
                + visitsOperator
                + visitsNumber;
        final String queryExample = Constants.SELECTMESSAGE + value;
        rulesObj.setQuery(queryExample);
      } else if (rulesDetails.has(Constants.BILLINGOPERATOR)
          && rulesDetails.has(Constants.REWARDSOPERATOR)) {

        final String billingOperator = rulesDetails.getString(Constants.BILLINGOPERATOR);
        final String billingAmount = rulesDetails.getString(Constants.BILLINGAMOUNT);
        final String rewardsOperator = rulesDetails.getString(Constants.REWARDSOPERATOR);
        final String rewardPoints = rulesDetails.getString(Constants.REWARDPOINT);
        final String value =
            Constants.TOTALAMOUNT
                + billingOperator
                + billingAmount
                + Constants.TOTALREWARDS
                + rewardsOperator
                + rewardPoints;
        final String queryExample = Constants.SELECTMESSAGE + value;
        rulesObj.setQuery(queryExample);
      } else if (rulesDetails.has(Constants.VISITSOPERATOR)
          && rulesDetails.has(Constants.REWARDSOPERATOR)) {

        final String rewardsOperator = rulesDetails.getString(Constants.REWARDSOPERATOR);
        final String rewardPoints = rulesDetails.getString(Constants.REWARDPOINT);
        final String visitsOperator = rulesDetails.getString(Constants.VISITSOPERATOR);
        final String visitsNumber = rulesDetails.getString(Constants.VISITSNUMBER);
        final String value =
            "total_visits"
                + visitsOperator
                + visitsNumber
                + Constants.TOTALREWARDS
                + rewardsOperator
                + rewardPoints;
        final String queryExample = Constants.SELECTMESSAGE + value;
        rulesObj.setQuery(queryExample);
      } else if (rulesDetails.has(Constants.BILLINGOPERATOR)) {
        final String billingOperator = rulesDetails.getString(Constants.BILLINGOPERATOR);
        final String billingAmount = rulesDetails.getString(Constants.BILLINGAMOUNT);
        final String value = Constants.TOTALAMOUNT + billingOperator + billingAmount;
        final String queryExample = Constants.SELECTMESSAGE + value;
        rulesObj.setQuery(queryExample);
      } else if (rulesDetails.has(Constants.REWARDSOPERATOR)) {
        final String rewardsOperator = rulesDetails.getString(Constants.REWARDSOPERATOR);
        final String rewardPoints = rulesDetails.getString(Constants.REWARDPOINT);
        final String value = "total_rewards" + rewardsOperator + rewardPoints;
        final String queryExample = Constants.SELECTMESSAGE + value;
        rulesObj.setQuery(queryExample);
      } else if (rulesDetails.has(Constants.VISITSOPERATOR)) {
        final String visitsOperator = rulesDetails.getString(Constants.VISITSOPERATOR);
        final String visitsNumber = rulesDetails.getString(Constants.VISITSNUMBER);
        final String value = "total_visits" + visitsOperator + visitsNumber;
        final String queryExample = Constants.SELECTMESSAGE + value;
        rulesObj.setQuery(queryExample);
      }
      this.rulesService.save(rulesObj);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "RulesControlerr in the save() of" + ex);
    }
    return rulesObj;
  }

  @PostMapping(path = RouteConstants.UPDATE_RULES_DETAILS)
  public final void updateRules(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {

    final JSONObject rules = new JSONObject(payload);
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    try {
      final Rules ruleDetails = this.rulesService.getRuleDetailsByRuleId(rules.getLong("ruleId"));
      ruleDetails.setRuleName(rules.getString("ruleName"));
      ruleDetails.setQuery(rules.getString("query"));
      ruleDetails.setBranchId(employeeDetails.getBranch());
      ruleDetails.setOrgId(employeeDetails.getOrganisation());
      this.rulesService.update(ruleDetails);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "rulesController in the update() of" + ex);
    }
  }

  @GetMapping(path = RouteConstants.GET_RULES_DETAILS)
  public final ResponseEntity<Object> getRuleDetails(
      final HttpServletRequest request, final HttpServletResponse response) {

    Rules rule = null;

    try {
      final String ruleId = request.getParameter("ruleId");
      if (ruleId != null && !ruleId.isEmpty()) {
        rule = this.rulesService.getRuleDetailsByRuleId(Long.parseLong(ruleId));
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.error("RuleDetails" + ex);
    }
    return new ResponseEntity<>(rule, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.GET_ALL_RULES_DETAILS)
  public final List<Rules> getRules() {

    List<Rules> rules = new ArrayList<Rules>();
    try {
      rules = this.rulesService.getAllRulesDetails();
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getRulesDetails" + ex);
    }
    return rules;
  }

  @GetMapping(path = RouteConstants.RULES_DETAILS_BY_ORGIDANDBRANCHID)
  public final ResponseEntity<Object> getRulesDetailsByOrgAndBranch(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<Rules> rules = null;
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          rules = this.rulesService.getRulesDetailsByOrg(orgId);
        } else {
          rules = this.rulesService.getRulesDetailsByOrgAndBranch(orgId, branchId);
        }
      } else {
        return new ResponseEntity<>(rules, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.info(Constants.MARKER, "getRulesDetailsByOrgAndBranch" + ex);
    }
    return new ResponseEntity<>(rules, HttpStatus.OK);
  }
}
