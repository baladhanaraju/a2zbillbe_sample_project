package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductVariation;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.ProductVariationService;
import com.a2zbill.services.VariationOptionsService;
import com.a2zbill.services.VariationTypesService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductVariationController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductVariationController.class);

  @Autowired private ProductVariationService productVariationService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private OrganisationService organisationService;

  @Autowired private VariationTypesService variationTypesService;

  @Autowired private VariationOptionsService variationOptionsService;

  @Autowired private ProductService productService;

  @PostMapping(path = RouteConstants.SAVE_PRODUCT_VARIATION_DETAILS)
  public final ProductVariation addProductVariation(
      @RequestBody final String productVariationDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final long orgId = employeeDetails.getOrganisation().getId();
    final Branch branch = employeeDetails.getBranch();
    final JSONObject jsonObject = new JSONObject(productVariationDetails);
    final ProductVariation productVariation = new ProductVariation();
    try {
      final Long productId = jsonObject.getLong(Constants.PRODUCTID);
      final Long variationOptionId = jsonObject.getLong(Constants.VARIATOINOPTIONID);
      final Long variationTypeId = jsonObject.getLong(Constants.VARIATIONTYPEID);
      final BigDecimal price = BigDecimal.valueOf(jsonObject.getDouble(Constants.PRICE));
      final String description = jsonObject.getString(Constants.DESCRIPTION);
      productVariation.setBranch(branch);
      productVariation.setDescription(description);
      productVariation.setOrganisation(organisationService.getOrganisationById(orgId));
      productVariation.setProduct(productService.getProductsById(productId));
      productVariation.setVariationOption(
          variationOptionsService.getVariationOptionsById(variationOptionId));
      productVariation.setVariationType(
          variationTypesService.getVariationTypesById(variationTypeId));
      productVariation.setPrice(price);
      productVariationService.save(productVariation);
    } catch (final Exception em) {
      LOGGER.error(Constants.MARKER, "Saving ProductVariation" + em);
    }

    return productVariation;
  }

  @GetMapping(path = RouteConstants.GET_ALL_PRODUCTVARIATION_DETAILS)
  public final ResponseEntity<Object> getAllProductVariationDetails(
      final HttpServletRequest request, final HttpServletResponse response) {

    List<ProductVariation> productVariation = new ArrayList<ProductVariation>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branch = identifier.getBranchId();

        if (branch == 0) {
          productVariation = productVariationService.getProductVariationDetailsByOrgId(orgId);
        } else {
          productVariation =
              productVariationService.getProductVariationDetailsByOrgIdAndBranchId(orgId, branch);
        }

      } else {
        return new ResponseEntity<>(productVariation, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getProudctVariationDetails" + e);
    }
    return new ResponseEntity<>(productVariation, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.UPDATE_PRODUCT_VARIATION_DETAILS)
  public final Map<String, String> updateProductVariationDetails(
      @RequestBody final String productVariationDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException {

    final JSONObject jsonObject = new JSONObject(productVariationDetails);
    final Long id = jsonObject.getLong("id");
    final String key = jsonObject.getString("key");
    final String value = jsonObject.getString("value");

    final Map<String, String> productVariationUpdate = new HashMap<>();
    try {
      final ProductVariation productVariation =
          productVariationService.getProductVariationDetailsByProductVariationId(id);
      if (key.equals(Constants.PRICE)) {
        productVariation.setPrice(new BigDecimal(value));
      } else if (key.equals(Constants.DESCRIPTION)) {
        productVariation.setDescription(value);
      } else {
        productVariationUpdate.put("productVariationUpdate", "key Name does not match");
      }

      productVariationService.update(productVariation);
      productVariationUpdate.put(
          "productVariationUpdate", "productVariation updated for this id " + id);
    } catch (final RuntimeException e) {
      LOGGER.error("productVariationController update" + e, e);
    }
    return productVariationUpdate;
  }

  @GetMapping(path = RouteConstants.GET_PRODUCT_VARIATION_DETAILS)
  public final List<Map<String, Object>> getproductVariationDetails(
      final HttpServletRequest request, final HttpServletResponse response) {
    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Branch branch = employeeDetails.getBranch();
    final List<Object[]> productVariationDetails =
        productVariationService.getproductVariationDetails(
            employeeDetails.getOrganisation().getId(), employeeDetails.getBranch().getId());
    final List<Map<String, Object>> productList = new ArrayList<>();
    if (branch != null) {
      for (final Object[] products : productVariationDetails) {
        final Map<String, Object> productsMap = new HashMap<>();
        final String newProductName = products[1] + "-" + products[23];
        productsMap.put(Constants.PRODUCTID, products[0]);
        productsMap.put(Constants.PRODUCTNAME, newProductName);
        productsMap.put(Constants.HSNNUMBERS, products[2]);
        productsMap.put("productPrice", products[3]);
        productsMap.put("productCount", products[4]);
        productsMap.put("packageFlag", products[5]);
        productsMap.put("includingTaxFlag", products[6]);
        productsMap.put("productCategoryId", products[7]);
        productsMap.put("productCategoryName", products[8]);
        productsMap.put("CategoryTypeId", products[9]);
        productsMap.put("CategoryTypeName", products[10]);
        productsMap.put("productFeatures", products[11]);
        productsMap.put("ProductImage", products[12]);
        productsMap.put("OrganisationId", products[13]);
        productsMap.put("OrganisationName", products[14]);
        productsMap.put("BranchId", products[15]);
        productsMap.put("BranchName", products[16]);
        productsMap.put("CGST", products[17]);
        productsMap.put("SGST", products[18]);
        productsMap.put("IGST", products[19]);
        productsMap.put("vegFlag", products[20]);
        productsMap.put("productVariationId", products[21]);
        productsMap.put(Constants.VARIATOINOPTIONID, products[22]);
        productsMap.put("optionName", products[23]);
        productsMap.put("optionDisplayName", products[24]);
        productsMap.put("quantityMeasurementId", products[25]);
        productsMap.put("quantity", products[26]);
        productsMap.put(Constants.DISPLAYNAME, products[27]);
        productsMap.put("basicUnit", products[28]);
        productsMap.put("basicunitDisplayname", products[29]);
        productsMap.put("conversionFactor", products[30]);
        productsMap.put(Constants.VARIATIONTYPEID, products[31]);
        productsMap.put("variationName", products[32]);
        productsMap.put(Constants.DISPLAYNAME, products[33]);
        productsMap.put(Constants.PRICE, products[34]);
        productsMap.put(Constants.DESCRIPTION, products[35]);
        productsMap.put("actualWeight", products[36]);
        productsMap.put("code", products[37]);
        productList.add(productsMap);
      }
    } else {
      for (final Object[] products : productVariationDetails) {
        final Map<String, Object> productsMap = new HashMap<>();
        productsMap.put(Constants.PRODUCTID, products[0]);
        productsMap.put(Constants.PRODUCTNAME, products[1]);
        productsMap.put(Constants.HSNNUMBERS, products[2]);
        productsMap.put("productPrice", products[3]);
        productsMap.put("productCount", products[4]);
        productsMap.put("packageFlag", products[5]);
        productsMap.put("includingTaxFlag", products[6]);
        productsMap.put("productCategoryId", products[7]);
        productsMap.put("productCategoryName", products[8]);
        productsMap.put("CategoryTypeId", products[9]);
        productsMap.put("CategoryTypeName", products[10]);
        productsMap.put("productFeatures", products[11]);
        productsMap.put("ProductImage", products[12]);
        productsMap.put("OrganisationId", products[13]);
        productsMap.put("OrganisationName", products[14]);
        productsMap.put("BranchId", products[15]);
        productsMap.put("BranchName", products[16]);
        productsMap.put("CGST", products[17]);
        productsMap.put("SGST", products[18]);
        productsMap.put("IGST", products[19]);
        productsMap.put("vegFlag", products[20]);
        productsMap.put("productVariationId", products[21]);
        productsMap.put(Constants.VARIATOINOPTIONID, products[22]);
        productsMap.put("optionName", products[23]);
        productsMap.put("optionDisplayName", products[24]);
        productsMap.put("quantityMeasurementId", products[25]);
        productsMap.put("quantity", products[26]);
        productsMap.put(Constants.DISPLAYNAME, products[27]);
        productsMap.put("basicUnit", products[28]);
        productsMap.put("basicunitDisplayname", products[29]);
        productsMap.put("conversionFactor", products[30]);
        productsMap.put(Constants.VARIATIONTYPEID, products[31]);
        productsMap.put("variationName", products[32]);
        productsMap.put(Constants.DISPLAYNAME, products[33]);
        productsMap.put(Constants.PRICE, products[34]);
        productsMap.put(Constants.DESCRIPTION, products[35]);
        productsMap.put("actualWeight", products[36]);
        productsMap.put("code", products[37]);
        productList.add(productsMap);
      }
    }

    return productList;
  }

  @SuppressWarnings("unused")
  @GetMapping(path = RouteConstants.GET_ALLPRODUCTS_VARIATION_DETAILS)
  public final ResponseEntity<Object> getAllproductDetails(
      final HttpServletRequest request, final HttpServletResponse response) {

    final List<Map<String, Object>> listOfProducts = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        List<Product> productDetails = null;
        List<ProductVariation> productVariation = null;

        if (branchId != 0) {
          productDetails = this.productService.getAllProducts(orgId, branchId);
        } else {
          productDetails = this.productService.getAllProductsByOrgId(orgId);
        }
        if (productDetails != null) {
          for (final Product product : productDetails) {
            final Map<String, Object> productsMap = new HashMap<>();
            productVariation =
                productVariationService.getProductVariationByProductIdAndOrgBranchId(
                    product.getId(), orgId, branchId);
            productsMap.put("products", product);
            productsMap.put("productVariations", productVariation);
            listOfProducts.add(productsMap);
          }
        }
      } else {
        return new ResponseEntity<>(listOfProducts, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error("getAllProductvariationDetails", ex);
    }
    return new ResponseEntity<>(listOfProducts, HttpStatus.OK);
  }
}
