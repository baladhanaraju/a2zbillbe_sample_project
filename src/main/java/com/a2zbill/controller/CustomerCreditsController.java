package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.CustomerCredits;
import com.a2zbill.domain.CustomerHistory;
import com.a2zbill.domain.ExchangeHistory;
import com.a2zbill.domain.Product;
import com.a2zbill.services.BillingService;
import com.a2zbill.services.CustomerCreditsService;
import com.a2zbill.services.CustomerHistoryService;
import com.a2zbill.services.ExchangeHistoryService;
import com.a2zbill.services.ProductService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.paytm.domain.Credits;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.OrganisationService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerCreditsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerCreditsController.class);

  @Value("${jwt.header}")
  private String tokenHeader;

  @Autowired private CustomerCreditsService customerCreditsService;

  @Autowired private ProductService productService;

  @Autowired private BillingService billingService;

  @Autowired private CustomerService customerService;

  @Autowired private CustomerHistoryService customerHistoryService;

  @Autowired private ExchangeHistoryService exchangeHistoryService;

  @Autowired private OrganisationService organisationService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @SuppressWarnings("unused")
  @PostMapping(value = RouteConstants.SAVE_CUSTOMERCREDIT_DETAILS)
  public final Map<String, Object> addCustomerCreditsDetails(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload) {

    final Map<String, Object> map = new HashMap<>();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long orgId = employeeDetails.getOrganisation().getId();
      CustomerCredits customerCredits = new CustomerCredits();
      final CustomerHistory customerHistory = new CustomerHistory();
      final ExchangeHistory exchangeHistory = new ExchangeHistory();
      final String selectType = request.getParameter("returnType");
      final long billId = Long.parseLong(request.getParameter("billId"));
      final JSONObject customerCreditsDetails = new JSONObject(payload);
      String returnOption = "";
      String returnValue = "";
      Long customerId = null;
      Billing billing = null;
      final Branch branchId = employeeDetails.getBranch();

      if (branchId != null) {
        billing =
            this.billingService.getBillingDetailsByBillIdAndOrgIdAndBranchId(
                billId, orgId, branchId.getId());
      } else {
        billing = this.billingService.getBillingDetailsByBillIdAndOrgId(billId, orgId);
      }
      try {
        customerId = billing.getCustomer().getCustomerId();
      } catch (final Exception ex) {
        map.put("customerId", "customerId does not exist");
      }
      if (selectType.contains("replace")) {
        String[] data = selectType.split("\\");
        returnValue = data[0];
        returnOption = data[1];
      } else {
        returnValue = selectType;
      }
      if (branchId != null) {
        exchangeHistory.setBranch(branchId);
      } else {
        exchangeHistory.setBranch(null);
      }
      Organisation organisation =
          this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
      Product productId =
          this.productService.getProductsById(customerCreditsDetails.getLong("productId"));
      exchangeHistory.setOrganisation(organisation);
      exchangeHistory.setAmount(new BigDecimal(customerCreditsDetails.getInt("amount")));
      exchangeHistory.setDate(new Date(System.currentTimeMillis()));
      exchangeHistory.setBillingDetails(billing);
      exchangeHistory.setExchangeType(customerCreditsDetails.getString("exchangeType"));
      exchangeHistory.setProductId(productId);
      exchangeHistory.setProductName(productId.getProductName());
      map.put("exchangeHistory", exchangeHistory);
      this.exchangeHistoryService.save(exchangeHistory);

      if (returnValue.equals("credit")) {
        try {
          customerCredits = this.customerCreditsService.getCustomerCreditsByCustomerId(customerId);
          customerCredits.setDebitAmount(
              new BigDecimal(customerCreditsDetails.getInt("debitAmount")));
          customerCredits.setModifiedDate(new Date(System.currentTimeMillis()));
          this.customerCreditsService.update(customerCredits);
          map.put("customerCredits", customerCredits);
        } catch (Exception ex) {
          customerCredits.setCreateDate(new Date(System.currentTimeMillis()));
          customerCredits.setModifiedDate(new Date(System.currentTimeMillis()));
          if (branchId != null) {
            customerCredits.setBranch(branchId);
          } else {
            customerCredits.setBranch(null);
          }
          Customer customer = this.customerService.getCustomerDetailsBycustomerId(customerId);
          customerCredits.setOrganisation(organisation);
          customerCredits.setCustomer(customer);
          customerCredits.setDebitAmount(
              new BigDecimal(customerCreditsDetails.getInt("debitAmount")));
          customerCredits.setCreditAmount(
              new BigDecimal(customerCreditsDetails.getInt("creditAmount")));
          map.put("customerCredits", customerCredits);
          this.customerCreditsService.save(customerCredits);
        }
      }
      customerHistory.setDate(new Date(System.currentTimeMillis()));
      if (branchId != null) {
        customerHistory.setBranch(branchId);
      } else {
        customerHistory.setBranch(null);
      }
      Customer customer = this.customerService.getCustomerDetailsBycustomerId(customerId);
      customerHistory.setOrganisation(organisation);
      customerHistory.setCustomer(customer);
      customerHistory.setDebitAmount(new BigDecimal(customerCreditsDetails.getInt("debitAmount")));
      customerHistory.setCreditAmount(
          new BigDecimal(customerCreditsDetails.getInt("creditAmount")));
      map.put("customerHistory", customerHistory);
      this.customerHistoryService.save(customerHistory);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "addCustomerCreditsDetails" + ex);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.CUSTOMER_CREDIT_ORG)
  public final ResponseEntity<Object> getCreditDetails(
      final HttpServletRequest request, final HttpServletResponse response) {

    Credits credits = new Credits();
    try {
      final String authToken = request.getHeader(tokenHeader);
      final String token = authToken.substring(7);
      final Identifier identifier = jwtTokenUtil.getSubjectFromToken(token);
      final long userId = identifier.getUserId();
      Customer customerDetails = null;
      if (identifier.getUserType().equalsIgnoreCase("C")) {
        customerDetails = customerService.getCustomerDetailsBycustomerId(userId);
      }

      credits =
          customerCreditsService.getAllCreditsByOrgidCustomer(
              customerDetails.getOrganisation().getId());

    } catch (final NullPointerException e) {
      LOGGER.error("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getCustomerCreditDetails" + e);
    }

    return new ResponseEntity<>(credits, HttpStatus.OK);
  }
}
