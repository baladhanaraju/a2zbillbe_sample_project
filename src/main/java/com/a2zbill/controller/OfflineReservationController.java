package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.Reservation;
import com.a2zbill.services.CounterService;
import com.a2zbill.services.ReservationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsss.basic.config.KafkaSender;
import com.tsss.basic.domain.AuthProvider;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.CustomerService;
import com.tsss.basic.service.EmployeeService;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OfflineReservationController {

  private static final Logger LOGGER = LoggerFactory.getLogger(OfflineReservationController.class);

  @Autowired private CounterService counterService;

  @Autowired private ReservationService reservationService;

  @Autowired private CustomerService customerService;

  @Autowired private KafkaSender kafkaSender;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  /*
   * we are getting multiple reservation records in offline side i need to save
   * those all details
   */
  /*
   * save and cancel payload
   * [{"reservetable":1,"AvailableCount":2,"starttime":"10:00","endtime": "11:00",
   * "customerMobilenumber":"8179327347","date":"2019-08-20","reservetype":
   * "reserve","note":"iuoj", "beflag":"false","status":"Open",
   * "_id":"5a4wkziqut:1566285038245","_rev":
   * "1-eaf31df1fd61b77afeaca7c61dbd87ee"} ]
   */
  @SuppressWarnings("all")
  @PostMapping(path = RouteConstants.SAVE_OFFLINE_RESERVATION_DETAILS)
  public final Map<String, Object> addOfflineReservationDetails(
      @RequestBody final String reservationDetails,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException, Exception {

    final JSONArray reservationArray = new JSONArray(reservationDetails);
    final Map<String, Object> producerMap = new HashMap<>();

    for (int i = 0; i < reservationArray.length(); i++) {

      final JSONObject reservation = (JSONObject) reservationArray.get(i);
      final long reserveTable = reservation.getLong("reservetable");
      final String starttime = reservation.getString("starttime");
      final String reserveType = reservation.getString("reservetype");
      final String note = reservation.getString("note");
      final long kidsCount = reservation.getLong("kidsCount");
      final long adultCount = reservation.getLong("adultCount");
      final String mobileNumber = reservation.getString("customerMobilenumber");
      final String customerName = reservation.getString("customerName");
      final String dateString = reservation.getString("date");
      final String status = reservation.getString("status");
      final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      final SimpleDateFormat sdftime = new SimpleDateFormat("HH:mm");
      final Date date = sdf.parse(dateString);
      final ObjectMapper objMapper = new ObjectMapper();
      final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final Branch branch = employeeDetails.getBranch();
      final long orgId = employeeDetails.getOrganisation().getId();
      final Reservation reservations = new Reservation();
      final Date startTime = sdftime.parse(starttime);
      final SimpleDateFormat df = new SimpleDateFormat("HH:mm");
      final Date d = df.parse(starttime);
      final Calendar cal = Calendar.getInstance();
      cal.setTime(d);
      cal.add(Calendar.MINUTE, 15);
      cal.add(Calendar.MINUTE, -30);

      final Customer customer =
          customerService.getCustomerByMobileNumberAndByOrgIdBranchId(
              mobileNumber, orgId, branch.getId());
      if (customer != null) {
        reservations.setCustomer(customer);
        producerMap.put("PERSON_NAME", customer.getCustomerName());
        producerMap.put("CustomerStatus", "Customer exists");
      } else {
        producerMap.put("CustomerStatus", "Customer doesn't exists");
        final Customer customer1 = new Customer();
        customer1.setMobileNumber(reservation.getString("customerMobilenumber"));
        customer1.setCustomerName(reservation.getString("customerName"));
        customer1.setBranch(branch);
        customer1.setOrganisation(employeeDetails.getOrganisation());
        customer1.setSocialProvider(AuthProvider.local);
        customerService.save(customer1);
        reservations.setCustomer(customer1);
        producerMap.put("PERSON_NAME", customer1.getCustomerName());
      }
      if (reserveTable == 0) {
        final Reservation exitreservations =
            reservationService.getReservationDetailsBySlotTimeBranch(
                date, starttime, reserveTable, orgId, branch.getId());

        if (exitreservations != null && exitreservations.getStatus().equalsIgnoreCase("Open")
            || exitreservations.getStatus().equalsIgnoreCase("allotted")
            || exitreservations.getStatus().equalsIgnoreCase("Waiting")) {

          exitreservations.setStatus(status);
          reservationService.update(exitreservations);
          producerMap.put(
              "ReservationStatus",
              "Reservation  already exists for the time " + df.format(startTime) + "!");
        } else {
          reservations.setStartTime(starttime);
          reservations.setReserveDate(date);
          reservations.setKidscount(kidsCount);
          reservations.setAdultcount(adultCount);
          reservations.setStatus(status);
          reservations.setCreatedDate(new Date());
          reservations.setModifiedDate(new Date());
          reservations.setOrganisation(employeeDetails.getOrganisation());
          reservations.setBranch(branch);
          reservations.setReservationType(reserveType);
          reservations.setNote(note);
          reservationService.save(reservations);
          // to send kafka producer and topic is mail
          producerMap.put("ReservationStatus", "Reservation is successful!!");
          producerMap.put("template", "reservation");
          producerMap.put("orgId", employeeDetails.getOrganisation().getId());
          producerMap.put("ORG_NAME", employeeDetails.getOrganisation().getOrgName());
          producerMap.put("branchId", branch.getId());
          producerMap.put("BRANCH_NAME", branch.getBranchName());
          producerMap.put("mobileNumber", mobileNumber);
          producerMap.put("customerName", customerName);
          producerMap.put("reserveTable", reserveTable);
          producerMap.put("date", sdf.format(date));
          producerMap.put("AdultCount", adultCount);
          producerMap.put("kidsCount", kidsCount);
          producerMap.put("startTime", starttime);
          producerMap.put("status", status);
          producerMap.put("createdDate", sdf.format(new Date()));
          producerMap.put("modifiedDate", sdf.format(new Date()));
          producerMap.put("reservationType", reserveType);
          producerMap.put("note", note);
          final String kafkadata = objMapper.writeValueAsString(producerMap);
          kafkaSender.sendMail(kafkadata);
        }
      } else {

        final Reservation exitreservations =
            reservationService.getReservationDetailsBySlotTimeBranch(
                date, starttime, reserveTable, orgId, branch.getId());
        if (exitreservations != null) {
          if (exitreservations.getStatus().equalsIgnoreCase("Open")
              || exitreservations.getStatus().equalsIgnoreCase("allotted")
              || exitreservations.getStatus().equalsIgnoreCase("Waiting")) {

            exitreservations.setStatus(status);
            reservationService.update(exitreservations);
            producerMap.put(
                "ReservationStatus",
                "Reservation for this table "
                    + reserveTable
                    + " already exists for the time "
                    + df.format(startTime)
                    + "!");
          } else {
            reservations.setStartTime(starttime);
            reservations.setReserveTable(counterService.getCounterDetailsById(reserveTable));
            reservations.setReserveDate(date);
            reservations.setKidscount(kidsCount);
            reservations.setAdultcount(adultCount);
            reservations.setStatus(status);
            reservations.setCreatedDate(new Date());
            reservations.setModifiedDate(new Date());
            reservations.setOrganisation(employeeDetails.getOrganisation());
            reservations.setBranch(branch);
            reservations.setReservationType(reserveType);
            reservations.setNote(note);
            reservationService.save(reservations);
            // to send kafka producer and topic is mail
            producerMap.put("ReservationStatus", "Reservation is successful!!");
            producerMap.put("template", "reservation");
            producerMap.put("orgId", employeeDetails.getOrganisation().getId());
            producerMap.put("ORG_NAME", employeeDetails.getOrganisation().getOrgName());
            producerMap.put("branchId", branch.getId());
            producerMap.put("BRANCH_NAME", branch.getBranchName());
            producerMap.put("mobileNumber", mobileNumber);
            producerMap.put("customerName", customerName);
            producerMap.put("reserveTable", reserveTable);
            producerMap.put("date", sdf.format(date));
            producerMap.put("AdultCount", adultCount);
            producerMap.put("kidsCount", kidsCount);
            producerMap.put("startTime", starttime);
            producerMap.put("status", status);
            producerMap.put("createdDate", sdf.format(new Date()));
            producerMap.put("modifiedDate", sdf.format(new Date()));
            producerMap.put("reservationType", reserveType);
            producerMap.put("note", note);
            final String kafkadata = objMapper.writeValueAsString(producerMap);
            kafkaSender.sendMail(kafkadata);
          }

        } else {

          reservations.setStartTime(starttime);
          reservations.setReserveDate(date);
          reservations.setReserveTable(counterService.getCounterDetailsById(reserveTable));
          reservations.setKidscount(kidsCount);
          reservations.setAdultcount(adultCount);
          reservations.setStatus(status);
          reservations.setCreatedDate(new Date());
          reservations.setModifiedDate(new Date());
          reservations.setOrganisation(employeeDetails.getOrganisation());
          reservations.setBranch(branch);
          reservations.setReservationType(reserveType);
          reservations.setNote(note);
          reservationService.save(reservations);
          // to send kafka producer and topic is mail
          producerMap.put("ReservationStatus", "Reservation is successful!!");
          producerMap.put("template", "reservation");
          producerMap.put("orgId", employeeDetails.getOrganisation().getId());
          producerMap.put("ORG_NAME", employeeDetails.getOrganisation().getOrgName());
          producerMap.put("branchId", branch.getId());
          producerMap.put("BRANCH_NAME", branch.getBranchName());
          producerMap.put("mobileNumber", mobileNumber);
          producerMap.put("customerName", customerName);
          producerMap.put("reserveTable", reserveTable);
          producerMap.put("date", sdf.format(date));
          producerMap.put("AdultCount", adultCount);
          producerMap.put("kidsCount", kidsCount);
          producerMap.put("startTime", starttime);
          producerMap.put("status", status);
          producerMap.put("createdDate", sdf.format(new Date()));
          producerMap.put("modifiedDate", sdf.format(new Date()));
          producerMap.put("reservationType", reserveType);
          producerMap.put("note", note);
          final String kafkadata = objMapper.writeValueAsString(producerMap);
          kafkaSender.sendMail(kafkadata);
        }
      }
    }
    return producerMap;
  }

  @PostMapping(path = RouteConstants.UPDATE_OFFLINE_RESERVATION_DETAILS)
  public final Map<String, String> updateOfflineReservationDetails(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException, IOException, ParseException {

    final JSONObject reservation = new JSONObject(reservationDetails);
    final Long id = reservation.getLong("id");
    final String key = reservation.getString("key");
    final String value = reservation.getString("value");

    final Map<String, String> reservationUpdate = new HashMap<>();
    try {
      final Reservation reservations =
          this.reservationService.getReservationDetailsByReservationid(id);
      if (key.equals("allocatedTable")) {
        reservations.setAllocatedTable(Integer.parseInt(value));
        reservationUpdate.put("reservationUpdate", "reservationDetails updated for this id  " + id);
        reservations.setStatus(Constants.CLOSESTATUS);
        reservations.setModifiedDate(new Date(System.currentTimeMillis()));

      } else {

        reservationUpdate.put("reservationUpdate", "key Name does not match");
      }
      this.reservationService.update(reservations);

    } catch (final RuntimeException ex) {
      LOGGER.error("reservationController update" + ex, ex);
    }
    return reservationUpdate;
  }

  @GetMapping(path = RouteConstants.GET_ALL_OFFLINE_RESERVATION_DETAILS)
  public final ResponseEntity<Object> getOfflineReservationDetailsByOrgAndBranch(
      final HttpServletRequest request, final HttpServletResponse responce) {
    List<Reservation> reservation = new ArrayList<>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        if (branchId == 0) {
          reservation = this.reservationService.getReservationDetailsByOrg(orgId);
        } else {
          reservation =
              this.reservationService.getReservationDetailsByOrgAndBranch(orgId, branchId);
        }
      } else {
        return new ResponseEntity<>(reservation, HttpStatus.UNAUTHORIZED);
      }
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "getReservationDetails " + ex);
    }
    return new ResponseEntity<>(reservation, HttpStatus.OK);
  }

  @SuppressWarnings("all")
  @PostMapping(path = RouteConstants.CANCLE_OFFLINE_RESERVATION_DETAILS)
  public final Map<String, String> updateOfflineReservationDetailsByid(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String reservationDetails)
      throws JSONException, IOException, ParseException {

    final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails =
        this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Map<String, Object> producerMap = new HashMap<>();
    final ObjectMapper objMapper = new ObjectMapper();

    final JSONArray reservationArray = new JSONArray(reservationDetails);

    for (int i = 0; i < reservationArray.length(); i++) {

      final JSONObject reservation = (JSONObject) reservationArray.get(i);
      final Long id = reservation.getLong("id");

      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

      final Map<String, String> reservationUpdate = new HashMap<String, String>();
      try {
        final Reservation reservations =
            this.reservationService.getReservationDetailsByReservationid(id);
        reservations.setStatus("cancel");
        this.reservationService.update(reservations);

        producerMap.put("template", "cancellation");
        producerMap.put("orgId", reservations.getOrganisation().getId());
        producerMap.put("ORG_NAME", employeeDetails.getOrganisation().getOrgName());
        if (reservations.getBranch() == null) {
          producerMap.put("branchId", "null");
          producerMap.put("BRANCH_NAME", "null");
        } else {
          producerMap.put("branchId", reservations.getBranch().getId());
          producerMap.put("BRANCH_NAME", reservations.getBranch().getBranchName());
        }
        producerMap.put("mobileNumber", reservations.getCustomer().getMobileNumber());
        producerMap.put("reserveTable", reservations.getReserveTable());
        producerMap.put("PERSON_NAME", reservations.getCustomer().getCustomerName());
        producerMap.put("date", sdf.format(reservations.getReserveDate()));
        producerMap.put("kidsCount", reservations.getKidscount());
        producerMap.put("startTime", reservations.getStartTime());
        producerMap.put("startTime", reservations.getStartTime());
        producerMap.put("status", "CLOSE_STATUS");
        producerMap.put("createdDate", sdf.format(reservations.getCreatedDate()));
        producerMap.put("modifiedDate", sdf.format(reservations.getModifiedDate()));
        producerMap.put("reservationType", reservations.getReservationType());
        producerMap.put("note", reservations.getNote());
        final String kafkadata = objMapper.writeValueAsString(producerMap);

        this.kafkaSender.sendMail(kafkadata);
        reservationUpdate.put(
            "ReservationUpdate",
            reservations.getReserveTable() + " Table ReservationDetails Canceled Successfully");
      } catch (final RuntimeException ex) {
        LOGGER.error("reservationController update" + ex, ex);
      }
      // return reservationUpdate;
    }
    return null;
  }
}
