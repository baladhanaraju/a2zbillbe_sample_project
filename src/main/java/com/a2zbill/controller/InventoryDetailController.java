package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.InventoryHistory;
import com.a2zbill.domain.InventoryReports;
import com.a2zbill.domain.InventorySection;
import com.a2zbill.domain.InventorySectionData;
import com.a2zbill.domain.InventorySectionDataHistory;
import com.a2zbill.domain.InvoiceBill;
import com.a2zbill.domain.Item;
import com.a2zbill.domain.LooseProduct;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import com.a2zbill.services.InventoryDetailService;
import com.a2zbill.services.InventoryHistoryService;
import com.a2zbill.services.InventoryReportsService;
import com.a2zbill.services.InventorySectionDataHistoryService;
import com.a2zbill.services.InventorySectionDataService;
import com.a2zbill.services.InventorySectionService;
import com.a2zbill.services.InvoiceBillService;
import com.a2zbill.services.LooseProductService;
import com.a2zbill.services.ProductService;
import com.a2zbill.services.impl.ItemServiceImpl;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Vendor;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.VendorService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class InventoryDetailController {

  private static final Logger LOGGER = LoggerFactory.getLogger(InventoryDetailController.class);

  private final String LooseProductCode = "#w";

  @Autowired private ProductService productsService;

  @Autowired private ItemServiceImpl itemsService;

  @Autowired private LooseProductService looseProductService;

  @Autowired private BranchService branchService;

  @Autowired private InventoryHistoryService inventoryHistoryService;

  @Autowired private InventoryDetailService inventoryDetailService;

  @Autowired private VendorService vendorService;

  @Autowired private InvoiceBillService invoiceBillService;

  @Value("${jwt.header}")
  private String tokenHeader;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private InventorySectionService inventorySectionService;

  @Autowired private InventorySectionDataService inventorySectionDataService;

  @Autowired private InventorySectionDataHistoryService inventorySectionDataHistoryService;

  @Autowired private InventoryReportsService inventoryReportsService;

  @PostMapping(path = RouteConstants.INVENTORY_CARTPRODUCT_DETAILS)
  public final Map<String, Object> getproductDetails(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse responce)
      throws JSONException {

    Product productDetails = null;
    Item itemDetails = null;
    final Map<String, Object> productDetails1 = new HashMap<>();
    LooseProduct looseProduct = null;
    try {
      final JSONObject jsonObject = new JSONObject(payload);
      final String QTY = "1";
      final String code = jsonObject.getString("productcode");

      if (code.contains(LooseProductCode)) {
        final String[] codes = code.split(LooseProductCode);
        final String loosecode = codes[1];
        looseProduct = looseProductService.getLooseProductDetailsByCode(loosecode);
      } else {
        productDetails = productsService.getProductsByProductCode(code);
      }
      if (productDetails != null || looseProduct != null) {
        itemDetails = itemsService.getItemsByHsnNo(productDetails.getHsnNumber());
        if (itemDetails != null) {
          productDetails1.put("SGST", itemDetails.getPercentageDetails().getCGST());
          productDetails1.put("CGST", itemDetails.getPercentageDetails().getSGST());
          productDetails1.put("IGST", itemDetails.getPercentageDetails().getIGST());

        } else {
          productDetails1.put("SGST", 0);
          productDetails1.put("CGST", 0);
          productDetails1.put("IGST", 0);
        }
        productDetails1.put("ProductCode", productDetails.getCode());
        productDetails1.put("ProductName", productDetails.getProductName());
        productDetails1.put("HsnNumber", productDetails.getHsnNumber());
        productDetails1.put("ProductPrice", productDetails.getProductPrice());
        productDetails1.put("Quantity", QTY);

      } else {

        productDetails1.put("product", "Products Not Available, Please Add Products");
      }

    } catch (final Exception e) {
      LOGGER.error("savecartProductDetails" + e);
    }
    return productDetails1;
  }

  @PostMapping(path = RouteConstants.SAVE_INVENTORY)
  public final Map<String, Object> getInventoryDetail(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestParam(name = "file", required = false) final MultipartFile file,
      @RequestParam("data") final String payload)
      throws JSONException {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Map<String, Object> map = new HashMap<>();
    final long orgId = employeeDetails.getOrganisation().getId();
    final JSONObject jo = new JSONObject(payload);
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final JSONArray ja = jo.getJSONArray("items");
    InvoiceBill invoiceBill = new InvoiceBill();
    final String invoiceNumber = jo.getString("invoiceNumber");
    final String fromType = jo.getString("fromType");
    if (fromType.contentEquals("branch")) {
      try {

        final InvoiceBill invoicebill =
            invoiceBillService.getInvoiceBillByInvoiceNumber(invoiceNumber);
        if (invoicebill != null) {
          map.put("Message", "The invoiceNumber already Added");
          map.put("flag", "false");
          return map;
        } else {
          invoiceBill = new InvoiceBill();
          invoiceBill.setInvoiceNumber(invoiceNumber);
        }
        invoiceBill.setDate(sdf.parse(jo.getString("date")));
        invoiceBill.setCreatedDate(new Date());
        invoiceBill.setModifiedDate(new Date());
        if (!jo.get("discount").equals("")) {
          final double discount = jo.getDouble("discount");
          invoiceBill.setDiscountAmount(BigDecimal.valueOf(discount));
        }
        try {
          invoiceBill.setImage(file.getBytes());
        } catch (final NullPointerException ex) {
          invoiceBill.setImage(null);
        }
        if (jo.has("extractedData") && !jo.getString("extractedData").equals("")) {
          final Map<String, Object> extractedDataMap = new HashMap<>();
          extractedDataMap.put("data", jo.getString("extractedData"));
          invoiceBill.setExtractedData(extractedDataMap);
        }

        invoiceBill.setNote(jo.getString("note"));
        invoiceBill.setOrganisation(employeeDetails.getOrganisation());
        invoiceBill.setBranch(branchService.getBranchById(jo.getLong("fromId")));
        final double totalAmount = jo.getDouble("totalAmount");
        invoiceBill.setTotalAmount(BigDecimal.valueOf(totalAmount));
        invoiceBill.setVendorId(vendorService.getVendorDetailsById(jo.getLong("vendorId")));
        final double taxbleAmount = jo.getDouble("taxbleAmount");
        invoiceBill.setTaxableAmount(BigDecimal.valueOf(taxbleAmount));
        invoiceBillService.save(invoiceBill);
        for (int i = 0; i < ja.length(); i++) {
          final JSONObject jsonObject = ja.getJSONObject(i);
          final JSONObject selectedItemName = jsonObject.getJSONObject("selectedItemName");
          final Long productId = selectedItemName.getLong("id");
          final Product product = productsService.getProductsById(productId);
          final InventoryHistory inventoryHistory = new InventoryHistory();
          Vendor vendor = new Vendor();
          final long branchId = jo.getLong("fromId");
          InventoryDetail inventoryDetails = new InventoryDetail();
          vendor = vendorService.getVendorDetailsById(jo.getLong("vendorId"));
          final double quantity = jsonObject.getDouble("Quantity");
          final BigDecimal qty = BigDecimal.valueOf(quantity);
          InventoryReports inventoryReportDetails = null;

          InventoryDetail inventoryDetail =
              inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                  product.getId(), branchId);
          if (inventoryDetail != null) {
            final BigDecimal totalQuantity = inventoryDetail.getQuantity().add(qty);

            inventoryDetail.setQuantity(totalQuantity);
            final double rate = jsonObject.getDouble("Rate");
            inventoryDetail.setBuyingPrice(product.getProductPrice());
            inventoryDetail.setSellingPrice(BigDecimal.valueOf(rate));
            inventoryDetail.setDate(new Date(System.currentTimeMillis()));
            inventoryDetail.setVendor(vendor);
            inventoryDetailService.update(inventoryDetail);
            map.put("Message", "Products updated successfully");

            final InventoryDetail inventoryDetailsByProduct =
                inventoryDetailService.getInventoryDetailByProductNameAndBranchIdAndOrgId(
                    product.getProductName(), branchId, orgId);
            inventoryReportDetails =
                inventoryReportsService.getInventoryByDateAndInventoryId(
                    new Date(), inventoryDetailsByProduct.getInventoryId(), orgId, branchId);
            if (inventoryReportDetails != null) {
              final BigDecimal reportAvailableStockQuantity =
                  inventoryReportDetails.getStockQuantity();
              final BigDecimal presentStockQuantity = reportAvailableStockQuantity.add(qty);
              inventoryReportDetails.setStockQuantity(presentStockQuantity);
              inventoryReportDetails.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReportsService.update(inventoryReportDetails);
            } else {
              final InventoryReports inventoryReports = new InventoryReports();
              inventoryReports.setInventoryDetail(inventoryDetailsByProduct);
              inventoryReports.setStockQuantity(qty);
              inventoryReports.setConsumedQuantity(new BigDecimal(0));
              inventoryReports.setCreatedDate(new Date());
              inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setOrganisation(employeeDetails.getOrganisation());
              inventoryReports.setBranch(branchService.getBranchById(branchId));
              inventoryReportsService.save(inventoryReports);
            }

            inventoryHistory.setInventoryId(inventoryDetailsByProduct);
            inventoryHistory.setVendorId(vendor);
            inventoryHistory.setInventoryHistoryDate(new Date(System.currentTimeMillis()));
            final double quantity1 = jsonObject.getDouble("Quantity");
            inventoryHistory.setQuantity(BigDecimal.valueOf(quantity1));
            final Branch branches = branchService.getBranchById(branchId);
            inventoryHistory.setBranch(branches);
            final double taxDouble = jsonObject.getDouble("Tax");
            final double halfTax = taxDouble / 2;
            final BigDecimal tax = BigDecimal.valueOf(halfTax);
            inventoryHistory.setCgst(tax);
            inventoryHistory.setIgst(new BigDecimal(0));
            inventoryHistory.setSgst(tax);
            final double rate1 = jsonObject.getDouble("Rate");
            inventoryHistory.setBuyingPrice(BigDecimal.valueOf(rate1));
            final SimpleDateFormat df = new SimpleDateFormat("yy-MM-dd");
            if (!jsonObject.get("expiryDate").equals("")) {
              inventoryHistory.setExpiryDate(sdf.parse(jsonObject.getString("expiryDate")));
              inventoryHistory.setBatchNumber(df.format(inventoryHistory.getExpiryDate()));
            }
            inventoryHistory.setConsumedCount(new BigDecimal(0));
            inventoryHistory.setExpiryCount(new BigDecimal(0));
            inventoryHistory.setInvoiceBillId(invoiceBill);
            inventoryHistory.setOrganisation(employeeDetails.getOrganisation());
            inventoryHistoryService.save(inventoryHistory);

          } else {
            inventoryDetails = new InventoryDetail();
            inventoryDetails.setProduct(product);
            final double rate1 = jsonObject.getDouble("Rate");
            inventoryDetails.setBuyingPrice(product.getProductPrice());
            inventoryDetails.setSellingPrice(BigDecimal.valueOf(rate1));
            inventoryDetails.setQuantity(qty);
            inventoryDetails.setDate(new Date(System.currentTimeMillis()));
            final Branch branch = branchService.getBranchById(branchId);
            inventoryDetails.setBranchDetail(branch);
            inventoryDetails.setOrganisation(employeeDetails.getOrganisation());
            inventoryDetails.setVendor(vendor);
            inventoryDetails.setReorderQnty(20);
            inventoryDetailService.save(inventoryDetails);
            map.put("Message", "Products added successfully");

            inventoryDetail =
                inventoryDetailService.getInventoryDetailByProductNameAndBranchIdAndOrgId(
                    product.getProductName(), branchId, orgId);
            final InventoryReports inventoryReportDetail =
                inventoryReportsService.getInventoryByDateAndInventoryId(
                    new Date(), inventoryDetail.getInventoryId(), orgId, branchId);

            if (inventoryReportDetail != null) {
              final BigDecimal reportAvailableStockQuantity =
                  inventoryReportDetail.getStockQuantity();
              final BigDecimal presentStockQuantity = reportAvailableStockQuantity.add(qty);
              inventoryReportDetail.setStockQuantity(presentStockQuantity);
              inventoryReportDetail.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReportsService.update(inventoryReportDetail);
            } else {
              final InventoryReports inventoryReports = new InventoryReports();
              inventoryReports.setInventoryDetail(inventoryDetail);
              inventoryReports.setStockQuantity(qty);
              inventoryReports.setConsumedQuantity(new BigDecimal(0));
              inventoryReports.setCreatedDate(new Date());
              inventoryReports.setCreatedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setOrganisation(employeeDetails.getOrganisation());
              inventoryReports.setModifiedTime(new Date(System.currentTimeMillis()));
              inventoryReports.setBranch(branch);
              inventoryReportsService.save(inventoryReports);
            }
            inventoryHistory.setInventoryId(inventoryDetail);

            inventoryHistory.setVendorId(vendor);
            inventoryHistory.setInventoryHistoryDate(new Date(System.currentTimeMillis()));
            final double quantity1 = jsonObject.getDouble("Quantity");
            inventoryHistory.setQuantity(BigDecimal.valueOf(quantity1));
            final Branch branches = branchService.getBranchById(branchId);
            inventoryHistory.setBranch(branches);
            final double tax1 = jsonObject.getDouble("Tax");
            final BigDecimal tax = BigDecimal.valueOf(tax1 / 2);
            inventoryHistory.setCgst(tax);
            inventoryHistory.setIgst(new BigDecimal(0));
            inventoryHistory.setSgst(tax);
            final double rate2 = jsonObject.getDouble("Rate");
            inventoryHistory.setBuyingPrice(BigDecimal.valueOf(rate2));
            final SimpleDateFormat df = new SimpleDateFormat("yy-MM-dd");
            if (!jsonObject.get("expiryDate").equals("")) {
              inventoryHistory.setExpiryDate(sdf.parse(jsonObject.getString("expiryDate")));
              inventoryHistory.setBatchNumber(df.format(inventoryHistory.getExpiryDate()));
            }
            inventoryHistory.setConsumedCount(new BigDecimal(0));
            inventoryHistory.setExpiryCount(new BigDecimal(0));
            inventoryHistory.setInvoiceBillId(invoiceBill);
            inventoryHistory.setOrganisation(employeeDetails.getOrganisation());
            inventoryHistoryService.save(inventoryHistory);
          }
        }
      } catch (final Exception e) {
        LOGGER.error("getInventoryDetail" + e);
      }
    } else if (fromType.contentEquals("section")) {
      final long inventorySectionId = jo.getLong("fromSectionId");
      final long branchId = jo.getLong("fromId");
      InventorySection inventorySection = new InventorySection();
      List<InventorySectionDataHistory> list = null;
      list =
          inventorySectionDataHistoryService
              .getInventorySectionDataHistoryByOrgIdAndReferenceNumber(
                  orgId, jo.getString("invoiceNumber"));
      if (list.size() > 0) {
        map.put("Message", "Reference Number (invoiceNumber) already exists try another number");
        map.put("flag", "false");
        return map;
      } else {

      }
      try {

        inventorySection =
            this.inventorySectionService.getInventorySectionByOrgIdBranchIdAndId(
                orgId, branchId, inventorySectionId);
        if (inventorySection == null) {
          map.put("Message", "No inventorySections found for this orgId branchId and sectionId");
          map.put("flag", "false");
          return map;
        }
        for (int i = 0; i < ja.length(); ++i) {
          InventorySectionData inventorySectionData = new InventorySectionData();
          final InventorySectionDataHistory inventorySectionDataHistory =
              new InventorySectionDataHistory();
          final JSONObject jsonObject = ja.getJSONObject(i);
          final JSONObject selectedItemName = jsonObject.getJSONObject("selectedItemName");
          final Long productId = selectedItemName.getLong("id");

          inventorySectionData =
              this.inventorySectionDataService.getInventorySectionDataByproductIdAndOrgIdBranchId(
                  inventorySection.getSectionName(), productId, orgId, branchId);
          if (inventorySectionData != null) {
            inventorySectionData.setModifiedDate(new Date());
            final double quantity = jsonObject.getDouble("Quantity");
            final BigDecimal qty = BigDecimal.valueOf(quantity);
            final BigDecimal totalQuantity = inventorySectionData.getQuantity().add(qty);
            inventorySectionData.setQuantity(totalQuantity);
            this.inventorySectionDataService.update(inventorySectionData);
            map.put("Message", "Products updated successfully");
          } else {
            inventorySectionData = new InventorySectionData();
            inventorySectionData.setSectionName(inventorySection.getSectionName());
            final double quantity = jsonObject.getDouble("Quantity");
            inventorySectionData.setQuantity(BigDecimal.valueOf(quantity));
            inventorySectionData.setInventorySectionId(inventorySection);
            inventorySectionData.setProductId(productsService.getProductsById(productId));
            inventorySectionData.setCreatedDate(new Date());
            inventorySectionData.setModifiedDate(new Date());
            inventorySectionData.setBranch(branchService.getBranchById(jo.getLong("fromId")));
            inventorySectionData.setOrganisation(employeeDetails.getOrganisation());
            this.inventorySectionDataService.save(inventorySectionData);
            map.put("Message", "Products added successfully");
          }

          inventorySectionDataHistory.setSectionName(inventorySection.getSectionName());
          final double quantity = jsonObject.getDouble("Quantity");
          inventorySectionDataHistory.setQuantity(BigDecimal.valueOf(quantity));
          inventorySectionDataHistory.setInventorySectionId(inventorySection);
          inventorySectionDataHistory.setProductId(productsService.getProductsById(productId));
          inventorySectionDataHistory.setCreatedDate(new Date());
          inventorySectionDataHistory.setModifiedDate(new Date());
          inventorySectionDataHistory.setDate(new Date());
          inventorySectionDataHistory.setVendor(
              vendorService.getVendorDetailsById(jo.getLong("vendorId")));
          inventorySectionDataHistory.setReferenceNumber(jo.getString("invoiceNumber"));
          final double totalAmount = jo.getDouble("totalAmount");
          inventorySectionDataHistory.setAmount(BigDecimal.valueOf(totalAmount));
          final double taxbleAmount = jo.getDouble("taxbleAmount");
          inventorySectionDataHistory.setTaxableAmount(BigDecimal.valueOf(taxbleAmount));
          if (!jo.get("discount").equals("")) {
            final double discount = jo.getDouble("discount");
            inventorySectionDataHistory.setDiscountAmount(BigDecimal.valueOf(discount));
          }
          final double rate = jsonObject.getDouble("Rate");
          inventorySectionDataHistory.setBuyingPrice(BigDecimal.valueOf(rate));
          inventorySectionDataHistory.setBranch(branchService.getBranchById(jo.getLong("fromId")));
          inventorySectionDataHistory.setOrganisation(employeeDetails.getOrganisation());
          this.inventorySectionDataHistoryService.save(inventorySectionDataHistory);
        }
      } catch (final Exception em) {
        LOGGER.error("type Section" + em);
      }
    }
    return map;
  }

  @GetMapping(path = RouteConstants.INVENTORY_HISTORY_DETAILS)
  public final List<InventoryHistory> getInventoryHistoryDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final Branch branch = employeeDetails.getBranch();
    List<InventoryHistory> inventoryHistoryDetails = null;
    try {
      if (branch == null) {

        inventoryHistoryDetails =
            inventoryHistoryService.getAllInventoryHistorysByOrg(
                employeeDetails.getOrganisation().getId());
      } else {

        inventoryHistoryDetails =
            inventoryHistoryService.getAllInventoryHistorys(
                branch.getId(), employeeDetails.getOrganisation().getId());
      }
    } catch (final Exception e) {
      LOGGER.error("getInventoryHistoryDetail" + e);
    }
    return inventoryHistoryDetails;
  }

  @PostMapping(path = RouteConstants.INVENTORY_ID)
  public final Map<String, String> updateInventoryDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String inventoryDetail,
      @PathVariable("id") final Long id)
      throws JSONException {
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);

    final Branch branch = employeeDetails.getBranch();
    final long orgId = employeeDetails.getOrganisation().getId();

    final JSONObject inventory = new JSONObject(inventoryDetail);
    final String key = inventory.getString("key");
    final String value = inventory.getString("value");

    final Map<String, String> invUpdate = new HashMap<>();

    try {
      InventoryDetail inventoryDetails = null;
      if (branch == null) {
        inventoryDetails = inventoryDetailService.getInventoryDetailByIdOrgId(id, orgId);
      } else {
        final long branchId = branch.getId();
        inventoryDetails =
            inventoryDetailService.getInventoryDetailByIdOrgIdBranch(id, orgId, branchId);
      }

      if (inventoryDetails != null) {
        if (key.equals("Price")) {
          inventoryDetails.setSellingPrice(new BigDecimal(value));
          invUpdate.put("invUpdate", "InventoryDetails Updated For This Id  " + id);
        } else if (key.equals("quantity")) {
          inventoryDetails.setQuantity(new BigDecimal(value));
          invUpdate.put("invUpdate", "InventoryDetails Updated For This Id  " + id);

        } else if (key.equals("productName")) {
          final Product product =
              productsService.getProductsById(inventoryDetails.getProduct().getId());
          product.setProductName(value);
          productsService.update(product);
          inventoryDetails.setProduct(product);
          invUpdate.put("invUpdate", "InventoryDetails Updated For This Id  " + id);

        } else {

          invUpdate.put("invUpdate", "key Name doesnot matched");
        }
        this.inventoryDetailService.update(inventoryDetails);

      } else {

        invUpdate.put("invUpdate", "Id Did Not Matched For this Organisation and Branch");
      }
    } catch (final Exception e) {
      LOGGER.error("updateInventoryDetails" + e);
    }
    return invUpdate;
  }

  @SuppressWarnings("unused")
  @GetMapping(path = RouteConstants.NOTIFICATION_INVENTORY_DETAILS)
  public final List<Map<String, Object>> getInventoryNotifications(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final Branch branch = employeeDetails.getBranch();
    List<InventoryDetail> inventoryDetails = null;

    final List<Map<String, Object>> reorderInventoryDetails = new ArrayList<>();
    long productId = 0;
    List<ReOrder> reOrderDetailsByStatus = null;
    Product products = null;
    if (branch == null) {

      inventoryDetails = inventoryDetailService.getReOrderInventoryDetailsByOrg(orgId);
      for (final InventoryDetail inventoryDetailByOrg : inventoryDetails) {
        final Map<String, Object> orderDetails = new HashMap<>();
        productId = inventoryDetailByOrg.getProduct().getId();
        reOrderDetailsByStatus =
            inventoryDetailService.getReorderProductByStatusByOrgId(orgId, productId);

        if (reOrderDetailsByStatus.size() == 0) {
          products = productsService.getProductsByProductIdAndOrgId(productId, orgId);
        }
        final List<ProductVendor> productVendors =
            inventoryDetailService.getProductVendorsbyId(productId);

        orderDetails.put("ProductId", inventoryDetailByOrg.getProduct().getId());
        orderDetails.put("ProductName", inventoryDetailByOrg.getProduct().getProductName());
        orderDetails.put("AvailableOrderQuantity", inventoryDetailByOrg.getQuantity());
        orderDetails.put("ReOrderQuantity", inventoryDetailByOrg.getReorderQnty());
        if (productVendors.isEmpty()) {
          orderDetails.put("VendorName", inventoryDetailByOrg.getVendor().getVendorName());

        } else {
          orderDetails.put("VendorName", productVendors);
        }

        orderDetails.put("Quantity", 0);
        try {
          orderDetails.put("selectedVendor", productVendors.get(0));
        } catch (final Exception e) {
          final List<Object> VendorDetails = new ArrayList<Object>();
          VendorDetails.add(inventoryDetailByOrg.getVendor().getId());
          VendorDetails.add(inventoryDetailByOrg.getVendor().getVendorName());
          orderDetails.put("selectedVendor", VendorDetails);
          reorderInventoryDetails.add(orderDetails);
        }
      }

    } else {

      inventoryDetails =
          inventoryDetailService.getReOrderInventoryDetails(
              orgId, employeeDetails.getBranch().getId());
      for (final InventoryDetail inventoryDetail : inventoryDetails) {
        final Map<String, Object> orderDetails = new HashMap<>();
        productId = inventoryDetail.getProduct().getId();
        reOrderDetailsByStatus =
            inventoryDetailService.getReorderProductByStatus(
                orgId, employeeDetails.getBranch().getId(), productId);

        if (reOrderDetailsByStatus.size() == 0) {
          products =
              productsService.getProductsByProductIdAndOrgIdAndBranchId(
                  productId, orgId, branch.getId());
        }

        final List<ProductVendor> productVendors =
            inventoryDetailService.getProductVendorsbyId(productId);

        orderDetails.put("ProductId", inventoryDetail.getProduct().getId());
        orderDetails.put("ProductName", inventoryDetail.getProduct().getProductName());
        orderDetails.put("AvailableOrderQuantity", inventoryDetail.getQuantity());
        orderDetails.put("ReOrderQuantity", inventoryDetail.getReorderQnty());
        if (productVendors.isEmpty()) {
          orderDetails.put("VendorName", inventoryDetail.getVendor().getVendorName());

        } else {
          orderDetails.put("VendorName", productVendors);
        }

        orderDetails.put("Quantity", 0);
        try {
          orderDetails.put("selectedVendor", productVendors.get(0));
        } catch (final Exception e) {
          final List<Object> VendorDetails = new ArrayList<Object>();
          VendorDetails.add(inventoryDetail.getVendor().getId());
          VendorDetails.add(inventoryDetail.getVendor().getVendorName());
          orderDetails.put("selectedVendor", VendorDetails);
        }
        reorderInventoryDetails.add(orderDetails);
      }
    }
    return reorderInventoryDetails;
  }

  @GetMapping(path = RouteConstants.INVENTORY_DETAILS)
  public final ResponseEntity<Object> getInventoryDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<InventoryDetail> inventoryDetail = new ArrayList<InventoryDetail>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();

        if (branchId == 0) {
          inventoryDetail = inventoryDetailService.getAllInventoryDetailsByOrgId(orgId);

        } else {
          inventoryDetail = inventoryDetailService.getAllInventoryDetails(branchId, orgId);
        }
      } else {
        return new ResponseEntity<>(inventoryDetail, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error(Constants.MARKER, "getInventoryDetails " + e);
    }
    return new ResponseEntity<>(inventoryDetail, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.REORDER_INVENTORY_DETAILS)
  public final List<Map<String, Object>> getInventoryDetail(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = Long.parseLong(request.getParameter("branchId"));

    List<InventoryDetail> inventoryDetails = null;

    final List<Map<String, Object>> reorderInventoryDetails = new ArrayList<>();
    long productId = 0;
    List<ReOrder> reOrderDetailsByStatus = null;
    Product products = null;
    inventoryDetails = inventoryDetailService.getAllInventoryDetails(branchId, orgId);

    for (final InventoryDetail inventoryDetail : inventoryDetails) {

      final Map<String, Object> orderDetails = new HashMap<>();
      productId = inventoryDetail.getProduct().getId();
      reOrderDetailsByStatus =
          inventoryDetailService.getReorderProductByStatus(orgId, branchId, productId);

      if (reOrderDetailsByStatus.size() == 0) {
        products =
            productsService.getProductsByProductIdAndOrgIdAndBranchId(productId, orgId, branchId);

        orderDetails.put("ProductId", products.getId());
        orderDetails.put("ProductName", products.getProductName());
      } else {
        orderDetails.put("ProductId", productId);
        orderDetails.put("ProductName", inventoryDetail.getProduct().getProductName());
      }
      final List<ProductVendor> productVendors =
          inventoryDetailService.getProductVendorsbyId(productId);

      orderDetails.put("AvailableOrderQuantity", inventoryDetail.getQuantity());
      orderDetails.put("ReOrderQuantity", inventoryDetail.getReorderQnty());

      if (productVendors.isEmpty())
        orderDetails.put("VendorName", inventoryDetail.getVendor().getVendorName());
      else orderDetails.put("VendorName", productVendors);

      orderDetails.put("Quantity", 0);
      try {
        orderDetails.put("selectedVendor", productVendors.get(0));
      } catch (final Exception e) {
        final List<Object> VendorDetails = new ArrayList<Object>();
        VendorDetails.add(inventoryDetail.getVendor().getId());
        VendorDetails.add(inventoryDetail.getVendor().getVendorName());
        orderDetails.put("selectedVendor", VendorDetails);
      }
      reorderInventoryDetails.add(orderDetails);
    }

    return reorderInventoryDetails;
  }

  @GetMapping(path = RouteConstants.INVENTORY_NAMES)
  public final List<InventoryDetail> getInventoryDetailsNames(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final String ProductName = request.getParameter("productName");

    final List<InventoryDetail> inventoryDetails = new ArrayList<InventoryDetail>();

    try {

      final List<Product> products = productsService.getAllProductsByName(ProductName);

      for (final Product product : products) {

        final InventoryDetail inventoryDetail =
            inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
                product.getId(), employeeDetails.getBranch().getId());
        if (inventoryDetail != null) {
          inventoryDetails.add(inventoryDetail);
        }
      }

    } catch (final Exception e) {
      LOGGER.error("InventoryDetails" + e);
    }
    return inventoryDetails;
  }

  @GetMapping(path = RouteConstants.PRODUCTCODE_DETAILS)
  public final Map<String, Object> getProductCountDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final Map<String, Object> map = new HashMap<>();
    Product productDetail = null;
    try {

      final String code = request.getParameter("productCode");
      productDetail = productsService.getProductsByProductCode(code);
      map.put("productName", productDetail.getProductName());
      map.put("productCount", productDetail.getProductCount());

    } catch (final Exception e) {
      LOGGER.error("getAddInventoryDetails" + e);
    }
    return map;
  }

  @PostMapping(path = RouteConstants.ADD_INVENTORY_DETAILS)
  public final void getAddInventoryDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final String productCode = request.getParameter("productCode");
    final String addProductCode = request.getParameter("addProductCode");
    final String productQuantityStr = request.getParameter("productQuantity");
    final BigDecimal productQuantity = new BigDecimal(productQuantityStr);
    Product productDetail = null;
    InventoryDetail inventoryDetail = null;
    try {
      productDetail = productsService.getProductsByProductCode(addProductCode);

      inventoryDetail =
          inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
              productDetail.getId(), employeeDetails.getBranch().getId());
      if (inventoryDetail != null) {
        final BigDecimal addInventoryQuantity = inventoryDetail.getQuantity().add(productQuantity);
        inventoryDetail.setQuantity(addInventoryQuantity);
        inventoryDetailService.update(inventoryDetail);
      }
      productDetail = productsService.getProductsByProductCode(productCode);
      inventoryDetail =
          inventoryDetailService.getInventoryDetailsByProductIdAndBranchId(
              productDetail.getId(), employeeDetails.getBranch().getId());
      if (inventoryDetail != null) {
        final BigDecimal subInventoryQuantity =
            inventoryDetail.getQuantity().subtract(productQuantity);
        inventoryDetail.setQuantity(subInventoryQuantity);
        inventoryDetailService.update(inventoryDetail);
      }
    } catch (final Exception e) {
      LOGGER.error("getAddInventoryDetails" + e);
    }
  }

  @SuppressWarnings("unused")
  @GetMapping(path = RouteConstants.ADMIN_INVENTORY_BRANCHES)
  public final List<Map<String, Object>> getProductVendorBranchesByOrg(
      final HttpServletRequest request, final HttpServletResponse response) {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final long orgId = employeeDetails.getOrganisation().getId();
    final List<Object> branches = new ArrayList<Object>();
    List<InventoryDetail> inventoryDetails = null;
    final String branch = request.getParameter("branchId");
    final List<Branch> listOfbranches = branchService.getBranchesByOrganisationId(orgId);
    final List<Map<String, Object>> reorderInventoryDetails = new ArrayList<>();
    final long branchId = Long.parseLong(branch);
    for (final Branch listOfBranch : listOfbranches) {
      if (branchId != listOfBranch.getId()) {
        final Map<String, Object> branchMap = new HashMap<>();
        branchMap.put("branchId", listOfBranch.getId());
        branchMap.put("branchName", listOfBranch.getBranchName());
        branches.add(branchMap);
      }
    }
    List<ReOrder> reOrderDetailsByStatus = null;
    Product products = null;
    inventoryDetails = inventoryDetailService.getReOrderInventoryDetails(orgId, branchId);
    for (final InventoryDetail inventoryDetail : inventoryDetails) {
      final Map<String, Object> orderDetails = new HashMap<>();
      final long productId = inventoryDetail.getProduct().getId();
      reOrderDetailsByStatus =
          inventoryDetailService.getReorderProductByStatus(orgId, branchId, productId);

      if (reOrderDetailsByStatus.size() == 0) {
        products =
            productsService.getProductsByProductIdAndOrgIdAndBranchId(productId, orgId, branchId);
      }
      orderDetails.put("ProductId", inventoryDetail.getProduct().getId());
      orderDetails.put("ProductName", inventoryDetail.getProduct().getProductName());
      orderDetails.put("AvailableOrderQuantity", inventoryDetail.getQuantity());
      orderDetails.put("ReOrderQuantity", inventoryDetail.getReorderQnty());
      orderDetails.put("Quantity", 0);
      orderDetails.put("Branches", branches);
      orderDetails.put("orgId", orgId);
      try {
        orderDetails.put("selectedBranch", branches.get(0));
      } catch (final Exception e) {
        orderDetails.put("selectedBranch", branchId);
      }
      reorderInventoryDetails.add(orderDetails);
    }

    return reorderInventoryDetails;
  }

  @PostMapping(path = RouteConstants.INVENTORY_HISTORY_DATES_VENDOR)
  public final List<Map<String, Object>> getAllInventoryHistoryDetailsWithDatesAndVendor(
      @RequestBody final String payload,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws JSONException, ParseException {

    final JSONObject jsonObject = new JSONObject(payload);
    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    List<InventoryHistory> list = new ArrayList<InventoryHistory>();
    final List<Map<String, Object>> invList = new ArrayList<Map<String, Object>>();

    final long orgId = employeeDetails.getOrganisation().getId();
    final Branch branch = employeeDetails.getBranch();
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date fromDate = sdf.parse(jsonObject.getString("fromDate"));
    final Date toDate = sdf.parse(jsonObject.getString("toDate"));
    final String vendorId = jsonObject.getString("vendorId");
    try {
      if (branch == null) {
        if (vendorId.equals("")) {
          list =
              inventoryHistoryService.getAllInventoryHistoryDetailsByOrgAndBetweenTwoDates(
                  fromDate, toDate, orgId);
        } else {
          list =
              inventoryHistoryService.getAllInventoryHistoryDetailsByOrgAndVendorBetweenTwoDates(
                  fromDate, toDate, orgId, Long.parseLong(vendorId));
        }
      } else {
        if (vendorId.equals("")) {
          list =
              inventoryHistoryService.getAllInventoryHistoryDetailsByOrgAndBranchAndBetweenTwoDates(
                  fromDate, toDate, orgId, branch.getId());
        } else {
          list =
              inventoryHistoryService
                  .getAllInventoryHistoryDetailsByOrgAndBranchAndVendorBetweenTwoDates(
                      fromDate, toDate, orgId, branch.getId(), Long.parseLong(vendorId));
        }
      }

      for (InventoryHistory inv : list) {
        final Map<String, Object> invDetails = new HashMap<>();

        Vendor vendor = vendorService.getVendorDetailsById(inv.getVendorId().getId());
        if (vendor != null) {
          invDetails.put("date", inv.getInventoryHistoryDate());
          invDetails.put("item", inv.getInventoryId().getProduct().getProductName());

          invDetails.put("rate", inv.getBuyingPrice());

          invDetails.put("quantity", inv.getQuantity());

          invDetails.put("amount", inv.getBuyingPrice().multiply(inv.getQuantity()));

          invDetails.put("vendorName", inv.getVendorId().getVendorName());

        } else {
          invDetails.put("date", "");
          invDetails.put("item", "");

          invDetails.put("rate", "");

          invDetails.put("quantity", "");

          invDetails.put("amount", "");

          invDetails.put("vendorName", "");
        }

        invList.add(invDetails);
      }

    } catch (final Exception ex) {
      LOGGER.error("getAllInventoryHistoryDetailsWithDatesAndVendor" + ex);
    }
    return invList;
  }

  @GetMapping(path = RouteConstants.INVENTORY_DETAILS_ALL)
  public final ResponseEntity<Object> getInventoryDetailsByproduct(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<InventoryDetail> inventoryDetail = new ArrayList<InventoryDetail>();
    try {
      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = Long.parseLong(request.getParameter("branchId"));

        if (branchId == 0) {
          inventoryDetail = inventoryDetailService.getAllInventoryDetailsByOrgId(orgId);

        } else {
          inventoryDetail = inventoryDetailService.getAllInventoryDetails(branchId, orgId);
        }
      } else {
        return new ResponseEntity<>(inventoryDetail, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException e) {
      LOGGER.info("No Records Found");
    } catch (final Exception e) {
      LOGGER.error("getInventoryDetail " + e);
    }
    return new ResponseEntity<>(inventoryDetail, HttpStatus.OK);
  }

  @GetMapping(path = RouteConstants.INVENTORYHISTORY_PRODUCTID)
  public final List<InventoryHistory> getAllInventoryHistoryDetailsByProductId(
      final HttpServletRequest request, final HttpServletResponse responce) throws ParseException {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    final Date fromDate = sdf.parse(request.getParameter("fromDate"));
    final Date toDate = sdf.parse(request.getParameter("toDate"));
    final Long branchId = Long.parseLong(request.getParameter("branchId"));
    final long orgId = employeeDetails.getOrganisation().getId();
    final long productId = Long.parseLong(request.getParameter("productId"));
    List<InventoryHistory> inventoryHistory = null;
    try {
      final InventoryDetail inventoryDetails =
          inventoryDetailService.getInventoryDetailsByProductIdAndBranchIdAndOrgId(
              productId, branchId, orgId);
      if (inventoryDetails != null) {
        inventoryHistory =
            inventoryHistoryService
                .getAllInventoryHistoryDetailsByInventoryIdAndOrgAndBranchAndBetweenTwoDates(
                    inventoryDetails.getInventoryId(), orgId, branchId, fromDate, toDate);
      }
    } catch (final Exception e) {
      LOGGER.error("getInventoryDetail" + e);
    }
    return inventoryHistory;
  }

  @GetMapping(path = RouteConstants.INVENTORYHISTORY_INVOICEID)
  public final Map<Object, Object> getAllInventoryHistoryDetailsByInvoiceBillId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    final Map<Object, Object> map = new HashMap<Object, Object>();
    try {
      final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final long branchId = employeeDetails.getBranch().getId();
      final long orgId = employeeDetails.getOrganisation().getId();
      final String invoiceNumber = request.getParameter("invoiceNumber");
      List<InventoryHistory> inventoryHistory = null;
      final Branch branch = branchService.getBranchById(branchId);
      final InvoiceBill invoice = invoiceBillService.getInvoiceBillByInvoiceNumber(invoiceNumber);
      if (invoice != null) {
        if (branch == null) {
          inventoryHistory =
              inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                  orgId, invoice.getId());
        } else {
          inventoryHistory =
              inventoryHistoryService.getAllInventoryHistoryDetailsByInvoiceBillId(
                  branch.getId(), orgId, invoice.getId());
        }
        if (inventoryHistory != null && inventoryHistory.size() == 0) {
          map.put("message", "Couldn't find inventoryHistory Details for this invoiceNumber!");
          map.put("inventoryHistory", inventoryHistory);
        } else {
          map.put("message", " inventoryHistory Details for this invoiceNumber");
          map.put("inventoryHistory", inventoryHistory);
        }
      } else {
        map.put("message", "Invoice Number doesn't exist!");
        map.put("inventoryHistory", inventoryHistory);
      }
    } catch (final Exception e) {
      LOGGER.error(e.toString(), e);
    }
    return map;
  }

  @GetMapping(path = RouteConstants.INVENTORY_HISTORY_AVAILQUANTITY)
  public final List<Map<String, Object>> getAvailableQuantityAndExpiryDateByProductId(
      final HttpServletRequest request, final HttpServletResponse response) {

    final long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
    final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
    final List<Map<String, Object>> list = new ArrayList<>();
    final long orgId = employeeDetails.getOrganisation().getId();
    final long branchId = employeeDetails.getBranch().getId();
    final String givenProductId = request.getParameter("productId");
    final Map<String, Object> errorMap = new HashMap<String, Object>();
    long productId = 0;
    if (givenProductId == null || givenProductId.equals("")) {
      errorMap.put("Message", "Product must select");
      list.add(errorMap);
    } else {
      productId = Long.parseLong(givenProductId);
    }
    List<Object[]> details = null;
    try {
      details =
          inventoryHistoryService.getAvailableQuantityWithExpiryDateByProductIdAndOrgIdAndBrnachId(
              productId, orgId, branchId);

      for (final Object[] detail : details) {
        final Map<String, Object> map = new HashMap<>();
        map.put("expiryDate", detail[0]);
        map.put("availableQuantity", detail[1]);
        list.add(map);
      }

    } catch (final Exception ex) {
      LOGGER.error("getAvailableQuantityAndExpiryDateByProductId", ex);
    }
    return list;
  }

  @GetMapping(path = RouteConstants.INVENTORY_REPORT_DETAILS)
  public final ResponseEntity<Map<String, Object>> getInventoryReports(
      final HttpServletRequest request, final HttpServletResponse response) throws ParseException {

    final Map<String, Object> inventoryReportsMap = new HashMap<>();

    try {
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      final String givenDate = request.getParameter("toDate");
      final String branch = request.getParameter("BranchName");
      long branchId = 0;
      Date date = null;
      if (branch.equals("")) {
        inventoryReportsMap.put("errorMessage", "Branch Name must select");

      } else {
        branchId = Long.parseLong(branch);
      }
      if (givenDate.equals("")) {
        date = new Date();
      } else {
        date = sdf.parse(givenDate);
      }
      final List<InventoryReports> inventoryReports =
          inventoryReportsService.getInventoryReportsByDate(date, branchId);
      if (inventoryReports != null) {
        System.out.println(inventoryReports.isEmpty());
        System.out.println(inventoryReports.size());
        if (inventoryReports.size() > 0) {
          inventoryReportsMap.put("successMessage", inventoryReports);
        } else {
          inventoryReportsMap.put(
              "errorMessage", "Inventory Reports not available for selected date and branch");
        }
      } else {
        inventoryReportsMap.put(
            "errorMessage", "Inventory Reports not available for selected date and branch");
      }
    } catch (Exception e) {
      LOGGER.error("getstock day wise Report  " + "select required branch");
      return new ResponseEntity<>(inventoryReportsMap, HttpStatus.OK);
    }
    return new ResponseEntity<>(inventoryReportsMap, HttpStatus.OK);
  }

  @PostMapping(path = RouteConstants.ADIMN_DATES_VENDOR_PRODUCT_REPORT)
  public final List<InventoryHistory> getInventoryHistoryDetailsByDatesAndVendorAndProduct(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload) {
    List<InventoryHistory> list = new ArrayList<InventoryHistory>();
    try {
      final Long employeeNumber = jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails = employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final JSONObject jsonObject = new JSONObject(payload);
      final long branchId = jsonObject.getLong("branchId");
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      final Date fromDate = sdf.parse(jsonObject.getString("fromDate"));
      final Date toDate = sdf.parse(jsonObject.getString("toDate"));
      final String vendorId = jsonObject.getString("vendorId");
      final String productId = jsonObject.getString("productId");
      final long orgId = employeeDetails.getOrganisation().getId();
      if (vendorId.isEmpty() && productId.isEmpty()) {
        list =
            inventoryHistoryService.getAllInventoryHistoryDetailsByOrgAndBranchAndBetweenTwoDates(
                fromDate, toDate, orgId, branchId);
      } else if (productId.isEmpty()) {
        list =
            inventoryHistoryService
                .getAllInventoryHistoryDetailsByOrgAndBranchAndVendorBetweenTwoDates(
                    fromDate, toDate, orgId, branchId, Long.parseLong(vendorId));
      } else if (vendorId.isEmpty()) {
        list =
            inventoryHistoryService
                .getInventoryHisotryDetailsByDatesAndProductIdAndOrgIdAndBranchId(
                    fromDate, toDate, Long.parseLong(productId), orgId, branchId);
      } else {
        list =
            inventoryHistoryService
                .getInventoryHistoryDetailsByDateAndVendorIdAndProductIdAndOrgIdAndBranchId(
                    fromDate,
                    toDate,
                    Long.parseLong(vendorId),
                    Long.parseLong(productId),
                    orgId,
                    branchId);
      }
    } catch (final Exception e) {
      LOGGER.error("Error at getInventoryHistoryDetailsByDatesAndVendorAndProduct" + e);
    }
    return list;
  }
}
