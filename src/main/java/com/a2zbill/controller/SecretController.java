package com.a2zbill.controller;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.controller.constants.RouteConstants;
import com.a2zbill.domain.SecretKey;
import com.a2zbill.services.SecretKeyService;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.security.jwt.Identifier;
import com.tsss.basic.security.jwt.JwtTokenUtil;
import com.tsss.basic.service.BranchService;
import com.tsss.basic.service.EmployeeService;
import com.tsss.basic.service.EncryptDecryptService;
import com.tsss.basic.service.OrganisationService;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecretController {

  private static final Logger LOGGER = LoggerFactory.getLogger(SecretController.class);

  @Autowired private SecretKeyService secretService;

  @Autowired private JwtTokenUtil jwtTokenUtil;

  @Autowired private EmployeeService employeeService;

  @Autowired private BranchService branchService;

  @Autowired private OrganisationService organisationService;

  @Autowired private EncryptDecryptService encryptDecryptService;

  @PostMapping(path = RouteConstants.SECRET_SAVE)
  public final SecretKey addSecretDetails(
      final HttpServletRequest request,
      final HttpServletResponse response,
      @RequestBody final String payload)
      throws JSONException {

    final SecretKey secret = new SecretKey();
    try {
      final long employeeNumber = this.jwtTokenUtil.getEmployeeNumberFromToken(request);
      final Employee employeeDetails =
          this.employeeService.getEmployeeByEmployeeNumber(employeeNumber);
      final JSONObject secretDetails = new JSONObject(payload);

      secret.setCommandName(secretDetails.getString("commandName"));
      secret.setCreatedDate(new Date(System.currentTimeMillis()));
      secret.setModifiedDate(new Date(System.currentTimeMillis()));
      secret.setSecretKey(
          this.encryptDecryptService.encrypts(secretDetails.getString("secretKey")));
      final Branch branch = this.branchService.getBranchById(employeeDetails.getBranch().getId());
      secret.setBranch(branch);
      final Organisation organisation =
          this.organisationService.getOrganisationById(employeeDetails.getOrganisation().getId());
      secret.setOrganisation(organisation);
      this.secretService.save(secret);
    } catch (final Exception ex) {
      LOGGER.error(Constants.MARKER, "SecretController in the addSecretDetails() of" + ex);
    }
    return secret;
  }

  @PostMapping(path = RouteConstants.SECRET_DETAILS_UPDATE)
  public final SecretKey updateSecretDetails(
      final HttpServletRequest request,
      final HttpServletResponse responce,
      @RequestBody final String secretDetails)
      throws JSONException, IOException, IllegalBlockSizeException, BadPaddingException {

    final JSONObject secret = new JSONObject(secretDetails);
    SecretKey secretKey = null;
    final long secretId = Long.parseLong(request.getParameter("secretId"));
    try {
      secretKey = this.secretService.getSecretKeyById(secretId);
      secretKey.setSecretKey(this.encryptDecryptService.encrypt(secret.getString("secretKey")));
      this.secretService.update(secretKey);
    } catch (RuntimeException ex) {
      LOGGER.error("updateSecretDetails" + ex, ex);
    }
    return secretKey;
  }

  @GetMapping(path = RouteConstants.GET_ALL_SECRET_DETAILS)
  public final List<SecretKey> getSecretDetails(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<SecretKey> secretKey = null;
    try {

      secretKey = this.secretService.getAllSecret();
      for (SecretKey secret : secretKey) {
        secret.setSecretKey(this.encryptDecryptService.decrypt(secret.getSecretKey()));
        secretKey.add(secret);
      }
    } catch (Exception ex) {
      LOGGER.error(Constants.MARKER, "getSecretDetails" + ex);
    }
    return secretKey;
  }

  @GetMapping(path = RouteConstants.GET_SECRET_DETAILS)
  public final ResponseEntity<Object> getSecretDetailsByOrgIdBranchId(
      final HttpServletRequest request, final HttpServletResponse responce) {

    List<SecretKey> secretKey = null;
    try {

      final Identifier identifier = (Identifier) request.getAttribute("identifier");
      if (identifier != null) {
        final long orgId = identifier.getOrgId();
        final long branchId = identifier.getBranchId();
        secretKey = this.secretService.getAllSecretByOrgIdBranchId(orgId, branchId);
        for (SecretKey secret : secretKey) {
          secret.setSecretKey(this.encryptDecryptService.decrypt(secret.getSecretKey()));
          secretKey.add(secret);
        }
        secretKey = this.secretService.getAllSecretByOrgId(orgId);
        for (SecretKey secret : secretKey) {
          secret.setSecretKey(this.encryptDecryptService.decrypt(secret.getSecretKey()));
          secretKey.add(secret);
        }
      } else {
        return new ResponseEntity<>(secretKey, HttpStatus.UNAUTHORIZED);
      }
    } catch (final NullPointerException ne) {
      LOGGER.error("not found" + ne, ne);
    } catch (Exception ex) {
      LOGGER.error(Constants.MARKER, "getSecretDetails" + ex);
    }
    return new ResponseEntity<>(secretKey, HttpStatus.OK);
  }
}
