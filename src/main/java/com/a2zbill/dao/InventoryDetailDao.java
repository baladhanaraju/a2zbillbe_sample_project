package com.a2zbill.dao;

import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import java.math.BigDecimal;
import java.util.List;

public interface InventoryDetailDao {

  void save(InventoryDetail inventoryDetail);

  boolean delete(long id);

  boolean update(InventoryDetail inventoryDetail);

  List<InventoryDetail> getAllInventoryDetails();

  List<InventoryDetail> getAllInventoryDetails(long branchId, long orgId);

  List<InventoryDetail> getAllInventoryDetailsByOrgId(long orgId);

  InventoryDetail getInventoryDetailsByProductId(long productId);

  InventoryDetail getInventoryDetailsByProductIdAndBranchId(long productId, long branchId);

  InventoryDetail getInventoryDetailsByProductIdAndBranchIdAndOrgId(
      final long productId, final long branchId, final long orgId);

  List<InventoryDetail> getInventoryDetailsByProductIds(long productId);

  InventoryDetail getInventoryDetailById(long inventoryId);

  InventoryDetail getInventoryDetailByIdOrgId(long inventoryId, long orgId);

  InventoryDetail getInventoryDetailByIdOrgIdBranch(long inventoryId, long orgId, long branchId);

  public BigDecimal getInventoryCurrentStockQTYByorgBranch(long orgId, long branchId);

  Long getInventoryCurrentStockQTY();

  BigDecimal getInventoryDetailStockAmount();

  BigDecimal getInventoryDetailStockAmountByorg(long orgId);

  public BigDecimal getInventoryDetailStockAmountByorgBranch(long orgId, long branchId);

  List<ReOrder> getReOrderDetailsById(long productId);

  List<ProductVendor> getProductVendorsbyId(long productId);

  List<InventoryDetail> getReOrderInventoryDetails(long orgId, long branchId);

  List<InventoryDetail> getReOrderInventoryDetailsByOrg(long orgId);

  BigDecimal getInventoryCurrentStockQTYByorg(long orgId);

  List<ReOrder> getReorderProductByStatus(long orgId, long branchId, long productId);

  InventoryDetail getInventoryDetailsByProductIdAndOrgId(long productId, long orgId);

  List<ReOrder> getReorderProductByStatusByOrgId(long orgId, long productId);

  void updateInventoryQuantity(long productId, BigDecimal quantity);

  List<InventoryDetail> getInventoryDetailByBranch(long branchId);

  InventoryDetail getInventoryDetailByProductNameAndBranchIdAndOrgId(
      String productName, long branchId, long orgId);
}
