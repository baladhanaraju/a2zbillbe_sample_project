package com.a2zbill.dao;

import com.a2zbill.domain.Reservation;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public interface ReservationDao {
  void save(Reservation reservation);

  boolean delete(Reservation reservation);

  void update(Reservation reservation);

  List<Reservation> getAllReservationDetails();

  Reservation getReservationDetailsByid(long id);

  List<Reservation> getReservationDetailsByStatus(String status);

  List<Reservation> getReservationDetailsByStarttimeOrgAndBranch(
      Date currentdate, long reservetable, String time, long orgId, long branchId);

  List<Reservation> getReservationDetailsByDateAndTime(
      Date date, long reservedtable, String startTime, long orgId, long branchId);

  List<Reservation> getReservationDetailsByDateAndTimeAndOrg(
      Date date, long reservedtable, String startTime, long orgId);

  List<Reservation> getReservationDetailsByOrgAndBranch(long orgId, long branchId);

  List<Reservation> getReservationDetailsByOrg(long orgId);

  List<Reservation> getReservationDetailsByStarttimeOrg(
      final Date currentdate, long reservetable, String time, long orgId);

  List<Reservation> getAllReservationsByDate(Date date, long orgId, long branchId);

  List<Reservation> getReservationDeatilsByTable(
      Date date, long reservetable, long orgId, long branchId);

  List<Reservation> getReservationDeatilsByTableAndOrg(Date date, long reservetable, long orgId);

  Reservation getReservationByTable(Date date, long reservetable, long orgId, long branchId);

  Reservation getReservationByTableAndOrg(Date date, long reservetable, long orgId);

  Reservation getReservationdeatilsByTableAndcustomerId(
      long customer, Date date, long reservetable, long orgId, long branchId);

  Reservation getReservationdeatilsByTableAndOrgAndcustomerId(
      long customer, Date date, long reservetable, long orgId);

  List<Reservation> getReservationDetailsByStarttimeOrgAndBranch(
      String startTime, long orgId, long branchId);

  List<Reservation> getReservationDetailsByStarttimeOrg(String startTime, long orgId);

  Reservation getReservationDetailsByreserveTableOrgAndBranch(
      Date date, long reservetable, String startTime, long orgId, long branchId);

  Reservation getReservationDetailsByreserveTableOrg(
      long reservetable, String startTime, long orgId);

  List<Reservation> getReservationDetailsByreserveTableAndOrgBranch(
      Date date, long reservetable, String startTime, long orgId, long branchId);

  List<Reservation> getReservationDetailsByReservedTableAndOrgAndBranchAndBetweenTwoTimesAndDate(
      long reservedTable,
      Date reserveDate,
      long orgId,
      long branchId,
      String lowestTime,
      String highestTime);

  BigInteger getCountOfReservationByOrganisationIdAndCurrentMonth(long orgId);

  BigInteger getCountOfReservationByOrganisationIdAndCurrentMonthBranchId(
      long orgId, long branchId);

  List<Reservation> getAllReservationsFromCurrentDate(
      long orgId, long branchId, Date date, String startTime);

  Reservation getReservationDetailsBySlotTimeBranch(
      Date date, String startTime, long reservetable, long orgId, long branchId);

  List<Reservation> getReservationDetailsBySlotTimeorg(
      String startTime, long reservetable, long orgId);
}
