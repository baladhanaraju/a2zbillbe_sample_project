package com.a2zbill.dao;

import com.a2zbill.domain.InvoiceDetails;
import java.util.List;

public interface InvoiceDetailsDao {

  void save(final InvoiceDetails invoiceDetails);

  boolean delete(final long id);

  boolean update(final InvoiceDetails invoiceDetails);

  List<InvoiceDetails> getAllInvoiceDetails();

  InvoiceDetails getInvoiceDetailsByVendorId(final long vendorId);

  InvoiceDetails getInvoiceDetailsByInvoiceId(final long invoiceId);

  List<InvoiceDetails> getAllInvoiceDetailsByOrgAndBranch(final long orgId, final long branchId);

  List<InvoiceDetails> getAllInvoiceDetailsByOrg(final long orgId);

  List<Object[]> getInvoiceDetailsByVendor(final long vendorId, final long orgId);
}
