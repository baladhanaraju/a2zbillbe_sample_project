package com.a2zbill.dao;

import com.a2zbill.domain.CustomerHistory;

public interface CustomerHistoryDao {
  void save(CustomerHistory customerHistory);

  boolean delete(long id);

  boolean update(CustomerHistory customerHistory);
}
