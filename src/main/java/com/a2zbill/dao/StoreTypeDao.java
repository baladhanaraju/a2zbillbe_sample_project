package com.a2zbill.dao;

import com.a2zbill.domain.StoreType;
import java.util.List;

public interface StoreTypeDao {

  public void save(StoreType storeType);

  public boolean delete(long id);

  public boolean update(StoreType storeType);

  List<StoreType> getAllStoreTypeDetails();

  StoreType storeTypegetDetalisById(final long id);
}
