package com.a2zbill.dao;

import com.a2zbill.domain.AlteredBillings;
import java.util.List;

public interface AlteredBillingsDao {

  public void save(final AlteredBillings alteredbillings);

  public Boolean update(final AlteredBillings alteredbillings);

  public Boolean delete(final AlteredBillings alteredbillings);

  List<AlteredBillings> getAllAlteredBillings();

  AlteredBillings getAlteredBillingsbyid(final long id);
}
