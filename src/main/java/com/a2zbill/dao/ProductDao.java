package com.a2zbill.dao;

import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductVariation;
import java.util.List;
import java.util.Map;

public interface ProductDao {

  void save(Product products);

  boolean delete(Long id);

  boolean update(Product products);

  Product getProductsById(Long id);

  List<Product> getAllProducts();

  List<Product> getAllProducts(long orgId, long branchId);

  List<Product> getProductsBySizeVariationIdAndProductName(
      long orgId, long sizeId, String productName);

  List<Product> getAllProductsByOrgId(long orgId);

  Product getProductsByProductCode(String productCode);

  Product getProductsByOrgId(Long orgId);

  Product getProductsByProductCodeAndOrgId(final String productCode, final Long orgId);

  Product getProductsByProductCodeAndOrgIdAndBranchId(
      final String productCode, final Long orgId, final Long branchId);

  Product getProductsByProductName(String productName);

  Product getProductsByProductNameAndOrgAndBranch(String productName, long orgId, long branchId);

  Product getProductsByProductNameAndOrgIdAndBranchId(
      String productName, long orgId, long branchId);

  Product getProductsByProductNameAndOrgId(String productName, long orgId);

  Product getProductFeaturesById(Long id);

  List<Product> getAllProductsName();

  long getProductsCount();

  long getProductsCount(Long orgId);

  long getProductsCounByOrgIdAndBranchId(Long brnachId, Long orgId);

  Product getProductsByHsnCodeAndProductCodeAndProductName(
      String hsnCode, String productCode, String productName);

  Product getProductsByProductCodeAndProductNameAndOrgAndBranch(
      String productCode, String productName, long orgId, long branchId);

  Product getProductsByHsnCodeAndProductName(String hsnCode, String productName);

  List<Product> getAllProductsByName(String productName);

  List<Product> getProductsByProductCategoryId(long productCategoryId);

  Product getProductsByHsnCodeAndProductCode(String hsnCode, String productCode);

  Product getProductsByProductIdAndOrgIdAndBranchId(long productId, long orgId, long branchId);

  Product getProductsByProductIdAndOrgId(long productId, long orgId);

  Product getProductsByHsnCodeAndProductNameByBranchIdAndOrgId(
      String hsnCode, String productName, long branchId, long orgId);

  List<Product> getAllProductsByHsnCode();

  List<Product> getProductsByProductCategoryName(String productCategoryName);

  List<Product> getProductsByProductCategoryNameIfRedeemFlagIsFalse(String productCategoryName);

  Product getProductsByProductNameAndOrgIdAndBranchIdAndSizeId(
      String producName, long orgId, long branchId, Long sizeVariationId);

  List<ProductVariation> getAllProductsAndVariationDetails(long orgId, long branchId);

  List<ProductVariation> getAllProductsAndVariationDetailsByOrg(long orgId);

  List<Product> getProductsByProductCategoryNameAndOrgId(String productCategoryName, long orgId);

  List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchId(
      String productCategoryName, long orgId, long branchId);

  List<Object[]> getAllProductByParentId(long orgId, long branchId);

  List<Object[]> getAllProductByOrgAndParentId(long orgId);

  List<Object[]> getProductByParentId(long orgId, long branchId, long parentId);

  List<Object[]> getProductByParentIdAndOrgId(long orgId, long parentId);

  List<Product> getProductByorgIdandBranchIdstatus(String productCategoryName, long orgId);

  List<Product> getMenuProductDetailsOrgIdandStatus(
      String productCategoryName, long orgId, long branchId);

  List<Product> getMenuProductDetailsOrgAndBranchAndCategory(
      String productCategoryName, String categoryName, long orgId, long branchId);

  List<Product> getMenuProductDetailsOrgAndCategory(
      String productCategoryName, String categoryName, long orgId);

  List<Map<String, Object>> getProductPackagingDetails();

  Map<String, Object> getProductPackagingDetailsByProductId(long productId);

  List<Product> getProductById(long productId);

  List<Product> getProductByorgIdandBranchIdstatus(long orgId, long branchId);

  List<Product> getProductByorgIdandBranchIdstatus(long orgId);

  List<Product> getProductByorgIdandBranchIdstatusActive(long orgId, long branchId);

  List<Product> getProductByorgIdandstatusActive(long orgId);

  List<Product> getProductsByProductCategoryNameAndOrgIdpagination(
      final String productCategoryName,
      final long orgId,
      final int pageNum,
      final int numOfRecords);

  List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
      final String productCategoryName,
      final long orgId,
      final long branchId,
      final int pageNum,
      final int numOfRecords);

  Long getProductsCountByProductCategoryNameAndOrgId(String productCategoryName, long orgId);

  long getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
      String productCategoryName, long orgId, long branchId);

  List<Object[]> getProductsByProductCategoryNameMap(final String productCategoryName);

  List<Object[]> getProductNameAndIdsByProductCategoryId(final long productCategoryId);

  List<Object[]> getProductsByOrgPath(final String pathUrl, final long productCategoryId);

  List<Object[]> getProductsByCategoryId(final long categoryId);

  String getproductNamesByProductId(final long productId);

  List<Product> getAllproductsByElastic();
}
