package com.a2zbill.dao;

import com.a2zbill.domain.SuggestedContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuggestedContentDao extends JpaRepository<SuggestedContent, Long> {}
