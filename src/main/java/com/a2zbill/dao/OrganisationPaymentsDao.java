package com.a2zbill.dao;

import com.a2zbill.domain.OrganisationPayments;
import java.util.List;

public interface OrganisationPaymentsDao {

  void save(OrganisationPayments organisationPayments);

  List<OrganisationPayments> getAllOrganisationPayments();

  void update(OrganisationPayments organisationPayments);

  List<OrganisationPayments> getOrganisationPaymentsByOrgId(final long orgId);

  OrganisationPayments getOrganisationPaymentsByOrgIdAndPaymentId(
      final long orgId, final long paymentId);
}
