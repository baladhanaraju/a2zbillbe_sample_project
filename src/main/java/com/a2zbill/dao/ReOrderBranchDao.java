package com.a2zbill.dao;

import com.a2zbill.domain.ReOrderBranch;

public interface ReOrderBranchDao {
  void save(ReOrderBranch reOrderBranch);
}
