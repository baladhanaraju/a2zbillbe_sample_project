package com.a2zbill.dao;

import com.a2zbill.domain.Percentage;
import java.util.List;

public interface PercentageDao {
  List<Percentage> getAllPercentages();
}
