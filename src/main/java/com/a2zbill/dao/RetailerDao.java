package com.a2zbill.dao;

import com.a2zbill.domain.Retailer;
import java.util.List;

public interface RetailerDao {
  void save(Retailer retailer);

  List<Retailer> getRetailerDetails();

  Retailer getRetailerDetailsByGSTNumber(String retailerGstNumber);
}
