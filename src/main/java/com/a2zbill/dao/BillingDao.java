package com.a2zbill.dao;

import com.a2zbill.domain.Billing;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface BillingDao {

  void save(Billing billing);

  boolean update(Billing billing);

  Billing getBillingDetailsByCartId(long cartId);

  Billing getBillingDetailsByBillId(long billId);

  Billing getBillingDetailsByBillIdAndOrgId(final long billId, final long orgId);

  Billing getBillingDetailsByBillIdAndOrgIdAndBranchId(
      final long billId, final long orgId, final long branchId);

  List<Billing> getBillingDetailsByDateAndCounter(Date billingdate, long counter);

  BigDecimal getBillingCurrentDaySaleAmount(Date billingdate);

  BigDecimal getBillingCurrentDaySaleAmountByOrgBranch(Date billingDate, long orgId, long branchId);

  BigDecimal getBillingCurrentMonthSaleAmount(int Month, int year);

  BigDecimal getBillingCurrentMonthSaleAmountByorgBranch(
      int Month, int year, long orgId, long branchId);

  BigDecimal getBillingLastSixMonthSaleAmount(int Month, int year);

  List<Billing> getBillingsByCurrentDate(Date billingdate);

  List<Billing> getBillingsByCurrentDateAndOrgId(Date billingdate, Date todate, long orgId);

  List<Billing> getBillingsByCurrentDateAndOrgBranch(long orgId, long branchId, Date billingdate);

  List<Billing> getBillingDetailsByDateAndCounterAndOrgBranch(
      long orgId, long branchId, Date billingdate, long counter);

  List<Billing> getBillingDetailsByDateAndCounterAndOrgId(
      long orgId, Date billingdate, long counter);

  List<Billing> getAllBillingDetailsByDate(Date date);

  List<Billing> getAllBillingDetailsByDateAndBrachIdAndOrgId(Date date, long branchId, long orgId);

  List<Billing> getAllBillingDetailsByCurrentDate();

  List<BigDecimal> getSumOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId);

  List<BigDecimal> getSumOfBillingDetailsByOrgBranchisNull(Date formDate, Date endDate, long orgId);

  List<Billing> getBillingsByStartAndEndDateAndOrgAndBranch(
      Date startDate, Date endDate, long orgId, long branchId);

  List<Object[]> getnetsaleOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId);

  List<Object[]> getAllTxesOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId);

  /*
   * List<Billing> getAllCashOfBillingDetailsByBranchAndOrg(Date formDate, Date
   * endDate, long orgId, long branchId); List<Billing>
   * getAllCreditOfBillingDetailsByBranchAndOrg(Date formDate, Date endDate, long
   * orgId, long branchId); List<Billing>
   * getAllDebitOfBillingDetailsByBranchAndOrg(Date formDate, Date endDate, long
   * orgId, long branchId);
   */
  List<Billing> getBillingsByCategoryAndStartAndEndDateAndOrgAndBranch(
      Date startDate, Date endDate, long orgId, long branchId);

  List<Object[]> getAllDiscountOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId);

  List<Object[]> getAllServiceChargesOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId);

  List<Object[]> getAllServiceChargesOfBillingDetailsByOrg(Date formDate, Date endDate, long orgId);

  List<Object[]> getAllGrossSalesOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId);

  List<Billing> getBillingsByOrgIdByFromdate(Date billingdate, long orgId);

  BigDecimal getBillingCurrentDaySaleAmountByOrg(Date billingDate, long orgId);

  BigDecimal getBillingCurrentMonthSaleAmountorg(int Month, int year, long orgId);

  List<Billing> getBillingDetailsByDatesAndOrgIdAndCounterNumber(
      Date fromDate, Date toDate, long orgId, long counterNumber);

  List<Billing> getBillingDetailsByDatesAndOrgIdAndBranchIdAndCounterNumber(
      Date fromDate, Date toDate, long orgId, long branchId, long counterNumber);

  List<Object[]> getnetsaleOfBillingDetailsByOrgAndBranchisNull(
      Date formDate, Date endDate, long orgId);

  List<Object[]> getAllTxesOfBillingDetailsByOrgAndBranchisNull(
      Date formDate, Date endDate, long orgId);

  /*
   * List<Billing> getAllCashOfBillingDetailsByOrgAndBranchisNull(Date formDate,
   * Date endDate, long orgId); List<Billing>
   * getAllCreditOfBillingDetailsByOrgAndBranchisNull(Date formDate, Date endDate,
   * long orgId); List<Billing>
   * getAllDebitOfBillingDetailsByOrgAndBranchisNull(Date formDate, Date endDate,
   * long orgId);
   */
  List<Object[]> getAllDiscountOfBillingDetailsByOrgAndBranchisNull(
      Date formDate, Date endDate, long orgId);

  List<Object[]> getAllGrossSalesOfBillingDetailsByOrgAndBranchisNull(
      Date formDate, Date endDate, long orgId);

  List<Billing> getBillingDetailsByCustomerId(long custId);

  List<Billing> getBillingByCustomerId(long custId);

  List<Object[]> getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndBranchIdGroupedByPaymentMode(
      Date fromDate, Date endDate, long orgId, long branchId);

  List<Object[]> getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndGroupedByPaymentMode(
      Date fromDate, Date endDate, long orgId);

  List<Object[]> getSaleReportBetweenTwoDatesByOrgIdAndBranchIdAndDates(
      Date fromDate, Date toDate, long orgId, long branchId);

  Billing getBillingDetailsByOrdersId(final long ordersId);

  Billing getBillingDetailsByBillNumberAndOrg(
      final String billNumber, final long orgId, final Date date);

  Billing getBillingDetailsByBillNumberAndOrgAndBranch(
      final String billNumber, final long orgId, final long branchId, final Date date);

  Billing getBillingDetailsByBillNumberCountAndOrg(
      final String billNumber, final long orgId, final Date date);

  Billing getBillingDetailsByBillNumberCountAndOrgAndBranch(
      final String billNumber, final long orgId, final long branchId, final Date date);
}
