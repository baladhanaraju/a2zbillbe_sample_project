package com.a2zbill.dao;

import com.a2zbill.domain.Flows;
import java.util.List;

public interface FlowsDao {
  void save(Flows flows);

  Flows getFlowsById(final Long id);

  List<Flows> getFlowsDetails();
}
