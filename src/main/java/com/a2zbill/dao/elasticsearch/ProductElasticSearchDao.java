package com.a2zbill.dao.elasticsearch;

import com.a2zbill.domain.elasticsearch.ProductElasticSearch;
import java.math.BigDecimal;
import java.util.List;

public interface ProductElasticSearchDao {

  ProductElasticSearch insertProduct(final ProductElasticSearch product);

  public List<ProductElasticSearch> getProductByProductName(final String productName);

  public List<ProductElasticSearch> getProductByProductNameAndOrgId(
      final String searchKey, final long orgId);

  public List<ProductElasticSearch> getProductByProductNameAndBranchIdAndOrgId(
      final String searchKey, final long branchId, final long orgId);

  public ProductElasticSearch getProductByProductIdAndOrgId(final Long productId, final long orgId);

  public ProductElasticSearch getProductByProductIdAndBranchIdAndOrgId(
      final Long productId, final long branchId, final long orgId);

  public List<ProductElasticSearch> getProductsByBranchIdAndOrgIdProductNameAndPriceRange(
      final long branchId,
      final long orgId,
      final String productName,
      final BigDecimal lowPrice,
      final BigDecimal highPrice);

  public List<ProductElasticSearch> getProductsByOrgIdAndProductNameAndPriceRange(
      final long orgId,
      final String productName,
      final BigDecimal lowPrice,
      final BigDecimal highPrice);
}
