package com.a2zbill.dao;

import com.a2zbill.domain.Frequency;
import java.util.List;

public interface FrequencyDao {

  void save(Frequency frequency);

  boolean delete(final long id);

  boolean update(final Frequency frequency);

  List<Frequency> getAllFrequencys();

  Frequency getFrequencyById(long id);
}
