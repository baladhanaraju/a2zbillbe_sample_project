package com.a2zbill.dao;

import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.EmployeeCounter;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import java.math.BigDecimal;
import java.util.List;

public interface CounterProductsDao {

  void save(final EmployeeCounter employeeCounter);

  List<Long> getAssignedCounters(final long branchId, final long orgId);

  List<Long> getOpenCounters();

  Long getEmployeeId(final String emailId);

  void insertEmployeeCounterDetails(final EmployeeCounter employeeCounter);

  List<Long> getEmployeeCounters(final long employeeId);

  List<CartDetail> getCartProductDetails(List<CartDetail> cartDetails);

  List<Long> getRemainCounter();

  List<Long> getAssignedCounters();

  List<Long> getEmployeeCounterIds(final long employeeId);

  void saveVendorproducts(final ProductVendor productId);

  List<BigDecimal> getCounterTotalBillAmount(final long cartId);

  int deleteEmployeeCounters(final long employeeId);

  // boolean update(final ReOrder reOrder);

  void saveReOrder(final ReOrder reorder);

  void updateVendorProducts(final ProductVendor productVendor);

  int updateReOrderByProductIdAndReorderId(final long productId, final long reorderId);

  List<ProductVendor> viewOrderProducts(final long productId);

  ReOrder getReorderDetailsByReorderId(final long reOrderId);

  boolean updateReorder(final ReOrder reOrder);

  List<ReOrder> getReorderListByInvoiceIdAndOrganisationId(final long invoiceId, final long orgId);

  List<ReOrder> getReorderListByInvoiceIdAndOrganisationIdAndBranchId(
      final long invoiceId, final long orgId, final long branchId);

  List<ReOrder> viewReorderDetails(final long productId);

  List<Object[]> viewReorderDetailsByStatus(final long orgId, final long branchId);

  List<ReOrder> getReOrderDetailsByInvoiceId(final long invoiceId);

  List<Object[]> viewReorderDetailsByStatus(final long orgId);

  List<ReOrder> getReorderDetailsByProductId(final long productId);

  List<ReOrder> getAllReOrderDetailsByInvoiceId(final long invoiceId);

  ReOrder getReOrderDetailsById(final long id);

  List<ReOrder> getAllReorderDetailsByOrgId(final long orgId);

  List<ReOrder> getAllReorderDetailsByOrgIdAndBranchId(final long orgId, final long branchId);
}
