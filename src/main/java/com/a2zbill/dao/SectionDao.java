package com.a2zbill.dao;

import com.a2zbill.domain.Section;
import java.util.List;

public interface SectionDao {
  void save(Section section);

  boolean delete(long id);

  boolean update(Section section);

  List<Section> getAllSectionDetails();

  List<Section> getSectionDetailsByOrgIdAndBranchId(long orgId, long branchId);

  List<Section> getSectionDetailsByOrgId(long orgId);

  Section getSectionDetailsBySectionId(long sectionId);

  List<Section> getSectionDetailsByOrgAndStatus(long orgId);

  List<Section> getSectionDetailsByOrgIdAndBranchAndStatus(long orgId, long branchId);

  Section getSectiondetails(String sectionName);

  Section getSectiondetailsdisplayname(String displayName);
}
