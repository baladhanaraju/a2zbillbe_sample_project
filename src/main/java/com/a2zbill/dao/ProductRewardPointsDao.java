package com.a2zbill.dao;

import com.a2zbill.domain.ProductRewardPoint;
import java.util.List;

public interface ProductRewardPointsDao {

  public void save(ProductRewardPoint productRewardPoint);

  boolean delete(final long id);

  boolean update(final ProductRewardPoint productRewardPoint);

  List<ProductRewardPoint> getAllProductRewardPoints();

  ProductRewardPoint getAllProductRewardPointsByproductId(
      final long productId, final long orgId, final long branchId);

  ProductRewardPoint getAllProductRewardPointsByproductIdOrgId(
      final long productId, final long orgId);

  ProductRewardPoint getProductRewardPointsByproductId(final long productId);
}
