package com.a2zbill.dao;

import com.a2zbill.domain.Order;
import com.a2zbill.domain.OrderStatus;
import java.util.List;

public interface OrderDao {
  public void save(final Order order);

  public boolean delete(final long id);

  public boolean update(final Order Order);

  List<Order> getAllOrderDetails();

  Order getOrderById(final long id);

  Order getOrderByGuid(final String guid);

  Order getOrdersDetailsByStatus(final String orderStatus, final long counterId);

  OrderStatus getOrderStatusById(final long id);
}
