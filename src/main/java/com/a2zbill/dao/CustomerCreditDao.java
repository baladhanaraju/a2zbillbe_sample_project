package com.a2zbill.dao;

import com.a2zbill.domain.CustomerCredit;
import com.a2zbill.domain.CustomerCreditHistory;
import java.util.List;

public interface CustomerCreditDao {

  void save(CustomerCredit customerCredit);

  void update(CustomerCredit customerCredit);

  CustomerCredit getCustomerCreditDetailsByCustomerId(long customerId);

  List<CustomerCreditHistory> getCustomerCreditHistoryByCustomerCreditId(long customerCreditId);

  List<CustomerCreditHistory> getAllCustomerCreditHistoryByOrgAndBranch(long orgId, long branchId);

  List<CustomerCreditHistory> getAllCustomerCreditHistoryByOrg(long orgId);

  List<CustomerCredit> getAllCustomerCreditsByOrgAndBranch(long orgId, long branchId);

  List<CustomerCredit> getAllCustomerCreditsByOrg(long orgId);

  void saveCustomerCreditHistory(CustomerCreditHistory customerCreditHistory);

  void updateCustomerCreditHistory(CustomerCreditHistory customerCreditHistory);
}
