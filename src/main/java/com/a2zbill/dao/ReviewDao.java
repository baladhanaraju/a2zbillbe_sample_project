package com.a2zbill.dao;

import com.a2zbill.domain.Review;
import java.util.List;

public interface ReviewDao {
  void save(Review review);

  boolean delete(Long id);

  boolean update(Review review);

  List<Review> getAllReviewDetails();

  List<Review> getAllReviewDetailsByOrgBranchId(long orgId, long branchId);

  List<Review> getAllReviewDetailsByOrg(long orgId);
}
