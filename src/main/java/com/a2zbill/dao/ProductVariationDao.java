package com.a2zbill.dao;

import com.a2zbill.domain.ProductVariation;
import java.util.List;

public interface ProductVariationDao {

  void save(ProductVariation productVariation);

  boolean delete(long id);

  boolean update(ProductVariation productVariation);

  List<ProductVariation> getAllProductVariationDetails();

  List<ProductVariation> getProductVariationDetailsByOrgIdAndBranchId(long orgId, long branchId);

  List<ProductVariation> getProductVariationDetailsByOrgId(long orgId);

  ProductVariation getProductVariationDetailsByProductVariationId(long productVariationId);

  List<Object[]> getproductVariationDetails(long orgId, long branchId);

  List<Object[]> getproductVariationDetailsByOrg(long orgId);

  List<ProductVariation> getProductVariationByProductIdAndOrgBranchId(
      long productId, long orgId, long branchId);

  List<ProductVariation> getProductVariationByProductIdAndOrg(long productId, long orgId);
}
