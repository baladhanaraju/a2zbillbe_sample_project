package com.a2zbill.dao;

import com.a2zbill.domain.ProductComposition;
import java.util.List;

public interface ProductCompositionDao {
  void save(ProductComposition productComposition);

  boolean delete(long id);

  boolean update(ProductComposition productComposition);

  List<ProductComposition> getAllProductComposition();

  List<ProductComposition> getProductCompositionbyPackageFlag(Boolean packageFlag);

  List<ProductComposition> getAllProductCompositionByProductId(long productId);
}
