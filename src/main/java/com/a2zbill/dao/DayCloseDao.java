package com.a2zbill.dao;

import com.a2zbill.domain.DayClose;
import java.util.Date;
import java.util.List;

public interface DayCloseDao {
  void save(DayClose DayClose);

  boolean update(DayClose DayClose);

  List<DayClose> getAllDayCloseDatails();

  List<DayClose> getDayCloseDatails(Date fromdate, Date toDate, long branchId);
}
