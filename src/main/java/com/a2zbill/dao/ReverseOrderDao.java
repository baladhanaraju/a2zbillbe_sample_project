package com.a2zbill.dao;

import com.a2zbill.domain.ReverseOrder;
import java.util.List;

public interface ReverseOrderDao {
  void save(ReverseOrder reverseOrder);

  boolean update(ReverseOrder reverseOrder);

  boolean delete(long id);

  List<ReverseOrder> getAllReverseOrderDetails();

  ReverseOrder getReverseOrderDetailsById(final long reverseOrderId);
}
