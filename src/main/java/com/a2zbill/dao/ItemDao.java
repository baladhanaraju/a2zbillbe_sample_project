package com.a2zbill.dao;

import com.a2zbill.domain.Item;
import java.util.List;

public interface ItemDao {

  void save(final Item items);

  boolean delete(final Long id);

  boolean update(final Item items);

  Item getItemsById(final Long id);

  List<Item> getAllItemsName();

  Item getItemsByHsnNo(final String hsnNo);

  List<Item> getAllItemsByHsnNo();

  Item getItemsByName(final String commodity);
}
