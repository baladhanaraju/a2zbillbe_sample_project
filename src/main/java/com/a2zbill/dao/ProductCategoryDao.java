package com.a2zbill.dao;

import com.a2zbill.domain.ProductCategory;
import com.tsss.basic.domain.MyJson;
import java.util.List;

public interface ProductCategoryDao {
  void save(final ProductCategory productCategory);

  boolean update(final ProductCategory productCategory);

  List<ProductCategory> getAllProductCategory();

  ProductCategory getProductCategoryByProductCategoryName(final String productCategoryName);

  ProductCategory getProductCategoryByProductCategoryNameorgIdAndbrnachId(
      final String productCategoryName, final long orgId, final long brnachId);

  ProductCategory getProductCategoryById(final Long id);

  MyJson getFeaturesById(final Long id);

  ProductCategory gettFeaturesById(final Long id);

  List<ProductCategory> getProductCategorys(final long orgId);

  List<ProductCategory> getProductCategorysByOrgAndBranchId(final long branchId, final long orgId);

  List<ProductCategory> getProductCategorysBasedBranchIdAndOrgId(
      final long branchId, final long orgId);

  List<ProductCategory> getProductCategorysBasedBranchIdAndOrgIdAndParentId(
      final long branchId, final long orgId, final long categoryTypeId, final long parentId);

  List<ProductCategory> getProductCategorysBasedOnOrgIdAndParentId(
      final long orgId, final long categoryTypeId, final long parentId);

  List<ProductCategory> getProductCategorysBasedBranchIdAndOrgId(
      final long branchId, final long orgId, final long categoryTypeId);

  List<ProductCategory> getProductCategoriesIfRedeemFlagIsTrue(
      final long branchId, final long orgId);

  List<ProductCategory> getProductCategoriesRedeemFlagIsTrue(final long orgId);

  List<ProductCategory> getProductCategoriesIfRedeemFlagIsFalseAndOrgIdAndBranchId(
      final long branchId, final long orgId);

  List<ProductCategory> getProductCategoriesIfRedeemFlagIsFalseAndOrgId(final long orgId);

  ProductCategory getProductCategoryByProductCategoryNameorgId(
      final String productCategoryName, final long orgId);

  List<ProductCategory> getProductCategoryByBranchIdAndOrgIdAndParentId(
      final long branchId, final long orgId, final long parentId);

  List<ProductCategory> getProductCategoryByOrgIdAndParentId(final long orgId, final long parentId);

  List<ProductCategory> getProductCategoriesByCategoryTypeId(final long categoryTypeId);

  List<ProductCategory> getProductCategoriesBasedOnOrgIdAndCategoryType(
      final long orgId, final long categoryTypeId);

  ProductCategory getProductCategoryByProductTypeName(final String productTypeName);

  List<String> getProductCategoryNames(
      final long branchId, final long orgId, final long categoryTypeId);

  List<Object[]> getProductCategoryNameAndIdMap(final long branchId, final long orgId);

  List<Long> getProductCategoryIds(
      final long branchId, final long orgId, final long categoryTypeId);

  List<Object[]> getCategoriesBycategoryType(final long categoryId, final String pathUrl);

  Integer[] getCategoryTypesByStoreTemplateId(long storeTemplateId);

  List<Object[]> getProductCategorysByOrg(final long orgId);

  List<Object[]> getProductCategorysByOrgAndBranch(final long orgId, final long branchId);
}
