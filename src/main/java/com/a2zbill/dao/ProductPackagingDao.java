package com.a2zbill.dao;

import com.a2zbill.domain.ProductPackaging;
import java.util.List;

public interface ProductPackagingDao {

  void save(ProductPackaging productPackging);

  boolean delete(ProductPackaging productPackging);

  boolean update(ProductPackaging productPackging);

  List<ProductPackaging> getAllProductPackagingDetails();

  ProductPackaging getProductPackagingDetailsByProductId(final long productId);

  List<ProductPackaging> getProductPackgingDetailsByParentId(final long parentId);
}
