package com.a2zbill.dao;

import com.a2zbill.domain.CustomerVendorToken;
import java.util.List;

public interface CustomerVendorTokenDao {

  void save(CustomerVendorToken customerVendorToken);

  List<CustomerVendorToken> getAllCustomerVendorToken();

  CustomerVendorToken getTokenAndRootNameBymobileNumber(
      final String mobileNumber, final long orgId);
}
