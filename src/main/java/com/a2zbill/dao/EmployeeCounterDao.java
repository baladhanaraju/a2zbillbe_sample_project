package com.a2zbill.dao;

import com.a2zbill.domain.EmployeeCounter;
import java.util.List;

public interface EmployeeCounterDao {
  List<EmployeeCounter> getAllEmployeeCounterDetails();

  List<EmployeeCounter> getAllEmployeeCounterDetailsByorgIdBranchId(
      final long orgId, final long branchId);

  List<EmployeeCounter> getAllEmployeeCounterDetailsByorgId(final long orgId);
}
