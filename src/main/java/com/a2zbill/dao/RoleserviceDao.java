package com.a2zbill.dao;

import com.tsss.basic.domain.Roles;
import java.util.List;

public interface RoleserviceDao {
  void save(Roles role);

  boolean delete(long id);

  boolean update(Roles role);

  List<Roles> getAllRolesServiceDetails();
}
