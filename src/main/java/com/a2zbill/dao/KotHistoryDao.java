package com.a2zbill.dao;

import com.a2zbill.domain.KotHistory;

public interface KotHistoryDao {

  void save(KotHistory kotHistory);

  void update(KotHistory kotHistory);
}
