package com.a2zbill.dao;

import com.a2zbill.domain.InventoryReports;
import java.util.Date;
import java.util.List;

public interface InventoryReportsDao {

  InventoryReports getInventoryByDateAndInventoryId(
      Date createdDate, long inventoryId, long orgId, long branchId);

  void save(InventoryReports inventoryReports);

  boolean update(InventoryReports inventoryReports);

  List<InventoryReports> getInventoryReportsByDate(Date date, long branchId);
}
