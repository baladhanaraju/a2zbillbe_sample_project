package com.a2zbill.dao;

import com.a2zbill.domain.FormSchema;
import java.util.List;
import java.util.Map;

public interface FormSchemaDao {

  void save(final FormSchema formSchema);

  boolean delete(final long id);

  boolean update(final FormSchema formSchema);

  List<FormSchema> getAllFormSchema();

  Map<String, Object> getFormSchemaDetailsById(final long Id);

  FormSchema getAllFormSchemaDetailsById(final long Id);
}
