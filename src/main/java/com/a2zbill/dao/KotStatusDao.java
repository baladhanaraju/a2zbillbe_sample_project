package com.a2zbill.dao;

import com.a2zbill.domain.KotStatus;
import java.util.List;

public interface KotStatusDao {

  void saveKotStatus(final KotStatus kotStatus);

  void update(final KotStatus kotStatus);

  boolean delete(final Long id);

  List<KotStatus> getAllKotStatus();

  KotStatus getKotStatusById(final long id);

  List<KotStatus> getKotStatusByCounterNumber(final long counterNumber);

  List<KotStatus> getKotStatusByCounterNumber(final long counterNumber, final long branchId);

  KotStatus getKotStatusByBranchId(
      final long branchId, final String productName, final int quantity, final long counterNumber);
}
