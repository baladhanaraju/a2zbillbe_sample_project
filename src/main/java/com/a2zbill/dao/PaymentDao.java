package com.a2zbill.dao;

import com.a2zbill.domain.Payment;
import java.util.List;

public interface PaymentDao {
  void save(final Payment payment);

  void update(final Payment payment);

  List<Payment> getAllPayments();

  void savePaymentMode(final Payment payment);
  // List<Payment> getPaymentDetailsByOrgIdAndBranchId(long orgId, long branchId);
  Payment getPaymentType(final String paymentType);

  Payment getPaymentById(final long paymentId);

  List<Payment> getAllPaymentsDefaultTrue();
}
