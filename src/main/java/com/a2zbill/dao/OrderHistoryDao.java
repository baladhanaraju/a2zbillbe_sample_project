package com.a2zbill.dao;

import com.a2zbill.domain.OrderHistory;
import java.util.List;

public interface OrderHistoryDao {

  public void save(final OrderHistory orderHistory);

  public boolean delete(final long id);

  public boolean update(final OrderHistory orderHistory);

  List<OrderHistory> getAllOrderHistoryDetails();

  OrderHistory getOrderHistoryById(final long id);
}
