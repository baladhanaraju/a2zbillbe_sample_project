package com.a2zbill.dao;

import com.a2zbill.domain.Cart;
import java.util.List;

public interface CartDao {
  void save(Cart cart);

  boolean delete(Long id);

  boolean update(Cart cart);

  List<Cart> getAllCartDetails();

  Cart getCartDetailsOnStatus(final String cartStatus, final long counterId);

  List<Cart> getCartDetailsByCounterId(long counterId);

  Cart getCartDetailsByCartId(final long cartId);

  List<Cart> getCartDetailsByStatus();

  Cart getCartByGuid(final String guid);

  Cart getCartDetailsByStatus(final String cartStatus, final long customerId, final long orgId);
}
