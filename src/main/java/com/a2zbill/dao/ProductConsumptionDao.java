package com.a2zbill.dao;

import com.a2zbill.domain.ProductConsumption;
import java.util.Date;
import java.util.List;

public interface ProductConsumptionDao {

  void save(ProductConsumption productConsumption);

  boolean delete(final long id);

  boolean update(ProductConsumption productComposition);

  List<ProductConsumption> getAllProductConsumption();

  ProductConsumption getProductConsumptionById(long id);

  ProductConsumption getProductConsumptionByproductId(long productId);

  ProductConsumption getProductConsumptionByproductAndcurrentDate(long productId, Date date);

  ProductConsumption
      getProductConsumptionByproductAndmenuProductAndwastageFlagAnddateAndOrgAndBranch(
          long productId, long menuProductId, boolean status, Date date, long orgId, long branchId);

  ProductConsumption getProductConsumptionBymenuProductAndwastageFlagAnddateAndOrgAndBranch(
      long menuProductId, boolean status, Date date, long orgId, long branchId);
}
