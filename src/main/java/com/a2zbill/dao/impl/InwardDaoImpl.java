package com.a2zbill.dao.impl;

import com.a2zbill.dao.InwardDao;
import com.a2zbill.domain.Inward;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InwardDaoImpl implements InwardDao {

  private static final Logger logger = LoggerFactory.getLogger(InwardDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final Inward inwards) {
    try {
      entityManager.persist(inwards);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final Inward inwards) {
    try {
      entityManager.merge(inwards);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Inward> getAllInwards() {

    List<Inward> inwardDetails = new ArrayList<>();
    try {
      inwardDetails = entityManager.createQuery("from Inward order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInwards ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inwardDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Inward> getInwardDetailsByMonthAndYear(final String month, final String year) {

    List<Inward> inward = new ArrayList<>();
    try {
      inward =
          this.entityManager
              .createQuery("from Inward where month=?1 and year=?2")
              .setParameter(1, month)
              .setParameter(2, year)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInwardDetailsByMonthAndYear ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inward;
  }
}
