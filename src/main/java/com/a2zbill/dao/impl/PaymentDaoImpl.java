package com.a2zbill.dao.impl;

import com.a2zbill.dao.PaymentDao;
import com.a2zbill.domain.Payment;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentDaoImpl implements PaymentDao {

  private static final Logger logger = LoggerFactory.getLogger(PaymentDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(Payment payment) {
    try {
      entityManager.persist(payment);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Payment> getAllPayments() {

    List<Payment> paymentDetails = new ArrayList<>();
    try {
      paymentDetails =
          this.entityManager.createQuery("from Payment order by payment_id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllPayments ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return paymentDetails;
  }

  @Override
  public void savePaymentMode(Payment payment) {
    try {
      entityManager.persist(payment);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public Payment getPaymentType(String paymentType) {

    Payment paymentTypeDetails = null;
    try {
      paymentTypeDetails =
          (Payment)
              entityManager
                  .createQuery("from Payment p where paymentType=?1")
                  .setParameter(1, paymentType)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getPaymentType ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return paymentTypeDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Payment> getAllPaymentsDefaultTrue() {

    List<Payment> paymentDetails = new ArrayList<>();
    try {
      paymentDetails = entityManager.createQuery("from Payment where defalt=true").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllPaymentsDefaultTrue ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return paymentDetails;
  }

  @Override
  public Payment getPaymentById(long paymentId) {

    Payment payment = null;
    try {
      payment =
          (Payment)
              entityManager
                  .createQuery("FROM Payment where paymentId=?1")
                  .setParameter(1, paymentId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getPaymentById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return payment;
  }

  @Override
  public void update(Payment payment) {
    try {
      entityManager.merge(payment);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }
}
