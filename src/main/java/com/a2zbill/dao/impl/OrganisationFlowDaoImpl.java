package com.a2zbill.dao.impl;

import com.a2zbill.dao.OrganisationFlowDao;
import com.a2zbill.domain.OrganisationFlow;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrganisationFlowDaoImpl implements OrganisationFlowDao {

  private static final Logger logger = LoggerFactory.getLogger(OrganisationFlowDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(OrganisationFlow organisationFlow) {
    try {
      entityManager.persist(organisationFlow);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public OrganisationFlow getOrganisationFlowById(final Long id) {

    OrganisationFlow organisationFlow = null;
    try {
      organisationFlow =
          (OrganisationFlow)
              entityManager
                  .createQuery("from Flows where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrganisationFlowById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return organisationFlow;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrganisationFlow> getOrganisationFlowByOrgId(final Long orgId) {

    List<OrganisationFlow> organisationFlow = new ArrayList<>();
    try {
      organisationFlow =
          entityManager
              .createQuery("from OrganisationFlow where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrganisationFlowByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return organisationFlow;
  }
}
