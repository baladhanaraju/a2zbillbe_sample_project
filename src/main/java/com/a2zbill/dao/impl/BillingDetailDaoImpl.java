package com.a2zbill.dao.impl;

import com.a2zbill.dao.BillingDetailDao;
import com.a2zbill.domain.BillingDetail;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BillingDetailDaoImpl implements BillingDetailDao {

  private static final Logger logger = LoggerFactory.getLogger(BillingDetailDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(BillingDetail billingDetails) {
    try {
      entityManager.persist(billingDetails);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(BillingDetail billingDetails) {
    try {
      entityManager.merge(billingDetails);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BillingDetail> getAllBillingDetails() {

    List<BillingDetail> billingDetail = new ArrayList<>();
    try {
      billingDetail = entityManager.createQuery("from BillingDetails order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllBillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingDetail;
  }
}
