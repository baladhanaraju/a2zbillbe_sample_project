package com.a2zbill.dao.impl;

import com.a2zbill.dao.FormSchemaDao;
import com.a2zbill.domain.FormSchema;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FormSchemaDaoImpl implements FormSchemaDao {

  private static final Logger logger = LoggerFactory.getLogger(FormSchemaDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final FormSchema formSchema) {
    try {
      entityManager.persist(formSchema);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final FormSchema formSchema) {
    try {
      entityManager.merge(formSchema);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<FormSchema> getAllFormSchema() {

    List<FormSchema> formSchema = new ArrayList<>();
    try {
      formSchema =
          entityManager
              .createQuery("select formschema from FormSchema order by id")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllFormSchema ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formSchema;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Map<String, Object> getFormSchemaDetailsById(final long Id) {

    Map<String, Object> formSchema = null;
    try {
      formSchema =
          (Map<String, Object>)
              entityManager
                  .createQuery("select formschema from FormSchema where id=?1")
                  .setParameter(1, Id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFormSchemaDetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formSchema;
  }

  @Override
  public FormSchema getAllFormSchemaDetailsById(final long Id) {

    FormSchema formSchema = null;
    try {
      formSchema =
          (FormSchema)
              entityManager
                  .createQuery(" from FormSchema where id=?1")
                  .setParameter(1, Id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllFormSchemaDetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formSchema;
  }
}
