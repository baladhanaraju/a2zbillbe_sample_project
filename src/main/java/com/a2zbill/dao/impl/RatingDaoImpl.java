package com.a2zbill.dao.impl;

import com.a2zbill.dao.RatingDao;
import com.a2zbill.domain.Rating;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RatingDaoImpl implements RatingDao {

  private static final Logger logger = LoggerFactory.getLogger(RatingDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Rating rating) {
    try {
      entityManager.persist(rating);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(Rating rating) {
    try {
      entityManager.merge(rating);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Rating> getAllRatingDetails() {

    List<Rating> ratingDetails = new ArrayList<>();
    try {
      ratingDetails = entityManager.createQuery("from Rating order by id").getResultList();

    } catch (final Exception ex) {
      logger.error("getAllRatingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return ratingDetails;
  }

  @Override
  public Rating getRatingByRatingId(final Long ratingId) {

    Rating ratingDetails = null;
    try {
      ratingDetails =
          (Rating)
              entityManager
                  .createQuery("FROM Rating where id=?1")
                  .setParameter(1, ratingId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRatingByRatingId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return ratingDetails;
  }

  @Override
  public Rating getRatingByProductId(final Long productId) {

    Rating ratingDetails = null;
    try {
      ratingDetails =
          (Rating)
              entityManager
                  .createQuery("FROM Rating where product.id=?1")
                  .setParameter(1, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRatingByProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return ratingDetails;
  }

  @Override
  public Rating getRatingByEmpIdAndProductId(final Long productId, final Long empId) {

    Rating ratingDetails = null;
    try {
      ratingDetails =
          (Rating)
              entityManager
                  .createQuery("FROM Rating where product.id =?1 and empDetails.id =?2")
                  .setParameter(1, productId)
                  .setParameter(2, empId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRatingByEmpIdAndProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return ratingDetails;
  }
}
