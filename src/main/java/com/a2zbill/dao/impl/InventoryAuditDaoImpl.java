package com.a2zbill.dao.impl;

import com.a2zbill.dao.InventoryAuditDao;
import com.a2zbill.domain.InventoryAudit;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryAuditDaoImpl implements InventoryAuditDao {

  private static final Logger logger = LoggerFactory.getLogger(InventoryAuditDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(final InventoryAudit inventoryAudit) {
    try {
      entityManager.persist(inventoryAudit);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final InventoryAudit inventoryAudit) {
    try {
      entityManager.remove(inventoryAudit);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final InventoryAudit inventoryAudit) {
    try {
      entityManager.merge(inventoryAudit);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryAudit> getAllInventoryAudit() {

    List<InventoryAudit> inventoryAudit = new ArrayList<>();
    try {
      inventoryAudit = entityManager.createQuery("from InventoryAudit order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryAudit ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryAudit;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryAudit> getAllInventoryAuditByOrgBranchId(
      final long orgId, final long branchId) {

    List<InventoryAudit> inventoryAudit = new ArrayList<>();
    try {
      inventoryAudit =
          entityManager
              .createQuery("from InventoryAudit where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryAuditByOrgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryAudit;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryAudit> getAllInventoryAuditByOrg(final long orgId) {

    List<InventoryAudit> inventoryAudit = new ArrayList<>();
    try {
      inventoryAudit =
          entityManager
              .createQuery("from InventoryAudit where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryAuditByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryAudit;
  }
}
