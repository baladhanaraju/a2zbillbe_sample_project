package com.a2zbill.dao.impl;

import com.a2zbill.dao.InvoiceDetailsDao;
import com.a2zbill.domain.InvoiceDetails;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InvoiceDetailsDaoImpl implements InvoiceDetailsDao {

  private static final Logger logger = LoggerFactory.getLogger(InvoiceDetailsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(InvoiceDetails invoiceDetails) {
    try {
      entityManager.persist(invoiceDetails);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(InvoiceDetails invoiceDetails) {
    try {
      entityManager.merge(invoiceDetails);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceDetails> getAllInvoiceDetails() {

    List<InvoiceDetails> invoiceDetails = new ArrayList<>();
    try {
      invoiceDetails = entityManager.createQuery("from InvoiceDetails order by id").getResultList();

    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceDetails;
  }

  @Override
  public InvoiceDetails getInvoiceDetailsByVendorId(final long vendorId) {

    InvoiceDetails InvoiceDetails = null;
    try {
      InvoiceDetails =
          (InvoiceDetails)
              entityManager
                  .createQuery("FROM InvoiceDetails where vendor.id=?1")
                  .setParameter(1, vendorId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInvoiceDetailsByVendorId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return InvoiceDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceDetails> getAllInvoiceDetailsByOrg(long orgId) {

    List<InvoiceDetails> invoiceDetails = new ArrayList<>();
    try {
      invoiceDetails =
          entityManager
              .createQuery("from InvoiceDetails where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceDetails;
  }

  @Override
  public InvoiceDetails getInvoiceDetailsByInvoiceId(long invoiceId) {

    InvoiceDetails invoiceDetails = null;
    try {
      invoiceDetails =
          (InvoiceDetails)
              entityManager
                  .createQuery("from InvoiceDetails where id=?1")
                  .setParameter(1, invoiceId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInvoiceDetailsByInvoiceId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceDetails> getAllInvoiceDetailsByOrgAndBranch(long orgId, long branchId) {

    List<InvoiceDetails> invoiceDetails = new ArrayList<>();
    try {
      invoiceDetails =
          entityManager
              .createQuery("from InvoiceDetails where organisation.id=?1 and branch.id=?2 ")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInvoiceDetailsByVendor(final long vendorId, final long orgId) {
    List<Object[]> invoiceDetails = new ArrayList<>();
    try {
      invoiceDetails =
          entityManager
              .createQuery(
                  "select id, startDate, endDate, status, reason,branch.branchName,vendor.vendorName from InvoiceDetails where vendor.id=?1 and organisation.id=?2")
              .setParameter(1, vendorId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceDetails;
  }
}
