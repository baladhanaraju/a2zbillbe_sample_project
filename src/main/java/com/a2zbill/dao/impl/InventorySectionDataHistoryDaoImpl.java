package com.a2zbill.dao.impl;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.dao.InventorySectionDataHistoryDao;
import com.a2zbill.domain.InventorySectionDataHistory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventorySectionDataHistoryDaoImpl implements InventorySectionDataHistoryDao {

  private static final Logger logger =
      LoggerFactory.getLogger(InventorySectionDataHistoryDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(InventorySectionDataHistory inventorySectionDataHistory) {
    try {
      entityManager.persist(inventorySectionDataHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(InventorySectionDataHistory inventorySectionDataHistory) {
    try {
      entityManager.merge(inventorySectionDataHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory> getAllInventorySectionDataHistory() {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager.createQuery("from InventorySectionDataHistory order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionDataHistory ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @Override
  public InventorySectionDataHistory getInventorySectionDataHistoryByInventorySectionId(
      final long inventorySectionId) {
    return null;
  }

  @Override
  public InventorySectionDataHistory getInventorySectionDataHistoryByproductIdAndOrgIdBranchId(
      final long productId, final long orgId, final long branchId) {
    return null;
  }

  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByOrgIdBranchId(
      final long orgId, final long branchId) {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByItemWiseOrgIdBranchId(
      final long orgId, final long branchId, final Date date) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createNativeQuery(
                  "select sum(i.quantity),p.product_name from product.inventory_section_data_history as i\r\n"
                      + "INNER JOIN product.products as p ON i.product_id=p.id \r\n"
                      + "INNER JOIN product.inventory_section as v ON i.inventory_section_id=v.id \r\n"
                      + "where v.consumed_flag='true' and i.modified_date=?3\r\n"
                      + "group by branchid,orgid ,p.product_name Having orgid=?1 and branchid=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, date)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByItemWiseOrgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseOrgIdBranchIdByDateWise(
          final long orgId, final long branchId, final Date fromdate, final Date todate) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createNativeQuery(
                  "select sum(i.quantity),p.product_name from product.inventory_section_data_history as i INNER JOIN product.products as p ON i.product_id=p.id INNER JOIN product.inventory_section as v ON i.inventory_section_id=v.id where v.consumed_flag='true' and i.modified_date>=? and i.modified_date<=? group by branchid,orgid ,p.product_name Having orgid=? and branchid=?")
              .setParameter(1, fromdate)
              .setParameter(2, todate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByItemWiseOrgIdBranchIdByDateWise ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByItemWiseOrgIdByDateWise(
      final long orgId, final Date fromDate, final Date toDate) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createNativeQuery(
                  "select sum(i.quantity),p.product_name from product.inventory_section_data_history as i INNER JOIN product.products as p ON i.product_id=p.id INNER JOIN product.inventory_section as v ON i.inventory_section_id=v.id where v.consumed_flag='true' and i.modified_date>=? and i.modified_date<=? group by orgid ,p.product_name Having orgid=?")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByItemWiseOrgIdByDateWise ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getSumOfInventoryHistoryDetailsByOrgBranch(
      Date formDate, Date endDate, long orgId, long branchId) {

    List<Object[]> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createNativeQuery(
                  "select sum(quantity) as qty,sum(amount) as amt, p.product_name from product.inventory_section_data_history  as i INNER JOIN product.products as p On i.product_id=p.id where date>=?1 and date<=?2 and orgid=?3 and branchid=?4 group by p.product_name")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSumOfInventoryHistoryDetailsByOrgBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getSumOfInventoryHistoryDetailsByOrgBranchisNull(
      Date formDate, Date endDate, long orgId) {

    List<Object[]> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createNativeQuery(
                  "select sum(quantity) as qty,sum(amount) as amt, p.product_name from product.inventory_section_data_history  as i INNER JOIN product.products as p On i.product_id=p.id where date>=?1 and date<=?2  and orgid=?3 group by p.product_name")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSumOfInventoryHistoryDetailsByOrgBranchisNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchId(
      long orgId, long branchId, Date fromdate, Date todate, long frombranchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),inventorySectionId.sectionName,date,branch.branchName, fromSectionId.sectionName,fromBranchId.branchName from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and fromBranchId.id=?3 group by inventorySectionId.sectionName,date,branch.branchName, fromSectionId.sectionName,fromBranchId.branchName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByBillIdAndDates(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long sectionId,
      long toSectionId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromSectionId.sectionName from InventorySectionDataHistory i where i.date between :stdate and :enddate  and referenceNumber=?1 and branch.id=?2 and organisation.id=?3 and fromSectionId.id=?4 and inventorySectionId.id=?5 group by referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromSectionId.sectionName")
              .setParameter(Constants.STDATE, formDate)
              .setParameter(Constants.ENDDATE, endDate)
              .setParameter(1, referenceNumber)
              .setParameter(2, branchId)
              .setParameter(3, orgId)
              .setParameter(4, sectionId)
              .setParameter(5, toSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByBillIdAndDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryTotalAmt(
      Date formDate, Date endDate, long branchId, long orgId, long sectionId, long toSectionId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromSectionId.sectionName from InventorySectionDataHistory i where i.date between :stdate and :enddate  and branch.id=?1 and organisation.id=?2 and fromSectionId.id=?3 and inventorySectionId.id=?4  group by referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromSectionId.sectionName")
              .setParameter(Constants.STDATE, formDate)
              .setParameter(Constants.ENDDATE, endDate)
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .setParameter(3, sectionId)
              .setParameter(4, toSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryTotalAmt ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSectionId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId, long fromSectionId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),inventorySectionId.sectionName,date,branch.branchName, fromSectionId.sectionName from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and inventorySectionId.id=?3 and fromSectionId.id=?4 group by inventorySectionId.sectionName,date,branch.branchName, fromSectionId.sectionName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, sectionId)
              .setParameter(4, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSumOfQuantity(
          long orgId, long branchId, Date fromdate, Date todate) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select productId.productName,SUM(quantity),buyingPrice,amount,sectionName,date,branch.branchName from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 group by productId.productName,buyingPrice,amount,sectionName,date")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSumOfQuantity ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryBySection(
      long orgId, long branchId, long sectionId) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "from InventorySectionDataHistory where fromSectionId.id=?1 and fromBranchId.id=?2 and organisation.id=?3")
              .setParameter(1, sectionId)
              .setParameter(2, branchId)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryBySection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByVendorIdBranchId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId, long vendorId) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select vendor.vendorName,referenceNumber,date,SUM(amount) from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and inventorySectionId.id=?3 and vendor.id=?4 group by vendor.vendorName,referenceNumber,date")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, sectionId)
              .setParameter(4, vendorId)
              .getResultList();
      System.out.println("inventorySectionDataHistory size" + inventorySectionDataHistory.size());
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByVendorIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByBranchId(
      long orgId, long branchId, Date fromDate, Date toDate, long sectionId) {

    List<InventorySectionDataHistory> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select vendor.vendorName,SUM(amount) from InventorySectionDataHistory i where i.date between :stdate and :enddate and branch.id=?1 and organisation.id=?2 and inventorySectionId.id=?3 group by vendor.vendorName")
              .setParameter(Constants.STDATE, fromDate)
              .setParameter(Constants.ENDDATE, toDate)
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .setParameter(3, sectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdFromBranchId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId, long fromBranchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),date,branch.branchName,fromBranchId.branchName,inventorySectionId.sectionName from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and inventorySectionId.id=?3 and fromBranchId.id=?4 group by date,branch.branchName,fromBranchId.branchName,inventorySectionId.sectionName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, sectionId)
              .setParameter(4, fromBranchId)
              .getResultList();
      System.out.println("inventorySectionDataHistory size" + inventorySectionDataHistory.size());
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByDepartmentWiseOrgIdFromBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByTotalAmt(
      Date formDate, Date endDate, long fromSectionId, long orgId, long branchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),vendor.vendorName,referenceNumber,branch.branchName,inventorySectionId.sectionName,date,organisation.orgName from InventorySectionDataHistory i where i.date between :stdate and :enddate and inventorySectionId.id=?1 and organisation.id=?2 and branch.id=?3  group by vendor.vendorName,referenceNumber,branch.branchName,inventorySectionId.sectionName,date,organisation.orgName")
              .setParameter(Constants.STDATE, formDate)
              .setParameter(Constants.ENDDATE, endDate)
              .setParameter(1, fromSectionId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByTotalAmt ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdAndSectionId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          (List<Object[]>)
              entityManager
                  .createQuery(
                      "select productId.productName,quantity,buyingPrice,amount,date from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and inventorySectionId.id=?3 and vendor.id IS NULL")
                  .setParameter(Constants.STDATE, fromdate)
                  .setParameter(Constants.ENDDATE, todate)
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .setParameter(3, sectionId)
                  .getResultList();
      System.out.println("inventorySectionDataHistory size" + inventorySectionDataHistory.size());
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdAndSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]>
      getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromBranchId(
          Date fromDate, Date endDate, long orgId, long sectionId, long fromBranchId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createNativeQuery(
                  "select date,sum(amount),from_branch_id,inventory_section_id,reference_number from product.inventory_section_data_history where date between ?1 and ?2 and  orgid = ?3 and inventory_section_id = ?4 and from_branch_id=?5 group by reference_number,inventory_section_id, from_branch_id, date")
              .setParameter(1, fromDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, sectionId)
              .setParameter(5, fromBranchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(
          "getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]>
      getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromSectionId(
          Date fromDate, Date endDate, long orgId, long sectionId, long fromSectionId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createNativeQuery(
                  "select date,sum(amount),from_section_id,inventory_section_id,reference_number from product.inventory_section_data_history where date between ?1 and ?2 and  orgid = ?3 and inventory_section_id = ?4 and from_section_id=?5 group by reference_number,inventory_section_id, from_section_id, date")
              .setParameter(1, fromDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, sectionId)
              .setParameter(5, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(
          "getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public Object[]
      getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromBranchIdAndReferenceNumber(
          Date fromDate,
          Date endDate,
          long orgId,
          long sectionId,
          long fromBranchId,
          String referenceNumber) {

    Object[] inventorySectionDataHistory = null;
    try {
      inventorySectionDataHistory =
          (Object[])
              entityManager
                  .createNativeQuery(
                      "select date,sum(amount),from_branch_id,inventory_section_id,reference_number from product.inventory_section_data_history where date between ?1 and ?2 and  orgid = ?3 and inventory_section_id = ?4 and from_branch_id=?5  and reference_number = ?6 group by reference_number,inventory_section_id, from_branch_id, date")
                  .setParameter(1, fromDate)
                  .setParameter(2, endDate)
                  .setParameter(3, orgId)
                  .setParameter(4, sectionId)
                  .setParameter(5, fromBranchId)
                  .setParameter(6, referenceNumber)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(
          "getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromBranchIdAndReferenceNumber ",
          ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @Override
  public Object[]
      getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromSectionIdAndReferenceNumber(
          Date fromDate,
          Date endDate,
          long orgId,
          long sectionId,
          long fromSectionId,
          String referenceNumber) {

    Object[] inventorySectionDataHistory = null;
    try {
      inventorySectionDataHistory =
          (Object[])
              this.entityManager
                  .createNativeQuery(
                      "select date,sum(amount),from_section_id,inventory_section_id,reference_number from product.inventory_section_data_history where date between ?1 and ?2 and  orgid = ?3 and inventory_section_id = ?4 and from_section_id=?5  and reference_number = ?6 group by reference_number,inventory_section_id, from_section_id, date")
                  .setParameter(1, fromDate)
                  .setParameter(2, endDate)
                  .setParameter(3, orgId)
                  .setParameter(4, sectionId)
                  .setParameter(5, fromSectionId)
                  .setParameter(6, referenceNumber)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(
          "getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromSectionIdAndReferenceNumber ",
          ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByOrgIdFromBranchId(
      Date fromdate, Date todate, long orgId, long branchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          this.entityManager
              .createQuery(
                  "select sum(amount),vendor.id,vendor.vendorName,referenceNumber,fromBranchId.id,fromBranchId.branchName,fromSectionId.id,fromSectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,inventorySectionId.id,inventorySectionId.sectionName from InventorySectionDataHistory i where i.date between :stdate and :enddate  and organisation.id=?1 and branch.id=?2   group by vendor.id,vendor.vendorName,referenceNumber,fromBranchId.id,fromBranchId.branchName,fromSectionId.id,fromSectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,inventorySectionId.id,inventorySectionId.sectionName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByOrgIdFromBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long sectionId, long fromSectionId, long orgId, long branchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          this.entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,fromSectionId.id,fromSectionId.sectionName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName from InventorySectionDataHistory i where i.date between :stdate and :enddate and inventorySectionId.id=?1 and fromSectionId.id=?2  and organisation.id=?3 and branch.id=?4   group by referenceNumber,fromSectionId.id,fromSectionId.sectionName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, sectionId)
              .setParameter(2, fromSectionId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByOrgIdBranchIdSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByOrgIdBranchIdSection(
      Date fromdate, Date todate, long sectionId, long frombranchId, long orgId, long branchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          this.entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,fromBranchId.id,fromBranchId.branchName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName from InventorySectionDataHistory i where i.date between :stdate and :enddate and inventorySectionId.id=?1 and fromBranchId.id=?2  and organisation.id=?3 and branch.id=?4   group by referenceNumber,fromBranchId.id,fromBranchId.branchName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, sectionId)
              .setParameter(2, frombranchId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByOrgIdBranchIdSection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByTransferOrgIdFromBranchId(
      Date fromdate, Date todate, long orgId, long branchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          this.entityManager
              .createQuery(
                  "select sum(amount),vendor.id,vendor.vendorName,referenceNumber,fromBranchId.id,fromBranchId.branchName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,branch.id,branch.branchName from InventorySectionDataHistory i where i.date between :stdate and :enddate  and organisation.id=?1 and branch.id=?2   group by vendor.id,vendor.vendorName,referenceNumber,fromBranchId.id,fromBranchId.branchName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,branch.id,branch.branchName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByTransferOrgIdFromBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByTransferOrgIdBranchIdSection(
      Date fromdate, Date todate, long sectionId, long frombranchId, long orgId, long branchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          this.entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,fromBranchId.id,fromBranchId.branchName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,branch.id,branch.branchName from InventorySectionDataHistory i where i.date between :stdate and :enddate and inventorySectionId.id=?1 and fromBranchId.id=?2  and organisation.id=?3 and branch.id=?4   group by referenceNumber,fromBranchId.id,fromBranchId.branchName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,branch.id,branch.branchName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, sectionId)
              .setParameter(2, frombranchId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByTransferOrgIdBranchIdSection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByTransferOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long sectionId, long fromSectionId, long orgId, long branchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          this.entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,fromSectionId.id,fromSectionId.sectionName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,branch.id,branch.branchName from InventorySectionDataHistory i where i.date between :stdate and :enddate and inventorySectionId.id=?1 and fromSectionId.id=?2  and organisation.id=?3 and branch.id=?4   group by referenceNumber,fromSectionId.id,fromSectionId.sectionName,inventorySectionId.id,inventorySectionId.sectionName,date,createdDate,modifiedDate,organisation.id,organisation.orgName,branch.id,branch.branchName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, sectionId)
              .setParameter(2, fromSectionId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByTransferOrgIdBranchIdSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseConsolidatedOrgIdBranchIdAndSectionId(
          long orgId, long branchId, Date fromdate, Date todate, long fromSectionId) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and fromSectionId.id=?3 and vendor.id IS NULL")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, fromSectionId)
              .getResultList();
      System.out.println("inventorySectionDataHistory size" + inventorySectionDataHistory.size());
    } catch (final Exception ex) {
      logger.error(
          "getInventorySectionDataHistoryByItemWiseConsolidatedOrgIdBranchIdAndSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseOrgIdBranchIdAndFromSectionId(
          long orgId, Date fromdate, Date todate, long fromBranchId) {

    List<InventorySectionDataHistory> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and fromBranchId.id=?2 and vendor.id IS NULL")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, fromBranchId)
              .getResultList();
      System.out.println("inventorySectionDataHistory size" + inventorySectionDataHistory.size());
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByItemWiseOrgIdBranchIdAndFromSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryQuantityByProductAndFromToSection(
      long orgId, long branchId, Date fromdate, Date todate, long fromsectionId, long toSectionId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(quantity),productId.productName,date,fromSectionId.sectionName,branch.branchName,inventorySectionId.sectionName from InventorySectionDataHistory where fromSectionId.id=?1 and branch.id=?2 and inventorySectionId.id=?3 and organisation.id=?4 and date between :stDate and :endDate group by productId.productName,date,fromSectionId.sectionName,branch.branchName,inventorySectionId.sectionName")
              .setParameter(1, fromsectionId)
              .setParameter(2, branchId)
              .setParameter(3, toSectionId)
              .setParameter(4, orgId)
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryQuantityByProductAndFromToSection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryQuantityByProductAndFromBranchToSection(
      long orgId, long branchId, Date fromdate, Date todate, long fromBranchId, long toSectionId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(quantity),productId.productName,date,fromBranchId.branchName,branch.branchName,inventorySectionId.sectionName from InventorySectionDataHistory where fromBranchId.id=?1 and branch.id=?2 and inventorySectionId.id=?3 and organisation.id=?4 and date between :stDate and :endDate group by productId.productName,date,fromBranchId.branchName,branch.branchName,inventorySectionId.sectionName")
              .setParameter(1, fromBranchId)
              .setParameter(2, branchId)
              .setParameter(3, toSectionId)
              .setParameter(4, orgId)
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryQuantityByProductAndFromBranchToSection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromBranchId(
          Date fromDate, Date endDate, long orgId, long sectionId, long fromBranchId) {

    List<InventorySectionDataHistory> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and inventorySectionId.id=?2 and fromBranchId.id=?3")
              .setParameter(Constants.STDATE, fromDate)
              .setParameter(Constants.ENDDATE, endDate)
              .setParameter(1, orgId)
              .setParameter(2, sectionId)
              .setParameter(3, fromBranchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(
          "getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory>
      getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromSectionId(
          Date fromDate, Date endDate, long orgId, long sectionId, long fromSectionId) {

    List<InventorySectionDataHistory> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InventorySectionDataHistory i where i.date between :stdate and :enddate and organisation.id=?1 and inventorySectionId.id=?2 and fromSectionId.id=?3")
              .setParameter(Constants.STDATE, fromDate)
              .setParameter(Constants.ENDDATE, endDate)
              .setParameter(1, orgId)
              .setParameter(2, sectionId)
              .setParameter(3, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(
          "getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionDataHistory> getInventorySectionDataHistoryByOrgIdAndReferenceNumber(
      long orgId, String referenceNumber) {

    List<InventorySectionDataHistory> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InventorySectionDataHistory where organisation.id=?1 and referenceNumber = ?2")
              .setParameter(1, orgId)
              .setParameter(2, referenceNumber)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByOrgIdAndReferenceNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByBranch(
      long orgId, long branchId, Date fromdate, Date todate, long fromBranchId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,fromBranchId.branchName,inventorySectionId.sectionName from InventorySectionDataHistory  i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and fromBranchId.id=?3 group by referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,fromBranchId.branchName,inventorySectionId.sectionName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, fromBranchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByBranchReference(
      long orgId, long branchId, Date fromdate, Date todate, long fromBranchId, String reference) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,fromBranchId.branchName,inventorySectionId.sectionName from InventorySectionDataHistory where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and fromBranchId.id=?3  and referenceNumber=?4 group by referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,fromBranchId.branchName,inventorySectionId.sectionName")
              .setParameter(Constants.STDATE, fromdate)
              .setParameter(Constants.ENDDATE, todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, fromBranchId)
              .setParameter(4, reference)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByBranchReference ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByTotalAmountBranch(
      long orgId, long branchId, Date fromdate, Date todate) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createNativeQuery(
                  "select sum(amount),vendor_id,reference_number,from_section_id,date,created_date,modified_date,orgid,branchid from product.inventory_section_data_history i where i.date>=?1 and date<=?2  and orgid=?3  and branchid=?4 group by vendor_id,reference_number,from_section_id,date,created_date,modified_date,orgid,branchid")
              .setParameter(1, fromdate)
              .setParameter(2, todate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByTotalAmountBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryTotalAmtToSection(
      Date formDate, Date endDate, long branchId, long orgId, long sectionId, long fromBranchId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromBranchId.branchName from InventorySectionDataHistory i where i.date between :stdate and :enddate  and branch.id=?1 and organisation.id=?2 and fromBranchId.id=?3 and inventorySectionId.id=?4 group by referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromBranchId.branchName")
              .setParameter(Constants.STDATE, formDate)
              .setParameter(Constants.ENDDATE, endDate)
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .setParameter(3, fromBranchId)
              .setParameter(4, sectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryTotalAmtToSection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInventorySectionDataHistoryByBillIdAndDatesToSectionReference(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long fromBranchId,
      long toSectionId) {

    List<Object[]> inventorySectionDataHistory = new ArrayList<>();
    try {
      inventorySectionDataHistory =
          entityManager
              .createQuery(
                  "select sum(amount),referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromBranchId.branchName from InventorySectionDataHistory i where i.date between :stdate and :enddate  and referenceNumber=?1 and branch.id=?2 and organisation.id=?3 and fromBranchId.id=?4 and inventorySectionId.id=?5 group by referenceNumber,date,createdDate,modifiedDate,organisation.orgName,branch.branchName,inventorySectionId.sectionName,fromBranchId.branchName")
              .setParameter(Constants.STDATE, formDate)
              .setParameter(Constants.ENDDATE, endDate)
              .setParameter(1, referenceNumber)
              .setParameter(2, branchId)
              .setParameter(3, orgId)
              .setParameter(4, fromBranchId)
              .setParameter(5, toSectionId)
              .getResultList();
      System.out.println(inventorySectionDataHistory.size());

    } catch (final Exception ex) {
      logger.error("getInventorySectionDataHistoryByBillIdAndDatesToSectionReference ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataHistory;
  }
}
