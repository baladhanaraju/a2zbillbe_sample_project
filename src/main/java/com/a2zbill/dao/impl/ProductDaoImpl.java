package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductDao;
import com.a2zbill.domain.Product;
import com.a2zbill.domain.ProductVariation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("unchecked")
public class ProductDaoImpl implements ProductDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final Product products) {
    try {
      entityManager.persist(products);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(final Product products) {
    try {
      entityManager.merge(products);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public Product getProductsById(final Long id) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getAllProducts() {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails = entityManager.createQuery("from Product order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProducts ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  public Product getProductsByProductCode(final String productCode) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where code =?1")
                  .setParameter(1, productCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductName(final String productName) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where productName =?1")
                  .setParameter(1, productName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getAllProductsName() {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager.createQuery("select productName from Product ORDER BY id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductsName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public long getProductsCount() {

    Query query = null;
    try {
      query = (Query) entityManager.createQuery("SELECT count(*) FROM Product");
    } catch (final Exception ex) {
      logger.error("getProductsCount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    if (query != null) {
      return query.getResultList().size();
    }
    return 0;
  }

  @Override
  public List<Product> getAllProductsByName(@Param("productName") final String productName) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery("from  Product p where p.productName LIKE CONCAT('%',:productName,'%')")
              .setParameter("productName", productName)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductsByName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductsByProductCategoryId(final long productCategoryId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery("FROM Product where productCategory.id =?1 order by id")
              .setParameter(1, productCategoryId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByHsnCodeAndProductCodeAndProductName(
      final String hsnCode, final String productCode, final String productName) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where hsnNumber =?1 and code =?2 and productName=?3")
                  .setParameter(1, hsnCode)
                  .setParameter(2, productCode)
                  .setParameter(3, productName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByHsnCodeAndProductCodeAndProductName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductFeaturesById(final Long id) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductFeaturesById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return productsDetails;
  }

  @Override
  public Product getProductsByHsnCodeAndProductCode(
      final String hsnCode, final String productCode) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where hsnNumber =?1 and code =?2")
                  .setParameter(1, hsnCode)
                  .setParameter(2, productCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByHsnCodeAndProductCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByOrgId(final Long orgId) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where orgId =?1 ")
                  .setParameter(1, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductCodeAndOrgId(final String productCode, final Long orgId) {
    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where code=?1 and organisation.id =?2 ")
                  .setParameter(1, productCode)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCodeAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductCodeAndOrgIdAndBranchId(
      final String productCode, final Long orgId, final Long branchId) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery(
                      "FROM Product where code=?1 and organisation.id =?2 and  (branch.id=?3 or branch.id is null)")
                  .setParameter(1, productCode)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCodeAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByHsnCodeAndProductName(
      final String hsnCode, final String productName) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where hsnNumber =?1  and productName=?2")
                  .setParameter(1, hsnCode)
                  .setParameter(2, productName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByHsnCodeAndProductName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getAllProducts(final long orgId, final long branchId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "from Product where organisation.id =?1  and (branch.id=?2 or branch.id is null)")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProducts ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public long getProductsCount(final Long orgId) {

    Long count = null;
    try {
      count =
          (Long)
              entityManager
                  .createQuery("SELECT count(*) FROM Product where organisation.id=?1")
                  .setParameter(1, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsCount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return count;
  }

  @Override
  public long getProductsCounByOrgIdAndBranchId(final Long brnachId, final Long orgId) {

    Long count = null;
    try {
      count =
          (Long)
              entityManager
                  .createQuery(
                      "SELECT count(*) FROM Product where (branch.id=?1 or branch.id is null) and organisation.id=?2")
                  .setParameter(1, brnachId)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsCounByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return count;
  }

  @Override
  public Product getProductsByProductNameAndOrgIdAndBranchId(
      final String productName, final long orgId, final long branchId) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery(
                      "FROM Product where productName =?1 and organisation.id=?2 and (branch.id=?3 or branch.id is null)")
                  .setParameter(1, productName)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getProductsByProductNameAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductNameAndOrgId(final String productName, final long orgId) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where productName =?1 and organisation.id=?2 ")
                  .setParameter(1, productName)
                  .setParameter(2, orgId)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getProductsByProductNameAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductIdAndOrgIdAndBranchId(
      final long productId, final long orgId, final long branchId) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery(
                      "FROM Product where id =?1 and organisation.id=?2 and (branch.id=?3 or branch.id is null)")
                  .setParameter(1, productId)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductIdAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByHsnCodeAndProductNameByBranchIdAndOrgId(
      final String hsnCode, final String productName, final long branchId, final long orgId) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery(
                      "FROM Product where hsnNumber =?1  and productName=?2 and (branch.id=?3 or branch.id is null) and organisation.id=?4")
                  .setParameter(1, hsnCode)
                  .setParameter(2, productName)
                  .setParameter(3, branchId)
                  .setParameter(4, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByHsnCodeAndProductNameByBranchIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getAllProductsByOrgId(final long orgId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery("from Product where organisation.id =?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductIdAndOrgId(final long productId, final long orgId) {

    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery("FROM Product where id =?1 and organisation.id=?2")
                  .setParameter(1, productId)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getAllProductsByHsnCode() {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager.createQuery("FROM Product where hsnNumber IS NOT NULL").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductsByHsnCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductsByProductCategoryName(final String productCategoryName) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery("FROM Product where productCategory.categoryType.categoryName =?1")
              .setParameter(1, productCategoryName)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryName ", ex);
    } finally {

    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductsByProductCategoryNameIfRedeemFlagIsFalse(
      final String productCategoryName) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 and productCategory.redeemFlag = false order by status")
              .setParameter(1, productCategoryName)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryNameIfRedeemFlagIsFalse ", ex);
    } finally {

    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductsBySizeVariationIdAndProductName(
      final long orgId, final long sizeId, final String productName) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "from Product where organisation.id =?1 and sizeVariations.id = ?2 and productName = ?3")
              .setParameter(1, orgId)
              .setParameter(2, sizeId)
              .setParameter(3, productName)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsBySizeVariationIdAndProductName ", ex);
    } finally {

    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductNameAndOrgIdAndBranchIdAndSizeId(
      final String producName, final long orgId, final long branchId, final Long sizeVariationId) {
    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery(
                      "FROM Product where productName=?1 and organisation.id=?2 and branch.id=?3 and sizeVariations.id=?4")
                  .setParameter(1, producName)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .setParameter(4, sizeVariationId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductNameAndOrgIdAndBranchIdAndSizeId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<ProductVariation> getAllProductsAndVariationDetails(
      final long orgId, final long branchId) {
    List<ProductVariation> products = new ArrayList<>();
    try {
      products =
          entityManager
              .createQuery(
                  "from Product product left join ProductVariation productVariation on product.id=productVariation.product.id left join VariationOptions variationOptions on variationOptions.id=productVariation.variationOption.id left join VariationTypes variationTypes on variationTypes.id=productVariation.variationType.id left join QuantityMeasurements quantityMeasurements on quantityMeasurements.id=variationOptions.quantityMeasurement.id where product.organisation.id=?1 and product.branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductsAndVariationDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return products;
  }

  @Override
  public List<ProductVariation> getAllProductsAndVariationDetailsByOrg(final long orgId) {
    List<ProductVariation> products = new ArrayList<>();
    try {
      products =
          (List<ProductVariation>)
              entityManager
                  .createNativeQuery(
                      "select * from product.products left join product.product_variation on product.products.id=product.product_variation.product_id left join product.variation_options on product.variation_options.id=product.product_variation.variation_options_id left join product.variation_types on product.variation_types.id=product.product_variation.variation_type_id left join product.quantity_measurements on product.quantity_measurements.id=product.variation_options.quantity_measurement_id where product.products.org_id=?1")
                  .setParameter(1, orgId)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductsAndVariationDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return products;
  }

  @Override
  public List<Object[]> getAllProductByParentId(final long orgId, final long branchId) {
    List<Object[]> products = new ArrayList<>();
    try {
      products =
          entityManager
              .createQuery(
                  "select id, productName, hsnNumber, productPrice, productPackaging, code, productCategory.productTypeName, variationType.displayName, measurement, CGST, SGST, IGST, image from Product where organisation.id=?1 and branch.id=?2 and parentId=?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, 0l)
              .getResultList();

    } catch (final Exception ex) {
      logger.error("getAllProductByParentId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return products;
  }

  @Override
  public List<Object[]> getAllProductByOrgAndParentId(final long orgId) {
    List<Object[]> products = new ArrayList<>();
    try {
      products =
          entityManager
              .createQuery(
                  "select id, productName, hsnNumber, productPrice, productPackaging, code, productCategory.productTypeName, variationType.displayName, measurement, CGST, SGST, IGST, image from Product where organisation.id=?1 and parentId=?2")
              .setParameter(1, orgId)
              .setParameter(2, 0l)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductByOrgAndParentId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return products;
  }

  @Override
  public List<Object[]> getProductByParentId(
      final long orgId, final long branchId, final long parentId) {

    List<Object[]> products = new ArrayList<>();
    try {
      products =
          entityManager
              .createQuery(
                  "select id, productName, hsnNumber, productPrice, productPackaging, code, productCategory.productTypeName, variationType.displayName, measurement, CGST, SGST, IGST, image from Product where organisation.id=?1 and branch.id=?2 and parentId=?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, parentId)
              .getResultList();

    } catch (final Exception ex) {
      logger.error("getProductByParentId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return products;
  }

  @Override
  public List<Object[]> getProductByParentIdAndOrgId(final long orgId, final long parentId) {

    List<Object[]> products = new ArrayList<Object[]>();
    try {
      products =
          entityManager
              .createQuery(
                  "select id, productName, hsnNumber, productPrice, productPackaging, code, productCategory.productTypeName, variationType.displayName, measurement, CGST, SGST, IGST, image from Product where organisation.id=?1 and parentId=?2")
              .setParameter(1, orgId)
              .setParameter(2, parentId)
              .getResultList();

    } catch (final Exception ex) {
      logger.error("getProductByParentIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return products;
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgId(
      final String productCategoryName, final long orgId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2  order by status ")
              .setParameter(1, productCategoryName)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryNameAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchId(
      final String productCategoryName, final long orgId, final long branchId) {

    List<Product> productsDetails = new ArrayList<>();

    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2 and branch.id = ?3 order by status")
              .setParameter(1, productCategoryName)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryNameAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductNameAndOrgAndBranch(
      final String productName, final long orgId, final long branchId) {
    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery(
                      "FROM Product where productName =?1 and organisation.id=?2 and branch.id=?3")
                  .setParameter(1, productName)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getProductsByProductNameAndOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Product getProductsByProductCodeAndProductNameAndOrgAndBranch(
      final String productCode, final String productName, final long orgId, final long branchId) {
    Product productsDetails = null;
    try {
      productsDetails =
          (Product)
              entityManager
                  .createQuery(
                      "FROM Product where code =?1 and productName=?2 and organisation.id=?3 and branch.id=?4")
                  .setParameter(1, productCode)
                  .setParameter(2, productName)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCodeAndProductNameAndOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatus(final long orgId, final long branchId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "From Product where organisation.id=?1 and branch.id=?2 order by status desc")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductByorgIdandBranchIdstatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatus(final long orgId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery("From Product where organisation.id=?1 order by status desc")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductByorgIdandBranchIdstatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatus(
      final String productCategoryName, final long orgId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2 and status= true")
              .setParameter(1, productCategoryName)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductByorgIdandBranchIdstatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getMenuProductDetailsOrgIdandStatus(
      final String productCategoryName, final long orgId, final long branchId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2 and status= true")
              .setParameter(1, productCategoryName)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getMenuProductDetailsOrgIdandStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getMenuProductDetailsOrgAndBranchAndCategory(
      final String productCategoryName,
      final String categoryName,
      final long orgId,
      final long branchId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where ( productCategory.categoryType.categoryName =?1 or productCategory.categoryType.categoryName =?2 ) and organisation.id = ?3 and branch.id = ?4")
              .setParameter(1, productCategoryName)
              .setParameter(2, categoryName)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getMenuProductDetailsOrgAndBranchAndCategory ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getMenuProductDetailsOrgAndCategory(
      final String productCategoryName, final String categoryName, final long orgId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 or productCategory.categoryType.categoryName =?2 and organisation.id = ?3")
              .setParameter(1, productCategoryName)
              .setParameter(2, categoryName)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getMenuProductDetailsOrgAndCategory ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Map<String, Object>> getProductPackagingDetails() {

    List<Map<String, Object>> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager.createQuery(" select productPackaging FROM Product ").getResultList();
    } catch (final Exception ex) {
      logger.error("getProductPackagingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Map<String, Object> getProductPackagingDetailsByProductId(final long productId) {

    Map<String, Object> productsDetails = null;
    try {
      productsDetails =
          (Map<String, Object>)
              entityManager
                  .createQuery(" select productPackaging FROM Product where id=?1 ")
                  .setParameter(1, productId)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getProductPackagingDetailsByProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductById(final long productId) {

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery("FROM Product where id=?1")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductByorgIdandBranchIdstatusActive(
      final long orgId, final long branchId) {

    List<Product> productDetails = new ArrayList<>();
    try {
      productDetails =
          entityManager
              .createQuery(
                  " FROM Product where organisation.id = ?1 and branch.id=?2 and status=True")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductByorgIdandBranchIdstatusActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productDetails;
  }

  @Override
  public List<Product> getProductByorgIdandstatusActive(final long orgId) {
    List<Product> productDetails = new ArrayList<>();
    try {
      productDetails =
          entityManager
              .createQuery(" FROM Product where organisation.id = ?1 and status=True")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductByorgIdandstatusActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return productDetails;
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgIdpagination(
      final String productCategoryName,
      final long orgId,
      final int pageNum,
      final int numOfRecords) {

    final int firstResult = (pageNum - 1) * numOfRecords;

    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2  order by id,status DESC")
              .setFirstResult(firstResult)
              .setMaxResults(numOfRecords)
              .setParameter(1, productCategoryName)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryNameAndOrgIdpagination ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Product> getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination(
      final String productCategoryName,
      final long orgId,
      final long branchId,
      final int pageNum,
      final int numOfRecords) {

    final int firstResult = (pageNum - 1) * numOfRecords;
    List<Product> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager
              .createQuery(
                  "FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2 and branch.id = ?3 order by id,status DESC ")
              .setFirstResult(firstResult)
              .setMaxResults(numOfRecords)
              .setParameter(1, productCategoryName)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryNameAndOrgIdAndBranchIdpagination ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Long getProductsCountByProductCategoryNameAndOrgId(
      final String productCategoryName, final long orgId) {

    Long productsDetails = null;
    try {
      productsDetails =
          (Long)
              entityManager
                  .createQuery(
                      " Select count(*) FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2 ")
                  .setParameter(1, productCategoryName)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsCountByProductCategoryNameAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public long getProductsCountByProductCategoryNameAndOrgIdAndBranchId(
      final String productCategoryName, final long orgId, final long branchId) {
    Long productsDetails = null;
    try {
      productsDetails =
          (Long)
              entityManager
                  .createQuery(
                      " Select count(*) FROM Product where productCategory.categoryType.categoryName =?1 and organisation.id = ?2 and branch.id = ?3 ")
                  .setParameter(1, productCategoryName)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductsCountByProductCategoryNameAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public List<Object[]> getProductsByProductCategoryNameMap(final String productCategoryName) {
    try {
      return entityManager
          .createQuery(
              "select id, productName, productPrice, code, productCount, packageFlag, includingTaxFlag, SGST, CGST, variationType.displayName FROM Product where productCategory.categoryType.categoryName =?1")
          .setParameter(1, productCategoryName)
          .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return new ArrayList<>();
  }

  @Override
  public List<Object[]> getProductNameAndIdsByProductCategoryId(final long productCategoryId) {
    try {
      return entityManager
          .createQuery(
              "select id, productName FROM Product where productCategory.id =?1 order by id")
          .setParameter(1, productCategoryId)
          .getResultList();

    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return new ArrayList<>();
  }

  @Override
  public List<Object[]> getProductsByOrgPath(final String pathUrl, final long productCategoryId) {
    List<Object[]> products = new ArrayList<Object[]>();
    try {
      products =
          entityManager
              .createQuery(
                  "select id, productName, productPrice, productFeatures, image, variationType.displayName,variationType.quantityMeasurement.displayName,measurement,hsnNumber,code,CGST,SGST  FROM Product where organisation.pathUrl=?1 and productCategory.id=?2")
              .setParameter(1, pathUrl)
              .setParameter(2, productCategoryId)
              .getResultList();

    } catch (final Exception ex) {
      logger.error("getProductsByProductCategoryName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return products;
  }

  @Override
  public List<Object[]> getProductsByCategoryId(long categoryId) {
    List<Object[]> products = new ArrayList<Object[]>();
    try {
      products =
          entityManager
              .createQuery(
                  "select id, productName, productPrice, productFeatures, image, variationType.displayName,variationType.quantityMeasurement.displayName,measurement,code,hsnNumber,SGST,CGST  FROM Product where productCategory.id=?1")
              .setParameter(1, categoryId)
              .getResultList();

    } catch (final Exception ex) {
      logger.error("getProductsByCategoryId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return products;
  }

  @Override
  public String getproductNamesByProductId(final long productId) {
    String productName = null;
    try {
      productName =
          (String)
              entityManager
                  .createQuery("select productName from Product where id=?1")
                  .setParameter(1, productId)
                  .getSingleResult();
    } catch (final Exception e) {
      logger.error("getproductNamesByProductId ", e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productName;
  }

  @Override
  public List<Product> getAllproductsByElastic() {
    List<Product> productDetails = new ArrayList<>();
    try {
      productDetails = entityManager.createQuery(" FROM Product where id>1000").getResultList();
    } catch (final Exception ex) {
      logger.error("getProductByorgIdandstatusActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return productDetails;
  }
}
