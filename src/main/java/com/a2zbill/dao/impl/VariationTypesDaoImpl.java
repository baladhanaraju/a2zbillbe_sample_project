package com.a2zbill.dao.impl;

import com.a2zbill.dao.VariationTypesDao;
import com.a2zbill.domain.VariationTypes;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VariationTypesDaoImpl implements VariationTypesDao {

  private static final Logger logger = LoggerFactory.getLogger(VariationTypesDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public List<VariationTypes> getAllVariationTypes() {

    List<VariationTypes> variationTypesDetails = new ArrayList<>();
    try {
      variationTypesDetails =
          entityManager.createQuery("FROM VariationTypes order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllVariationTypes ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationTypesDetails;
  }

  @Override
  public VariationTypes getVariationTypesById(long id) {

    VariationTypes variationType = null;
    try {
      variationType =
          (VariationTypes)
              entityManager
                  .createQuery("From VariationTypes c where c.id = ?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getVariationTypesById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationType;
  }

  @Override
  public VariationTypes getVariationTypesByName(String name) {

    VariationTypes variationType = null;
    try {
      variationType =
          (VariationTypes)
              entityManager
                  .createQuery("From VariationTypes c where c.variationName = ?1")
                  .setParameter(1, name)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getVariationTypesByName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationType;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<VariationTypes> getOptionsByVariationType(long variationTypeId) {

    List<VariationTypes> variationTypesDetails = new ArrayList<>();
    try {
      variationTypesDetails =
          entityManager
              .createQuery("from VariationTypes where id=?1")
              .setParameter(1, variationTypeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOptionsByVariationType ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationTypesDetails;
  }
}
