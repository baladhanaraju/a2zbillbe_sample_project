package com.a2zbill.dao.impl;

import com.a2zbill.dao.RetailerDao;
import com.a2zbill.domain.Retailer;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RetailerDaoImpl implements RetailerDao {

  private static final Logger logger = LoggerFactory.getLogger(RetailerDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(Retailer retailer) {
    try {
      entityManager.persist(retailer);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Retailer> getRetailerDetails() {

    List<Retailer> retailerDetails = new ArrayList<>();
    try {
      retailerDetails = entityManager.createQuery("from Retailer order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getRetailerDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return retailerDetails;
  }

  @Override
  public Retailer getRetailerDetailsByGSTNumber(final String retailerGstNumber) {

    Retailer retailerDetails = null;
    try {
      retailerDetails =
          (Retailer)
              entityManager
                  .createQuery("from Retailer where retailerGstNumber = ?1")
                  .setParameter(1, retailerGstNumber)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRetailerDetailsByGSTNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return retailerDetails;
  }
}
