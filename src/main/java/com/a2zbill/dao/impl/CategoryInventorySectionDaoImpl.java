package com.a2zbill.dao.impl;

import com.a2zbill.dao.CategoryInventorySectionDao;
import com.a2zbill.domain.CategoryInventorySection;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryInventorySectionDaoImpl implements CategoryInventorySectionDao {

  private static final Logger logger =
      LoggerFactory.getLogger(CategoryInventorySectionDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final CategoryInventorySection categoryInventorySection) {
    try {
      entityManager.persist(categoryInventorySection);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.close();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(final CategoryInventorySection categoryInventorySection) {
    try {
      entityManager.merge(categoryInventorySection);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CategoryInventorySection> getAllCategoryInventorySection() {

    List<CategoryInventorySection> categoryInventorySection = new ArrayList<>();
    try {
      categoryInventorySection =
          this.entityManager.createQuery("From CategoryInventorySection").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCategoryInventorySection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return categoryInventorySection;
  }

  @Override
  public CategoryInventorySection getCategoryInventorySectionByCategoryIdAndInventorySectionId(
      final long categoryId, final long inventorySectionId) {

    CategoryInventorySection categoryInventorySection = null;
    try {
      categoryInventorySection =
          (CategoryInventorySection)
              this.entityManager
                  .createQuery(
                      "From CategoryInventorySection where categoryId.id = ?1 and inventorySection.id = ?2")
                  .setParameter(1, categoryId)
                  .setParameter(2, inventorySectionId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllCategoryInventorySection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return categoryInventorySection;
  }
}
