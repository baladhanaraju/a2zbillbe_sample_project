package com.a2zbill.dao.impl;

import com.a2zbill.dao.DayCloseDao;
import com.a2zbill.domain.DayClose;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DayCloseDaoImpl implements DayCloseDao {

  private static final Logger logger = LoggerFactory.getLogger(DayCloseDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(DayClose DayClose) {
    try {
      entityManager.persist(DayClose);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean update(DayClose DayClose) {
    try {
      entityManager.merge(DayClose);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<DayClose> getAllDayCloseDatails() {

    List<DayClose> dayClose = new ArrayList<>();
    try {
      dayClose = entityManager.createQuery("from DayClose order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllDayCloseDatails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return dayClose;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<DayClose> getDayCloseDatails(Date fromdate, Date toDate, long branchId) {

    List<DayClose> dayClose = new ArrayList<>();
    try {
      dayClose =
          entityManager
              .createQuery(
                  "from DayClose d where d.date between :stdate and :enddate and branchId.id=?1")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getDayCloseDatails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return dayClose;
  }
}
