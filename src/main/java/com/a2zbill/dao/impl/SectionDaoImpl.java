package com.a2zbill.dao.impl;

import com.a2zbill.dao.SectionDao;
import com.a2zbill.domain.Section;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SectionDaoImpl implements SectionDao {

  private static final Logger logger = LoggerFactory.getLogger(SectionDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(Section section) {
    try {
      entityManager.persist(section);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      Object result = entityManager.find(Section.class, id);
      if (result != null) {
        entityManager.remove(result);
        return true;
      }
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(Section section) {
    try {
      entityManager.merge(section);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Section> getAllSectionDetails() {

    List<Section> sectionDetails = new ArrayList<>();
    try {
      sectionDetails = entityManager.createQuery("FROM Section order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSectionDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return sectionDetails;
  }

  @SuppressWarnings("unchecked")
  public List<Section> getSectionDetailsByOrgIdAndBranchId(final long orgId, final long branchId) {

    List<Section> section = new ArrayList<>();
    try {
      section =
          entityManager
              .createQuery("from Section where organisation.id=?1 and branch.id=?2 and status=?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, true)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSectionDetailsByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return section;
  }

  @Override
  public Section getSectionDetailsBySectionId(final long sectionId) {

    Section sectionDetails = null;
    try {
      sectionDetails =
          (Section)
              entityManager
                  .createQuery("FROM Section c where c.id =?1")
                  .setParameter(1, sectionId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getSectionDetailsBySectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return sectionDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Section> getSectionDetailsByOrgId(final long orgId) {

    List<Section> section = new ArrayList<>();
    try {
      section =
          entityManager
              .createQuery("from Section where organisation.id=?1 and status=?2")
              .setParameter(1, orgId)
              .setParameter(2, true)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSectionDetailsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return section;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Section> getSectionDetailsByOrgAndStatus(long orgId) {

    List<Section> section = new ArrayList<>();
    try {
      section =
          entityManager
              .createQuery(" from Section where organisation.id=?1 order by status")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSectionDetailsByOrgAndStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return section;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Section> getSectionDetailsByOrgIdAndBranchAndStatus(long orgId, long branchId) {

    List<Section> section = new ArrayList<>();
    try {
      section =
          entityManager
              .createQuery(
                  " from Section where organisation.id=?1 and branch.id=?2 order by status")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSectionDetailsByOrgIdAndBranchAndStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return section;
  }

  @Override
  public Section getSectiondetails(String sectionName) {

    Section SectionDetails = null;
    try {
      SectionDetails =
          (Section)
              this.entityManager
                  .createQuery("from Section where sectionName=?1")
                  .setParameter(1, sectionName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getSectiondetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return SectionDetails;
  }

  @Override
  public Section getSectiondetailsdisplayname(String displayName) {

    Section SectionDetails = null;
    try {
      SectionDetails =
          (Section)
              this.entityManager
                  .createQuery("from Section where displayName=?1")
                  .setParameter(1, displayName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getSectiondetailsdisplayname ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return SectionDetails;
  }
}
