package com.a2zbill.dao.impl;

import com.a2zbill.dao.OrderLineItemsDao;
import com.a2zbill.domain.OrderLineItems;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderLineItemsDaoImpl implements OrderLineItemsDao {

  private static final Logger logger = LoggerFactory.getLogger(OrderLineItemsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final OrderLineItems orderLineItems) {
    try {
      entityManager.persist(orderLineItems);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final OrderLineItems orderLineItems) {
    try {
      entityManager.merge(orderLineItems);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getAllOrderLineItems() {

    List<OrderLineItems> orderLineItems = new ArrayList<>();
    try {
      orderLineItems = entityManager.createQuery("from OrderLineItems").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllOrderLineItems ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getOrderLineItemsByorderId(final long orderId) {

    List<OrderLineItems> orderLineItems = new ArrayList<>();
    try {
      orderLineItems =
          (List<OrderLineItems>)
              entityManager
                  .createQuery("from OrderLineItems where orderId.id=?1")
                  .setParameter(1, orderId)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrderLineItemsByorderId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getOrderLineItemsByCounterIdAndFlowId() {

    List<OrderLineItems> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createQuery(
                  "select productName, counterDetails.id, quantity, hsnCode from OrderLineItems where orderId.orderStatus like 'Open' and "
                      + "orderId.id = (select orderId.id from OrderFlow where flow.id = 1) order by time")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrderLineItemsByCounterIdAndFlowId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public OrderLineItems getOrderLineItemsByProductNameAndHsnCode(
      Long counterId, String productName, String hsnCode) {

    OrderLineItems orderLineItems = null;
    try {
      orderLineItems =
          (OrderLineItems)
              entityManager
                  .createQuery(
                      " from OrderLineItems where  orderId.orderStatus like 'Open' and counterDetails.id = ?1 and productName=?2 and hsnCode = ?3")
                  .setParameter(1, counterId)
                  .setParameter(2, productName)
                  .setParameter(3, hsnCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderLineItemsByProductNameAndHsnCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }

  @Override
  public OrderLineItems getOrderDetailsByCounterIdProductNameAndHsnCode(
      Long counterId, String productName, String hsnCode) {

    OrderLineItems orderLineItems = null;
    try {
      orderLineItems =
          (OrderLineItems)
              entityManager
                  .createQuery(
                      " from OrderLineItems where  cartDetails.cartStatus like 'Open' and counterDetails.id = ?1 and productName = ?2 and hsnCode = ?3 ")
                  .setParameter(1, counterId)
                  .setParameter(2, productName)
                  .setParameter(3, hsnCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderDetailsByCounterIdProductNameAndHsnCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getOrderLineItemsGroupByProductNameifBrnachNull(
      Date fromDate, Date toDate, long orgId) {

    List<OrderLineItems> orderLineItems = new ArrayList<>();
    try {
      orderLineItems =
          entityManager
              .createNativeQuery(
                  "select product_name, sum(quantity),product_price,org_id from orders.order_line_items where date between date = ?1 and date = ?2 group by product_name,product_price,org_id Having org_id = ?3")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrderLineItemsGroupByProductNameifBrnachNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getOrderLineItemsGroupByProductName(
      Date fromDate, Date toDate, long orgId, long branchId) {

    List<OrderLineItems> orderLineItems = new ArrayList<>();
    try {
      orderLineItems =
          entityManager
              .createNativeQuery(
                  "select product_name, sum(quantity),product_price,org_id,branch_id from orders.order_line_items where  date>= ?1 and date<= ?2 group by product_name,product_price,org_id,branch_id Having org_id = ?3 and branch_id = ?4")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrderLineItemsGroupByProductName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getproductQuantityByBranchAndOrgIdAndDate(
      long orgId, long branchId, Date fromDate, Date toDate) {

    List<OrderLineItems> orderLineItems = new ArrayList<>();
    try {
      orderLineItems =
          entityManager
              .createNativeQuery(
                  "SELECT product_name, sum(quantity) FROM orders.order_line_items WHERE org_id = ?1 and branch_id = ?2  and  date>= ?3 and date<= ?4 GROUP BY product_name,org_id,branch_id")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, fromDate)
              .setParameter(4, toDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductQuantityByBranchAndOrgIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getproductQuantityByOrgIdAndDate(
      long orgId, Date fromDate, Date toDate) {

    List<OrderLineItems> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "SELECT product_name, sum(quantity) FROM orders.order_line_items WHERE org_id = ?1 and date>= ?2 and date<= ?3 GROUP BY product_name,org_id,branch_id")
              .setParameter(1, orgId)
              .setParameter(2, fromDate)
              .setParameter(3, toDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductQuantityByOrgIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public OrderLineItems getOrderLineItemsById(long orderLineItemId) {

    OrderLineItems orderLineDetails = null;
    try {
      orderLineDetails =
          (OrderLineItems)
              entityManager
                  .createQuery(" from OrderLineItems where id=?1")
                  .setParameter(1, orderLineItemId)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getOrderLineItemsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineDetails;
  }

  @Override
  public OrderLineItems getOrderLineItemsByIdAndProductName(long orderId, String productName) {

    OrderLineItems orderLineDetails = null;
    try {
      orderLineDetails =
          (OrderLineItems)
              entityManager
                  .createQuery(" from OrderLineItems where orderId.id=?1 and productName=?2")
                  .setParameter(1, orderId)
                  .setParameter(2, productName)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getOrderLineItemsByIdAndProductName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderLineItems> getOrderLineItemsByProductName(
      String productName, long orgId, long branchId) {

    List<OrderLineItems> orderLineItems = new ArrayList<>();

    try {
      orderLineItems =
          this.entityManager
              .createQuery(
                  "from OrderLineItems where  productName=?1 and organisation.id=?2 and branchId.id=?3 ")
              .setParameter(1, productName)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductQuantityByOrgIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderLineItems;
  }
}
