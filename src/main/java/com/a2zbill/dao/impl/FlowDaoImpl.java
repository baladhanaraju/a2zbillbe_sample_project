package com.a2zbill.dao.impl;

import com.a2zbill.dao.FlowDao;
import com.a2zbill.domain.Flow;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FlowDaoImpl implements FlowDao {

  private static final Logger logger = LoggerFactory.getLogger(FlowDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final Flow flow) {
    try {
      entityManager.persist(flow);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(final Flow flow) {
    try {
      entityManager.merge(flow);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public Flow getFlowById(final Long id) {

    Flow flow = null;
    try {
      flow =
          (Flow)
              this.entityManager
                  .createQuery("FROM Flow where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFlowById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return flow;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Flow> getAllFlowDetails() {

    List<Flow> flow = new ArrayList<>();
    try {
      flow = this.entityManager.createQuery("from Flow order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllFlowDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return flow;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Flow> getFlowByOrgId(final Long orgId) {

    List<Flow> flow = new ArrayList<>();
    try {
      flow =
          entityManager
              .createQuery(
                  "FROM Flow where organisation.id =?1 and flowType.typeName like'order flow'")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getFlowByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return flow;
  }

  @Override
  public Flow getFlowByFlowTypeId(final Long flowTypeId, final Long flowRanking) {

    Flow flow = null;
    try {
      flow =
          (Flow)
              entityManager
                  .createQuery("FROM Flow where flowType.id =?1 and flowRanking=?2")
                  .setParameter(1, flowTypeId)
                  .setParameter(2, flowRanking)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFlowByFlowTypeId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return flow;
  }

  @Override
  public Flow getFlowByRaking(final Long flowRanking) {

    Flow flow = null;
    try {
      flow =
          (Flow)
              entityManager
                  .createQuery("FROM Flow where  flowRanking=?1")
                  .setParameter(1, flowRanking)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFlowByRaking ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return flow;
  }
}
