package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductConsumptionDao;
import com.a2zbill.domain.ProductConsumption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductConsumptionDaoImpl implements ProductConsumptionDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductConsumptionDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final ProductConsumption productConsumption) {
    try {
      entityManager.persist(productConsumption);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final ProductConsumption productComposition) {
    try {
      entityManager.merge(productComposition);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductConsumption> getAllProductConsumption() {

    List<ProductConsumption> productConsumption = new ArrayList<>();
    try {
      productConsumption =
          entityManager.createQuery("from ProductConsumption order by id").getResultList();

    } catch (final Exception ex) {
      logger.error("getAllProductConsumption ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productConsumption;
  }

  @Override
  public ProductConsumption getProductConsumptionById(final long id) {

    ProductConsumption productConsumption = null;
    try {
      productConsumption =
          (ProductConsumption)
              entityManager
                  .createQuery("from ProductConsumption where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductConsumptionById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productConsumption;
  }

  @Override
  public ProductConsumption getProductConsumptionByproductAndcurrentDate(
      final long productId, final Date date) {

    ProductConsumption productConsumption = null;
    try {
      productConsumption =
          (ProductConsumption)
              entityManager
                  .createQuery("from ProductConsumption where product.id=?1 and date=?2")
                  .setParameter(1, productId)
                  .setParameter(2, date)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductConsumptionByproductAndcurrentDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productConsumption;
  }

  @Override
  public ProductConsumption getProductConsumptionByproductId(final long productId) {

    ProductConsumption productConsumption = null;
    try {
      productConsumption =
          (ProductConsumption)
              entityManager
                  .createQuery("from ProductConsumption where product.id=?1")
                  .setParameter(1, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductConsumptionByproductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productConsumption;
  }

  @Override
  public ProductConsumption
      getProductConsumptionByproductAndmenuProductAndwastageFlagAnddateAndOrgAndBranch(
          final long productId,
          final long menuProductId,
          final boolean status,
          final Date date,
          final long orgId,
          final long branchId) {

    ProductConsumption productConsumption = null;
    try {
      productConsumption =
          (ProductConsumption)
              entityManager
                  .createQuery(
                      "from ProductConsumption where product.id=?1 and menuProductId=?2 and wastageFlag=?3 and date=?4 and organisation.id=?5 and branch.id=?6")
                  .setParameter(1, productId)
                  .setParameter(2, menuProductId)
                  .setParameter(3, status)
                  .setParameter(4, date)
                  .setParameter(5, orgId)
                  .setParameter(6, branchId)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getProductConsumptionByproductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productConsumption;
  }

  @Override
  public ProductConsumption getProductConsumptionBymenuProductAndwastageFlagAnddateAndOrgAndBranch(
      final long menuProductId,
      final boolean status,
      final Date date,
      final long orgId,
      final long branchId) {

    ProductConsumption productConsumption = null;
    try {
      productConsumption =
          (ProductConsumption)
              entityManager
                  .createQuery(
                      "from ProductConsumption where menuProductId=?1 and wastageFlag=?2 and date=?3 and organisation.id=?4 and branch.id=?5")
                  .setParameter(1, menuProductId)
                  .setParameter(2, status)
                  .setParameter(3, date)
                  .setParameter(4, orgId)
                  .setParameter(5, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductConsumptionBymenuProductAndwastageFlagAnddateAndOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productConsumption;
  }
}
