package com.a2zbill.dao.impl;

import com.a2zbill.dao.InventorySectionDataDao;
import com.a2zbill.domain.InventorySectionData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventorySectionDataDaoImpl implements InventorySectionDataDao {

  private static final Logger logger = LoggerFactory.getLogger(InventorySectionDataDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(InventorySectionData inventorySectionData) {
    try {
      entityManager.persist(inventorySectionData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(InventorySectionData inventorySectionData) {
    try {
      entityManager.merge(inventorySectionData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionData> getAllInventorySectionData() {

    List<InventorySectionData> inventorySectionDataDetail = new ArrayList<>();
    try {
      inventorySectionDataDetail =
          entityManager.createQuery("from InventorySectionData order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionData ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @Override
  public InventorySectionData getInventorySectionDataByInventorySectionId(
      final long inventorySectionId) {

    InventorySectionData inventorySectionDataDetail = null;
    try {
      inventorySectionDataDetail =
          (InventorySectionData)
              entityManager
                  .createQuery("from InventorySectionData where inventorySectionId.id=?1")
                  .setParameter(1, inventorySectionId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionData ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @Override
  public InventorySectionData getInventorySectionDataByproductIdAndOrgIdBranchId(
      String inventorysectionName, final long productId, final long orgId, final long branchId) {

    InventorySectionData inventorySectionDataDetail = null;
    try {
      inventorySectionDataDetail =
          (InventorySectionData)
              entityManager
                  .createQuery(
                      "from InventorySectionData where sectionName=?1 and productId.id=?2 and organisation.id=?3 and branch.id=?4")
                  .setParameter(1, inventorysectionName)
                  .setParameter(2, productId)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataByproductIdAndOrgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionData> getAllInventorySectionDataByOrgIdBranchId(
      final long orgId, final long branchId) {

    List<InventorySectionData> inventorySectionDataDetail = new ArrayList<>();
    try {
      inventorySectionDataDetail =
          entityManager
              .createQuery("from InventorySectionData where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionDataByOrgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionData> getAllInventorySectionDataByOrgId(final long orgId) {

    List<InventorySectionData> inventorySectionDataDetail = new ArrayList<>();
    try {
      inventorySectionDataDetail =
          entityManager
              .createQuery("from InventorySectionData where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionDataByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @Override
  public InventorySectionData getInventorySectionDataByproductIdAndOrgId(
      String inventorysectionName, long productId, long orgId) {

    InventorySectionData inventorySectionDataDetail = null;
    try {
      inventorySectionDataDetail =
          (InventorySectionData)
              entityManager
                  .createQuery(
                      "from InventorySectionData where sectionName=?1 and productId.id=?2 and organisation.id=?3")
                  .setParameter(1, inventorysectionName)
                  .setParameter(2, productId)
                  .setParameter(3, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataByproductIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionData> getInventorySectionDataByFromDateToDateOrgIdAndBranchId(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {

    List<InventorySectionData> inventorySectionDataDetail = new ArrayList<>();
    try {
      inventorySectionDataDetail =
          entityManager
              .createNativeQuery(
                  "select *from product.inventory_section_data where created_date =?1 and modified_date = ?2 and orgid=?3 and branchid=?4")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataByFromDateToDateOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionData> getInventorySectionDataByFromDateToDateOrgId(
      final Date fromDate, final Date toDate, final long orgId) {

    List<InventorySectionData> inventorySectionDataDetail = new ArrayList<>();
    try {
      inventorySectionDataDetail =
          entityManager
              .createNativeQuery(
                  "from InventorySectionData where createdDate = ?1 and modifiedDate = ?2 and organisation.id=?3")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataByFromDateToDateOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDataDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySectionData> getInventorySectionDataByOrgIdAndBranchIdAndSectionName(
      long orgId, long branchId, String sectionName) {

    List<InventorySectionData> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InventorySectionData where  organisation.id=?1 and branch.id=?2 and sectionName=?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, sectionName)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionDataByOrgIdAndBranchIdAndSectionName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }
}
