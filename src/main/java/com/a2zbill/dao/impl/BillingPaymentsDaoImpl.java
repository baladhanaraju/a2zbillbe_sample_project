package com.a2zbill.dao.impl;

import com.a2zbill.dao.BillingPaymentsDao;
import com.a2zbill.domain.Billing;
import com.a2zbill.domain.BillingPayments;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BillingPaymentsDaoImpl implements BillingPaymentsDao {

  private static final Logger logger = LoggerFactory.getLogger(BillingPaymentsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(BillingPayments billing) {
    try {
      entityManager.persist(billing);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean update(BillingPayments billing) {
    try {
      entityManager.merge(billing);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public Billing getBillingDetailsByBillId(final long billId) {
    return null;
  }

  @Override
  public Billing getBillingPaymentsAndOrgIdAndBranchId(
      final long billId, final long orgId, final long branchId) {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BillingPayments> getBillingPaymentsDetailsByBillId(final long billId) {

    List<BillingPayments> list = new ArrayList<>();
    try {
      list =
          (List<BillingPayments>)
              entityManager
                  .createQuery("from BillingPayments  where billing.id =?1")
                  .setParameter(1, billId)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingNumTypeDetailsByBillingTypeName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }
}
