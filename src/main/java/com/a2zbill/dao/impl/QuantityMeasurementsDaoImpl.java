package com.a2zbill.dao.impl;

import com.a2zbill.dao.QuantityMeasurementsDao;
import com.a2zbill.domain.QuantityMeasurements;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class QuantityMeasurementsDaoImpl implements QuantityMeasurementsDao {

  private static final Logger logger = LoggerFactory.getLogger(QuantityMeasurementsDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(QuantityMeasurements quantityMeasurements) {
    try {
      entityManager.persist(quantityMeasurements);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(QuantityMeasurements quantityMeasurements) {
    try {
      entityManager.merge(quantityMeasurements);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<QuantityMeasurements> getAllQuantityMeasurements() {

    List<QuantityMeasurements> quantityMeasurements = new ArrayList<>();
    try {
      quantityMeasurements =
          entityManager.createQuery("from QuantityMeasurements order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllQuantityMeasurements ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return quantityMeasurements;
  }

  @Override
  public QuantityMeasurements getQuantityMeasurementsById(Long id) {

    QuantityMeasurements quantityMeasurements = null;
    try {
      quantityMeasurements =
          (QuantityMeasurements)
              entityManager
                  .createQuery("FROM QuantityMeasurements where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getQuantityMeasurementsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return quantityMeasurements;
  }

  @Override
  public QuantityMeasurements getQuantityMeasurementsByName(String name) {

    QuantityMeasurements quantityMeasurements = null;
    try {
      quantityMeasurements =
          (QuantityMeasurements)
              entityManager
                  .createQuery("FROM QuantityMeasurements where displayName =?1")
                  .setParameter(1, name)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getQuantityMeasurementsByName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return quantityMeasurements;
  }
}
