package com.a2zbill.dao.impl;

import com.a2zbill.controller.constants.Constants;
import com.a2zbill.dao.InvoiceBillDao;
import com.a2zbill.domain.InvoiceBill;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InvoiceBillDaoImpl implements InvoiceBillDao {

  private static final Logger logger = LoggerFactory.getLogger(InvoiceBillDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(InvoiceBill invoiceBill) {
    try {
      entityManager.persist(invoiceBill);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean update(InvoiceBill invoiceBill) {
    try {
      entityManager.merge(invoiceBill);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean delete(InvoiceBill invoiceBill) {
    try {
      entityManager.remove(invoiceBill);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public InvoiceBill getInvoiceBillByIdAndOrgId(long invoiceBillId, long orgId) {

    InvoiceBill invoiceBill = null;
    try {
      invoiceBill =
          (InvoiceBill)
              entityManager
                  .createQuery("from InvoiceBill where id = ?1 and organisation.id = ?2")
                  .setParameter(1, invoiceBillId)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillByIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBill;
  }

  @Override
  public InvoiceBill getInvoiceBillByIdAndOrgIdAndBranchId(
      long invoiceBillId, long orgId, long branchId) {

    InvoiceBill invoiceBill = null;
    try {
      invoiceBill =
          (InvoiceBill)
              entityManager
                  .createQuery(
                      "from InvoiceBill where id = ?1 and organisation.id = ?2 and branch.id = ?3")
                  .setParameter(1, invoiceBillId)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillByIdAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBill;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceBillsByOrgId(long orgId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from InvoiceBill where organisation.id = ?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceBillsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceBillsByOrgIdAndBranchId(long orgId, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from InvoiceBill where organisation.id = ?1 and branch.id = ?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceBillsByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public InvoiceBill getInvoiceBillByInvoiceNumber(String invoiceNumber) {

    InvoiceBill invoiceBill = null;
    try {
      invoiceBill =
          (InvoiceBill)
              entityManager
                  .createQuery("from InvoiceBill where invoiceNumber = ?1")
                  .setParameter(1, invoiceNumber)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillByInvoiceNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBill;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDates(Date fromDate, Date toDate, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and branch.id=?1")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsVendorNameTotalAmount(
      Date fromDate, Date toDate, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select vendorId.vendorName,SUM(totalAmount) from InvoiceBill i where i.date between :stdate and :enddate and branch.id=?1 group by vendorId.vendorName")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsVendorNameTotalAmount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesVendor(
      Date fromDate, Date toDate, long branchId, long vendorId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and branch.id=?1 and vendorId.id=?2")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchId)
              .setParameter(2, vendorId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDatesVendor ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInvoiceVendorDetails(
      String invoiceNumber, Date fromDate, Date toDate, long branchId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createNativeQuery(
                  "select count(vendor_id),vendor_id,vendor_name,invoice_number,organisation.branch.id,organisation.branch.branch_name from product.invoice_bill inner join organisation.branch on organisation.branch.id=product.invoice_bill.branch_id inner join organisation.vendor on organisation.vendor.id=product.invoice_bill.vendor_id where invoice_number=?1 and  date>=?2 and date<=?3 and product.invoice_bill.branch_id=?4 group by invoice_number,vendor_id,vendor_name,organisation.branch.id,organisation.branch.branch_name")
              .setParameter(1, invoiceNumber)
              .setParameter(2, fromDate)
              .setParameter(3, toDate)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceVendorDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllInvoiceVendorDetails(Date fromDate, Date toDate, long branchId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createNativeQuery(
                  "select count(vendor_id),vendor_id,vendor_name,invoice_number,organisation.branch.id,organisation.branch.branch_name from product.invoice_bill inner join organisation.branch on organisation.branch.id=product.invoice_bill.branch_id inner join organisation.vendor on organisation.vendor.id=product.invoice_bill.vendor_id where date>=?2 and date<=?3 and product.invoice_bill.branch_id=?1 group by invoice_number,vendor_id,vendor_name,organisation.branch.id,organisation.branch.branch_name")
              .setParameter(2, fromDate)
              .setParameter(3, toDate)
              .setParameter(1, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceVendorDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public InvoiceBill getAllInvoiceDetailsByDatesByRefernceNumber(
      Date fromDate,
      Date toDate,
      long orgId,
      String refernceNumber,
      long branchId,
      long branchvendorId) {

    InvoiceBill invoicebill = null;
    try {
      invoicebill =
          (InvoiceBill)
              entityManager
                  .createQuery(
                      "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and invoiceNumber=?2 and branch.id=?3 and branchVendor.id=?4 ")
                  .setParameter("stdate", fromDate)
                  .setParameter("enddate", toDate)
                  .setParameter(1, orgId)
                  .setParameter(2, refernceNumber)
                  .setParameter(3, branchId)
                  .setParameter(4, branchvendorId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDatesByRefernceNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoicebill;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchAndBranchVendor(
      Date fromDate, Date toDate, long orgId, long branchId, long branchVendorId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill where date between ?1 and ?2 and organisation.id = ?3 and branch.id = ?4 and branchVendor.id = ?5")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .setParameter(5, branchVendorId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDatesAndBranchAndBranchVendor ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchVendorAndOrganisation(
      Date fromDate, Date toDate, long branchVendorId, long orgId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and branchVendor.id=?1 and organisation.id=?2")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchVendorId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDatesAndBranchVendorAndOrganisation ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDates(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long frombranchId) {

    List<InvoiceBill> invoiceBillDetails = new ArrayList<>();
    try {
      invoiceBillDetails =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2  and branchVendor.id =?3 and invoiceNumber=?4")
              .setParameter("stdate", formDate)
              .setParameter("enddate", endDate)
              .setParameter(4, referenceNumber)
              .setParameter(2, branchId)
              .setParameter(1, orgId)
              .setParameter(3, frombranchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInVoiceBillDetailsByBillIdAndDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBillDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInvoiceBillsTotalAmt(
      Date formDate, Date endDate, long branchId, long orgId, long frombranchId) {

    List<InvoiceBill> invoiceBillDetails = new ArrayList<>();
    try {
      invoiceBillDetails =
          entityManager
              .createQuery(
                  " from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2  and branchVendor.id =?3")
              .setParameter("stdate", formDate)
              .setParameter("enddate", endDate)
              .setParameter(2, branchId)
              .setParameter(1, orgId)
              .setParameter(3, frombranchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillsTotalAmt ", ex);
    } finally {

    }
    return invoiceBillDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsVendorNameTotalAmount(
      long orgId, Date fromDate, Date toDate, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select vendorId.vendorName,SUM(totalAmount) from InvoiceBill i where i.date between :stdate and :enddate and branch.id=?1 group by vendorId.vendorName")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsVendorNameTotalAmount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesVendor(
      long orgId, Date fromDate, Date toDate, long branchId, long vendorId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select vendorId.vendorName,invoiceNumber,date,totalAmount from InvoiceBill i where i.date between :stdate and :enddate and branch.id=?1 and vendorId.id=?2")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchId)
              .setParameter(2, vendorId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDatesVendor ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllInvoiceDetailsBySection(
      long orgId, long branchId, Date fromDate, Date toDate, long fromSectionId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select sum(totalAmount),fromSectionId.sectionName,branch.branchName,date from InvoiceBill i where i.date between :stdate and :enddate and branch.id=?1 and organisation.id=?2 and fromSectionId.id=?3 group by fromSectionId.sectionName,branch.branchName,date")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .setParameter(3, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsBySection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllInvoiceDetailsByFromBranch(
      long orgId, long branchId, Date fromDate, Date toDate, long fromBranchId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select sum(totalAmount),branchVendor.branchName,branch.branchName,date from InvoiceBill i where  date>=?1 and date<=?2 and branchVendor.id=?3 and organisation.id=?4 and branch.id=?5 group by branchVendor.branchName,branch.branchName,date")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, fromBranchId)
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByFromBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDateBydate(
      Date fromDate, Date toDate, long orgId, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  " from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 ")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDateBydate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByBranchAndVendorIsNull(
      long orgId, Date fromdate, Date todate, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and vendorId.id IS NULL")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", todate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByBranchAndVendorIsNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByDatesAndBranchAndFromSectionId(
      Date fromDate, Date toDate, long orgId, long branchId, long fromSectionId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill where date between ?1 and ?2 and organisation.id = ?3 and branch.id = ?4 and fromSectionId.id = ?5")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .setParameter(5, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByDatesAndBranchAndFromSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public InvoiceBill getInvoiceDetailsByDatesAndRefernceNumberAndBranchAndFromSectionId(
      Date fromDate,
      Date toDate,
      long orgId,
      String referenceNumber,
      long branchId,
      long fromSectionId) {

    InvoiceBill invoicebill = null;
    try {
      invoicebill =
          (InvoiceBill)
              entityManager
                  .createQuery(
                      "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and invoiceNumber=?2 and branch.id=?3 and fromSectionId.id=?4 ")
                  .setParameter("stdate", fromDate)
                  .setParameter("enddate", toDate)
                  .setParameter(1, orgId)
                  .setParameter(2, referenceNumber)
                  .setParameter(3, branchId)
                  .setParameter(4, fromSectionId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInvoiceDetailsByDatesAndRefernceNumberAndBranchAndFromSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoicebill;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInvoiceBillByOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long fromSectionId, long orgId, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "  from InvoiceBill i where i.date between :stdate and :enddate  and fromSectionId.id=?1 and organisation.id=?2 and branch.id=?3 ")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", todate)
              .setParameter(1, fromSectionId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillByOrgIdBranchIdSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInvoiceBillByTransferOrgIdFromBranchId(
      Date fromdate, Date todate, long fromBranchId, long orgId, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  " from InvoiceBill  i where i.date between :stdate and :enddate  and branchVendor.id=?1 and organisation.id=?2 and branch.id=?3 ")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", todate)
              .setParameter(1, fromBranchId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillByTransferOrgIdFromBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInvoiceBillByTransferOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long fromSectionId, long orgId, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  " from InvoiceBill  i where i.date between :stdate and :enddate  and fromSectionId.id=?1 and organisation.id=?2 and branch.id=?3 ")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", todate)
              .setParameter(1, fromSectionId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillByTransferOrgIdBranchIdSectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInvoiceBillByOrgIdFromBranchId(
      Date fromdate, Date todate, long fromBranchId, long orgId, long branchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "  from InvoiceBill i where i.date between :stdate and :enddate  and branchVendor.id=?1 and organisation.id=?2 and branch.id=?3")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", todate)
              .setParameter(1, fromBranchId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillByOrgIdFromBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllInvoiceBillProductQuantityByFromToBranch(
      long orgId, long branchId, Date fromdate, Date todate, long frombranchId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              // .createQuery("select sum(InventoryHistory.quantity),productName,date from
              // InvoiceBill join InventoryHistory on InvoiceBill.id =
              // InventoryHistory.invoiceBillId.id join InventoryDetail on
              // InventoryHistory.inventoryId=InventoryDetail.inventoryId join Product on
              // Product.id = InventoryDetail.product.id where date between :stDate and :endDate
              // and organisation.id=?1 and branch.id=?2 and branchVendor.id=?3 group by
              // Product.id,Product.productName")
              .createNativeQuery(
                  "select sum(product.inventory_history.quantity),product.inventory_details.product_id, product.invoice_bill.date, product.invoice_bill.branch_vendor,product.invoice_bill.branch_id from product.invoice_bill join product.inventory_history on product.inventory_history.invoice_bill_id=product.invoice_bill.id join product.inventory_details on  product.inventory_history.inventory_id = product.inventory_details.id where invoice_bill.date>=?1 and product.invoice_bill.date<=?2 and branch_vendor=?3 and product.invoice_bill.org_id=?4 and product.invoice_bill.branch_id=?5 group by product.inventory_details.product_id, product.invoice_bill.date, product.invoice_bill.branch_vendor, product.invoice_bill.branch_id")
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .setParameter(3, frombranchId)
              .setParameter(1, fromdate)
              .setParameter(2, todate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceBillProductQuantityByFromToBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllInvoiceBillProductQuantityByFromSectionToBranch(
      long orgId, long branchId, Date fromdate, Date todate, long fromSectionId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createNativeQuery(
                  "select sum(product.inventory_history.quantity),product.inventory_details.product_id, product.invoice_bill.date, product.invoice_bill.from_section_id,product.invoice_bill.branch_id from product.invoice_bill join product.inventory_history on product.inventory_history.invoice_bill_id=product.invoice_bill.id join product.inventory_details on  product.inventory_history.inventory_id = product.inventory_details.id where invoice_bill.date>=?1 and product.invoice_bill.date<=?2 and from_section_id=?3 and product.invoice_bill.org_id=?4 and product.invoice_bill.branch_id=?5 group by product.inventory_details.product_id, product.invoice_bill.date, product.invoice_bill.from_section_id,product.invoice_bill.branch_id")
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .setParameter(3, fromSectionId)
              .setParameter(1, fromdate)
              .setParameter(2, todate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceBillProductQuantityByFromSectionToBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByBranchAndVendorIdIsNull(
      long orgId, Date fromdate, Date todate, long fromBranchId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branchVendor.id=?2 and vendorId.id IS NULL")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", todate)
              .setParameter(1, orgId)
              .setParameter(2, fromBranchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByBranchAndVendorIdIsNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceDetailsByBranchAndSectionIdIsNull(
      long orgId, Date fromdate, Date todate, long fromSectionId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and fromSectionId.id=?2 and vendorId.id IS NULL")
              .setParameter("stdate", fromdate)
              .setParameter("enddate", todate)
              .setParameter(1, orgId)
              .setParameter(2, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceDetailsByBranchAndSectionIdIsNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceBillBySection(
      Date fromDate, Date toDate, long orgId, long branchId, long fromsectionId) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  " from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and fromSectionId.id=?3 ")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, fromsectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceBillBySection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getAllInvoiceBillBySectionreference(
      Date fromDate, Date toDate, long orgId, long branchId, long fromsectionId, String reference) {

    List<InvoiceBill> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  " from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 and fromSectionId.id=?3 and invoiceNumber=?4")
              .setParameter("stdate", fromDate)
              .setParameter("enddate", toDate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, fromsectionId)
              .setParameter(4, reference)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceBillBySectionreference ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllInvoiceBillByTotalAmountSection(
      Date fromDate, Date toDate, long orgId, long branchId, long fromsectionId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "select sum(total_amount), vendor_id,invoice_number,from_section_id, date,created_date,modified_date, org_id from product.invoice_bill i where i.date>=?1 and date<=?2 and from_section_id=?3 and org_id=?4 and branch_id=?5 group by vendor_id,invoice_number,from_section_id, date,created_date,modified_date, org_id")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .setParameter(3, fromsectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInvoiceBillByTotalAmountSection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDatesToBranchReference(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long fromSectionId) {

    List<InvoiceBill> invoiceBillDetails = new ArrayList<>();
    try {
      invoiceBillDetails =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2  and fromSectionId.id =?3 and invoiceNumber=?4")
              .setParameter("stdate", formDate)
              .setParameter("enddate", endDate)
              .setParameter(4, referenceNumber)
              .setParameter(2, branchId)
              .setParameter(1, orgId)
              .setParameter(3, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInVoiceBillDetailsByBillIdAndDatesToBranchReference ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBillDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InvoiceBill> getInVoiceBillDetailsByBillIdAndDatesToBranch(
      Date formDate, Date endDate, long branchId, long orgId, long fromSectionId) {

    List<InvoiceBill> invoiceBillDetails = new ArrayList<>();
    try {
      invoiceBillDetails =
          entityManager
              .createQuery(
                  "from InvoiceBill i where i.date between :stdate and :enddate and organisation.id=?1 and branch.id=?2  and fromSectionId.id =?3")
              .setParameter("stdate", formDate)
              .setParameter("enddate", endDate)
              .setParameter(2, branchId)
              .setParameter(1, orgId)
              .setParameter(3, fromSectionId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInVoiceBillDetailsByBillIdAndDatesToBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBillDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getInvoiceBillDetailsByLoginVendor(final long vendorId, final long orgId) {
    List<Object[]> invoiceBillDetails = new ArrayList<>();
    try {

      invoiceBillDetails =
          entityManager
              .createQuery(
                  "select id, invoiceNumber, note, totalAmount, taxableAmount, discountAmount, date, vendorId.vendorName, branch.branchName, image from InvoiceBill where vendorId.id=?1 and organisation.id=?2")
              .setParameter(1, vendorId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillDetailsByLoginVendor ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBillDetails;
  }

  @Override
  public List<Object[]> getInvoiceBillDetailsByReport(
      Date fromDate, Date toDate, long orgId, long branchId) {
    List<Object[]> invoiceBillDetails = new ArrayList<>();

    try {

      invoiceBillDetails =
          entityManager
              .createQuery(
                  "select sum(totalAmount) ,invoiceNumber, date,  branch.branchName, branchVendor.branchName ,organisation.orgName,vendorId.vendorName  from InvoiceBill where date between :stdate and :enddate and organisation.id=?1 and branch.id=?2 group by invoiceNumber, date,  branch.branchName, branchVendor.branchName ,organisation.orgName,vendorId.vendorName   ")
              .setParameter(Constants.STDATE, fromDate)
              .setParameter(Constants.ENDDATE, toDate)
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInvoiceBillDetailsByLoginVendor ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invoiceBillDetails;
  }
}
