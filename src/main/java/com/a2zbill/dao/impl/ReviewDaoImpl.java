package com.a2zbill.dao.impl;

import com.a2zbill.dao.ReviewDao;
import com.a2zbill.domain.Review;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReviewDaoImpl implements ReviewDao {

  private static final Logger logger = LoggerFactory.getLogger(ReviewDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(Review review) {
    try {
      entityManager.persist(review);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(Review review) {
    try {
      entityManager.merge(review);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Review> getAllReviewDetails() {

    List<Review> reviewDetails = new ArrayList<>();
    try {
      reviewDetails = entityManager.createQuery("from Review  order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReviewDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reviewDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Review> getAllReviewDetailsByOrgBranchId(final long orgId, final long branchId) {

    List<Review> reviewDetails = new ArrayList<>();
    try {
      reviewDetails =
          entityManager
              .createQuery("from Review where organisation.id=?1 and  branch.id=?2 ")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReviewDetailsByOrgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reviewDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Review> getAllReviewDetailsByOrg(final long orgId) {

    List<Review> reviewDetails = new ArrayList<>();
    try {
      reviewDetails =
          entityManager
              .createQuery("from Review where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReviewDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reviewDetails;
  }
}
