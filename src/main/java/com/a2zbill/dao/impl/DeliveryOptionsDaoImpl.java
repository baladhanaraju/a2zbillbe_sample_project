package com.a2zbill.dao.impl;

import com.a2zbill.dao.DeliveryOptionsDao;
import com.a2zbill.domain.DeliveryOptions;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DeliveryOptionsDaoImpl implements DeliveryOptionsDao {

  private static final Logger logger = LoggerFactory.getLogger(DeliveryOptionsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final DeliveryOptions deliveryOptions) {
    try {
      entityManager.persist(deliveryOptions);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final DeliveryOptions deliveryOptions) {
    try {
      entityManager.remove(deliveryOptions);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final DeliveryOptions deliveryOptions) {
    try {
      entityManager.merge(deliveryOptions);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public DeliveryOptions getDeliveryOptionsByid(final long id) {

    DeliveryOptions deliveryOptions = null;
    try {
      deliveryOptions =
          (DeliveryOptions)
              this.entityManager
                  .createQuery("from DeliveryOptions where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getDeliveryOptionsByid ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return deliveryOptions;
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<DeliveryOptions> getAllDeliveryOptions() {

    List<DeliveryOptions> deliveryOptions = new ArrayList<>();
    try {
      deliveryOptions =
          entityManager.createQuery("from DeliveryOptions order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllDeliveryOptions ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return deliveryOptions;
  }
}
