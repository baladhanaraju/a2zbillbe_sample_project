package com.a2zbill.dao.impl;

import com.a2zbill.dao.CounterDao;
import com.a2zbill.domain.Counter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CounterDaoImpl implements CounterDao {

  private static final Logger logger = LoggerFactory.getLogger(CounterDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Counter counter) {
    try {
      entityManager.persist(counter);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    Object result = entityManager.find(Counter.class, id);
    try {
      if (result != null) {
        entityManager.remove(result);
        return true;
      }
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(Counter counter) {
    try {
      entityManager.merge(counter);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getAllCounterDetails() {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          entityManager.createQuery("FROM Counter order by counter_id").getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsOnStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsOnStatus(final String counterStatus) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          entityManager
              .createQuery("FROM Counter c where c.counterStatus = ?1")
              .setParameter(1, counterStatus)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsOnStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @Override
  public Counter getCounterDetailsById(final long id) {

    Counter counterDetails = null;
    try {
      counterDetails =
          (Counter)
              entityManager
                  .createQuery("FROM Counter c where c.id = ?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @Override
  public Counter getCounterDetailOnStatus(final String counterStatus) {

    Counter counterDetails = null;
    try {
      counterDetails =
          (Counter)
              entityManager
                  .createQuery("FROM Counter c where c.counterStatus = ?1")
                  .setParameter(1, counterStatus)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCounterDetailOnStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getAllCounterDetail(final long orgId, final long branchId) {

    List<Long> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          entityManager
              .createQuery(
                  "select counterNumber FROM Counter where orgId.id = ?1 and branchId.id = ?2 and counterStatus = 'Closed' ORDER BY id ASC")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCounterDetail ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailOnStatusByOrgIdandBranchId(
      final long orgId, final long branchId, final String counterStatus) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          entityManager
              .createQuery(
                  "FROM Counter where orgId.id =?1 and branchId.id=?2 and counterStatus=?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, counterStatus)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailOnStatusByOrgIdandBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getAllCounterDetailsByOrgBranchId(long orgId, long branchId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          entityManager
              .createQuery(
                  " FROM Counter where orgId.id =?1 and branchId.id=?2 and counterStatus='Closed' ORDER BY id ASC")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCounterDetailsByOrgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgBranchIdBySectionId(
      final long orgId, final long branchId, final long sectionId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          entityManager
              .createQuery(
                  " FROM Counter where orgId.id = ?1 and branchId.id=?2 and section.id=?3 and status=?4 ORDER BY id ASC")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, sectionId)
              .setParameter(4, true)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgBranchIdBySectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @Override
  public Counter getCounterDetailsBycounterNumber(long orgId, long branchId, long counterNumber) {

    Counter Counter = null;
    try {
      Counter =
          (Counter)
              this.entityManager
                  .createQuery(
                      "from Counter where counterNumber = ?1 and orgId.id = ?2 and branchId.id = ?3")
                  .setParameter(1, counterNumber)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsBycounterNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return Counter;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getAllCounterDetailsByOrgId(final long orgId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  " FROM Counter where orgId.id = ?1 and counterStatus='Closed' ORDER BY id ASC")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCounterDetailsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailOnStatusByOrgId(
      final long orgId, final String counterStatus) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery("FROM Counter where orgId.id = ?1 and counterStatus = ?2")
              .setParameter(1, orgId)
              .setParameter(2, counterStatus)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailOnStatusByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdBySectionId(final long orgId, final long sectionId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  " FROM Counter where orgId.id = ?1 and section.id = ?2  and status=?3 ORDER BY id ASC")
              .setParameter(1, orgId)
              .setParameter(2, sectionId)
              .setParameter(3, true)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdBySectionId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetilsByOrgIdAndBranchId(long orgId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(" FROM Counter where orgId.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetilsByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsBasedOnOrgIdAndBranchId(long orgId, long branchId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(" FROM Counter where orgId.id=?1 and branchId.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsBasedOnOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdandStatusActive(long OrgId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(" FROM Counter where orgId.id = ?1 and status=true")
              .setParameter(1, OrgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdandStatusActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdandbranchIdStatusActive(long orgId, long branchId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  " FROM Counter where orgId.id = ?1 and branchId.id = ?2 and status =true")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdandbranchIdStatusActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdandStatusInActive(
      long OrgId, long branchId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(" FROM Counter where orgId.id = ?1 and branchId.id=?2 and status =false")
              .setParameter(1, OrgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdBranchIdandStatusInActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdandStatusInActive(long OrgId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(" FROM Counter where orgId.id=?1 and status =false")
              .setParameter(1, OrgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdandStatusInActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdandStatusActiveAndInActive(
      long orgId, long branchId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  " FROM Counter where orgId.id =?1 and branchId.id=?2 order by status desc")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdBranchIdandStatusActiveAndInActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdandStatusActiveAndInActive(long orgId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(" FROM Counter where orgId.id =?1  order by status desc")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdandStatusActiveAndInActive ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getAllCounterDetailByStatus(long orgId, long branchId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(" FROM Counter where orgId.id =?1 and branchId.id=?2 order by status")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCounterDetailByStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgBranchIdBySectionAndStatus(
      long orgId, long branchId, long sectionId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  " FROM Counter where orgId.id = ?1 and section.id = ?2 and status=?3 ORDER BY status")
              .setParameter(1, orgId)
              .setParameter(2, sectionId)
              .setParameter(3, true)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgBranchIdBySectionAndStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdSectionIdandOnline(
      long orgId, long branchId, long sectionId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  "From Counter where orgId.id = ?1 and branchId.id=?2 and section.id = ?3 and online=?4 ORDER BY online")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, sectionId)
              .setParameter(4, false)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdBranchIdSectionIdandOnline ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdSectionIdandOnline(long orgId, long sectionId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  "From Counter where orgId.id = ?1 and branchId.id=?2 and section.id = ?3 and online=?4 ORDER BY online")
              .setParameter(1, orgId)
              .setParameter(2, sectionId)
              .setParameter(3, false)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdSectionIdandOnline ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdSectionIdandtrue(long orgId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery(
                  "From Counter where orgId.id = ?1 and branchId.id=?2 and section.id = ?3 and online=?4 ORDER BY online")
              .setParameter(1, orgId)
              .setParameter(2, true)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdSectionIdandtrue ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Counter> getCounterDetailsByOrgIdBranchIdSectionIdandtrue(long orgId, long branchId) {

    List<Counter> counterDetails = new ArrayList<>();
    try {
      counterDetails =
          this.entityManager
              .createQuery("From Counter where orgId.id = ?1 and branchId.id=?2 and online ='TRUE'")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterDetailsByOrgIdBranchIdSectionIdandtrue ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }

  @Override
  public Counter getCounterDetailOnlineStatus(boolean counterStatus) {

    Counter counterDetails = null;
    try {
      counterDetails =
          (Counter)
              this.entityManager
                  .createQuery("From Counter where  online=?1 ")
                  .setParameter(1, counterStatus)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCounterDetailOnlineStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterDetails;
  }
}
