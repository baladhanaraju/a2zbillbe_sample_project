package com.a2zbill.dao.impl;

import com.a2zbill.dao.RoleserviceDao;
import com.tsss.basic.domain.Roles;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleserviceDaoImpl implements RoleserviceDao {

  private static final Logger logger = LoggerFactory.getLogger(RoleserviceDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Roles role) {
    try {
      entityManager.persist(role);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(Roles role) {
    try {
      entityManager.merge(role);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Roles> getAllRolesServiceDetails() {

    List<Roles> roleDetails = new ArrayList<>();
    try {
      roleDetails = this.entityManager.createQuery("from Role order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRolesServiceDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return roleDetails;
  }
}
