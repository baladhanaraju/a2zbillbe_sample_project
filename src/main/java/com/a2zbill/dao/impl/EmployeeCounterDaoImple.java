package com.a2zbill.dao.impl;

import com.a2zbill.dao.EmployeeCounterDao;
import com.a2zbill.domain.EmployeeCounter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeCounterDaoImple implements EmployeeCounterDao {

  private static final Logger logger = LoggerFactory.getLogger(EmployeeCounterDaoImple.class);

  @Autowired private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public List<EmployeeCounter> getAllEmployeeCounterDetails() {

    List<EmployeeCounter> employeeCounterDetails = new ArrayList<>();
    try {
      employeeCounterDetails =
          this.entityManager.createQuery("from EmployeeCounter order by id ").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllEmployeeCounterDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return employeeCounterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<EmployeeCounter> getAllEmployeeCounterDetailsByorgIdBranchId(
      final long orgId, final long branchId) {

    List<EmployeeCounter> employeeCounterDetails = new ArrayList<>();
    try {
      employeeCounterDetails =
          this.entityManager
              .createQuery("from EmployeeCounter where  orgId.id=?1 and branchId.id=?2 ")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllEmployeeCounterDetailsByorgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return employeeCounterDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<EmployeeCounter> getAllEmployeeCounterDetailsByorgId(final long orgId) {

    List<EmployeeCounter> employeeCounterDetails = new ArrayList<>();
    try {
      employeeCounterDetails =
          this.entityManager
              .createQuery("from EmployeeCounter where orgId.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllEmployeeCounterDetailsByorgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return employeeCounterDetails;
  }
}
