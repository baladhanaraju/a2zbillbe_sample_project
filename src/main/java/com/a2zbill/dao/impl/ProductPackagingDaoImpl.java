package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductPackagingDao;
import com.a2zbill.domain.ProductPackaging;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductPackagingDaoImpl implements ProductPackagingDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductPackagingDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(ProductPackaging productPackaging) {
    try {
      entityManager.persist(productPackaging);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(ProductPackaging productPackaging) {
    try {
      entityManager.remove(productPackaging);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(ProductPackaging productPackaging) {
    try {
      entityManager.merge(productPackaging);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductPackaging> getAllProductPackagingDetails() {
    List<ProductPackaging> list =
        entityManager.createQuery("from ProductPackging order by id").getResultList();

    return list;
  }

  @Override
  public ProductPackaging getProductPackagingDetailsByProductId(long productId) {
    ProductPackaging productPackagingDetails =
        (ProductPackaging) entityManager.createQuery("FROM ProductPackging p where p.id = ?1");
    return productPackagingDetails;
  }

  @Override
  public List<ProductPackaging> getProductPackgingDetailsByParentId(long parentId) {
    @SuppressWarnings("unchecked")
    List<ProductPackaging> productPackgingDetails =
        entityManager
            .createQuery("FROM ProductPackaging where  parentproductId.id=?1")
            .setParameter(1, parentId)
            .getResultList();
    return productPackgingDetails;
  }
}
