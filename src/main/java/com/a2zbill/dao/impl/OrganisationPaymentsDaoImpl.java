package com.a2zbill.dao.impl;

import com.a2zbill.dao.OrganisationPaymentsDao;
import com.a2zbill.domain.OrganisationPayments;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrganisationPaymentsDaoImpl implements OrganisationPaymentsDao {

  private static final Logger logger = LoggerFactory.getLogger(OrganisationPaymentsDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(OrganisationPayments organisationPayments) {
    try {
      entityManager.persist(organisationPayments);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrganisationPayments> getAllOrganisationPayments() {

    List<OrganisationPayments> paymentDetails = new ArrayList<>();
    try {
      paymentDetails =
          entityManager.createQuery("from OrganisationPayments order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllOrganisationPayments ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return paymentDetails;
  }

  @Override
  public void update(OrganisationPayments organisationPayments) {
    try {
      entityManager.merge(organisationPayments);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrganisationPayments> getOrganisationPaymentsByOrgId(long orgId) {
    List<OrganisationPayments> paymentDetails = new ArrayList<>();
    try {
      paymentDetails =
          entityManager
              .createQuery("from OrganisationPayments where orgId.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrganisationPaymentsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return paymentDetails;
  }

  @Override
  public OrganisationPayments getOrganisationPaymentsByOrgIdAndPaymentId(
      long orgId, long paymentId) {
    OrganisationPayments paymentDetails = null;
    try {
      paymentDetails =
          (OrganisationPayments)
              entityManager
                  .createQuery(
                      "from OrganisationPayments where orgId.id=?1 and paymentId.paymentId=?2")
                  .setParameter(1, orgId)
                  .setParameter(2, paymentId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrganisationPaymentsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return paymentDetails;
  }
}
