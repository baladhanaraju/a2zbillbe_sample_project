package com.a2zbill.dao.impl;

import com.a2zbill.dao.FrequencyDao;
import com.a2zbill.domain.Frequency;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FrequencyDaoImpl implements FrequencyDao {

  private static final Logger logger = LoggerFactory.getLogger(FrequencyDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(Frequency frequency) {
    try {
      entityManager.persist(frequency);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(Frequency frequency) {
    try {
      entityManager.merge(frequency);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Frequency> getAllFrequencys() {

    List<Frequency> frequencyDetails = new ArrayList<>();
    try {
      frequencyDetails = entityManager.createQuery("from Frequency order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllFrequencys ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return frequencyDetails;
  }

  @Override
  public Frequency getFrequencyById(long id) {

    Frequency frequencyDetails = null;
    try {
      frequencyDetails =
          (Frequency)
              entityManager
                  .createQuery("from Frequency where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFrequencyById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return frequencyDetails;
  }
}
