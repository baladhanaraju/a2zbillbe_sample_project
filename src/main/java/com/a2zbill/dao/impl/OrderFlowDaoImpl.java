package com.a2zbill.dao.impl;

import com.a2zbill.dao.OrderFlowDao;
import com.a2zbill.domain.OrderFlow;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderFlowDaoImpl implements OrderFlowDao {

  private static final Logger logger = LoggerFactory.getLogger(OrderFlowDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final OrderFlow orderFlow) {
    try {
      entityManager.persist(orderFlow);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(final OrderFlow orderFlow) {
    try {
      entityManager.merge(orderFlow);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public OrderFlow getOrderFlowById(final Long id) {

    OrderFlow orderFlow = null;
    try {
      orderFlow =
          (OrderFlow)
              entityManager
                  .createQuery("FROM OrderFlow where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderFlowById ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return orderFlow;
  }

  @Override
  public OrderFlow getOrderFlowByCartId(final Long cartId) {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderFlow> getAllOrderFlow() {

    List<OrderFlow> orderFlow = new ArrayList<>();
    try {
      orderFlow = entityManager.createQuery("from OrderFlow order by id").getResultList();

    } catch (final Exception ex) {
      logger.error("getAllOrderFlow ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return orderFlow;
  }

  @Override
  public OrderFlow getOrderFlowByCartIdAndProductIdAndFlowId(
      final Long cartId, final Long productId) {

    OrderFlow orderFlow = null;
    try {
      orderFlow =
          (OrderFlow)
              entityManager
                  .createQuery(
                      "FROM OrderFlow where cartDetails.cartId =?1 and products.id=?2 and "
                          + "flow.flowName like 'kitchen section'")
                  .setParameter(1, cartId)
                  .setParameter(2, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderFlowByCartIdAndProductIdAndFlowId ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return orderFlow;
  }

  @Override
  public OrderFlow getOrderFlowByCartIdAndProductIdAndCartIdByServiceSection(
      final Long cartId, final Long productId) {

    OrderFlow orderFlow = null;
    try {
      orderFlow =
          (OrderFlow)
              entityManager
                  .createQuery(
                      "FROM OrderFlow where cartDetails.cartId =?1 and products.id=?2 and "
                          + "flow.flowName like 'service section'")
                  .setParameter(1, cartId)
                  .setParameter(2, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderFlowByCartIdAndProductIdAndCartIdByServiceSection ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return orderFlow;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderFlow> getOrderFlowByFlowId() {

    List<OrderFlow> orderFlow = new ArrayList<>();
    try {
      orderFlow =
          entityManager
              .createQuery(
                  "select products.productName,SUM(quantity) from OrderFlow where cartDetails.cartStatus like 'Open' and "
                      + "flow.id = (select id  from Flow where flowName like 'kitchen section' and flowType.id=1) group by products.productName")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrderFlowByFlowId ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return orderFlow;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<OrderFlow> getOrderFlowByCounterIdAndFlowId() {

    List<OrderFlow> orderFlow = new ArrayList<>();
    try {
      orderFlow =
          entityManager
              .createQuery(
                  "select products.productName,quantity, products.id, cartDetails.id from OrderFlow where cartDetails.cartStatus like 'Open' and "
                      + "flow.id = (select id  from Flow where flowName like 'kitchen section' and flowType.id=1) order by time")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOrderFlowByCounterIdAndFlowId ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return orderFlow;
  }
}
