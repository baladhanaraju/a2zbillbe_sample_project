package com.a2zbill.dao.impl;

import com.a2zbill.dao.InventorySectionDao;
import com.a2zbill.domain.InventorySection;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventorySectionDaoImpl implements InventorySectionDao {

  private static final Logger logger = LoggerFactory.getLogger(InventorySectionDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(InventorySection inventorySection) {
    try {
      entityManager.persist(inventorySection);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(InventorySection inventorySection) {
    try {
      entityManager.merge(inventorySection);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySection> getAllInventorySection() {

    List<InventorySection> inventorySectionDetail = new ArrayList<>();
    try {
      inventorySectionDetail =
          this.entityManager.createQuery("from InventorySection order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySection ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySection> getAllInventorySectionByOrgIdBranchId(
      final long orgId, final long branchId) {

    List<InventorySection> inventorySectionDetail = new ArrayList<>();
    try {
      inventorySectionDetail =
          this.entityManager
              .createQuery("from InventorySection where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionByOrgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySection> getAllInventorySectionByOrgIdBranchIdAndMasterFlag(
      final long orgId, final long branchId, final String consumedFlag) {

    List<InventorySection> inventorySectionDetail = new ArrayList<>();
    try {
      inventorySectionDetail =
          this.entityManager
              .createQuery(
                  "from InventorySection where organisation.id=?1 and branch.id=?2 and consumedFlag=?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, consumedFlag)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionByOrgIdBranchIdAndMasterFlag ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @Override
  public InventorySection getInventorySectionByOrgIdBranchIdAndMasterFlag(
      final long orgId, final long branchId, final String consumedFlag) {

    InventorySection inventorySectionDetail = null;
    try {
      inventorySectionDetail =
          (InventorySection)
              this.entityManager
                  .createQuery(
                      "from InventorySection where organisation.id=?1 and branch.id=?2 and consumedFlag=?3")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .setParameter(3, consumedFlag)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionByOrgIdBranchIdAndMasterFlag ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @Override
  public InventorySection getInventorySectionByOrgIdBranchIdAndId(
      final long orgId, final long branchId, final long id) {

    InventorySection inventorySectionDetail = null;
    try {
      inventorySectionDetail =
          (InventorySection)
              this.entityManager
                  .createQuery(
                      "from InventorySection where organisation.id=?1 and branch.id=?2 and id=?3")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .setParameter(3, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventorySectionByOrgIdBranchIdAndId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @Override
  public InventorySection getInventorySectionBySectionNameOrgIdBranchId(
      final String sectionName, final long orgId, final long branchId) {

    InventorySection inventorySectionDetail = null;
    try {
      inventorySectionDetail =
          (InventorySection)
              this.entityManager
                  .createQuery(
                      "from InventorySection where sectionName=?1 and organisation.id=?2 and branch.id=?3 ")
                  .setParameter(1, sectionName)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventorySectionBySectionNameOrgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @Override
  public InventorySection getInventorySectionByOrgIdAndId(final long orgId, final long id) {

    InventorySection inventorySectionDetail = null;
    try {
      inventorySectionDetail =
          (InventorySection)
              this.entityManager
                  .createQuery("from InventorySection where organisation.id=?1 and id=?2")
                  .setParameter(1, orgId)
                  .setParameter(2, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventorySectionByOrgIdAndId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySection> getAllInventorySectionByOrgIdBranchIdByStaticKotFlag(
      final long orgId, final long branchId) {

    List<InventorySection> inventorySectionDetail = new ArrayList<>();
    try {
      inventorySectionDetail =
          this.entityManager
              .createQuery(
                  "from InventorySection where organisation.id=?1 and branch.id=?2 and kotFlag='true' ")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionByOrgIdBranchIdByStaticKotFlag ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventorySection> getAllInventorySectionByOrgId(final long orgId) {

    List<InventorySection> inventorySectionDetail = new ArrayList<>();
    try {
      inventorySectionDetail =
          this.entityManager
              .createQuery("from InventorySection where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventorySectionByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @Override
  public InventorySection getInventorySectionById(Long id) {

    InventorySection inventorySectionDetail = null;
    try {
      inventorySectionDetail =
          (InventorySection)
              this.entityManager
                  .createQuery("FROM InventorySection where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventorySectionById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }

  @Override
  public List<Object[]> getInventorySectionNameByOrgId(long orgId) {
    List<Object[]> inventorySections = new ArrayList<Object[]>();
    try {
      inventorySections =
          (List<Object[]>)
              this.entityManager
                  .createQuery(
                      "select id, sectionName, branch.id, branch.branchName FROM InventorySection where organisation.id =?1")
                  .setParameter(1, orgId)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionNameByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySections;
  }

  @Override
  public List<Object[]> getInventorySectionNamesByOrgIdAndBranchId(long orgId, long branchId) {
    List<Object[]> inventorySectionDetail = new ArrayList<>();
    try {
      inventorySectionDetail =
          this.entityManager
              .createQuery(
                  "select id, sectionName, branch.id, branch.branchName from InventorySection where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventorySectionNamesByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventorySectionDetail;
  }
}
