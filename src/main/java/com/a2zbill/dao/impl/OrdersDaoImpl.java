package com.a2zbill.dao.impl;

import com.a2zbill.dao.OrderDao;
import com.a2zbill.domain.Order;
import com.a2zbill.domain.OrderStatus;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrdersDaoImpl implements OrderDao {

  private static final Logger logger = LoggerFactory.getLogger(OrdersDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final Order order) {
    try {
      entityManager.persist(order);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(final Order order) {
    try {
      entityManager.merge(order);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Order> getAllOrderDetails() {

    List<Order> orders = new ArrayList<>();
    try {
      orders = entityManager.createQuery("from Order").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllOrderDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orders;
  }

  @Override
  public Order getOrderById(final long id) {

    Order orders = null;
    try {
      orders =
          (Order)
              entityManager
                  .createQuery("from Order  where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orders;
  }

  @Override
  public Order getOrderByGuid(final String guid) {

    Order orderDetails = null;
    try {
      orderDetails =
          (Order)
              entityManager
                  .createQuery("from Order where guid =?1")
                  .setParameter(1, guid)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderByGuid ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderDetails;
  }

  @Override
  public Order getOrdersDetailsByStatus(final String orderStatus, final long counterId) {

    Order orderDetails = null;
    try {
      orderDetails =
          (Order)
              entityManager
                  .createQuery(
                      "from Order where orderStatus =?1 and counterDetails.counterNumber=?2")
                  .setParameter(1, orderStatus)
                  .setParameter(1, counterId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrdersDetailsByStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderDetails;
  }

  @Override
  public OrderStatus getOrderStatusById(long id) {
    OrderStatus orderDetails = null;
    try {
      orderDetails =
          (OrderStatus)
              entityManager
                  .createQuery("select o.orderStatus from Order o where o.id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderStatusById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderDetails;
  }
}
