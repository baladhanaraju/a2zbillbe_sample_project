package com.a2zbill.dao.impl;

import com.a2zbill.dao.InventoryReportsDao;
import com.a2zbill.domain.InventoryReports;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryReportsDaoImpl implements InventoryReportsDao {

  private static final Logger logger = LoggerFactory.getLogger(InventoryReportsDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(InventoryReports inventoryReports) {
    try {
      entityManager.persist(inventoryReports);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean update(InventoryReports inventoryReports) {
    try {
      entityManager.merge(inventoryReports);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public InventoryReports getInventoryByDateAndInventoryId(
      Date createdDate, long inventoryId, long orgId, long branchId) {

    InventoryReports inventoryReports = null;
    try {
      inventoryReports =
          (InventoryReports)
              entityManager
                  .createQuery(
                      "from InventoryReports where createdDate=?1 and inventoryDetail.id=?2 and organisation.id=?3 and branch.id=?4")
                  .setParameter(1, createdDate)
                  .setParameter(2, inventoryId)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getResultList()
                  .stream()
                  .findFirst()
                  .orElse(null);
    } catch (final Exception ex) {
      logger.error("getInventoryByDateAndInventoryId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryReports;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryReports> getInventoryReportsByDate(Date date, long branchId) {

    List<InventoryReports> inventoryReports = new ArrayList<>();
    try {
      inventoryReports =
          entityManager
              .createQuery("From InventoryReports where createdDate=?1 and branch.id=?2")
              .setParameter(1, date)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventoryReportsByDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryReports;
  }
}
