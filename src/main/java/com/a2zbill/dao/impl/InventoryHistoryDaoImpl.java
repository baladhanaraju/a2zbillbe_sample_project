package com.a2zbill.dao.impl;

import com.a2zbill.dao.InventoryHistoryDao;
import com.a2zbill.domain.InventoryHistory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryHistoryDaoImpl implements InventoryHistoryDao {

  private static final Logger logger = LoggerFactory.getLogger(InventoryHistoryDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(InventoryHistory inventoryHistory) {
    try {
      entityManager.persist(inventoryHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(InventoryHistory inventoryHistory) {
    try {
      entityManager.merge(inventoryHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistorys() {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          entityManager.createQuery("from InventoryHistory order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistorys ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistorys(final long branchId, final long orgId) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          entityManager
              .createQuery("from InventoryHistory where branch.id=?1 and organisation.id=?2")
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistorys ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistorysByOrg(final long orgId) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          entityManager
              .createQuery("from InventoryHistory where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistorysByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndVendorBetweenTwoDates(
      Date fromDate, Date toDate, long orgId, long vendorId) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryHistoryDate BETWEEN ?1 and ?2 and  organisation.id=?3 and vendorId.id=?4")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, vendorId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistoryDetailsByOrgAndVendorBetweenTwoDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBranchAndVendorBetweenTwoDates(
      Date fromDate, Date toDate, long orgId, long branchId, long vendorId) {
    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryHistoryDate BETWEEN ?1 and ?2 and  organisation.id=?3 and vendorId.id=?4 and branch.id=?5")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, vendorId)
              .setParameter(5, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistoryDetailsByOrgAndBranchAndVendorBetweenTwoDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBranchAndBetweenTwoDates(
      Date fromDate, Date toDate, long orgId, long branchId) {
    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryHistoryDate BETWEEN ?1 and ?2 and  organisation.id=?3 and branch.id=?4")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistoryDetailsByOrgAndBranchAndBetweenTwoDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByOrgAndBetweenTwoDates(
      Date fromDate, Date toDate, long orgId) {
    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryHistoryDate BETWEEN ?1 and ?2 and  organisation.id=?3")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistoryDetailsByOrgAndBetweenTwoDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistorysByInventoryId(long branchId, long orgId) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery("from InventoryHistory where branch.id=?1 and organisation.id=?2")
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistorysByInventoryId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistorysByInventoryId(
      long branchId, long orgId, long inventoryId) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where branch.id=?1 and organisation.id=?2 and inventoryId.inventoryId=?3  ")
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .setParameter(3, inventoryId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistorysByInventoryId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByInvoiceBillId(
      long branchId, long orgId, long invoiceId) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where branch.id=?1 and organisation.id=?2 and invoiceBillId.id=?3 ")
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .setParameter(3, invoiceId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistoryDetailsByInvoiceBillId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getAllInventoryHistoryDetailsByInvoiceBillId(
      long orgId, long invoiceId) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where organisation.id=?1 and invoiceBillId.id=?2 ")
              .setParameter(1, orgId)
              .setParameter(2, invoiceId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryHistoryDetailsByInvoiceBillId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory>
      getAllInventoryHistoryDetailsByInventoryIdAndOrgAndBranchAndBetweenTwoDates(
          long inventoryId, long orgId, long branchId, Date fromDate, Date toDate) {

    List<InventoryHistory> inventoryHistoryDetail = new ArrayList<>();
    try {
      inventoryHistoryDetail =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryId.inventoryId=?1 and organisation.id=?2 and branch.id=?3 and inventoryHistoryDate BETWEEN ?4 and ?5")
              .setParameter(1, inventoryId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .setParameter(4, fromDate)
              .setParameter(5, toDate)
              .getResultList();
    } catch (Exception e) {
      logger.error(
          " getAllInventoryHistoryDetailsByInventoryIdAndOrgAndBranchAndBetweenTwoDates DaoImpl"
              + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryHistoryDetail;
  }

  @Override
  public String getMaxCountByBatchNumberAndInventoryIdAndOrgAndBranch(
      String batchNumber, long inventoryId, long orgId, long branchId) {

    String count = null;
    try {
      count =
          (String)
              this.entityManager
                  .createNativeQuery(
                      "select max(SUBSTRING(batch_number, 10)) from product.inventory_history where batch_number LIKE ?1 and inventory_id = ?2 and org_id = ?3 and branch_id = ?4")
                  .setParameter(1, batchNumber + "-%")
                  .setParameter(2, inventoryId)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getSingleResult();
    } catch (Exception e) {
      logger.error(" getMaxCountByBatchNumberAndInventoryIdAndOrgAndBranch DaoImpl" + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return count;
  }

  @Override
  public InventoryHistory getInventoryHisotryDetailsByBatchNumberAndInventoryIdAndOrgAndBranch(
      String batchNumber, long inventoryId, long orgId, long branchId) {
    InventoryHistory invHistory = null;
    try {
      invHistory =
          (InventoryHistory)
              this.entityManager
                  .createQuery(
                      "from InventoryHistory where batchNumber = ?1 and inventoryId.inventoryId = ?2 and organisation.id = ?3 and branch.id=?4")
                  .setParameter(1, batchNumber)
                  .setParameter(2, inventoryId)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getSingleResult();
    } catch (Exception e) {
      logger.error(
          " getInventoryHisotryDetailsByBatchNumberAndInventoryIdAndOrgAndBranch DaoImpl" + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return invHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory>
      getInventoryHsitoryDetailsByExpiryDateAndProductIdAndOrgIdAndBranchIdOrderByAvailableQuantity(
          Date expiryDate, long productId, long orgId, long branchId) {

    List<InventoryHistory> list = new ArrayList<>();
    try {
      list =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where expiryDate = ?1 and inventoryId.product.id = ?2 and organisation.id = ?3 and branch.id = ?4 and quantity-(consumedCount+expiryCount)>0 order by quantity-(consumedCount+expiryCount)")
              .setParameter(1, expiryDate)
              .setParameter(2, productId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (Exception e) {
      logger.error(
          " getInventoryHsitoryDetailsByExpiryDateAndProductIdAndOrgIdAndBranchIdOrderByAvailableQuantity DaoImpl"
              + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory>
      getInventoryHistoryDetailsByProductIdAndOrgIdAndBranchIdOrderByExpiryDate(
          long productId, long orgId, long branchId) {

    List<InventoryHistory> list = new ArrayList<>();
    try {
      list =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryId.product.id = ?1 and organisation.id = ?2 and branch.id = ?3 and quantity-(consumedCount+expiryCount)>0 order by expiryDate,quantity-(consumedCount+expiryCount)")
              .setParameter(1, productId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (Exception e) {
      logger.error(
          " getInventoryHistoryDetailsByProductIdAndOrgIdAndBranchIdOrderByExpiryDate DaoImpl" + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAvailableQuantityWithExpiryDateByProductIdAndOrgIdAndBrnachId(
      long productId, long orgId, long branchId) {
    List<Object[]> list = new ArrayList<>();
    try {
      list =
          this.entityManager
              .createNativeQuery(
                  "Select t2.expiry_date as exp_date,Sum(t2.quantity - (t2.consumed_count + t2.expiry_count)) as availableqty From product.inventory_details as t1 inner join product.inventory_history as t2 On t1.id=t2.inventory_id where (t2.quantity - (t2.consumed_count + t2.expiry_count ) )  > 0 and t1.product_id=?1 and t2.org_id = ?2 and t1.branch_id=?3 Group by  t2.expiry_date Order By exp_date")
              .setParameter(1, productId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (Exception e) {
      logger.error(" getAvailableQuantityWithExpiryDateByProductIdAndOrgIdAndBrnachId DaoImpl" + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory> getInventoryHisotryDetailsByDatesAndProductIdAndOrgIdAndBranchId(
      Date fromDate, Date toDate, long productId, long orgId, long branchId) {

    List<InventoryHistory> list = new ArrayList<>();
    try {
      list =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryHistoryDate BETWEEN ?1 and ?2 and inventoryId.product.id = ?3 and organisation.id = ?4 and branch.id = ?5")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, productId)
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .getResultList();
    } catch (Exception e) {
      logger.error("getInventoryHisotryDetailsByDatesAndProductIdAndOrgIdAndBranchId DaoImpl " + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryHistory>
      getInventoryHistoryDetailsByDateAndVendorIdAndProductIdAndOrgIdAndBranchId(
          Date fromDate, Date toDate, long vendorId, long productId, long orgId, long branchId) {

    List<InventoryHistory> list = new ArrayList<>();
    try {
      list =
          this.entityManager
              .createQuery(
                  "from InventoryHistory where inventoryHistoryDate BETWEEN ?1 and ?2 and vendorId.id = ?3 and inventoryId.product.id = ?4 and organisation.id = ?5 and branch.id = ?6")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, vendorId)
              .setParameter(4, productId)
              .setParameter(5, orgId)
              .setParameter(6, branchId)
              .getResultList();
    } catch (Exception e) {
      logger.error(
          "getInventoryHistoryDetailsByDateAndVendorIdAndProductIdAndOrgIdAndBranchId DaoImpl "
              + e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }
}
