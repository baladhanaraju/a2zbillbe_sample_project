package com.a2zbill.dao.impl;

import com.a2zbill.dao.CustomerCreditsDao;
import com.a2zbill.domain.CustomerCredits;
import com.tsss.basic.paytm.domain.Credits;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerCreditsDaoImpl implements CustomerCreditsDao {

  private static final Logger logger = LoggerFactory.getLogger(CustomerCreditsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(CustomerCredits customerCredits) {
    try {
      entityManager.persist(customerCredits);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(CustomerCredits customerCredits) {
    try {
      entityManager.merge(customerCredits);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerCredits> getAllCustomerCredits() {

    List<CustomerCredits> customerCredits = new ArrayList<>();
    try {
      customerCredits =
          this.entityManager.createQuery("from CustomerCredits order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCustomerCredits ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCredits;
  }

  @Override
  public CustomerCredits getCustomerCreditsById(final Long id) {

    CustomerCredits customerCredits = null;
    try {
      customerCredits =
          (CustomerCredits)
              this.entityManager
                  .createQuery("FROM CustomerCredits where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCustomerCreditsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCredits;
  }

  @Override
  public CustomerCredits getCustomerCreditsByCustomerId(final Long customerId) {

    CustomerCredits customerCredits = null;
    try {
      customerCredits =
          (CustomerCredits)
              this.entityManager
                  .createQuery("from CustomerCredits where customer.id=?1")
                  .setParameter(1, customerId)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("getCustomerCreditsByCustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCredits;
  }

  @Override
  public Credits getAllCreditsByOrgidCustomer(final long orgid) {

    Credits credits = null;
    try {
      credits =
          (Credits)
              entityManager
                  .createQuery(" from Credits where organisation.id=?1")
                  .setParameter(1, orgid)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCustomerCreditsByCustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return credits;
  }
}
