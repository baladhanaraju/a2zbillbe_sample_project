package com.a2zbill.dao.impl;

import com.a2zbill.dao.CancelHistoryDao;
import com.a2zbill.domain.CancelHistory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CancelHistoryDaoImpl implements CancelHistoryDao {

  private static final Logger logger = LoggerFactory.getLogger(CancelHistoryDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(CancelHistory cancelHistory) {
    try {
      entityManager.persist(cancelHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(CancelHistory cancelHistory) {
    try {
      entityManager.merge(cancelHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CancelHistory> getAllCancelHistory() {

    List<CancelHistory> cancelHistory = new ArrayList<>();
    try {
      cancelHistory = entityManager.createQuery("from CancelHistory order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCancelHistory ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cancelHistory;
  }

  @Override
  public CancelHistory getCancelHistoryById(final Long id) {

    CancelHistory cancelHistory = null;
    try {
      cancelHistory =
          (CancelHistory)
              entityManager
                  .createQuery("FROM CancelHistory where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCancelHistoryById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cancelHistory;
  }
}
