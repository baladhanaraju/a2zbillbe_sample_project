package com.a2zbill.dao.impl;

import com.a2zbill.dao.StoreTypeDao;
import com.a2zbill.domain.StoreType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("unchecked")
public class StoreTypeDaoImpl implements StoreTypeDao {

  private static final Logger logger = LoggerFactory.getLogger(StoreTypeDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(StoreType storeType) {
    try {
      entityManager.persist(storeType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(StoreType storeType) {
    try {
      entityManager.merge(storeType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public List<StoreType> getAllStoreTypeDetails() {

    List<StoreType> storeType = new ArrayList<>();
    try {
      storeType = this.entityManager.createQuery("from StoreType order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllStoreTypeDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return storeType;
  }

  @Override
  public StoreType storeTypegetDetalisById(long id) {

    StoreType storeType = null;
    try {
      storeType =
          (StoreType)
              entityManager
                  .createQuery("from StoreType where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("storeTypegetDetalisById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return storeType;
  }
}
