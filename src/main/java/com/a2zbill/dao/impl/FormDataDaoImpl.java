package com.a2zbill.dao.impl;

import com.a2zbill.dao.FormDataDao;
import com.a2zbill.domain.FormData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FormDataDaoImpl implements FormDataDao {

  private static final Logger logger = LoggerFactory.getLogger(FormDataDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final FormData formData) {
    try {
      entityManager.persist(formData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final FormData formData) {
    try {
      entityManager.merge(formData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<FormData> getAllFormData() {

    List<FormData> formData = new ArrayList<>();
    try {
      formData = entityManager.createQuery("form FormData order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllFormData ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formData;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<FormData> getFormDataByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {

    List<FormData> formData = new ArrayList<>();
    try {
      formData =
          entityManager
              .createQuery(
                  " from FormData where  createdDate>=?1 and createdDate<=?2 and organisation.id=?3 and branch.id=?4")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getFormDataByBranchAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formData;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<FormData> getFormDataByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {

    List<FormData> formData = new ArrayList<>();
    try {
      formData =
          entityManager
              .createQuery(
                  " from FormData where  createdDate>=?1 and createdDate<=?2 and organisation.id=?3")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getFormDataByOrgAndBranchisNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formData;
  }

  @Override
  public Long getFormDataByOrgandBranch(final long orgId, final long branchId) {

    Long formData = null;
    try {
      formData =
          (Long)
              entityManager
                  .createQuery(
                      " from FormData where (branch.id=?1 or branch.id is null) and organisation.id=?2")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFormDataByOrgandBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formData;
  }
}
