package com.a2zbill.dao.impl;

import com.a2zbill.dao.RulesDao;
import com.a2zbill.domain.Rules;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RulesDaoImpl implements RulesDao {

  private static final Logger logger = LoggerFactory.getLogger(RulesDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Rules rules) {
    try {
      entityManager.persist(rules);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Rules rules) {
    try {
      entityManager.remove(rules);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(Rules rules) {
    try {
      entityManager.merge(rules);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Rules> getAllRulesDetails() {

    List<Rules> list = new ArrayList<>();
    try {
      list = entityManager.createQuery("from Rules order by rule_id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllFormData ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public Rules getRuleDetailsByRuleId(final long ruleId) {

    Rules ruleDetails = null;
    try {
      ruleDetails =
          (Rules)
              entityManager
                  .createQuery("FROM Rules c where c.ruleId =?1")
                  .setParameter(1, ruleId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRuleDetailsByRuleId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return ruleDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Rules> getRulesDetailsByOrgAndBranch(final long orgId, final long branchId) {

    List<Rules> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from Rules where orgId.id =?1 and branchId.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getRuleDetailsByRuleId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Rules> getRulesDetailsByOrg(final long orgId) {

    List<Rules> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from Rules where orgId.id =?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getRuleDetailsByRuleId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }
}
