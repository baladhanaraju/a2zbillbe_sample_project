package com.a2zbill.dao.impl;

import com.a2zbill.dao.DocumentsDataDao;
import com.a2zbill.domain.DocumentsData;
import com.a2zbill.domain.ProductCategory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentsDataDaoImpl implements DocumentsDataDao {

  private static final Logger logger = LoggerFactory.getLogger(DocumentsDataDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void saveDocumentData(DocumentsData documentsData) {
    try {
      entityManager.persist(documentsData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getCategoryDetailsByCategoryType(
      final long orgId, final long branchId) {

    List<ProductCategory> categoryProducts = new ArrayList<>();
    try {
      categoryProducts =
          entityManager
              .createQuery(
                  "from ProductCategory where categoryType.categoryName=?1 and organisation.id=?2 and branch.id=?3")
              .setParameter(1, "Document category")
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCategoryDetailsByCategoryType ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return categoryProducts;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getCategoryDetailsByCategoryTypeByOrg(final long orgId) {

    List<ProductCategory> categoryProducts = new ArrayList<>();
    try {
      categoryProducts =
          entityManager
              .createQuery(
                  "from ProductCategory where categoryType.categoryName=?1 and organisation.id=?2")
              .setParameter(1, "Document category")
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCategoryDetailsByCategoryTypeByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return categoryProducts;
  }
}
