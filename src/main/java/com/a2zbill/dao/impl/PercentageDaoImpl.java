package com.a2zbill.dao.impl;

import com.a2zbill.dao.PercentageDao;
import com.a2zbill.domain.Percentage;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PercentageDaoImpl implements PercentageDao {

  private static final Logger logger = LoggerFactory.getLogger(PercentageDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public List<Percentage> getAllPercentages() {

    List<Percentage> percentageDetails = new ArrayList<>();
    try {
      percentageDetails =
          (List<Percentage>)
              entityManager.createQuery("from Percentage order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllPercentages ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return percentageDetails;
  }
}
