package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductRewardPointsDao;
import com.a2zbill.domain.ProductRewardPoint;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductRewardPointsDaoImpl implements ProductRewardPointsDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductRewardPointsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(ProductRewardPoint productRewardPoint) {
    try {
      entityManager.persist(productRewardPoint);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(ProductRewardPoint productRewardPoint) {
    try {
      entityManager.merge(productRewardPoint);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductRewardPoint> getAllProductRewardPoints() {

    List<ProductRewardPoint> productRewardPoint = new ArrayList<>();
    try {
      productRewardPoint =
          entityManager.createQuery("from ProductRewardPoint order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductRewardPoints ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRewardPoint;
  }

  @Override
  public ProductRewardPoint getAllProductRewardPointsByproductId(
      final long productId, final long orgId, final long branchId) {

    ProductRewardPoint productRewardPoint = null;
    try {
      productRewardPoint =
          (ProductRewardPoint)
              entityManager
                  .createQuery(
                      "from ProductRewardPoint where product.id = ?1 and orgId.id=?2 and branchId.id=?3")
                  .setParameter(1, productId)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllProductRewardPointsByproductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRewardPoint;
  }

  @Override
  public ProductRewardPoint getProductRewardPointsByproductId(final long productId) {

    ProductRewardPoint productRewardPoint = null;
    try {
      productRewardPoint =
          (ProductRewardPoint)
              entityManager
                  .createQuery("from ProductRewardPoint where product.id=?1")
                  .setParameter(1, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductRewardPointsByproductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRewardPoint;
  }

  @Override
  public ProductRewardPoint getAllProductRewardPointsByproductIdOrgId(
      final long productId, final long orgId) {

    ProductRewardPoint productRewardPoint = null;
    try {
      productRewardPoint =
          (ProductRewardPoint)
              entityManager
                  .createQuery("from ProductRewardPoint where product.id = ?1 and orgId.id=?2")
                  .setParameter(1, productId)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllProductRewardPointsByproductIdOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRewardPoint;
  }
}
