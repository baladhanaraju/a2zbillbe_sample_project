package com.a2zbill.dao.impl;

import com.a2zbill.dao.SyncDao;
import com.a2zbill.domain.Sync;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SyncDaoImpl implements SyncDao {

  private static final Logger logger = LoggerFactory.getLogger(SyncDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(Sync sync) {
    try {
      entityManager.persist(sync);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(Sync sync) {
    try {
      entityManager.merge(sync);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Sync> getAllSyncDetails() {

    List<Sync> sync = new ArrayList<>();
    try {
      sync = entityManager.createQuery("from Sync order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSyncDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return sync;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Sync> getAllSyncDetailsByOrgBranchId(final long orgId, final long branchId) {

    List<Sync> syncDetails = new ArrayList<>();
    try {
      syncDetails =
          entityManager
              .createQuery("from Sync where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSyncDetailsByOrgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return syncDetails;
  }

  @Override
  public Sync getSyncDetailsByOrgBranchIdAndSyncUrl(
      final long orgId, final long branchId, final String syncUrl) {

    Sync syncDetails = null;
    try {
      syncDetails =
          (Sync)
              entityManager
                  .createQuery("from Sync where organisation.id=?1 and branch.id=?2 and syncUrl=?3")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .setParameter(3, syncUrl)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getSyncDetailsByOrgBranchIdAndSyncUrl ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return syncDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Sync> getAllSyncDetailsByOrg(final long orgId) {

    List<Sync> syncDetails = new ArrayList<>();
    try {
      syncDetails =
          entityManager
              .createQuery("from Sync where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSyncDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return syncDetails;
  }
}
