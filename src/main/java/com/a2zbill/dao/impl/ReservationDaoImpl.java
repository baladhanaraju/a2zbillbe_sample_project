package com.a2zbill.dao.impl;

import com.a2zbill.dao.ReservationDao;
import com.a2zbill.domain.Reservation;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReservationDaoImpl implements ReservationDao {

  private static final Logger logger = LoggerFactory.getLogger(ReservationDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Reservation reservation) {
    try {
      entityManager.persist(reservation);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Reservation reservation) {
    try {
      entityManager.remove(reservation);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public void update(Reservation reservation) {
    try {
      entityManager
          .createQuery("update Reservation set allocatedTable=?1 where customer.id=?2")
          .setParameter(1, reservation.getAllocatedTable())
          .setParameter(2, reservation.getCustomer().getCustomerId())
          .executeUpdate();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getAllReservationDetails() {

    List<Reservation> list = new ArrayList<>();
    try {
      list = entityManager.createQuery("from Reservation order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReservationDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public Reservation getReservationDetailsByid(final long id) {

    Reservation reservationDetails = null;
    try {
      reservationDetails =
          (Reservation)
              entityManager
                  .createQuery("FROM Reservation r  where r.id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByid ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservationDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByStatus(final String status) {

    List<Reservation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from Reservation where status=?1")
              .setParameter(1, status)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByDateAndTime(
      final Date date,
      final long reservedtable,
      final String startTime,
      final long orgId,
      final long branchId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          entityManager
              .createQuery(
                  "from Reservation where reserveDate=?1 and reserveTable.id=?2 and startTime>=?3  and organisation.id=?4 and branch.id=?5")
              .setParameter(1, date)
              .setParameter(2, reservedtable)
              .setParameter(3, startTime)
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByDateAndTime ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByDateAndTimeAndOrg(
      final Date date, final long reservedtable, final String startTime, long orgId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          entityManager
              .createQuery(
                  "from Reservation where reserveDate=?1 and reserveTable.id=?2 and startTime>=?3  and organisation.id=?4")
              .setParameter(1, date)
              .setParameter(2, reservedtable)
              .setParameter(3, startTime)
              .setParameter(4, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByDateAndTimeAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByOrgAndBranch(
      final long orgId, final long branchId) {

    List<Reservation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from Reservation where organisation.id=?1 and (branch.id=?2 or branch.id=null)")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByOrg(final long orgId) {

    List<Reservation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from Reservation where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrgAndBranch(
      final Date currentdate,
      final long reservetable,
      final String time,
      final long orgId,
      final long branchId) {

    List<Reservation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from Reservation where reserveDate=?1 and reserveTable.id=?2 and startTime=?3 and organisation.id=?4 and branch.id=?5")
              .setParameter(1, currentdate)
              .setParameter(2, reservetable)
              .setParameter(3, time)
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByStarttimeOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrg(
      final Date currentdate, final long reservetable, final String time, final long orgId) {

    List<Reservation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from Reservation where reserveDate=?1 and reserveTable.id=?2 and time=?3 and organisation.id=?4")
              .setParameter(1, currentdate)
              .setParameter(2, reservetable)
              .setParameter(3, time)
              .setParameter(4, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByStarttimeOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getAllReservationsByDate(Date date, long orgId, long branchId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          entityManager
              .createQuery(
                  "from Reservation where reserveDate=?1 and organisation.id=?2 and branch.id=?3 and status=?4")
              .setParameter(1, date)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .setParameter(4, "Open")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReservationsByDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDeatilsByTable(
      Date date, long reservetable, long orgId, long branchId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          entityManager
              .createQuery(
                  "from Reservation where reserveDate=?1 and organisation.id=?2 and branch.id=?3 and status=?4 and reserveTable.id=?5")
              .setParameter(1, date)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .setParameter(4, "Open")
              .setParameter(5, reservetable)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDeatilsByTable ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDeatilsByTableAndOrg(
      Date date, long reservetable, long orgId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          entityManager
              .createQuery(
                  "from Reservation where reserveDate=?1 and organisation.id=?2 and status=?3 and reserveTable.id=?4")
              .setParameter(1, date)
              .setParameter(2, orgId)
              .setParameter(3, "Open")
              .setParameter(4, reservetable)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDeatilsByTableAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @Override
  public Reservation getReservationByTable(
      Date date, long reservetable, long orgId, long branchId) {

    Reservation reservations = null;
    try {
      reservations =
          (Reservation)
              entityManager
                  .createQuery(
                      "from Reservation where reserveDate=?1 and organisation.id=?2 and branch.id=?3 and status=?4 and reserveTable.id=?5")
                  .setParameter(1, date)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .setParameter(4, "Open")
                  .setParameter(5, reservetable)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationdeatilsByTable ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @Override
  public Reservation getReservationByTableAndOrg(Date date, long reservetable, long orgId) {

    Reservation reservations = null;
    try {
      reservations =
          (Reservation)
              entityManager
                  .createQuery(
                      "from Reservation where reserveDate=?1 and organisation.id=?2 and  status=?3 and reserveTable.id=?4")
                  .setParameter(1, date)
                  .setParameter(2, orgId)
                  .setParameter(3, "Open")
                  .setParameter(4, reservetable)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationdeatilsByTableAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @Override
  public Reservation getReservationdeatilsByTableAndcustomerId(
      long customer, Date date, long reservetable, long orgId, long branchId) {

    Reservation reservations = null;
    try {
      reservations =
          (Reservation)
              entityManager
                  .createQuery(
                      "from Reservation where customer.customerId=?1 and reserveDate=?2 and organisation.id=?3 and branch.id=?4 and status=?5 and reserveTable.id=?6")
                  .setParameter(1, customer)
                  .setParameter(2, date)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .setParameter(5, "Open")
                  .setParameter(6, reservetable)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationdeatilsByTableAndcustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @Override
  public Reservation getReservationdeatilsByTableAndOrgAndcustomerId(
      long customer, Date date, long reservetable, long orgId) {

    Reservation reservations = null;
    try {
      reservations =
          (Reservation)
              entityManager
                  .createQuery(
                      "from Reservation where customer.customerId=?1 and reserveDate=?2 and organisation.id=?3 and  status=?4 and reserveTable.id=?5")
                  .setParameter(1, customer)
                  .setParameter(2, date)
                  .setParameter(3, orgId)
                  .setParameter(4, "Open")
                  .setParameter(5, reservetable)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationdeatilsByTableAndOrgAndcustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrgAndBranch(
      String startTime, long orgId, long branchId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          (List<Reservation>)
              entityManager
                  .createQuery(
                      "from Rservation where startTime=?1 and organisation.id=?2 and branch.id=?3")
                  .setParameter(1, startTime)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByStarttimeOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByStarttimeOrg(String startTime, long orgId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          (List<Reservation>)
              entityManager
                  .createQuery("from Rservation where startTime=?1 and organisation.id=?2")
                  .setParameter(1, startTime)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByStarttimeOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @Override
  public Reservation getReservationDetailsByreserveTableOrgAndBranch(
      Date date, long reservetable, String startTime, long orgId, long branchId) {

    Reservation reservations = null;
    try {
      reservations =
          (Reservation)
              entityManager
                  .createQuery(
                      "from Reservation where reserveTable.id=?1 and startTime=?2 and organisation.id=?3 and branch.id=?4")
                  .setParameter(1, reservetable)
                  .setParameter(2, startTime)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByreserveTableOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @Override
  public Reservation getReservationDetailsByreserveTableOrg(
      long reservetable, String startTime, long orgId) {

    Reservation reservations = null;
    try {
      reservations =
          (Reservation)
              entityManager
                  .createQuery(
                      "from Reservation where reserveTable.id=?1 and startTime=?2 and organisation.id=?3")
                  .setParameter(1, reservetable)
                  .setParameter(2, startTime)
                  .setParameter(3, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByreserveTableOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsByreserveTableAndOrgBranch(
      Date date, long reservetable, String startTime, long orgId, long branchId) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          entityManager
              .createQuery(
                  "from Reservation where reserveTable.id=?1 and startTime=?2 and organisation.id=?3 and branch.id=?4")
              .setParameter(1, reservetable)
              .setParameter(2, startTime)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsByreserveTableAndOrgBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation>
      getReservationDetailsByReservedTableAndOrgAndBranchAndBetweenTwoTimesAndDate(
          long reservedTable,
          Date reserveDate,
          long orgId,
          long branchId,
          String lowestTime,
          String highestTime) {

    List<Reservation> reservations = new ArrayList<>();
    try {
      reservations =
          entityManager
              .createQuery(
                  "from Reservation where reserveTable.id=?1 and organisation.id=?2 and branch.id=?3 and startTime between ?4 and ?5 and reserveDate =?6 and status=?7")
              .setParameter(1, reservedTable)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .setParameter(4, lowestTime)
              .setParameter(5, highestTime)
              .setParameter(6, reserveDate)
              .setParameter(7, "Open")
              .getResultList();
    } catch (final Exception ex) {
      logger.error(
          "getReservationDetailsByReservedTableAndOrgAndBranchAndBetweenTwoTimesAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reservations;
  }

  @Override
  public BigInteger getCountOfReservationByOrganisationIdAndCurrentMonth(long orgId) {

    BigInteger count = null;
    try {
      count =
          (BigInteger)
              entityManager
                  .createNativeQuery(
                      "select count(*) from reservation.reservation where date_part('month',reserve_date) = date_part('month',current_date)  and date_part('year',reserve_date) = date_part('year',current_date) and org_id =?1")
                  .setParameter(1, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCountOfReservationByOrganisationIdAndCurrentMonth ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return count;
  }

  @Override
  public BigInteger getCountOfReservationByOrganisationIdAndCurrentMonthBranchId(
      long orgId, long branchId) {

    BigInteger count = null;
    try {
      count =
          (BigInteger)
              entityManager
                  .createNativeQuery(
                      "select count(*) from reservation.reservation where date_part('month',reserve_date) = date_part('month',current_date) and date_part('year',reserve_date) = date_part('year',current_date) and org_id =?1 and branch_id =?2")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCountOfReservationByOrganisationIdAndCurrentMonthBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return count;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getAllReservationsFromCurrentDate(
      long orgId, long branchId, Date date, String startTime) {

    List<Reservation> list = new ArrayList<>();
    try {
      list =
          (List<Reservation>)
              entityManager
                  .createQuery(
                      "from Reservation where organisation.id=?1 and branch.id=?2 and reserveDate>=?3")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .setParameter(3, date)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReservationsFromCurrentDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public Reservation getReservationDetailsBySlotTimeBranch(
      Date date, String startTime, long reservetable, long orgId, long branchId) {

    Reservation list = null;
    try {
      list =
          (Reservation)
              entityManager
                  .createQuery(
                      "from Reservation where reserveDate=?1 and startTime=?2 and reserveTable.id=?3  and organisation.id=?4 and branch.id=?5")
                  .setParameter(1, date)
                  .setParameter(2, startTime)
                  .setParameter(3, reservetable)
                  .setParameter(4, orgId)
                  .setParameter(5, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsBySlotTimeBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Reservation> getReservationDetailsBySlotTimeorg(
      String startTime, long reservetable, long orgId) {

    List<Reservation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery(
                  "from Reservation where  startTime=?1 and reserveTable.id=?2  and organisation.id=?4")
              .setParameter(1, startTime)
              .setParameter(2, reservetable)
              .setParameter(3, "Open")
              .setParameter(4, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReservationDetailsBySlotTimeorg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }
}
