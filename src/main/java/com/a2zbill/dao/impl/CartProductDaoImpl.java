package com.a2zbill.dao.impl;

import com.a2zbill.dao.CartProductDao;
import com.a2zbill.domain.CartDetail;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CartProductDaoImpl implements CartProductDao {

  private static final Logger logger = LoggerFactory.getLogger(CartProductDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final CartDetail cartDetails) {
    try {
      entityManager.persist(cartDetails);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      Object result = this.entityManager.find(CartDetail.class, id);
      if (result != null) {
        this.entityManager.remove(result);
        return true;
      }
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return false;
  }

  @Override
  public boolean update(CartDetail cartDetails) {
    try {
      entityManager.merge(cartDetails);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public List<CartDetail> getAllCounterDetails() {
    return new ArrayList<>();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsByCartId(final long cartId) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createQuery("from CartDetail where cartDetails.cartId = ?1")
              .setParameter(1, cartId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCartId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsByCartIdAndDeptIdAndCartStatus(
      final Long cartId, final String cartStatus, final Long deptId) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createQuery(
                  "from CartDetail as cd where cd.cartId in (select c.cartId from Cart as c where cartStatus = ?1) and department.id = ?2 ")
              .setParameter(1, cartStatus)
              .setParameter(2, deptId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCartIdAndDeptIdAndCartStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsDeptIdAndCartStatus() {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery("from CartDetail where  Cart.cartStatus = 'Open' and flow.id = 1")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsDeptIdAndCartStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsByCounterIdAndFlowId() {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createQuery(
                  "select productName, counterDetails.id, quantity, hsnCode from CartDetail where cartDetails.cartStatus like 'Open' and "
                      + "cartDetails.cartId = (select cartDetails.cartId from OrderFlow where flow.id = 1) order by time")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCounterIdAndFlowId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsByCounterIdAndFlowId(
      final Long counterId, final Long flowId) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createQuery(
                  "from CartDetail where  cartDetails.id = ?1 and "
                      + "flow.id=(select id from Flow where flowType.id = ?2 and flowName like 'kitchen section')")
              .setParameter(1, counterId)
              .setParameter(2, flowId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCounterIdAndFlowId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsByFlowId() {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createQuery(
                  "select productName,SUM(quantity) from CartDetail where cartDetails.cartStatus like 'Open' and "
                      + "cartDetails.cartId = (select cartDetails.cartId from OrderFlow where flow.id = 1) group by productName")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByFlowId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public CartDetail getCartDetailsByProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode) {

    CartDetail cartDetails = null;
    try {
      cartDetails =
          (CartDetail)
              this.entityManager
                  .createQuery(
                      " from CartDetail where  cartDetails.cartStatus like 'Open' and counterDetails.id = ?1 and productName=?2 and hsnCode = ?3")
                  .setParameter(1, counterId)
                  .setParameter(2, productName)
                  .setParameter(3, hsnCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByProductNameAndHsnCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public CartDetail getCartDetailsByCounterIdProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode) {

    CartDetail cartDetails = null;
    try {
      cartDetails =
          (CartDetail)
              entityManager
                  .createQuery(
                      " from CartDetail where  cartDetails.cartStatus like 'Open' and counterDetails.id = ?1 and productName = ?2 and hsnCode = ?3 ")
                  .setParameter(1, counterId)
                  .setParameter(2, productName)
                  .setParameter(3, hsnCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCounterIdProductNameAndHsnCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsGroupByProductName(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "select product_name, sum(quantity),product_price,org_id,branch_id from product.cart_product where  date>= ?1 and date<= ?2 group by product_name,product_price,org_id,branch_id Having org_id = ?3 and branch_id = ?4")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsGroupByProductName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getproductQuantityByBranchIdAndDate(
      final long orgId, final Long branchId, final Date date) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "SELECT product_name, sum(quantity) FROM prouct.cart_product WHERE branch_id =?1 and date=?2 GROUP BY product_name,org_id,branch_id")
              .setParameter(1, branchId)
              .setParameter(2, date)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductQuantityByBranchIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsGroupByProductNameifBrnachNull(
      final Date fromDate, final Date toDate, final long orgId) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "select product_name, sum(quantity),product_price,org_id from product.cart_product where date between date = ?1 and date = ?2 group by product_name,product_price,org_id Having org_id = ?3")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsGroupByProductNameifBrnachNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getproductQuantityByBranchAndOrgIdAndDate(
      final long orgId, final long branchId, final Date fromDate, final Date toDate) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "SELECT product_name, sum(quantity) FROM product.cart_product WHERE org_id = ?1 and branch_id = ?2  and  date>= ?3 and date<= ?4 GROUP BY product_name,org_id,branch_id")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, fromDate)
              .setParameter(4, toDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductQuantityByBranchAndOrgIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getproductQuantityByOrgIdAndDate(
      final long orgId, final Date fromDate, final Date toDate) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "SELECT product_name, sum(quantity) FROM product.cart_product WHERE org_id = ?1 and date>= ?2 and date<= ?3 GROUP BY product_name,org_id,branch_id")
              .setParameter(1, orgId)
              .setParameter(2, fromDate)
              .setParameter(3, toDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductQuantityByBranchAndOrgIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getIngredientWiseProductQuantitySumByOrgIdAndDate(
      long orgId, long branchId, Date date) {

    List<Object[]> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "select product_name, quantity from product.cart_product where date = ?1 and org_id= ?2 and branch_id= ?3")
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .setParameter(1, date)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getIngredientWiseProductQuantitySumByOrgIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BigDecimal> getIngredientWiseProductQuantitySumByOrgIdBranchIdAndDate(
      long orgId, long branchId, Date date) {

    List<BigDecimal> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createNativeQuery(
                  "select quantity from product.cart_product where date = ?1 and org_id= ?2 and branch_id= ?3")
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .setParameter(1, date)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getIngredientWiseProductQuantitySumByOrgIdBranchIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartDetailsByProductName(
      String productName, long orgId, long branchId) {

    List<CartDetail> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          this.entityManager
              .createQuery(
                  "from CartDetail where productName=?1 and organisation.id=?2 and branch.id=?3")
              .setParameter(1, productName)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByProductName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public CartDetail getCartDetailByCartId(long cartId) {
    CartDetail cartDetails = null;
    try {

      cartDetails =
          (CartDetail)
              this.entityManager
                  .createQuery("from CartDetail where Id=?1")
                  .setParameter(1, cartId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCartDetailByCartId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getCartDetailsByCartDetailsId(long cartId) {
    List<Object[]> object = new ArrayList<>();
    try {
      object =
          entityManager
              .createQuery(
                  "select Id,productName,quantity,hsnCode,productPrice,discountAmount,cgstPercentage,sgstPercentage,IgstPercentage,time,totalAmount,taxableAmount from CartDetail where cartDetails.cartId = ?1")
              .setParameter(1, cartId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(" getCartproducts", e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return object;
  }
}
