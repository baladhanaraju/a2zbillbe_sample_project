package com.a2zbill.dao.impl;

import com.a2zbill.dao.CustomerVendorTokenDao;
import com.a2zbill.domain.CustomerVendorToken;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerVendorTokenDaoImpl implements CustomerVendorTokenDao {

  private static final Logger logger = LoggerFactory.getLogger(CustomerVendorTokenDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(CustomerVendorToken customerVendorToken) {
    try {
      entityManager.persist(customerVendorToken);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerVendorToken> getAllCustomerVendorToken() {

    List<CustomerVendorToken> customerVendorTokenList = new ArrayList<>();
    try {
      customerVendorTokenList =
          entityManager.createQuery("from CustomerVendorToken order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCustomerVendorToken ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerVendorTokenList;
  }

  @Override
  public CustomerVendorToken getTokenAndRootNameBymobileNumber(
      final String mobileNumber, final long orgId) {

    CustomerVendorToken details = null;
    try {
      details =
          (CustomerVendorToken)
              entityManager
                  .createQuery(
                      "from CustomerVendorToken where mobileNumber = ?1 and organisation.id = ?2")
                  .setParameter(1, mobileNumber)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getTokenAndRootNameBymobileNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return details;
  }
}
