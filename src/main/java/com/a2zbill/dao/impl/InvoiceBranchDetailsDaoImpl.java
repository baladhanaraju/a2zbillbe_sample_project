package com.a2zbill.dao.impl;

import com.a2zbill.dao.InvoiceBranchDetailsDao;
import com.a2zbill.domain.InvoiceBranchDetails;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InvoiceBranchDetailsDaoImpl implements InvoiceBranchDetailsDao {

  private static final Logger logger = LoggerFactory.getLogger(InvoiceBranchDetailsDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(InvoiceBranchDetails invoiceBranchDetails) {
    try {
      entityManager.persist(invoiceBranchDetails);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }
}
