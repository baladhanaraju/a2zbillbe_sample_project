package com.a2zbill.dao.impl;

import com.a2zbill.dao.RecipeIngredientsDao;
import com.a2zbill.domain.RecipeIngredients;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RecipeIngredientsDaoImpl implements RecipeIngredientsDao {

  private static final Logger logger = LoggerFactory.getLogger(RecipeIngredientsDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(RecipeIngredients recipeIngredients) {
    try {
      entityManager.persist(recipeIngredients);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(RecipeIngredients recipeIngredients) {
    try {
      entityManager.merge(recipeIngredients);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeIngredients> getAllRecipeIngredients() {

    List<RecipeIngredients> recipeIngredients = new ArrayList<>();
    try {
      recipeIngredients =
          entityManager.createQuery("from RecipeIngredients order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeIngredients ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeIngredients;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByOrgAndBranch(long orgId, long branchId) {

    List<RecipeIngredients> recipeIngredients = new ArrayList<>();
    try {
      recipeIngredients =
          entityManager
              .createQuery("from RecipeIngredients where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeIngredientsByOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeIngredients;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByOrg(long orgId) {

    List<RecipeIngredients> recipeIngredients = new ArrayList<>();
    try {
      recipeIngredients =
          entityManager
              .createQuery("from RecipeIngredients where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeIngredientsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeIngredients;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByRecipeId(long recipeId) {

    List<RecipeIngredients> recipeIngredients = new ArrayList<>();
    try {
      recipeIngredients =
          entityManager
              .createQuery("from RecipeIngredients where productRecipe.id=?1")
              .setParameter(1, recipeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeIngredientsByRecipeId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeIngredients;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeIngredients> getAllRecipeIngredientsByProductId(long productId) {

    List<RecipeIngredients> recipeIngredients = new ArrayList<>();
    try {
      recipeIngredients =
          entityManager
              .createQuery("from RecipeIngredients where product.id=?1")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeIngredientsByProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeIngredients;
  }

  @Override
  public RecipeIngredients getRecipeIngredientsByRecipeIdAndProductId(
      long recipeId, long productId) {

    RecipeIngredients recipeIngredients = null;
    try {
      recipeIngredients =
          (RecipeIngredients)
              entityManager
                  .createQuery("from RecipeIngredients where productRecipe.id=?1 and product.id=?2")
                  .setParameter(1, recipeId)
                  .setParameter(2, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRecipeIngredientsByRecipeIdAndProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeIngredients;
  }
}
