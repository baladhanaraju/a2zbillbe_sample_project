package com.a2zbill.dao.impl;

import com.a2zbill.dao.FormTypeDao;
import com.a2zbill.domain.FormType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FormTypeDaoImpl implements FormTypeDao {

  private static final Logger logger = LoggerFactory.getLogger(FormTypeDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final FormType formType) {
    try {
      entityManager.persist(formType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final FormType formType) {
    try {
      entityManager.merge(formType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<FormType> getAllFormTypes() {

    List<FormType> formType = new ArrayList<>();
    try {
      formType = entityManager.createQuery("FROM FormType order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllFormTypes ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formType;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Map<String, Object> getFormTypedetailsById(final long formid) {

    Map<String, Object> formType = null;
    try {
      formType =
          (Map<String, Object>)
              this.entityManager
                  .createQuery(" from FormType where id=?1")
                  .setParameter(1, formid)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFormTypedetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formType;
  }

  @Override
  public FormType getFormTypeById(final long formid) {

    FormType formType = null;
    try {
      formType =
          (FormType)
              this.entityManager
                  .createQuery("from FormType where id=?1")
                  .setParameter(1, formid)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFormTypeById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return formType;
  }
}
