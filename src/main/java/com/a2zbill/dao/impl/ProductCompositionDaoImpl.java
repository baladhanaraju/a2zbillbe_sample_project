package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductCompositionDao;
import com.a2zbill.domain.ProductComposition;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductCompositionDaoImpl implements ProductCompositionDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductCompositionDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final ProductComposition productComposition) {
    try {
      entityManager.persist(productComposition);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(final ProductComposition productComposition) {
    try {
      entityManager.merge(productComposition);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductComposition> getAllProductComposition() {

    List<ProductComposition> productComposition = new ArrayList<>();
    try {
      productComposition =
          entityManager.createQuery("from ProductComposition order by id").getResultList();

    } catch (final Exception ex) {
      logger.error(" getAllProductComposition", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productComposition;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductComposition> getProductCompositionbyPackageFlag(final Boolean packageFlag) {

    List<ProductComposition> productComposition = new ArrayList<>();
    try {
      productComposition =
          entityManager
              .createQuery("from ProductComposition where packageFlag=?1")
              .setParameter(1, packageFlag)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCompositionbyPackageFlag", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productComposition;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductComposition> getAllProductCompositionByProductId(final long productId) {

    List<ProductComposition> productComposition = new ArrayList<>();
    try {
      productComposition =
          entityManager
              .createQuery("from ProductComposition where productId.id=?1")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getAllProductCompositionByProductId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productComposition;
  }
}
