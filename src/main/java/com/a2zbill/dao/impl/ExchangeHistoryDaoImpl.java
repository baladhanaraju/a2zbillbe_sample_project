package com.a2zbill.dao.impl;

import com.a2zbill.dao.ExchangeHistoryDao;
import com.a2zbill.domain.ExchangeHistory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ExchangeHistoryDaoImpl implements ExchangeHistoryDao {

  private static final Logger logger = LoggerFactory.getLogger(ExchangeHistoryDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(ExchangeHistory exchangeHistory) {
    try {
      entityManager.persist(exchangeHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(ExchangeHistory exchangeHistory) {
    try {
      entityManager.merge(exchangeHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExchangeHistory> getAllExchangeHistory() {

    List<ExchangeHistory> exchangeHistory = new ArrayList<>();
    try {
      exchangeHistory =
          entityManager.createQuery("from ExchangeHistory order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllExchangeHistory ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return exchangeHistory;
  }

  @Override
  public ExchangeHistory getExchangeHistoryById(final Long id) {

    ExchangeHistory exchangeHistory = null;
    try {
      exchangeHistory =
          (ExchangeHistory)
              entityManager
                  .createQuery("FROM ExchangeHistory where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getExchangeHistoryById ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return exchangeHistory;
  }
}
