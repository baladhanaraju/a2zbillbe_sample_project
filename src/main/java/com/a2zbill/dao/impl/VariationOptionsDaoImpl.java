package com.a2zbill.dao.impl;

import com.a2zbill.dao.VariationOptionsDao;
import com.a2zbill.domain.VariationOptions;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VariationOptionsDaoImpl implements VariationOptionsDao {

  private static final Logger logger = LoggerFactory.getLogger(VariationOptionsDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public List<VariationOptions> getAllVariationOptions() {

    List<VariationOptions> variationOptionsDetails = new ArrayList<>();
    try {
      variationOptionsDetails =
          entityManager.createQuery("FROM VariationOptions order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllVariationOptions ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationOptionsDetails;
  }

  @Override
  public VariationOptions getVariationOptionsById(long id) {

    VariationOptions variationOption = null;
    try {
      variationOption =
          (VariationOptions)
              entityManager
                  .createQuery("From VariationOptions c where c.id = ?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getVariationOptionsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationOption;
  }

  @Override
  public VariationOptions getVariationOptionsByName(String name) {

    VariationOptions variationOption = null;
    try {
      variationOption =
          (VariationOptions)
              entityManager
                  .createQuery("From VariationOptions c where c.optionName = ?1")
                  .setParameter(1, name)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getVariationOptionsByName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationOption;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<VariationOptions> getVariationOptionsByType(long typeId) {

    List<VariationOptions> variationOptionsDetails = new ArrayList<>();
    try {
      variationOptionsDetails =
          entityManager
              .createQuery("FROM VariationOptions where variationTypes.id=?1")
              .setParameter(1, typeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getVariationOptionsByType ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return variationOptionsDetails;
  }
}
