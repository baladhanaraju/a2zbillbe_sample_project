package com.a2zbill.dao.impl;

import com.a2zbill.dao.AlteredBillingsDao;
import com.a2zbill.domain.AlteredBillings;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AlteredBillingsDaoImpl implements AlteredBillingsDao {

  private static final Logger logger = LoggerFactory.getLogger(AlteredBillingsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(AlteredBillings alteredbillings) {
    try {
      entityManager.persist(alteredbillings);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public Boolean update(AlteredBillings alteredbillings) {
    try {
      entityManager.merge(alteredbillings);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public Boolean delete(AlteredBillings alteredbillings) {
    try {
      entityManager.remove(alteredbillings);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public List<AlteredBillings> getAllAlteredBillings() {
    return new ArrayList<>();
  }

  @Override
  public AlteredBillings getAlteredBillingsbyid(final long id) {

    AlteredBillings alteredbillings = null;
    try {
      alteredbillings =
          (AlteredBillings)
              entityManager
                  .createQuery("FROM AlteredBillings  where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("AlteredBillingsDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return alteredbillings;
  }
}
