package com.a2zbill.dao.impl;

import com.a2zbill.dao.StoreTemplateDao;
import com.a2zbill.domain.StoreTemplate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StoreTemplateDaoImpl implements StoreTemplateDao {

  private static final Logger logger = LoggerFactory.getLogger(StoreTemplateDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(StoreTemplate storeTemplate) {
    try {
      entityManager.persist(storeTemplate);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(StoreTemplate storeTemplate) {
    try {
      entityManager.merge(storeTemplate);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public StoreTemplate getStoreTemplateById(final Long id) {

    StoreTemplate storeTemplate = null;
    try {
      storeTemplate =
          (StoreTemplate)
              entityManager
                  .createQuery("from StoreTemplate where id=?1 ")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getStoreTemplateById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return storeTemplate;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<StoreTemplate> getAllStoreTemplate() {

    List<StoreTemplate> storeTemplate = new ArrayList<>();
    try {
      storeTemplate = entityManager.createQuery("from StoreTemplate order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllStoreTemplate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return storeTemplate;
  }

  @Override
  public long getStoreTemplateByorgPathUrl(String pathUrl) {
    long id = 0;
    try {
      id =
          (Long)
              entityManager
                  .createQuery("select storeTemplateId from Organisation  where pathUrl=?1")
                  .setParameter(1, pathUrl)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getStoreTemplateByorgPathUrl ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return id;
  }
}
