package com.a2zbill.dao.impl;

import com.a2zbill.dao.ItemDao;
import com.a2zbill.domain.Item;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ItemDaoImpl implements ItemDao {

  private static final Logger logger = LoggerFactory.getLogger(ItemDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Item items) {
    try {
      entityManager.persist(items);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    return false;
  }

  @Override
  public boolean update(Item items) {
    return false;
  }

  @Override
  public Item getItemsById(Long id) {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Item> getAllItemsName() {

    List<Item> itemDetails = new ArrayList<>();
    try {
      itemDetails =
          entityManager.createQuery("select commodity from Item order by sl_no").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllItemsName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return itemDetails;
  }

  @Override
  public Item getItemsByHsnNo(final String hsnNo) {

    Item itemDetails = null;
    try {
      itemDetails =
          (Item)
              entityManager
                  .createQuery("FROM Item i where i.hsnCode =?1")
                  .setParameter(1, hsnNo)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getItemsByHsnNo ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return itemDetails;
  }

  @Override
  public Item getItemsByName(String commodity) {

    Item itemDetails = null;
    try {
      itemDetails =
          (Item)
              entityManager
                  .createQuery("FROM Item i where i.commodity =?1")
                  .setParameter(1, commodity)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getItemsByName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return itemDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Item> getAllItemsByHsnNo() {

    List<Item> itemDetails = new ArrayList<>();
    try {
      itemDetails =
          entityManager.createQuery("FROM Item i where i.hsnCode IS NOT NULL").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllItemsByHsnNo ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return itemDetails;
  }
}
