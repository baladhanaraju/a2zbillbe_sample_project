package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductDealsDao;
import com.a2zbill.domain.ProductDeals;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDealsDaoImpl implements ProductDealsDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductDealsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final ProductDeals productDeals) {
    try {
      entityManager.persist(productDeals);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final ProductDeals productDeals) {
    try {
      entityManager.merge(productDeals);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public ProductDeals getProductDealsById(final Long id) {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductDeals> getAllProductDeals() {

    List<ProductDeals> productsDetails = new ArrayList<>();
    try {
      productsDetails = entityManager.createQuery("from ProductDeals order by id").getResultList();
    } catch (final Exception ex) {
      logger.error(" getAllProductDeals", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return productsDetails;
  }

  @Override
  public ProductDeals getProductDealsByOfferCode(final String productCode) {

    ProductDeals productsDetails = null;
    try {
      productsDetails =
          (ProductDeals)
              entityManager
                  .createQuery("FROM Product where code =?1")
                  .setParameter(1, productCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getProductDealsByOfferCode", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }

    return productsDetails;
  }

  @Override
  public ProductDeals getProductDealsByIdAndOfferCode(final Long id, final String offerCode) {

    ProductDeals productsDeals = null;
    try {
      productsDeals =
          (ProductDeals)
              entityManager
                  .createQuery("FROM ProductDeals where id =?1 and offerCode =?2")
                  .setParameter(1, id)
                  .setParameter(2, offerCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getProductDealsByIdAndOfferCode", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDeals;
  }
}
