package com.a2zbill.dao.impl;

import com.a2zbill.dao.ReOrderBranchDao;
import com.a2zbill.domain.ReOrderBranch;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReOrderBranchDaoImpl implements ReOrderBranchDao {

  private static final Logger logger = LoggerFactory.getLogger(ReOrderBranchDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(ReOrderBranch reOrderBranch) {
    try {
      entityManager.persist(reOrderBranch);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }
}
