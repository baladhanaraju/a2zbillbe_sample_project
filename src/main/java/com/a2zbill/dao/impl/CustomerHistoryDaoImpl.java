package com.a2zbill.dao.impl;

import com.a2zbill.dao.CustomerHistoryDao;
import com.a2zbill.domain.CustomerHistory;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerHistoryDaoImpl implements CustomerHistoryDao {

  private static final Logger logger = LoggerFactory.getLogger(CustomerHistoryDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(CustomerHistory customerHistory) {
    try {
      entityManager.persist(customerHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(CustomerHistory customerHistory) {
    try {
      entityManager.merge(customerHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }
}
