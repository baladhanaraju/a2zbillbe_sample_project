package com.a2zbill.dao.impl;

import com.a2zbill.dao.RecipeProductDao;
import com.a2zbill.domain.RecipeProduct;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RecipeProductDaoImpl implements RecipeProductDao {

  private static final Logger logger = LoggerFactory.getLogger(RecipeProductDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(RecipeProduct recipeProduct) {
    try {
      entityManager.persist(recipeProduct);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(RecipeProduct recipeProduct) {
    try {
      entityManager.merge(recipeProduct);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeProduct> getAllRecipeProductDetails() {

    List<RecipeProduct> recipeProduct = new ArrayList<>();
    try {
      recipeProduct = entityManager.createQuery("from RecipeProduct order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeProductDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeProduct;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeProduct> getAllRecipeProductByOrgAndBranch(long orgId, long branchId) {

    List<RecipeProduct> recipeProduct = new ArrayList<>();
    try {
      recipeProduct =
          entityManager
              .createQuery("from RecipeProduct where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeProductByOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeProduct;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeProduct> getAllRecipeProductByOrg(long orgId) {

    List<RecipeProduct> recipeProduct = new ArrayList<>();
    try {
      recipeProduct =
          entityManager
              .createQuery("from RecipeProduct where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeProductByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeProduct;
  }

  @Override
  public RecipeProduct getRecipeProductById(long recipeProductId) {

    RecipeProduct recipeProduct = null;
    try {
      recipeProduct =
          (RecipeProduct)
              entityManager
                  .createQuery("from RecipeProduct where id=?1")
                  .setParameter(1, recipeProductId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRecipeProductById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeProduct;
  }

  @Override
  public RecipeProduct getRecipeProductByRecipeIdAndProductId(long recipeId, long productId) {

    RecipeProduct recipeProduct = null;
    try {
      recipeProduct =
          (RecipeProduct)
              entityManager
                  .createQuery("from RecipeProduct where productRecipe.id=?1 and product.id=?2")
                  .setParameter(1, recipeId)
                  .setParameter(2, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getRecipeProductByRecipeIdAndProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeProduct;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeProduct> getAllRecipeProductsProductIdAndOrgAndBranch(
      long orgId, long branchId, long productId) {

    List<RecipeProduct> recipeProduct = new ArrayList<>();
    try {
      recipeProduct =
          entityManager
              .createQuery(
                  "from RecipeProduct where organisation.id=?1 and branch.id=?2 and product.id=?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeProductsProductIdAndOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeProduct;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<RecipeProduct> getAllRecipeProductByRecipeId(long recipeId) {

    List<RecipeProduct> recipeProduct = new ArrayList<>();
    try {
      recipeProduct =
          entityManager
              .createQuery("from RecipeProduct where productRecipe.id=?1")
              .setParameter(1, recipeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllRecipeProductByRecipeId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return recipeProduct;
  }
}
