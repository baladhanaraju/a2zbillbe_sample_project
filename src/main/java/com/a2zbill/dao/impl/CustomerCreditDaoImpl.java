package com.a2zbill.dao.impl;

import com.a2zbill.dao.CustomerCreditDao;
import com.a2zbill.dao.CustomerCreditsDao;
import com.a2zbill.domain.CustomerCredit;
import com.a2zbill.domain.CustomerCreditHistory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerCreditDaoImpl implements CustomerCreditDao {

  private static final Logger logger = LoggerFactory.getLogger(CustomerCreditDaoImpl.class);
  @Autowired private EntityManager entityManager;

  @Autowired CustomerCreditsDao customerCreditsDao;

  @Override
  public void save(CustomerCredit customerCredit) {
    try {
      entityManager.persist(customerCredit);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void update(CustomerCredit customerCredit) {
    try {
      entityManager.merge(customerCredit);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public CustomerCredit getCustomerCreditDetailsByCustomerId(long customerId) {
    CustomerCredit customerCredit = null;
    try {
      customerCredit =
          (CustomerCredit)
              entityManager
                  .createQuery("from CustomerCredit where customerId.customerId=?1")
                  .setParameter(1, customerId)
                  .getSingleResult();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCredit;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerCreditHistory> getCustomerCreditHistoryByCustomerCreditId(
      long customerCreditId) {
    List<CustomerCreditHistory> customerCreditHistory = new ArrayList<CustomerCreditHistory>();
    try {
      customerCreditHistory =
          entityManager
              .createQuery("from CustomerCreditHistory where customerCreditId.id=?1")
              .setParameter(1, customerCreditId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCreditHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerCreditHistory> getAllCustomerCreditHistoryByOrgAndBranch(
      long orgId, long branchId) {
    List<CustomerCreditHistory> customerCreditHistory = new ArrayList<CustomerCreditHistory>();
    try {
      customerCreditHistory =
          entityManager
              .createQuery(
                  "from CustomerCreditHistory where organisationId.id=?1 and branchId.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCreditHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerCreditHistory> getAllCustomerCreditHistoryByOrg(long orgId) {
    List<CustomerCreditHistory> customerCreditHistory = new ArrayList<CustomerCreditHistory>();
    try {
      customerCreditHistory =
          entityManager
              .createQuery("from CustomerCreditHistory where organisationId.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCreditHistory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerCredit> getAllCustomerCreditsByOrgAndBranch(long orgId, long branchId) {
    List<CustomerCredit> customerCredit = new ArrayList<CustomerCredit>();
    try {
      customerCredit =
          entityManager
              .createQuery("from CustomerCredit where organisationId.id=?1 and branchId.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCredit;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerCredit> getAllCustomerCreditsByOrg(long orgId) {
    List<CustomerCredit> customerCredit = new ArrayList<CustomerCredit>();
    try {
      customerCredit =
          entityManager
              .createQuery("from CustomerCredit where organisationId.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerCredit;
  }

  @Override
  public void saveCustomerCreditHistory(CustomerCreditHistory customerCreditHistory) {
    try {
      entityManager.persist(customerCreditHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void updateCustomerCreditHistory(CustomerCreditHistory customerCreditHistory) {
    try {
      entityManager.merge(customerCreditHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }
}
