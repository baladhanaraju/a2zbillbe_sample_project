package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductRecipeDao;
import com.a2zbill.domain.ProductRecipe;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductRecipeDaoImpl implements ProductRecipeDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductRecipeDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(ProductRecipe productRecipe) {
    try {
      entityManager.persist(productRecipe);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(ProductRecipe productRecipe) {
    try {
      entityManager.merge(productRecipe);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductRecipe> getAllProductRecipeDetails() {

    List<ProductRecipe> productRecipe = new ArrayList<>();
    try {
      productRecipe = entityManager.createQuery("from ProductRecipe order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductRecipeDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductRecipe> getAllProductRecipesByOrgAndBranch(long orgId, long branchId) {

    List<ProductRecipe> productRecipe = new ArrayList<>();
    try {
      productRecipe =
          entityManager
              .createQuery("from ProductRecipe where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductRecipesByOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductRecipe> getAllProductRecipesByOrg(long orgId) {

    List<ProductRecipe> productRecipe = new ArrayList<>();
    try {
      productRecipe =
          entityManager
              .createQuery("from ProductRecipe where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductRecipesByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }

  @Override
  public ProductRecipe getProductRecipesById(long recipeId) {

    ProductRecipe productRecipe = null;
    try {
      productRecipe =
          (ProductRecipe)
              entityManager
                  .createQuery("from ProductRecipe where id=?1")
                  .setParameter(1, recipeId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductRecipesById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }
  // SELECT * from  product.recipe_ingredients  LEFT JOIN product.product_recipe ON
  // product.product_recipe.id=product.recipe_ingredients.recipe_id  where recipe_id=?1
  @SuppressWarnings("unchecked")
  @Override
  public List<ProductRecipe> getAllProductRecipesByIngredientsId(long ingredientsId) {

    List<ProductRecipe> productRecipe = new ArrayList<>();
    try {
      productRecipe =
          entityManager
              .createNativeQuery(
                  "select * from product.recipe_ingredients ri left join product.product_recipe pr on(pr.id=ri.recipe_id)=?1")
              .setParameter(1, ingredientsId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductRecipesByIngredientsId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductRecipe> getAllProductRecipesById(long id) {

    List<ProductRecipe> productRecipe = new ArrayList<>();
    try {
      productRecipe =
          entityManager
              .createQuery("from ProductRecipe where id=?1")
              .setParameter(1, id)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductRecipesById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }

  @Override
  public ProductRecipe getProductRecipesByRecipeName(String recipeName) {

    ProductRecipe productRecipe = null;
    try {
      productRecipe =
          (ProductRecipe)
              entityManager
                  .createQuery("from ProductRecipe where recipeName=?1")
                  .setParameter(1, recipeName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductRecipesByRecipeName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }

  @Override
  public ProductRecipe getProductRecipesByRecipebyId(long id) {

    ProductRecipe productRecipe = null;
    try {
      productRecipe =
          (ProductRecipe)
              entityManager
                  .createQuery("from ProductRecipe where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductRecipesByRecipebyId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productRecipe;
  }
}
