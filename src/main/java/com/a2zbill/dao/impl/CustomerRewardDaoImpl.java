package com.a2zbill.dao.impl;

import com.a2zbill.dao.CustomerRewardDao;
import com.a2zbill.domain.CustomerReward;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRewardDaoImpl implements CustomerRewardDao {

  private static final Logger logger = LoggerFactory.getLogger(CustomerRewardDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final CustomerReward customerReward) {
    try {
      entityManager.persist(customerReward);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final CustomerReward customerReward) {
    try {
      entityManager.merge(customerReward);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public CustomerReward getCustomerRewardsById(final Long id) {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerReward> getAllCustomerRewards() {

    List<CustomerReward> customerRewards = new ArrayList<>();
    try {
      customerRewards =
          (List<CustomerReward>)
              entityManager.createQuery("from CustomerReward order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCustomerRewards ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return customerRewards;
  }
}
