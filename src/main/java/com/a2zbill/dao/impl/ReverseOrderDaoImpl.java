package com.a2zbill.dao.impl;

import com.a2zbill.dao.ReverseOrderDao;
import com.a2zbill.domain.ReverseOrder;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReverseOrderDaoImpl implements ReverseOrderDao {

  private static final Logger logger = LoggerFactory.getLogger(ReverseOrderDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(ReverseOrder reverseOrder) {
    try {
      entityManager.persist(reverseOrder);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(ReverseOrder reverseOrder) {
    try {
      entityManager.merge(reverseOrder);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReverseOrder> getAllReverseOrderDetails() {

    List<ReverseOrder> reverseOrderDetails = new ArrayList<>();
    try {
      reverseOrderDetails =
          entityManager.createQuery("FROM ReverseOrder order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReverseOrderDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reverseOrderDetails;
  }

  @Override
  public ReverseOrder getReverseOrderDetailsById(final long reverseOrderId) {

    ReverseOrder reverseOrder = null;
    try {
      reverseOrder =
          (ReverseOrder)
              entityManager
                  .createQuery("From ReverseOrder c where c.id = ?1")
                  .setParameter(1, reverseOrderId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReverseOrderDetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reverseOrder;
  }
}
