package com.a2zbill.dao.impl;

import com.a2zbill.dao.OrderHistoryDao;
import com.a2zbill.domain.OrderHistory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderHistoryDaoImpl implements OrderHistoryDao {

  private static final Logger logger = LoggerFactory.getLogger(OrderHistoryDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(OrderHistory orderHistory) {
    try {
      entityManager.persist(orderHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(OrderHistory orderHistory) {
    try {
      entityManager.merge(orderHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public List<OrderHistory> getAllOrderHistoryDetails() {
    List<OrderHistory> orderHistory = new ArrayList<>();
    try {
      orderHistory = entityManager.createQuery("from OrderHistory").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllOrderHistoryDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderHistory;
  }

  @Override
  public OrderHistory getOrderHistoryById(long id) {
    OrderHistory orderHistory = null;
    try {
      orderHistory =
          (OrderHistory)
              entityManager
                  .createQuery("from OrderHistory  where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOrderById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return orderHistory;
  }
}
