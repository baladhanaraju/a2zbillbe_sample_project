package com.a2zbill.dao.impl;

import com.a2zbill.dao.InventoryDetailDao;
import com.a2zbill.domain.InventoryDetail;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryDetailsDaoImpl implements InventoryDetailDao {

  private static final Logger logger = LoggerFactory.getLogger(InventoryDetailsDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(InventoryDetail inventoryDetail) {
    try {
      entityManager.persist(inventoryDetail);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(InventoryDetail inventoryDetail) {
    try {
      entityManager.merge(inventoryDetail);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryDetail> getAllInventoryDetails() {

    List<InventoryDetail> inventoryDetail = new ArrayList<>();
    try {
      inventoryDetail =
          entityManager
              .createQuery("from InventoryDetail ORDER BY inventoryId ASC")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public InventoryDetail getInventoryDetailsByProductId(long productId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery("from InventoryDetail where product.id=?1")
                  .setParameter(1, productId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailsByProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryDetail> getInventoryDetailsByProductIds(long productId) {

    List<InventoryDetail> inventoryDetail = new ArrayList<>();
    try {
      inventoryDetail =
          entityManager
              .createQuery("from InventoryDetail where product.id=?1")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailsByProductIds ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public InventoryDetail getInventoryDetailsByProductIdAndBranchId(
      final long productId, final long branchId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery("from InventoryDetail where product.id=?1 and BranchDetail.id=?2")
                  .setParameter(1, productId)
                  .setParameter(2, branchId)
                  .getResultList()
                  .stream()
                  .findFirst()
                  .orElse(null);
    } catch (final Exception ex) {
      logger.error("getInventoryDetailsByProductIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public InventoryDetail getInventoryDetailsByProductIdAndBranchIdAndOrgId(
      final long productId, final long branchId, final long orgId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery(
                      "from InventoryDetail where product.id=?1 and BranchDetail.id=?2 and organisation.id=?3")
                  .setParameter(1, productId)
                  .setParameter(2, branchId)
                  .setParameter(3, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailsByProductIdAndBranchIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public Long getInventoryCurrentStockQTY() {

    Long inventoryDetail = null;
    try {
      inventoryDetail =
          (Long)
              this.entityManager
                  .createQuery("Select SUM(quantity) FROM InventoryDetail")
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryCurrentStockQTY ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public BigDecimal getInventoryDetailStockAmount() {

    BigDecimal inventoryDetail = null;
    try {
      inventoryDetail =
          (BigDecimal)
              entityManager
                  .createQuery("Select SUM(buyingPrice*quantity) FROM InventoryDetail")
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailStockAmount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public InventoryDetail getInventoryDetailById(final long inventoryId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery("from InventoryDetail where inventoryId=?1")
                  .setParameter(1, inventoryId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryDetail> getAllInventoryDetails(final long branchId, final long orgId) {

    List<InventoryDetail> inventoryDetail = new ArrayList<>();
    try {
      inventoryDetail =
          entityManager
              .createQuery(
                  "from InventoryDetail where BranchDetail.id=?1 and organisation.id=?2 ORDER BY inventoryId ASC")
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getReOrderDetailsById(final long productId) {

    List<ReOrder> reorder = new ArrayList<>();
    try {
      reorder =
          entityManager
              .createQuery("from ReOrder where product.id=?1")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReOrderDetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reorder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductVendor> getProductVendorsbyId(final long productId) {

    List<ProductVendor> productVendor = new ArrayList<>();
    try {
      productVendor =
          entityManager
              .createQuery(
                  "select vendor.id,vendor.vendorName from ProductVendor where product.id=?1")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductVendorsbyId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVendor;
  }

  @Override
  public InventoryDetail getInventoryDetailByIdOrgId(long inventoryId, long orgId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery("from InventoryDetail where inventoryId=?1 and organisation.id=?2 ")
                  .setParameter(1, inventoryId)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailByIdOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryDetail> getReOrderInventoryDetails(final long orgId, final long branchId) {

    List<InventoryDetail> inventoryDetail = new ArrayList<>();
    try {
      inventoryDetail =
          entityManager
              .createQuery(
                  "from InventoryDetail where quantity<=reorderQnty and organisation.id=?1 and BranchDetail.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReOrderInventoryDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getReorderProductByStatus(
      final long orgId, final long branchId, final long productId) {

    List<ReOrder> vendor = new ArrayList<>();
    try {
      vendor =
          entityManager
              .createQuery(
                  "from ReOrder where organisation.id=?1 and branch.id=?2 and status=?3 and product.id=?4")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, "false")
              .setParameter(4, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReorderProductByStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return vendor;
  }

  @Override
  public BigDecimal getInventoryCurrentStockQTYByorgBranch(final long orgId, final long branchId) {

    BigDecimal inventoryDetail = null;
    try {
      inventoryDetail =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "Select SUM(quantity) FROM InventoryDetail where organisation.id=?1 and BranchDetail.id=?2")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryCurrentStockQTYByorgBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public BigDecimal getInventoryDetailStockAmountByorgBranch(
      final long orgId, final long branchId) {

    BigDecimal inventoryDetail = null;
    try {
      inventoryDetail =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "Select SUM(buyingPrice*quantity) FROM InventoryDetail where organisation.id=?1 and BranchDetail.id=?2")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailStockAmountByorgBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public InventoryDetail getInventoryDetailByIdOrgIdBranch(
      final long inventoryId, final long orgId, final long branchId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery(
                      "from InventoryDetail where inventoryId=?1 and organisation.id=?2 and BranchDetail.id=?3")
                  .setParameter(1, inventoryId)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailByIdOrgIdBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryDetail> getAllInventoryDetailsByOrgId(final long orgId) {

    List<InventoryDetail> inventoryDetail = new ArrayList<>();
    try {
      inventoryDetail =
          entityManager
              .createQuery(
                  "from InventoryDetail where quantity<=reorderQnty and organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllInventoryDetailsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public BigDecimal getInventoryCurrentStockQTYByorg(long orgId) {

    BigDecimal inventoryDetail = null;
    try {
      inventoryDetail =
          (BigDecimal)
              entityManager
                  .createQuery("Select SUM(quantity) FROM InventoryDetail where organisation.id=?1")
                  .setParameter(1, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryCurrentStockQTYByorg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public BigDecimal getInventoryDetailStockAmountByorg(long orgId) {

    BigDecimal inventoryDetail = null;
    try {
      inventoryDetail =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "Select SUM(buyingPrice*quantity) FROM InventoryDetail where organisation.id=?1")
                  .setParameter(1, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailStockAmountByorg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @Override
  public InventoryDetail getInventoryDetailsByProductIdAndOrgId(
      final long productId, final long orgId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery("from InventoryDetail where product.id=?1 and organisation.id=?2")
                  .setParameter(1, productId)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailsByProductIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryDetail> getReOrderInventoryDetailsByOrg(final long orgId) {

    List<InventoryDetail> inventoryDetail = new ArrayList<>();
    try {
      inventoryDetail =
          entityManager
              .createQuery(
                  "from InventoryDetail where quantity<=reorderQnty and organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReOrderInventoryDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getReorderProductByStatusByOrgId(final long orgId, final long productId) {

    List<ReOrder> vendor = new ArrayList<>();
    try {
      vendor =
          entityManager
              .createQuery("from ReOrder where organisation.id=?1 and status=?2 and product.id=?3")
              .setParameter(1, orgId)
              .setParameter(2, "false")
              .setParameter(3, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReorderProductByStatusByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return vendor;
  }

  @Override
  public void updateInventoryQuantity(final long productId, final BigDecimal quantity) {
    try {
      entityManager
          .createQuery("update InventoryDetail set quantity=?1 where product.id=?2")
          .setParameter(1, quantity)
          .setParameter(2, productId)
          .executeUpdate();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<InventoryDetail> getInventoryDetailByBranch(long branchId) {

    List<InventoryDetail> inventoryDetails = new ArrayList<>();
    try {
      inventoryDetails =
          entityManager
              .createQuery("from InventoryDetail where BranchDetail.id=?1")
              .setParameter(1, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailByBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetails;
  }

  @Override
  public InventoryDetail getInventoryDetailByProductNameAndBranchIdAndOrgId(
      String productName, long branchId, long orgId) {

    InventoryDetail inventoryDetail = null;
    try {
      inventoryDetail =
          (InventoryDetail)
              entityManager
                  .createQuery(
                      "from InventoryDetail where product.productName=?1 and BranchDetail.id=?2 and organisation.id=?3")
                  .setParameter(1, productName)
                  .setParameter(2, branchId)
                  .setParameter(3, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getInventoryDetailByProductNameAndBranchIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return inventoryDetail;
  }
}
