package com.a2zbill.dao.impl;

import com.a2zbill.dao.OutwardDao;
import com.a2zbill.domain.Outward;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OutwardDaoImpl implements OutwardDao {

  private static final Logger logger = LoggerFactory.getLogger(OutwardDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final Outward outward) {
    try {
      entityManager.persist(outward);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final Outward inward) {
    try {
      entityManager.merge(inward);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Outward> getAllOutwardDetails() {

    List<Outward> outwardDetails = new ArrayList<>();
    try {
      outwardDetails = entityManager.createQuery("from Outward ORDER BY id ASC").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllOutwardDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return outwardDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Outward> getOutwardDetailsByMonthAndYear(final String month, final String year) {

    List<Outward> outward = new ArrayList<>();
    try {
      outward =
          entityManager
              .createQuery("from Outward where month=?1 and year=?2")
              .setParameter(1, month)
              .setParameter(2, year)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOutwardDetailsByMonthAndYear ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return outward;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Outward> getOutwardDetailsByGstNumber(final String gstNumber) {

    List<Outward> outward = new ArrayList<>();
    try {
      outward =
          entityManager
              .createQuery("from Outward where gstNumber=?1")
              .setParameter(1, gstNumber)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOutwardDetailsByGstNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return outward;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Outward> getOutwardDetailsByCustomerId(final long customerId) {

    List<Outward> outward = new ArrayList<>();
    try {
      outward =
          entityManager
              .createQuery("from Outward where customer.id=?1")
              .setParameter(1, customerId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOutwardDetailsByCustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return outward;
  }

  @Override
  public Outward getOutwardDetailByGstNumber(final String gstNumber) {

    Outward outward = null;
    try {
      outward =
          (Outward)
              entityManager
                  .createQuery("from Outward where gstNumber=?1")
                  .setParameter(1, gstNumber)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getOutwardDetailsByGstNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return outward;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Outward> getOutwardDetailsByMonthAndYearAndGSTNumber(
      final String month, final String year) {

    List<Outward> outward = new ArrayList<>();
    try {
      outward =
          entityManager
              .createQuery("from Outward where month=?1 and year=?2")
              .setParameter(1, month)
              .setParameter(2, year)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOutwardDetailsByMonthAndYearAndGSTNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return outward;
  }
}
