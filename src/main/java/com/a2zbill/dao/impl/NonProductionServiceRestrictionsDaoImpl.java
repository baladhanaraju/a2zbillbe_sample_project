package com.a2zbill.dao.impl;

import com.a2zbill.dao.NonProductionServiceRestrictionsDao;
import com.a2zbill.domain.NonProductionServiceRestrictions;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NonProductionServiceRestrictionsDaoImpl
    implements NonProductionServiceRestrictionsDao {

  @Autowired private EntityManager entityManager;

  private static final Logger logger =
      LoggerFactory.getLogger(NonProductionServiceRestrictionsDaoImpl.class);

  @Override
  public void save(NonProductionServiceRestrictions nonProductionServiceRestrictions) {
    try {
      entityManager.persist(nonProductionServiceRestrictions);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(NonProductionServiceRestrictions nonProductionServiceRestrictions) {
    try {
      entityManager.merge(nonProductionServiceRestrictions);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<NonProductionServiceRestrictions> getAllNonProductionServiceRestrictions() {

    List<NonProductionServiceRestrictions> nonProductionServiceRestrictions = new ArrayList<>();
    try {
      nonProductionServiceRestrictions =
          entityManager
              .createQuery("from NonProductionServiceRestrictions order by id")
              .getResultList();

    } catch (final Exception ex) {
      logger.error("getAllNonProductionServiceRestrictions ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return nonProductionServiceRestrictions;
  }
}
