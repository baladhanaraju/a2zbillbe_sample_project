package com.a2zbill.dao.impl;

import com.a2zbill.dao.CustomerRedeemProductDao;
import com.a2zbill.domain.CustomerRedeemProduct;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRedeemProductDaoImpl implements CustomerRedeemProductDao {

  private static final Logger logger = LoggerFactory.getLogger(CustomerRedeemProductDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(CustomerRedeemProduct customerRedeemProduct) {
    try {
      entityManager.persist(customerRedeemProduct);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(CustomerRedeemProduct CustomerRedeemProduct) {
    try {
      entityManager.merge(CustomerRedeemProduct);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerRedeemProduct> getAllCustomerRedeemProducts() {

    List<CustomerRedeemProduct> customerRedeemProduct = new ArrayList<>();
    try {
      customerRedeemProduct =
          entityManager.createQuery("from CustomerRedeemProduct order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCustomerRedeemProducts ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerRedeemProduct;
  }

  @Override
  public List<Object[]> getCustomerRedeemProductById(long productid) {

    List<Object[]> customerRedeemProduct = new ArrayList<>();
    try {
      customerRedeemProduct =
          entityManager
              .createNativeQuery(
                  "SELECT product.product_reward_points.product_id,product.products.product_name,product.products.code FROM product.product_reward_points INNER JOIN product.products ON product.product_reward_points.product_id = product.products.id where product.product_reward_points.product_id =?1")
              .setParameter(1, productid)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCustomerRedeemProductById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerRedeemProduct;
  }

  @Override
  public CustomerRedeemProduct getCustomerRedeemProductByproductIdAndCustId(
      final long productId, final long customerId) {

    CustomerRedeemProduct customerRedeemProduct = null;
    try {
      customerRedeemProduct =
          (CustomerRedeemProduct)
              entityManager
                  .createQuery(
                      "from CustomerRedeemProduct where productId.id = ?1 and customerId.id = ?2")
                  .setParameter(1, productId)
                  .setParameter(1, customerId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCustomerRedeemProductByproductIdAndCustId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerRedeemProduct;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerRedeemProduct> getCustomerRedeemProductBycustomerId(final long customerid) {

    List<CustomerRedeemProduct> customerRedeemProduct = null;
    try {
      customerRedeemProduct =
          entityManager
              .createQuery("from CustomerRedeemProduct where CustomerId.id = ?1")
              .setParameter(1, customerid)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCustomerRedeemProductBycustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerRedeemProduct;
  }

  @Override
  public CustomerRedeemProduct getCustomerRedeemProductByproductIdOrgIdAndBranchId(
      final long productid, final long orgId, final long branchId) {

    CustomerRedeemProduct customerRedeemProduct = null;
    try {
      customerRedeemProduct =
          (CustomerRedeemProduct)
              entityManager
                  .createQuery(
                      "from CustomerRedeemProduct where productId.id = ?1 and orgId.id = ?2 and branchId.id = ?3")
                  .setParameter(1, productid)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCustomerRedeemProductByproductIdOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerRedeemProduct;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CustomerRedeemProduct> getCustomerRedeemProductBycustomerIdByOrgIdAndBranchId(
      final long customerid, final long orgId, final long branchId) {

    List<CustomerRedeemProduct> customerRedeemProduct = new ArrayList<>();
    try {
      customerRedeemProduct =
          entityManager
              .createQuery(
                  "from CustomerRedeemProduct where CustomerId.id = ?1 and orgId.id = ?2 and branchId.id = ?3 ")
              .setParameter(1, customerid)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCustomerRedeemProductBycustomerIdByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerRedeemProduct;
  }

  @Override
  public CustomerRedeemProduct getCustomerRedeemProductByproductIdOrgId(
      final long productid, final long orgId) {

    CustomerRedeemProduct customerRedeemProduct = null;
    try {
      customerRedeemProduct =
          (CustomerRedeemProduct)
              entityManager
                  .createQuery("from CustomerRedeemProduct where productId.id =?1 and orgId.id =?2")
                  .setParameter(1, productid)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCustomerRedeemProductByproductIdOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return customerRedeemProduct;
  }
}
