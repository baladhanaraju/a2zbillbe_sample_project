package com.a2zbill.dao.impl;

import com.a2zbill.dao.LooseProductDao;
import com.a2zbill.domain.LooseProduct;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LooseProductDaoImpl implements LooseProductDao {

  private static final Logger logger = LoggerFactory.getLogger(LooseProductDaoImpl.class);

  @Autowired EntityManager entityManager;

  @Override
  public void save(LooseProduct looseProduct) {
    try {
      entityManager.persist(looseProduct);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean update(LooseProduct looseProduct) {
    try {
      entityManager.merge(looseProduct);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<LooseProduct> getAllLooseProductDetails() {

    List<LooseProduct> looseProductDetails = new ArrayList<>();
    try {
      looseProductDetails =
          entityManager.createQuery("from LooseProduct order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllLooseProductDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return looseProductDetails;
  }

  @Override
  public LooseProduct getLooseProductDetailsByCode(final String looseProductCode) {

    LooseProduct looseProductDetails = null;
    try {
      looseProductDetails =
          (LooseProduct)
              entityManager
                  .createQuery("from LooseProduct where barCode =?1")
                  .setParameter(1, looseProductCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getLooseProductDetailsByCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return looseProductDetails;
  }

  @Override
  public LooseProduct getLooseProductDetailsByHsnCode(final String looseProductHsnCode) {

    LooseProduct looseProductDetails = null;
    try {
      looseProductDetails =
          (LooseProduct)
              entityManager
                  .createQuery("from LooseProduct where hsncode =?1")
                  .setParameter(1, looseProductHsnCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getLooseProductDetailsByHsnCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return looseProductDetails;
  }

  @Override
  public LooseProduct getLooseProductDetailsById(final long looseProductId) {

    LooseProduct looseProductDetails = null;
    try {
      looseProductDetails =
          (LooseProduct)
              entityManager
                  .createQuery("from LooseProduct where id =?1")
                  .setParameter(1, looseProductId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getLooseProductDetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return looseProductDetails;
  }
}
