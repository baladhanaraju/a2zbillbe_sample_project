package com.a2zbill.dao.impl;

import com.a2zbill.dao.KotStatusDao;
import com.a2zbill.domain.KotStatus;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class KotStatusDaoImpl implements KotStatusDao {

  private static final Logger logger = LoggerFactory.getLogger(KotStatusDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void saveKotStatus(KotStatus kotStatus) {
    try {
      entityManager.persist(kotStatus);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void update(KotStatus kotStatus) {
    try {
      entityManager.merge(kotStatus);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<KotStatus> getAllKotStatus() {

    List<KotStatus> kotStatusDetails = new ArrayList<>();
    try {
      kotStatusDetails = entityManager.createQuery("from KotStatus order by id").getResultList();

    } catch (final Exception ex) {
      logger.error("getAllKotStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return kotStatusDetails;
  }

  @Override
  public KotStatus getKotStatusById(final long id) {

    KotStatus kotStatus = null;
    try {
      kotStatus =
          (KotStatus)
              entityManager
                  .createQuery("from KotStatus where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();

    } catch (final Exception ex) {
      logger.error("getKotStatusById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return kotStatus;
  }

  @SuppressWarnings("unchecked")
  @Override
  public KotStatus getKotStatusByBranchId(
      final long branchId, final String productName, final int quantity, final long counterNumber) {

    KotStatus kotStatus = new KotStatus();
    List<KotStatus> data = new ArrayList<>();
    try {
      data =
          entityManager
              .createQuery(
                  "from KotStatus where branchId=?1 and productName=?2 and quantity=?3 and counterNumber=?4 ")
              .setParameter(1, branchId)
              .setParameter(2, productName)
              .setParameter(3, quantity)
              .setParameter(4, counterNumber)
              .getResultList();
      if (data.size() != 0) {

        kotStatus = data.get(0);
      }
    } catch (final Exception ex) {
      logger.error("getKotStatusByBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return kotStatus;
  }

  @Override
  public boolean delete(Long id) {
    Object result = entityManager.find(KotStatus.class, id);
    if (result != null) {
      this.entityManager.remove(result);
      return true;
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<KotStatus> getKotStatusByCounterNumber(final long counterNumber) {

    List<KotStatus> kotStatus = new ArrayList<>();
    try {
      kotStatus =
          entityManager
              .createQuery("from KotStatus where counterNumber=?1")
              .setParameter(1, counterNumber)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getKotStatusByCounterNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return kotStatus;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<KotStatus> getKotStatusByCounterNumber(
      final long counterNumber, final long branchId) {

    List<KotStatus> kotStatus = new ArrayList<>();
    try {
      kotStatus =
          entityManager
              .createQuery("from KotStatus where counterNumber=?1 and branchId=?2")
              .setParameter(1, counterNumber)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getKotStatusByCounterNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return kotStatus;
  }
}
