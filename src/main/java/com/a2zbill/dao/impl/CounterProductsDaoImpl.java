package com.a2zbill.dao.impl;

import com.a2zbill.dao.CounterProductsDao;
import com.a2zbill.domain.CartDetail;
import com.a2zbill.domain.EmployeeCounter;
import com.a2zbill.domain.ProductVendor;
import com.a2zbill.domain.ReOrder;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CounterProductsDaoImpl implements CounterProductsDao {

  private static final Logger logger = LoggerFactory.getLogger(CounterProductsDaoImpl.class);
  @Autowired private EntityManager entityManager;

  @Override
  public void save(EmployeeCounter employeeCounter) {
    try {
      entityManager.persist(employeeCounter);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void insertEmployeeCounterDetails(EmployeeCounter employeeCounter) {
    try {
      entityManager.persist(employeeCounter);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void saveVendorproducts(ProductVendor productVendor) {
    try {
      entityManager.persist(productVendor);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void saveReOrder(ReOrder reorder) {
    try {
      entityManager.persist(reorder);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  /*@Override
  public boolean update(ReOrder reOrder) {
    try {
      entityManager.merge(reOrder);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }*/

  @Override
  public void updateVendorProducts(ProductVendor productVendor) {
    try {
      entityManager
          .createQuery(
              "update ProductVendor set vendor.id=(?1),expectedDuration=(?2) where product.id=(?3)")
          .setParameter(1, productVendor.getVendor().getId())
          .setParameter(2, productVendor.getExpectedDuration())
          .setParameter(3, productVendor.getProduct().getId())
          .executeUpdate();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean updateReorder(ReOrder reOrder) {
    try {
      entityManager.merge(reOrder);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public int updateReOrderByProductIdAndReorderId(final long productId, long reorderId) {

    Integer updateStatus = null;
    try {
      updateStatus =
          (Integer)
              entityManager
                  .createQuery("update ReOrder set status=(?1) where product.id=?2 and id=?3")
                  .setParameter(1, "true")
                  .setParameter(2, productId)
                  .setParameter(3, reorderId)
                  .executeUpdate();
    } catch (final Exception ex) {
      logger.error("CounterProductsDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return updateStatus;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getAssignedCounters(final long branchId, final long orgId) {
    List<Long> counters = new ArrayList<>();
    try {
      counters =
          (List<Long>)
              entityManager
                  .createQuery(
                      "select counter from EmployeeCounter where orgId.id=?1 and branchId.id=?2")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("getAssignedCounters ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counters;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getOpenCounters() {

    List<Long> counterstatus = new ArrayList<>();
    try {
      counterstatus =
          entityManager
              .createQuery("select id from Counter where counterStatus=?1")
              .setParameter(1, "Open")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getOpenCounters ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterstatus;
  }

  @Override
  public Long getEmployeeId(final String emailId) {

    Long employeeId = null;
    try {
      employeeId =
          (Long)
              entityManager
                  .createQuery("select id from Employee where emailAddress=?1")
                  .setParameter(1, emailId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getEmployeeId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return employeeId;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getEmployeeCounters(final long employeeId) {

    List<Long> counternumbers = new ArrayList<>();
    try {
      counternumbers =
          entityManager
              .createQuery("select counter from EmployeeCounter where emp_id =?1")
              .setParameter(1, employeeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getEmployeeCounters ", ex);
    } finally {

    }
    return counternumbers;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CartDetail> getCartProductDetails(List<CartDetail> cartids) {

    List<CartDetail> listOfcartProducts = new LinkedList<CartDetail>();
    List<CartDetail> cartDetails = new ArrayList<>();
    long cartid = 0;
    try {
      for (int i = 0; i < cartids.size(); i++) {
        cartid = cartids.get(i).getCartDetails().getCartId();
        cartDetails =
            entityManager
                .createQuery("from CartDetail where cartDetails.cartId=?1")
                .setParameter(1, cartid)
                .getResultList();
        for (CartDetail o : cartDetails) {
          listOfcartProducts.add(o);
        }
      }
    } catch (final Exception ex) {
      logger.error("getCartProductDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getRemainCounter() {

    List<Long> counters = new ArrayList<Long>();
    try {
      counters = entityManager.createQuery("select id from Counter").getResultList();
    } catch (final Exception ex) {
      logger.error("getRemainCounter ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counters;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getAssignedCounters() {

    List<Long> counters = new ArrayList<>();
    try {
      counters = entityManager.createQuery("select counter from EmployeeCounter").getResultList();
    } catch (final Exception ex) {
      logger.error("getAssignedCounters ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counters;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getEmployeeCounterIds(final long employeeId) {

    List<Long> counterdetails = new ArrayList<>();
    try {
      counterdetails =
          entityManager
              .createQuery("select counter from EmployeeCounter where emp_id =?1")
              .setParameter(1, employeeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getEmployeeCounterIds ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return counterdetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BigDecimal> getCounterTotalBillAmount(final long cartId) {

    List<BigDecimal> billAmount = new ArrayList<>();
    try {
      billAmount =
          entityManager
              .createNativeQuery(
                  "select (sum(total_amount)+sum(taxable_amount)) from product.cart_product where cart_id=?1")
              .setParameter(1, cartId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCounterTotalBillAmount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billAmount;
  }

  @Override
  public int deleteEmployeeCounters(final long employeeId) {

    Integer deletestatus = null;
    try {
      deletestatus =
          (Integer)
              entityManager
                  .createQuery("delete from EmployeeCounter where employee.id=?1")
                  .setParameter(1, employeeId)
                  .executeUpdate();
    } catch (final Exception ex) {
      logger.error("deleteEmployeeCounters ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return deletestatus;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductVendor> viewOrderProducts(final long productId) {

    List<ProductVendor> productVendor = new ArrayList<>();
    try {
      productVendor =
          entityManager
              .createQuery("from ProductVendor where product.id=(?1)")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("viewOrderProducts ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVendor;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> viewReorderDetails(final long productId) {

    List<ReOrder> reOrder = new ArrayList<>();
    try {
      reOrder =
          entityManager
              .createQuery("from ReOrder where product.id = (?1)")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("ViewReOrderDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reOrder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> viewReorderDetailsByStatus(final long orgId, final long branchId) {

    List<Object[]> reOrder = new ArrayList<>();
    try {
      reOrder =
          entityManager
              .createQuery(
                  "select id,product.id, product.productName, vendor.id, vendor.vendorName, orderDate, deliveryDate, status, quantity, organisation.id, organisation.orgName, branch.id,branch.branchName from ReOrder where organisation.id = ?1 and branch.id = ?2 and status = (?3)")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, "false")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("viewReorderDetailsByStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reOrder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getReOrderDetailsByInvoiceId(final long invoiceId) {

    List<ReOrder> reOrder = new ArrayList<>();
    try {
      reOrder =
          entityManager
              .createQuery("from ReOrder where invoiceDetails.id =  ?1")
              .setParameter(1, invoiceId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReOrderDetailsByInvoiceId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reOrder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> viewReorderDetailsByStatus(final long orgId) {

    List<Object[]> reOrder = new ArrayList<>();
    try {
      reOrder =
          entityManager
              .createQuery(
                  "select id,product.id,product.productName,vendor.id,vendor.vendorName,orderDate,deliveryDate,status,quantity,organisation.id,organisation.orgName,branch.id,branch.branchName from ReOrder where organisation.id=?1 and status = (?2)")
              .setParameter(1, orgId)
              .setParameter(2, "false")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("viewReorderDetailsByStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reOrder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getReorderDetailsByProductId(final long productId) {

    List<ReOrder> reOrder = new ArrayList<>();
    try {
      reOrder =
          entityManager
              .createQuery("from ReOrder where product.id = ?1")
              .setParameter(1, productId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReorderDetailsByProductId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reOrder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getAllReOrderDetailsByInvoiceId(long invoiceId) {

    List<ReOrder> reorder = new ArrayList<>();
    try {
      reorder =
          entityManager
              .createQuery("from ReOrder where invoiceDetails.id=?1")
              .setParameter(1, invoiceId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReOrderDetailsByInvoiceId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reorder;
  }

  @Override
  public ReOrder getReOrderDetailsById(long id) {

    ReOrder reorder = null;
    try {
      reorder =
          (ReOrder)
              this.entityManager
                  .createQuery("from ReOrder where id = ?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReOrderDetailsById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reorder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getAllReorderDetailsByOrgId(long orgId) {

    List<ReOrder> reOrder = new ArrayList<>();
    try {
      reOrder =
          entityManager
              .createQuery("from ReOrder where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("AllReOrderDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reOrder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getAllReorderDetailsByOrgIdAndBranchId(long orgId, long branchId) {

    List<ReOrder> reOrder = new ArrayList<>();
    try {
      reOrder =
          entityManager
              .createQuery("from ReOrder where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllReorderDetailsByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reOrder;
  }

  @Override
  public ReOrder getReorderDetailsByReorderId(final long reOrderId) {

    ReOrder reorder = null;
    try {
      reorder =
          (ReOrder)
              entityManager
                  .createQuery("from ReOrder where id = ?1")
                  .setParameter(1, reOrderId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getReorderDetailsByReorderId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return reorder;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getReorderListByInvoiceIdAndOrganisationId(long invoiceId, long orgId) {

    List<ReOrder> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from ReOrder where invoiceDetails.id = ?1 and organisation.id = ?2")
              .setParameter(1, invoiceId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReorderListByInvoiceIdAndOrganisationId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ReOrder> getReorderListByInvoiceIdAndOrganisationIdAndBranchId(
      long invoiceId, long orgId, long branchId) {

    List<ReOrder> list = new ArrayList<>();
    try {
      list =
          this.entityManager
              .createQuery(
                  "from ReOrder where invoiceDetails.id = ?1 and organisation.id = ?2 and branch.id = ?3")
              .setParameter(1, invoiceId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getReorderListByInvoiceIdAndOrganisationIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }
}
