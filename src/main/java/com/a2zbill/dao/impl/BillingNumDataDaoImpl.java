package com.a2zbill.dao.impl;

import com.a2zbill.dao.BillingNumDataDao;
import com.a2zbill.domain.BillingNumData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BillingNumDataDaoImpl implements BillingNumDataDao {

  private static final Logger logger = LoggerFactory.getLogger(BillingNumDataDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(BillingNumData billingNumData) {
    try {
      entityManager.persist(billingNumData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(BillingNumData billingNumData) {
    try {
      entityManager.remove(billingNumData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(BillingNumData billingNumData) {
    try {
      entityManager.merge(billingNumData);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BillingNumData> getAllBillingNumData() {

    List<BillingNumData> billingNumData = new ArrayList<>();
    try {
      billingNumData = entityManager.createQuery("from BillingNumData order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllBillingNumData ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return billingNumData;
  }

  @Override
  public BillingNumData getAllBillingNumDataByDateAndOrgBranchId(
      final Date date, final long billingNumberTypeId, final long orgId, final long branchId) {

    BillingNumData billingNumData = null;
    try {
      billingNumData =
          (BillingNumData)
              entityManager
                  .createQuery(
                      "from BillingNumData where  billdate = ?1 and billingNumType.id = ?2  and organisation.id = ?3 and branch.id = ?4")
                  .setParameter(1, date)
                  .setParameter(2, billingNumberTypeId)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllBillingNumDataByDateAndOrgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingNumData;
  }

  @Override
  public BillingNumData getAllBillingNumDataByDateExtractAndOrgBranchId(
      final long billingNumberTypeId,
      final int day,
      final int month,
      final int year,
      final long orgId,
      final long branchId) {

    BillingNumData billingNumData = null;
    try {
      billingNumData =
          (BillingNumData)
              entityManager
                  .createQuery(
                      "from BillingNumData where billingNumType.id=?1 and EXTRACT(DAY FROM billdate) = ?2 and EXTRACT(MONTH FROM billdate) = ?3 and EXTRACT(YEAR FROM billdate) = ?4 and  organisation.id=?5 and branch.id = ?6")
                  .setParameter(1, billingNumberTypeId)
                  .setParameter(2, day)
                  .setParameter(3, month)
                  .setParameter(4, year)
                  .setParameter(5, orgId)
                  .setParameter(6, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getAllBillingNumDataByDateExtractAndOrgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingNumData;
  }
}
