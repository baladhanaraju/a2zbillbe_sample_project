package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductVariationDao;
import com.a2zbill.domain.ProductVariation;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductVariationDaoImpl implements ProductVariationDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductVariationDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(ProductVariation productVariation) {
    try {
      entityManager.persist(productVariation);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(ProductVariation productVariation) {
    try {
      entityManager.merge(productVariation);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductVariation> getAllProductVariationDetails() {

    List<ProductVariation> productVariationDetails = new ArrayList<>();
    try {
      productVariationDetails =
          entityManager.createQuery("from ProductVariation order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductVariationDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVariationDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductVariation> getProductVariationDetailsByOrgIdAndBranchId(
      long orgId, long branchId) {

    List<ProductVariation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from ProductVariation where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductVariationDetailsByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductVariation> getProductVariationDetailsByOrgId(long orgId) {

    List<ProductVariation> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createQuery("from ProductVariation where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductVariationDetailsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @Override
  public ProductVariation getProductVariationDetailsByProductVariationId(long productVariationId) {

    ProductVariation productVariationDetails = null;
    try {
      productVariationDetails =
          (ProductVariation)
              entityManager
                  .createQuery("FROM ProductVariation c where c.id =?1")
                  .setParameter(1, productVariationId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getProductVariationDetailsByProductVariationId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVariationDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getproductVariationDetails(long orgId, long branchId) {

    List<Object[]> productVariations = new ArrayList<>();
    try {
      productVariations =
          entityManager
              .createQuery(
                  "select product.id,product.productName,product.hsnNumber,product.productPrice,product.productCount,product.packageFlag,product.includingTaxFlag,product.productCategory.id,product.productCategory.productTypeName,product.productCategory.categoryType.id,product.productCategory.categoryType.categoryName,product.productFeatures,product.image,product.organisation.id,product.organisation.orgName,product.branch.id,product.branch.branchName,product.CGST,product.SGST,product.IGST,product.vegFlag,productVariation.id,productVariation.variationOption.id,productVariation.variationOption.optionName,productVariation.variationOption.optionDisplayName,productVariation.variationOption.quantityMeasurement.id,productVariation.variationOption.quantityMeasurement.quantity,productVariation.variationOption.quantityMeasurement.displayName,productVariation.variationOption.quantityMeasurement.basicUnit,productVariation.variationOption.quantityMeasurement.basicunitDisplayname,productVariation.variationOption.quantityMeasurement.conversionFactor,productVariation.variationType.id,productVariation.variationType.variationName,productVariation.variationType.displayName,productVariation.price,productVariation.description,productVariation.actualWeight,productVariation.code from Product product join ProductVariation productVariation on productVariation.product.id=product.id join VariationOptions variationOptions on variationOptions.id=productVariation.variationOption.id join VariationTypes variationTypes on variationTypes.id=productVariation.variationType.id join QuantityMeasurements quantityMeasurements on quantityMeasurements.id=variationOptions.quantityMeasurement.id where product.organisation.id=?1 and product.branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductVariationDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVariations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getproductVariationDetailsByOrg(long orgId) {

    List<Object[]> productVariations = new ArrayList<>();
    try {
      productVariations =
          entityManager
              .createQuery(
                  "select product.id,product.productName,product.hsnNumber,product.productPrice,product.productCount,product.packageFlag,product.includingTaxFlag,product.productCategory.id,product.productCategory.productTypeName,product.productCategory.categoryType.id,product.productCategory.categoryType.categoryName,product.productFeatures,product.image,product.organisation.id,product.organisation.orgName,product.branch.id,product.branch.branchName,product.CGST,product.SGST,product.IGST,product.vegFlag,productVariation.id,productVariation.variationOption.id,productVariation.variationOption.optionName,productVariation.variationOption.optionDisplayName,productVariation.variationOption.quantityMeasurement.id,productVariation.variationOption.quantityMeasurement.quantity,productVariation.variationOption.quantityMeasurement.displayName,productVariation.variationOption.quantityMeasurement.basicUnit,productVariation.variationOption.quantityMeasurement.basicunitDisplayname,productVariation.variationOption.quantityMeasurement.conversionFactor,productVariation.variationType.id,productVariation.variationType.variationName,productVariation.variationType.displayName,productVariation.price,productVariation.description,productVariation.actualWeight,productVariation.code from Product product join ProductVariation productVariation on productVariation.product.id=product.id join VariationOptions variationOptions on variationOptions.id=productVariation.variationOption.id join VariationTypes variationTypes on variationTypes.id=productVariation.variationType.id join QuantityMeasurements quantityMeasurements on quantityMeasurements.id=variationOptions.quantityMeasurement.id where product.organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getproductVariationDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVariations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductVariation> getProductVariationByProductIdAndOrgBranchId(
      long productId, long orgId, long branchId) {

    List<ProductVariation> productVariations = new ArrayList<>();
    try {
      productVariations =
          entityManager
              .createQuery(
                  "from ProductVariation where product.id=?1 and organisation.id=?2 and branch.id=?3")
              .setParameter(1, productId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductVariationByProductIdAndOrgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVariations;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductVariation> getProductVariationByProductIdAndOrg(long productId, long orgId) {

    List<ProductVariation> productVariations = new ArrayList<>();
    try {
      productVariations =
          entityManager
              .createQuery("from ProductVariation where product.id=?1 and organisation.id=?2")
              .setParameter(1, productId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getProductVariationByProductIdAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productVariations;
  }
}
