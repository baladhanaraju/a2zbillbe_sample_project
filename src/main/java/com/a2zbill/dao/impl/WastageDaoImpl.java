package com.a2zbill.dao.impl;

import com.a2zbill.dao.WastageDao;
import com.a2zbill.domain.Wastage;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("wastageDao")
@SuppressWarnings("unchecked")
public class WastageDaoImpl implements WastageDao {

  private static final Logger logger = LoggerFactory.getLogger(WastageDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Wastage wastage) {
    try {
      entityManager.persist(wastage);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public Boolean update(Wastage wastage) {
    try {
      entityManager.merge(wastage);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public Boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public List<Wastage> getAllWastages() {

    List<Wastage> wastageDetails = new ArrayList<>();
    try {
      wastageDetails = this.entityManager.createQuery("from Wastage order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllWastages ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return wastageDetails;
  }
}
