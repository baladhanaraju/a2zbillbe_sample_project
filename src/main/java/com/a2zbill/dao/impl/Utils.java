package com.a2zbill.dao.impl;

import javax.persistence.EntityManager;

public final class Utils {

  private Utils() {}

  public static void closeEntityManager(final EntityManager entityManager) {
    if (entityManager != null) {
      entityManager.close();
    }
  }
}
