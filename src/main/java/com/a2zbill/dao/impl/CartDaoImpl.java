package com.a2zbill.dao.impl;

import com.a2zbill.dao.CartDao;
import com.a2zbill.domain.Cart;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CartDaoImpl implements CartDao {

  private static final Logger logger = LoggerFactory.getLogger(CartDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Cart cart) {
    try {
      entityManager.persist(cart);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(Cart cart) {
    try {
      entityManager.merge(cart);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Cart> getAllCartDetails() {

    List<Cart> cartDetails = new ArrayList<>();
    try {
      cartDetails = entityManager.createQuery("from Cart order by cart_id").getResultList();
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public Cart getCartDetailsOnStatus(final String cartStatus, final long counterId) {

    Cart cartDetails = null;
    try {
      cartDetails =
          (Cart)
              entityManager
                  .createQuery("from Cart where cartStatus=?1 and counterDetails.counterNumber=?2")
                  .setParameter(1, cartStatus)
                  .setParameter(2, counterId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCartDetailsOnStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Cart> getCartDetailsByCounterId(final long counterId) {

    List<Cart> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager
              .createQuery("from Cart where counterDetails.counterNumber=?1")
              .setParameter(1, counterId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCounterId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public Cart getCartDetailsByCartId(final long cartId) {

    Cart cartDetails = null;
    try {
      cartDetails =
          (Cart)
              entityManager
                  .createQuery("from Cart where cartId=?1")
                  .setParameter(1, cartId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCartId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Cart> getCartDetailsByStatus() {

    List<Cart> cartDetails = new ArrayList<>();
    try {
      cartDetails =
          entityManager.createQuery("from Cart where cartStatus like 'Open' ").getResultList();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCartId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public Cart getCartByGuid(final String guid) {

    Cart cartDetails = null;
    try {
      cartDetails =
          (Cart)
              this.entityManager
                  .createQuery("from Cart where guid = ? 1")
                  .setParameter(1, guid)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCartDetailsByCartId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }

  @Override
  public Cart getCartDetailsByStatus(String cartStatus, long customerId, long orgId) {
    Cart cartDetails = null;
    try {
      cartDetails =
          (Cart)
              entityManager
                  .createQuery(
                      "from Cart where cartStatus=?1 and customer.customerId=?2 and organisation.id=?3")
                  .setParameter(1, cartStatus)
                  .setParameter(2, customerId)
                  .setParameter(3, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCartDetailsOnStatus ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cartDetails;
  }
}
