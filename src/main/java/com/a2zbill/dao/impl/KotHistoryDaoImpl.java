package com.a2zbill.dao.impl;

import com.a2zbill.dao.KotHistoryDao;
import com.a2zbill.domain.KotHistory;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class KotHistoryDaoImpl implements KotHistoryDao {

  private static final Logger logger = LoggerFactory.getLogger(KotHistoryDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(KotHistory kotHistory) {
    try {
      entityManager.persist(kotHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void update(KotHistory kotHistory) {
    try {
      entityManager.merge(kotHistory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }
}
