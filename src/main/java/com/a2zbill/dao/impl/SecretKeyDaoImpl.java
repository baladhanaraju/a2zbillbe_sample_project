package com.a2zbill.dao.impl;

import com.a2zbill.dao.SecretKeyDao;
import com.a2zbill.domain.SecretKey;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SecretKeyDaoImpl implements SecretKeyDao {

  private static final Logger logger = LoggerFactory.getLogger(SecretKeyDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(SecretKey secret) {
    try {
      entityManager.persist(secret);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public void update(SecretKey secret) {
    try {
      entityManager.merge(secret);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      Object result = entityManager.find(SecretKey.class, id);
      if (result != null) {
        entityManager.remove(result);
        return true;
      }
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<SecretKey> getAllSecret() {

    List<SecretKey> secretDetails = new ArrayList<>();
    try {
      secretDetails = entityManager.createQuery("from SecretKey order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSecret ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return secretDetails;
  }

  @Override
  public SecretKey getSecretKeyById(final long id) {

    SecretKey secretKey = null;
    try {
      secretKey =
          (SecretKey)
              entityManager
                  .createQuery("from SecretKey where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getSecretKeyById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return secretKey;
  }

  @Override
  public SecretKey getDetailsBySecretKey(final String secretKey) {

    SecretKey secretKeyDetails = null;
    try {
      secretKeyDetails =
          (SecretKey)
              entityManager
                  .createQuery("from SecretKey where secretKey=?1")
                  .setParameter(1, secretKey)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getDetailsBySecretKey ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return secretKeyDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<SecretKey> getAllSecretByOrgIdBranchId(long orgId, long branchId) {

    List<SecretKey> secretKey = new ArrayList<>();
    try {
      secretKey =
          entityManager
              .createQuery("from SecretKey where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSecretByOrgIdBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return secretKey;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<SecretKey> getAllSecretByOrgId(long orgId) {

    List<SecretKey> secretKey = new ArrayList<>();
    try {
      secretKey =
          entityManager
              .createQuery("from SecretKey where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSecretByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return secretKey;
  }
}
