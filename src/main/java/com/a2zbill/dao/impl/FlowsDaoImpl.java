package com.a2zbill.dao.impl;

import com.a2zbill.dao.FlowsDao;
import com.a2zbill.domain.Flows;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FlowsDaoImpl implements FlowsDao {

  private static final Logger logger = LoggerFactory.getLogger(FlowsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final Flows flows) {
    try {
      entityManager.persist(flows);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public Flows getFlowsById(final Long id) {

    Flows flows = null;
    try {
      flows =
          (Flows)
              entityManager
                  .createQuery("from Flows where id=?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFlowsById ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return flows;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Flows> getFlowsDetails() {

    List<Flows> flows = new ArrayList<>();
    try {
      flows = entityManager.createQuery("from Flows order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getFlowsDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return flows;
  }
}
