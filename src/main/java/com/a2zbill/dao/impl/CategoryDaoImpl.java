package com.a2zbill.dao.impl;

import com.a2zbill.dao.CategoryDao;
import com.a2zbill.domain.Category;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDaoImpl implements CategoryDao {

  private static final Logger logger = LoggerFactory.getLogger(CategoryDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Category category) {
    try {
      entityManager.persist(category);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(Category category) {
    try {
      entityManager.merge(category);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Category> getAllCategories() {

    List<Category> productsDetails = new ArrayList<>();
    try {
      productsDetails =
          entityManager.createQuery("from Category order by category_id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCategories ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public Category getCategoryByName(final String categoryName) {

    Category categoryDetails = null;
    try {
      categoryDetails =
          (Category)
              entityManager
                  .createQuery("from Category where categoryName=?1")
                  .setParameter(1, categoryName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCategoryByName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return categoryDetails;
  }
}
