package com.a2zbill.dao.impl;

import com.a2zbill.dao.BillingReportDao;
import com.a2zbill.domain.BillingReport;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BillingReportDaoImpl implements BillingReportDao {

  private static final Logger logger = LoggerFactory.getLogger(BillingReportDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(BillingReport billingreport) {
    try {
      entityManager.persist(billingreport);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(BillingReport billingreport) {
    try {
      entityManager.merge(billingreport);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BillingReport> getAllBillingReport() {

    List<BillingReport> billingReport = new ArrayList<>();
    try {
      billingReport =
          entityManager.createQuery("from BillingReport order by report_id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllBillingReport ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingReport;
  }

  @Override
  public BillingReport getBillingReportByEmpid(final long empid, final Date date) {

    BillingReport billingReport = null;
    try {
      billingReport =
          (BillingReport)
              entityManager
                  .createQuery("from BillingReport where employee.id = ?1 and date = ?2")
                  .setParameter(1, empid)
                  .setParameter(2, date)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingReportByEmpid ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingReport;
  }
}
