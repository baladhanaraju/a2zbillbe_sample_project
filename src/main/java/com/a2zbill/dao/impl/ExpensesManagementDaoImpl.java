package com.a2zbill.dao.impl;

import com.a2zbill.dao.ExpensesManagementDao;
import com.a2zbill.domain.ExpensesManagement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ExpensesManagementDaoImpl implements ExpensesManagementDao {

  private static final Logger logger = LoggerFactory.getLogger(ExpensesManagementDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final ExpensesManagement expensesManagement) {
    try {
      entityManager.persist(expensesManagement);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final ExpensesManagement expensesManagement) {
    try {
      entityManager.remove(expensesManagement);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(final ExpensesManagement expensesManagement) {
    try {
      entityManager.merge(expensesManagement);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public ExpensesManagement getExpensesManagementById(final long id) {

    ExpensesManagement expensesManagement = null;
    try {
      expensesManagement =
          (ExpensesManagement)
              this.entityManager
                  .createQuery("from ExpensesManagement c where c.id = ?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getExpensesManagementById ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return expensesManagement;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExpensesManagement> getAllExpensesManagements() {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement =
          entityManager.createQuery("from ExpensesManagement order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllExpensesManagements ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return expensesManagement;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExpensesManagement> getExpensesByOrgIdAndBranchId(
      final long orgId, final long branchId) {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement =
          entityManager
              .createQuery("from ExpensesManagement where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getExpensesByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return expensesManagement;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExpensesManagement> getExpensesByOrgId(final long orgId) {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement =
          entityManager
              .createQuery("from ExpensesManagement where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getExpensesByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return expensesManagement;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExpensesManagement> getExpensesByOrgIdAndBranchIdAndDate(
      final long orgId, final long branchId, final Date date) {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement =
          entityManager
              .createQuery(
                  "from ExpensesManagement where organisation.id=?1 and branch.id=?2 and date =?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, date)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getExpensesByOrgIdAndBranchIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return expensesManagement;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExpensesManagement> getExpensesByOrgIdAndDate(final long orgId, final Date date) {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement =
          entityManager
              .createQuery("from ExpensesManagement where organisation.id=?1 and date=?2")
              .setParameter(1, orgId)
              .setParameter(2, date)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getExpensesByOrgIdAndDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return expensesManagement;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExpensesManagement> getExpensesDetailsByOrgAndBranch(
      final Date fromDate, final Date toDate, final long orgId, final long branchId) {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement =
          entityManager
              .createQuery(
                  "from ExpensesManagement where date>=?1 and date<=?2 and organisation.id=?3 and branch.id=?4")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getExpensesDetailsByOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return expensesManagement;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ExpensesManagement> getExpensesDetailsByOrgAndBranchisNUll(
      final Date fromDate, final Date toDate, final long orgId) {

    List<ExpensesManagement> expensesManagement = new ArrayList<>();
    try {
      expensesManagement =
          entityManager
              .createQuery(
                  "from ExpensesManagement where date>=?1 and date<=?2 and organisation.id=?3 ")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getExpensesDetailsByOrgAndBranchisNUll ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return expensesManagement;
  }
}
