package com.a2zbill.dao.impl;

import com.a2zbill.dao.FlowTypeDao;
import com.a2zbill.domain.FlowType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FlowTypeDaoImpl implements FlowTypeDao {

  private static final Logger logger = LoggerFactory.getLogger(FlowTypeDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(FlowType flowType) {
    try {
      entityManager.persist(flowType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(FlowType flowType) {
    try {
      entityManager.merge(flowType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public FlowType getFlowTypeById(final Long id) {

    FlowType flowType = null;
    try {
      flowType =
          (FlowType)
              entityManager
                  .createQuery("FROM FlowType where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getFlowTypeById ", ex);
    } finally {
      if (entityManager != null) entityManager.close();
    }
    return flowType;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<FlowType> getAllFlowTypeDetails() {

    List<FlowType> flowType = new ArrayList<>();
    try {
      flowType = entityManager.createQuery("from FlowType order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllFlowTypeDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return flowType;
  }
}
