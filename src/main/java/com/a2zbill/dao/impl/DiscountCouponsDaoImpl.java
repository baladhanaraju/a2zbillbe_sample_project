package com.a2zbill.dao.impl;

import com.a2zbill.dao.DiscountCouponsDao;
import com.a2zbill.domain.DiscountCoupons;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DiscountCouponsDaoImpl implements DiscountCouponsDao {

  private static final Logger logger = LoggerFactory.getLogger(DiscountCouponsDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final DiscountCoupons discountCoupons) {
    try {
      entityManager.persist(discountCoupons);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(final Long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(final DiscountCoupons discountCoupons) {
    try {
      entityManager.merge(discountCoupons);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public DiscountCoupons getDiscountCouponsById(final Long id) {
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<DiscountCoupons> getAllDiscountCoupons() {

    List<DiscountCoupons> discountCoupons = new ArrayList<>();
    try {
      discountCoupons =
          entityManager.createQuery("from DiscountCoupons order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllDiscountCoupons ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return discountCoupons;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Map<String, Object> getDealNameById(final Long id) {

    Map<String, Object> dealNameDetails = null;
    try {
      dealNameDetails =
          (Map<String, Object>)
              this.entityManager
                  .createQuery("select dealName FROM DiscountCoupons where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getDealNameById ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return dealNameDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Map<String, Object>> getAllDealNames() {

    List<Map<String, Object>> dealNameDetails = new ArrayList<>();
    try {
      dealNameDetails =
          entityManager.createQuery("select dealName FROM DiscountCoupons ").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllDealNames ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return dealNameDetails;
  }

  @Override
  public DiscountCoupons getDiscountCouponsByOfferCode(final String offerCode) {

    DiscountCoupons offerDetails = null;
    try {
      offerDetails =
          (DiscountCoupons)
              this.entityManager
                  .createQuery("FROM DiscountCoupons where offerCode = ?1")
                  .setParameter(1, offerCode)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getDiscountCouponsByOfferCode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return offerDetails;
  }
}
