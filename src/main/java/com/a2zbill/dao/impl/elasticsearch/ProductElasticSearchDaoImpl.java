package com.a2zbill.dao.impl.elasticsearch;

import com.a2zbill.dao.elasticsearch.ProductElasticSearchDao;
import com.a2zbill.domain.elasticsearch.ProductElasticSearch;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.*;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ProductElasticSearchDaoImpl implements ProductElasticSearchDao {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductElasticSearchDaoImpl.class);

  private final String INDEX = "productsdata";
  private final String TYPE = "productsdetails";
  private RestHighLevelClient restHighLevelClient;
  private ObjectMapper objectMapper;

  public ProductElasticSearchDaoImpl(
      final ObjectMapper objectMapper, final RestHighLevelClient restHighLevelClient) {
    this.objectMapper = objectMapper;
    this.restHighLevelClient = restHighLevelClient;
  }

  public ProductElasticSearch insertProduct(final ProductElasticSearch product) {
    // product.setId(UUID.randomUUID());
    @SuppressWarnings("unchecked")
    final Map<Object, Object> dataMap = objectMapper.convertValue(product, Map.class);
    //    final IndexRequest indexRequest =
    //        new IndexRequest(INDEX, TYPE, UUID.randomUUID().toString()).source(dataMap);
    //
    final IndexRequest indexRequest =
        new IndexRequest(INDEX, TYPE, UUID.randomUUID().toString())
            .source(dataMap, XContentType.JSON);
    try {
      final IndexResponse response =
          restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
      LOGGER.info("Elastic search response::::" + response);
    } catch (final ElasticsearchException e) {
      e.getDetailedMessage();
      LOGGER.error(" Elastic save response::::" + e);
    } catch (final java.io.IOException ex) {
      LOGGER.error(" Elastic save response::::" + ex);
      ex.getLocalizedMessage();
    }
    return product;
  }

  public List<ProductElasticSearch> getProductByProductName(final String productName) {
    final SearchRequest searchRequest = new SearchRequest();
    try {
      searchRequest.indices(INDEX);
      searchRequest.types(TYPE);
      final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      final MatchQueryBuilder matchQueryBuilder =
          QueryBuilders.matchQuery("productName", productName).operator(Operator.AND);
      searchSourceBuilder.query(matchQueryBuilder);
      searchRequest.source(searchSourceBuilder);
      final SearchResponse searchResponse =
          restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
      return getSearchResult(searchResponse);
    } catch (final Exception e) {
      LOGGER.error("Elastic get response::::" + e);
    }
    return new ArrayList<>();
  }

  private List<ProductElasticSearch> getSearchResult(final SearchResponse response) {
    final List<ProductElasticSearch> profileDocuments = new ArrayList<>();
    try {
      final SearchHit[] searchHit = response.getHits().getHits();
      for (final SearchHit hit : searchHit) {
        profileDocuments.add(
            objectMapper.convertValue(hit.getSourceAsMap(), ProductElasticSearch.class));
      }
      return profileDocuments;
    } catch (final Exception e) {
      LOGGER.error("Elastic get Search response::::" + e);
    }
    return new ArrayList<>();
  }

  private ProductElasticSearch getSingleSearchResult(final SearchResponse response) {
    final List<ProductElasticSearch> profileDocuments = new ArrayList<>();
    try {
      final SearchHit[] searchHit = response.getHits().getHits();
      for (final SearchHit hit : searchHit) {
        profileDocuments.add(
            objectMapper.convertValue(hit.getSourceAsMap(), ProductElasticSearch.class));
      }
      if (profileDocuments.size() > 0) {
        LOGGER.info(
            "Elastic get single product  Search by prodId response::::"
                + profileDocuments.get(0).toString());
        return profileDocuments.get(0);
      }
    } catch (final Exception e) {
      LOGGER.error("Elastic get Search response::::" + e);
    }
    return null;
  }

  public List<ProductElasticSearch> getProductByProductNameAndBranchIdAndOrgId(
      final String searchKey, final long branchId, final long orgId) {
    try {
      final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
      final SearchRequest searchRequest = new SearchRequest();
      searchRequest.indices(INDEX);
      searchRequest.types(TYPE);
      // sourceBuilder.timeout(new TimeValue(600, TimeUnit.SECONDS)); // Request
      // timeout
      // sourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC)); //Result
      // set ordering
      final BoolQueryBuilder query = new BoolQueryBuilder();
      query.must(new MatchQueryBuilder("productName", searchKey));
      // query.must(new MatchQueryBuilder("code", searchKey));
      // query.must(new MatchQueryBuilder("productPrice", searchKey));
      query.must(new MatchQueryBuilder("branchId", branchId));
      query.must(new MatchQueryBuilder("organisationId", orgId));
      sourceBuilder.query(query);
      // SearchRequest searchRequest = new SearchRequest("comment");
      searchRequest.source(sourceBuilder);
      final SearchResponse searchResponse =
          restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
      return getSearchResult(searchResponse);
      // final SearchResponse searchResponse = esClient.search(searchRequest);

      // SearchHits hits = searchResponse.getHits();
    } catch (final Exception e) {
      LOGGER.error("Elastic get multi Search response::::" + e);
    }
    return new ArrayList<>();
  }

  @Override
  public ProductElasticSearch getProductByProductIdAndBranchIdAndOrgId(
      final Long productId, long branchId, long orgId) {
    try {
      final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
      final SearchRequest searchRequest = new SearchRequest();
      searchRequest.indices(INDEX);
      searchRequest.types(TYPE);
      // sourceBuilder.timeout(new TimeValue(600, TimeUnit.SECONDS)); // Request
      // timeout
      // sourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC)); //Result
      // set ordering
      final BoolQueryBuilder query = new BoolQueryBuilder();
      query.must(new MatchQueryBuilder("id", productId));
      query.must(new MatchQueryBuilder("branchId", branchId));
      query.must(new MatchQueryBuilder("organisationId", orgId));
      sourceBuilder.query(query);
      // SearchRequest searchRequest = new SearchRequest("comment");
      searchRequest.source(sourceBuilder);
      final SearchResponse searchResponse =
          restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
      return getSingleSearchResult(searchResponse);
      // final SearchResponse searchResponse = esClient.search(searchRequest);

      // SearchHits hits = searchResponse.getHits();
    } catch (final Exception e) {
      LOGGER.error("Elastic get multi Search response::::" + e);
    }
    return null;
  }

  public List<ProductElasticSearch> getProductByProductNameAndOrgId(
      final String searchKey, final long orgId) {
    try {
      final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
      final SearchRequest searchRequest = new SearchRequest();
      searchRequest.indices(INDEX);
      searchRequest.types(TYPE);
      // sourceBuilder.timeout(new TimeValue(600, TimeUnit.SECONDS)); // Request
      // timeout
      // sourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC)); //Result
      // set ordering
      final BoolQueryBuilder query = new BoolQueryBuilder();
      query.must(new MatchQueryBuilder("productName", searchKey));
      // query.must(new MatchQueryBuilder("code", searchKey));
      // query.must(new MatchQueryBuilder("productPrice", searchKey));
      query.must(new MatchQueryBuilder("organisationId", orgId));
      sourceBuilder.query(query);
      // SearchRequest searchRequest = new SearchRequest("comment");
      searchRequest.source(sourceBuilder);
      final SearchResponse searchResponse =
          restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
      return getSearchResult(searchResponse);
      // final SearchResponse searchResponse = esClient.search(searchRequest);

      // SearchHits hits = searchResponse.getHits();
    } catch (final Exception e) {
      LOGGER.error("Elastic get multi Search response::::" + e);
    }
    return new ArrayList<>();
  }

  @Override
  public ProductElasticSearch getProductByProductIdAndOrgId(
      final Long productId, final long orgId) {
    try {
      final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
      final SearchRequest searchRequest = new SearchRequest();
      searchRequest.indices(INDEX);
      searchRequest.types(TYPE);
      // sourceBuilder.timeout(new TimeValue(600, TimeUnit.SECONDS)); // Request
      // timeout
      // sourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC)); //Result
      // set ordering
      final BoolQueryBuilder query = new BoolQueryBuilder();
      query.must(new MatchQueryBuilder("id", productId));
      query.must(new MatchQueryBuilder("organisationId", orgId));
      sourceBuilder.query(query);
      // SearchRequest searchRequest = new SearchRequest("comment");
      searchRequest.source(sourceBuilder);
      final SearchResponse searchResponse =
          restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
      return getSingleSearchResult(searchResponse);
      // final SearchResponse searchResponse = esClient.search(searchRequest);

      // SearchHits hits = searchResponse.getHits();
    } catch (final Exception e) {
      LOGGER.error("Elastic get multi Search response::::" + e);
    }
    return null;
  }

  public List<ProductElasticSearch> getProductsByBranchIdAndOrgIdProductNameAndPriceRange(
      final long branchId,
      final long orgId,
      final String productName,
      final BigDecimal lowPrice,
      final BigDecimal highPrice) {
    final SearchRequest searchRequest = new SearchRequest();
    try {
      searchRequest.indices(INDEX);
      searchRequest.types(TYPE);
      final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      // productName name
      final BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
      boolQueryBuilder.must(QueryBuilders.matchQuery("productName", productName));
      boolQueryBuilder.must(QueryBuilders.matchQuery("branchId", branchId));
      boolQueryBuilder.must(QueryBuilders.matchQuery("organisationId", orgId));
      // price range
      boolQueryBuilder.filter(
          QueryBuilders.rangeQuery("productPrice").from(lowPrice).to(highPrice));
      searchSourceBuilder.query(boolQueryBuilder);
      searchRequest.source(searchSourceBuilder);
      final SearchResponse searchResponse =
          restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
      return getSearchResult(searchResponse);

    } catch (final Exception e) {
      LOGGER.error("Elastic search product range  response::::" + e);
    }
    return new ArrayList<>();
  }

  public List<ProductElasticSearch> getProductsByOrgIdAndProductNameAndPriceRange(
      final long orgId,
      final String productName,
      final BigDecimal lowPrice,
      final BigDecimal highPrice) {
    final SearchRequest searchRequest = new SearchRequest();
    try {
      searchRequest.indices(INDEX);
      searchRequest.types(TYPE);
      final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      // productName name
      final BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
      boolQueryBuilder.must(QueryBuilders.matchQuery("productName", productName));
      boolQueryBuilder.must(QueryBuilders.matchQuery("organisationId", orgId));
      // price range
      boolQueryBuilder.filter(
          QueryBuilders.rangeQuery("productPrice").from(lowPrice).to(highPrice));
      searchSourceBuilder.query(boolQueryBuilder);
      searchRequest.source(searchSourceBuilder);
      final SearchResponse searchResponse =
          restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
      return getSearchResult(searchResponse);

    } catch (final Exception e) {
      LOGGER.error("Elastic search product range  response::::" + e);
    }
    return new ArrayList<>();
  }
}
