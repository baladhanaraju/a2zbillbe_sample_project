package com.a2zbill.dao.impl;

import com.a2zbill.dao.ProductCategoryDao;
import com.a2zbill.domain.ProductCategory;
import com.tsss.basic.domain.MyJson;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductCategoryDaoImpl implements ProductCategoryDao {

  private static final Logger logger = LoggerFactory.getLogger(ProductCategoryDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(final ProductCategory productCategory) {
    try {
      entityManager.persist(productCategory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getAllProductCategory() {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager.createQuery("from ProductCategory order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllProductCategory", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @Override
  public ProductCategory getProductCategoryByProductCategoryName(final String productCategoryName) {

    ProductCategory productCategory = null;
    try {
      productCategory =
          (ProductCategory)
              entityManager
                  .createQuery("from ProductCategory where productTypeName =?1")
                  .setParameter(1, productCategoryName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getProductCategoryByProductCategoryName", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @Override
  public MyJson getFeaturesById(final Long id) {

    MyJson productsDetails = null;
    try {
      productsDetails =
          (MyJson)
              entityManager
                  .createQuery("select features FROM ProductCategory where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getFeaturesById", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productsDetails;
  }

  @Override
  public ProductCategory gettFeaturesById(final Long id) {

    ProductCategory productCategory = null;
    try {
      productCategory =
          (ProductCategory)
              entityManager
                  .createQuery("select features FROM ProductCategory where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" gettFeaturesById", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @Override
  public ProductCategory getProductCategoryById(final Long id) {

    ProductCategory productCategory = null;
    try {
      productCategory =
          (ProductCategory)
              entityManager
                  .createQuery("from ProductCategory where id =?1")
                  .setParameter(1, id)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getProductCategoryById", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategorys(final long orgId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where organisation.id=?1 and categoryType.id=2 ORDER BY id ASC")
              .setParameter(1, orgId)
              .getResultList();

    } catch (final Exception ex) {
      logger.error(" getProductCategorys", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategorysByOrgAndBranchId(
      final long branchId, final long orgId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where (branch.id=?1 or branch.id is null) and organisation.id=?2 and categoryType.id=2 ORDER BY id ASC")
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategorysByOrgAndBranchId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @Override
  public ProductCategory getProductCategoryByProductCategoryNameorgIdAndbrnachId(
      final String productCategoryName, final long orgId, final long brnachId) {

    ProductCategory productCategory = null;
    try {
      productCategory =
          (ProductCategory)
              entityManager
                  .createQuery(
                      "from ProductCategory where productTypeName=?1 and organisation.id=?2 and branch.id=?3")
                  .setParameter(1, productCategoryName)
                  .setParameter(2, orgId)
                  .setParameter(3, brnachId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getProductCategoryByProductCategoryNameorgIdAndbrnachId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategorysBasedBranchIdAndOrgId(
      final long branchId, final long orgId) {
    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where (branch.id=?1 or branch.id is null) and organisation.id=?2 order by id ASC")
              .setParameter(1, branchId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategorysBasedBranchIdAndOrgId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategorysBasedBranchIdAndOrgIdAndParentId(
      final long branchId, final long orgId, final long categoryTypeId, final long parentId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where organisation.id=?1 and branch.id=?2 and categoryType.id=?3 and parentId=?4")
              .setParameter(2, branchId)
              .setParameter(1, orgId)
              .setParameter(3, categoryTypeId)
              .setParameter(4, parentId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategorysBasedBranchIdAndOrgIdAndParentId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoriesIfRedeemFlagIsTrue(
      final long branchId, final long orgId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where redeemFlag = true and organisation.id =?1 and branch.id =?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategoriesIfRedeemFlagIsTrue", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoriesIfRedeemFlagIsFalseAndOrgIdAndBranchId(
      final long branchId, final long orgId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where redeemFlag = false and organisation.id =?1 and branch.id =?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategoriesIfRedeemFlagIsFalseAndOrgIdAndBranchId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @Override
  public ProductCategory getProductCategoryByProductCategoryNameorgId(
      final String productCategoryName, final long orgId) {

    ProductCategory productCategory = null;
    try {
      productCategory =
          (ProductCategory)
              entityManager
                  .createQuery(
                      "from ProductCategory where productTypeName =?1 and organisation.id=?2 ")
                  .setParameter(1, productCategoryName)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getProductCategoryByProductCategoryNameorgId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategorysBasedBranchIdAndOrgId(
      final long branchId, final long orgId, final long categoryTypeId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where organisation.id=?1 and branch.id=?2 and categoryType.id=?3")
              .setParameter(2, branchId)
              .setParameter(1, orgId)
              .setParameter(3, categoryTypeId)
              .getResultList();

    } catch (final Exception ex) {
      logger.error(" getProductCategorysBasedBranchIdAndOrgId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoryByBranchIdAndOrgIdAndParentId(
      final long branchId, final long orgId, final long parentId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where organisation.id=?1 and branch.id=?2 and parentId=?3")
              .setParameter(2, branchId)
              .setParameter(1, orgId)
              .setParameter(3, parentId)
              .getResultList();

    } catch (final Exception ex) {
      logger.error(" getProductCategoryByBranchIdAndOrgIdAndParentId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoriesRedeemFlagIsTrue(final long orgId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery("from ProductCategory where redeemFlag = true and organisation.id =?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategoriesRedeemFlagIsTrue", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoriesIfRedeemFlagIsFalseAndOrgId(final long orgId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery("from ProductCategory where redeemFlag = false and organisation.id =?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategoriesIfRedeemFlagIsFalseAndOrgId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategorysBasedOnOrgIdAndParentId(
      final long orgId, final long categoryTypeId, final long parentId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery(
                  "from ProductCategory where organisation.id=?1 and  categoryType.id=?2 and parentId=?3")
              .setParameter(1, orgId)
              .setParameter(2, categoryTypeId)
              .setParameter(3, parentId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategorysBasedOnOrgIdAndParentId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoryByOrgIdAndParentId(
      final long orgId, final long parentId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery("from ProductCategory where organisation.id=?1  and parentId=?2")
              .setParameter(1, orgId)
              .setParameter(2, parentId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategoryByOrgIdAndParentId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoriesByCategoryTypeId(final long categoryTypeId) {

    List<ProductCategory> productCategories = new ArrayList<>();
    try {
      productCategories =
          entityManager
              .createQuery("from ProductCategory where categoryType.id=?1")
              .setParameter(1, categoryTypeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategoriesByCategoryTypeId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategories;
  }

  @Override
  public boolean update(final ProductCategory productCategory) {

    try {
      entityManager.merge(productCategory);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<ProductCategory> getProductCategoriesBasedOnOrgIdAndCategoryType(
      final long orgId, final long categoryTypeId) {

    List<ProductCategory> productCategory = new ArrayList<>();
    try {
      productCategory =
          entityManager
              .createQuery("from ProductCategory where organisation.id=?1 and categoryType.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, categoryTypeId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(" getProductCategoriesBasedOnOrgIdAndCategoryType", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @Override
  public ProductCategory getProductCategoryByProductTypeName(final String productTypeName) {

    ProductCategory productTypeDetails = null;
    try {
      productTypeDetails =
          (ProductCategory)
              entityManager
                  .createQuery("from ProductCategory  where productTypeName=?1")
                  .setParameter(1, productTypeName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error(" getProductCategoryByProductTypeName", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productTypeDetails;
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<String> getProductCategoryNames(
      final long branchId, final long orgId, final long categoryTypeId) {
    List<String> productCategory = new ArrayList<>();
    try {
      if (branchId != 0 && orgId != 0) {
        productCategory =
            entityManager
                .createQuery(
                    "select DISTINCT categoryType.categoryName from ProductCategory where (branch.id=?1 or branch.id is null) and organisation.id=?2 and categoryType.id=1")
                .setParameter(1, branchId)
                .setParameter(2, orgId)
                .getResultList();
      } else if (orgId != 0) {
        productCategory =
            entityManager
                .createQuery(
                    "select DISTINCT categoryType.categoryName from ProductCategory where organisation.id=?1")
                .setParameter(1, orgId)
                .getResultList();
      } else if (categoryTypeId != 0) {
        productCategory =
            entityManager
                .createQuery(
                    "select DISTINCT categoryType.categoryName from ProductCategory where categoryType.id=?1")
                .setParameter(1, categoryTypeId)
                .getResultList();
      }
    } catch (final Exception ex) {
      logger.error(" getProductCategorys", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getProductCategoryNameAndIdMap(final long branchId, final long orgId) {
    try {
      if (branchId != 0) {
        return entityManager
            .createQuery(
                "select id, productTypeName from ProductCategory where (branch.id=?1 or branch.id is null) and organisation.id=?2 order by id ASC")
            .setParameter(1, branchId)
            .setParameter(2, orgId)
            .getResultList();
      } else {
        return entityManager
            .createQuery(
                "select id, productTypeName from ProductCategory where organisation.id=?1 ORDER BY id ASC")
            .setParameter(1, orgId)
            .getResultList();
      }
    } catch (final Exception ex) {
      logger.error(" getProductCategorysBasedBranchIdAndOrgId", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return new ArrayList<>();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Long> getProductCategoryIds(
      final long branchId, final long orgId, final long categoryTypeId) {
    List<Long> productCategory = new ArrayList<>();
    try {
      if (branchId != 0 && orgId != 0) {
        productCategory =
            entityManager
                .createQuery(
                    "select id from ProductCategory where (branch.id=?1 or branch.id is null) and organisation.id=?2 and categoryType.id=1")
                .setParameter(1, branchId)
                .setParameter(2, orgId)
                .getResultList();
      } else if (orgId != 0) {
        productCategory =
            entityManager
                .createQuery("select id from ProductCategory where organisation.id=?1")
                .setParameter(1, orgId)
                .getResultList();
      } else if (categoryTypeId != 0) {
        productCategory =
            entityManager
                .createQuery("select id from ProductCategory where categoryType.id=?1")
                .setParameter(1, categoryTypeId)
                .getResultList();
      }
    } catch (final Exception ex) {
      logger.error(" getProductCategorys", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return productCategory;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getCategoriesBycategoryType(long categoryId, String pathUrl) {
    List<Object[]> object = new ArrayList<>();
    try {
      object =
          entityManager
              .createQuery(
                  "select id,productTypeName from ProductCategory where categoryType.id=?1 and organisation.pathUrl=?2")
              .setParameter(1, categoryId)
              .setParameter(2, pathUrl)
              .getResultList();
    } catch (final Exception e) {
      logger.error(" getProductCategorys", e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return object;
  }

  @Override
  public Integer[] getCategoryTypesByStoreTemplateId(long storeTemplateId) {
    Integer[] object = null;
    try {
      object =
          (Integer[])
              entityManager
                  .createQuery("select categoryType from StoreTemplate where id=?1")
                  .setParameter(1, storeTemplateId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCategoryTypesByOrgPathUrl ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return object;
  }

  @Override
  public List<Object[]> getProductCategorysByOrg(long orgId) {
    List<Object[]> object = new ArrayList<>();
    try {
      object =
          entityManager
              .createQuery(
                  "select id,productTypeName from ProductCategory where  organisation.id=?1 order by id ASC")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(" getCartproducts", e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return object;
  }

  @Override
  public List<Object[]> getProductCategorysByOrgAndBranch(long orgId, long branchId) {
    List<Object[]> object = new ArrayList<>();
    try {
      object =
          entityManager
              .createQuery(
                  "select id,productTypeName from ProductCategory where organisation.id=?1 and branch.id=?2 order by id ASC ")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception e) {
      logger.error(" getCartproducts", e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return object;
  }
}
