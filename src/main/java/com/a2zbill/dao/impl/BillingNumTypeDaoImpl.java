package com.a2zbill.dao.impl;

import com.a2zbill.dao.BillingNumTypeDao;
import com.a2zbill.domain.BillingNumType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BillingNumTypeDaoImpl implements BillingNumTypeDao {

  private static final Logger logger = LoggerFactory.getLogger(BillingNumTypeDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(BillingNumType billingNumType) {
    try {
      entityManager.persist(billingNumType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(BillingNumType billingNumType) {
    try {
      entityManager.remove(billingNumType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public boolean update(BillingNumType billingNumType) {
    try {
      entityManager.merge(billingNumType);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BillingNumType> getAllBillingNumType() {

    List<BillingNumType> billingNumType = new ArrayList<>();
    try {
      billingNumType = entityManager.createQuery("from BillingNumType order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllBillingNumType ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingNumType;
  }

  @Override
  public BillingNumType getBillingNumTypeDetailsByBillingTypeName(final String billingTypeName) {

    BillingNumType billingNumType = null;
    try {
      billingNumType =
          (BillingNumType)
              entityManager
                  .createQuery("from BillingNumType where billingTypeName = ?1")
                  .setParameter(1, billingTypeName)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingNumTypeDetailsByBillingTypeName ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingNumType;
  }
}
