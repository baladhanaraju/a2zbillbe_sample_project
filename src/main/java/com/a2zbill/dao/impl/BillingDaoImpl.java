package com.a2zbill.dao.impl;

import com.a2zbill.dao.BillingDao;
import com.a2zbill.domain.Billing;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BillingDaoImpl implements BillingDao {

  private static final Logger logger = LoggerFactory.getLogger(BillingDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(Billing billing) {
    try {
      entityManager.persist(billing);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean update(Billing billing) {
    try {
      entityManager.merge(billing);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @Override
  public Billing getBillingDetailsByCartId(final long cartId) {

    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery("from Billing where cartId.cartId = ?1 ")
                  .setParameter(1, cartId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public Billing getBillingDetailsByBillId(final long billId) {

    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery("from Billing where id = ?1")
                  .setParameter(1, billId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingDetailsByDateAndCounter(
      final Date billingdate, final long counter) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery("from Billing where billDate = ?1 and counterDetails.id = ?2")
              .setParameter(1, billingdate)
              .setParameter(2, counter)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public BigDecimal getBillingCurrentDaySaleAmount(final Date billingdate) {

    BigDecimal currentDaySaleAmount = null;
    try {
      currentDaySaleAmount =
          (BigDecimal)
              entityManager
                  .createQuery("Select SUM(totalAmount) FROM Billing where billDate = ?1")
                  .setParameter(1, billingdate)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingCurrentDaySaleAmount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return currentDaySaleAmount;
  }

  @Override
  public BigDecimal getBillingCurrentMonthSaleAmount(final int Month, final int year) {

    BigDecimal currentMonthSaleAmount = null;
    try {
      currentMonthSaleAmount =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "select SUM(totalAmount) from Billing where EXTRACT(MONTH FROM billDate) = ?1 and EXTRACT(YEAR FROM billDate) = ?2")
                  .setParameter(1, Month)
                  .setParameter(2, year)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingCurrentMonthSaleAmount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return currentMonthSaleAmount;
  }

  @Override
  public BigDecimal getBillingLastSixMonthSaleAmount(final int Month, final int year) {

    BigDecimal sixMonthSaleAmount = null;
    try {
      sixMonthSaleAmount =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "select SUM(totalAmount) from Billing where EXTRACT(MONTH FROM billDate) = ?1 and EXTRACT(YEAR FROM billDate) = ?2")
                  .setParameter(1, Month)
                  .setParameter(2, year)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingLastSixMonthSaleAmount ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return sixMonthSaleAmount;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingsByCurrentDate(final Date billingdate) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery("from Billing where billDate=?1")
              .setParameter(1, billingdate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingsByCurrentDate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingsByCurrentDateAndOrgBranch(
      final long orgId, final long branchId, final Date billingdate) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "from Billing where organisation.id = ?1 and branch.id = ?2 and billDate = ?3")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, billingdate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingDetailsByDateAndCounterAndOrgBranch(
      final long orgId, final long branchId, final Date billingdate, final long counter) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "from Billing where organisation.id =?1 and branch.id = ?2 and billDate = ?3 and counterDetails.id = ?4")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, billingdate)
              .setParameter(4, counter)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingsByCurrentDateAndOrgId(
      final Date billingdate, final Date todate, final long orgId) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery("from Billing where organisation.id = ?3 and billDate BETWEEN ?1 and ?2")
              .setParameter(1, billingdate)
              .setParameter(2, todate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingDetailsByDateAndCounterAndOrgId(
      final long orgId, final Date billingdate, final long counter) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "from Billing where  organisation.id = ?1 and billDate = ?2 and counterDetails.id = ?3")
              .setParameter(1, orgId)
              .setParameter(2, billingdate)
              .setParameter(3, counter)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public Billing getBillingDetailsByBillIdAndOrgIdAndBranchId(
      final long billId, final long orgId, final long branchId) {

    Billing billing = null;
    try {
      billing =
          (Billing)
              this.entityManager
                  .createQuery(
                      "from Billing where  id = ?1 and organisation.id = ?2 and branch.id = ?3")
                  .setParameter(1, billId)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getAllBillingDetailsByDate(final Date date) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          (List<Billing>)
              entityManager
                  .createQuery("from Billing where billDate = ?1")
                  .setParameter(1, date)
                  .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public BigDecimal getBillingCurrentDaySaleAmountByOrgBranch(
      final Date billingDate, final long orgId, final long branchId) {

    BigDecimal currentDaySaleAmount = null;
    try {
      currentDaySaleAmount =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "Select SUM(totalAmount) FROM Billing where billDate = ?1 and organisation.id = ?2 and branch.id = ?3")
                  .setParameter(1, billingDate)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return currentDaySaleAmount;
  }

  @Override
  public BigDecimal getBillingCurrentMonthSaleAmountByorgBranch(
      final int Month, final int year, final long orgId, final long branchId) {

    BigDecimal currentMonthSaleAmount = null;
    try {
      currentMonthSaleAmount =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "select SUM(totalAmount) from Billing where EXTRACT(MONTH FROM billDate) = ?1 and EXTRACT(YEAR FROM billDate) = ?2 and organisation.id = ?3 and branch.id = ?4")
                  .setParameter(1, Month)
                  .setParameter(2, year)
                  .setParameter(3, orgId)
                  .setParameter(4, branchId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return currentMonthSaleAmount;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getAllBillingDetailsByDateAndBrachIdAndOrgId(
      final Date date, final long branchId, final long orgId) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "from Billing where billDate = ?1 and branch.id = ?2 and organisation.id = ?3")
              .setParameter(1, date)
              .setParameter(2, branchId)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getAllBillingDetailsByCurrentDate() {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createNativeQuery(
                  "select branch_id, org_id, sum(total_amount) as branchwisepurchase from product.billings where date=CURRENT_DATE group by id,org_id,branch_id")
              .getResultList();
    } catch (final Exception ex) {
      logger.error("BillingDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BigDecimal> getSumOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {

    List<BigDecimal> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createNativeQuery(
                  "select  SUM(total_amount) from product.billings where  date>= ?1 and date <= ?2  and org_id = ?3 and branch_id = ?4 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSumOfBillingDetailsByBranchAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BigDecimal> getSumOfBillingDetailsByOrgBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {

    List<BigDecimal> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createNativeQuery(
                  "select  SUM(total_amount) from product.billings where  date >= ?1 and date <= ?2  and org_id = ?3 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSumOfBillingDetailsByOrgBranchisNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingsByStartAndEndDateAndOrgAndBranch(
      final Date startDate, final Date endDate, final long orgId, final long branchId) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "from Billing where organisation.id = ?1 and  branch_id = ?2 and billDate BETWEEN ?3 and ?4")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, startDate)
              .setParameter(4, endDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingsByStartAndEndDateAndOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getnetsaleOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId) {

    List<Object[]> billingNetSale = new ArrayList<>();
    try {
      billingNetSale =
          entityManager
              .createNativeQuery(
                  "select COUNT(id),SUM(net_amount) from product.billings where date>= ?1 and date <= ?2 and org_id = ?3 and branch_id = ?4 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getnetsaleOfBillingDetailsByBranchAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingNetSale;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllTxesOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId) {

    List<Object[]> billingTaxes = new ArrayList<>();
    try {
      billingTaxes =
          entityManager
              .createNativeQuery(
                  "select COUNT(id),SUM(gst_amount) from product.billings where date>= ?1 and date <= ?2 and org_id = ?3 and branch_id = ?4 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllTxesOfBillingDetailsByBranchAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingTaxes;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllDiscountOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {

    List<Object[]> billingDiscount = new ArrayList<>();
    try {
      billingDiscount =
          entityManager
              .createNativeQuery(
                  "select COUNT(id), SUM(discount_amount) from product.billings where date>= ?1 and date<= ?2 and org_id = ?3 and branch_id = ?4 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllDiscountOfBillingDetailsByBranchAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingDiscount;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllGrossSalesOfBillingDetailsByBranchAndOrg(
      final Date formDate, final Date endDate, final long orgId, final long branchId) {

    List<Object[]> billingGrossSales = new ArrayList<>();
    try {
      billingGrossSales =
          entityManager
              .createNativeQuery(
                  "select COUNT(id), SUM(total_amount) from product.billings where date>= ?1 and date<= ?2 and org_id = ?3 and branch_id = ?4 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllGrossSalesOfBillingDetailsByBranchAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingGrossSales;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingsByOrgIdByFromdate(final Date billingdate, final long orgId) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery("from Billing where  billDate = ? 1 and organisation.id = ? 2")
              .setParameter(1, billingdate)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingsByOrgIdByFromdate ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingsByCategoryAndStartAndEndDateAndOrgAndBranch(
      final Date startDate, final Date endDate, final long orgId, final long branchId) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "from Billing where organisation .id = ? 1 and branch .id = ? 2 and (billDate between  ? 3 and  ? 4)")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, startDate)
              .setParameter(4, endDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingsByCategoryAndStartAndEndDateAndOrgAndBranch ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public Billing getBillingDetailsByBillIdAndOrgId(final long billId, final long orgId) {

    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery("from Billing where id = ? 1 and organisation.id = ? 2")
                  .setParameter(1, billId)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByBillIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public BigDecimal getBillingCurrentDaySaleAmountByOrg(final Date billingDate, final long orgId) {

    BigDecimal currentDaySaleAmount = null;
    try {
      currentDaySaleAmount =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "Select SUM(totalAmount) FROM Billing where billDate = ? 1 and organisation.id = ? 2")
                  .setParameter(1, billingDate)
                  .setParameter(2, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingCurrentDaySaleAmountByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return currentDaySaleAmount;
  }

  @Override
  public BigDecimal getBillingCurrentMonthSaleAmountorg(
      final int Month, final int year, final long orgId) {

    BigDecimal currentMonthSaleAmount = null;
    try {
      currentMonthSaleAmount =
          (BigDecimal)
              entityManager
                  .createQuery(
                      "select SUM(totalAmount) from Billing where EXTRACT(MONTH FROM billDate) = ? 1 and EXTRACT(YEAR FROM billDate) = ? 2 and organisation.id = ? 3")
                  .setParameter(1, Month)
                  .setParameter(2, year)
                  .setParameter(3, orgId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingCurrentMonthSaleAmountorg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return currentMonthSaleAmount;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingDetailsByDatesAndOrgIdAndCounterNumber(
      final Date fromDate, final Date toDate, final long orgId, final long counterNumber) {

    List<Billing> billingDetails = new ArrayList<>();
    try {
      billingDetails =
          entityManager
              .createQuery(
                  "from Billing where organisation.id = ? 1 and counterDetails.id = ? 2 and billDate between ? 3 and ? 4")
              .setParameter(1, orgId)
              .setParameter(2, counterNumber)
              .setParameter(3, fromDate)
              .setParameter(4, toDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByDatesAndOrgIdAndCounterNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingDetailsByDatesAndOrgIdAndBranchIdAndCounterNumber(
      final Date fromDate,
      final Date toDate,
      final long orgId,
      final long branchId,
      final long counterNumber) {

    List<Billing> billingDetails = new ArrayList<>();
    try {
      billingDetails =
          entityManager
              .createQuery(
                  "from Billing where organisation.id = ? 1 and branch.id = ? 2 and counterDetails.id = ? 3 and billDate between ? 4 and ? 5")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .setParameter(3, counterNumber)
              .setParameter(4, fromDate)
              .setParameter(5, toDate)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByDatesAndOrgIdAndBranchIdAndCounterNumber ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingDetails;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getnetsaleOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {

    List<Object[]> billingNetSale = new ArrayList<>();
    try {
      billingNetSale =
          entityManager
              .createNativeQuery(
                  "select COUNT(id), SUM(net_amount) from product.billings where date>= ?1 and date <= ?2 and org_id = ?3 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getnetsaleOfBillingDetailsByOrgAndBranchisNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingNetSale;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllTxesOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {

    List<Object[]> billingTaxes = new ArrayList<>();
    try {
      billingTaxes =
          entityManager
              .createNativeQuery(
                  "select COUNT(id), SUM(gst_amount) from product.billings where date>= ?1 and date<= ?2 and org_id = ?3 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllTxesOfBillingDetailsByOrgAndBranchisNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingTaxes;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllDiscountOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {

    List<Object[]> billingDiscount = new ArrayList<>();
    try {
      billingDiscount =
          entityManager
              .createNativeQuery(
                  "select COUNT(id), SUM(discount_amount) from product.billings where date>= ?1 and date<= ?2 and org_id = ?3 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllDiscountOfBillingDetailsByOrgAndBranchisNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingDiscount;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllGrossSalesOfBillingDetailsByOrgAndBranchisNull(
      final Date formDate, final Date endDate, final long orgId) {

    List<Object[]> billingGrossSales = new ArrayList<>();
    try {
      billingGrossSales =
          entityManager
              .createNativeQuery(
                  "select COUNT (id), SUM(total_amount) from product.billings where date>= ?1 and date<= ?2 and org_id = ?3 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllGrossSalesOfBillingDetailsByOrgAndBranchisNull ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingGrossSales;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingDetailsByCustomerId(long custId) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "select COUNT(billDate), SUM(totalAmount), customer.customerName, customer.mobileNumber FROM Billing where customer.customerId=?1 group by customer.customerName, customer.mobileNumber")
              .setParameter(1, custId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByCustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Billing> getBillingByCustomerId(long custId) {

    List<Billing> billing = new ArrayList<>();
    try {
      billing =
          entityManager
              .createQuery(
                  "select billDate, totalAmount, customer.customerName, customer.mobileNumber FROM Billing where customer.customerId=?1 order by id asc ")
              .setParameter(1, custId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getBillingByCustomerId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]>
      getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndBranchIdGroupedByPaymentMode(
          Date fromDate, Date endDate, long orgId, long branchId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createNativeQuery(
                  "Select t2.payment_mode,sum(t2.amount),count(t2.payment_mode) From product.billings as t1 Join product.billing_payments as t2 On t1.id=t2.billing_id and t1.date between ?1 and ?2 and t2.org_id = ?3 and t2.branch_id= ?4  Group By t2.payment_mode")
              .setParameter(1, fromDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error(
          "getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndBranchIdGroupedByPaymentMode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndGroupedByPaymentMode(
      Date fromDate, Date endDate, long orgId) {

    List<Object[]> list = new ArrayList<>();
    try {
      list =
          entityManager
              .createNativeQuery(
                  "Select t2.payment_mode,sum(t2.amount),count(t2.payment_mode) From product.billings as t1 Join product.billing_payments as t2 On t1.id=t2.billing_id and t1.date between ?1 and ?2 and t2.org_id = ?3 Group By t2.payment_mode")
              .setParameter(1, fromDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSumOfBillingsReportBetweenTwoDatesAndOrgIdAndGroupedByPaymentMode ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllServiceChargesOfBillingDetailsByBranchAndOrg(
      Date formDate, Date endDate, long orgId, long branchId) {

    List<Object[]> billingDiscount = new ArrayList<>();
    try {
      billingDiscount =
          entityManager
              .createNativeQuery(
                  "select COUNT(id), SUM(service_charge) from product.billings where date>= ?1 and date<= ?2 and org_id = ?3 and branch_id = ?4 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllServiceChargesOfBillingDetailsByBranchAndOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingDiscount;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getAllServiceChargesOfBillingDetailsByOrg(
      Date formDate, Date endDate, long orgId) {

    List<Object[]> billingDiscount = new ArrayList<>();
    try {
      billingDiscount =
          entityManager
              .createNativeQuery(
                  "select COUNT(id), SUM(service_charge) from product.billings where date>= ?1 and date<= ?2 and org_id = ?3 ")
              .setParameter(1, formDate)
              .setParameter(2, endDate)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllServiceChargesOfBillingDetailsByOrg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billingDiscount;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Object[]> getSaleReportBetweenTwoDatesByOrgIdAndBranchIdAndDates(
      Date fromDate, Date toDate, long orgId, long branchId) {

    List<Object[]> saleReport = new ArrayList<>();
    try {
      saleReport =
          entityManager
              .createNativeQuery(
                  "select sum(t1.total_amount) as total,sum(t1.gst_amount) as gst,sum(t1.discount_amount) as discount,sum(t1.service_charge) as service,sum(t1.net_amount) as net, t1.date as date , count (t1.*) as billsCount, sum(t2.occupied_capacity) as occupiedCapacity from product.billings  as t1 Join product.cart as t2 on t1.cart_id = t2.cart_id  group by t1.date,t1.org_id,t1.branch_id Having t1.date>= ?1 and  t1.date<= ?2  and  t1.org_id = ?3 and  t1.branch_id = ?4")
              .setParameter(1, fromDate)
              .setParameter(2, toDate)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSaleReportBetweenTwoDatesByOrgIdAndBranchIdAndDates ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return saleReport;
  }

  @Override
  public Billing getBillingDetailsByOrdersId(final long ordersId) {

    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery("from Billing where order.id =?1 ")
                  .setParameter(1, ordersId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByOrdersId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public Billing getBillingDetailsByBillNumberAndOrg(String billNumber, long orgId, Date date) {
    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery(
                      "from Billing where billingNumber =?1 and organisation.id=?2 and billDate=?3 ")
                  .setParameter(1, billNumber)
                  .setParameter(2, orgId)
                  .setParameter(3, date)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByOrdersId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public Billing getBillingDetailsByBillNumberAndOrgAndBranch(
      String billNumber, long orgId, long branchId, Date date) {
    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery(
                      "from Billing where billingNumber =?1 and organisation.id=?2 and branch.id=?3 and billDate=?4 ")
                  .setParameter(1, billNumber)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .setParameter(4, date)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByOrdersId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public Billing getBillingDetailsByBillNumberCountAndOrg(
      String billNumber, long orgId, Date date) {
    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery(
                      "from Billing where billNumberCount =?1 and organisation.id=?2 and billDate=?3 ")
                  .setParameter(1, billNumber)
                  .setParameter(2, orgId)
                  .setParameter(3, date)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByOrdersId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }

  @Override
  public Billing getBillingDetailsByBillNumberCountAndOrgAndBranch(
      String billNumber, long orgId, long branchId, Date date) {
    Billing billing = null;
    try {
      billing =
          (Billing)
              entityManager
                  .createQuery(
                      "from Billing where billNumberCount =?1 and organisation.id=?2 and branch.id=?3 and billDate=?4 ")
                  .setParameter(1, billNumber)
                  .setParameter(2, orgId)
                  .setParameter(3, branchId)
                  .setParameter(4, date)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getBillingDetailsByOrdersId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return billing;
  }
}
