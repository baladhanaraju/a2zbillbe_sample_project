package com.a2zbill.dao.impl;

import com.a2zbill.dao.SubscriptionDao;
import com.a2zbill.domain.Subscription;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SubscriptionDaoImpl implements SubscriptionDao {

  private static final Logger logger = LoggerFactory.getLogger(SubscriptionDaoImpl.class);

  @Autowired public EntityManager entityManager;

  @Override
  public void save(Subscription subscription) {
    try {
      entityManager.persist(subscription);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(Subscription subscription) {
    try {
      entityManager.merge(subscription);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getAllSubscriptions() {

    List<Subscription> subscriptionDetail = new ArrayList<>();
    try {
      subscriptionDetail =
          entityManager.createQuery("from Subscription order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllSubscriptions ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscriptionDetail;
  }

  @Override
  public Subscription getSubscriptionByFrequencyId(long frequencyId) {

    Subscription subscription = null;
    try {
      subscription =
          (Subscription)
              entityManager
                  .createQuery("from Subscription where frequency.id=?1")
                  .setParameter(1, frequencyId)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getSubscriptionByFrequencyId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndOrgId(Date date, long orgId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1 and endDate>=?1  and organisation.id=?2")
              .setParameter(1, date)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndOrgIdAndbranchId(
      Date date, long orgId, long branchId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1 and endDate>=?1  and organisation.id=?2 and branch.id=?3 ")
              .setParameter(1, date)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndOrgIdAndbranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndorgId(
      Date date, long employeeId, long orgId) {
    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1  and endDate>=?1 and employee.id=?2 and organisation.id=?3 and parentId=?4")
              .setParameter(1, date)
              .setParameter(2, employeeId)
              .setParameter(3, orgId)
              .setParameter(4, 0)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndEmployeeIdAndorgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndOrgIdAndbranchId(
      Date date, long employeeId, long orgId, long branchId) {
    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1  and endDate>=?1 and employee.id=?2 and organisation.id=?3  and branch.id=?4")
              .setParameter(1, date)
              .setParameter(2, employeeId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndEmployeeIdAndOrgIdAndbranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndproductIdAndOrgId(
      Date date, long productId, long orgId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1  and endDate>=?1 and product.id=?2 and organisation.id=?3")
              .setParameter(1, date)
              .setParameter(2, productId)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndproductIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId(
      Date date, long productId, long orgId, long branchId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1  and endDate>=?1 and product.id=?2 and organisation.id=?3 and branch.id=?4")
              .setParameter(1, date)
              .setParameter(2, productId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndproductIdAndorgIdAndbranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgId(
      Date date, long employeeId, long productId, long orgId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1 and endDate>=?1 and employee.id =?2 and product.id=?3 and organisation.id=?4")
              .setParameter(1, date)
              .setParameter(2, employeeId)
              .setParameter(3, productId)
              .setParameter(4, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription>
      getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgIdAndBranchId(
          Date date, long employeeId, long productId, long orgId, long branchId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where startDate<=?1  and endDate>=?1 and employee.id =?2 and product.id =?3 and organisation.id =?4 and branch.id=?5")
              .setParameter(1, date)
              .setParameter(2, employeeId)
              .setParameter(3, productId)
              .setParameter(4, orgId)
              .setParameter(5, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByDateAndEmployeeIdAndproductIdAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgIdAndbranchId(
      long employeeId, long productId, long orgId, long branchId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where employee.id =?1 and product.id =?2 and organisation.id =?3 and branch.id=?4")
              .setParameter(1, employeeId)
              .setParameter(2, productId)
              .setParameter(3, orgId)
              .setParameter(4, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgIdAndbranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgId(
      long employeeId, long productId, long orgId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where employee.id =?1 and product.id =?2 and organisation.id =?3")
              .setParameter(1, employeeId)
              .setParameter(2, productId)
              .setParameter(3, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByEmployeeIdAndProductIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByOrgId(long orgId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery("from Subscription where  organisation.id =?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByOrgIdAndBranchId(long orgId, long branchId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery("from Subscription where  organisation.id =?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndOrgIdAndBranchId(
      long employeeId, long orgId, long branchId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where employee.id=?1 and  organisation.id =?2 and branch.id=?3")
              .setParameter(1, employeeId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByEmployeeIdAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByEmployeeIdAndOrgId(
      long employeeId, long orgId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery("from Subscription where employee.id=?1 and  organisation.id =?2")
              .setParameter(1, employeeId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByEmployeeIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByProductIdAndOrgIdAndBranchId(
      long productId, long orgId, long branchId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery(
                  "from Subscription where product.id=?1  and  organisation.id =?2  and branch.id=?3")
              .setParameter(1, productId)
              .setParameter(2, orgId)
              .setParameter(3, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByProductIdAndOrgIdAndBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Subscription> getSubscriptionDetailsByProductIdAndOrgId(long productId, long orgId) {

    List<Subscription> subscription = new ArrayList<>();
    try {
      subscription =
          entityManager
              .createQuery("from Subscription where product.id=?1  and  organisation.id =?2 ")
              .setParameter(1, productId)
              .setParameter(2, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getSubscriptionDetailsByProductIdAndOrgId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return subscription;
  }
}
