package com.a2zbill.dao.impl;

import com.a2zbill.dao.CancelItemDao;
import com.a2zbill.domain.CancelItem;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CancelItemDaoImpl implements CancelItemDao {

  private static final Logger logger = LoggerFactory.getLogger(CancelItemDaoImpl.class);

  @Autowired private EntityManager entityManager;

  @Override
  public void save(CancelItem cancelItem) {
    try {
      entityManager.persist(cancelItem);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
  }

  @Override
  public boolean delete(long id) {
    try {
      entityManager.remove(id);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return false;
  }

  @Override
  public boolean update(CancelItem cancelItem) {
    try {
      entityManager.merge(cancelItem);
    } catch (final Exception e) {
      logger.error(e.toString(), e);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CancelItem> getAllCancelItemDetails() {

    List<CancelItem> cancelItem = new ArrayList<>();
    try {
      cancelItem = entityManager.createQuery("from CancelItem order by id").getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCancelItemDetails ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cancelItem;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CancelItem> getAllCancelItemDetailsByorgBranchId(
      final long orgId, final long branchId) {

    List<CancelItem> cancelItem = new ArrayList<>();
    try {
      cancelItem =
          entityManager
              .createQuery("from CancelItem where organisation.id=?1 and branch.id=?2")
              .setParameter(1, orgId)
              .setParameter(2, branchId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCancelItemDetailsByorgBranchId ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cancelItem;
  }

  @Override
  public CancelItem getCancelItemDetailsByorgBranchIdAndGuid(
      final long orgId, final long branchId, final String guid) {

    CancelItem cancelItem = null;
    try {
      cancelItem =
          (CancelItem)
              entityManager
                  .createQuery(
                      "from CancelItem where organisation.id=?1 and branch.id=?2 and guid=?3")
                  .setParameter(1, orgId)
                  .setParameter(2, branchId)
                  .setParameter(3, guid)
                  .getSingleResult();
    } catch (final Exception ex) {
      logger.error("getCancelItemDetailsByorgBranchIdAndGuid ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cancelItem;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<CancelItem> getAllCancelItemDetailsByorg(final long orgId) {

    List<CancelItem> cancelItem = new ArrayList<>();
    try {
      cancelItem =
          this.entityManager
              .createQuery("from CancelItem where organisation.id=?1")
              .setParameter(1, orgId)
              .getResultList();
    } catch (final Exception ex) {
      logger.error("getAllCancelItemDetailsByorg ", ex);
    } finally {
      Utils.closeEntityManager(entityManager);
    }
    return cancelItem;
  }
}
