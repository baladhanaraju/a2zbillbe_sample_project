package com.a2zbill.dao;

import com.a2zbill.domain.QuantityMeasurements;
import java.util.List;

public interface QuantityMeasurementsDao {
  void save(QuantityMeasurements quantityMeasurements);

  boolean delete(long id);

  boolean update(QuantityMeasurements quantityMeasurements);

  List<QuantityMeasurements> getAllQuantityMeasurements();

  QuantityMeasurements getQuantityMeasurementsById(Long id);

  QuantityMeasurements getQuantityMeasurementsByName(String name);
}
