package com.a2zbill.dao;

import com.a2zbill.domain.InvoiceBranchDetails;

public interface InvoiceBranchDetailsDao {

  void save(InvoiceBranchDetails invoiceBranchDetails);
}
