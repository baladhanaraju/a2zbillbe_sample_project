package com.a2zbill.dao;

import com.a2zbill.domain.Sync;
import java.util.List;

public interface SyncDao {
  void save(Sync sync);

  boolean delete(Long id);

  boolean update(Sync sync);

  List<Sync> getAllSyncDetails();

  List<Sync> getAllSyncDetailsByOrgBranchId(long orgId, long branchId);

  List<Sync> getAllSyncDetailsByOrg(long orgId);

  Sync getSyncDetailsByOrgBranchIdAndSyncUrl(long orgId, long branchId, String syncUrl);
}
