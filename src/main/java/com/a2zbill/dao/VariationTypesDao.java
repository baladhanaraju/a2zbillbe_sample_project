package com.a2zbill.dao;

import com.a2zbill.domain.VariationTypes;
import java.util.List;

public interface VariationTypesDao {

  List<VariationTypes> getAllVariationTypes();

  VariationTypes getVariationTypesById(long id);

  VariationTypes getVariationTypesByName(String name);

  List<VariationTypes> getOptionsByVariationType(long variationTypeId);
}
