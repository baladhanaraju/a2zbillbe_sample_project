package com.a2zbill.dao;

import com.a2zbill.domain.ExchangeHistory;
import java.util.List;

public interface ExchangeHistoryDao {
  void save(ExchangeHistory exchangeHistory);

  boolean delete(long id);

  boolean update(ExchangeHistory exchangeHistory);

  List<ExchangeHistory> getAllExchangeHistory();

  ExchangeHistory getExchangeHistoryById(Long id);
}
