package com.a2zbill.dao;

import com.a2zbill.domain.OrganisationFlow;
import java.util.List;

public interface OrganisationFlowDao {

  void save(final OrganisationFlow organisationFlow);

  OrganisationFlow getOrganisationFlowById(final Long id);

  List<OrganisationFlow> getOrganisationFlowByOrgId(final Long orgId);
}
