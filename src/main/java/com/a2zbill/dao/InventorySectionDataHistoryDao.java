package com.a2zbill.dao;

import com.a2zbill.domain.InventorySectionDataHistory;
import java.util.Date;
import java.util.List;

public interface InventorySectionDataHistoryDao {
  void save(InventorySectionDataHistory inventorySectionDataHistory);

  boolean delete(long id);

  boolean update(InventorySectionDataHistory inventorySectionDataHistory);

  List<InventorySectionDataHistory> getAllInventorySectionDataHistory();

  InventorySectionDataHistory getInventorySectionDataHistoryByInventorySectionId(
      long inventorySectionId);

  InventorySectionDataHistory getInventorySectionDataHistoryByproductIdAndOrgIdBranchId(
      long productId, long orgId, long branchId);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryByOrgIdBranchId(
      long orgId, long branchId);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryByItemWiseOrgIdBranchId(
      long orgId, long branchId, Date date);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryByItemWiseOrgIdBranchIdByDateWise(
      long orgId, long branchId, Date fromdate, Date todate);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryByItemWiseOrgIdByDateWise(
      long orgId, Date fromDate, Date toDate);

  public List<Object[]> getSumOfInventoryHistoryDetailsByOrgBranch(
      Date formDate, Date endDate, long orgId, long branchId);

  public List<Object[]> getSumOfInventoryHistoryDetailsByOrgBranchisNull(
      Date formDate, Date endDate, long orgId);

  List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSumOfQuantity(
          long orgId, long branchId, Date fromdate, Date todate);

  List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdSectionId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId, long fromSectionId);

  List<Object[]> getInventorySectionDataHistoryByBillIdAndDates(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long sectionId,
      long toSectionId);

  List<Object[]> getInventorySectionDataHistoryTotalAmt(
      Date formDate, Date endDate, long branchId, long orgId, long sectionId, long toSectionId);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryBySection(
      long orgId, long branchId, long sectionId);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryByBranchId(
      long orgId, long branchId, Date fromDate, Date toDate, long sectionId);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryByVendorIdBranchId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId, long vendorId);

  List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchId(
      long orgId, long branchId, Date fromdate, Date todate, long fromBranchId);

  List<Object[]> getInventorySectionDataHistoryByTotalAmt(
      Date formDate, Date endDate, long fromSectionId, long orgId, long branchId);

  List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdFromBranchId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId, long fromBranchId);

  List<Object[]> getInventorySectionDataHistoryByDepartmentWiseOrgIdBranchIdAndSectionId(
      long orgId, long branchId, Date fromdate, Date todate, long sectionId);

  List<Object[]> getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromBranchId(
      Date fromDate, Date endDate, long orgId, long sectionId, long fromBranchId);

  List<Object[]> getInventorySectionDataHistoryDetailsByDatesAndOrgIdAndSectionIdAndFromSectionId(
      Date fromDate, Date endDate, long orgId, long sectionId, long fromSectionId);

  Object[]
      getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromBranchIdAndReferenceNumber(
          Date fromDate,
          Date endDate,
          long orgId,
          long sectionId,
          long fromBranchId,
          String referenceNumber);

  Object[]
      getInventorySectionDataHistoryByDatesAndOrgIdAndSectionIdAndFromSectionIdAndReferenceNumber(
          Date fromDate,
          Date endDate,
          long orgId,
          long sectionId,
          long fromSectionId,
          String referenceNumber);

  List<Object[]> getInventorySectionDataHistoryByOrgIdFromBranchId(
      Date fromdate, Date todate, long orgId, long branchId);

  List<Object[]> getInventorySectionDataHistoryByOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long sectionId, long fromSectionId, long orgId, long branchId);

  List<Object[]> getInventorySectionDataHistoryByOrgIdBranchIdSection(
      Date fromdate, Date todate, long sectionId, long frombranchId, long orgId, long branchId);

  List<Object[]> getInventorySectionDataHistoryByTransferOrgIdFromBranchId(
      Date fromdate, Date todate, long orgId, long branchId);

  List<Object[]> getInventorySectionDataHistoryByTransferOrgIdBranchIdSection(
      Date fromdate, Date todate, long sectionId, long frombranchId, long orgId, long branchId);

  List<Object[]> getInventorySectionDataHistoryByTransferOrgIdBranchIdSectionId(
      Date fromdate, Date todate, long sectionId, long fromSectionId, long orgId, long branchId);

  List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseConsolidatedOrgIdBranchIdAndSectionId(
          long orgId, long branchId, Date fromdate, Date todate, long fromSectionId);

  List<InventorySectionDataHistory>
      getInventorySectionDataHistoryByItemWiseOrgIdBranchIdAndFromSectionId(
          long orgId, Date fromdate, Date todate, long fromBranchId);

  List<Object[]> getInventorySectionDataHistoryQuantityByProductAndFromToSection(
      long orgId, long branchId, Date fromdate, Date todate, long fromsectionId, long toSectionId);

  List<Object[]> getInventorySectionDataHistoryQuantityByProductAndFromBranchToSection(
      long orgId, long branchId, Date fromdate, Date todate, long fromBranchId, long toSectionId);

  List<InventorySectionDataHistory>
      getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromBranchId(
          Date fromDate, Date endDate, long orgId, long sectionId, long fromBranchId);

  List<InventorySectionDataHistory>
      getInventorySectionDataHistoryListByDatesAndOrgIdAndSectionIdAndFromSectionId(
          Date fromDate, Date endDate, long orgId, long sectionId, long fromSectionId);

  List<InventorySectionDataHistory> getInventorySectionDataHistoryByOrgIdAndReferenceNumber(
      long orgId, String referenceNumber);

  List<Object[]> getInventorySectionDataHistoryByBranch(
      long orgId, long branchId, Date fromdate, Date todate, long fromBranchId);

  List<Object[]> getInventorySectionDataHistoryByBranchReference(
      long orgId, long branchId, Date fromdate, Date todate, long fromBranchId, String reference);

  List<Object[]> getInventorySectionDataHistoryByTotalAmountBranch(
      long orgId, long branchId, Date fromdate, Date todate);

  List<Object[]> getInventorySectionDataHistoryTotalAmtToSection(
      Date formDate, Date endDate, long branchId, long orgId, long sectionId, long fromBranchId);

  List<Object[]> getInventorySectionDataHistoryByBillIdAndDatesToSectionReference(
      Date formDate,
      Date endDate,
      String referenceNumber,
      long branchId,
      long orgId,
      long fromBranchId,
      long toSectionId);
}
