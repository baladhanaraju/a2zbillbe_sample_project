package com.a2zbill.dao;

import com.a2zbill.domain.Flow;
import java.util.List;

public interface FlowDao {
  void save(Flow flow);

  boolean delete(Long id);

  boolean update(Flow flow);

  Flow getFlowById(Long id);

  Flow getFlowByRaking(Long flowRanking);

  Flow getFlowByFlowTypeId(Long flowTypeId, Long flowRanking);

  List<Flow> getFlowByOrgId(Long orgId);

  List<Flow> getAllFlowDetails();
}
