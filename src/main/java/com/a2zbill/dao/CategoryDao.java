package com.a2zbill.dao;

import com.a2zbill.domain.Category;
import java.util.List;

public interface CategoryDao {
  void save(Category category);

  boolean delete(Long id);

  boolean update(Category category);

  List<Category> getAllCategories();

  Category getCategoryByName(String categoryName);
}
