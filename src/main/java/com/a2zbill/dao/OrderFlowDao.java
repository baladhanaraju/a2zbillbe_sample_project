package com.a2zbill.dao;

import com.a2zbill.domain.OrderFlow;
import java.util.List;

public interface OrderFlowDao {

  void save(OrderFlow orderFlow);

  boolean delete(Long id);

  boolean update(OrderFlow orderFlow);

  OrderFlow getOrderFlowById(Long id);

  List<OrderFlow> getOrderFlowByCounterIdAndFlowId();

  OrderFlow getOrderFlowByCartId(Long cartId);

  OrderFlow getOrderFlowByCartIdAndProductIdAndFlowId(Long cartId, Long productId);

  OrderFlow getOrderFlowByCartIdAndProductIdAndCartIdByServiceSection(Long cartId, Long productId);

  List<OrderFlow> getOrderFlowByFlowId();

  List<OrderFlow> getAllOrderFlow();
}
