package com.a2zbill.dao;

import com.a2zbill.domain.DeliveryOptions;
import java.util.List;

public interface DeliveryOptionsDao {

  void save(final DeliveryOptions deliveryOptions);

  boolean delete(final DeliveryOptions deliveryOptions);

  boolean update(final DeliveryOptions deliveryOptions);

  DeliveryOptions getDeliveryOptionsByid(final long id);

  List<DeliveryOptions> getAllDeliveryOptions();
}
