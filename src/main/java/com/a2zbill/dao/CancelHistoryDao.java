package com.a2zbill.dao;

import com.a2zbill.domain.CancelHistory;
import java.util.List;

public interface CancelHistoryDao {
  void save(CancelHistory cancelHistory);

  boolean delete(long id);

  boolean update(CancelHistory cancelHistory);

  List<CancelHistory> getAllCancelHistory();

  CancelHistory getCancelHistoryById(Long id);
}
