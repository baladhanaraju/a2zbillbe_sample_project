package com.a2zbill.dao;

import com.a2zbill.domain.DiscountCoupons;
import java.util.List;
import java.util.Map;

public interface DiscountCouponsDao {
  void save(final DiscountCoupons discountCoupons);

  boolean delete(final Long id);

  boolean update(final DiscountCoupons productDeals);

  DiscountCoupons getDiscountCouponsById(final Long id);

  DiscountCoupons getDiscountCouponsByOfferCode(final String offerCode);

  List<Map<String, Object>> getAllDealNames();

  Map<String, Object> getDealNameById(final Long id);

  List<DiscountCoupons> getAllDiscountCoupons();
}
