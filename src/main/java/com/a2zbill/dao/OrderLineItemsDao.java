package com.a2zbill.dao;

import com.a2zbill.domain.OrderLineItems;
import java.util.Date;
import java.util.List;

public interface OrderLineItemsDao {

  void save(final OrderLineItems orderLineItems);

  boolean delete(final long id);

  boolean update(final OrderLineItems orderLineItems);

  List<OrderLineItems> getAllOrderLineItems();

  OrderLineItems getOrderLineItemsById(final long orderLineItemId);

  List<OrderLineItems> getOrderLineItemsByorderId(final long orderId);

  List<OrderLineItems> getOrderLineItemsByCounterIdAndFlowId();

  OrderLineItems getOrderLineItemsByProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode);

  OrderLineItems getOrderDetailsByCounterIdProductNameAndHsnCode(
      final Long counterId, final String productName, final String hsnCode);

  List<OrderLineItems> getOrderLineItemsGroupByProductNameifBrnachNull(
      final Date fromDate, final Date toDate, final long orgId);

  List<OrderLineItems> getOrderLineItemsGroupByProductName(
      final Date fromDate, final Date toDate, final long orgId, final long branchId);

  List<OrderLineItems> getproductQuantityByBranchAndOrgIdAndDate(
      final long orgId, final long branchId, final Date fromDate, final Date toDate);

  List<OrderLineItems> getproductQuantityByOrgIdAndDate(
      final long orgId, final Date fromDate, final Date toDate);

  OrderLineItems getOrderLineItemsByIdAndProductName(final long orderId, final String productName);

  List<OrderLineItems> getOrderLineItemsByProductName(
      final String productName, final long orgId, final long branchId);
}
