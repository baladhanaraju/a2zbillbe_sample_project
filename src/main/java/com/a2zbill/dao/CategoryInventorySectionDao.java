package com.a2zbill.dao;

import com.a2zbill.domain.CategoryInventorySection;
import java.util.List;

public interface CategoryInventorySectionDao {
  void save(final CategoryInventorySection categoryInventorySection);

  boolean delete(final long id);

  boolean update(final CategoryInventorySection categoryInventorySection);

  List<CategoryInventorySection> getAllCategoryInventorySection();

  CategoryInventorySection getCategoryInventorySectionByCategoryIdAndInventorySectionId(
      final long categoryId, final long inventorySectionId);
}
