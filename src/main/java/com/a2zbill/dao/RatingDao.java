package com.a2zbill.dao;

import com.a2zbill.domain.Rating;
import java.util.List;

public interface RatingDao {
  void save(Rating rating);

  boolean delete(Long id);

  boolean update(Rating rating);

  Rating getRatingByRatingId(Long ratingId);

  Rating getRatingByProductId(Long productId);

  Rating getRatingByEmpIdAndProductId(Long productId, Long empId);

  List<Rating> getAllRatingDetails();
}
