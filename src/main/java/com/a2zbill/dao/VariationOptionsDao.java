package com.a2zbill.dao;

import com.a2zbill.domain.VariationOptions;
import java.util.List;

public interface VariationOptionsDao {

  List<VariationOptions> getAllVariationOptions();

  VariationOptions getVariationOptionsById(long id);

  VariationOptions getVariationOptionsByName(String name);

  List<VariationOptions> getVariationOptionsByType(long typeId);
}
