package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "product_packaging", schema = "product")
public class ProductPackaging implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_packaging_id_seq")
  @SequenceGenerator(
      name = "product_packaging_id_seq",
      sequenceName = "product_packaging_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "parent_product_id", unique = true, nullable = true)
  private Product parentproductId;

  public ProductPackaging() {}

  public ProductPackaging(final long id, final Product product, final Product parentproductId) {
    super();
    this.id = id;
    this.product = product;
    this.parentproductId = parentproductId;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(final Product product) {
    this.product = product;
  }

  public Product getParentproductId() {
    return parentproductId;
  }

  public void setParentproductId(final Product parentproductId) {
    this.parentproductId = parentproductId;
  }
}
