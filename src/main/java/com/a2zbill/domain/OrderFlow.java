package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "order_flow", schema = "product")
public class OrderFlow {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "order_flow_id_seq")
  @SequenceGenerator(
      name = "order_flow_id_seq",
      sequenceName = "order_flow_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id", nullable = false)
  private Long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "cart_id", insertable = true, updatable = true)
  private Cart cartDetails;

  @Override
  public String toString() {
    return "OrderFlow [id="
        + id
        + ", cartDetails="
        + cartDetails
        + ", products="
        + products
        + ", flow="
        + flow
        + ", time="
        + time
        + ", quantity="
        + quantity
        + "]";
  }

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product products;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "flow_id", unique = true, nullable = true)
  private Flow flow;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "time", length = 29)
  private Date time;

  @Column(name = "quantity")
  private long quantity;

  public OrderFlow() {}

  public long getQuantity() {
    return this.quantity;
  }

  public void setQuantity(final long quantity) {
    this.quantity = quantity;
  }

  public OrderFlow(
      final Long id,
      Cart cartDetails,
      final Product products,
      final Flow flow,
      final Date time,
      final long quantity) {
    super();
    this.id = id;
    this.cartDetails = cartDetails;
    this.products = products;
    this.flow = flow;
    this.time = time;
    this.quantity = quantity;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public Cart getCartDetails() {
    return this.cartDetails;
  }

  public void setCartDetails(final Cart cartDetails) {
    this.cartDetails = cartDetails;
  }

  public Product getProducts() {
    return this.products;
  }

  public void setProducts(final Product products) {
    this.products = products;
  }

  public Flow getFlow() {
    return this.flow;
  }

  public void setFlow(final Flow flow) {
    this.flow = flow;
  }

  public Date getTime() {
    return this.time;
  }

  public void setTime(final Date time) {
    this.time = time;
  }
}
