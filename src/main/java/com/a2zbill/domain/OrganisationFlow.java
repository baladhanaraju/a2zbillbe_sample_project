package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Organisation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "organisation_flow", schema = "flow")
public class OrganisationFlow {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "flow_id_seq")
  @SequenceGenerator(
      name = "Organisation_flow_id_seq",
      sequenceName = "organisation_flow_id_seq",
      allocationSize = 1,
      schema = "flow")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "flow_id", unique = true, nullable = true)
  private Flows flows;

  public OrganisationFlow() {}

  public OrganisationFlow(final long id, final Organisation organisation, final Flows flows) {
    super();
    this.id = id;
    this.organisation = organisation;
    this.flows = flows;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Flows getFlows() {
    return this.flows;
  }

  public void setFlows(final Flows flows) {
    this.flows = flows;
  }

  @Override
  public String toString() {
    return "OrganisationFlow [id="
        + id
        + ", organisation="
        + organisation
        + ", flows="
        + flows
        + "]";
  }
}
