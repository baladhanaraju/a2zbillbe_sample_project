package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "reorder", schema = "product")
public class ReOrder {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "reorder_id_seq")
  @SequenceGenerator(
      name = "reorder_id_seq",
      sequenceName = "reorder_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "vendor_id", unique = true, nullable = true)
  private Vendor vendor;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "invoice_id", unique = true, nullable = true)
  private InvoiceDetails invoiceDetails;

  @Column(name = "order_date")
  private Date orderDate;

  @Column(name = "delivery_date")
  private Date deliveryDate;

  @Column(name = "status")
  private String status;

  @Column(name = "quantity")
  private double quantity;

  @Column(name = "pending_quantity")
  private long pendingquantity;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public ReOrder() {}

  public ReOrder(
      long id,
      Product product,
      Vendor vendor,
      InvoiceDetails invoiceDetails,
      Date orderDate,
      Date deliveryDate,
      String status,
      double quantity,
      long pendingquantity,
      Organisation organisation,
      Branch branch) {
    super();
    this.id = id;
    this.product = product;
    this.vendor = vendor;
    this.invoiceDetails = invoiceDetails;
    this.orderDate = orderDate;
    this.deliveryDate = deliveryDate;
    this.status = status;
    this.quantity = quantity;
    this.pendingquantity = pendingquantity;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Vendor getVendor() {
    return vendor;
  }

  public void setVendor(Vendor vendor) {
    this.vendor = vendor;
  }

  public Date getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(Date orderDate) {
    this.orderDate = orderDate;
  }

  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public double getQuantity() {
    return quantity;
  }

  public void setQuantity(double quantity) {
    this.quantity = quantity;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public InvoiceDetails getInvoiceDetails() {
    return invoiceDetails;
  }

  public void setInvoiceDetails(InvoiceDetails invoiceDetails) {
    this.invoiceDetails = invoiceDetails;
  }

  public long getPendingquantity() {
    return pendingquantity;
  }

  public void setPendingquantity(long pendingquantity) {
    this.pendingquantity = pendingquantity;
  }
}
