package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "sync", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class Sync implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "Sync_id_seq")
  @SequenceGenerator(
      name = "Sync_id_seq",
      sequenceName = "Sync_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "sync_url ")
  private String syncUrl;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  /** The Date. */
  @Column(name = "created_date")
  private Date createdDate;

  /** The Date. */
  @Column(name = "modified_date")
  private Date modifiedDate;

  public Sync() {}

  public Sync(
      final long id,
      final String syncUrl,
      final Organisation organisation,
      final Branch branch,
      final Date createdDate,
      final Date modifiedDate) {
    super();
    this.id = id;
    this.syncUrl = syncUrl;
    this.organisation = organisation;
    this.branch = branch;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getSyncUrl() {
    return this.syncUrl;
  }

  public void setSyncUrl(final String syncUrl) {
    this.syncUrl = syncUrl;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  @Override
  public String toString() {
    return "Sync [id="
        + id
        + ", syncurl="
        + syncUrl
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + "]";
  }
}
