package com.a2zbill.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "frequency", schema = "subscription")
public class Frequency implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "frequency_id_seq")
  @SequenceGenerator(
      name = "frequency_id_seq",
      sequenceName = "frequency_id_seq",
      allocationSize = 1,
      schema = "subscription")
  @Column(name = "id")
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "days")
  private String days;

  public Frequency() {}

  public Frequency(long id, String name, String days) {
    super();
    this.id = id;
    this.name = name;
    this.days = days;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDays() {
    return days;
  }

  public void setDays(String days) {
    this.days = days;
  }
}
