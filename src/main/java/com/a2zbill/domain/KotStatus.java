package com.a2zbill.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "kot_status", schema = "kot")
@SuppressWarnings("serial")
public class KotStatus implements Serializable {

  private long id;

  private String productName;

  private int quantity;

  private long counterNumber;

  private long branchId;

  private long empId;

  private String productImage;

  @Column(name = "kot_count_id")
  private long kotCountId;

  public KotStatus() {}

  public KotStatus(
      final long id,
      final String productName,
      final int quantity,
      final long counterNumber,
      final long branchId,
      final long empId,
      final String productImage,
      final long kotCountId) {
    super();
    this.id = id;
    this.productName = productName;
    this.quantity = quantity;
    this.counterNumber = counterNumber;
    this.branchId = branchId;
    this.empId = empId;
    this.productImage = productImage;
    this.kotCountId = kotCountId;
  }

  @Column(name = "emp_id")
  public long getEmpId() {
    return this.empId;
  }

  public void setEmpId(final long empId) {
    this.empId = empId;
  }

  @Column(name = "product_image")
  public String getProductImage() {
    return this.productImage;
  }

  public void setProductImage(final String productImage) {
    this.productImage = productImage;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "kot_status_id")
  @SequenceGenerator(
      name = "kot_status_id",
      sequenceName = "kot_status_id",
      allocationSize = 1,
      schema = "kot")
  @Column(name = "id")
  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  @Column(name = "product_name")
  public String getProductName() {
    return this.productName;
  }

  public void setProductName(final String productName) {
    this.productName = productName;
  }

  @Column(name = "quantity")
  public int getQuantity() {
    return this.quantity;
  }

  public void setQuantity(final int quantity) {
    this.quantity = quantity;
  }

  @Column(name = "counter_number")
  public long getCounterNumber() {
    return this.counterNumber;
  }

  public void setCounterNumber(final long counterNumber) {
    this.counterNumber = counterNumber;
  }

  @Column(name = "branch_id")
  public long getBranchId() {
    return this.branchId;
  }

  public void setBranchId(final long branchId) {
    this.branchId = branchId;
  }

  public long getKotCountId() {
    return this.kotCountId;
  }

  public void setKotCountId(final long kotCountId) {
    this.kotCountId = kotCountId;
  }

  @Override
  public String toString() {
    return "KotStatus [id="
        + id
        + ", productName="
        + productName
        + ", quantity="
        + quantity
        + ", counterNumber="
        + counterNumber
        + ", branchId="
        + branchId
        + ", empId="
        + empId
        + ", productImage="
        + productImage
        + ", kotCountId="
        + kotCountId
        + "]";
  }
}
