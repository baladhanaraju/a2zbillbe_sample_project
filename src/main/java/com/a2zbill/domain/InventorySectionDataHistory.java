package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "inventory_section_data_history", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class InventorySectionDataHistory implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inventorysectiondata_history_id_seq")
  @SequenceGenerator(
      name = "inventorysectiondata_history_id_seq",
      sequenceName = "inventorysectiondata_history_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "section_name")
  private String sectionName;

  @Column(name = "quantity")
  private BigDecimal quantity;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product productId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "inventory_section_id", unique = true, nullable = true)
  private InventorySection inventorySectionId;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "orgid", insertable = true, updatable = true)
  private Organisation organisation;

  @Column(name = "date")
  private Date date;

  @Column(name = "reference_number")
  private String referenceNumber;

  @Column(name = "amount")
  private BigDecimal amount;

  @Column(name = "buying_price")
  private BigDecimal buyingPrice;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branchid", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "vendor_id", unique = true, nullable = true)
  private Vendor vendor;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "from_section_id", unique = true, nullable = true)
  private InventorySection fromSectionId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "from_branch_id", unique = true, nullable = true)
  private Branch fromBranchId;

  @Column(name = "taxable_amount")
  private BigDecimal taxableAmount;

  @Column(name = "discount_amount")
  private BigDecimal discountAmount;

  public InventorySectionDataHistory() {}

  public InventorySectionDataHistory(
      long id,
      String sectionName,
      BigDecimal quantity,
      Product productId,
      InventorySection inventorySectionId,
      Date createdDate,
      Date modifiedDate,
      Organisation organisation,
      Date date,
      String referenceNumber,
      BigDecimal amount,
      BigDecimal buyingPrice,
      Branch branch,
      Vendor vendor,
      InventorySection fromSectionId,
      Branch fromBranchId,
      BigDecimal taxableAmount,
      BigDecimal discountAmount) {
    super();
    this.id = id;
    this.sectionName = sectionName;
    this.quantity = quantity;
    this.productId = productId;
    this.inventorySectionId = inventorySectionId;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.organisation = organisation;
    this.date = date;
    this.referenceNumber = referenceNumber;
    this.amount = amount;
    this.buyingPrice = buyingPrice;
    this.branch = branch;
    this.vendor = vendor;
    this.fromSectionId = fromSectionId;
    this.fromBranchId = fromBranchId;
    this.taxableAmount = taxableAmount;
    this.discountAmount = discountAmount;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSectionName() {
    return this.sectionName;
  }

  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public Product getProductId() {
    return this.productId;
  }

  public void setProductId(Product productId) {
    this.productId = productId;
  }

  public InventorySection getInventorySectionId() {
    return this.inventorySectionId;
  }

  public void setInventorySectionId(InventorySection inventorySectionId) {
    this.inventorySectionId = inventorySectionId;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(String referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getBuyingPrice() {
    return buyingPrice;
  }

  public void setBuyingPrice(BigDecimal buyingPrice) {
    this.buyingPrice = buyingPrice;
  }

  public Vendor getVendor() {
    return vendor;
  }

  public void setVendor(Vendor vendor) {
    this.vendor = vendor;
  }

  public InventorySection getFromSectionId() {
    return fromSectionId;
  }

  public void setFromSectionId(InventorySection fromSectionId) {
    this.fromSectionId = fromSectionId;
  }

  public Branch getFromBranchId() {
    return fromBranchId;
  }

  public void setFromBranchId(Branch fromBranchId) {
    this.fromBranchId = fromBranchId;
  }

  public BigDecimal getTaxableAmount() {
    return taxableAmount;
  }

  public void setTaxableAmount(BigDecimal taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  public BigDecimal getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }
}
