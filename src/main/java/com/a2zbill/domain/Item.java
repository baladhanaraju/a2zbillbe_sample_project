package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "gst_items", schema = "gst")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class Item implements Serializable {
  private static final long serialVersionUID = 1L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "gst_items_id_seq")
  @SequenceGenerator(
      name = "gst_items_id_seq",
      sequenceName = "gst_items_id_seq",
      allocationSize = 1,
      schema = "gst")
  @Column(name = "sl_no")
  private long id;

  /** The hsnCode. */
  @Column(name = "hsn_no")
  private String hsnCode;

  /** The commodity. */
  @Column(name = "commodity")
  private String commodity;

  /** The createdDate. */
  @Column(name = "created_date")
  private Date createdDate;

  /** The categoryId. */
  @Column(name = "category_id")
  private long categoryId;

  /** The percentageDetails. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "percentage_id", insertable = true, updatable = true)
  private Percentage percentageDetails;

  /*
   * @JsonBackReference
   *
   * @OneToMany(fetch = FetchType.LAZY, mappedBy = "itemCategory", cascade =
   * CascadeType.ALL) private Set<Product> productDetail = new
   * HashSet<Product>(0);
   */

  /** Instantiates a new Item. */
  public Item() {}

  /**
   * Instantiates a new Counter.
   *
   * @param id the id
   * @param hsnCode the hsnCode
   * @param commodity the commodity
   * @param createdDate the createdDate
   * @param categoryId the categoryId
   * @param percentageDetails the percentageDetails
   */
  public Item(
      final long id,
      final String hsnCode,
      final String commodity,
      final Date createdDate,
      final long categoryId,
      final Percentage percentageDetails) {
    super();
    this.id = id;
    this.hsnCode = hsnCode;
    this.commodity = commodity;
    this.createdDate = createdDate;
    this.categoryId = categoryId;
    this.percentageDetails = percentageDetails;
  }

  /**
   * Gets the Id.
   *
   * @return the Id
   */
  public long getId() {
    return this.id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(final long id) {
    this.id = id;
  }

  /**
   * Gets the hsnCode.
   *
   * @return the hsnCode
   */
  public String getHsnCode() {
    return this.hsnCode;
  }

  /**
   * Sets the hsnCode.
   *
   * @param hsnCode the new hsnCode
   */
  public void setHsnCode(final String hsnCode) {
    this.hsnCode = hsnCode;
  }

  /**
   * Gets the commodity.
   *
   * @return the commodity
   */
  public String getCommodity() {
    return this.commodity;
  }

  /**
   * Sets the commodity.
   *
   * @param commodity the new commodity
   */
  public void setCommodity(final String commodity) {
    this.commodity = commodity;
  }

  /**
   * Gets the createdDate.
   *
   * @return the createdDate
   */
  public Date getCreatedDate() {
    return this.createdDate;
  }

  /**
   * Sets the createdDate.
   *
   * @param createdDate the new createdDate
   */
  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  /**
   * Gets the categoryId.
   *
   * @return the categoryId
   */
  public long getCategoryId() {
    return this.categoryId;
  }

  /**
   * Sets the categoryId.
   *
   * @param categoryId the new categoryId
   */
  public void setCategoryId(final long categoryId) {
    this.categoryId = categoryId;
  }

  public Percentage getPercentageDetails() {
    return this.percentageDetails;
  }

  public void setPercentageDetails(final Percentage percentageDetails) {
    this.percentageDetails = percentageDetails;
  }

  @Override
  public String toString() {
    return "Item [id="
        + id
        + ", hsnCode="
        + hsnCode
        + ", commodity="
        + commodity
        + ", createdDate="
        + createdDate
        + ", categoryId="
        + categoryId
        + ", percentageDetails="
        + percentageDetails
        + "]";
  }
}
