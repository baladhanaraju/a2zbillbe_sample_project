package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "exchange_history", schema = "customer")
public class ExchangeHistory {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "exchange_history_id_seq")
  @SequenceGenerator(
      name = "exchange_history_id_seq",
      sequenceName = "exchange_history_id_seq",
      allocationSize = 1,
      schema = "customer")
  @Column(name = "id")
  private long id;

  @Column(name = "amount")
  private BigDecimal amount;

  @Column(name = "product_name")
  private String productName;

  @Column(name = "exchange_type")
  private String exchangeType;

  @Column(name = "date")
  private Date date;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "billing_id", unique = true, nullable = true)
  private Billing billingDetails;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product productId;

  public ExchangeHistory() {}

  public ExchangeHistory(
      long id,
      BigDecimal amount,
      String productName,
      String exchangeType,
      Date date,
      Organisation organisation,
      Branch branch,
      Billing billingDetails,
      Product productId) {
    super();
    this.id = id;
    this.amount = amount;
    this.productName = productName;
    this.exchangeType = exchangeType;
    this.date = date;
    this.organisation = organisation;
    this.branch = branch;
    this.billingDetails = billingDetails;
    this.productId = productId;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public BigDecimal getAmount() {
    return this.amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getProductName() {
    return this.productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getExchangeType() {
    return this.exchangeType;
  }

  public void setExchangeType(String exchangeType) {
    this.exchangeType = exchangeType;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Billing getBillingDetails() {
    return this.billingDetails;
  }

  public void setBillingDetails(Billing billingDetails) {
    this.billingDetails = billingDetails;
  }

  public Product getProductId() {
    return this.productId;
  }

  public void setProductId(Product productId) {
    this.productId = productId;
  }

  @Override
  public String toString() {
    return "ExchangeHistory [id="
        + id
        + ", amount="
        + amount
        + ", productName="
        + productName
        + ", exchangeType="
        + exchangeType
        + ", date="
        + date
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", billingDetails="
        + billingDetails
        + ", productId="
        + productId
        + "]";
  }
}
