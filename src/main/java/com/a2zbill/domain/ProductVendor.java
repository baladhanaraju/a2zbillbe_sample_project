package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Vendor;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "product_vendor", schema = "product")
public class ProductVendor {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "vendors_products_id_seq")
  @SequenceGenerator(
      name = "vendors_products_id_seq",
      sequenceName = "vendors_products_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "vendor_id", unique = true, nullable = true)
  private Vendor vendor;

  @Column(name = "expected_delivery_duration")
  private int expectedDuration;

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Product getProduct() {
    return this.product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Vendor getVendor() {
    return this.vendor;
  }

  public void setVendor(Vendor vendor) {
    this.vendor = vendor;
  }

  public int getExpectedDuration() {
    return this.expectedDuration;
  }

  public void setExpectedDuration(int expectedDuration) {
    this.expectedDuration = expectedDuration;
  }
}
