package com.a2zbill.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "kot_history", schema = "kot")
@SuppressWarnings("serial")
public class KotHistory implements Serializable {

  private long id;

  private String productName;

  private int quantity;

  private long counterNumber;

  private long branchId;

  private String section;

  private long kotStatusId;

  private long empId;

  private String productImage;

  @Column(name = "kot_count_id")
  private long kotCountId;

  public KotHistory() {}

  public KotHistory(
      long id,
      String productName,
      int quantity,
      long counterNumber,
      long branchId,
      String section,
      long kotStatusId,
      long empId,
      String productImage,
      long kotCountId) {
    super();
    this.id = id;
    this.productName = productName;
    this.quantity = quantity;
    this.counterNumber = counterNumber;
    this.branchId = branchId;
    this.section = section;
    this.kotStatusId = kotStatusId;
    this.empId = empId;
    this.productImage = productImage;
    this.kotCountId = kotCountId;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "kot_history_id")
  @SequenceGenerator(
      name = "kot_history_id",
      sequenceName = "kot_history_id",
      allocationSize = 1,
      schema = "kot")
  @Column(name = "id")
  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Column(name = "product_name")
  public String getProductName() {
    return this.productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  @Column(name = "quantity")
  public int getQuantity() {
    return this.quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Column(name = "counter_number")
  public long getCounterNumber() {
    return this.counterNumber;
  }

  public void setCounterNumber(long counterNumber) {
    this.counterNumber = counterNumber;
  }

  @Column(name = "branch_id")
  public long getBranchId() {
    return this.branchId;
  }

  public void setBranchId(long branchId) {
    this.branchId = branchId;
  }

  @Column(name = "section")
  public String getSection() {
    return this.section;
  }

  public void setSection(String section) {
    this.section = section;
  }

  @Column(name = "kot_status_id")
  public long getKotStatusId() {
    return this.kotStatusId;
  }

  public void setKotStatusId(long kotStatusId) {
    this.kotStatusId = kotStatusId;
  }

  @Column(name = "emp_id")
  public long getEmpId() {
    return this.empId;
  }

  public void setEmpId(long empId) {
    this.empId = empId;
  }

  @Column(name = "product_image")
  public String getProductImage() {
    return this.productImage;
  }

  public void setProductImage(String productImage) {
    this.productImage = productImage;
  }

  public long getKotCountId() {
    return this.kotCountId;
  }

  public void setKotCountId(long kotCountId) {
    this.kotCountId = kotCountId;
  }
}
