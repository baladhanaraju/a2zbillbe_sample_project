package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "documents_data", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class DocumentsData implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product.documents_data_id_seq")
  @SequenceGenerator(
      name = "product.documents_data_id_seq",
      sequenceName = "product.documents_data_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "document_name")
  private String documentName;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "category_id", unique = true, nullable = true)
  private ProductCategory categoryId;

  @Column(name = "image")
  private byte[] image;

  @Column(name = "data_image")
  private String imageData;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation orgId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branchId;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getDocumentName() {
    return documentName;
  }

  public void setDocumentName(final String documentName) {
    this.documentName = documentName;
  }

  public ProductCategory getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(final ProductCategory categoryId) {
    this.categoryId = categoryId;
  }

  public String getImageData() {
    return imageData;
  }

  public void setImageData(final String imageData) {
    this.imageData = imageData;
  }

  public Organisation getOrgId() {
    return orgId;
  }

  public void setOrgId(final Organisation orgId) {
    this.orgId = orgId;
  }

  public Branch getBranchId() {
    return branchId;
  }

  public void setBranchId(final Branch branchId) {
    this.branchId = branchId;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public byte[] getImage() {
    return image;
  }

  public void setImage(final byte[] image) {
    this.image = image;
  }

  @Override
  public String toString() {
    return "DocumentsData [id="
        + id
        + ", categoryId="
        + categoryId
        + ", image="
        + image
        + ", imageData="
        + imageData
        + ", orgId="
        + orgId
        + ", branchId="
        + branchId
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + "]";
  }
}
