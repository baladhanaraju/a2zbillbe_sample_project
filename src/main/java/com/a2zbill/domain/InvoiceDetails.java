package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "invoice", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
public class InvoiceDetails implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "invoice_id_seq")
  @SequenceGenerator(
      name = "invoice_id_seq",
      sequenceName = "invoice_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "start_date")
  private Date startDate;

  @Column(name = "end_date")
  private Date endDate;

  @Column(name = "status_flag")
  private String status;

  @Column(name = "reason")
  private String reason;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "vendor_id", unique = true, nullable = true)
  private Vendor vendor;

  public InvoiceDetails() {}

  public InvoiceDetails(
      long id,
      Date startDate,
      Date endDate,
      String status,
      String reason,
      Organisation organisation,
      Branch branch,
      Vendor vendor) {
    super();
    this.id = id;
    this.startDate = startDate;
    this.endDate = endDate;
    this.status = status;
    this.reason = reason;
    this.organisation = organisation;
    this.branch = branch;
    this.vendor = vendor;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Date getStartDate() {
    return this.startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return this.endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getReason() {
    return this.reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Vendor getVendor() {
    return this.vendor;
  }

  public void setVendor(Vendor vendor) {
    this.vendor = vendor;
  }
}
