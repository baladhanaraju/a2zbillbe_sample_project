package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "products", schema = "product")
@Cacheable
@org.hibernate.annotations.Cache(
    usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE)
public class Product implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "products_id_seq")
  @SequenceGenerator(
      name = "products_id_seq",
      sequenceName = "products_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "product_name")
  private String productName;

  @Column(name = "hsn_code")
  private String hsnNumber;

  @Column(name = "product_price")
  private BigDecimal productPrice;

  @Column(name = "code")
  private String code;

  @Column(name = "product_count")
  private long productCount;

  @Column(name = "package_flag")
  private boolean packageFlag;

  @Column(name = "including_tax_flag")
  private String includingTaxFlag;

  /** The productCategory. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_category", insertable = true, updatable = true)
  private ProductCategory productCategory;

  @Column(name = "product_features", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> productFeatures;

  @Column(name = "images")
  private String image;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @Column(name = "cgst")
  private BigDecimal CGST;

  /** The SGST. */
  @Column(name = "sgst")
  private BigDecimal SGST;

  /** The IGST. */
  @Column(name = "igst")
  private BigDecimal IGST;

  @Column(name = "veg_flag")
  private boolean vegFlag;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "variation_type_id")
  private VariationTypes variationType;

  @Column(name = "parent_id")
  private long parentId;

  @Column(name = "status")
  private boolean status;

  @Column(name = "subscription_flag")
  private boolean subscriptionFlag;

  @Column(name = "product_packaging", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> productPackaging;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "variation_type_id_1")
  private VariationTypes variationType1;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "variation_type_id_2")
  private VariationTypes variationType2;

  @Column(name = "measurement")
  private String measurement;

  @Column(name = "measurement_1")
  private String measurement1;

  @Column(name = "measurement_2")
  private String measurement2;

  public Product() {}

  public String getImage() {
    return this.image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Product(String productName) {
    this.productName = "Invalid HSN Number";
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public String getIncludingTaxFlag() {
    return this.includingTaxFlag;
  }

  public void setIncludingTaxFlag(String includingTaxFlag) {
    this.includingTaxFlag = includingTaxFlag;
  }

  public long getProductCount() {
    return this.productCount;
  }

  public void setProductCount(long productCount) {
    this.productCount = productCount;
  }

  public long getId() {
    return this.id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(long id) {
    this.id = id;
  }

  public String getProductName() {
    return this.productName;
  }

  /**
   * Sets the productName.
   *
   * @param productName the new productName
   */
  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getHsnNumber() {
    return this.hsnNumber;
  }

  /**
   * Sets the hsnNumber.
   *
   * @param hsnNumber the new hsnNumber
   */
  public void setHsnNumber(String hsnNumber) {
    this.hsnNumber = hsnNumber;
  }

  /**
   * Sets the productPrice.
   *
   * <p>the new productPrice
   */
  public BigDecimal getProductPrice() {
    return this.productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public boolean getPackageFlag() {
    return this.packageFlag;
  }

  public void setPackageFlag(boolean packageFlag) {
    this.packageFlag = packageFlag;
  }

  public ProductCategory getProductCategory() {
    return this.productCategory;
  }

  public void setProductCategory(ProductCategory productCategory) {
    this.productCategory = productCategory;
  }

  public BigDecimal getCGST() {
    return this.CGST;
  }

  public void setCGST(BigDecimal cGST) {
    this.CGST = cGST;
  }

  public BigDecimal getSGST() {
    return this.SGST;
  }

  public void setSGST(BigDecimal sGST) {
    this.SGST = sGST;
  }

  public BigDecimal getIGST() {
    return this.IGST;
  }

  public void setIGST(BigDecimal iGST) {
    this.IGST = iGST;
  }

  public boolean isVegFlag() {
    return vegFlag;
  }

  public void setVegFlag(boolean vegFlag) {
    this.vegFlag = vegFlag;
  }

  public VariationTypes getVariationType() {
    return variationType;
  }

  public void setVariationType(VariationTypes variationType) {
    this.variationType = variationType;
  }

  public long getParentId() {
    return parentId;
  }

  public void setParentId(long parentId) {
    this.parentId = parentId;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public boolean isSubscriptionFlag() {
    return subscriptionFlag;
  }

  public void setSubscriptionFlag(boolean subscriptionFlag) {
    this.subscriptionFlag = subscriptionFlag;
  }

  public Map<String, Object> getProductFeatures() {
    return productFeatures;
  }

  public void setProductFeatures(Map<String, Object> productFeatures) {
    this.productFeatures = productFeatures;
  }

  public Map<String, Object> getProductPackaging() {
    return productPackaging;
  }

  public void setProductPackaging(Map<String, Object> productPackaging) {
    this.productPackaging = productPackaging;
  }

  public VariationTypes getVariationType1() {
    return variationType1;
  }

  public void setVariationType1(VariationTypes variationType1) {
    this.variationType1 = variationType1;
  }

  public VariationTypes getVariationType2() {
    return variationType2;
  }

  public void setVariationType2(VariationTypes variationType2) {
    this.variationType2 = variationType2;
  }

  public String getMeasurement() {
    return measurement;
  }

  public void setMeasurement(String measurement) {
    this.measurement = measurement;
  }

  public String getMeasurement1() {
    return measurement1;
  }

  public void setMeasurement1(String measurement1) {
    this.measurement1 = measurement1;
  }

  public String getMeasurement2() {
    return measurement2;
  }

  public void setMeasurement2(String measurement2) {
    this.measurement2 = measurement2;
  }

  public Product(
      long id,
      String productName,
      String hsnNumber,
      BigDecimal productPrice,
      String code,
      long productCount,
      boolean packageFlag,
      String includingTaxFlag,
      ProductCategory productCategory,
      Map<String, Object> productFeatures,
      String image,
      Organisation organisation,
      Branch branch,
      BigDecimal cGST,
      BigDecimal sGST,
      BigDecimal iGST,
      boolean vegFlag,
      VariationTypes variationType,
      long parentId,
      boolean status,
      boolean subscriptionFlag,
      Map<String, Object> productPackaging,
      VariationTypes variationType1,
      VariationTypes variationType2,
      String measurement,
      String measurement1,
      String measurement2) {
    super();
    this.id = id;
    this.productName = productName;
    this.hsnNumber = hsnNumber;
    this.productPrice = productPrice;
    this.code = code;
    this.productCount = productCount;
    this.packageFlag = packageFlag;
    this.includingTaxFlag = includingTaxFlag;
    this.productCategory = productCategory;
    this.productFeatures = productFeatures;
    this.image = image;
    this.organisation = organisation;
    this.branch = branch;
    CGST = cGST;
    SGST = sGST;
    IGST = iGST;
    this.vegFlag = vegFlag;
    this.variationType = variationType;
    this.parentId = parentId;
    this.status = status;
    this.subscriptionFlag = subscriptionFlag;
    this.productPackaging = productPackaging;
    this.variationType1 = variationType1;
    this.variationType2 = variationType2;
    this.measurement = measurement;
    this.measurement1 = measurement1;
    this.measurement2 = measurement2;
  }

  @Override
  public String toString() {
    return "Product [id="
        + id
        + ", productName="
        + productName
        + ", hsnNumber="
        + hsnNumber
        + ", productPrice="
        + productPrice
        + ", code="
        + code
        + ", productCount="
        + productCount
        + ", packageFlag="
        + packageFlag
        + ", includingTaxFlag="
        + includingTaxFlag
        + ", productCategory="
        + productCategory
        + ", productFeatures="
        + productFeatures
        + ", image="
        + image
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", CGST="
        + CGST
        + ", SGST="
        + SGST
        + ", IGST="
        + IGST
        + ", vegFlag="
        + vegFlag
        + ", quantityMeasurements= "
        + variationType
        + ", parentId="
        + parentId
        + ", status="
        + status
        + ", subscriptionFlag="
        + subscriptionFlag
        + ", productPackaging="
        + productPackaging
        + ", variationType1="
        + variationType1
        + ", variationType2="
        + variationType2
        + ", measurement="
        + measurement
        + ", measurement1="
        + measurement1
        + ", measurement2="
        + measurement2
        + "]";
  }
}
