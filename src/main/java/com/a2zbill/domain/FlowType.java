package com.a2zbill.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "flow_type", schema = "product")
public class FlowType implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "flow_type_id_seq")
  @SequenceGenerator(
      name = "flow_type_id_seq",
      sequenceName = "flow_type_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "type_name")
  private String typeName;

  public FlowType() {}

  public FlowType(final long id, final String typeName) {
    super();
    this.id = id;
    this.typeName = typeName;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getTypeName() {
    return this.typeName;
  }

  public void setTypeName(final String typeName) {
    this.typeName = typeName;
  }
}
