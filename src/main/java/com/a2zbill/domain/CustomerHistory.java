package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Organisation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "customer_history", schema = "customer")
public class CustomerHistory {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "customer_history_id_seq")
  @SequenceGenerator(
      name = "customer_history_id_seq",
      sequenceName = "customer_history_id_seq",
      allocationSize = 1,
      schema = "customer")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", insertable = true, updatable = true)
  private Customer customer;

  @Column(name = "credit_amount")
  private BigDecimal creditAmount;

  /** The taxableAmount. */
  @Column(name = "debit_amount")
  private BigDecimal debitAmount;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @Column(name = "date")
  private Date date;

  public CustomerHistory() {}

  public CustomerHistory(
      long id,
      Customer customer,
      BigDecimal creditAmount,
      BigDecimal debitAmount,
      Organisation organisation,
      Branch branch,
      Date date) {
    super();
    this.id = id;
    this.customer = customer;
    this.creditAmount = creditAmount;
    this.debitAmount = debitAmount;
    this.organisation = organisation;
    this.branch = branch;
    this.date = date;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Customer getCustomer() {
    return this.customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public BigDecimal getCreditAmount() {
    return this.creditAmount;
  }

  public void setCreditAmount(BigDecimal creditAmount) {
    this.creditAmount = creditAmount;
  }

  public BigDecimal getDebitAmount() {
    return this.debitAmount;
  }

  public void setDebitAmount(BigDecimal debitAmount) {
    this.debitAmount = debitAmount;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
  public String toString() {
    return "CustomerHistory [id="
        + id
        + ", customer="
        + customer
        + ", creditAmount="
        + creditAmount
        + ", debitAmount="
        + debitAmount
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", date="
        + date
        + "]";
  }
}
