package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "rules", schema = "offers")
public class Rules {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "rule_id_seq")
  @SequenceGenerator(
      name = "rule_id_seq",
      sequenceName = "rule_id_seq",
      allocationSize = 1,
      schema = "offers")
  @Column(name = "rule_id", nullable = false)
  private long ruleId;

  @Column(name = "rule_name")
  private String ruleName;

  @Column(name = "query")
  private String query;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branchId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation orgId;

  public Rules() {}

  public Rules(long ruleId, String ruleName, String query, Branch branchId, Organisation orgId) {
    super();
    this.ruleId = ruleId;
    this.ruleName = ruleName;
    this.query = query;
    this.branchId = branchId;
    this.orgId = orgId;
  }

  public Branch getBranchId() {
    return this.branchId;
  }

  public void setBranchId(Branch branchId) {
    this.branchId = branchId;
  }

  public Organisation getOrgId() {
    return this.orgId;
  }

  public void setOrgId(Organisation orgId) {
    this.orgId = orgId;
  }

  public long getRuleId() {
    return this.ruleId;
  }

  public void setRuleId(long ruleId) {
    this.ruleId = ruleId;
  }

  public String getRuleName() {
    return this.ruleName;
  }

  public void setRuleName(String ruleName) {
    this.ruleName = ruleName;
  }

  public String getQuery() {
    return this.query;
  }

  public void setQuery(String query) {
    this.query = query;
  }
}
