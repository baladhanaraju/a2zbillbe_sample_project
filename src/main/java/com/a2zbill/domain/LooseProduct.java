package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "package_products", schema = "product")
public class LooseProduct implements Serializable {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "loose_product_id_seq")
  @SequenceGenerator(
      name = "loose_product_id_seq",
      sequenceName = "loose_product_id_seq",
      allocationSize = 1,
      schema = "product")
  private long id;

  @Column(name = "product_name")
  private String productName;

  @Column(name = "hsncode")
  private String hsnCode;

  @Column(name = "package_product_code")
  private String barCode;

  @Column(name = "price")
  private BigDecimal price;

  @Column(name = "quantity")
  private double quantity;

  /** The looseProducts. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public LooseProduct() {}

  public LooseProduct(
      final long id,
      final String productName,
      final String hsnCode,
      final String barCode,
      final BigDecimal price,
      final double quantity,
      final Product product,
      final Branch branch,
      final Organisation organisation) {
    super();
    this.id = id;
    this.productName = productName;
    this.hsnCode = hsnCode;
    this.barCode = barCode;
    this.price = price;
    this.quantity = quantity;
    this.branch = branch;
    this.organisation = organisation;
    this.product = product;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getProductName() {
    return this.productName;
  }

  public void setProductName(final String productName) {
    this.productName = productName;
  }

  public String getHsnCode() {
    return this.hsnCode;
  }

  public void setHsnCode(final String hsnCode) {
    this.hsnCode = hsnCode;
  }

  public String getBarCode() {
    return this.barCode;
  }

  public void setBarCode(final String barCode) {
    this.barCode = barCode;
  }

  public BigDecimal getPrice() {
    return this.price;
  }

  public void setPrice(final BigDecimal price) {
    this.price = price;
  }

  public double getQuantity() {
    return this.quantity;
  }

  public void setQuantity(final double quantity) {
    this.quantity = quantity;
  }

  public Product getProduct() {
    return this.product;
  }

  public void setProduct(final Product product) {
    this.product = product;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  @Override
  public String toString() {
    return "LooseProduct [id="
        + id
        + ", productName="
        + productName
        + ", hsnCode="
        + hsnCode
        + ", barCode="
        + barCode
        + ", price="
        + price
        + ", quantity="
        + quantity
        + ", product="
        + product
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + "]";
  }
}
