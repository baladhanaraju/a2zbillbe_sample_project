package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "order_line_items", schema = "orders")
public class OrderLineItems implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "order_line_items_id_seq")
  @SequenceGenerator(
      name = "order_line_items_id_seq",
      sequenceName = "order_line_items_id_seq",
      allocationSize = 1,
      schema = "orders")
  @Column(name = "id")
  private long id;

  @Column(name = "product_name")
  private String productName;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "counter_id", insertable = true, updatable = true)
  private Counter counterDetails;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "order_id", unique = true, nullable = true)
  private Order orderId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  @Column(name = "taxable_amount")
  private BigDecimal taxableAmount;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branchId;

  @Column(name = "cgst")
  private BigDecimal cgst;

  @Column(name = "igst")
  private BigDecimal igst;

  @Column(name = "sgst")
  private BigDecimal sgst;

  @Column(name = "instructions")
  private String instructions;

  @Column(name = "hsn_code")
  private String hsnCode;

  @Column(name = "quantity")
  private long quantity;

  @Column(name = "product_price")
  private BigDecimal productPrice;

  @Column(name = "total_amount")
  private BigDecimal totalAmount;

  @Column(name = "discount_amount")
  private BigDecimal discountAmount;

  @Column(name = "date")
  private Date time;

  public OrderLineItems() {}

  public OrderLineItems(
      final long id,
      final String productName,
      final Counter counterDetails,
      final Order orderId,
      final Organisation organisation,
      final BigDecimal taxableAmount,
      final Branch branchId,
      final BigDecimal cgst,
      final BigDecimal igst,
      final BigDecimal sgst,
      final String instructions,
      final String hsnCode,
      final long quantity,
      final BigDecimal productPrice,
      final BigDecimal totalAmount,
      final BigDecimal discountAmount,
      final Date time) {
    super();
    this.id = id;
    this.productName = productName;
    this.counterDetails = counterDetails;
    this.orderId = orderId;
    this.organisation = organisation;
    this.taxableAmount = taxableAmount;
    this.branchId = branchId;
    this.cgst = cgst;
    this.igst = igst;
    this.sgst = sgst;
    this.instructions = instructions;
    this.hsnCode = hsnCode;
    this.quantity = quantity;
    this.productPrice = productPrice;
    this.totalAmount = totalAmount;
    this.discountAmount = discountAmount;
    this.time = time;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(final String productName) {
    this.productName = productName;
  }

  public Order getOrderId() {
    return orderId;
  }

  public void setOrderId(final Order orderId) {
    this.orderId = orderId;
  }

  public Branch getBranchId() {
    return branchId;
  }

  public void setBranchId(final Branch branchId) {
    this.branchId = branchId;
  }

  public BigDecimal getCgst() {
    return cgst;
  }

  public void setCgst(final BigDecimal cgst) {
    this.cgst = cgst;
  }

  public BigDecimal getIgst() {
    return igst;
  }

  public void setIgst(final BigDecimal igst) {
    this.igst = igst;
  }

  public BigDecimal getSgst() {
    return sgst;
  }

  public void setSgst(final BigDecimal sgst) {
    this.sgst = sgst;
  }

  public String getInstructions() {
    return instructions;
  }

  public void setInstructions(final String instructions) {
    this.instructions = instructions;
  }

  public String getHsnCode() {
    return hsnCode;
  }

  public void setHsnCode(final String hsnCode) {
    this.hsnCode = hsnCode;
  }

  public long getQuantity() {
    return quantity;
  }

  public void setQuantity(final long quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(final BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(final BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public BigDecimal getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(final BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(final Date time) {
    this.time = time;
  }

  public BigDecimal getTaxableAmount() {
    return taxableAmount;
  }

  public void setTaxableAmount(final BigDecimal taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  public Counter getCounterDetails() {
    return counterDetails;
  }

  public void setCounterDetails(final Counter counterDetails) {
    this.counterDetails = counterDetails;
  }
}
