package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Vendor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "inward", schema = "gstforms")
public class Inward implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inword_id_seq")
  @SequenceGenerator(
      name = "inword_id_seq",
      sequenceName = "inword_id_seq",
      allocationSize = 1,
      schema = "gstforms")
  @Column(name = "id")
  private long id;

  @Column(name = "date")
  private Date date;

  @Column(name = "billing_amount")
  private BigDecimal billingamount;

  @Column(name = "cgst_amount")
  private BigDecimal cgst_amount;

  @Column(name = "sgst_amount")
  private BigDecimal sgst_amount;

  @Column(name = "igst_amount")
  private BigDecimal igst_amount;

  @Column(name = "billing_id")
  private long billingid;

  @Column(name = "gst_number")
  private String gst_number;

  @Column(name = "month")
  private String month;

  @Column(name = "year")
  private String year;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "vendor_id", insertable = true, updatable = true)
  private Vendor vendorid;

  public Inward() {}

  public Inward(
      final long id,
      final Date date,
      final BigDecimal billingamount,
      final BigDecimal cgst_amount,
      final BigDecimal sgst_amount,
      final BigDecimal igst_amount,
      final long billingid,
      final String gst_number,
      final String month,
      final String year,
      final Vendor vendorid) {
    super();
    this.id = id;
    this.date = date;
    this.billingamount = billingamount;
    this.cgst_amount = cgst_amount;
    this.sgst_amount = sgst_amount;
    this.igst_amount = igst_amount;
    this.billingid = billingid;
    this.gst_number = gst_number;
    this.month = month;
    this.year = year;
    this.vendorid = vendorid;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public BigDecimal getBillingamount() {
    return this.billingamount;
  }

  public void setBillingamount(final BigDecimal billingamount) {
    this.billingamount = billingamount;
  }

  public BigDecimal getCgst_amount() {
    return this.cgst_amount;
  }

  public void setCgst_amount(final BigDecimal cgst_amount) {
    this.cgst_amount = cgst_amount;
  }

  public BigDecimal getSgst_amount() {
    return this.sgst_amount;
  }

  public void setSgst_amount(final BigDecimal sgst_amount) {
    this.sgst_amount = sgst_amount;
  }

  public BigDecimal getIgst_amount() {
    return this.igst_amount;
  }

  public void setIgst_amount(final BigDecimal igst_amount) {
    this.igst_amount = igst_amount;
  }

  public long getBillingid() {
    return this.billingid;
  }

  public void setBillingid(final long billingid) {
    this.billingid = billingid;
  }

  public Vendor getVendorid() {
    return this.vendorid;
  }

  public void setVendorid(final Vendor vendorid) {
    this.vendorid = vendorid;
  }

  public String getGst_number() {
    return this.gst_number;
  }

  public void setGst_number(final String gst_number) {
    this.gst_number = gst_number;
  }

  public String getMonth() {
    return this.month;
  }

  public void setMonth(final String month) {
    this.month = month;
  }

  public String getYear() {
    return this.year;
  }

  public void setYear(final String year) {
    this.year = year;
  }

  @Override
  public String toString() {
    return "Inward [id="
        + id
        + ", date="
        + date
        + ", billingamount="
        + billingamount
        + ", cgst_amount="
        + cgst_amount
        + ", sgst_amount="
        + sgst_amount
        + ", igst_amount="
        + igst_amount
        + ", billingid="
        + billingid
        + ", gst_number="
        + gst_number
        + ", month="
        + month
        + ", year="
        + year
        + ", vendorid="
        + vendorid
        + "]";
  }
}
