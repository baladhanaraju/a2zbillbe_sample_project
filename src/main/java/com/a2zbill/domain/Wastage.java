package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "wastage", schema = "product")
public class Wastage implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "wastage_id_seq")
  @SequenceGenerator(
      name = "wastage_id_seq",
      sequenceName = "wastage_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "quantity")
  private long quantity;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product product;

  @Column(name = "date")
  private Date date;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branchid", insertable = true, updatable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "orgid", insertable = true, updatable = true)
  private Organisation organisation;

  public Wastage() {}

  public Wastage(
      long id,
      long quantity,
      Product product,
      Date date,
      Date createdDate,
      Date modifiedDate,
      Branch branch,
      Organisation organisation) {
    super();
    this.id = id;
    this.quantity = quantity;
    this.product = product;
    this.date = date;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.branch = branch;
    this.organisation = organisation;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getQuantity() {
    return quantity;
  }

  public void setQuantity(long quantity) {
    this.quantity = quantity;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  @Override
  public String toString() {
    return "Wastage [id="
        + id
        + ", quantity="
        + quantity
        + ", product="
        + product
        + ", date="
        + date
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", branch="
        + branch
        + ", organisation="
        + organisation
        + "]";
  }
}
