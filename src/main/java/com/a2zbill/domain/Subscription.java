package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "subscription", schema = "subscription")
public class Subscription implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "subscription_id_seq")
  @SequenceGenerator(
      name = "subscription_id_seq",
      sequenceName = "subscription_id_seq",
      allocationSize = 1,
      schema = "subscription")
  @Column(name = "id")
  private long id;

  @Column(name = "date")
  private Date date;

  @Column(name = "start_date")
  private Date startDate;

  @Column(name = "end_date")
  private Date endDate;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "auto_renewal")
  private boolean autoRenewal;

  @Column(name = "cron_expression")
  private String cronExpression;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "emp_id", insertable = true, updatable = true)
  private Employee employee;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", unique = true, nullable = true)
  private Customer customer;

  public Subscription() {}

  public Subscription(
      long id,
      Date date,
      Date startDate,
      Date endDate,
      Date createdDate,
      Date modifiedDate,
      boolean autoRenewal,
      String cronExpression,
      Product product,
      Employee employee,
      Organisation organisation,
      Branch branch,
      Customer customer) {
    super();
    this.id = id;
    this.date = date;
    this.startDate = startDate;
    this.endDate = endDate;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.autoRenewal = autoRenewal;
    this.cronExpression = cronExpression;
    this.product = product;
    this.employee = employee;
    this.organisation = organisation;
    this.branch = branch;
    this.customer = customer;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public boolean isAutoRenewal() {
    return autoRenewal;
  }

  public void setAutoRenewal(boolean autoRenewal) {
    this.autoRenewal = autoRenewal;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Employee getEmployee() {
    return employee;
  }

  public void setEmployee(Employee employee) {
    this.employee = employee;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public String getCronExpression() {
    return cronExpression;
  }

  public void setCronExpression(String cronExpression) {
    this.cronExpression = cronExpression;
  }
}
