package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "variation_types", schema = "product")
public class VariationTypes {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "variation_type_id_seq")
  @SequenceGenerator(
      name = "variation_type_id_seq",
      sequenceName = "variation_type_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "variation_name")
  private String variationName;

  @Column(name = "display_name")
  private String displayName;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "quantity_measurement_id", unique = true, nullable = true)
  private QuantityMeasurements quantityMeasurement;

  @Column(name = "predefined_options", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> predefinedOptions;

  public VariationTypes() {}

  public VariationTypes(
      long id,
      String variationName,
      String displayName,
      QuantityMeasurements quantityMeasurement,
      Map<String, Object> predefinedOptions) {
    super();
    this.id = id;
    this.variationName = variationName;
    this.displayName = displayName;
    this.quantityMeasurement = quantityMeasurement;
    this.predefinedOptions = predefinedOptions;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getVariationName() {
    return variationName;
  }

  public void setVariationName(String variationName) {
    this.variationName = variationName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public QuantityMeasurements getQuantityMeasurement() {
    return quantityMeasurement;
  }

  public void setQuantityMeasurement(QuantityMeasurements quantityMeasurement) {
    this.quantityMeasurement = quantityMeasurement;
  }

  public Map<String, Object> getPredefinedOptions() {
    return predefinedOptions;
  }

  public void setPredefinedOptions(Map<String, Object> predefinedOptions) {
    this.predefinedOptions = predefinedOptions;
  }
}
