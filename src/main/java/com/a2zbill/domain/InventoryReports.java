package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "inventory_reports", schema = "product")
public class InventoryReports implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inventory_reports_id_seq")
  @SequenceGenerator(
      name = "inventory_reports_id_seq",
      sequenceName = "inventory_reports_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "inventory_id", unique = true, nullable = true)
  private InventoryDetail inventoryDetail;

  @Column(name = "stock_quantity")
  private BigDecimal stockQuantity;

  @Column(name = "consumed_quantity")
  private BigDecimal consumedQuantity;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "created_ts")
  private Date createdTime;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @Column(name = "modified_ts")
  private Date modifiedTime;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public InventoryDetail getInventoryDetail() {
    return inventoryDetail;
  }

  public void setInventoryDetail(InventoryDetail inventoryDetail) {
    this.inventoryDetail = inventoryDetail;
  }

  public BigDecimal getStockQuantity() {
    return stockQuantity;
  }

  public void setStockQuantity(BigDecimal stockQuantity) {
    this.stockQuantity = stockQuantity;
  }

  public BigDecimal getConsumedQuantity() {
    return consumedQuantity;
  }

  public void setConsumedQuantity(BigDecimal consumedQuantity) {
    this.consumedQuantity = consumedQuantity;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Date getModifiedTime() {
    return modifiedTime;
  }

  public void setModifiedTime(Date modifiedTime) {
    this.modifiedTime = modifiedTime;
  }

  @Override
  public String toString() {
    return "InventoryReports [id="
        + id
        + ", inventoryDetail="
        + inventoryDetail
        + ", stockQuantity="
        + stockQuantity
        + ", consumedQuantity="
        + consumedQuantity
        + ", createdDate="
        + createdDate
        + ", createdTime="
        + createdTime
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", modifiedTime="
        + modifiedTime
        + "]";
  }
}
