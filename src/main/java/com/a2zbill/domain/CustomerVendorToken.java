package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "customer_vendor_token", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = true)
@Proxy(lazy = false)
public class CustomerVendorToken implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "customer_vendor_token_id_seq")
  @SequenceGenerator(
      name = "customer_vendor_token_id_seq",
      sequenceName = "customer_vendor_token_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private Long id;

  @Column(name = "mobile_number", length = 15)
  private String mobileNumber;

  @Column(name = "token", length = 50)
  private String token;

  @Column(name = "root_name", length = 50)
  private String rootName;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public CustomerVendorToken() {}

  public CustomerVendorToken(
      final Long id,
      final String mobileNumber,
      final String token,
      final String rootName,
      final Organisation organisation,
      final Branch branch) {
    super();
    this.id = id;
    this.mobileNumber = mobileNumber;
    this.token = token;
    this.rootName = rootName;
    this.organisation = organisation;
    this.branch = branch;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getMobileNumber() {
    return this.mobileNumber;
  }

  public void setMobileNumber(final String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getToken() {
    return this.token;
  }

  public void setToken(final String token) {
    this.token = token;
  }

  public String getRootName() {
    return this.rootName;
  }

  public void setRootName(final String rootName) {
    this.rootName = rootName;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }
}
