package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Customer;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "outward", schema = "gstforms")
public class Outward implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "outward_id_seq")
  @SequenceGenerator(
      name = "outward_id_seq",
      sequenceName = "outward_id_seq",
      allocationSize = 1,
      schema = "gstforms")
  @Column(name = "id")
  private long id;

  /** The Billing details. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "billing_id", insertable = true, updatable = true)
  private Billing billingDetail;

  /** The CustomerDetails. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", insertable = true, updatable = true)
  private Customer customer;

  @Column(name = "gst_number")
  private String gstNumber;

  @Column(name = "price")
  private BigDecimal taxableAmount;

  @Column(name = "cgst_amount")
  private BigDecimal CGSTAmount;

  @Column(name = "sgst_amount")
  private BigDecimal SGSTAmount;

  @Column(name = "igst_amount")
  private BigDecimal IGSTAmount;

  @Column(name = "total_amount")
  private BigDecimal totalAmount;

  @Column(name = "date")
  private String OutwardDate;

  @Column(name = "month")
  private String month;

  @Column(name = "year")
  private String year;

  @Column(name = "customer_gst_number")
  private String customerGSTNumber;

  public Outward() {}

  public Outward(
      final long id,
      final Billing billingDetail,
      final Customer customer,
      final String gstNumber,
      final BigDecimal taxableAmount,
      final BigDecimal cGSTAmount,
      final BigDecimal sGSTAmount,
      final BigDecimal iGSTAmount,
      final BigDecimal totalAmount,
      final String outwardDate,
      final String month,
      final String year,
      final String customerGSTNumber) {
    super();
    this.id = id;
    this.billingDetail = billingDetail;
    this.customer = customer;
    this.gstNumber = gstNumber;
    this.taxableAmount = taxableAmount;
    this.CGSTAmount = cGSTAmount;
    this.SGSTAmount = sGSTAmount;
    this.IGSTAmount = iGSTAmount;
    this.totalAmount = totalAmount;
    this.OutwardDate = outwardDate;
    this.month = month;
    this.year = year;
    this.customerGSTNumber = customerGSTNumber;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Billing getBillingDetail() {
    return this.billingDetail;
  }

  public void setBillingDetail(final Billing billingDetail) {
    this.billingDetail = billingDetail;
  }

  public Customer getCustomer() {
    return this.customer;
  }

  public void setCustomer(final Customer customer) {
    this.customer = customer;
  }

  public String getGstNumber() {
    return this.gstNumber;
  }

  public void setGstNumber(final String gstNumber) {
    this.gstNumber = gstNumber;
  }

  public BigDecimal getTaxableAmount() {
    return this.taxableAmount;
  }

  public void setTaxableAmount(final BigDecimal taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  public BigDecimal getCGSTAmount() {
    return this.CGSTAmount;
  }

  public void setCGSTAmount(final BigDecimal cGSTAmount) {
    this.CGSTAmount = cGSTAmount;
  }

  public BigDecimal getSGSTAmount() {
    return this.SGSTAmount;
  }

  public void setSGSTAmount(final BigDecimal sGSTAmount) {
    this.SGSTAmount = sGSTAmount;
  }

  public BigDecimal getIGSTAmount() {
    return this.IGSTAmount;
  }

  public void setIGSTAmount(final BigDecimal iGSTAmount) {
    this.IGSTAmount = iGSTAmount;
  }

  public BigDecimal getTotalAmount() {
    return this.totalAmount;
  }

  public void setTotalAmount(final BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getOutwardDate() {
    return this.OutwardDate;
  }

  public void setOutwardDate(final String outwardDate) {
    this.OutwardDate = outwardDate;
  }

  public String getMonth() {
    return this.month;
  }

  public void setMonth(final String month) {
    this.month = month;
  }

  public String getYear() {
    return this.year;
  }

  public void setYear(final String year) {
    this.year = year;
  }

  public String getCustomerGSTNumber() {
    return this.customerGSTNumber;
  }

  public void setCustomerGSTNumber(final String customerGSTNumber) {
    this.customerGSTNumber = customerGSTNumber;
  }

  @Override
  public String toString() {
    return "Outward [id="
        + id
        + ", billingDetail="
        + billingDetail
        + ", customer="
        + customer
        + ", gstNumber="
        + gstNumber
        + ", taxableAmount="
        + taxableAmount
        + ", CGSTAmount="
        + CGSTAmount
        + ", SGSTAmount="
        + SGSTAmount
        + ", IGSTAmount="
        + IGSTAmount
        + ", totalAmount="
        + totalAmount
        + ", OutwardDate="
        + OutwardDate
        + ", month="
        + month
        + ", year="
        + year
        + ", customerGSTNumber="
        + customerGSTNumber
        + "]";
  }
}
