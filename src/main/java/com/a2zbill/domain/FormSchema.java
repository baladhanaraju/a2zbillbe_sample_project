package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "form_schema", schema = "form")
public class FormSchema implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "formschema_id_seq")
  @SequenceGenerator(
      name = "formschema_id_seq",
      sequenceName = "formschema_id_seq",
      allocationSize = 1,
      schema = "form")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "formtype_id", unique = true, nullable = true)
  private FormType formtype;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "form_schema", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> formschema;

  public FormSchema() {}

  public FormSchema(
      final long id,
      final FormType formtype,
      final Date createdDate,
      final Date modifiedDate,
      final Map<String, Object> formschema) {
    super();
    this.id = id;
    this.formtype = formtype;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.formschema = formschema;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public FormType getFormtype() {
    return this.formtype;
  }

  public void setFormtype(final FormType formtype) {
    this.formtype = formtype;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Map<String, Object> getFormschema() {
    return formschema;
  }

  public void setFormschema(final Map<String, Object> formschema) {
    this.formschema = formschema;
  }
}
