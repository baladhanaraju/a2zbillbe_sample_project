package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "product_composition", schema = "product")
public class ProductComposition implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_composition_id")
  @SequenceGenerator(
      name = "product_composition_id",
      sequenceName = "product_composition_id",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product productId;

  @Column(name = "package_flag")
  private String packageFlag;

  @Column(name = "json_data", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> jsonData;

  public ProductComposition() {}

  public ProductComposition(
      long id, Product productId, String packageFlag, Map<String, Object> jsonData) {
    super();
    this.id = id;
    this.productId = productId;
    this.packageFlag = packageFlag;
    this.jsonData = jsonData;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Product getProductId() {
    return this.productId;
  }

  public void setProductId(Product productId) {
    this.productId = productId;
  }

  public String getPackageFlag() {
    return this.packageFlag;
  }

  public void setPackageFlag(String packageFlag) {
    this.packageFlag = packageFlag;
  }

  public Map<String, Object> getJsonData() {
    return jsonData;
  }

  public void setJsonData(Map<String, Object> jsonData) {
    this.jsonData = jsonData;
  }

  @Override
  public String toString() {
    return "ProductComposition [id="
        + id
        + ", productId="
        + productId
        + ", packageFlag="
        + packageFlag
        + ", jsonData="
        + jsonData
        + "]";
  }
}
