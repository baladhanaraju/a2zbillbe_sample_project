package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "organisation_payment_modes", schema = "product")
public class OrganisationPayments implements Serializable {

  private long id;

  private Organisation orgId;

  private Payment paymentId;

  private Date createdDate;

  private Date modifiedDate;

  public OrganisationPayments() {}

  public OrganisationPayments(
      final long id,
      final Organisation orgId,
      final Payment paymentId,
      final Date createdDate,
      final Date modifiedDate) {
    super();
    this.id = id;
    this.orgId = orgId;
    this.paymentId = paymentId;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
  }

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "organisation_payment_modes_id_seq")
  @SequenceGenerator(
      name = "organisation_payment_modes_id_seq",
      sequenceName = "organisation_payment_modes_id_seq",
      allocationSize = 1,
      schema = "product")
  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  public Organisation getOrgId() {
    return orgId;
  }

  public void setOrgId(final Organisation orgId) {
    this.orgId = orgId;
  }

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "payment_id", insertable = true, updatable = true)
  public Payment getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(final Payment paymentId) {
    this.paymentId = paymentId;
  }

  @Column(name = "created_date")
  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  @Column(name = "modified_date")
  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  @Override
  public String toString() {
    return "OrganisationPayments [id="
        + id
        + ", orgId="
        + orgId
        + ", paymentId="
        + paymentId
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + "]";
  }
}
