package com.a2zbill.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "billing_num_type", schema = "product")
public class BillingNumType {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "billing_num_type_id_seq")
  @SequenceGenerator(
      name = "billing_num_type_id_seq",
      sequenceName = "billing_num_type_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "billing_type_name")
  private String billingTypeName;

  @Column(name = "type_value")
  private String typeValue;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  private BillingNumType() {}

  private BillingNumType(
      long id, String billingTypeName, String typeValue, Date createdDate, Date modifiedDate) {
    super();
    this.id = id;
    this.billingTypeName = billingTypeName;
    this.typeValue = typeValue;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getBillingTypeName() {
    return this.billingTypeName;
  }

  public void setBillingTypeName(String billingTypeName) {
    this.billingTypeName = billingTypeName;
  }

  public String getTypeValue() {
    return this.typeValue;
  }

  public void setTypeValue(String typeValue) {
    this.typeValue = typeValue;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }
}
