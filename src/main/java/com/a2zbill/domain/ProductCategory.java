package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.CategoryType;
import com.tsss.basic.domain.Organisation;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "category", schema = "product")
public class ProductCategory implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_category_id_seq")
  @SequenceGenerator(
      name = "product_category_id_seq",
      sequenceName = "product_category_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "producttype_name")
  private String productTypeName;

  @Column(name = "producttype_desc")
  private String productTypeDescription;

  @Column(name = "features", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> features;

  @Column(name = "images")
  private String image;

  @Column(name = " parentid")
  private long parentId;

  @Column(name = " category_ranking")
  private long ranking;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @Column(name = "redeem_flag")
  private boolean redeemFlag;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "category_type_id ", unique = true, nullable = true)
  private CategoryType categoryType;

  public long getParentId() {
    return this.parentId;
  }

  public void setParentId(final long parentId) {
    this.parentId = parentId;
  }

  public String getImage() {
    return this.image;
  }

  public void setImage(final String image) {
    this.image = image;
  }

  public ProductCategory() {}

  public ProductCategory(
      final long id,
      final String productTypeName,
      final String productTypeDescription,
      final Map<String, Object> features,
      final String image,
      final long parentId,
      final long ranking,
      final Organisation organisation,
      final Branch branch,
      final boolean redeemFlag,
      final CategoryType categoryType) {
    super();
    this.id = id;
    this.productTypeName = productTypeName;
    this.productTypeDescription = productTypeDescription;
    this.features = features;
    this.image = image;
    this.parentId = parentId;
    this.ranking = ranking;
    this.organisation = organisation;
    this.branch = branch;
    this.redeemFlag = redeemFlag;
    this.categoryType = categoryType;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getProductTypeName() {
    return this.productTypeName;
  }

  public void setProductTypeName(final String productTypeName) {
    this.productTypeName = productTypeName;
  }

  public String getProductTypeDescription() {
    return this.productTypeDescription;
  }

  public void setProductTypeDescription(final String productTypeDescription) {
    this.productTypeDescription = productTypeDescription;
  }

  public Map<String, Object> getFeatures() {
    return features;
  }

  public void setFeatures(final Map<String, Object> features) {
    this.features = features;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public boolean isRedeemFlag() {
    return this.redeemFlag;
  }

  public void setRedeemFlag(final boolean redeemFlag) {
    this.redeemFlag = redeemFlag;
  }

  public CategoryType getCategoryType() {
    return this.categoryType;
  }

  public void setCategoryType(final CategoryType categoryType) {
    this.categoryType = categoryType;
  }

  public long getRanking() {
    return this.ranking;
  }

  public void setRanking(final long ranking) {
    this.ranking = ranking;
  }

  @Override
  public String toString() {
    return "ProductCategory [id="
        + id
        + ", productTypeName="
        + productTypeName
        + ", productTypeDescription="
        + productTypeDescription
        + ", features="
        + features
        + ", image="
        + image
        + ", parentId="
        + parentId
        + ", ranking="
        + ranking
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", redeemFlag="
        + redeemFlag
        + ", categoryType="
        + categoryType
        + "]";
  }
}
