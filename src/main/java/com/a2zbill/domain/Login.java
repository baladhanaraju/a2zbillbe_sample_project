package com.a2zbill.domain;

import java.io.Serializable;

public class Login implements Serializable {
  private static final long serialVersionUID = 1L;

  private String userName;

  private String password;

  public Login() {}

  public String getUserName() {
    return userName;
  }

  public void setUserName(final String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }
}
