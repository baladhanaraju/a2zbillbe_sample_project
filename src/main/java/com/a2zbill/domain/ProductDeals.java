package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
    name = "product_deals",
    schema = "product",
    uniqueConstraints = {
      @UniqueConstraint(columnNames = "offer_title"),
      @UniqueConstraint(columnNames = "offer_code")
    })
public class ProductDeals {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_deals_id_seq")
  @SequenceGenerator(
      name = "product_deals_id_seq",
      sequenceName = "product_deals_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id", unique = true, nullable = false)
  private long id;

  @Column(name = "product_code")
  private String productCode;

  @Column(name = "offer_code")
  private String offerCode;

  @Column(name = "offer_title")
  private String offerTitle;

  @Column(name = "flat_discount", precision = 131089, scale = 0)
  private Double flatDiscount;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created_date", length = 29)
  private Date createdDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date", length = 29)
  private Date endDate;

  @Column(name = "status")
  private Boolean status;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product productId;

  public ProductDeals() {}

  public ProductDeals(
      final long id,
      final String productCode,
      final String offerCode,
      final String offerTitle,
      final Double flatDiscount,
      final Date createdDate,
      final Date endDate,
      final Boolean status,
      final Product productId) {
    super();
    this.id = id;
    this.productCode = productCode;
    this.offerCode = offerCode;
    this.offerTitle = offerTitle;
    this.flatDiscount = flatDiscount;
    this.createdDate = createdDate;
    this.endDate = endDate;
    this.status = status;
    this.productId = productId;
  }

  public final long getId() {
    return id;
  }

  public final void setId(final long id) {
    this.id = id;
  }

  public final String getProductCode() {
    return productCode;
  }

  public final void setProductCode(final String productCode) {
    this.productCode = productCode;
  }

  public final String getOfferCode() {
    return offerCode;
  }

  public final void setOfferCode(final String offerCode) {
    this.offerCode = offerCode;
  }

  public final String getOfferTitle() {
    return offerTitle;
  }

  public final void setOfferTitle(final String offerTitle) {
    this.offerTitle = offerTitle;
  }

  public final Double getFlatDiscount() {
    return flatDiscount;
  }

  public final void setFlatDiscount(final Double flatDiscount) {
    this.flatDiscount = flatDiscount;
  }

  public final Date getCreatedDate() {
    return createdDate;
  }

  public final void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public final Date getEndDate() {
    return endDate;
  }

  public final void setEndDate(final Date endDate) {
    this.endDate = endDate;
  }

  public final Boolean getStatus() {
    return status;
  }

  public final void setStatus(final Boolean status) {
    this.status = status;
  }

  public final Product getProductId() {
    return productId;
  }

  public final void setProductId(final Product productId) {
    this.productId = productId;
  }
}
