package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "day_close", schema = "form")
@JsonIgnoreProperties(ignoreUnknown = true)
@Proxy(lazy = false)
public class DayClose {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "day_close_id_seq")
  @SequenceGenerator(
      name = "day_close_id_seq",
      sequenceName = "day_close_id_seq",
      allocationSize = 1,
      schema = "form")
  @Column(name = "id")
  private long id;

  @Column(name = "payment_id")
  private long paymentId;

  /** The startTime. */
  @Column(name = "date")
  private Date date;

  @Column(name = "payment_type")
  private String paymentType;

  @Column(name = "payment_amount")
  private BigDecimal paymentAmount;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branchId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation orgId;

  public DayClose() {}

  public DayClose(
      final long id,
      final long paymentId,
      final Date date,
      final String paymentType,
      final BigDecimal paymentAmount,
      final Branch branchId,
      final Organisation orgId) {
    super();
    this.id = id;
    this.paymentId = paymentId;
    this.date = date;
    this.paymentType = paymentType;
    this.paymentAmount = paymentAmount;
    this.branchId = branchId;
    this.orgId = orgId;
  }

  public long getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(final long paymentId) {
    this.paymentId = paymentId;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public String getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(final String paymentType) {
    this.paymentType = paymentType;
  }

  public BigDecimal getPaymentAmount() {
    return paymentAmount;
  }

  public void setPaymentAmount(final BigDecimal paymentAmount) {
    this.paymentAmount = paymentAmount;
  }

  public Branch getBranchId() {
    return branchId;
  }

  public void setBranchId(final Branch branchId) {
    this.branchId = branchId;
  }

  public Organisation getOrgId() {
    return orgId;
  }

  public void setOrgId(final Organisation orgId) {
    this.orgId = orgId;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "DayClose [id="
        + id
        + ", paymentId="
        + paymentId
        + ", date="
        + date
        + ", paymentType="
        + paymentType
        + ", paymentAmount="
        + paymentAmount
        + ", branchId="
        + branchId
        + ", orgId="
        + orgId
        + "]";
  }
}
