package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "billing_payments", schema = "product")
@Proxy(lazy = false)
public class BillingPayments implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "billing_secondary_payments_id_seq")
  @SequenceGenerator(
      name = "billing_secondary_payments_id_seq",
      sequenceName = "billing_secondary_payments_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "billing_id", insertable = true, updatable = true)
  private Billing billing;

  @Column(name = "payment_mode")
  private String paymentMode;

  @Column(name = "amount")
  private long amount;

  @Column(name = "date")
  private Date date;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  public BillingPayments() {}

  public BillingPayments(
      long id,
      Billing billing,
      String paymentMode,
      long amount,
      Branch branch,
      Organisation organisation,
      Date date) {
    super();
    this.id = id;
    this.billing = billing;
    this.paymentMode = paymentMode;
    this.amount = amount;
    this.branch = branch;
    this.organisation = organisation;
    this.date = date;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Billing getBilling() {
    return this.billing;
  }

  public void setBilling(Billing billing) {
    this.billing = billing;
  }

  public String getPaymentMode() {
    return this.paymentMode;
  }

  public void setPaymentMode(String paymentMode) {
    this.paymentMode = paymentMode;
  }

  public long getAmount() {
    return this.amount;
  }

  public void setAmount(long amount) {
    this.amount = amount;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
  public String toString() {
    return "BillingPayments [id="
        + id
        + ", billing="
        + billing
        + ", paymentMode="
        + paymentMode
        + ", amount="
        + amount
        + ", date="
        + date
        + ", branch="
        + branch
        + ", organisation="
        + organisation
        + "]";
  }
}
