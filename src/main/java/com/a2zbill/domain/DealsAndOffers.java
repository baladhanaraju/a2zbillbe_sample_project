package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "deals_offers", schema = "deals")
public class DealsAndOffers {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "deals_offers_id_seq")
  @SequenceGenerator(
      name = "deals_offers_id_seq",
      sequenceName = "deals_offersid_seq",
      allocationSize = 1)
  @Column(name = "id", unique = true, nullable = false)
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product productId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "deals_id", insertable = true, updatable = true)
  private ProductDeals dealsId;

  public DealsAndOffers() {}

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Product getProductId() {
    return this.productId;
  }

  public void setProductId(final Product productId) {
    this.productId = productId;
  }

  public ProductDeals getDealsId() {
    return this.dealsId;
  }

  public void setDealsId(final ProductDeals dealsId) {
    this.dealsId = dealsId;
  }

  public DealsAndOffers(final long id, final Product productId, final ProductDeals dealsId) {
    super();
    this.id = id;
    this.productId = productId;
    this.dealsId = dealsId;
  }
}
