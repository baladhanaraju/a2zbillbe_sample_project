package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Roles;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "role_service", schema = "product")
public class Roleservice implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "roll_service")
  @SequenceGenerator(name = "id_seq", sequenceName = "", allocationSize = 1, schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "role_id", unique = true, nullable = true)
  private Roles role;

  @Column(name = "default_service")
  private String defaultService;

  public Roleservice() {}

  public Roleservice(long id, Roles role, String defaultService) {
    this.id = id;
    this.role = role;
    this.defaultService = defaultService;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Roles getRole() {
    return role;
  }

  public void setRole(final Roles role) {
    this.role = role;
  }

  public String getDefaultService() {
    return defaultService;
  }

  public void setDefaultService(final String defaultService) {
    this.defaultService = defaultService;
  }
}
