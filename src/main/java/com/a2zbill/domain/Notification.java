/*package com.a2zbill.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name="notification_info", schema="notification")
@SuppressWarnings("serial")
public class Notification implements Serializable {

public Notification()
{

}

public Notification(long id, long orgId, long branchId, long empId, String notificationToken) {
super();
this.id = id;
this.orgId = orgId;
this.branchId = branchId;
this.empId = empId;
this.notificationToken = notificationToken;
}

@Id
@GeneratedValue(strategy=GenerationType.AUTO,generator="notification_id")
@SequenceGenerator(name="notification_id",sequenceName="notification_id",allocationSize=1,schema="notification")
@Column(name="id")
public long getId() {
return this.id;
}

public void setId(long id) {
this.id = id;
}

private long id;
private long orgId;
@Column(name = "org_id")
public long getOrgId() {
return this.orgId;
}
public void setOrgId(long orgId) {
this.orgId = orgId;
}
@Column(name = "branch_id")
public long getBranchId() {
return this.branchId;
}

public void setBranchId(long branchId) {
this.branchId = branchId;
}

@Column(name = "emp_id")
public long getEmpId() {
return this.empId;
}

public void setEmpId(long empId) {
this.empId = empId;
}
@Column(name = "notification_token")
public String getNotificationToken() {
return this.notificationToken;
}

public void setNotificationToken(String notificationToken) {
this.notificationToken = notificationToken;
}

private long branchId;
private long empId;
private String notificationToken;

}
*/
