package com.a2zbill.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ItemDetail implements Serializable {

  private Double taxval;

  private long rt;

  private Double camt;

  private Double samt;

  private Double csamt;

  public ItemDetail() {}

  public ItemDetail(Double taxval, long rt, Double camt, Double samt, Double csamt) {
    super();
    this.taxval = taxval;
    this.rt = rt;
    this.camt = camt;
    this.samt = samt;
    this.csamt = csamt;
  }

  public Double getTaxval() {
    return this.taxval;
  }

  public void setTaxval(Double taxval) {
    this.taxval = taxval;
  }

  public long getRt() {
    return this.rt;
  }

  public void setRt(long rt) {
    this.rt = rt;
  }

  public Double getCamt() {
    return this.camt;
  }

  public void setCamt(Double camt) {
    this.camt = camt;
  }

  public Double getSamt() {
    return this.samt;
  }

  public void setSamt(Double samt) {
    this.samt = samt;
  }

  public Double getCsamt() {
    return this.csamt;
  }

  public void setCsamt(Double csamt) {
    this.csamt = csamt;
  }
}
