package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "product_recipe", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class ProductRecipe implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_recipe_id_seq")
  @SequenceGenerator(
      name = "product_recipe_id_seq",
      sequenceName = "product_recipe_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "created_date")
  private Date cteatedDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "recipe_name")
  private String recipeName;

  @Column(name = "quantity")
  private long quantity;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "measurement_id", insertable = true, updatable = true)
  private QuantityMeasurements quantityMeasurements;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @Column(name = "status")
  private boolean status;

  public ProductRecipe() {}

  public ProductRecipe(
      long id,
      Date cteatedDate,
      Date modifiedDate,
      String recipeName,
      long quantity,
      QuantityMeasurements quantityMeasurements,
      Organisation organisation,
      Branch branch,
      boolean status) {
    super();
    this.id = id;
    this.cteatedDate = cteatedDate;
    this.modifiedDate = modifiedDate;
    this.recipeName = recipeName;
    this.quantity = quantity;
    this.quantityMeasurements = quantityMeasurements;
    this.organisation = organisation;
    this.branch = branch;
    this.status = status;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Date getCteatedDate() {
    return this.cteatedDate;
  }

  public void setCteatedDate(Date cteatedDate) {
    this.cteatedDate = cteatedDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getRecipeName() {
    return recipeName;
  }

  public void setRecipeName(String recipeName) {
    this.recipeName = recipeName;
  }

  public QuantityMeasurements getQuantityMeasurements() {
    return quantityMeasurements;
  }

  public void setQuantityMeasurements(QuantityMeasurements quantityMeasurements) {
    this.quantityMeasurements = quantityMeasurements;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public long getQuantity() {
    return quantity;
  }

  public void setQuantity(long quantity) {
    this.quantity = quantity;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }
}
