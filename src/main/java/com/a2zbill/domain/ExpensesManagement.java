package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "expenses_management", schema = "product")
public class ExpensesManagement implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "expenses_management_id_seq")
  @SequenceGenerator(
      name = "expenses_management_id_seq",
      sequenceName = "expenses_management_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "invoice_id")
  private String invoiceId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "vendor_id", unique = true, nullable = true)
  private Vendor vendor;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "amount")
  private BigDecimal amount;

  @Column(name = "date")
  private Date date;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  public ExpensesManagement() {}

  public ExpensesManagement(
      final long id,
      final String name,
      final String invoiceId,
      final Vendor vendor,
      final Date createdDate,
      final Date modifiedDate,
      final BigDecimal amount,
      final Date date,
      final Branch branch,
      final Organisation organisation) {
    super();
    this.id = id;
    this.name = name;
    this.invoiceId = invoiceId;
    this.vendor = vendor;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.amount = amount;
    this.date = date;
    this.branch = branch;
    this.organisation = organisation;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(final BigDecimal amount) {
    this.amount = amount;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public String getInvoiceId() {
    return invoiceId;
  }

  public void setInvoiceId(final String invoiceId) {
    this.invoiceId = invoiceId;
  }

  public Vendor getVendor() {
    return vendor;
  }

  public void setVendor(final Vendor vendor) {
    this.vendor = vendor;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }
}
