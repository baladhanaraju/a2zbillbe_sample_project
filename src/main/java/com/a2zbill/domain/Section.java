package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "section", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class Section {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "section_id_seq")
  @SequenceGenerator(
      name = "section_id_seq",
      sequenceName = "section_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "section_name")
  private String sectionName;

  @Column(name = "display_name")
  private String displayName;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branch;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "status")
  private boolean status;

  public Section() {}

  public Section(
      long id,
      String sectionName,
      String displayName,
      Organisation organisation,
      Branch branch,
      Date createdDate,
      Date modifiedDate,
      boolean status) {
    super();
    this.id = id;
    this.sectionName = sectionName;
    this.displayName = displayName;
    this.organisation = organisation;
    this.branch = branch;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.status = status;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSectionName() {
    return this.sectionName;
  }

  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }

  public String getDisplayName() {
    return this.displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Section [id="
        + id
        + ", sectionName="
        + sectionName
        + ", displayName="
        + displayName
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", status="
        + status
        + "]";
  }
}
