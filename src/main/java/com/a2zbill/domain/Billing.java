package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "billings", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class Billing implements Serializable {

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "billing_id_seq")
  @SequenceGenerator(
      name = "billing_id_seq",
      sequenceName = "billing_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  /** The cart details. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "cart_id", insertable = true, updatable = true)
  private Cart cartId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "order_id", insertable = true, updatable = true)
  private Order order;

  /** The counterDetails. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", insertable = true, updatable = true)
  private Customer customer;

  /** The totalAmount. */
  @Column(name = "total_amount")
  private BigDecimal totalAmount;

  @Column(name = "net_amount")
  private BigDecimal netAmount;

  /** The taxableAmount. */
  @Column(name = "gst_amount")
  private BigDecimal taxableAmount;

  /** The Date. */
  @Column(name = "date")
  private Date billDate;

  @Column(name = "cgstAmount")
  private BigDecimal cgstAmount;

  @Column(name = "sgst_amount")
  private BigDecimal sgstAmount;

  /** The employeeDetails. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "emp_id", insertable = true, updatable = true)
  private Employee employeeDetails;

  /** The counterDetails. */
  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "counter_id", insertable = true, updatable = true)
  private Counter counterDetails;

  @Column(name = "discount_percentage")
  private BigDecimal discountPercentage;

  @Column(name = "discount_amount")
  private BigDecimal discountAmount;

  @Column(name = "bill_number_count")
  private String billNumberCount;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @Column(name = "rewards")
  private long rewards;

  @Column(name = "time")
  private String time;

  @Column(name = "billing_number")
  private String billingNumber;

  @Column(name = "offer_description")
  private String offerDescription;

  @Column(name = "service_charge")
  private long serviceCharge;

  @Column(name = "print_count")
  private long printCount;

  @Column(name = "redeem_amount")
  private BigDecimal redeemAmount;

  public Billing() {}

  public Billing(
      long id,
      Cart cartId,
      Order order,
      Customer customer,
      BigDecimal totalAmount,
      BigDecimal netAmount,
      BigDecimal taxableAmount,
      Date billDate,
      BigDecimal cgstAmount,
      BigDecimal sgstAmount,
      Employee employeeDetails,
      Counter counterDetails,
      BigDecimal discountPercentage,
      BigDecimal discountAmount,
      String billNumberCount,
      Organisation organisation,
      Branch branch,
      long rewards,
      String time,
      String billingNumber,
      String offerDescription,
      long serviceCharge,
      long printCount,
      BigDecimal redeemAmount) {
    super();
    this.id = id;
    this.cartId = cartId;
    this.order = order;
    this.customer = customer;
    this.totalAmount = totalAmount;
    this.netAmount = netAmount;
    this.taxableAmount = taxableAmount;
    this.billDate = billDate;
    this.cgstAmount = cgstAmount;
    this.sgstAmount = sgstAmount;
    this.employeeDetails = employeeDetails;
    this.counterDetails = counterDetails;
    this.discountPercentage = discountPercentage;
    this.discountAmount = discountAmount;
    this.billNumberCount = billNumberCount;
    this.organisation = organisation;
    this.branch = branch;
    this.rewards = rewards;
    this.time = time;
    this.billingNumber = billingNumber;
    this.offerDescription = offerDescription;
    this.serviceCharge = serviceCharge;
    this.printCount = printCount;
    this.redeemAmount = redeemAmount;
  }

  public long getId() {
    return this.id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(long id) {
    this.id = id;
  }

  /**
   * Gets the cart details.
   *
   * @return the cartId
   */
  public Cart getCartId() {
    return this.cartId;
  }

  /**
   * Sets the cart details.
   *
   * @param cartId the new cartId
   */
  public void setCartId(Cart cartId) {
    this.cartId = cartId;
  }

  /**
   * Gets the customerId.
   *
   * @return the customerId
   */
  public Customer getCustomer() {
    return this.customer;
  }

  /**
   * Sets the customerId.
   *
   * @param customer the new customer
   */
  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  /**
   * Gets the totalAmount.
   *
   * @return the totalAmount
   */
  public BigDecimal getTotalAmount() {
    return this.totalAmount;
  }

  /**
   * Sets the totalAmount.
   *
   * @param totalAmount the new totalAmount
   */
  public void setTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  /**
   * Gets the taxableAmount.
   *
   * @return the taxableAmount
   */
  public BigDecimal getTaxableAmount() {
    return this.taxableAmount;
  }

  /**
   * Sets the taxableAmount.
   *
   * @param taxableAmount the new taxableAmount
   */
  public void setTaxableAmount(BigDecimal taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  /**
   * Gets the billDate.
   *
   * @return the billDate
   */
  public Date getBillDate() {
    return this.billDate;
  }

  /**
   * Sets the billDate.
   *
   * @param billDate the new billDate
   */
  public void setBillDate(Date billDate) {
    this.billDate = billDate;
  }

  /**
   * Gets the employeeDetails.
   *
   * @return the employeeDetails
   */
  public Employee getEmployeeDetails() {
    return this.employeeDetails;
  }

  /**
   * Sets the employeeDetails.
   *
   * @param employeeDetails the new employeeDetails
   */
  public void setEmployeeDetails(Employee employeeDetails) {
    this.employeeDetails = employeeDetails;
  }

  /**
   * Gets the rewardDetails.
   *
   * @return the rewardDetails
   */
  public Counter getCounterDetails() {
    return this.counterDetails;
  }

  public BigDecimal getCgstAmount() {
    return this.cgstAmount;
  }

  public void setCgstAmount(BigDecimal cgstAmount) {
    this.cgstAmount = cgstAmount;
  }

  public BigDecimal getSgstAmount() {
    return this.sgstAmount;
  }

  public void setSgstAmount(BigDecimal sgstAmount) {
    this.sgstAmount = sgstAmount;
  }

  public void setCounterDetails(Counter counterDetails) {
    this.counterDetails = counterDetails;
  }

  public BigDecimal getDiscountPercentage() {
    return this.discountPercentage;
  }

  public void setDiscountPercentage(BigDecimal discountPercentage) {
    this.discountPercentage = discountPercentage;
  }

  public BigDecimal getDiscountAmount() {
    return this.discountAmount;
  }

  public void setDiscountAmount(BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public String getBillNumberCount() {
    return this.billNumberCount;
  }

  public void setBillNumberCount(String billNumberCount) {
    this.billNumberCount = billNumberCount;
  }

  public long getRewards() {
    return this.rewards;
  }

  public void setRewards(long rewards) {
    this.rewards = rewards;
  }

  public String getTime() {
    return this.time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getBillingNumber() {
    return this.billingNumber;
  }

  public void setBillingNumber(String billingNumber) {
    this.billingNumber = billingNumber;
  }

  public String getOfferDescription() {
    return offerDescription;
  }

  public void setOfferDescription(String offerDescription) {
    this.offerDescription = offerDescription;
  }

  public long getServiceCharge() {
    return serviceCharge;
  }

  public void setServiceCharge(long serviceCharge) {
    this.serviceCharge = serviceCharge;
  }

  public BigDecimal getNetAmount() {
    return netAmount;
  }

  public void setNetAmount(BigDecimal netAmount) {
    this.netAmount = netAmount;
  }

  public long getPrintCount() {
    return printCount;
  }

  public void setPrintCount(long printCount) {
    this.printCount = printCount;
  }

  public BigDecimal getRedeemAmount() {
    return redeemAmount;
  }

  public void setRedeemAmount(BigDecimal redeemAmount) {
    this.redeemAmount = redeemAmount;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }
}
