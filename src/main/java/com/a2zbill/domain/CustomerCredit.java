package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "customer_credit", schema = "credits")
public class CustomerCredit implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "customer_credit_seq")
  @SequenceGenerator(
      name = "customer_credit_seq",
      sequenceName = "customer_credit_seq",
      allocationSize = 1,
      schema = "credits")
  @Column(name = "id", nullable = false)
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", insertable = true, updatable = true)
  private Customer customerId;

  @Column(name = "credit_limit")
  private BigDecimal creditLimit;

  @Column(name = "credit_amount")
  private BigDecimal creditAmount;

  @Column(name = "payable_amount")
  private BigDecimal payableAmount;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisationId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branchId;

  @JsonIgnore
  @Column(name = "created_ts")
  private Date createdTs;

  @JsonIgnore
  @Column(name = "modified_ts")
  private Date modifiedTs;

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Customer getCustomerId() {
    return customerId;
  }

  public void setCustomerId(final Customer customerId) {
    this.customerId = customerId;
  }

  public BigDecimal getCreditLimit() {
    return creditLimit;
  }

  public void setCreditLimit(final BigDecimal creditLimit) {
    this.creditLimit = creditLimit;
  }

  public BigDecimal getCreditAmount() {
    return creditAmount;
  }

  public void setCreditAmount(final BigDecimal creditAmount) {
    this.creditAmount = creditAmount;
  }

  public BigDecimal getPayableAmount() {
    return payableAmount;
  }

  public void setPayableAmount(final BigDecimal payableAmount) {
    this.payableAmount = payableAmount;
  }

  public Organisation getOrganisationId() {
    return organisationId;
  }

  public void setOrganisationId(final Organisation organisationId) {
    this.organisationId = organisationId;
  }

  public Branch getBranchId() {
    return branchId;
  }

  public void setBranchId(final Branch branchId) {
    this.branchId = branchId;
  }

  public Date getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(final Date createdTs) {
    this.createdTs = createdTs;
  }

  public Date getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(final Date modifiedTs) {
    this.modifiedTs = modifiedTs;
  }
}
