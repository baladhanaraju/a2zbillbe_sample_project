package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cancel_history", schema = "kot")
public class CancelHistory implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "cancel_history_id")
  @SequenceGenerator(
      name = "cancel_history_id",
      sequenceName = "cancel_history_id",
      allocationSize = 1,
      schema = "kot")
  @Column(name = "id")
  private long id;

  @Column(name = "product_name")
  private String productName;

  @Column(name = "counter_number")
  private long counterNumber;

  @Column(name = "date")
  private Date date;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "emp_id", unique = true, nullable = true)
  private Employee employee;

  public CancelHistory() {}

  public CancelHistory(
      final long id,
      final String productName,
      final long counterNumber,
      final Date date,
      final Organisation organisation,
      final Branch branch,
      final Employee employee) {
    super();
    this.id = id;
    this.productName = productName;
    this.counterNumber = counterNumber;
    this.date = date;
    this.organisation = organisation;
    this.branch = branch;
    this.employee = employee;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getProductName() {
    return this.productName;
  }

  public void setProductName(final String productName) {
    this.productName = productName;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public Employee getEmployee() {
    return this.employee;
  }

  public void setEmployee(final Employee employee) {
    this.employee = employee;
  }

  public long getCounterNumber() {
    return this.counterNumber;
  }

  public void setCounterNumber(final long counterNumber) {
    this.counterNumber = counterNumber;
  }

  @Override
  public String toString() {
    return "CancelHistory [id="
        + id
        + ", productName="
        + productName
        + ", counterNumber="
        + counterNumber
        + ", date="
        + date
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", employee="
        + employee
        + "]";
  }
}
