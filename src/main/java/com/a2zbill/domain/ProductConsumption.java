package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "product_consumption", schema = "product")
public class ProductConsumption implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_consumption_id_seq")
  @SequenceGenerator(
      name = "product_consumption_id_seq",
      sequenceName = "product_consumption_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "amount")
  private BigDecimal amount;

  @Column(name = "created_date")
  private Date cteatedDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "quantity_used")
  private BigDecimal quantityUsed;

  @Column(name = "date")
  private Date date;

  @Column(name = "wastage_flag")
  private boolean wastageFlag;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @Column(name = "menu_product_id")
  private long menuProductId;

  public ProductConsumption() {}

  public ProductConsumption(
      long id,
      BigDecimal amount,
      Date cteatedDate,
      Date modifiedDate,
      BigDecimal quantityUsed,
      Date date,
      boolean wastageFlag,
      Product product,
      Branch branch,
      Organisation organisation,
      long menuProductId) {
    super();
    this.id = id;
    this.amount = amount;
    this.cteatedDate = cteatedDate;
    this.modifiedDate = modifiedDate;
    this.quantityUsed = quantityUsed;
    this.date = date;
    this.wastageFlag = wastageFlag;
    this.product = product;
    this.branch = branch;
    this.organisation = organisation;
    this.menuProductId = menuProductId;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Date getCteatedDate() {
    return cteatedDate;
  }

  public void setCteatedDate(Date cteatedDate) {
    this.cteatedDate = cteatedDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public BigDecimal getQuantityUsed() {
    return quantityUsed;
  }

  public void setQuantityUsed(BigDecimal quantityUsed) {
    this.quantityUsed = quantityUsed;
  }

  public long getMenuProductId() {
    return menuProductId;
  }

  public void setMenuProductId(long menuProductId) {
    this.menuProductId = menuProductId;
  }

  public boolean isWastageFlag() {
    return wastageFlag;
  }

  public void setWastageFlag(boolean wastageFlag) {
    this.wastageFlag = wastageFlag;
  }
}
