package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "billing_details", schema = "product")
public class BillingDetail implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "billingdetails_id_seq")
  @SequenceGenerator(
      name = "billingdetails_id_seq",
      sequenceName = "billingdetails_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "qnty_stock")
  private long quantityStock;

  @Column(name = "qnty_use")
  private long quantityUse;

  @Column(name = "qnty_remaining")
  private long quantityRemaing;

  @Column(name = "create_date")
  private Date createDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "ordered_date")
  private Date orderdDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "inv_id", unique = true, nullable = true)
  private InventoryDetail inventoryDetails;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "vendor_id", unique = true, nullable = true)
  private Vendor vendor;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "billing_id", unique = true, nullable = true)
  private Billing billingDetails;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  /*
   *@Column(name="casepack") private long casepack;
   *
   *@Column(name="casepack_pieces") private long casePackPieces;
   */
  public BillingDetail() {}

  public BillingDetail(
      long id,
      long quantityStock,
      long quantityUse,
      long quantityRemaing,
      Date createDate,
      Date modifiedDate,
      Date orderdDate,
      InventoryDetail inventoryDetails,
      Vendor vendor,
      Branch branch,
      Billing billingDetails,
      Organisation organisation) {
    super();
    this.id = id;
    this.quantityStock = quantityStock;
    this.quantityUse = quantityUse;
    this.quantityRemaing = quantityRemaing;
    this.createDate = createDate;
    this.modifiedDate = modifiedDate;
    this.orderdDate = orderdDate;
    this.inventoryDetails = inventoryDetails;
    this.vendor = vendor;
    this.branch = branch;
    this.billingDetails = billingDetails;
    this.organisation = organisation;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getQuantityStock() {
    return this.quantityStock;
  }

  public void setQuantityStock(long quantityStock) {
    this.quantityStock = quantityStock;
  }

  public long getQuantityUse() {
    return this.quantityUse;
  }

  public void setQuantityUse(long quantityUse) {
    this.quantityUse = quantityUse;
  }

  public long getQuantityRemaing() {
    return this.quantityRemaing;
  }

  public void setQuantityRemaing(long quantityRemaing) {
    this.quantityRemaing = quantityRemaing;
  }

  public Date getCreateDate() {
    return this.createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Date getOrderdDate() {
    return this.orderdDate;
  }

  public void setOrderdDate(Date orderdDate) {
    this.orderdDate = orderdDate;
  }

  public InventoryDetail getInventoryDetails() {
    return this.inventoryDetails;
  }

  public void setInventoryDetails(InventoryDetail inventoryDetails) {
    this.inventoryDetails = inventoryDetails;
  }

  public Vendor getVendor() {
    return this.vendor;
  }

  public void setVendor(Vendor vendor) {
    this.vendor = vendor;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Billing getBillingDetails() {
    return this.billingDetails;
  }

  public void setBillingDetails(Billing billingDetails) {
    this.billingDetails = billingDetails;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  @Override
  public String toString() {
    return "BillingDetail [id="
        + id
        + ", quantityStock="
        + quantityStock
        + ", quantityUse="
        + quantityUse
        + ", quantityRemaing="
        + quantityRemaing
        + ", createDate="
        + createDate
        + ", modifiedDate="
        + modifiedDate
        + ", orderdDate="
        + orderdDate
        + ", inventoryDetails="
        + inventoryDetails
        + ", vendor="
        + vendor
        + ", branch="
        + branch
        + ", billingDetails="
        + billingDetails
        + "]";
  }
}
