package com.a2zbill.domain;

import java.io.Serializable;

public class B2C implements Serializable {

  private static final long serialVersionUID = 1L;

  private String ctin;

  private Invoice inv;

  public B2C() {}

  public B2C(String ctin, Invoice inv) {
    super();
    this.ctin = ctin;
    this.inv = inv;
  }

  public String getCtin() {
    return this.ctin;
  }

  public void setCtin(String ctin) {
    this.ctin = ctin;
  }

  public Invoice getInv() {
    return this.inv;
  }

  public void setInv(Invoice inv) {
    this.inv = inv;
  }
}
