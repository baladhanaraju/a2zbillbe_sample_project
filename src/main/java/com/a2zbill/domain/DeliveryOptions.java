package com.a2zbill.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "delivery_options", schema = "delivery")
public class DeliveryOptions implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "delivery_options_id_seq")
  @SequenceGenerator(
      name = "delivery_options_id_seq",
      sequenceName = "delivery_options_id_seq",
      allocationSize = 1,
      schema = "delivery")
  @Column(name = "id")
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "primary")
  private boolean primary;

  public DeliveryOptions() {}

  public DeliveryOptions(
      final long id,
      final String name,
      final Date createdDate,
      final Date modifiedDate,
      final boolean primary) {
    super();
    this.id = id;
    this.name = name;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.primary = primary;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public boolean isPrimary() {
    return primary;
  }

  public void setPrimary(final boolean primary) {
    this.primary = primary;
  }
}
