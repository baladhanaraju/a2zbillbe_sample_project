package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "reverse_order", schema = "product")
public class ReverseOrder {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "reverse_order_id_seq")
  @SequenceGenerator(
      name = "reverse_order_id_seq",
      sequenceName = "reverse_order_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "reorder_id", insertable = true, updatable = true)
  private ReOrder reorderId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "invoice_id", insertable = true, updatable = true)
  private InvoiceDetails invoiceId;

  @Column(name = "reason")
  private String reason;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "reverse_invoice_id", insertable = true, updatable = true)
  private InvoiceDetails reverseInvoiceId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branch;

  public ReverseOrder() {}

  public ReverseOrder(
      long id,
      ReOrder reorderId,
      InvoiceDetails invoiceId,
      String reason,
      InvoiceDetails reverseInvoiceId,
      Organisation organisation,
      Branch branch) {
    super();
    this.id = id;
    this.reorderId = reorderId;
    this.invoiceId = invoiceId;
    this.reason = reason;
    this.reverseInvoiceId = reverseInvoiceId;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public ReOrder getReorderId() {
    return this.reorderId;
  }

  public void setReorderId(ReOrder reorderId) {
    this.reorderId = reorderId;
  }

  public InvoiceDetails getInvoiceId() {
    return this.invoiceId;
  }

  public void setInvoiceId(InvoiceDetails invoiceId) {
    this.invoiceId = invoiceId;
  }

  public String getReason() {
    return this.reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public InvoiceDetails getReverseInvoiceId() {
    return this.reverseInvoiceId;
  }

  public void setReverseInvoiceId(InvoiceDetails reverseInvoiceId) {
    this.reverseInvoiceId = reverseInvoiceId;
  }
}
