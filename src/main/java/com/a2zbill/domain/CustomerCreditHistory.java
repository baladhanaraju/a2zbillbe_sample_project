package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "customer_credit_history", schema = "credits")
public class CustomerCreditHistory implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "customer_credit_history_seq")
  @SequenceGenerator(
      name = "customer_credit_history_seq",
      sequenceName = "customer_credit_history_seq",
      allocationSize = 1,
      schema = "credits")
  @Column(name = "id", nullable = false)
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_credit_id", insertable = true, updatable = true)
  private CustomerCredit customerCreditId;

  @Column(name = "spend_amount")
  private BigDecimal spendAmount;

  @Column(name = "paid_amount")
  private BigDecimal paidAmount;

  @Column(name = "payment_mode")
  private String paymentMode;

  @JsonIgnore
  @Column(name = "created_ts")
  private Date createdTs;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisationId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branchId;

  @JsonIgnore
  @Column(name = "modified_ts")
  private Date modifiedTs;

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public CustomerCredit getCustomerCreditId() {
    return customerCreditId;
  }

  public void setCustomerCreditId(final CustomerCredit customerCreditId) {
    this.customerCreditId = customerCreditId;
  }

  public BigDecimal getSpendAmount() {
    return spendAmount;
  }

  public void setSpendAmount(final BigDecimal spendAmount) {
    this.spendAmount = spendAmount;
  }

  public BigDecimal getPaidAmount() {
    return paidAmount;
  }

  public void setPaidAmount(final BigDecimal paidAmount) {
    this.paidAmount = paidAmount;
  }

  public String getPaymentMode() {
    return paymentMode;
  }

  public void setPaymentMode(final String paymentMode) {
    this.paymentMode = paymentMode;
  }

  public Date getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(final Date createdTs) {
    this.createdTs = createdTs;
  }

  public Date getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(final Date modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Organisation getOrganisationId() {
    return organisationId;
  }

  public void setOrganisationId(Organisation organisationId) {
    this.organisationId = organisationId;
  }

  public Branch getBranchId() {
    return branchId;
  }

  public void setBranchId(Branch branchId) {
    this.branchId = branchId;
  }

  @Override
  public String toString() {
    return "CustomerCreditHistory [id="
        + id
        + ", customerCreditId="
        + customerCreditId
        + ", spendAmount="
        + spendAmount
        + ", paidAmount="
        + paidAmount
        + ", paymentMode="
        + paymentMode
        + ", createdTs="
        + createdTs
        + ", organisationId="
        + organisationId
        + ", branchId="
        + branchId
        + ", modifiedTs="
        + modifiedTs
        + "]";
  }
}
