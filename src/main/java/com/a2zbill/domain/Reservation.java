package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "reservation", schema = "reservation")
public class Reservation implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "reservation_id_seq")
  @SequenceGenerator(
      name = "reservation_id_seq",
      sequenceName = "reservation_id_seq",
      allocationSize = 1,
      schema = "reservation")
  @Column(name = "id")
  private long id;

  @Column(name = "start_time")
  private String startTime;

  @Temporal(TemporalType.DATE)
  @Column(name = "reserve_date")
  private Date reserveDate;

  @Column(name = "kids_Count")
  private long kidscount;

  @Column(name = "adult_Count")
  private long adultcount;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "reserve_table", insertable = true, updatable = true)
  private Counter reserveTable;

  @Column(name = "allocated_table")
  private int allocatedTable;

  @Column(name = "status")
  private String status;

  @Temporal(TemporalType.DATE)
  @Column(name = "created_date")
  private java.util.Date createdDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "modified_date")
  private java.util.Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", unique = true, nullable = true)
  private Customer customer;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branch;

  @Column(name = "note")
  private String note;

  @Column(name = "reservation_type")
  private String reservationType;

  public Reservation() {}

  public Reservation(
      long id,
      String startTime,
      Date reserveDate,
      long kidscount,
      long adultcount,
      Counter reserveTable,
      int allocatedTable,
      String status,
      Date createdDate,
      Date modifiedDate,
      Customer customer,
      Organisation organisation,
      Branch branch,
      String note,
      String reservationType) {
    super();
    this.id = id;
    this.startTime = startTime;
    this.reserveDate = reserveDate;
    this.kidscount = kidscount;
    this.adultcount = adultcount;
    this.reserveTable = reserveTable;
    this.allocatedTable = allocatedTable;
    this.status = status;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.customer = customer;
    this.organisation = organisation;
    this.branch = branch;
    this.note = note;
    this.reservationType = reservationType;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getStartTime() {
    return this.startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public Date getReserveDate() {
    return this.reserveDate;
  }

  public void setReserveDate(Date reserveDate) {
    this.reserveDate = reserveDate;
  }

  public long getKidscount() {
    return kidscount;
  }

  public void setKidscount(long kidscount) {
    this.kidscount = kidscount;
  }

  public long getAdultcount() {
    return adultcount;
  }

  public void setAdultcount(long adultcount) {
    this.adultcount = adultcount;
  }

  public Counter getReserveTable() {
    return reserveTable;
  }

  public void setReserveTable(Counter reserveTable) {
    this.reserveTable = reserveTable;
  }

  public int getAllocatedTable() {
    return this.allocatedTable;
  }

  public void setAllocatedTable(int allocatedTable) {
    this.allocatedTable = allocatedTable;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Customer getCustomer() {
    return this.customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public String getNote() {
    return this.note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getReservationType() {
    return this.reservationType;
  }

  public void setReservationType(String reservationType) {
    this.reservationType = reservationType;
  }
}
