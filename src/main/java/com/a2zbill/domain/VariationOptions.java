package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "variation_options", schema = "product")
public class VariationOptions {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "variation_options_id_seq")
  @SequenceGenerator(
      name = "variation_options_id_seq",
      sequenceName = "variation_options_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "option_name")
  private String optionName;

  @Column(name = "option_display_name")
  private String optionDisplayName;

  @Column(name = "weight")
  private BigDecimal weight;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "quantity_measurement_id", unique = true, nullable = true)
  private QuantityMeasurements quantityMeasurement;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "variation_type_id", unique = true, nullable = true)
  private VariationTypes variationTypes;

  public VariationOptions() {}

  public VariationOptions(
      final long id,
      final String optionName,
      final String optionDisplayName,
      final BigDecimal weight,
      final QuantityMeasurements quantityMeasurement,
      final VariationTypes variationTypes) {
    super();
    this.id = id;
    this.optionName = optionName;
    this.optionDisplayName = optionDisplayName;
    this.weight = weight;
    this.quantityMeasurement = quantityMeasurement;
    this.variationTypes = variationTypes;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getOptionName() {
    return optionName;
  }

  public void setOptionName(final String optionName) {
    this.optionName = optionName;
  }

  public String getOptionDisplayName() {
    return optionDisplayName;
  }

  public void setOptionDisplayName(final String optionDisplayName) {
    this.optionDisplayName = optionDisplayName;
  }

  public QuantityMeasurements getQuantityMeasurement() {
    return quantityMeasurement;
  }

  public void setQuantityMeasurement(final QuantityMeasurements quantityMeasurement) {
    this.quantityMeasurement = quantityMeasurement;
  }

  public VariationTypes getVariationTypes() {
    return variationTypes;
  }

  public void setVariationTypes(VariationTypes variationTypes) {
    this.variationTypes = variationTypes;
  }

  public BigDecimal getWeight() {
    return weight;
  }

  public void setWeight(final BigDecimal weight) {
    this.weight = weight;
  }
}
