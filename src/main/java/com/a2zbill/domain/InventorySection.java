package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "inventory_section", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class InventorySection implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inventorysection_id_seq")
  @SequenceGenerator(
      name = "inventorysection_id_seq",
      sequenceName = "inventorysection_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "section_name")
  private String sectionName;

  @Column(name = "consumed_flag")
  private boolean consumedFlag;

  @Column(name = "kot_flag")
  private boolean kotFlag;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public InventorySection() {}

  public InventorySection(
      long id,
      String sectionName,
      boolean consumedFlag,
      boolean kotFlag,
      Organisation organisation,
      Branch branch) {
    super();
    this.id = id;
    this.sectionName = sectionName;
    this.consumedFlag = consumedFlag;
    this.kotFlag = kotFlag;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSectionName() {
    return this.sectionName;
  }

  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public boolean isConsumedFlag() {
    return consumedFlag;
  }

  public void setConsumedFlag(boolean consumedFlag) {
    this.consumedFlag = consumedFlag;
  }

  public boolean isKotFlag() {
    return kotFlag;
  }

  public void setKotFlag(boolean kotFlag) {
    this.kotFlag = kotFlag;
  }

  @Override
  public String toString() {
    return "InventorySection [id="
        + id
        + ", sectionName="
        + sectionName
        + ", consumedFlag="
        + consumedFlag
        + ", kotFlag="
        + kotFlag
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + "]";
  }
}
