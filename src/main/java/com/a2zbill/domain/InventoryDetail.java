package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "inventory_details", schema = "product")
public class InventoryDetail implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inventorydetails_id_seq")
  @SequenceGenerator(
      name = "inventorydetails_id_seq",
      sequenceName = "inventorydetails_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long inventoryId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product product;

  @Column(name = "qnty")
  private BigDecimal quantity;

  @Column(name = "date")
  private Date date;

  @Column(name = "buying_price")
  private BigDecimal buyingPrice;

  @Column(name = "selling_price")
  private BigDecimal sellingPrice;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch BranchDetail;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "vendor_id", unique = true, nullable = true)
  private Vendor vendor;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @Column(name = "reorder_qnty")
  private int reorderQnty;

  public InventoryDetail() {}

  public InventoryDetail(
      long inventoryId,
      Product product,
      BigDecimal quantity,
      Date date,
      BigDecimal buyingPrice,
      BigDecimal sellingPrice,
      Branch branchDetail,
      Vendor vendor,
      Organisation organisation,
      int reorderQnty) {
    super();
    this.inventoryId = inventoryId;
    this.product = product;
    this.quantity = quantity;
    this.date = date;
    this.buyingPrice = buyingPrice;
    this.sellingPrice = sellingPrice;
    this.BranchDetail = branchDetail;
    this.vendor = vendor;
    this.organisation = organisation;
    this.reorderQnty = reorderQnty;
  }

  public InventoryDetail(String products) {}

  public long getInventoryId() {
    return this.inventoryId;
  }

  public void setInventoryId(long inventoryId) {
    this.inventoryId = inventoryId;
  }

  public Product getProduct() {
    return this.product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Branch getBranchDetail() {
    return this.BranchDetail;
  }

  public void setBranchDetail(Branch branchDetail) {
    this.BranchDetail = branchDetail;
  }

  public Vendor getVendor() {
    return this.vendor;
  }

  public void setVendor(Vendor vendor) {
    this.vendor = vendor;
  }

  public int getReorderQnty() {
    return this.reorderQnty;
  }

  public void setReorderQnty(int reorderQnty) {
    this.reorderQnty = reorderQnty;
  }

  public BigDecimal getBuyingPrice() {
    return this.buyingPrice;
  }

  public void setBuyingPrice(BigDecimal buyingPrice) {
    this.buyingPrice = buyingPrice;
  }

  public BigDecimal getSellingPrice() {
    return this.sellingPrice;
  }

  public void setSellingPrice(BigDecimal sellingPrice) {
    this.sellingPrice = sellingPrice;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  @Override
  public String toString() {
    return "InventoryDetail [inventoryId="
        + inventoryId
        + ", product="
        + product
        + ", quantity="
        + quantity
        + ", date="
        + date
        + ", buyingPrice="
        + buyingPrice
        + ", sellingPrice="
        + sellingPrice
        + ", BranchDetail="
        + BranchDetail
        + ", vendor="
        + vendor
        + ", organisation="
        + organisation
        + ", reorderQnty="
        + reorderQnty
        + "]";
  }
}
