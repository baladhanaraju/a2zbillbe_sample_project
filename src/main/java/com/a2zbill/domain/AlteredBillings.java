package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.util.Date;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "altered_billings", schema = "product")
public class AlteredBillings {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "altered_billings_type_data_id_seq")
  @SequenceGenerator(
      name = "altered_billings_type_data_id_seq",
      sequenceName = "altered_billings_type_data_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "date")
  private Date date;

  @Column(name = "items_added", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> itemsAdded;

  @Column(name = "items_removed", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> itemsRemoved;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "billing_id", unique = true, nullable = true)
  private Billing billingDetails;

  @Column(name = "item_status")
  private String itemstatus;

  @Column(name = "old_total_amount")
  private String oldTotalAmount;

  @Column(name = "new_total_amount")
  private String newTotalAmount;

  public AlteredBillings() {}

  public AlteredBillings(
      final long id,
      final Date createdDate,
      final Date modifiedDate,
      final Date date,
      final Map<String, Object> itemsAdded,
      final Map<String, Object> itemsRemoved,
      final Billing billingDetails,
      final String itemstatus,
      final String oldTotalAmount,
      final String newTotalAmount) {
    super();
    this.id = id;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.date = date;
    this.itemsAdded = itemsAdded;
    this.itemsRemoved = itemsRemoved;
    this.billingDetails = billingDetails;
    this.itemstatus = itemstatus;
    this.oldTotalAmount = oldTotalAmount;
    this.newTotalAmount = newTotalAmount;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public String getOldTotalAmount() {
    return oldTotalAmount;
  }

  public void setOldTotalAmount(final String oldTotalAmount) {
    this.oldTotalAmount = oldTotalAmount;
  }

  public String getNewTotalAmount() {
    return newTotalAmount;
  }

  public void setNewTotalAmount(final String newTotalAmount) {
    this.newTotalAmount = newTotalAmount;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public Map<String, Object> getItemsAdded() {
    return itemsAdded;
  }

  public void setItemsAdded(final Map<String, Object> itemsAdded) {
    this.itemsAdded = itemsAdded;
  }

  public Map<String, Object> getItemsRemoved() {
    return itemsRemoved;
  }

  public void setItemsRemoved(final Map<String, Object> itemsRemoved) {
    this.itemsRemoved = itemsRemoved;
  }

  public Billing getBillingDetails() {
    return billingDetails;
  }

  public void setBillingDetails(final Billing billingDetails) {
    this.billingDetails = billingDetails;
  }

  public String getItemstatus() {
    return itemstatus;
  }

  public void setItemstatus(final String itemstatus) {
    this.itemstatus = itemstatus;
  }

  @Override
  public String toString() {
    return "AlteredBillings [id="
        + id
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", date="
        + date
        + ", itemsAdded="
        + itemsAdded
        + ", itemsRemoved="
        + itemsRemoved
        + ", billingDetails="
        + billingDetails
        + ", itemstatus="
        + itemstatus
        + ", oldTotalAmount="
        + oldTotalAmount
        + ", newTotalAmount="
        + newTotalAmount
        + "]";
  }
}
