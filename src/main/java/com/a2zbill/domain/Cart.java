package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "cart", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = true)
@Proxy(lazy = false)
public class Cart implements Serializable {

  /// ** The id. */

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "cart_id_seq")
  @SequenceGenerator(
      name = "cart_id_seq",
      sequenceName = "cart_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "cart_id")
  private long cartId;

  /** The startTime. */
  @Column(name = "start_time")
  private Date startTime;

  @Column(name = "guid")
  private String guid;

  public String getGuid() {
    return this.guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  /** The endTime. */
  @Column(name = "end_time")
  private Date endTime;

  /** The cartStatus. */
  @Column(name = "cart_status")
  private String cartStatus;

  /** The counterDetails. */
  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "counter_id", insertable = true, updatable = true)
  private Counter counterDetails;

  @JsonManagedReference
  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JsonIgnore
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", unique = true, nullable = true)
  private Customer customer;

  @Column(name = "parent_guid")
  private String parentguId;

  @Column(name = "description")
  private String description;

  @Column(name = "max_capacity")
  private long maxCapacity;

  @Column(name = "occupied_capacity")
  private long occupiedCapacity;

  /** Instantiates a new cart. */
  public Cart() {}

  /**
   * Instantiates a new billing.
   *
   * @param cartId the cartId
   * @param startTime the startTime
   * @param endTime the endTime
   * @param cartStatus the cartStatus
   * @param counterDetails the counterDetails
   * @param cartProductDetails the cartProductDetails
   * @param rewardDetails the rewardDetails
   */

  /**
   * Gets the cartId.
   *
   * @return the cartId
   */
  public long getCartId() {
    return this.cartId;
  }

  public Cart(
      long cartId,
      Date startTime,
      String guid,
      Date endTime,
      String cartStatus,
      Counter counterDetails,
      Organisation organisation,
      Branch branch,
      Customer customer,
      String parentguId,
      String description) {
    super();
    this.cartId = cartId;
    this.startTime = startTime;
    this.guid = guid;
    this.endTime = endTime;
    this.cartStatus = cartStatus;
    this.counterDetails = counterDetails;
    this.organisation = organisation;
    this.branch = branch;
    this.customer = customer;
    this.parentguId = parentguId;
    this.description = description;
  }

  public String getParentguId() {
    return this.parentguId;
  }

  public void setParentguId(String parentguId) {
    this.parentguId = parentguId;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Sets the cartId.
   *
   * @param cartId the new cartId
   */
  public void setCartId(long cartId) {
    this.cartId = cartId;
  }

  /**
   * Gets the startTime.
   *
   * @return the startTime
   */
  public Date getStartTime() {
    return this.startTime;
  }
  /**
   * Sets the startTime.
   *
   * @param startTime the new startTime
   */
  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  /**
   * Gets the endTime.
   *
   * @return the endTime
   */
  public Date getEndTime() {
    return this.endTime;
  }
  /**
   * Sets the endTime.
   *
   * @param endTime the new endTime
   */
  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  /**
   * Gets the cartStatus.
   *
   * @return the cartStatus
   */
  public String getCartStatus() {
    return this.cartStatus;
  }
  /**
   * Sets the cartStatus.
   *
   * @param cartStatus the new cartStatus
   */
  public void setCartStatus(String cartStatus) {
    this.cartStatus = cartStatus;
  }

  /**
   * Gets the counterDetails.
   *
   * @return the counterDetails
   */
  public Counter getCounterDetails() {
    return this.counterDetails;
  }

  /**
   * Sets the counterDetails.
   *
   * @param counterDetails the new counterDetails
   */
  public void setCounterDetails(Counter counterDetails) {
    this.counterDetails = counterDetails;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public long getMaxCapacity() {
    return this.maxCapacity;
  }

  public void setMaxCapacity(long maxCapacity) {
    this.maxCapacity = maxCapacity;
  }

  public long getOccupiedCapacity() {
    return this.occupiedCapacity;
  }

  public void setOccupiedCapacity(long occupiedCapacity) {
    this.occupiedCapacity = occupiedCapacity;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }
}
