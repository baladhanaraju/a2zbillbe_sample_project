package com.a2zbill.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class GSTR1 implements Serializable {

  private String gstin;

  private String fp;

  private String gt;

  private String curGt;

  private List<B2B> b2b = new ArrayList<B2B>();

  private List<B2C> b2c = new ArrayList<B2C>();

  public GSTR1() {}

  public GSTR1(String gstin, String fp, String gt, String curGt, List<B2B> b2b, List<B2C> b2c) {
    super();
    this.gstin = gstin;
    this.fp = fp;
    this.gt = gt;
    this.curGt = curGt;
    this.b2b = b2b;
    this.b2c = b2c;
  }

  public String getGstin() {
    return this.gstin;
  }

  public void setGstin(String gstin) {
    this.gstin = gstin;
  }

  public String getFp() {
    return this.fp;
  }

  public void setFp(String fp) {
    this.fp = fp;
  }

  public String getGt() {
    return this.gt;
  }

  public void setGt(String gt) {
    this.gt = gt;
  }

  public String getCurGt() {
    return this.curGt;
  }

  public void setCurGt(String curGt) {
    this.curGt = curGt;
  }

  public List<B2B> getB2b() {
    return this.b2b;
  }

  public void setB2b(List<B2B> b2b) {
    this.b2b = b2b;
  }

  public List<B2C> getB2c() {
    return this.b2c;
  }

  public void setB2c(List<B2C> b2c) {
    this.b2c = b2c;
  }
}
