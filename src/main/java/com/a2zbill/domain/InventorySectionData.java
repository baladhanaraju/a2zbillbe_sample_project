package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "inventory_section_data", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class InventorySectionData implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inventorysectiondata_id_seq")
  @SequenceGenerator(
      name = "inventorysectiondata_id_seq",
      sequenceName = "inventorysectiondata_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "section_name")
  private String sectionName;

  @Column(name = "quantity")
  private BigDecimal quantity;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product productId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "inventory_section_id", unique = true, nullable = true)
  private InventorySection inventorySectionId;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "orgid", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branchid", unique = true, nullable = true)
  private Branch branch;

  public InventorySectionData() {}

  public InventorySectionData(
      long id,
      String sectionName,
      BigDecimal quantity,
      Product productId,
      InventorySection inventorySectionId,
      Date createdDate,
      Date modifiedDate,
      Organisation organisation,
      Branch branch) {
    super();
    this.id = id;
    this.sectionName = sectionName;
    this.quantity = quantity;
    this.productId = productId;
    this.inventorySectionId = inventorySectionId;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSectionName() {
    return this.sectionName;
  }

  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public Product getProductId() {
    return this.productId;
  }

  public void setProductId(Product productId) {
    this.productId = productId;
  }

  public InventorySection getInventorySectionId() {
    return this.inventorySectionId;
  }

  public void setInventorySectionId(InventorySection inventorySectionId) {
    this.inventorySectionId = inventorySectionId;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  @Override
  public String toString() {
    return "InventorySectionData [id="
        + id
        + ", sectionName="
        + sectionName
        + ", quantity="
        + quantity
        + ", productId="
        + productId
        + ", inventorySectionId="
        + inventorySectionId
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + "]";
  }
}
