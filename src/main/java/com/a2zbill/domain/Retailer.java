package com.a2zbill.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "retailer", schema = "gstforms")
public class Retailer implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "retailer_id_seq")
  @SequenceGenerator(
      name = "retailer_id_seq",
      sequenceName = "retailer_id_seq",
      allocationSize = 1,
      schema = "product")
  private long id;

  @Column(name = "company_name")
  private String companyName;

  @Column(name = "gst_number")
  private String retailerGstNumber;

  @Column(name = "address")
  private String retailerAddress;

  @Column(name = "date")
  private String retailerDate;

  public Retailer() {}

  public Retailer(
      final long id,
      final String companyName,
      final String retailerGstNumber,
      final String retailerAddress,
      final String retailerDate) {
    super();
    this.id = id;
    this.companyName = companyName;
    this.retailerGstNumber = retailerGstNumber;
    this.retailerAddress = retailerAddress;
    this.retailerDate = retailerDate;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getCompanyName() {
    return this.companyName;
  }

  public void setCompanyName(final String companyName) {
    this.companyName = companyName;
  }

  public String getRetailerGstNumber() {
    return this.retailerGstNumber;
  }

  public void setRetailerGstNumber(final String retailerGstNumber) {
    this.retailerGstNumber = retailerGstNumber;
  }

  public String getRetailerAddress() {
    return this.retailerAddress;
  }

  public void setRetailerAddress(final String retailerAddress) {
    this.retailerAddress = retailerAddress;
  }

  public String getRetailerDate() {
    return this.retailerDate;
  }

  public void setRetailerDate(final String retailerDate) {
    this.retailerDate = retailerDate;
  }
}
