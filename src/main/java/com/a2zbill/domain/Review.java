package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "review", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class Review implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "review_id_seq")
  @SequenceGenerator(
      name = "review_id_seq",
      sequenceName = "review_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "guid")
  private String guid;

  @Column(name = "parent_guid")
  private String parentGuid;

  @Column(name = "billingid")
  private String billingId;

  /** The totalAmount. */
  @Column(name = "bill_amount")
  private BigDecimal billAmount;

  /** The taxableAmount. */
  @Column(name = "total_gst_amount")
  private BigDecimal taxableAmount;

  /** The Date. */
  @Column(name = "date")
  private Date billDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public Review() {}

  public Review(
      final long id,
      final String guid,
      final String parentGuid,
      final String billingId,
      final BigDecimal billAmount,
      final BigDecimal taxableAmount,
      final Date billDate,
      final Organisation organisation,
      final Branch branch) {
    super();
    this.id = id;
    this.guid = guid;
    this.parentGuid = parentGuid;
    this.billingId = billingId;
    this.billAmount = billAmount;
    this.taxableAmount = taxableAmount;
    this.billDate = billDate;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getGuid() {
    return this.guid;
  }

  public void setGuid(final String guid) {
    this.guid = guid;
  }

  public String getParentGuid() {
    return this.parentGuid;
  }

  public void setParentGuid(final String parentGuid) {
    this.parentGuid = parentGuid;
  }

  public String getBillingId() {
    return this.billingId;
  }

  public void setBillingId(final String billingId) {
    this.billingId = billingId;
  }

  public BigDecimal getBillAmount() {
    return this.billAmount;
  }

  public void setBillAmount(final BigDecimal billAmount) {
    this.billAmount = billAmount;
  }

  public BigDecimal getTaxableAmount() {
    return this.taxableAmount;
  }

  public void setTaxableAmount(final BigDecimal taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  public Date getBillDate() {
    return this.billDate;
  }

  public void setBillDate(final Date billDate) {
    this.billDate = billDate;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  @Override
  public String toString() {
    return "Review [id="
        + id
        + ", guid="
        + guid
        + ", parentGuid="
        + parentGuid
        + ", billingId="
        + billingId
        + ", billAmount="
        + billAmount
        + ", taxableAmount="
        + taxableAmount
        + ", billDate="
        + billDate
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + "]";
  }
}
