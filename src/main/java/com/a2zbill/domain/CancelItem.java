package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "cancel_item", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class CancelItem implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "cancel_item_id_seq")
  @SequenceGenerator(
      name = "cancel_item_id_seq",
      sequenceName = "cancel_item_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "guid")
  private String guid;

  @Column(name = "counter_number")
  private long counternumber;

  @Column(name = "product_name")
  private String productname;

  @Column(name = "date")
  private Date date;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @Column(name = "reason")
  private String reason;

  public CancelItem() {}

  public CancelItem(
      final long id,
      final String guid,
      final long counternumber,
      final String productname,
      final Date date,
      final Organisation organisation,
      final Branch branch,
      final String reason) {
    super();
    this.id = id;
    this.guid = guid;
    this.counternumber = counternumber;
    this.productname = productname;
    this.date = date;
    this.organisation = organisation;
    this.branch = branch;
    this.reason = reason;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getGuid() {
    return this.guid;
  }

  public void setGuid(final String guid) {
    this.guid = guid;
  }

  public long getCounternumber() {
    return this.counternumber;
  }

  public void setCounternumber(final long counternumber) {
    this.counternumber = counternumber;
  }

  public String getProductname() {
    return this.productname;
  }

  public void setProductname(final String productname) {
    this.productname = productname;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public String getReason() {
    return this.reason;
  }

  public void setReason(final String reason) {
    this.reason = reason;
  }

  @Override
  public String toString() {
    return "CancelItem [id="
        + id
        + ", guid="
        + guid
        + ", counternumber="
        + counternumber
        + ", productname="
        + productname
        + ", date="
        + date
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", reason="
        + reason
        + "]";
  }
}
