package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "invoice_bill", schema = "product")
public class InvoiceBill implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "invoice_bill_id_seq")
  @SequenceGenerator(
      name = "invoice_bill_id_seq",
      sequenceName = "invoice_bill_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "invoice_number")
  private String invoiceNumber;

  @Column(name = "note")
  private String note;

  @Column(name = "total_amount")
  private BigDecimal totalAmount;

  @Column(name = "taxable_amount")
  private BigDecimal taxableAmount;

  @Column(name = "discount_amount")
  private BigDecimal discountAmount;

  @Column(name = "date")
  private Date date;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
  @JoinColumn(name = "vendor_id", insertable = true, updatable = true)
  private Vendor vendorId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_vendor", unique = true, nullable = true)
  private Branch branchVendor;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "from_section_id", insertable = true, updatable = true)
  private InventorySection fromSectionId;

  @Column(name = "image")
  private byte[] image;

  @Column(name = "extracted_data", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> extractedData;

  public InvoiceBill() {}

  public InvoiceBill(
      long id,
      String invoiceNumber,
      String note,
      BigDecimal totalAmount,
      BigDecimal taxableAmount,
      BigDecimal discountAmount,
      Date date,
      Date createdDate,
      Date modifiedDate,
      Vendor vendorId,
      Branch branch,
      Branch branchVendor,
      Organisation organisation,
      InventorySection fromSectionId,
      byte[] image,
      Map<String, Object> extractedData) {
    super();
    this.id = id;
    this.invoiceNumber = invoiceNumber;
    this.note = note;
    this.totalAmount = totalAmount;
    this.taxableAmount = taxableAmount;
    this.discountAmount = discountAmount;
    this.date = date;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.vendorId = vendorId;
    this.branch = branch;
    this.branchVendor = branchVendor;
    this.organisation = organisation;
    this.fromSectionId = fromSectionId;
    this.image = image;
    this.extractedData = extractedData;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getInvoiceNumber() {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String invoiceNumber) {
    this.invoiceNumber = invoiceNumber;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  public BigDecimal getTaxableAmount() {
    return taxableAmount;
  }

  public void setTaxableAmount(BigDecimal taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  public BigDecimal getDiscountAmount() {
    return discountAmount;
  }

  public void setDiscountAmount(BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Vendor getVendorId() {
    return vendorId;
  }

  public void setVendorId(Vendor vendorId) {
    this.vendorId = vendorId;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranchVendor() {
    return branchVendor;
  }

  public void setBranchVendor(Branch branchVendor) {
    this.branchVendor = branchVendor;
  }

  public InventorySection getFromSectionId() {
    return fromSectionId;
  }

  public void setFromSectionId(InventorySection fromSectionId) {
    this.fromSectionId = fromSectionId;
  }

  public byte[] getImage() {
    return image;
  }

  public void setImage(byte[] image) {
    this.image = image;
  }

  public Map<String, Object> getExtractedData() {
    return extractedData;
  }

  public void setExtractedData(Map<String, Object> extractedData) {
    this.extractedData = extractedData;
  }
}
