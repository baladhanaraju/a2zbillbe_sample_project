package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "billing_num_type_data", schema = "product")
public class BillingNumData {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "billing_num_type_data_id_seq")
  @SequenceGenerator(
      name = "billing_num_type_data_id_seq",
      sequenceName = "billing_num_type_data_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "billing_num_type_id", insertable = true, updatable = true)
  private BillingNumType billingNumType;

  @Column(name = "current_count")
  private long currentCount;

  // @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date")
  private Date billdate;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  public BillingNumData() {}

  public BillingNumData(
      long id,
      Organisation organisation,
      Branch branch,
      BillingNumType billingNumType,
      long currentCount,
      Date billdate,
      Date createdDate,
      Date modifiedDate) {
    super();
    this.id = id;
    this.organisation = organisation;
    this.branch = branch;
    this.billingNumType = billingNumType;
    this.currentCount = currentCount;
    this.billdate = billdate;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public BillingNumType getBillingNumType() {
    return this.billingNumType;
  }

  public void setBillingNumType(BillingNumType billingNumType) {
    this.billingNumType = billingNumType;
  }

  public long getCurrentCount() {
    return this.currentCount;
  }

  public void setCurrentCount(long currentCount) {
    this.currentCount = currentCount;
  }

  public Date getBilldate() {
    return this.billdate;
  }

  public void setBilldate(Date billdate) {
    this.billdate = billdate;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  @Override
  public String toString() {
    return "BillingNumData [id="
        + id
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", billingNumType="
        + billingNumType
        + ", currentCount="
        + currentCount
        + ", billdate="
        + billdate
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + "]";
  }
}
