package com.a2zbill.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Items implements Serializable {

  private long num;

  private ItemDetail itm_det;

  public Items() {}

  public Items(long num, ItemDetail itm_det) {
    super();
    this.num = num;
    this.itm_det = itm_det;
  }

  public long getNum() {
    return this.num;
  }

  public void setNum(long num) {
    this.num = num;
  }

  public ItemDetail getItm_det() {
    return this.itm_det;
  }

  public void setItm_det(ItemDetail itm_det) {
    this.itm_det = itm_det;
  }
}
