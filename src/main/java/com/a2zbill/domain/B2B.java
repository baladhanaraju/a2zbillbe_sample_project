package com.a2zbill.domain;

import java.io.Serializable;

@SuppressWarnings("serial")
public class B2B implements Serializable {

  private String ctin;

  private Invoice inv;

  public B2B() {}

  public B2B(String ctin, Invoice inv) {
    super();
    this.ctin = ctin;
    this.inv = inv;
  }

  public String getCtin() {
    return this.ctin;
  }

  public void setCtin(String ctin) {
    this.ctin = ctin;
  }

  public Invoice getInv() {
    return this.inv;
  }

  public void setInv(Invoice inv) {
    this.inv = inv;
  }
}
