package com.a2zbill.domain;

import com.a2zbill.domain.datatype.GenericArrayUserType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@Table(name = "store_type", schema = "organisation")
@TypeDef(name = "GenericArrayUserType", typeClass = GenericArrayUserType.class)
@Proxy(lazy = false)
public class StoreType implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "store_type_id")
  @SequenceGenerator(
      name = "store_type_id",
      sequenceName = "store_type_id",
      allocationSize = 1,
      schema = "organisation")
  @Column(name = "id")
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "date")
  private Date date;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "link")
  @Type(type = "GenericArrayUserType")
  private Integer[] link;

  @Column(name = "offlinelinks_array")
  @Type(type = "GenericArrayUserType")
  private Integer[] offlineLink;

  public StoreType() {}

  public StoreType(
      final long id,
      final String name,
      final Date date,
      final Date createdDate,
      final Date modifiedDate,
      final Integer[] link,
      final Integer[] offlineLink) {
    super();
    this.id = id;
    this.name = name;
    this.date = date;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.link = link;
    this.offlineLink = offlineLink;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Integer[] getLink() {
    return link;
  }

  public void setLink(final Integer[] link) {
    this.link = link;
  }

  public Integer[] getOfflineLink() {
    return offlineLink;
  }

  public void setOfflineLink(final Integer[] offlineLink) {
    this.offlineLink = offlineLink;
  }
}
