package com.a2zbill.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class Invoice implements Serializable {

  private long inum;

  private Date idt;

  private long val;

  private long pos;

  private String rchrg;

  private List<Items> itms = new ArrayList<Items>();

  public Invoice() {}

  public Invoice(long inum, Date idt, long val, long pos, String rchrg, List<Items> itms) {
    super();
    this.inum = inum;
    this.idt = idt;
    this.val = val;
    this.pos = pos;
    this.rchrg = rchrg;
    this.itms = itms;
  }

  public long getInum() {
    return this.inum;
  }

  public void setInum(long inum) {
    this.inum = inum;
  }

  public Date getIdt() {
    return this.idt;
  }

  public void setIdt(Date idt) {
    this.idt = idt;
  }

  public long getVal() {
    return this.val;
  }

  public void setVal(long val) {
    this.val = val;
  }

  public long getPos() {
    return this.pos;
  }

  public void setPos(long pos) {
    this.pos = pos;
  }

  public String getRchrg() {
    return this.rchrg;
  }

  public void setRchrg(String rchrg) {
    this.rchrg = rchrg;
  }

  public List<Items> getItms() {
    return this.itms;
  }

  public void setItemsDetails(List<Items> itms) {
    this.itms = itms;
  }
}
