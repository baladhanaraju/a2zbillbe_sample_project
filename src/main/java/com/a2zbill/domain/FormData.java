package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "form_data", schema = "form")
public class FormData implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "formdata_id_seq")
  @SequenceGenerator(
      name = "formdata_id_seq",
      sequenceName = "formdata_id_seq",
      allocationSize = 1,
      schema = "form")
  @Column(name = "id")
  private long id;

  @Column(name = "form_schema", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> formschema;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "formtype_id", unique = true, nullable = true)
  private FormType formtype;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "formschema_id", unique = true, nullable = true)
  private FormSchema formschemaid;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "emp_id", unique = true, nullable = true)
  private Employee employee;

  public FormData() {}

  public FormData(
      final long id,
      final Map<String, Object> formschema,
      final Date createdDate,
      final Date modifiedDate,
      final FormType formtype,
      final FormSchema formschemaid,
      final Branch branch,
      final Organisation organisation,
      final Employee employee) {
    super();
    this.id = id;
    this.formschema = formschema;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.formtype = formtype;
    this.formschemaid = formschemaid;
    this.branch = branch;
    this.organisation = organisation;
    this.employee = employee;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Map<String, Object> getFormschema() {
    return formschema;
  }

  public void setFormschema(final Map<String, Object> formschema) {
    this.formschema = formschema;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public FormType getFormtype() {
    return this.formtype;
  }

  public void setFormtype(final FormType formtype) {
    this.formtype = formtype;
  }

  public FormSchema getFormschemaid() {
    return this.formschemaid;
  }

  public void setFormschemaid(final FormSchema formschemaid) {
    this.formschemaid = formschemaid;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Employee getEmployee() {
    return this.employee;
  }

  public void setEmployee(final Employee employee) {
    this.employee = employee;
  }

  @Override
  public String toString() {
    return "FormData [id="
        + id
        + ", formschema="
        + formschema
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", formtype="
        + formtype
        + ", formschemaid="
        + formschemaid
        + ", branch="
        + branch
        + ", organisation="
        + organisation
        + ", employee="
        + employee
        + "]";
  }
}
