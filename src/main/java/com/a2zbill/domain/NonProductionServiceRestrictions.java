package com.a2zbill.domain;

import com.a2zbill.domain.datatype.GenericArrayUserType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Arrays;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "GenericArrayUserType", typeClass = GenericArrayUserType.class)
@Table(name = "nonproduction_service_restrictions", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
public class NonProductionServiceRestrictions implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "nonproduction_service_id_seq")
  @SequenceGenerator(
      name = "nonproduction_service_id_seq",
      sequenceName = "nonproduction_service_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "service_type")
  private String serviceType;

  @Column(name = "member_type")
  private String memberType;

  @Column(name = "allowed_members")
  @Type(type = "GenericArrayUserType")
  private Integer[] members;

  public NonProductionServiceRestrictions() {}

  public NonProductionServiceRestrictions(
      long id, String serviceType, String memberType, Integer[] members) {
    super();
    this.id = id;
    this.serviceType = serviceType;
    this.memberType = memberType;
    this.members = members;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getServiceType() {
    return serviceType;
  }

  public void setServiceType(String serviceType) {
    this.serviceType = serviceType;
  }

  public String getMemberType() {
    return memberType;
  }

  public void setMemberType(String memberType) {
    this.memberType = memberType;
  }

  public Integer[] getMembers() {
    return members;
  }

  public void setMembers(Integer[] members) {
    this.members = members;
  }

  @Override
  public String toString() {
    return "NonProductionServiceRestrictions [id="
        + id
        + ", serviceType="
        + serviceType
        + ", memberType="
        + memberType
        + ", members="
        + Arrays.toString(members)
        + "]";
  }
}
