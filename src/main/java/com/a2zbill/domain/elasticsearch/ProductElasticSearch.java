package com.a2zbill.domain.elasticsearch;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProductElasticSearch {

  private Long id;
  private String productName;
  private String hsnNumber;
  private BigDecimal productPrice;
  private String code;
  private long productCount;
  private boolean packageFlag;
  private String includingTaxFlag;
  private String image;
  private BigDecimal CGST;
  private BigDecimal SGST;
  private BigDecimal IGST;
  private boolean vegFlag;
  private long parentId;
  private boolean status;
  private boolean subscriptionFlag;
  private String measurement;
  private String measurement1;
  private String measurement2;
  private Long branchId;
  private Long organisationId;

  public ProductElasticSearch() {}

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(final String productName) {
    this.productName = productName;
  }

  public String getHsnNumber() {
    return hsnNumber;
  }

  public void setHsnNumber(final String hsnNumber) {
    this.hsnNumber = hsnNumber;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(final BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public long getProductCount() {
    return productCount;
  }

  public void setProductCount(final long productCount) {
    this.productCount = productCount;
  }

  public boolean isPackageFlag() {
    return packageFlag;
  }

  public void setPackageFlag(final boolean packageFlag) {
    this.packageFlag = packageFlag;
  }

  public String getIncludingTaxFlag() {
    return includingTaxFlag;
  }

  public void setIncludingTaxFlag(final String includingTaxFlag) {
    this.includingTaxFlag = includingTaxFlag;
  }

  public String getImage() {
    return image;
  }

  public void setImage(final String image) {
    this.image = image;
  }

  public BigDecimal getCGST() {
    return CGST;
  }

  public void setCGST(final BigDecimal CGST) {
    this.CGST = CGST;
  }

  public BigDecimal getSGST() {
    return SGST;
  }

  public void setSGST(final BigDecimal SGST) {
    this.SGST = SGST;
  }

  public BigDecimal getIGST() {
    return IGST;
  }

  public void setIGST(final BigDecimal IGST) {
    this.IGST = IGST;
  }

  public boolean isVegFlag() {
    return vegFlag;
  }

  public void setVegFlag(final boolean vegFlag) {
    this.vegFlag = vegFlag;
  }

  public long getParentId() {
    return parentId;
  }

  public void setParentId(final long parentId) {
    this.parentId = parentId;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(final boolean status) {
    this.status = status;
  }

  public boolean isSubscriptionFlag() {
    return subscriptionFlag;
  }

  public void setSubscriptionFlag(final boolean subscriptionFlag) {
    this.subscriptionFlag = subscriptionFlag;
  }

  public String getMeasurement() {
    return measurement;
  }

  public void setMeasurement(final String measurement) {
    this.measurement = measurement;
  }

  public String getMeasurement1() {
    return measurement1;
  }

  public void setMeasurement1(final String measurement1) {
    this.measurement1 = measurement1;
  }

  public String getMeasurement2() {
    return measurement2;
  }

  public void setMeasurement2(final String measurement2) {
    this.measurement2 = measurement2;
  }

  public Long getBranchId() {
    return branchId;
  }

  public void setBranchId(final Long branchId) {
    this.branchId = branchId;
  }

  public Long getOrganisationId() {
    return organisationId;
  }

  public void setOrganisationId(final Long organisationId) {
    this.organisationId = organisationId;
  }
}
