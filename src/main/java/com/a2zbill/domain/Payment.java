package com.a2zbill.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "modeofpayment", schema = "product")
public class Payment implements Serializable {

  @Id
  @Column(name = "payment_id")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "payment_mode_id_seq")
  @SequenceGenerator(
      name = "payment_mode_id_seq",
      sequenceName = "payment_mode_id_seq",
      allocationSize = 1,
      schema = "product")
  private long paymentId;

  @Column(name = "payment_type")
  private String paymentType;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "defalt")
  private boolean defalt;

  public Payment() {}

  public Payment(final long paymentId, final String paymentType, final boolean defalt) {
    super();
    this.paymentId = paymentId;
    this.paymentType = paymentType;
    this.defalt = defalt;
  }

  public long getPaymentId() {
    return this.paymentId;
  }

  public void setPaymentId(final long paymentId) {
    this.paymentId = paymentId;
  }

  public String getPaymentType() {
    return this.paymentType;
  }

  public void setPaymentType(final String paymentType) {
    this.paymentType = paymentType;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public boolean isDefalt() {
    return defalt;
  }

  public void setDefalt(final boolean defalt) {
    this.defalt = defalt;
  }

  @Override
  public String toString() {
    return "Payment [paymentId="
        + paymentId
        + ", paymentType="
        + paymentType
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", defalt="
        + defalt
        + "]";
  }
}
