package com.a2zbill.domain;

public enum EmployeeDetailsName {
  ROLE_SALES_MANAGER,
  ROLE_ADMIN,
  ROLE_SALES_PERSON;
}
