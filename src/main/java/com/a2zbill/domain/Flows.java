package com.a2zbill.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "flows", schema = "flow")
public class Flows {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "flows_id_seq")
  @SequenceGenerator(
      name = "flows_id_seq",
      sequenceName = "flows_id_seq",
      allocationSize = 1,
      schema = "flow")
  @Column(name = "id")
  private long id;

  @Column(name = "flow_name")
  private String flowName;

  @Column(name = "description")
  private String description;

  public Flows() {}

  public Flows(final long id, final String flowName, final String description) {
    super();
    this.id = id;
    this.flowName = flowName;
    this.description = description;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getFlowName() {
    return this.flowName;
  }

  public void setFlowName(final String flowName) {
    this.flowName = flowName;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "Flows [id=" + id + ", flowName=" + flowName + ", description=" + description + "]";
  }
}
