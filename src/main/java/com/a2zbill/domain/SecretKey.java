package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "secretkey_table", schema = "product")
public class SecretKey {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "secretkey_table_id_seq")
  @SequenceGenerator(
      name = "secretkey_table_id_seq",
      sequenceName = "secretkey_table_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "secret_key")
  private String secretKey;

  @Column(name = "command_name")
  private String commandName;

  @Temporal(TemporalType.DATE)
  @Column(name = "created_date")
  private Date createdDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "modified_date")
  private Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public SecretKey() {}

  public SecretKey(
      long id,
      String secretKey,
      String commandName,
      Date createdDate,
      Date modifiedDate,
      Organisation organisation,
      Branch branch) {
    super();
    this.id = id;
    this.secretKey = secretKey;
    this.commandName = commandName;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSecretKey() {
    return this.secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public String getCommandName() {
    return this.commandName;
  }

  public void setCommandName(String commandName) {
    this.commandName = commandName;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  @Override
  public String toString() {
    return "Secret [id="
        + id
        + ", secretKey="
        + secretKey
        + ", commandName="
        + commandName
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + "]";
  }
}
