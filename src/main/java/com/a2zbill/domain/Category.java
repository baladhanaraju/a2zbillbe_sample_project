package com.a2zbill.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gst_category", schema = "gst")
public class Category implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "category_id")
  private int id;

  @Column(name = "category_hsn_code")
  private String categoryCode;

  @Column(name = "category_name")
  private String categoryName;

  @Column(name = " chapter_id")
  private long chapterId;

  @Column(name = " parentid")
  private long parentId;

  public Category() {}

  public Category(
      final int id,
      final String categoryCode,
      final String categoryName,
      final long chapterId,
      final long parentId) {
    super();
    this.id = id;
    this.categoryCode = categoryCode;
    this.categoryName = categoryName;
    this.chapterId = chapterId;
    this.parentId = parentId;
  }

  public int getId() {
    return this.id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getCategoryCode() {
    return this.categoryCode;
  }

  public void setCategoryCode(final String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryName() {
    return this.categoryName;
  }

  public void setCategoryName(final String categoryName) {
    this.categoryName = categoryName;
  }

  public long getChapterId() {
    return this.chapterId;
  }

  public void setChapterId(final long chapterId) {
    this.chapterId = chapterId;
  }

  public long getParentId() {
    return this.parentId;
  }

  public void setParentId(final long parentId) {
    this.parentId = parentId;
  }

  @Override
  public String toString() {
    return "Category [id="
        + id
        + ", categoryCode="
        + categoryCode
        + ", categoryName="
        + categoryName
        + ", chapterId="
        + chapterId
        + ", parentId="
        + parentId
        + "]";
  }
}
