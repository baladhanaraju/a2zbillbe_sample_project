package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cart_product", schema = "product")
public class CartDetail implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "cart_product_id_seq")
  @SequenceGenerator(
      name = "cart_product_id_seq",
      sequenceName = "cart_product_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long Id;

  /** The cartDetails. */
  @JsonManagedReference
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JsonIgnore
  @JoinColumn(name = "cart_id", insertable = true, updatable = true)
  private Cart cartDetails;

  /** The counterDetails. */
  @JsonIgnore
  @JsonManagedReference
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "counter_id", insertable = true, updatable = true)
  private Counter counterDetails;

  /** The productName. */
  @Column(name = "product_name")
  private String productName;

  /** The quantity. */
  @Column(name = "quantity")
  private long quantity;

  /** The hsnCode. */
  @Column(name = "hsn_code")
  private String hsnCode;

  /** The productPrice. */
  @Column(name = "product_price")
  private BigDecimal productPrice;

  /** The discountAmount. */
  @Column(name = "discount_amount")
  private BigDecimal discountAmount;

  /** The cgstPercentage. */
  @Column(name = "cgst")
  private BigDecimal cgstPercentage;

  /** The sgstPercentage. */
  @Column(name = "sgst")
  private BigDecimal sgstPercentage;

  /** The IgstPercentage. */
  @Column(name = "igst")
  private BigDecimal IgstPercentage;

  /** The time. */
  @Column(name = "date")
  private Date time;

  /** The totalAmount. */
  @Column(name = "total_amount")
  private BigDecimal totalAmount;

  /** The taxableAmount. */
  @Column(name = "taxable_amount")
  private BigDecimal taxableAmount;

  @JsonManagedReference
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JsonIgnore
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonIgnore
  @JsonManagedReference
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branch;

  @Column(name = "instructions")
  private String instructions;

  /** Instantiates a new CartDetails. */
  public CartDetail() {}

  /**
   * Instantiates a new CartDetails.
   *
   * @param id the id
   * @param cartDetails the cartDetails
   * @param counterDetails the counterDetails
   * @param productName the productName
   * @param quantity the quantity
   * @param hsnCode the hsnCode
   * @param productPrice the productPrice
   * @param discountAmount the discountAmount
   * @param cgstPercentage the cgstPercentage
   * @param sgstPercentage the sgstPercentage
   * @param igstPercentage the igstPercentage
   * @param organisation the organisation
   * @param branch the branch
   * @param instructions the instructions
   * @param time the time
   * @param totalAmount the totalAmount
   * @param taxableAmount the taxableAmount
   */
  public CartDetail(
      final long id,
      final Cart cartDetails,
      final Counter counterDetails,
      final String productName,
      final long quantity,
      final String hsnCode,
      final BigDecimal productPrice,
      final BigDecimal discountAmount,
      final BigDecimal cgstPercentage,
      final BigDecimal sgstPercentage,
      final BigDecimal igstPercentage,
      final Date time,
      final BigDecimal totalAmount,
      final BigDecimal taxableAmount,
      final Organisation organisation,
      final Branch branch,
      final String instructions) {
    super();
    this.Id = id;
    this.cartDetails = cartDetails;
    this.counterDetails = counterDetails;
    this.productName = productName;
    this.quantity = quantity;
    this.hsnCode = hsnCode;
    this.productPrice = productPrice;
    this.discountAmount = discountAmount;
    this.cgstPercentage = cgstPercentage;
    this.sgstPercentage = sgstPercentage;
    this.IgstPercentage = igstPercentage;
    this.time = time;
    this.totalAmount = totalAmount;
    this.taxableAmount = taxableAmount;
    this.organisation = organisation;
    this.branch = branch;
    this.instructions = instructions;
  }

  /**
   * Gets the Id.
   *
   * @return the Id
   */
  public long getId() {
    return this.Id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(final long id) {
    this.Id = id;
  }

  /**
   * Gets the cartDetails.
   *
   * @return the cartDetails
   */
  public Cart getCartDetails() {
    return this.cartDetails;
  }

  /**
   * Sets the cartDetails.
   *
   * @param cartDetails the new cartDetails
   */
  public void setCartDetails(final Cart cartDetails) {
    this.cartDetails = cartDetails;
  }

  /**
   * Gets the counterDetails.
   *
   * @return the counterDetails
   */
  public Counter getCounterDetails() {
    return this.counterDetails;
  }

  /**
   * Sets the counterDetails.
   *
   * @param counterDetails the new counterDetails
   */
  public void setCounterDetails(final Counter counterDetails) {
    this.counterDetails = counterDetails;
  }

  /**
   * Gets the productName.
   *
   * @return the productName
   */
  public String getProductName() {
    return this.productName;
  }

  /**
   * Sets the productName.
   *
   * @param productName the new productName
   */
  public void setProductName(final String productName) {
    this.productName = productName;
  }

  /**
   * Gets the quantity.
   *
   * @return the quantity
   */
  public long getQuantity() {
    return this.quantity;
  }

  /**
   * Sets the quantity.
   *
   * @param quantity the new quantity
   */
  public void setQuantity(final long quantity) {
    this.quantity = quantity;
  }

  /**
   * Gets the hsnCode.
   *
   * @return the hsnCode
   */
  public String getHsnCode() {
    return this.hsnCode;
  }

  /**
   * Sets the hsnCode.
   *
   * @param hsnCode the new hsnCode
   */
  public void setHsnCode(final String hsnCode) {
    this.hsnCode = hsnCode;
  }

  /**
   * Gets the productPrice.
   *
   * @return the productPrice
   */
  public BigDecimal getProductPrice() {
    return this.productPrice;
  }

  /**
   * Sets the productPrice.
   *
   * @param productPrice the new productPrice
   */
  public void setProductPrice(final BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  /**
   * Gets the discountAmount.
   *
   * @return the discountAmount
   */
  public BigDecimal getDiscountAmount() {
    return this.discountAmount;
  }

  /**
   * Sets the discountAmount.
   *
   * @param discountAmount the new discountAmount
   */
  public void setDiscountAmount(final BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }

  /**
   * Gets the cgstPercentage.
   *
   * @return the cgstPercentage
   */
  public BigDecimal getCgstPercentage() {
    return this.cgstPercentage;
  }

  /**
   * Sets the cgstPercentage.
   *
   * @param cgstPercentage the new cgstPercentage
   */
  public void setCgstPercentage(final BigDecimal cgstPercentage) {
    this.cgstPercentage = cgstPercentage;
  }

  /**
   * Gets the sgstPercentage.
   *
   * @return the sgstPercentage
   */
  public BigDecimal getSgstPercentage() {
    return this.sgstPercentage;
  }

  /**
   * Sets the sgstPercentage.
   *
   * @param sgstPercentage the new sgstPercentage
   */
  public void setSgstPercentage(final BigDecimal sgstPercentage) {
    this.sgstPercentage = sgstPercentage;
  }

  /**
   * Gets the IgstPercentage.
   *
   * @return the IgstPercentage
   */
  public BigDecimal getIgstPercentage() {
    return this.IgstPercentage;
  }

  /**
   * Sets the IgstPercentage.
   *
   * @param igstPercentage the new igstPercentage
   */
  public void setIgstPercentage(final BigDecimal igstPercentage) {
    this.IgstPercentage = igstPercentage;
  }

  /**
   * Gets the time.
   *
   * @return the time
   */
  public Date getTime() {
    return this.time;
  }

  /**
   * Sets the time.
   *
   * @param time the new time
   */
  public void setTime(final Date time) {
    this.time = time;
  }

  /**
   * Gets the totalAmount.
   *
   * @return the totalAmount
   */
  public BigDecimal getTotalAmount() {
    return this.totalAmount;
  }

  /**
   * Sets the totalAmount.
   *
   * @param totalAmount the new totalAmount
   */
  public void setTotalAmount(final BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  /**
   * Gets the taxableAmount.
   *
   * @return the taxableAmount
   */
  public BigDecimal getTaxableAmount() {
    return this.taxableAmount;
  }

  /**
   * Sets the taxableAmount.
   *
   * @param taxableAmount the new taxableAmount
   */
  public void setTaxableAmount(final BigDecimal taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public String getInstructions() {
    return this.instructions;
  }

  public void setInstructions(final String instructions) {
    this.instructions = instructions;
  }

  @Override
  public String toString() {
    return "CartDetail [Id="
        + Id
        + ", cartDetails="
        + cartDetails
        + ", counterDetails="
        + counterDetails
        + ", productName="
        + productName
        + ", quantity="
        + quantity
        + ", hsnCode="
        + hsnCode
        + ", productPrice="
        + productPrice
        + ", discountAmount="
        + discountAmount
        + ", cgstPercentage="
        + cgstPercentage
        + ", sgstPercentage="
        + sgstPercentage
        + ", IgstPercentage="
        + IgstPercentage
        + ", time="
        + time
        + ", totalAmount="
        + totalAmount
        + ", taxableAmount="
        + taxableAmount
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + ", instructions="
        + instructions
        + "]";
  }
}
