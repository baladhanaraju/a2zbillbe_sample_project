package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "reorder_branch", schema = "product")
public class ReOrderBranch implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "reorder_branch_id_seq")
  @SequenceGenerator(
      name = "reorder_branch_id_seq",
      sequenceName = "reorder_branch_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "invoice_branch_id", unique = true, nullable = true)
  private InvoiceBranchDetails invoiceBranchDetails;

  @Column(name = "order_date")
  private Date orderDate;

  @Column(name = "delivery_date")
  private Date deliveryDate;

  @Column(name = "status")
  private String status;

  @Column(name = "quantity")
  private long quantity;

  @Column(name = "pending_quantity")
  private long pendingquantity;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public ReOrderBranch() {}

  public ReOrderBranch(
      long id,
      Product product,
      InvoiceBranchDetails invoiceBranchDetails,
      Date orderDate,
      Date deliveryDate,
      String status,
      long quantity,
      long pendingquantity,
      Organisation organisation,
      Branch branch) {
    super();
    this.id = id;
    this.product = product;
    this.invoiceBranchDetails = invoiceBranchDetails;
    this.orderDate = orderDate;
    this.deliveryDate = deliveryDate;
    this.status = status;
    this.quantity = quantity;
    this.pendingquantity = pendingquantity;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public InvoiceBranchDetails getInvoiceBranchDetails() {
    return invoiceBranchDetails;
  }

  public void setInvoiceBranchDetails(InvoiceBranchDetails invoiceBranchDetails) {
    this.invoiceBranchDetails = invoiceBranchDetails;
  }

  public Date getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(Date orderDate) {
    this.orderDate = orderDate;
  }

  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public long getQuantity() {
    return quantity;
  }

  public void setQuantity(long quantity) {
    this.quantity = quantity;
  }

  public long getPendingquantity() {
    return pendingquantity;
  }

  public void setPendingquantity(long pendingquantity) {
    this.pendingquantity = pendingquantity;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  @Override
  public String toString() {
    return "ReOrderBranch [id="
        + id
        + ", product="
        + product
        + ", invoiceBranchDetails="
        + invoiceBranchDetails
        + ", orderDate="
        + orderDate
        + ", deliveryDate="
        + deliveryDate
        + ", status="
        + status
        + ", quantity="
        + quantity
        + ", pendingquantity="
        + pendingquantity
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + "]";
  }
}
