/*package com.a2zbill.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@SuppressWarnings("serial")
@Entity
@Table(name = "size_variations", schema = "product")
public class SizeVariations implements Serializable {

	private long id;

	private String name;

	public SizeVariations() {

	}

	public SizeVariations(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "size_variation_id_seq")
	@SequenceGenerator(name = "size_variation_id_seq", sequenceName = "size_variation_id_seq", allocationSize = 1, schema = "product")
	@Column(name = "id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "name", length = 150)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "SizeVariations [id=" + id + ", name=" + name + "]";
	}

}
*/
