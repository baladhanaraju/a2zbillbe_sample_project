package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "invoice_branch", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
public class InvoiceBranchDetails implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "invoice_branch_id_seq")
  @SequenceGenerator(
      name = "invoice_branch_id_seq",
      sequenceName = "invoice_branch_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "start_date")
  private Date startDate;

  @Column(name = "end_date")
  private Date endDate;

  @Column(name = "status_flag")
  private String status;

  @Column(name = "reason")
  private String reason;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public InvoiceBranchDetails() {}

  public InvoiceBranchDetails(
      long id,
      Date startDate,
      Date endDate,
      String status,
      String reason,
      Organisation organisation,
      Branch branch) {
    super();
    this.id = id;
    this.startDate = startDate;
    this.endDate = endDate;
    this.status = status;
    this.reason = reason;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  @Override
  public String toString() {
    return "InvoiceBranchDetails [id="
        + id
        + ", startDate="
        + startDate
        + ", endDate="
        + endDate
        + ", status="
        + status
        + ", reason="
        + reason
        + ", organisation="
        + organisation
        + ", branch="
        + branch
        + "]";
  }
}
