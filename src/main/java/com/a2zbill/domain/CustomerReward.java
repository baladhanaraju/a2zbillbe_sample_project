package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Customer;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "customer_rewards", schema = "customer")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerReward implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "customer_reward_id_seq")
  @SequenceGenerator(
      name = "customer_reward_id_seq",
      sequenceName = "customer_reward_id_seq",
      allocationSize = 1,
      schema = "customer")
  @Column(name = "id")
  private long id;

  @Column(name = "reward_points")
  private long rewardPoints;

  @Column(name = "redeem_amount")
  private long redeemAmount;

  @Column(name = "create_date")
  private Date createDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "cust_id", insertable = true, updatable = true)
  private Customer customer;

  public CustomerReward() {}

  public CustomerReward(
      long id,
      long rewardPoints,
      long redeemAmount,
      Date createDate,
      Date modifiedDate,
      Customer customer) {
    super();
    this.id = id;
    this.rewardPoints = rewardPoints;
    this.redeemAmount = redeemAmount;
    this.createDate = createDate;
    this.modifiedDate = modifiedDate;
    this.customer = customer;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getRewardPoints() {
    return this.rewardPoints;
  }

  public void setRewardPoints(long rewardPoints) {
    this.rewardPoints = rewardPoints;
  }

  public long getRedeemAmount() {
    return this.redeemAmount;
  }

  public void setRedeemAmount(long redeemAmount) {
    this.redeemAmount = redeemAmount;
  }

  public Date getCreateDate() {
    return this.createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Customer getCustomer() {
    return this.customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  @Override
  public String toString() {
    return "CustomerReward [id="
        + id
        + ", rewardPoints="
        + rewardPoints
        + ", redeemAmount="
        + redeemAmount
        + ", createDate="
        + createDate
        + ", modifiedDate="
        + modifiedDate
        + ", customer="
        + customer
        + "]";
  }
}
