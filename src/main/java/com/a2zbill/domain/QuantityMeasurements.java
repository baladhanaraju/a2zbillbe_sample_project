package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "quantity_measurements", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class QuantityMeasurements implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "quantity_measurements_id_seq")
  @SequenceGenerator(
      name = "quantity_measurements_id_seq",
      sequenceName = "quantity_measurements_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "quantity_name")
  private String quantity;

  @Column(name = "display_name")
  private String displayName;

  @Column(name = "basic_unit")
  private String basicUnit;

  @Column(name = "basicunit_dispaly_name")
  private String basicunitDisplayname;

  @Column(name = "conversion_factor")
  private long conversionFactor;

  public QuantityMeasurements() {}

  public QuantityMeasurements(
      long id,
      String quantity,
      String displayName,
      String basicUnit,
      String basicunitDisplayname,
      long conversionFactor) {
    super();
    this.id = id;
    this.quantity = quantity;
    this.displayName = displayName;
    this.basicUnit = basicUnit;
    this.basicunitDisplayname = basicunitDisplayname;
    this.conversionFactor = conversionFactor;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getQuantity() {
    return this.quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getBasicUnit() {
    return basicUnit;
  }

  public void setBasicUnit(String basicUnit) {
    this.basicUnit = basicUnit;
  }

  public String getBasicunitDisplayname() {
    return basicunitDisplayname;
  }

  public void setBasicunitDisplayname(String basicunitDisplayname) {
    this.basicunitDisplayname = basicunitDisplayname;
  }

  public long getConversionFactor() {
    return conversionFactor;
  }

  public void setConversionFactor(long conversionFactor) {
    this.conversionFactor = conversionFactor;
  }

  @Override
  public String toString() {
    return "QuantityMeasurements [id="
        + id
        + ", quantity="
        + quantity
        + ", displayName="
        + displayName
        + ", basicUnit="
        + basicUnit
        + ", basicunitDisplayname="
        + basicunitDisplayname
        + ", conversionFactor="
        + conversionFactor
        + "]";
  }
}
