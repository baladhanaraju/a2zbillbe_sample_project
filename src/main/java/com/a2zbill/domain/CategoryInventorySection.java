package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "category_inventory_section", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class CategoryInventorySection implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "category_inventory_section_id_seq")
  @SequenceGenerator(
      name = "category_inventory_section_id_seq",
      sequenceName = "category_inventory_section_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "inv_section_id", insertable = true, updatable = true)
  private InventorySection inventorySection;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "category_id", unique = true, nullable = true)
  private ProductCategory categoryId;

  public CategoryInventorySection() {}

  public CategoryInventorySection(
      long id, InventorySection inventorySection, ProductCategory categoryId) {
    super();
    this.id = id;
    this.inventorySection = inventorySection;
    this.categoryId = categoryId;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public InventorySection getInventorySection() {
    return this.inventorySection;
  }

  public void setInventorySection(final InventorySection inventorySection) {
    this.inventorySection = inventorySection;
  }

  public ProductCategory getCategoryId() {
    return this.categoryId;
  }

  public void setCategoryId(final ProductCategory categoryId) {
    this.categoryId = categoryId;
  }

  @Override
  public String toString() {
    return "CategoryInventorySection [id="
        + id
        + ", inventorySection="
        + inventorySection
        + ", categoryId="
        + categoryId
        + "]";
  }
}
