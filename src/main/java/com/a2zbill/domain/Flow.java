package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "flow", schema = "product")
public class Flow implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "flow_id_seq")
  @SequenceGenerator(
      name = "flow_id_seq",
      sequenceName = "flow_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "flow_name")
  private String flowName;

  @Column(name = "flow_ranking")
  private long flowRanking;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "flow_type_id", unique = true, nullable = true)
  private FlowType flowType;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  public Flow() {}

  public Flow(
      final long id,
      final String flowName,
      final long flowRanking,
      final FlowType flowType,
      final Organisation organisation) {
    super();
    this.id = id;
    this.flowName = flowName;
    this.flowRanking = flowRanking;
    this.flowType = flowType;
    this.organisation = organisation;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getFlowName() {
    return this.flowName;
  }

  public void setFlowName(final String flowName) {
    this.flowName = flowName;
  }

  public long getFlowRanking() {
    return this.flowRanking;
  }

  public void setFlowRanking(final long flowRanking) {
    this.flowRanking = flowRanking;
  }

  public FlowType getFlowType() {
    return this.flowType;
  }

  public void setFlowType(final FlowType flowType) {
    this.flowType = flowType;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }
}
