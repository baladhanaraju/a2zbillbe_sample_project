package com.a2zbill.domain;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@SuppressWarnings("serial")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "form_type", schema = "form")
public class FormType implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "formtype_id_seq")
  @SequenceGenerator(
      name = "formtype_id_seq",
      sequenceName = "formtype_id_seq",
      allocationSize = 1,
      schema = "form")
  @Column(name = "id")
  private long id;

  @Column(name = "form_type", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> formtype;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "form_name")
  private String formname;

  public FormType() {}

  public FormType(
      final long id,
      final Map<String, Object> formtype,
      final Date createdDate,
      final Date modifiedDate,
      final String formname) {
    super();
    this.id = id;
    this.formtype = formtype;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.formname = formname;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Map<String, Object> getFormtype() {
    return formtype;
  }

  public void setFormtype(final Map<String, Object> formtype) {
    this.formtype = formtype;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getFormname() {
    return this.formname;
  }

  public void setFormname(final String formname) {
    this.formname = formname;
  }
}
