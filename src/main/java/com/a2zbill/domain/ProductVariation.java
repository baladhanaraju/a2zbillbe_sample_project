package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "product_variation", schema = "product")
public class ProductVariation {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_variation_id_seq")
  @SequenceGenerator(
      name = "product_variation_id_seq",
      sequenceName = "product_variation_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_id")
  private Product product;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "variation_options_id")
  private VariationOptions variationOption;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "variation_type_id")
  private VariationTypes variationType;

  @Column(name = "price")
  private BigDecimal price;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "branch_id", nullable = true)
  private Branch branch;

  @Column(name = "description")
  private String description;

  @Column(name = "actual_weight")
  private BigDecimal actualWeight;

  @Column(name = "code")
  private String code;

  public ProductVariation() {}

  public ProductVariation(
      long id,
      Product product,
      VariationOptions variationOption,
      VariationTypes variationType,
      BigDecimal price,
      Organisation organisation,
      Branch branch,
      String description,
      BigDecimal actualWeight,
      String code) {
    super();
    this.id = id;
    this.product = product;
    this.variationOption = variationOption;
    this.variationType = variationType;
    this.price = price;
    this.organisation = organisation;
    this.branch = branch;
    this.description = description;
    this.actualWeight = actualWeight;
    this.code = code;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public VariationOptions getVariationOption() {
    return variationOption;
  }

  public void setVariationOption(VariationOptions variationOption) {
    this.variationOption = variationOption;
  }

  public VariationTypes getVariationType() {
    return variationType;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setVariationType(VariationTypes variationType) {
    this.variationType = variationType;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getActualWeight() {
    return actualWeight;
  }

  public void setActualWeight(BigDecimal actualWeight) {
    this.actualWeight = actualWeight;
  }
}
