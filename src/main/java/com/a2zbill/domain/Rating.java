package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsss.basic.domain.Employee;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "rating", schema = "customer")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rating implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "rating_id_seq")
  @SequenceGenerator(
      name = "rating_id_seq",
      sequenceName = "rating_id_seq",
      allocationSize = 1,
      schema = "customer")
  @Column(name = "id")
  private long id;

  @Column(name = "rating_status")
  private boolean ratingStatus;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "emp_id", insertable = true, updatable = true)
  private Employee empDetails;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = true, updatable = true)
  private Product product;

  public Rating() {}

  public Rating(long id, boolean ratingStatus, Employee empDetails, Product product) {
    super();
    this.id = id;
    this.ratingStatus = ratingStatus;
    this.empDetails = empDetails;
    this.product = product;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public boolean isRatingStatus() {
    return this.ratingStatus;
  }

  public void setRatingStatus(boolean ratingStatus) {
    this.ratingStatus = ratingStatus;
  }

  public Product getProduct() {
    return this.product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Employee getEmpDetails() {
    return this.empDetails;
  }

  public void setEmpDetails(Employee empDetails) {
    this.empDetails = empDetails;
  }
}
