package com.a2zbill.domain;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.io.Serializable;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "suggested_content", schema = "product")
public class SuggestedContent implements Serializable {

  private static final long serialVersionUID = 1L;

  private long id;

  private String contentType;

  private Map<String, Object> contentData;

  public SuggestedContent() {}

  public SuggestedContent(
      final long id, final String contentType, final Map<String, Object> contentData) {
    super();
    this.id = id;
    this.contentType = contentType;
    this.contentData = contentData;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "suggested_content_id_seq")
  @SequenceGenerator(
      name = "suggested_content_id_seq",
      sequenceName = "suggested_content_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id", nullable = false)
  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  @Column(name = "content_type")
  public String getContentType() {
    return contentType;
  }

  public void setContentType(final String contentType) {
    this.contentType = contentType;
  }

  @Column(name = "content_data", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  public Map<String, Object> getContentData() {
    return contentData;
  }

  public void setContentData(final Map<String, Object> contentData) {
    this.contentData = contentData;
  }
}
