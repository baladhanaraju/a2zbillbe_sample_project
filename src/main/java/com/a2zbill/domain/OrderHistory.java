package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "orders_history", schema = "orders")
public class OrderHistory implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "orders_history_id_seq")
  @SequenceGenerator(
      name = "orders_history_id_seq",
      sequenceName = "orders_history_id_seq",
      allocationSize = 1,
      schema = "orders")
  @Column(name = "id")
  private long id;

  @Column(name = "order_number")
  private String orderNumber;

  @Column(name = "delivery_status_time")
  private Date deliveryStatusTime;

  @Column(name = "reason")
  private String reason;

  @Column(name = "created_ts")
  private Date createdTs;

  @Column(name = "modified_ts")
  private Date modifiedTs;

  @Column(name = "db_created_ts")
  private Date DbcreatedTs;

  @Column(name = "db_modified_ts")
  private Date DbmodifiedTs;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "order_id", insertable = true, updatable = true)
  private Order order;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "order_status_id", insertable = true, updatable = true)
  private OrderStatus orderStatus;

  public OrderHistory() {}

  public OrderHistory(
      long id,
      String orderNumber,
      Date deliveryStatusTime,
      String reason,
      Date createdTs,
      Date modifiedTs,
      Date dbcreatedTs,
      Date dbmodifiedTs,
      Order order,
      Organisation organisation,
      Branch branch,
      OrderStatus orderStatus) {
    super();
    this.id = id;
    this.orderNumber = orderNumber;
    this.deliveryStatusTime = deliveryStatusTime;
    this.reason = reason;
    this.createdTs = createdTs;
    this.modifiedTs = modifiedTs;
    DbcreatedTs = dbcreatedTs;
    DbmodifiedTs = dbmodifiedTs;
    this.order = order;
    this.organisation = organisation;
    this.branch = branch;
    this.orderStatus = orderStatus;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public Date getDeliveryStatusTime() {
    return deliveryStatusTime;
  }

  public void setDeliveryStatusTime(Date deliveryStatusTime) {
    this.deliveryStatusTime = deliveryStatusTime;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Date getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Date createdTs) {
    this.createdTs = createdTs;
  }

  public Date getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Date modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Date getDbcreatedTs() {
    return DbcreatedTs;
  }

  public void setDbcreatedTs(Date dbcreatedTs) {
    DbcreatedTs = dbcreatedTs;
  }

  public Date getDbmodifiedTs() {
    return DbmodifiedTs;
  }

  public void setDbmodifiedTs(Date dbmodifiedTs) {
    DbmodifiedTs = dbmodifiedTs;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }
}
