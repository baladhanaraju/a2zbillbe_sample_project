package com.a2zbill.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "gst_percentage", schema = "gst")
@Proxy(lazy = false)
public class Percentage implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id")
  private long percentageId;

  /** The CGST. */
  @Column(name = "cgst")
  private BigDecimal CGST;

  /** The SGST. */
  @Column(name = "sgst")
  private BigDecimal SGST;

  /** The IGST. */
  @Column(name = "igst")
  private BigDecimal IGST;

  @Column(name = "gst")
  private BigDecimal GST;

  public Percentage() {}

  public Percentage(
      final long percentageId,
      final BigDecimal cGST,
      final BigDecimal sGST,
      final BigDecimal iGST,
      final BigDecimal gST) {
    super();
    this.percentageId = percentageId;
    this.CGST = cGST;
    this.SGST = sGST;
    this.IGST = iGST;
    this.GST = gST;
  }

  public long getPercentageId() {
    return this.percentageId;
  }

  public void setPercentageId(final long percentageId) {
    this.percentageId = percentageId;
  }

  public BigDecimal getCGST() {
    return this.CGST;
  }

  public void setCGST(final BigDecimal cGST) {
    this.CGST = cGST;
  }

  public BigDecimal getSGST() {
    return this.SGST;
  }

  public void setSGST(final BigDecimal sGST) {
    this.SGST = sGST;
  }

  public BigDecimal getIGST() {
    return this.IGST;
  }

  public void setIGST(final BigDecimal iGST) {
    this.IGST = iGST;
  }

  public BigDecimal getGST() {
    return this.GST;
  }

  public void setGST(final BigDecimal gST) {
    this.GST = gST;
  }
}
