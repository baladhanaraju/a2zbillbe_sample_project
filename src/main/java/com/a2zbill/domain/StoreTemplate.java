package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "store_template", schema = "gst")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class StoreTemplate implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "store_template_id_seq")
  @SequenceGenerator(
      name = "store_template_id_seq",
      sequenceName = "store_template_id_seq",
      allocationSize = 1,
      schema = "gst")
  @Column(name = "id")
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "sgst")
  private BigDecimal SGST;

  @Column(name = "cgst")
  private BigDecimal CGST;

  @Column(name = "igst")
  private BigDecimal IGST;

  @Column(name = "input_tax_credit_flag")
  private boolean inputTaxCreditFlag;

  @Column(name = "zero_percent_tax")
  private boolean zeroPercentTax;

  @Column(name = "description")
  private String description;

  @Column(name = "created_date")
  private Date createdDate;

  @Column(name = "modified_date")
  private Date modifiedDate;

  @Column(name = "template_type")
  private String templateType;

  @Column(name = "type")
  private String type;

  @Column(name = "category_type")
  @Type(type = "GenericArrayUserType")
  private Integer[] categoryType;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "store_type_id", insertable = true, updatable = true)
  private StoreType storeType;

  public StoreTemplate() {}

  public StoreTemplate(
      final long id,
      final String name,
      final BigDecimal sGST,
      final BigDecimal cGST,
      final BigDecimal iGST,
      final boolean inputTaxCreditFlag,
      final boolean zeroPercentTax,
      final String description,
      final Date createdDate,
      final Date modifiedDate,
      final String templateType,
      final String type,
      final Integer[] categoryType,
      final StoreType storeType) {
    super();
    this.id = id;
    this.name = name;
    SGST = sGST;
    CGST = cGST;
    IGST = iGST;
    this.inputTaxCreditFlag = inputTaxCreditFlag;
    this.zeroPercentTax = zeroPercentTax;
    this.description = description;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.templateType = templateType;
    this.type = type;
    this.categoryType = categoryType;
    this.storeType = storeType;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public BigDecimal getSGST() {
    return this.SGST;
  }

  public void setSGST(final BigDecimal sGST) {
    this.SGST = sGST;
  }

  public BigDecimal getCGST() {
    return this.CGST;
  }

  public void setCGST(final BigDecimal cGST) {
    this.CGST = cGST;
  }

  public BigDecimal getIGST() {
    return this.IGST;
  }

  public void setIGST(final BigDecimal iGST) {
    this.IGST = iGST;
  }

  public boolean isInputTaxCreditFlag() {
    return this.inputTaxCreditFlag;
  }

  public void setInputTaxCreditFlag(final boolean inputTaxCreditFlag) {
    this.inputTaxCreditFlag = inputTaxCreditFlag;
  }

  public boolean isZeroPercentTax() {
    return this.zeroPercentTax;
  }

  public void setZeroPercentTax(final boolean zeroPercentTax) {
    this.zeroPercentTax = zeroPercentTax;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public void setCreatedDate(final Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(final Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getTemplateType() {
    return this.templateType;
  }

  public void setTemplateType(final String templateType) {
    this.templateType = templateType;
  }

  public String getType() {
    return type;
  }

  public void setType(final String type) {
    this.type = type;
  }

  public Integer[] getCategoryType() {
    return categoryType;
  }

  public void setCategoryType(final Integer[] categoryType) {
    this.categoryType = categoryType;
  }

  public StoreType getStoreType() {
    return storeType;
  }

  public void setStoreType(final StoreType storeType) {
    this.storeType = storeType;
  }

  @Override
  public String toString() {
    return "StoreTemplate [id="
        + id
        + ", name="
        + name
        + ", SGST="
        + SGST
        + ", CGST="
        + CGST
        + ", IGST="
        + IGST
        + ", inputTaxCreditFlag="
        + inputTaxCreditFlag
        + ", zeroPercentTax="
        + zeroPercentTax
        + ", description="
        + description
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", templateType="
        + templateType
        + ", type="
        + type
        + ", categoryType="
        + Arrays.toString(categoryType)
        + ", storeType="
        + storeType
        + "]";
  }
}
