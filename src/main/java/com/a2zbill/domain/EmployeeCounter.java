package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "emp_counter", schema = "product")
public class EmployeeCounter implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "employeecounter_id_seq")
  @SequenceGenerator(
      name = "employeecounter_id_seq",
      sequenceName = "employeecounter_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "emp_id", unique = true, nullable = true)
  private Employee employee;

  @Column(name = "counter_number")
  private long counter;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation orgId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branchId;

  @Column(name = "date")
  private Date date;

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Employee getEmployee() {
    return this.employee;
  }

  public void setEmployee(final Employee employee) {
    this.employee = employee;
  }

  public long getCounter() {
    return this.counter;
  }

  public void setCounter(final long counter) {
    this.counter = counter;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public Organisation getOrgId() {
    return this.orgId;
  }

  public void setOrgId(final Organisation orgId) {
    this.orgId = orgId;
  }

  public Branch getBranchId() {
    return this.branchId;
  }

  public void setBranchId(final Branch branchId) {
    this.branchId = branchId;
  }

  @Override
  public String toString() {
    return "EmployeeCounter [id="
        + id
        + ", employee="
        + employee
        + ", counter="
        + counter
        + ", orgId="
        + orgId
        + ", branchId="
        + branchId
        + ", date="
        + date
        + "]";
  }
}
