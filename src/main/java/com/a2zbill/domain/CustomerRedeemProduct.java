package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "customer_redeem_product", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = true)
@Proxy(lazy = false)
public class CustomerRedeemProduct implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "customer_redeem_product_id_seq")
  @SequenceGenerator(
      name = "customer_redeem_product_id_seq",
      sequenceName = "customer_redeem_product_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  /** The startTime. */
  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customerid", unique = true, nullable = true)
  private Customer CustomerId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product productId;

  /** The startTime. */
  @Column(name = "redeem_points")
  private long redeemPoint;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation orgId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branchId;

  public CustomerRedeemProduct() {}

  public CustomerRedeemProduct(
      final long id,
      final Customer customerId,
      final Product productId,
      final long redeemPoint,
      final Organisation orgId,
      final Branch branchId) {
    super();
    this.id = id;
    this.CustomerId = customerId;
    this.productId = productId;
    this.redeemPoint = redeemPoint;
    this.orgId = orgId;
    this.branchId = branchId;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public long getRedeemPoint() {
    return this.redeemPoint;
  }

  public void setRedeemPoint(final long redeemPoint) {
    this.redeemPoint = redeemPoint;
  }

  public Product getProductId() {
    return this.productId;
  }

  public void setProductId(final Product productId) {
    this.productId = productId;
  }

  public Customer getCustomerId() {
    return this.CustomerId;
  }

  public void setCustomerId(final Customer customerId) {
    this.CustomerId = customerId;
  }

  public Organisation getOrgId() {
    return this.orgId;
  }

  public void setOrgId(final Organisation orgId) {
    this.orgId = orgId;
  }

  public Branch getBranchId() {
    return this.branchId;
  }

  public void setBranchId(final Branch branchId) {
    this.branchId = branchId;
  }

  @Override
  public String toString() {
    return "CustomerRedeemProduct [id="
        + id
        + ", CustomerId="
        + CustomerId
        + ", productId="
        + productId
        + ", redeemPoint="
        + redeemPoint
        + ", orgId="
        + orgId
        + ", branchId="
        + branchId
        + "]";
  }
}
