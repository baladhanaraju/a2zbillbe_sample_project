package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "counter", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class Counter implements Serializable {
  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "counter_id_seq")
  @SequenceGenerator(
      name = "counter_id_seq",
      sequenceName = "counter_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "counter_id")
  private long id;

  /** The counternumber. */
  @Column(name = "counter_number")
  private long counterNumber;

  /** The counterStatus. */
  @Column(name = "counter_status")
  private String counterStatus;

  @Column(name = "status")
  private boolean status;

  @Column(name = "online")
  private boolean online;

  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation orgId;

  @JsonManagedReference
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "branch_id", insertable = true, updatable = true)
  private Branch branchId;

  @Column(name = "display_Name")
  private String displayName;

  @Column(name = "max_capacity")
  private long maxCapacity;

  @JsonManagedReference
  @ManyToOne(
      fetch = FetchType.EAGER,
      cascade = {CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "section_id", insertable = true, updatable = true)
  private Section section;

  /** Instantiates a new Counter. */
  public Counter() {}

  /**
   * Instantiates a new Counter.
   *
   * @param id the id
   * @param counternumber the counternumber
   * @param counterStatus the counterStatus
   * @param cartProductDetails the cartProductDetails
   * @param cartDetails the cartDetails
   */

  /**
   * Gets the Id.
   *
   * @return the Id
   */
  public long getId() {
    return this.id;
  }

  public Counter(
      final long id,
      final long counterNumber,
      final String counterStatus,
      final boolean status,
      final boolean online,
      final Organisation orgId,
      final Branch branchId,
      final String displayName,
      final long maxCapacity,
      final Section section) {
    super();
    this.id = id;
    this.counterNumber = counterNumber;
    this.counterStatus = counterStatus;
    this.status = status;
    this.online = online;
    this.orgId = orgId;
    this.branchId = branchId;
    this.displayName = displayName;
    this.maxCapacity = maxCapacity;
    this.section = section;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(final long id) {
    this.id = id;
  }

  /**
   * Gets the counterNumber.
   *
   * @return the counterNumber
   */
  public long getCounterNumber() {
    return this.counterNumber;
  }

  /**
   * Sets the counterNumber.
   *
   * @param counterNumber the new counterNumber
   */
  public void setCounterNumber(final long counterNumber) {
    this.counterNumber = counterNumber;
  }

  /**
   * Gets the counterStatus.
   *
   * @return the counterStatus
   */
  public String getCounterStatus() {
    return this.counterStatus;
  }

  /**
   * Sets the counterStatus.
   *
   * @param counterStatus the new counterStatus
   */
  public void setCounterStatus(final String counterStatus) {
    this.counterStatus = counterStatus;
  }

  public Organisation getOrgId() {
    return this.orgId;
  }

  public void setOrgId(final Organisation orgId) {
    this.orgId = orgId;
  }

  public Branch getBranchId() {
    return this.branchId;
  }

  public void setBranchId(final Branch branchId) {
    this.branchId = branchId;
  }

  public String getDisplayName() {
    return this.displayName;
  }

  public void setDisplayName(final String displayName) {
    this.displayName = displayName;
  }

  public long getMaxCapacity() {
    return this.maxCapacity;
  }

  public void setMaxCapacity(final long maxCapacity) {
    this.maxCapacity = maxCapacity;
  }

  public Section getSection() {
    return this.section;
  }

  public void setSection(final Section section) {
    this.section = section;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(final boolean status) {
    this.status = status;
  }

  public boolean isOnline() {
    return online;
  }

  public void setOnline(final boolean online) {
    this.online = online;
  }

  @Override
  public String toString() {
    return "Counter [id="
        + id
        + ", counterNumber="
        + counterNumber
        + ", counterStatus="
        + counterStatus
        + ", status="
        + status
        + ", orgId="
        + orgId
        + ", branchId="
        + branchId
        + ", displayName="
        + displayName
        + ", maxCapacity="
        + maxCapacity
        + ", section="
        + section
        + "]";
  }
}
