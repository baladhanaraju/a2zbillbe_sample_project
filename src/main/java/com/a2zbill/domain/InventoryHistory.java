package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import com.tsss.basic.domain.Vendor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "inventory_history", schema = "product")
public class InventoryHistory implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inventoryhistory_id_seq")
  @SequenceGenerator(
      name = "inventoryhistory_id_seq",
      sequenceName = "inventoryhistory_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @Column(name = "quantity")
  private BigDecimal quantity;

  @Column(name = "sgst")
  private BigDecimal sgst;

  @Column(name = "cgst")
  private BigDecimal cgst;

  @Column(name = "igst")
  private BigDecimal igst;

  @Column(name = "buying_price")
  private BigDecimal buyingPrice;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
  @JoinColumn(name = "invoice_bill_id", insertable = true, updatable = true)
  private InvoiceBill invoiceBillId;

  @Temporal(TemporalType.DATE)
  @Column(name = "date")
  private Date inventoryHistoryDate;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
  @JoinColumn(name = "inventory_id", insertable = true, updatable = true)
  private InventoryDetail inventoryId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
  @JoinColumn(name = "vendor_id", insertable = true, updatable = true)
  private Vendor vendorId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @Column(name = "batch_number")
  private String batchNumber;

  @Temporal(TemporalType.DATE)
  @Column(name = "expiry_date")
  private Date expiryDate;

  @Column(name = "consumed_count")
  private BigDecimal consumedCount;

  @Column(name = "expiry_count")
  private BigDecimal expiryCount;

  public InventoryHistory() {}

  public InventoryHistory(
      long id,
      BigDecimal quantity,
      BigDecimal sgst,
      BigDecimal cgst,
      BigDecimal igst,
      BigDecimal buyingPrice,
      InvoiceBill invoiceBillId,
      Date inventoryHistoryDate,
      InventoryDetail inventoryId,
      Vendor vendorId,
      Branch branch,
      Organisation organisation,
      String batchNumber,
      Date expiryDate,
      BigDecimal consumedCount,
      BigDecimal expiryCount) {
    super();
    this.id = id;
    this.quantity = quantity;
    this.sgst = sgst;
    this.cgst = cgst;
    this.igst = igst;
    this.buyingPrice = buyingPrice;
    this.invoiceBillId = invoiceBillId;
    this.inventoryHistoryDate = inventoryHistoryDate;
    this.inventoryId = inventoryId;
    this.vendorId = vendorId;
    this.branch = branch;
    this.organisation = organisation;
    this.batchNumber = batchNumber;
    this.expiryDate = expiryDate;
    this.consumedCount = consumedCount;
    this.expiryCount = expiryCount;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public Date getInventoryHistoryDate() {
    return this.inventoryHistoryDate;
  }

  public void setInventoryHistoryDate(Date inventoryHistoryDate) {
    this.inventoryHistoryDate = inventoryHistoryDate;
  }

  public InventoryDetail getInventoryId() {
    return this.inventoryId;
  }

  public void setInventoryId(InventoryDetail inventoryId) {
    this.inventoryId = inventoryId;
  }

  public Vendor getVendorId() {
    return this.vendorId;
  }

  public void setVendorId(Vendor vendorId) {
    this.vendorId = vendorId;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(Organisation organisation) {
    this.organisation = organisation;
  }

  public BigDecimal getSgst() {
    return sgst;
  }

  public void setSgst(BigDecimal sgst) {
    this.sgst = sgst;
  }

  public BigDecimal getCgst() {
    return cgst;
  }

  public void setCgst(BigDecimal cgst) {
    this.cgst = cgst;
  }

  public BigDecimal getIgst() {
    return igst;
  }

  public void setIgst(BigDecimal igst) {
    this.igst = igst;
  }

  public BigDecimal getBuyingPrice() {
    return buyingPrice;
  }

  public void setBuyingPrice(BigDecimal buyingPrice) {
    this.buyingPrice = buyingPrice;
  }

  public String getBatchNumber() {
    return batchNumber;
  }

  public void setBatchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
  }

  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  public BigDecimal getConsumedCount() {
    return consumedCount;
  }

  public void setConsumedCount(BigDecimal consumedCount) {
    this.consumedCount = consumedCount;
  }

  public BigDecimal getExpiryCount() {
    return expiryCount;
  }

  public void setExpiryCount(BigDecimal expiryCount) {
    this.expiryCount = expiryCount;
  }

  public InvoiceBill getInvoiceBillId() {
    return invoiceBillId;
  }

  public void setInvoiceBillId(InvoiceBill invoiceBillId) {
    this.invoiceBillId = invoiceBillId;
  }

  @Override
  public String toString() {
    return "InventoryHistory [id="
        + id
        + ", quantity="
        + quantity
        + ", sgst="
        + sgst
        + ", cgst="
        + cgst
        + ", igst="
        + igst
        + ", buyingPrice="
        + buyingPrice
        + ", invoiceBillId="
        + invoiceBillId
        + ", inventoryHistoryDate="
        + inventoryHistoryDate
        + ", inventoryId="
        + inventoryId
        + ", vendorId="
        + vendorId
        + ", branch="
        + branch
        + ", organisation="
        + organisation
        + ", batchNumber="
        + batchNumber
        + ", expiryDate="
        + expiryDate
        + ", consumedCount="
        + consumedCount
        + ", expiryCount="
        + expiryCount
        + "]";
  }
}
