package com.a2zbill.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "order_status", schema = "orders")
public class OrderStatus implements Serializable {

  private long id;

  private String OrderStatus;

  private boolean notificationFlag;

  public OrderStatus() {}

  public OrderStatus(long id, String orderStatus, boolean notificationFlag) {
    super();
    this.id = id;
    OrderStatus = orderStatus;
    this.notificationFlag = notificationFlag;
  }

  @Id
  @Column(name = "id")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Column(name = "status")
  public String getOrderStatus() {
    return OrderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    OrderStatus = orderStatus;
  }

  @Column(name = "notification_flag")
  public boolean isNotificationFlag() {
    return notificationFlag;
  }

  public void setNotificationFlag(boolean notificationFlag) {
    this.notificationFlag = notificationFlag;
  }
}
