package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Customer;
import com.tsss.basic.domain.Employee;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;

@SuppressWarnings("serial")
@Entity
@Table(name = "orders", schema = "orders")
@JsonIgnoreProperties(ignoreUnknown = true)
@Proxy(lazy = false)
public class Order implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "orders_id_seq")
  @SequenceGenerator(
      name = "orders_id_seq",
      sequenceName = "orders_id_seq",
      allocationSize = 1,
      schema = "orders")
  @Column(name = "id")
  private long id;

  @Column(name = "guid")
  private String guid;

  @Column(name = "order_date")
  private Date Orderdate;

  @Column(name = "created_ts")
  private Date createdTs;

  @Column(name = "modified_ts")
  private Date modifiedTs;

  @Column(name = "db_created_ts")
  private Date DbcreatedTs;

  @Column(name = "db_modified_ts")
  private Date DbmodifiedTs;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "employee_id", insertable = true, updatable = true)
  private Employee employeeId;

  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "counter_id", insertable = true, updatable = true)
  private Counter counterDetails;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", insertable = true, updatable = true)
  private Customer customer;

  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
  @JoinColumn(name = "order_status_id", unique = true, nullable = true)
  private OrderStatus orderStatus;

  @Column(name = "delivery_address", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> deliveryAddress;

  @Column(name = "order_location_address", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> orderlocationAddress;

  @Column(name = "order_earning_amount ")
  private BigDecimal orderearningAmount;

  @JsonManagedReference
  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
  @JsonIgnore
  @JoinColumn(name = "org_id", insertable = true, updatable = true)
  private Organisation organisation;

  @JsonManagedReference
  @JsonIgnore
  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  public Order() {}

  public Order(
      final long id,
      final String guid,
      final Date orderdate,
      final OrderStatus orderStatus,
      final Date createdTs,
      final Date modifiedTs,
      final Date dbcreatedTs,
      final Date dbmodifiedTs,
      final Employee employeeId,
      final Counter counterDetails,
      final Customer customer,
      final Map<String, Object> deliveryAddress,
      final Map<String, Object> orderlocationAddress,
      final BigDecimal orderearningAmount,
      final Organisation organisation,
      final Branch branch) {
    super();
    this.id = id;
    this.guid = guid;
    Orderdate = orderdate;
    this.orderStatus = orderStatus;
    this.createdTs = createdTs;
    this.modifiedTs = modifiedTs;
    DbcreatedTs = dbcreatedTs;
    DbmodifiedTs = dbmodifiedTs;
    this.employeeId = employeeId;
    this.counterDetails = counterDetails;
    this.customer = customer;
    this.deliveryAddress = deliveryAddress;
    this.orderlocationAddress = orderlocationAddress;
    this.orderearningAmount = orderearningAmount;
    this.organisation = organisation;
    this.branch = branch;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getGuid() {
    return guid;
  }

  public Organisation getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public Branch getBranch() {
    return branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public void setGuid(final String guid) {
    this.guid = guid;
  }

  public Date getOrderdate() {
    return Orderdate;
  }

  public void setOrderdate(final Date orderdate) {
    Orderdate = orderdate;
  }

  public Date getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(final Date createdTs) {
    this.createdTs = createdTs;
  }

  public Date getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(final Date modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Date getDbcreatedTs() {
    return DbcreatedTs;
  }

  public void setDbcreatedTs(final Date dbcreatedTs) {
    DbcreatedTs = dbcreatedTs;
  }

  public Date getDbmodifiedTs() {
    return DbmodifiedTs;
  }

  public void setDbmodifiedTs(final Date dbmodifiedTs) {
    DbmodifiedTs = dbmodifiedTs;
  }

  public Employee getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(final Employee employeeId) {
    this.employeeId = employeeId;
  }

  public Counter getCounterDetails() {
    return counterDetails;
  }

  public void setCounterDetails(final Counter counterDetails) {
    this.counterDetails = counterDetails;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(final Customer customer) {
    this.customer = customer;
  }

  public Map<String, Object> getDeliveryAddress() {
    return deliveryAddress;
  }

  public void setDeliveryAddress(final Map<String, Object> deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }

  public Map<String, Object> getOrderlocationAddress() {
    return orderlocationAddress;
  }

  public void setOrderlocationAddress(final Map<String, Object> orderlocationAddress) {
    this.orderlocationAddress = orderlocationAddress;
  }

  public BigDecimal getOrderearningAmount() {
    return orderearningAmount;
  }

  public void setOrderearningAmount(final BigDecimal orderearningAmount) {
    this.orderearningAmount = orderearningAmount;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }
}
