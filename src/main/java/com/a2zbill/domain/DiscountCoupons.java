package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.offers.domain.DiscountType;
import com.tsss.basic.domain.Customer;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "discount_coupons", schema = "product")
public class DiscountCoupons {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "discount_coupons_id_seq")
  @SequenceGenerator(
      name = "discount_coupons_id_seq",
      sequenceName = "discount_coupons_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id", nullable = false)
  private long id;

  @Column(name = "deal_Name", columnDefinition = "jsonb")
  @Type(type = "jsonb")
  private Map<String, Object> dealName;

  @Column(name = "sold_items")
  private long soldItems;

  @Column(name = "free_items")
  private long freeItems;

  @Temporal(TemporalType.DATE)
  @Column(name = "start_time")
  private Date startTime;

  @Temporal(TemporalType.DATE)
  @Column(name = "end_time")
  private Date endTime;

  @Column(name = "discount_amount")
  private BigDecimal discountAmount;

  @Column(name = "percentage", length = 150)
  private String percentage;

  @Column(name = "offer_code", length = 100)
  private String offerCode;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "discount_type_id", unique = true, nullable = true)
  private DiscountType discountType;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "cust_id", unique = true, nullable = true)
  private Customer customer;

  public DiscountCoupons() {}

  public DiscountCoupons(
      final long id,
      final Map<String, Object> dealName,
      final long soldItems,
      final long freeItems,
      final Date startTime,
      final Date endTime,
      final BigDecimal discountAmount,
      final String percentage,
      final String offerCode,
      final DiscountType discountType,
      Customer customer) {
    super();
    this.id = id;
    this.dealName = dealName;
    this.soldItems = soldItems;
    this.freeItems = freeItems;
    this.startTime = startTime;
    this.endTime = endTime;
    this.discountAmount = discountAmount;
    this.percentage = percentage;
    this.offerCode = offerCode;
    this.discountType = discountType;
    this.customer = customer;
  }

  public String getOfferCode() {
    return this.offerCode;
  }

  public void setOfferCode(final String offerCode) {
    this.offerCode = offerCode;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Map<String, Object> getDealName() {
    return dealName;
  }

  public void setDealName(final Map<String, Object> dealName) {
    this.dealName = dealName;
  }

  public long getSoldItems() {
    return this.soldItems;
  }

  public void setSoldItems(final long soldItems) {
    this.soldItems = soldItems;
  }

  public long getFreeItems() {
    return this.freeItems;
  }

  public void setFreeItems(final long freeItems) {
    this.freeItems = freeItems;
  }

  public Date getStartTime() {
    return this.startTime;
  }

  public void setStartTime(final Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return this.endTime;
  }

  public void setEndTime(final Date endTime) {
    this.endTime = endTime;
  }

  public BigDecimal getDiscountAmount() {
    return this.discountAmount;
  }

  public void setDiscountAmount(final BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }

  public String getPercentage() {
    return this.percentage;
  }

  public void setPercentage(final String percentage) {
    this.percentage = percentage;
  }

  public DiscountType getDiscountType() {
    return this.discountType;
  }

  public void setDiscountType(final DiscountType discountType) {
    this.discountType = discountType;
  }

  public Customer getCustomer() {
    return this.customer;
  }

  public void setCustomer(final Customer customer) {
    this.customer = customer;
  }
}
