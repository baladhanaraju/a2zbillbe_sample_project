package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "inventory_audit", schema = "product")
public class InventoryAudit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "inventory_audit_id_seq")
  @SequenceGenerator(
      name = "inventory_audit_id_seq",
      sequenceName = "inventoryaudit_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private int id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product product;

  @Column(name = "product_name")
  private String productName;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branch;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation organisation;

  @Column(name = "inventory_quantity")
  private BigDecimal inventoryQuantity;

  @Column(name = "physical_quantity")
  private BigDecimal physicalQuantity;

  @Column(name = "reason")
  private String reason;

  @Column(name = "difference")
  private BigDecimal difference;

  @Column(name = "date")
  private Date date;

  public InventoryAudit() {}

  public InventoryAudit(
      final int id,
      final Product product,
      final String productName,
      final Branch branch,
      final Organisation organisation,
      final BigDecimal inventoryQuantity,
      final BigDecimal physicalQuantity,
      final String reason,
      final BigDecimal difference,
      final Date date) {
    super();
    this.id = id;
    this.product = product;
    this.productName = productName;
    this.branch = branch;
    this.organisation = organisation;
    this.inventoryQuantity = inventoryQuantity;
    this.physicalQuantity = physicalQuantity;
    this.reason = reason;
    this.difference = difference;
    this.date = date;
  }

  public int getId() {
    return this.id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public Product getProduct() {
    return this.product;
  }

  public void setProduct(final Product product) {
    this.product = product;
  }

  public String getProductName() {
    return this.productName;
  }

  public void setProductName(final String productName) {
    this.productName = productName;
  }

  public Branch getBranch() {
    return this.branch;
  }

  public void setBranch(final Branch branch) {
    this.branch = branch;
  }

  public Organisation getOrganisation() {
    return this.organisation;
  }

  public void setOrganisation(final Organisation organisation) {
    this.organisation = organisation;
  }

  public String getReason() {
    return this.reason;
  }

  public void setReason(final String reason) {
    this.reason = reason;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public BigDecimal getInventoryQuantity() {
    return inventoryQuantity;
  }

  public void setInventoryQuantity(final BigDecimal inventoryQuantity) {
    this.inventoryQuantity = inventoryQuantity;
  }

  public BigDecimal getPhysicalQuantity() {
    return physicalQuantity;
  }

  public void setPhysicalQuantity(final BigDecimal physicalQuantity) {
    this.physicalQuantity = physicalQuantity;
  }

  public void setDifference(final BigDecimal difference) {
    this.difference = difference;
  }

  public BigDecimal getDifference() {
    return difference;
  }

  @Override
  public String toString() {
    return "InventoryAudit [id="
        + id
        + ", product="
        + product
        + ", productName="
        + productName
        + ", branch="
        + branch
        + ", organisation="
        + organisation
        + ", inventoryQuantity="
        + inventoryQuantity
        + ", physicalQuantity="
        + physicalQuantity
        + ", reason="
        + reason
        + ", difference="
        + difference
        + ", date="
        + date
        + "]";
  }
}
