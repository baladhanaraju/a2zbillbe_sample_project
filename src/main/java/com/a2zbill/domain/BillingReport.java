package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Employee;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "billing_report", schema = "product")
public class BillingReport implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "billing_reportid_seq")
  @SequenceGenerator(
      name = "billing_reportid_seq",
      sequenceName = "billing_reportid_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "report_id")
  private long id;

  @Column(name = "opening_balance")
  private BigDecimal opening_balance;

  @Column(name = "closing_balance")
  private BigDecimal closing_balance;

  @Column(name = "date")
  private Date date;

  @Column(name = "created_time")
  private Date createdTime;

  @Column(name = "modified_time")
  private Date modifiedTime;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "emp_id", unique = true, nullable = true)
  private Employee employee;

  public BillingReport() {}

  public BillingReport(
      final long id,
      final BigDecimal opening_balance,
      final BigDecimal closing_balance,
      final Date date,
      final Date createdTime,
      final Date modifiedTime,
      final Employee employee) {
    super();
    this.id = id;
    this.opening_balance = opening_balance;
    this.closing_balance = closing_balance;
    this.date = date;
    this.createdTime = createdTime;
    this.modifiedTime = modifiedTime;
    this.employee = employee;
  }

  public long getId() {
    return this.id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public BigDecimal getOpening_balance() {
    return this.opening_balance;
  }

  public void setOpening_balance(final BigDecimal opening_balance) {
    this.opening_balance = opening_balance;
  }

  public BigDecimal getClosing_balance() {
    return this.closing_balance;
  }

  public void setClosing_balance(final BigDecimal closing_balance) {
    this.closing_balance = closing_balance;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public Date getCreatedTime() {
    return this.createdTime;
  }

  public void setCreatedTime(final Date createdTime) {
    this.createdTime = createdTime;
  }

  public Date getModifiedTime() {
    return this.modifiedTime;
  }

  public void setModifiedTime(final Date modifiedTime) {
    this.modifiedTime = modifiedTime;
  }

  public Employee getEmployee() {
    return this.employee;
  }

  public void setEmployee(final Employee employee) {
    this.employee = employee;
  }

  @Override
  public String toString() {
    return "BillingReport [id="
        + id
        + ", opening_balance="
        + opening_balance
        + ", closing_balance="
        + closing_balance
        + ", date="
        + date
        + ", createdTime="
        + createdTime
        + ", modifiedTime="
        + modifiedTime
        + ", employee="
        + employee
        + "]";
  }
}
