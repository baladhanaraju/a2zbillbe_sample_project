package com.a2zbill.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tsss.basic.domain.Branch;
import com.tsss.basic.domain.Organisation;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@SuppressWarnings("serial")
@Entity
@Table(name = "product_reward_points", schema = "product")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
public class ProductRewardPoint implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_reward_points_id_seq")
  @SequenceGenerator(
      name = "product_reward_points_id_seq",
      sequenceName = "product_reward_points_id_seq",
      allocationSize = 1,
      schema = "product")
  @Column(name = "id")
  private long id;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", unique = true, nullable = true)
  private Product product;

  @Column(name = "reward_points")
  private long rewardPonts;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "org_id", unique = true, nullable = true)
  private Organisation orgId;

  @JsonManagedReference
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "branch_id", unique = true, nullable = true)
  private Branch branchId;

  public ProductRewardPoint() {}

  public ProductRewardPoint(
      long id, Product product, long rewardPonts, Organisation orgId, Branch branchId) {
    super();
    this.id = id;
    this.product = product;
    this.rewardPonts = rewardPonts;
    this.orgId = orgId;
    this.branchId = branchId;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Product getProduct() {
    return this.product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public long getRewardPonts() {
    return this.rewardPonts;
  }

  public void setRewardPonts(long rewardPonts) {
    this.rewardPonts = rewardPonts;
  }

  public Organisation getOrgId() {
    return this.orgId;
  }

  public void setOrgId(Organisation orgId) {
    this.orgId = orgId;
  }

  public Branch getBranchId() {
    return this.branchId;
  }

  public void setBranchId(Branch branchId) {
    this.branchId = branchId;
  }

  @Override
  public String toString() {
    return "ProductRewardPoint [id="
        + id
        + ", product="
        + product
        + ", rewardPonts="
        + rewardPonts
        + ", orgId="
        + orgId
        + ", branchId="
        + branchId
        + "]";
  }
}
