package com.a2zbill.config;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Service
@EnableScheduling
public class KotService {

  private static final Logger LOGGER = LoggerFactory.getLogger(KotService.class);

  private Map<String, SseEmitter> emitters;

  private SseEmitter emitter;

  public Map<String, SseEmitter> getEmitters() {
    return emitters;
  }

  public void setEmitters(final Map<String, SseEmitter> emitters) {
    this.emitters = emitters;
  }

  public SseEmitter getEmitter() {
    return emitter;
  }

  public void setEmitter(final SseEmitter emitter) {
    this.emitter = emitter;
  }

  public SseEmitter getEmitterByBranchId(final String branchId) {
    try {
      for (final Map.Entry<String, SseEmitter> entry : emitters.entrySet()) {
        LOGGER.info("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        if (entry.getKey().equals(branchId)) {
          emitter = entry.getValue();
        }
      }
    } catch (final Exception e) {
      LOGGER.error("getEmitterByBranchId:" + e, e);
    }
    return emitter;
  }
}
