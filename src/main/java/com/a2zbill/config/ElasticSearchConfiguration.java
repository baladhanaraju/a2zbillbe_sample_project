package com.a2zbill.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ElasticSearchConfiguration extends AbstractFactoryBean<RestHighLevelClient> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchConfiguration.class);

  @Value("${spring.data.elasticsearch.cluster-nodes}")
  private String clusterNodes;

  @Value("${spring.data.elasticsearch.cluster-name}")
  private String clusterName;

  private RestHighLevelClient restHighLevelClient;

  @Value("${elasticsearch.cluster-host}")
  private String hostName;

  @Value("${elasticsearch.cluster-port}")
  private Integer port;

  @Value("${elasticsearch.cluster-userName}")
  private String userName;

  @Value("${elasticsearch.cluster-password}")
  private String password;

  @Value("${elasticsearch.cluster-protocol}")
  private String protocol;

  @Override
  public void destroy() {
    try {
      if (restHighLevelClient != null) {
        restHighLevelClient.close();
      }
    } catch (final Exception e) {
      LOGGER.error("Error closing ElasticSearch client: ", e);
    }
  }

  @Override
  public Class<RestHighLevelClient> getObjectType() {
    return RestHighLevelClient.class;
  }

  @Override
  public boolean isSingleton() {
    return false;
  }

  @Override
  public RestHighLevelClient createInstance() {
    return buildClient();
  }

  private RestHighLevelClient buildClient() {
    try {
      final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
      credentialsProvider.setCredentials(
          AuthScope.ANY, new UsernamePasswordCredentials(userName, password));
      final RestClientBuilder builder =
          RestClient.builder(new HttpHost(hostName, port, protocol))
              .setHttpClientConfigCallback(
                  httpClientBuilder ->
                      httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));

      return new RestHighLevelClient(builder);
    } catch (final Exception e) {
      LOGGER.error("Error closing ElasticSearch client: ", e);
    }
    return restHighLevelClient;
  }
}
