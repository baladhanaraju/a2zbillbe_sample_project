package com.tsss.a2zbill.test.util;

import com.a2zbill.domain.Section;
import java.util.List;
import java.util.StringJoiner;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.PayloadDocumentation;

public class SectionModelDescription {

  private SectionModelDescription() {}

  /** @return The full mobile suit description */
  public static FieldDescriptor[] getAllSectionsSuit() {
    return new FieldDescriptor[] {
      PayloadDocumentation.fieldWithPath("id").description("Section Id"),
      PayloadDocumentation.fieldWithPath("sectionName").description("Section Name"),
      PayloadDocumentation.fieldWithPath("displayName").description("Display Name"),
      PayloadDocumentation.fieldWithPath("createdDate").description("Section createdDate"),
      PayloadDocumentation.fieldWithPath("modifiedDate").description("Section modifiedDate")
    };
  }

  public static FieldDescriptor[] saveSectionSuit() {

    return new FieldDescriptor[] {
      PayloadDocumentation.fieldWithPath("id")
          .description("Section Id. " + getConstraints(Section.class, "id")),
      PayloadDocumentation.fieldWithPath("sectionName")
          .description("Section Name " + getConstraints(Section.class, "sectionName")),
      PayloadDocumentation.fieldWithPath("displayName")
          .description("Display Name " + getConstraints(Section.class, "displayName")),
      PayloadDocumentation.fieldWithPath("createdDate")
          .description("Section createdDate " + getConstraints(Section.class, "createdDate")),
      PayloadDocumentation.fieldWithPath("modifiedDate")
          .description("Section modifiedDate " + getConstraints(Section.class, "modifiedDate"))
    };
  }

  private static <T> String getConstraints(Class<T> clazz, String property) {
    ConstraintDescriptions userConstraints = new ConstraintDescriptions(clazz);
    List<String> descriptions = userConstraints.descriptionsForProperty(property);

    StringJoiner stringJoiner = new StringJoiner(". ", "", ".");
    descriptions.forEach(stringJoiner::add);
    return stringJoiner.toString();
  }
}
