package com.a2zbill;

import com.a2zbill.domain.Login;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class JwtToken {
  @Autowired private TestRestTemplate restTemplate = new TestRestTemplate();

  public String getTokenHeaderEmployee() {

    try {
      final String urlToken = "https://dev.api.reports.dealwallet.com/basictemplate/auth";

      final Login login = new Login();
      login.setUserName("barbqueemployee@gmail.com");
      login.setPassword("pridepride");

      final HttpHeaders headers = new HttpHeaders();
      headers.set("Content-Type", "application/json");

      final HttpEntity<Login> entity = new HttpEntity<Login>(login, headers);
      final ResponseEntity<String> responseToken =
          restTemplate.exchange(urlToken, HttpMethod.POST, entity, String.class);
      final String tokenObj = responseToken.getBody();
      if (tokenObj != null) {
        final JSONObject jsonObject = new JSONObject(tokenObj);
        return jsonObject.getString("token");
      }
    } catch (final Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public String getTokenHeaderCustmer() {

    try {
      final String urlToken = "https://dev.api.reports.dealwallet.com/basictemplate/auth/customer";

      final Login login = new Login();
      login.setUserName("8098098099");
      login.setPassword("pridepride");

      final HttpHeaders headers = new HttpHeaders();
      headers.set("Content-Type", "application/json");

      final HttpEntity<Login> entity = new HttpEntity<Login>(login, headers);

      final ResponseEntity<String> responseToken =
          restTemplate.exchange(urlToken, HttpMethod.POST, entity, String.class);
      final String tokenObj = responseToken.getBody();
      if (tokenObj != null) {
        final JSONObject jsonObject = new JSONObject(tokenObj);
        return jsonObject.getString("token");
      }
    } catch (final Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public HttpHeaders getHttpHeadersCustomer() {
    final HttpHeaders headers = new HttpHeaders();
    final String token = getTokenHeaderCustmer();
    if (token != null) {
      headers.set("Authorization", "Bearer " + token);
    } else {
      headers.set(
          "Authorization",
          "Bearer eyJhbGciOiJSUzI1NiJ9.eyJicmFuY2hJZCI6OCwic3ViIjoiNzM3X0MiLCJyb2xlcyI6WyJST0xFX0NVU1RPTUVSIl0sImlkIjo3MzcsImV4cCI6MTU5NjE5NTE4NjEwOSwiaWF0IjoxNTk1NTkwMzg2LCJvcmdJZCI6MjYxfQ.V2BjwZ2pqd9RQ3mrc6eGUsPOtmuRR3nLxHHJmlVHamZP8bO13PzLBYerOepVYtvd-V9JOTnNI9NoMYdu4dP5NWynpSXbmGy0186T7KDcF1Crb4O1VO44QDsd0-7LXue2vZDal__R32yPwXTQAqYG55JMX-bSwodvyiHMbAl_3RzlMQ-DAToaJ6dch60RwnmlyIQ9OUD-F5cva-akxyo_gYRzqeQ0b_KZFh9anDZ6iPyeCsnfy55VUYwajHiLr08wPgyH-3V3C0AacUhvk39ZYJvwXxSD9MgSJGqKRHIe5LyQc-Lq_ItEb7DthFK7BV6JP6mguW6Xf-0--Gl8OOWIMA");
    }
    return headers;
  }

  public HttpHeaders getHttpHeadersEmployee() {
    final HttpHeaders headers = new HttpHeaders();
    final String token = getTokenHeaderEmployee();
    if (token != null) {
      headers.set("Authorization", "Bearer " + token);
    } else {
      headers.set(
          "Authorization",
          "Bearer eyJhbGciOiJSUzI1NiJ9.eyJicmFuY2hJZCI6OCwic3ViIjoiNzg2NV9FIiwicm9sZXMiOlsiUk9MRV9FTVBMT1lFRV9SIiwiUk9MRV9BRE1JTl9XIl0sImlkIjo3ODY1LCJleHAiOjE1ODY5MzM2MjgzNTAsImlhdCI6MTU4NjMyODgyOCwib3JnSWQiOjI2MX0.KInIgqLKqOznzEdBrttI2-BxS9p3QWQF0TQ7FPBpblK-kTnRPsT1ycT5YikolPr3OusFCxuG-Wt6OcZXmZIfZC_12jvjo9Ir82fdFfwY-DIYuZ4bCXDnxfBSMdS386XJq2BE4GypAiO-D4AQjgUFSra59iHKDFykaGToD_6bsd3RvhMPDu1aLmzjxdRMSLM1P2Os2T3w3FAJyWd0AF4SxT4HCpH96CTmXbzU2Eff-7VBiqnV5cAaHdpPkctI3LfKfKWuWhmHw4HXXuoboqWcIgxAfjwBs4DC0qtqsTHENlzGod67EhuSgQMU5psrWZGm1vTjOoylKtkd3UPFUn2SZA");
    }
    return headers;
  }
}
