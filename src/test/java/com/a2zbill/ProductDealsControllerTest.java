package com.a2zbill;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import com.a2zbill.controller.constants.RouteConstants;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = A2ZBillApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductDealsControllerTest {

  @LocalServerPort private int port;

  @Autowired private TestRestTemplate restTemplate;

  final JwtToken jwtToken = new JwtToken();
  HttpHeaders headers = jwtToken.getHttpHeadersEmployee();

  @BeforeEach
  void setupThis() {
    System.out.println("@BeforeEach executed");
    headers = jwtToken.getHttpHeadersEmployee();
  }

  private String createURLWithPort(final String uri) {
    return "http://localhost:" + port + uri;
  }

  @Test
  public void getAllDeals() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.GET_ALL_DEALS_DETAILS),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }
}
