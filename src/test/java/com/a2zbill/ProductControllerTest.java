package com.a2zbill;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import com.a2zbill.controller.constants.RouteConstants;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = A2ZBillApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {
  @LocalServerPort private int port;

  @Autowired private TestRestTemplate restTemplate;

  final JwtToken jwtToken = new JwtToken();
  HttpHeaders headers = jwtToken.getHttpHeadersEmployee();

  @BeforeEach
  void setupThis() {
    System.out.println("@BeforeEach executed");
    headers = jwtToken.getHttpHeadersEmployee();
  }

  private String createURLWithPort(final String uri) {
    return "http://localhost:" + port + uri;
  }

  @Test
  public void getMenuProductTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.PRODUCT_MENU_DETAILS),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getProductNameDetailsTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.ALL_PRODUCT_NAME_DETAILS),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getProductDetailsTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.PRODUCT_CATEGORYDETAILS),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getSingleProductDetailsTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/api/singleProduct?productCode=98486"),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getProudctFeaturesDetailsTest() throws Exception {

    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/api/product/productFeaturesDetails/413"),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getAllProductDetailsTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.ALL_PRODUCT_DETAILS),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getItemDetailsTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);

    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.ALL_ITEM_DETAILS),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getEditProductPageTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/admin/updatePage/3000"), HttpMethod.GET, entity, String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getProductDetailsByNameTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/api/product/names?productName=VINEGAR"),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getBillingInfoTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/api/product/billing/details?billId=255"),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getBillingInfoByPercentagesTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/api/product/billing/percentages/details?billId=255"),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getCartProductDetailsByCounterIdTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.GET_ALL_CARTPRODUCT_DETAILS_COUNTRERID),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getOrderFlowByCounterIdTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.GET_ALL_ORDERFLOW_DETAILS),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getCartProductDetailsSummaryPageByFlowIdTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.GET_ALL_CARTPRODUCT_DETAILSSUMMARY),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getCartProSummaryPageByFlowIdTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.GET_ALL_ORDERFLOW_DETAILSSUMMARY),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getProductsOfMenuCategoryTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.MENUCATEGORY_PRODUCT_ACTIVE),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getAllProductsActiveTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.PRODUCT_LIST_ACTIVE),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getAllProductsTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort(RouteConstants.PRODUCT_LIST_SORTING),
            HttpMethod.GET,
            entity,
            String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getProductsBYCategoryTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/org/categoryproducts/30"), HttpMethod.GET, entity, String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }

  @Test
  public void getProductsBYProductIdTest() throws Exception {
    final String expected = "[]";
    final HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    final ResponseEntity<String> response =
        restTemplate.exchange(
            createURLWithPort("/product/413"), HttpMethod.GET, entity, String.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    if (response.getBody().isEmpty()) {
      JSONAssert.assertEquals(expected, response.getBody(), true);
    } else {
      assertFalse(response.getBody().isEmpty());
    }
  }
}
