from openpyxl import load_workbook
import requests

book = load_workbook("a2zbillbe-online-api-urls.xlsx")
sheet = book.active
headers = {
    "Authorization": "Bearer eyJhbGciOiJSUzI1NiJ9.eyJicmFuY2hJZCI6OCwic3ViIjoiNzg2NV9FIiwicm9sZXMiOlsiUk9MRV9FTVBMT1lFRV9SIiwiUk9MRV9BRE1JTl9XIl0sImlkIjo3ODY1LCJleHAiOjE1ODgxNzI2NjY3NjIsImlhdCI6MTU4NzU2Nzg2Niwib3JnSWQiOjI2MX0.Nu9Wy6sfgAWgtpSObbgb68eTaiePrZyCQLh9PsnBuUsXHPwlQmxYE9v_p_AdK-kX7jvZGm-cAw2klFHEnT1Zsq_owUg82aH-h-ek6YYXDEpYJVPjsSwT5t86ypRq3tqT105tPwdIjWpn9_UJox4HwuNLvC5-MKQBc0nKLJ66OE6vxQaHf6d0bka93FX8iPa3mnb7TQ6C6l_A1eV3xoWr85TTprY1Yf1CiEMj-CwkTGgO5GA-6J7ZL6VQSVkvH3DTJcpoN53S-UnoGLUywVT-SfI3OTKnnDCUoEE9Jh1KvdZzD5z4MJoIC8Agytq-xbAcbII15zqGmPP8Q_ri-i_d3g"
}
a2zbillbe = 'https://qa.api.a2zbill.dealwallet.com'


class TestA2ZBillBEAPIURLS:

    def __init__(self):
        for x in range(2, 216):
            if "a2zbillbe" == sheet.cell(row=x, column=1).value:
                api_main_url = a2zbillbe + sheet.cell(row=x, column=5).value
                response = requests.get(url=api_main_url, headers=headers).status_code
                # if response == 200:
                #     print('\033[0m', x, sheet.cell(row=x, column=3).value, (api_main_url), "response status code is ",
                #           response)
                if response != 200:
                    print('\033[93m', x, (api_main_url), "response failed  ",
                          response)


TestA2ZBillBEAPIURLS()
